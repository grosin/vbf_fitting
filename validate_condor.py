import os
import sys
import mmap


def check_error_file(resubmit=False):
    folder=sys.argv[1]
    source_dir="makeInputs/submission/"+folder+"/error/"
    #source_dir="/eos/user/g/grosin/"+folder+"/error/"
    data_list=os.listdir(source_dir)
    for error_file in data_list:
        with open(source_dir+error_file) as f:
            s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
            if s.find('Error') != -1:
                if resubmit:
                    submission_file="makeInputs/submission/"+folder+"/"+error_file[:-6]+"sub"
                    root_file="makeInputs/submission/"+folder+"/run/"+error_file[:-7]+"/output_"+folder[folder.find("jobsSamplesAndSys_")+len("jobsSamplesAndSys_"):]+".root"
                    os.system("rm "+root_file)
                    print("deleting "+root_file)
                    os.system("condor_submit "+submission_file)       
                print(error_file)
    source_dir="makeInputs/submission/"+folder+"/output/"
    data_list=os.listdir(source_dir)
    for error_file in data_list:
        with open(source_dir+error_file) as f:
            s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
            if s.find('Error') != -1:
                if resubmit:
                    submission_file="makeInputs/submission/"+folder+"/"+error_file[:-6]+"sub"
                    root_file="makeInputs/submission/"+folder+"/run/"+error_file[:-7]+"/output_"+folder[folder.find("jobsSamplesAndSys_")+len("jobsSamplesAndSys_"):]+".root"
                    os.system("rm "+root_file)
                    print("deleting "+root_file)
                    os.system("condor_submit "+submission_file)       
                print(error_file)
                    

def validate_list():
    samples=["top","diboson","Zjets","vbf","vh","htt","ggf","Vgamma"]
    all_systs="makeInputs/AllSystematics.txt"
    current_systs=sys.argv[1]
    all_systematics=set()
    with open(all_systs) as als:
        for line in als:
            for s in samples:
                all_systematics.add(s+"_"+line.strip())
    used_systs=set()
    with open(current_systs) as csf:
        for line in csf:
            used_systs.add(line.split("/")[7])
            
    print(all_systematics - used_systs)

def resubmit(sub_file):
    folder=sys.argv[1]
    submission_file="makeInputs/submission/"+folder+"/"+sub_file+".sub"
    root_file="makeInputs/submission/"+folder+"/run/"+sub_file+".root"
    os.system("rm "+root_file)
    print("deleting "+root_file)
    print(submission_file)
    os.system("condor_submit "+submission_file)



check_error_file()


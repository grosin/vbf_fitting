#!/bin/bash
source /atlasgpfs01/usatlas/data/jennyz/HistFitter/histfitter/setup.sh
echo $(pwd) >> /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/clusterPath1.txt
HistFitter.py -wfa -u "reread variable:$1" /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/HWW_3D_hist_noBDTZjetsInSR_noBDT_v21_decorr_theor.py
mv results/hist_v2_StefBinning_v21_r21_28_09_2020_v1_noBDTCut_SRL_ggFCRZeroOneJet_ABCD_TopCRinSR_decorr1JetSys_$1/ /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/results/$1_$2 
cd results/
echo $(pwd) >> /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/clusterPath.txt

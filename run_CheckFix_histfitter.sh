#!/bin/bash
/atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/checkAndFixFitInputs_compile $1 $2 true
/atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/checkAndFixFitInputs_compile $1 $2 false
cp $1.4.root /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/data/hist_v2_StefBinning_v21_r21_28_09_2020_v1_noBDTCut_SRL_ggFCRZeroOneJet_ABCD_TopCRinSR_decorr1JetSys_$2.root
source /atlasgpfs01/usatlas/data/jennyz/HistFitter/histfitter/setup.sh
HistFitter.py -wfa -u "reread variable:$2" /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/HWW_3D_hist_noBDTZjetsInSR_noBDT_v21_decorr_theor_statOnly.py
mv results/hist_v2_StefBinning_v21_r21_28_09_2020_v1_noBDTCut_SRL_ggFCRZeroOneJet_ABCD_TopCRinSR_decorr1JetSys_$2/ /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/results/$2_$3 

Executable = /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/run_checkNfix.sh
Output = /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/outCondor
Error = /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/errorCondor
Log = /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/logCondor
accounting_group = group_atlas.bnl
should_transfer_files = YES 
when_to_transfer_output = ON_EXIT
GetEnv          = True

+MaxRuntime = 1000
Queue 1 Arguments from (
/atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/makeInputs/dphijj_theoNov9_allSys.root SignedDPhijj
/atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/makeInputs/mll_theoNov9_allSys.root Mll
/atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/makeInputs/jet0_pt_theoNov9_allSys.root jet0_pt
/atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/makeInputs/lep0_pt_theoNov9_allSys.root lep0_pt
)


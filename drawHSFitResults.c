#include "tools.h"
//string prefix="Cut2Jet_v2_";
string prefix="";
bool isDiff=true;
bool divideByBinWidth=true;
bool  forceRebinned=false;

vector<string> split(const char *str, char c = ' ')
{
    vector<string> result;

    do
    {
        const char *begin = str;

        while(*str != c && *str)
            str++;
	//	cout<<"Ret "<<string(begin, str)<<endl;
        result.push_back(string(begin, str));
    } while (0 != *str++);

    return result;
}


TH1F *find_kname(string name, std::map<string, TH1F*> histMaps){
  TH1F *h=nullptr;
  for(std::map<string, TH1F*>::iterator it =histMaps.begin(); it!=histMaps.end(); it++){
    if( (*it).first.compare( name) ==0 ) { h=(TH1F*) (*it).second; break ;} 

  }
  return h; 

} 
 

string eraseSubStr(std::string in, const std::string & toErase)
{
  std::string mainStr=in;
  // Search for the substring in string
  size_t pos = mainStr.find(toErase);
 
  if (pos != std::string::npos)
	{
      // If found then erase it from string
      mainStr.erase(pos, toErase.length());
	}
  return mainStr;
}

void drawHSFitResults(string fname,string currentDistribution="Mjj",string binningFile="", string bbS="vbf0"){
  if ( currentDistribution.compare("inc")==0) isDiff=false;
  else isDiff=true;

  std::map<string,int> nBinsDiff;
  nBinsDiff["Mjj"]=6;
  nBinsDiff["lep0_pt"]=8;
  nBinsDiff["jet0_pt"]=7;
  nBinsDiff["Mll"]=8;
  nBinsDiff["DYjj"]=11;
  nBinsDiff["DYll"]=10;
  nBinsDiff["DPhill"]=8;

  //string currentDistribution="Mjj";
  std::map<string, vector<double>> binningMap; 
  bool hasRebinnedInput=binningFile.size()>0;
  if( hasRebinnedInput){
    ifstream bfile(binningFile);
    string line;
    while(std::getline(bfile,line)){
      //cout<<"Line "<<line<<endl;
    vector <string> sline=split(line.c_str());

    for(int i=2; i<(int)sline.size(); i++){
      binningMap[sline[0]].push_back(std::stod(sline[i]));
      //cout<<"Binning for "<<sline[0]<<" "<<sline[i]<<endl;
    }}

    bfile.close();
  }
  
  
  if(currentDistribution.compare("inc")==0) forceRebinned=true; 
  
  string basename=eraseSubStr(fname,".root");
  
  TFile *f=TFile::Open(fname.c_str());
  std::map<string, TObject*> objMap;
  std::map<string, TH1F*> histMaps;
  std::map<string, RooCurve*> curveMaps;
  std::map<string, RooHist*> rooHistMaps;
  std::map<string, string> legendMap;
  

  std::map<string, int> colorMap;

  colorMap[prefix+"ggf"]=kBlue+1;
  colorMap[prefix+"ggf1"]=kBlue+1;
  colorMap[prefix+"ggf2"]=kBlue+1;
  colorMap[prefix+"ggf3"]=kBlue+1;
  
  colorMap[prefix+"vbf"]=kRed;
  colorMap[prefix+"vbf0"]=kRed;
  
  if(isDiff){
    for(int i=0; i<(int) nBinsDiff[currentDistribution]; i++){
      colorMap[prefix+Form("vbf0_%d",i)]=kRed; 
    }
  }
  

  std::map<string,string> xAxisNames;
  xAxisNames["CRZjets"]="#it{m}_{T} [MeV]";
  xAxisNames["CRTop"]="#it{D}_{Top+WW}";
  xAxisNames["CRGGF1"]="#it{D}_{ggF}^{1}";
  xAxisNames["CRGGF2"]="#it{D}_{ggF}^{2}";
  xAxisNames["CRGGF3"]="#it{D}_{ggF}^{3}";
  xAxisNames["SRVBF"]="#it{D}_{vbf}";
  if( isDiff){
    for( int i=0; i<(int)20; i++){
      xAxisNames[Form("CRTop_%d",i)]="#it{D}_{Top+WW}";
      xAxisNames[Form("SRVBF_%d",i)]="#it{D}_{vbf}";
    }
  }
  
  colorMap[prefix+"Vgamma"]=kGreen+2;
  colorMap[prefix+"Zjets0"]=kGreen;
  colorMap[prefix+"diboson"]=kYellow+2;
  colorMap[prefix+"diboson1"]=kYellow+2;
  colorMap[prefix+"diboson2"]=kYellow+2;
  colorMap[prefix+"diboson3"]=kYellow+2;
  
  colorMap[prefix+"top"]=kMagenta+2;
  colorMap[prefix+"top1"]=kMagenta+2;
  colorMap[prefix+"top2"]=kMagenta+2;
  colorMap[prefix+"top3"]=kMagenta+2;
  
  colorMap[prefix+"vh"]=kBlue+3;
  colorMap[prefix+"htt"]=kBlue+4;
  colorMap[prefix+"Fakes"]=kYellow+4;
    
  colorMap[prefix+"Zjets1"]=kGreen;
  colorMap[prefix+"diboson1"]=kYellow+2;
  

  legendMap[prefix+"ggf"]="ggF";
  legendMap[prefix+"ggf1"]="ggF";
  legendMap[prefix+"ggf2"]="ggF";
  legendMap[prefix+"ggf3"]="ggF";
  
  legendMap[prefix+"vbf"]="VBF";
  legendMap[prefix+"vbf0"]="VBF";
  if(isDiff){
    for(int i=0; i<(int) nBinsDiff[currentDistribution]; i++)
      legendMap[prefix+Form("vbf0_%d",i)]="vbf"; 
  }
  
  legendMap[prefix+"Vgamma"]="#it{V}/#gamma";
  legendMap[prefix+"Zjets0"]="#it{Z}+jets";
  legendMap[prefix+"top"]="Top";
  legendMap[prefix+"top1"]="Top";
  legendMap[prefix+"top2"]="Top";
  legendMap[prefix+"top3"]="Top";
  legendMap[prefix+"diboson"]="Diboson";
  legendMap[prefix+"diboson1"]="Diboson";
  legendMap[prefix+"diboson2"]="Diboson";
  legendMap[prefix+"diboson3"]="Diboson";
  legendMap[prefix+"Fakes"]="Fakes";
  legendMap[prefix+"vh"]="VH";
  legendMap[prefix+"htt"]="H#rightarrow#tau#tau";
  legendMap["SM_total"]="Total";


  legendMap[prefix+"Vgamma1"]="#it{V}/#gamma";
  legendMap[prefix+"Zjets1"]="#it{Z}+jets";
  legendMap[prefix+"top1"]="Top";
  legendMap[prefix+"diboson1"]="Diboson";
  legendMap[prefix+"Fakes1"]="Fakes";

  // missing vh, ttH, et al 
           
  // components to draw 
  vector <string> draw;
 
  draw.push_back(prefix+"htt");
  draw.push_back(prefix+"vh");
  draw.push_back(prefix+"Vgamma");
  draw.push_back(prefix+"Fakes");
  draw.push_back(prefix+"ggf"); //draw.push_back(prefix+"Zjets"); draw.push_back(prefix+"top"); draw.push_back(prefix+"diboson");
  draw.push_back(prefix+"ggf1");
  draw.push_back(prefix+"ggf2");
  draw.push_back(prefix+"ggf3");
  if( !TString(fname.c_str()).Contains("CRZjets")){
    draw.push_back(prefix+"Zjets0");
    draw.push_back(prefix+"Zjets1");
  }
  draw.push_back(prefix+"diboson1");
  draw.push_back(prefix+"diboson2");
  draw.push_back(prefix+"diboson3");
  draw.push_back(prefix+"diboson");
  draw.push_back(prefix+"top1");
  draw.push_back(prefix+"top2");
  draw.push_back(prefix+"top3");
  draw.push_back(prefix+"top");

  if( TString(fname.c_str()).Contains("CRZjets")){
    draw.push_back(prefix+"Zjets0");
    draw.push_back(prefix+"Zjets1");
  }
  if(isDiff){
    for(int i=0; i<(int) nBinsDiff[currentDistribution]; i++)
      draw.push_back(prefix+Form("vbf0_%d",i));
  }
  else 
    draw.push_back(prefix+"vbf0");

  //for( int i=0; i<5; i++)
  //draw.push_back(prefix+"vbf0_"+Form("%d",i));
  
  vector <string> load;
  for( vector<string>::iterator it=draw.begin(); it!=draw.end(); it++) load.push_back(*it);
  load.push_back("h_asimovData");
  load.push_back("h_ratio");
  load.push_back("h_ratio_excl_sig");
  load.push_back("h_rel_error_band");
  load.push_back("h_total_error_band");

  // Stack of contributions 
  THStack *stack = new THStack(Form("stack_%s",basename.c_str()),"");
  
  //for(vector<string>::iterator it = load.begin(); it!=load.end(); it++){
  TKey *keyP=nullptr;
  TIter nextP(f->GetListOfKeys());

  
  
  // maximum y range;
  double maxRange=0;
  string firstSignal="";
  string whichReg="";
  string xAxisTitle="";
  for(std::map<string,string>::iterator it=xAxisNames.begin(); it!=xAxisNames.end(); it++){
    if( TString(fname.c_str()).Contains( (*it).first) ){
      cout<<(*it).second<<"\n";
      xAxisTitle=(*it).second;
      break ;
    }
  }
  
  vector <double > xbinsInclusive={0.5,0.7,0.86,0.94,1};

  for(std::map<string, vector<double>>::iterator il=binningMap.begin(); il!=binningMap.end(); il++){
    cout<<"Checking "<<fname<<" against "<<(*il).first<<endl;
    if( TString(fname.c_str()).Contains( (*il).first.c_str())) { whichReg=(*il).first; cout<<"found region "<<(*il).first<<endl; hasRebinnedInput=true; break; }
    hasRebinnedInput=false;
  }

  if( forceRebinned && !hasRebinnedInput && TString(fname.c_str()).Contains("SRVBF")){
    whichReg="SRVBF";
    binningMap["SRVBF"]=xbinsInclusive; 
    hasRebinnedInput=true;
  }
  
  if(!hasRebinnedInput) divideByBinWidth=false; 
  
  while ((keyP=(TKey*)nextP())) {
    string kName=keyP->GetName();
    //if (strcmp(keyP->GetClassName(),"TTree")) continue;
    //if( (TObject*)f->Get((*it).c_str())==nullptr) { cout<<"object "<<*it<<" not found skipping "<<endl; continue; }
    objMap[kName]=(TObject*)f->Get(kName.c_str());

    cout<<"key name"<<kName<<"\n";
    
    if( (TString(keyP->GetClassName())).Contains("TH1")){
      firstSignal=kName;

      if(hasRebinnedInput && whichReg.size()>0 ) {
	TH1F *hraw=(TH1F*)f->Get(kName.c_str());
	hraw->SetName(Form("%sRaw",kName.c_str()));
	vector <double> xbins=binningMap[whichReg]; 
	histMaps[kName]=new TH1F(kName.c_str(),"",xbins.size()-1,&xbins[0]);
	histMaps[kName]->GetXaxis()->SetTitle(hraw->GetXaxis()->GetTitle());
	histMaps[kName]->GetYaxis()->SetTitle("Events");
	// fill the rebinned contents 
	for( int b=1; b<(int)hraw->GetNbinsX()+1; b++){
	  histMaps[kName]->SetBinContent(b,hraw->GetBinContent(b));
	  histMaps[kName]->SetBinError(b,hraw->GetBinError(b));
	  cout<<"Remapping bin"<<b << " lowEdgeIn "<<hraw->GetBinLowEdge(b)<<" low edge out "<<	histMaps[kName]->GetBinLowEdge(b)<<endl;
	}
      }
      else {
	
	histMaps[kName]=(TH1F*)f->Get(kName.c_str());
      }
      
     
      if( divideByBinWidth) {
	for( int b=1; b<(int)histMaps[kName]->GetNbinsX()+1; b++){
	  double error=histMaps[kName]->GetBinError(b);
	  histMaps[kName]->SetBinContent(b,histMaps[kName]->GetBinContent(b)/histMaps[kName]->GetBinWidth(b));
	  histMaps[kName]->SetBinError(b,error);
	  cout<<"Dividing by bin width "<<endl;
	}
	histMaps[kName]->GetYaxis()->SetTitle("Events / bin ");
      }

      if( xAxisTitle.size() >0 )
	histMaps[kName]->GetXaxis()->SetTitle(xAxisTitle.c_str());
      
      maxRange=maxRange < histMaps[kName]->GetBinContent(histMaps[kName]->GetMaximumBin()) ? histMaps[kName]->GetBinContent(histMaps[kName]->GetMaximumBin()) : maxRange; 
      
      SetDef(histMaps[kName]);
      cout<<kName<<"\n";
      if( colorMap.count(kName) ) {
        cout<<"Setting color for "<<kName<<" color "<<colorMap[kName]<<endl;
        histMaps[kName]->SetFillColor(colorMap[kName]);
        histMaps[kName]->SetLineColor(kBlack);
        histMaps[kName]->SetLineWidth(2);
        //histMaps[kName]->SetFillStyle(1);
        //stack->Add((TH1F*) histMaps[kName]);
        
      }
    }
    
    else if ( (TString(keyP->GetClassName())).Contains("RooCurve")){
      curveMaps[kName]=(RooCurve*)f->Get(kName.c_str());
      if(hasRebinnedInput && whichReg.size()>0 && !TString(kName).Contains("ratio") ){
	vector <double> xbins=binningMap[whichReg];
	for( int i=0; i<(int)curveMaps[kName]->GetN(); i++){
	  double val=0; double xval=0;
	  curveMaps[kName]->GetPoint(i,xval,val);
	  if( divideByBinWidth) val=val/(xbins[i+1]-xbins[i]);
	  curveMaps[kName]->SetPoint(i,xbins[i]+(xbins[i+1]-xbins[i])*0.5,val);
	}
      }
    }
    else if ( (TString(keyP->GetClassName())).Contains("RooHist")){
      rooHistMaps[kName]=(RooHist*)f->Get(kName.c_str());
      
      if(hasRebinnedInput && whichReg.size()>0 && !TString(kName).Contains("ratio")){
	vector <double> xbins=binningMap[whichReg];
	for( int i=0; i<(int)rooHistMaps[kName]->GetN(); i++){
	  double val=0; double xval=0;
	  rooHistMaps[kName]->GetPoint(i,xval,val);
	  double errorLow=rooHistMaps[kName]->GetErrorYlow(i);
	  double errorHigh=rooHistMaps[kName]->GetErrorYhigh(i);
	  cout<<"old val "<<val;
	  
	  if(divideByBinWidth) {
	    val=val/(xbins[i+1]-xbins[i]);
	    errorLow=errorLow/(xbins[i+1]-xbins[i]);
	    errorHigh=errorHigh/(xbins[i+1]-xbins[i]);
	    cout<<"new val "<<val<<endl;
	}
	
	  rooHistMaps[kName]->SetPoint(i,xbins[i]+(xbins[i+1]-xbins[i])*0.5,val);
	  rooHistMaps[kName]->SetPointError(i,0,0,errorLow,errorHigh);
	  }
      }
    }
  }

  // add the histograms to the stack
  for(int i=0; i<(int)draw.size(); i++){
    stack->Add(find_kname(draw.at(i), histMaps));
  }
  


  
  rooHistMaps["h_ratio_excl_sig"]->SetMarkerStyle(25);
  rooHistMaps["h_ratio_excl_sig"]->SetMarkerColor(kRed);
  rooHistMaps["h_ratio_excl_sig"]->SetLineColor(kRed);


  TH1F *hDraw=new TH1F(Form("hDraw_%s",basename.c_str()),"",100,histMaps[firstSignal]->GetBinLowEdge(1),histMaps[firstSignal]->GetBinLowEdge(histMaps[firstSignal]->GetNbinsX()+1));
  SetDef(hDraw);
  hDraw->SetFillColor(kWhite);
  hDraw->SetLineColor(kWhite);
  hDraw->GetXaxis()->SetTitle(histMaps[firstSignal]->GetXaxis()->GetTitle());
  hDraw->GetXaxis()->SetTitleOffset(9999);
  hDraw->GetYaxis()->SetTitle(histMaps[firstSignal]->GetYaxis()->GetTitle());
  hDraw->GetYaxis()->SetTitleOffset(1.4);
  double minRange=(maxRange < 0.1 ?  0.5e-3:0.5e-2);
  if(maxRange > 0)
    hDraw->GetYaxis()->SetRangeUser(minRange,10);
  if( maxRange > 1)
    hDraw->GetYaxis()->SetRangeUser(minRange,maxRange*100);
  if( maxRange > 10){
    minRange=0.5;
    hDraw->GetYaxis()->SetRangeUser(minRange,maxRange*1e5);
  }

  TH1F *hDrawRatio=new TH1F(Form("hDrawRatio_%s",basename.c_str()),"",100,histMaps[firstSignal]->GetBinLowEdge(1),histMaps[firstSignal]->GetBinLowEdge(histMaps[firstSignal]->GetNbinsX()+1));
  SetDef(hDrawRatio);
  hDrawRatio->SetFillColor(kWhite);
  hDrawRatio->SetLineColor(kWhite);
  if(isDiff)
    hDrawRatio->GetXaxis()->SetTitle(histMaps[firstSignal]->GetXaxis()->GetTitle());
  else 
    hDrawRatio->GetXaxis()->SetTitle(histMaps[firstSignal]->GetXaxis()->GetTitle());
  //hDrawRatio->GetYaxis()->SetTitle("Data / Expectation");
  hDrawRatio->GetYaxis()->SetTitle("Ratio");
  hDrawRatio->GetYaxis()->CenterTitle(true);
  hDrawRatio->GetXaxis()->SetTitleOffset(3.0);
  hDrawRatio->GetYaxis()->SetRangeUser(-0.24,5);
  hDrawRatio->GetYaxis()->SetTitleOffset(1.4);

  /*
  hDrawRatio->GetXaxis()->SetTitleFont(43);
  hDrawRatio->GetYaxis()->SetTitleFont(43);
  hDrawRatio->GetXaxis()->SetLabelFont(43);
  hDrawRatio->GetYaxis()->SetLabelFont(43);
  hDrawRatio->GetXaxis()->SetTitleSize(20);
  hDrawRatio->GetYaxis()->SetTitleSize(20);
  hDrawRatio->GetXaxis()->SetLabelSize(20);
  hDrawRatio->GetYaxis()->SetLabelSize(20);
  hDrawRatio->GetYaxis()->SetRangeUser(0,+2.34);
  hDrawRatio->GetXaxis()->SetTitleOffset(2.5);
  hDrawRatio->GetYaxis()->SetTitleOffset(1.5);
  */

  TLegend *leg=DefLeg(0.75,0.5 ,0.9,0.95);
  leg->AddEntry(rooHistMaps["h_asimovData"],"Asimov","PE");
  // add the legend stuff in the correct order
  std::map<string,bool> addedLegs;
  for(std::map<string, string>::iterator it=legendMap.begin(); it!=legendMap.end(); it++)
    addedLegs[(*it).second]=false;

  //
  for(int i=draw.size()-1; i>-1; i--){
    for(std::map<string,TH1F*>::const_iterator it=histMaps.begin(); it!=histMaps.end(); it++){
      if( !TString( (*it).first.c_str()).Contains( draw.at(i).c_str())) continue ;
      if( colorMap.count( (*it).first) && !addedLegs[ legendMap[(*it).first]] ){
	leg->AddEntry((*it).second,legendMap[(*it).first].c_str(),"F");
	addedLegs[ legendMap[(*it).first]]=true;
      }}}
        
  TLatex *lat=new TLatex();
  lat->SetTextFont(43);

 
  TPad*    stack3 =nullptr;
  TPad*    comparison =nullptr;

   double canX=800;
   double canY=800;
  
   TCanvas *can=new TCanvas(Form("can_%s",basename.c_str()),"",canX,canY);{
     SetCanvasDefaults(can);
     can->cd();
     can->SetBottomMargin(0);

    stack3= new TPad(Form("stack3_%s",basename.c_str()), "", 0, 0.4, 1, 1);   
    comparison= new TPad(Form("comp3_%s",basename.c_str()), "", 0,0, 1, 0.4);

    stack3->SetRightMargin(0.1);
    stack3->SetLeftMargin(0.16);
    stack3->SetTopMargin(0.02);
    stack3->SetBottomMargin(0.001);
   
    comparison->SetRightMargin(0.1);
    comparison->SetTopMargin(0);
    comparison->SetBottomMargin(0.45);
    comparison->SetLeftMargin(0.16);
    
    stack3->SetTicks();
    comparison->SetTicks();
    
    can->cd();
    stack3->Draw();
    comparison->Draw();   

    stack3->cd();
    // top pad 
    stack3->SetLogy();
    hDraw->Draw();
    stack->Draw("hist same");
    curveMaps["h_total_error_band"]->Draw("same");
    rooHistMaps["h_asimovData"]->Draw("PE same");
    leg->Draw();
    //can->RedrawAxis();
    //lat->DrawLatexNDC(0.15,1e5,"#bf{#it{ATLAS}} Internal");
    ATLASLabel(lat,0.2,0.92,false,1,139.1);
    stack3->RedrawAxis();
    // bottom pad;

    for(int i=0;i<rooHistMaps["h_ratio"]->GetN();i++){
      rooHistMaps["h_ratio"];
    }
    comparison->cd();
    hDrawRatio->Draw();
    rooHistMaps["h_ratio"]->Draw("PE same");
    rooHistMaps["h_ratio_excl_sig"]->Draw("PE same");
    curveMaps["h_rel_error_band"]->Draw("E3 same");
    comparison->RedrawAxis();
    
  }

   vector <string> formats={"eps","pdf","png"};
   for(int f=0; f<(int)formats.size(); f++)
     can->Print(Form("%s.%s",can->GetName(),formats[f].c_str()),formats[f].c_str());
   
}

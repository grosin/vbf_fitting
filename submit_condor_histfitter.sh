Executable = /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/run_histfitter.sh
Output = /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/houtCondor
Error = /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/herrorCondor
Log = /atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/hlogCondor
accounting_group = group_atlas.bnl
should_transfer_files = YES 
when_to_transfer_output = ON_EXIT
GetEnv          = True

+MaxRuntime = 1000
Queue 1 Arguments from (
Mll fullSys
jet0_pt fullSys
lep0_pt fullSys
SignedDPhijj fullSys
)


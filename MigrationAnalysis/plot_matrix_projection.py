import numpy as np
import matplotlib.pyplot as plt

def normalize_matrix(matrix,fakes):
    row_sums = matrix.sum(axis=1)
    row_sums=row_sums[:,np.newaxis]
    new_matrix = matrix# / row_sums
    new_fakes=fakes#/row_sums
    return new_matrix,new_fakes

def label_matrix(matrix,a,precision=4):
    for i in range(matrix.shape[1]):
        for j in range(matrix.shape[0]):
            c = round(matrix[j,i],precision)
            a.text(i, j, str(c), va='center', ha='center')

def filter_events(stat_matrices,uncertainty_matrices):
    for k in range(len(stat_matrices)):
        for i in range(stat_matrices[k].shape[0]):
            for j in range(stat_matrices[k].shape[1]):
                if stat_matrices[k][i][j]< 0.005:
                    uncertainty_matrices[k][i][j]=0
                    
def file_to_matrix(filname):
    matrix_list=[]
    names=[]
    with open(filname) as mat_file:
        matrix=[]
        this_name=""
        for line in mat_file:
            print(line)
            if(("Bin" not in line) and len(line) > 2):
                print(line)
                this_name=line.rstrip()
                names.append(this_name)
                if(matrix):
                    matrix_list.append(np.array(matrix))
                    matrix=[]
            elif(line!="\n"):
                line_data=line.split(",")
                print(line_data)
                matrix.append([float(i) for i in line_data[2:]])
            else:
                matrix_list.append(np.array(matrix))
                matrix=[]
    return matrix_list,names

uncert_file="matrix_projection_stat_uncert.txt"
stat_file="matrix_projection_stat.txt"
mc_stat_list,names=file_to_matrix(stat_file)
#matrix_list,names=file_to_matrix(uncert_file)
normalized_matrices=[]

print(mc_stat_list,names)
for name,stat_matrix in zip(names,mc_stat_list):
    f,  (a0, a1) = plt.subplots(1, 2, gridspec_kw={'width_ratios': [5, 1],'hspace':0.2})
    f.suptitle(f"{name}")
    np_stat_matrix,fakes=normalize_matrix(stat_matrix[:,:-1],stat_matrix[:,-1:])
    normalized_matrices.append(np_stat_matrix)
    a0.set_title("Migration Matrix")
    a0.set_xlabel("reco")
    a0.set_ylabel("truth")
    mat=a0.imshow(np_stat_matrix,cmap="jet")
    label_matrix(np_stat_matrix,a0,precision=3)
    a1.set_title("Fakes")
    fakes_axis=a1.imshow(fakes,cmap="jet")
    label_matrix(fakes,a1,precision=3)
    a1.set_xticklabels([])
    f.colorbar(mat, ax=a0)
    f.colorbar(fakes_axis,ax=a1)
    
    f.tight_layout()
    #plt.show()
    plt.savefig(f"{name}_response_matrix.pdf")
    plt.cla()
    plt.close()
'''
#filter_events(normalized_matrices,matrix_list)
for name,uncert_mat in zip(names,matrix_list):
    f,  (a0, a1) = plt.subplots(1, 2, gridspec_kw={'width_ratios': [5, 1],'hspace':0.2})
    f.suptitle(f"{name} MC Stat Uncertainty")
    a0.set_title("Migration Uncert")

    mat=a0.imshow(uncert_mat[:,:-1],cmap="jet")
    label_matrix(uncert_mat[:,:-1],a0,precision=3)
    
    a1.set_title("Fakes Uncert")
    fakes_axis_2=a1.imshow(uncert_mat[:,-1:],cmap="jet")
    label_matrix(uncert_mat[:,-1:],a1,precision=3)
    a1.set_xticklabels([])
    f.colorbar(mat, ax=a0)
    f.colorbar(fakes_axis_2,ax=a1)
    f.tight_layout()
    plt.show()
'''

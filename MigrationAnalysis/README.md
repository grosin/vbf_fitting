Requires linking to RooUnfold
https://gitlab.cern.ch/RooUnfold/RooUnfold



To make the migration matrix file::

Open src/main.cpp
change the binning config file path to the current directory:
std::string config_file="file path here/binning_config.ini"

put the path the reco tree here
std::string reco_file_path="ntuples/reco/2jet.root";

and truth tree here:
std::string truth_file_path="ntuplse/truth/vbf_truth.root";

source ex.sh (puts roounfold in the linked libraries path)
make
./AnalyzeMigrations



To make the observables_c_Factors.txt file::

open c_factors_script.cpp
change the file path of  truth and reco tree
std::string truth_file_path="ntuples/truth/vbf_truth.root";
std::string reco_file_path="ntuples/reco/2jet.root";

and run the script in root c_factor() 
this will create the file c_factor_hists.root with all the histograms
to read the histograms  and create the config file run read_th1.py



plot the migration matrices with 
plot_matrix_projection.py
#include <iostream>
#include <TH1F.h>
#include <TTree.h>
#include <TFile.h>
#include <string>
#include <vector>

void c_factors(){


  std::vector<double> Mll_bin={10000,20000,30000,40000,55000,13000000};
  std::vector<double> Mjj_bin={450000,700000,950000,1200000,1500000,2200000,13000000};
  std::vector<double> DYll_bin={0,0.4,0.6,0.8,1.0,1000};
  std::vector<double> DYjj_bin={2.1,4.0,4.375,5,5.5,6.25,9.0};
  std::vector<double> DPhijj_bin={-3.1416,-1.57,0,1.57,3.1416};
  std::vector<double> DPhill_bin={0,0.2,0.4,0.6,0.8,1.4};
  std::vector<double> lead_lep_bin={22000,40000,50000,60000,70000,100000,500000};
  std::vector<double> lead_jet_bin={30000,90000,120000,160000,220000,700000};
  std::vector<double> sub_lep_bin={15000,22000,30000,40000,50000,200000};
  std::vector<double> sub_jet_bin={30000,45000,60000,90000,120000,13000000};
  std::vector<double> cosstar_bin={0,0.0625,0.125,0.1875,0.3125,1.0};
  std::vector<double> higgs_pt_bin={0,80000,120000,160000,260000,850000};
  std::vector<double> ptll_bin={0,60000,80000,100000,140000,1000000};
  
  std::string truth_file_path="ntupls/truth/vbf_PowPy8_truth.root";

  std::string reco_file_path="ntupls/reco/2jet.root";

  TFile *reco_file = new TFile(reco_file_path.c_str());

  TFile *truth_file= new TFile(truth_file_path.c_str());;


  TTree*truth_tree=(TTree*)truth_file->Get("Vbf_Total");

  TTree* reco_tree=(TTree*)reco_file->Get("vbf_nominal");


  TH1F *DYjj_reco_hist=new TH1F("DYjj_reco_hist","DYjj_reco_hist",DYjj_bin.size()-1,&DYjj_bin[0]);
  TH1F *DYll_reco_hist=new TH1F("DYll_reco_hist","DYll_reco_hist",DYll_bin.size()-1,&DYll_bin[0]);
  TH1F *Mjj_reco_hist=new TH1F("Mjj_reco_hist","Mjj_reco_hist",Mjj_bin.size()-1,&Mjj_bin[0]);
  TH1F *Mll_reco_hist=new TH1F("Mll_reco_hist","Mll_reco_hist",Mll_bin.size()-1,&Mll_bin[0]);
  TH1F *DPhill_reco_hist=new TH1F("DPhill_reco_hist","DPhill_reco_hist",DPhill_bin.size()-1,&DPhill_bin[0]);
  TH1F *DPhijj_reco_hist=new TH1F("DPhijj_reco_hist","DPhijj_reco_hist",DPhijj_bin.size()-1,&DPhijj_bin[0]);
  TH1F *Leading_lep_reco_hist=new TH1F("Leading_lep_reco","Leading_lep_reco_hist",lead_lep_bin.size()-1,&lead_lep_bin[0]);
  TH1F *Leading_jet_reco_hist=new TH1F("Leading_jet_reco","Leading_jet_reco_hist",lead_jet_bin.size()-1,&lead_jet_bin[0]);
  TH1F *sub_lep_reco_hist=new TH1F("sub_lep_reco","Leading_lep_reco_hist",sub_lep_bin.size()-1,&sub_lep_bin[0]);
  TH1F *sub_jet_reco_hist=new TH1F("sub_jet_reco","Leading_jet_reco_hist",sub_jet_bin.size()-1,&sub_jet_bin[0]);
  TH1F *higgs_pt_reco_hist=new TH1F("higgs_pt_reco_hist","Mjj_reco_hist",higgs_pt_bin.size()-1,&higgs_pt_bin[0]);
  TH1F *ptll_reco_hist=new TH1F("ptll_reco_hist","ptll_reco_hist",ptll_bin.size()-1,&ptll_bin[0]);
  TH1F *cosstar_reco_hist=new TH1F("costhetastar_reco_hist","cosstar_reco_hist",cosstar_bin.size()-1,&cosstar_bin[0]);

  TH1F *inclusive_reco_hist=new TH1F("inclusive_reco_hist","inclusive_reco_hist",1,0,1);
  
  TH1F *DYjj_truth_hist=new TH1F("DYjj_truth_hist","DYjj_truth_hist",DYjj_bin.size()-1,&DYjj_bin[0]);
  TH1F *DYll_truth_hist=new TH1F("DYll_truth_hist","DYll_truth_hist",DYll_bin.size()-1,&DYll_bin[0]);
  TH1F *Mjj_truth_hist=new TH1F("Mjj_truth_hist","Mjj_truth_hist",Mjj_bin.size()-1,&Mjj_bin[0]);
  TH1F *Mll_truth_hist=new TH1F("Mll_truth_hist","Mll_truth_hist",Mll_bin.size()-1,&Mll_bin[0]);
  TH1F *DPhill_truth_hist=new TH1F("DPhill_truth_hist","DPhill_truth_hist",DPhill_bin.size()-1,&DPhill_bin[0]);
  TH1F *DPhijj_truth_hist=new TH1F("DPhijj_truth_hist","DPhijj_truth_hist",DPhijj_bin.size()-1,&DPhijj_bin[0]);
  TH1F *Leading_lep_truth_hist=new TH1F("Leading_lep_truth","Leading_lep_truth_hist",lead_lep_bin.size()-1,&lead_lep_bin[0]);
  TH1F *Leading_jet_truth_hist=new TH1F("Leading_jet_truth","Leading_jet_truth_hist",lead_jet_bin.size()-1,&lead_jet_bin[0]);
  TH1F *sub_lep_truth_hist=new TH1F("sub_lep_truth","Leading_lep_truth_hist",sub_lep_bin.size()-1,&sub_lep_bin[0]);
  TH1F *sub_jet_truth_hist=new TH1F("sub_jet_truth","Leading_jet_truth_hist",sub_jet_bin.size()-1,&sub_jet_bin[0]);
  TH1F *higgs_pt_truth_hist=new TH1F("higgs_pt_truth_hist","Mjj_truth_hist",higgs_pt_bin.size()-1,&higgs_pt_bin[0]);
  TH1F *cosstar_truth_hist=new TH1F("costhetastar_truth_hist","cosstar_truth_hist",cosstar_bin.size()-1,&cosstar_bin[0]);
  TH1F *ptll_truth_hist=new TH1F("ptll_truth_hist","ptll_truth_hist",ptll_bin.size()-1,&ptll_bin[0]);

  TH1F *inclusive_truth_hist=new TH1F("inclusive_truth_hist","inclusive_truth_hist",1,0,1);
  float DYjj_truth;
  float DYll_truth;
  float Mjj_truth;
  float Mll_truth;
  float DPhill_truth;
  float DPhijj_truth;
  float lead_lep_truth;
  float lead_jet_truth;
  float higgs_pt_truth;
  float sub_lep_truth;
  float sub_jet_truth;
  float cosstar_truth;  
  double truth_weight;
  float ptll_truth;
  int bjet;

  float DYjj_reco;
  float DYll_reco;
  float Mjj_reco;
  float Mll_reco;
  float DPhill_reco;
  float DPhijj_reco;
  float lead_lep_reco;
  float lead_jet_reco;
  float higgs_pt_reco;
  float sub_lep_reco;
  float sub_jet_reco;
  float cosstar_reco;
  float ptll_reco;
  double reco_weight;
  float inSR;
  truth_tree->SetBranchAddress("DYjj_truth",&DYjj_truth);
  truth_tree->SetBranchAddress("DYll_truth",&DYll_truth);
  truth_tree->SetBranchAddress("Mjj_truth",&Mjj_truth);
  truth_tree->SetBranchAddress("Mll_truth",&Mll_truth);
  truth_tree->SetBranchAddress("SignedDPhijj_truth",&DPhijj_truth);
  truth_tree->SetBranchAddress("DPhill_truth",&DPhill_truth);
  truth_tree->SetBranchAddress("DYjj_truth",&DYjj_truth);
  truth_tree->SetBranchAddress("lep0_pT",&lead_lep_truth);
  truth_tree->SetBranchAddress("jet0_pT",&lead_jet_truth);
  truth_tree->SetBranchAddress("lep1_pT",&sub_lep_truth);
  truth_tree->SetBranchAddress("jet1_pT",&sub_jet_truth);
  truth_tree->SetBranchAddress("HIGGS_pT",&higgs_pt_truth);
  truth_tree->SetBranchAddress("Ptll_truth",&ptll_truth);

  truth_tree->SetBranchAddress("cosThetaStar_truth",&cosstar_truth);
  truth_tree->SetBranchAddress("weight",&truth_weight);
  truth_tree->SetBranchAddress("nBjet",&bjet);

  reco_tree->SetBranchAddress("DYjj",&DYjj_reco);
  reco_tree->SetBranchAddress("DYll",&DYll_reco);
  reco_tree->SetBranchAddress("Mjj",&Mjj_reco);
  reco_tree->SetBranchAddress("Mll",&Mll_reco);
  reco_tree->SetBranchAddress("SignedDPhijj",&DPhijj_reco);
  reco_tree->SetBranchAddress("DPhill",&DPhill_reco);
  reco_tree->SetBranchAddress("lep0_pt",&lead_lep_reco);
  reco_tree->SetBranchAddress("jet0_pt",&lead_jet_reco);
  reco_tree->SetBranchAddress("lep1_pt",&sub_lep_reco);
  reco_tree->SetBranchAddress("jet1_pt",&sub_jet_reco);
  reco_tree->SetBranchAddress("pt_H",&higgs_pt_reco);
  reco_tree->SetBranchAddress("costhetastar",&cosstar_reco);
  reco_tree->SetBranchAddress("Ptll",&ptll_reco);

  reco_tree->SetBranchAddress("inSR",&inSR);
  reco_tree->SetBranchAddress("weight",&reco_weight);
  int nRecoEvents=reco_tree->GetEntries();
  int nTruthEvents=truth_tree->GetEntries();

  for(int i =0 ; i< nTruthEvents;i++){
    truth_tree->GetEntry(i);
    if(bjet >=1) continue;
    if(Mjj_truth < 450000) continue;
    if(DPhill_truth >1.4) continue;
    DYjj_truth_hist->Fill(DYjj_truth,truth_weight);
    DYll_truth_hist->Fill(DYll_truth,truth_weight);
    Mjj_truth_hist->Fill(Mjj_truth,truth_weight);
    Mll_truth_hist->Fill(Mll_truth,truth_weight);
    DPhill_truth_hist->Fill(DPhill_truth,truth_weight);
    DPhijj_truth_hist->Fill(DPhijj_truth,truth_weight);
    Leading_lep_truth_hist->Fill(lead_lep_truth,truth_weight);
    Leading_jet_truth_hist->Fill(lead_jet_truth,truth_weight);
    sub_lep_truth_hist->Fill(sub_lep_truth,truth_weight);
    sub_jet_truth_hist->Fill(sub_jet_truth,truth_weight);
    cosstar_truth_hist->Fill(cosstar_truth,truth_weight);
    higgs_pt_truth_hist->Fill(higgs_pt_truth,truth_weight);
    ptll_truth_hist->Fill(ptll_truth,truth_weight);
    inclusive_truth_hist->Fill(0.5,truth_weight);
  }

  for(int i =0 ; i< nRecoEvents;i++){
    reco_tree->GetEntry(i);
    if((int)inSR !=1) continue;
    if(Mjj_reco < 450000) continue;
    if(DPhill_reco > 1.4) continue;
    DYjj_reco_hist->Fill(DYjj_reco,reco_weight);
    DYll_reco_hist->Fill(DYll_reco,reco_weight);
    Mjj_reco_hist->Fill(Mjj_reco,reco_weight);
    Mll_reco_hist->Fill(Mll_reco,reco_weight);
    DPhill_reco_hist->Fill(DPhill_reco,reco_weight);
    DPhijj_reco_hist->Fill(DPhijj_reco,reco_weight);
    Leading_lep_reco_hist->Fill(lead_lep_reco,reco_weight);
    Leading_jet_reco_hist->Fill(lead_jet_reco,reco_weight);
    sub_lep_reco_hist->Fill(sub_lep_reco,reco_weight);
    sub_jet_reco_hist->Fill(sub_jet_reco,reco_weight);
    cosstar_reco_hist->Fill(cosstar_reco,reco_weight);
    higgs_pt_reco_hist->Fill(higgs_pt_reco,reco_weight);
    ptll_reco_hist->Fill(ptll_reco,reco_weight);
    inclusive_reco_hist->Fill(0.5,reco_weight);
  }
  /**************************/

  TH1F* DYjj_ratio_hist=(TH1F*)DYjj_reco_hist->Clone("DYjj_Ratio");
  TH1F* DYll_ratio_hist=(TH1F*)DYll_reco_hist->Clone("DYll_Ratio");
  TH1F* Mjj_ratio_hist=(TH1F*)Mjj_reco_hist->Clone("Mjj_Ratio");
  TH1F* Mll_ratio_hist=(TH1F*)Mll_reco_hist->Clone("Mll_Ratio");
  TH1F* DPhill_ratio_hist=(TH1F*)DPhill_reco_hist->Clone("DPhill_Ratio");
  TH1F* DPhijj_ratio_hist=(TH1F*)DPhijj_reco_hist->Clone("DPhijj_Ratio");
  TH1F* Leading_lep_ratio_hist=(TH1F*)Leading_lep_reco_hist->Clone("Leading_lep_Ratio");
  TH1F* Leading_jet_ratio_hist=(TH1F*)Leading_jet_reco_hist->Clone("Leading_jet_Ratio");
  TH1F* sub_lep_ratio_hist=(TH1F*)sub_lep_reco_hist->Clone("sub_lep_Ratio");
  TH1F* sub_jet_ratio_hist=(TH1F*)sub_jet_reco_hist->Clone("sub_jet_Ratio");
  TH1F* higgs_pt_ratio_hist=(TH1F*)higgs_pt_reco_hist->Clone("higgs_pt_Ratio");
  TH1F* cosstar_ratio_hist=(TH1F*)cosstar_reco_hist->Clone("cosstar_Ratio");
  TH1F* ptll_ratio_hist=(TH1F*)ptll_reco_hist->Clone("ptll_Ratio");
  TH1F* inclusive_ratio_hist=(TH1F*)inclusive_reco_hist->Clone("inclusive_Ratio");

  DYjj_ratio_hist->Divide(DYjj_truth_hist);
  DYll_ratio_hist->Divide(DYll_truth_hist);
  Mjj_ratio_hist->Divide(Mjj_truth_hist);
  Mll_ratio_hist->Divide(Mll_truth_hist);
  DPhill_ratio_hist->Divide(DPhill_truth_hist);
  DPhijj_ratio_hist->Divide(DPhijj_truth_hist);
  Leading_lep_ratio_hist->Divide(Leading_lep_truth_hist);
  Leading_jet_ratio_hist->Divide(Leading_jet_truth_hist);
  sub_lep_ratio_hist->Divide(sub_lep_truth_hist);
  sub_jet_ratio_hist->Divide(sub_jet_truth_hist);
  higgs_pt_ratio_hist->Divide(higgs_pt_truth_hist);
  cosstar_ratio_hist->Divide(cosstar_truth_hist);
  ptll_ratio_hist->Divide(ptll_truth_hist);
  inclusive_ratio_hist->Divide(inclusive_truth_hist);
  TFile * c_hist_file=TFile::Open("c_factor_hists.root","RECREATE");
  c_hist_file->cd();

  
  DYjj_reco_hist->Write();
  DYll_reco_hist->Write();
  Mjj_reco_hist->Write();
  Mll_reco_hist->Write();
  DPhill_reco_hist->Write();
  DPhijj_reco_hist->Write();
  Leading_lep_reco_hist->Write();
  Leading_jet_reco_hist->Write();
  sub_lep_reco_hist->Write();
  sub_jet_reco_hist->Write();
  higgs_pt_reco_hist->Write();
  cosstar_reco_hist->Write();
  ptll_reco_hist->Write();
  inclusive_reco_hist->Write();
  
  DYjj_truth_hist->Write();
  DYll_truth_hist->Write();
  Mjj_truth_hist->Write();
  Mll_truth_hist->Write();
  DPhill_truth_hist->Write();
  DPhijj_truth_hist->Write();
  Leading_lep_truth_hist->Write();
  Leading_jet_truth_hist->Write();
  sub_lep_truth_hist->Write();
  sub_jet_truth_hist->Write();
  higgs_pt_truth_hist->Write();
  cosstar_truth_hist->Write();
  ptll_truth_hist->Write();
  inclusive_truth_hist->Write();
  
  DYjj_ratio_hist->Write();
  DYll_ratio_hist->Write();
  Mjj_ratio_hist->Write();
  Mll_ratio_hist->Write();
  DPhill_ratio_hist->Write();
  DPhijj_ratio_hist->Write();
  Leading_lep_ratio_hist->Write();
  Leading_jet_ratio_hist->Write();
  sub_lep_ratio_hist->Write();
  sub_jet_ratio_hist->Write();
  higgs_pt_ratio_hist->Write();
  cosstar_ratio_hist->Write();
  ptll_ratio_hist->Write();
  inclusive_ratio_hist->Write();

  c_hist_file->Close();
}

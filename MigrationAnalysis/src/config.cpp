#include  <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include "unfold.h"

bool isNewSection(std::string line){
  if(line.empty())
    return false;
  return line.at(0)=='[';
}

bool isComment(std::string line){
  if(line.empty())
    return false;
  return line.at(0)=='#';
}
std::vector<std::string> split(std::string full_string,std::string delim){
  std::vector<std::string> splits;
  auto start = 0U;
  auto end = full_string.find(delim);
  splits.push_back(full_string.substr(start, end - start));
  while (end != std::string::npos)
    {
      start = end + delim.length();
      end = full_string.find(delim, start);
      splits.push_back(full_string.substr(start, end - start));

    }
  
  return splits;
}

void read_binning(std::string binning_file){
  std::cout<<binning_file<<"\n";
  std::ifstream config_file(binning_file.c_str(),std::ios::in);
  std::string line;
  std::string section="";
  if(!config_file.is_open()){
    std::cout<<"can't open file\n";
    return;
  }
  while ( std::getline (config_file,line) ){
    if(line.empty() or isComment(line))
      continue;
    if(isNewSection(line)){

      section=line.substr(1,line.length()-2);
      std::cout<<"reading section "<<section<<"\n";
    }else{
      std::vector<std::string> line_split=split(line,":");
      if(line_split.at(0).compare("binning")==0){
        std::vector<std::string> bins=split(line_split.at(1),",");
        std::vector<double> binning;
        for(auto edge:bins){
          binning.push_back(std::stod(edge));
        }
        double  *array= new double[binning.size()];
         for(int i=0;i<binning.size();i++){
          *(array+i)=binning[i];
         }
        distribution_maps[section]={array,(int)binning.size()-1};
      }else{
        std::cout<<"encountered unexpected line"<<"\n";
        std::cout<<line<<"\n";
      }
    }
  }
  config_file.close();
}

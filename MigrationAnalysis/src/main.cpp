#include <TTree.h>
#include <vector>
#include <string>
#include <TH1F.h>
#include <TFile.h>
#include <iostream>
#include <TPad.h>
#include <TGraph.h>
#include <TGraphAsymmErrors.h>
#include <fstream>
#include <TObject.h>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>
#include <map>
#include <TNtuple.h>
#include <string>
#include <THStack.h>
#include <TStyle.h>
#include <RooUnfoldResponse.h>
#include "merge.h"
#include "unfold.h"
#include <bits/stdc++.h> 
#include <tuple>
#include "bin_analysis.h"
#include <TPaveLabel.h>
#include <algorithm>
#include "config.h"
#include <TProfile.h>
#include <TMatrixDSym.h>
#include <TLatex.h>
#include <TVectorD.h>
using namespace std;
std::map<std::string,distribution_container> distribution_maps;


string get_truth_name(string reco_name){
  std::unordered_map<std::string,std::string> truth_names;
  truth_names["Mjj"]="Mjj_truth";
  truth_names["Mll"]="Mll_truth";
  truth_names["DYjj"]="DYjj_truth";
  truth_names["DYll"]="DYll_truth";
  truth_names["pt_H"]="HIGGS_pT";
  truth_names["DPhill"]="DPhill_truth";
  truth_names["lep0_pt"]="lep0_pT";
  truth_names["lep1_pt"]="lep1_pT";
  truth_names["jet0_pt"]="jet0_pT";
  truth_names["jet1_pt"]="jet1_pT";
  truth_names["SignedDPhijj"]="SignedDPhijj_truth";
  truth_names["costhetastar"]="cosThetaStar_truth";
  truth_names["Ptll"]="Ptll_truth";
  truth_names["DPhijj"]="SignedDPhijj_truth";
  return truth_names[reco_name];
}

string to_lower(string st){
  transform(st.begin(), st.end(), st.begin(), ::tolower);
  return st;
}

void draw_purity_effeciency(string dist,RooUnfoldResponse UnfoldTuple){
  auto response_matrix=UnfoldTuple;
  TH1F* truth_hist=(TH1F*)UnfoldTuple.Htruth()->Clone("my_truth_hist");
  TH1F*reco_hist=(TH1F*)UnfoldTuple.Hmeasured()->Clone("my_reco_hist");

  TH1F* purity_hist=purity(response_matrix,reco_hist);
  TH1F *effeciency_hist=effeciency_plots(response_matrix,truth_hist);
  
  TCanvas * c2 = new TCanvas("c2","",700,900);

  std::map<std::string,std::string> unfolding_features={
                                                        {"pt_H","Higgs Pt"},
                                                        {"MTll","MTll"},
                                                        {"ptTot","Total Pt"},
                                                        {"Ptll","Ptll"},
                                                        {"lep0_pt","Leading Lep Pt"},
                                                        {"lep1_pt","Subleading Lep Pt"},
                                                        {"DYll","DYll"},
                                                        {"DPhill","DPhill"},
                                                        {"costhetastar","Cos(#theta*)"},
                                                        {"Mjj","Mjj"},
                                                        {"jet0_pt","Leading Jet Pt"},
                                                        {"jet1_pt","Subleading Jet Pt"},
                                                        {"DYjj","DYjj"},
                                                        {"Mll","Mll"},
                                                         {"SignedDPhijj","Signed DPhijj"},
                                                        {"DPhijj","DPhijj"}
  };
  std::map<std::string,std::string> VarUnitMap{
                                                        {"pt_H","[MeV]"},
                                                        {"lep0_pt","[MeV]"},
                                                        {"lep1_pt","[MeV]"},
                                                        {"DYll"," "},
                                                        {"DPhill","[Rad]"},
                                                        {"costhetastar"," "},
                                                        {"Mjj","[MeV]"},
                                                        {"jet0_pt","[MeV]"},
                                                        {"jet1_pt","[MeV]"},
                                                        {"DYjj"," "},
                                                        {"Mll","[MeV]"},
                                                        {"SignedDPhijj","[Rad]"},
                                                        {"DPhijj","[Rad]"}                                               

  };
  
  truth_hist->SetMarkerStyle(1);
  reco_hist->SetMarkerStyle(1);
  truth_hist->SetLineColor(kRed);
  reco_hist->SetLineColor(kBlue);
  //c2->Divide(1,2,0,0);
  c2->cd();
  gStyle->SetOptTitle(0);


  TLatex *lat=new TLatex();

  purity_hist->SetMarkerStyle(1);
  effeciency_hist->SetMarkerStyle(1);
  purity_hist->SetMinimum(0.5);    
  purity_hist->SetLineColor(kBlue);
  effeciency_hist->SetLineColor(kRed);
  purity_hist->SetTitle("Purity");
  effeciency_hist->SetTitle("Effeciency");
  effeciency_hist->GetXaxis()->SetTitle(Form("%s %s",unfolding_features[dist].c_str(),VarUnitMap[dist].c_str()));
  effeciency_hist->GetYaxis()->SetTitle("Effeciency and Purity Ratio");
  effeciency_hist->GetYaxis()->SetTitleOffset(1.5);
  //c2->cd(2);

  effeciency_hist->SetMinimum(0.5);
  effeciency_hist->Draw("E1");
  
  //gPad->SetLogy(1);
  purity_hist->Draw("E1 SAME");
  gStyle->SetOptStat(0);

  gPad->BuildLegend(0.75,0.85,0.95,0.95,"");
  TPaveLabel *title = new TPaveLabel(.35,.90,.55,.95,unfolding_features[dist].c_str(),unfolding_features[dist].c_str());
  double x_coordinate=(purity_hist->GetBinLowEdge(purity_hist->GetNbinsX()))/1.2;

  lat->DrawLatex(-1,0.65,"#it{#bf{ATLAS}} Internal");
  lat->DrawLatex(-1,0.65*0.96,Form("#sqrt{s} = 13 TeV, %.1f fb^{-1}",139.1));

  c2->Print((dist+"_eff_purity.pdf").c_str());

}



int c_factor(TH1F * truth, TH1F *reco){
  std::cout<<"reco Integral: "<<reco->Integral()<<"\n";
  std::cout<<"truthIntegral: "<<truth->Integral()<<"\n";
  return reco->Integral()/truth->Integral();
}

void create_data(TTree * truth_tree,TTree *reco_tree,std::string reco_feature,std::string truth_feature){
    std::cout<<"creating response\n";
    std::cout<<reco_tree<<"\n";
    std::ofstream projectFile ;
    projectFile.open("matrix_projection_stat.txt",std::ofstream::app);
    RooUnfoldResponse response=create_response(truth_tree,reco_tree,truth_feature.c_str(),reco_feature.c_str());
    std::cout<<"unfolding response\n";
    false_spectrum_comparison(response,reco_feature);
    draw_purity_effeciency(reco_feature,response);
    auto matrix=response.Mresponse();
    auto hist=response.Hresponse();
    auto truth=response.Htruth();
    auto reco=response.Hmeasured();
    auto fakes=response.Vfakes();
    int nbins=reco->GetNbinsX();
    double total_fake_events=0;
    double total_true_events=0;
    double total_reco_events=0;
    for(int i=0;i<nbins+1;i++){
      total_true_events+=truth->GetBinContent(i);
      total_reco_events+=reco->GetBinContent(i);
    }
    projectFile<<reco_feature<<"\n";;


    for(int i=0;i<nbins;i++){
      projectFile<<reco_feature<<",Bin"<<i<<",";
      double projection;//=matrix[i][i]*truth->GetBinContent(i+1);
      for(int j=0;j<nbins;j++){
        projection+=matrix[i][j]*truth->GetBinContent(j+1);
        projectFile<<matrix[i][j]*truth->GetBinContent(j+1)<<",";
      }
      projectFile<<fakes[i];
      total_fake_events+=fakes[i];
      projection=projection*reco->GetBinContent(i+1);
      projectFile<<"\n";
    }
    
    projectFile<<"\n";

      
    
    projectFile.close();
    std::cout<<"Total fakes\n";
    std::cout<<total_fake_events/reco->Integral()<<"\n";
    std::cout<<"total missies\n";
    std::cout<<(truth->Integral()-(reco->Integral()-total_fake_events))/truth->Integral()<<"\n";
}

int main(){
  std::string config_file="/afs/cern.ch/user/g/grosin/HWWAnalysis/diff/MigrationAnalysis/binning_config.ini";
  read_binning(config_file);

  std::vector<std::string>  unfolding_features={"Mjj","Mll","DPhill"};//,"costhetastar","DYll","DYjj","SignedDPhijj","lep0_pt","lep1_pt","jet0_pt","jet1_pt","Ptll","pt_H"};

  
  std::string truth_file_path="";
  std::string reco_file_path="";
  TFile * reco_file=TFile::Open(reco_file_path.c_str());
  TFile * truth_file=TFile::Open(truth_file_path.c_str());

  TTree* reco_tree=(TTree*)reco_file->Get("vbf_nominal");
  TTree* truth_tree=(TTree*)truth_file->Get("Vbf_Total");


  for(auto& feature : unfolding_features){
  create_data( truth_tree,reco_tree,feature,get_truth_name(feature));  
  }


  
  return 1;
}

#ifndef MERGE_H
#define MERGE_H
#include <vector>
#include "TH1F.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TGraph.h"
#include <iostream>
#include <TTree.h>
#include <string>
#include <unordered_map>

std::unordered_map<int,int> FillRunNumber(TTree * recoTree,const char * event_name="eventNumber",bool truth=false);
bool IsInMap(std::unordered_map<int,int> &mymap, int input);
bool IsInMap(std::unordered_map<std::string,std::string> &mymap, std::string input);

#endif /* MERGE_H */

#include <iostream>
#include <TH1F.h>
#include <string>
#include <vector>
#include <TTree.h>
#include <map>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>
#include <TProfile.h>
#include "unfold.h"
#include <fstream>
#include "RooUnfoldResponse.h"
#include <TMath.h>
#include "merge.h"
#include <TLatex.h>
#include <TStyle.h>
using namespace std;
TH1F*  purity(RooUnfoldResponse &response,TH1F * reco){
  TH2 *migration_matrix= response.Hresponse();
  TH1F * purity_hist=(TH1F*)reco->Clone();
  purity_hist->SetName("Reco_Purity");
  TH1D * purity_nom=migration_matrix->ProjectionX("",1,purity_hist->GetNbinsX());
  for(int i=0 ;i <purity_hist->GetNbinsX();i++){
    std::cout<<purity_nom->GetBinContent(i)<<"\n";
    for(int j=0 ;j <purity_hist->GetNbinsX();j++){
      std::cout<<migration_matrix->GetBinContent(i,j)<<",";
    }
    std::cout<<"\n";
  }
  purity_nom->Sumw2();
  reco->Sumw2();
  std::cout<<"profile\n";
  double ratio;
  double mm;
  double reco_content;
  for(int i=0 ;i <=purity_hist->GetNbinsX();i++){
    mm=migration_matrix->GetBinContent(i,i);
    reco_content=purity_nom->GetBinContent(i);
    if(reco_content!=0)
      ratio=mm/reco_content;
    else
      ratio=0;
    purity_hist->SetBinContent(i,ratio);
    double binomError=TMath::Abs( ( (1. - 2.* mm / reco_content) * mm  + mm*mm * reco_content / (reco_content*reco_content) ) / (reco_content*reco_content) );
    //if(binomError !=0)
    purity_hist->SetBinError(i,0);

  }
  std::cout<<"\n";
  return purity_hist;
}

TH1F* effeciency_plots(RooUnfoldResponse &response, TH1F* truth){
  TH2 *migration_matrix= response.Hresponse();
  TH1F * eff=(TH1F*)truth->Clone();
  eff->SetName("Truth_Effeciency");
  TH1D * eff_nom=migration_matrix->ProjectionY("",1,eff->GetNbinsX());
  eff_nom->Sumw2();
  truth->Sumw2();
  double mm;
  double truth_content;
  double ratio;
  for(int i=0 ;i <=eff->GetNbinsX();i++){
    truth_content=eff_nom->GetBinContent(i);
    mm=migration_matrix->GetBinContent(i,i);
    if(truth_content==0){
      ratio=0;
    }else{
    ratio=mm/truth_content;
    }
    eff->SetBinContent(i,ratio);
    //calculate binomial errors:
    double binomError=TMath::Abs( ( (1. - 2.* mm / truth_content) * mm  + mm*mm * truth_content / (truth_content*truth_content) ) / (truth_content*truth_content) );
    //if(binomError!=0)
    eff->SetBinError(i,0);
  }
  std::cout<<"\n";
  eff->SetStats(0);
  return eff;
}

TH1F * misses_plot(RooUnfoldResponse &response, TH1F* truth, std::string name){
  TH1F *misses=(TH1F*)truth->Clone();
  TH1D *matched_truth=response.Hresponse()->ProjectionY();
  misses->Add(matched_truth,-1);
  truth->SetName((name+"truth").c_str());
  matched_truth->SetName((name+"_projection").c_str());
  misses->SetName((name+"_misses").c_str());
  
  truth->SetLineColor(kRed);
  matched_truth->SetLineColor(kGreen);
  misses->SetLineColor(kBlue);
  //std::cout<<"Total misses %"<<misses->Integral() / truth->Integral()<<"\n";
  //misses->Divide(truth);

  // TCanvas * cav=new TCanvas();
  // cav->cd();
  // truth->Draw();
  // matched_truth->Draw("SAME");
  // misses->Draw("SAME");
  // gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
  // cav->Print((name+"Misses_vs_Truth.pdf").c_str());
  return misses;

}

TH1F * fakes_plot(RooUnfoldResponse &response, TH1F* reco){
  TH1F *fakes=(TH1F*)reco->Clone();
  TH1D *matched_reco=response.Hresponse()->ProjectionX();
  fakes->Add(matched_reco,-1);
  //std::cout<<"Total fakes %"<<fakes->Integral() / reco->Integral()<<"\n";
  //fakes->Divide(reco);
  return fakes;

}

void false_spectrum_comparison(RooUnfoldResponse &response, std::string name){
  TH1D *fakes =(TH1D*)response.Hfakes();
  TH1D *truth=(TH1D*)response.Htruth();
  TH1D *reco=(TH1D*)response.Hmeasured();
  TH2D * response_matrix=response.HresponseNoOverflow();
  TH1D * matchedReco=response_matrix->ProjectionX();
  TH1D * matchedTruth=response_matrix->ProjectionY();
  TH1D * misses=(TH1D*) truth->Clone();
  reco->SetMinimum(0);
  truth->SetMinimum(0);

  misses->SetName("Misses");
  misses->SetTitle("Misses");

  matchedReco->SetName("MatchedReco");
  matchedReco->SetTitle("Matched Reco");
  fakes->SetTitle("Fakes");
  misses->SetTitle("Misses");
  reco->SetTitle((name+" reco").c_str());
  truth->SetTitle((name+" truth").c_str());

  matchedTruth->SetName("MatchedTruth");
  matchedTruth->SetTitle("Matched Truth");
  
  misses->Add(matchedTruth,-1);
  fakes->SetLineColor(kRed);
  misses->SetLineColor(kRed);

  reco->SetLineColor(kBlue);
  truth->SetLineColor(kBlue);

  matchedReco->SetLineColor(kBlack);
  matchedTruth->SetLineColor(kBlack);
  TH1D* fakes_ratio=(TH1D*)fakes->Clone("fakes_ratio");
  TH1D* matched_ratio=(TH1D*)matchedReco->Clone("matched_ratio");
  fakes_ratio->Divide(reco);
  matched_ratio->Divide(reco);

  TH1D* misses_ratio=(TH1D*)misses->Clone("misses_ratio");
  misses_ratio->Divide(truth);
  TCanvas * cav=new TCanvas();
  cav->cd();
  TPad* pad1_reco = new TPad("pad1_r", "pad1_r",0.,.3,1.,1);
  pad1_reco->Draw();
  pad1_reco->cd();
  pad1_reco->SetBottomMargin(0);
  reco->Draw();
  matchedReco->Draw("SAME");
  fakes->Draw("SAME");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");

  cav->cd();
  TPad* pad2_reco = new TPad("pad2_r", "pad2_r",0,0.05,1,0.3);
  pad2_reco->SetTopMargin(0.0);
  pad2_reco->SetBottomMargin(0.25);
  pad2_reco->SetGridx(); // vertical grid
  pad2_reco->Draw();
  pad2_reco->cd();
  fakes_ratio->SetTitle(" Fakes Ratio To Total");
  fakes_ratio->GetYaxis()->SetRangeUser(0,.4);
  fakes_ratio->Draw();
  //draw_ratio(reco,fakes,name.c_str());

  cav->Print((name+"fakes_comparison.pdf").c_str());
  
  TCanvas * cav1=new TCanvas();
  cav1->cd();
  TPad* pad1_truth = new TPad("pad1_t", "pad1_t",0.,.3,1.,1);
  pad1_truth->Draw();
  pad1_truth->cd();
  pad1_truth->SetBottomMargin(0);
  gStyle->SetOptStat(0);
  truth->Draw();
  matchedTruth->Draw("SAME");
  misses->Draw("SAME");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");

  cav1->cd();
  TPad* pad2_truth = new TPad("pad2_t", "pad2_t",0,0.05,1,0.3);
  pad2_truth->SetTopMargin(0.0);
  pad2_truth->SetBottomMargin(0.25);
  pad2_truth->SetGridx(); // vertical grid
  pad2_truth->Draw();
  pad2_truth->cd();
  misses_ratio->SetTitle(" Misses Ratio To Total");
  misses_ratio->GetYaxis()->SetRangeUser(0.4,.8);
  misses_ratio->Draw();
  cav1->Print((name+"misses_comparison.pdf").c_str());
  std::cout<<"fakes ratio:"<<fakes->Integral()/reco->Integral()<<"\n";

}


void draw_false_spectrum(TH1 *total_reco, TH1* matched_reco, TH1 *fakes){

  total_reco->SetMinimum(0);


  //matched_reco->SetName("MatchedReco");
  //matched_reco->SetTitle("Matched Reco");

  //fakes->SetTitle("Fakes");
  string name=string(total_reco->GetName());
  fakes->SetLineColor(kRed);
  
    
  total_reco->SetLineColor(kBlue);


  matched_reco->SetLineColor(kBlack);

  TH1F* fakes_ratio=(TH1F*)fakes->Clone("ratio");

  fakes_ratio->Divide(total_reco);

  
  TCanvas * cav=new TCanvas();
  cav->cd();
  TPad* pad1_reco = new TPad("pad1_r", "pad1_r",0.,.3,1.,1);
  pad1_reco->Draw();
  pad1_reco->cd();
  pad1_reco->SetBottomMargin(0);
  total_reco->Draw();
  matched_reco->Draw("SAME");
  fakes->Draw("SAME");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");

  cav->cd();
  TPad* pad2_reco = new TPad("pad2_r", "pad2_r",0,0.05,1,0.3);
  pad2_reco->SetTopMargin(0.0);
  pad2_reco->SetBottomMargin(0.25);
  pad2_reco->SetGridx(); // vertical grid
  pad2_reco->Draw();
  pad2_reco->cd();
  fakes_ratio->SetTitle("Ratio To Total");
  fakes_ratio->GetYaxis()->SetRangeUser(0,.4);
  fakes_ratio->Draw();
  //draw_ratio(reco,fakes,name.c_str());
  cav->Print((name+"false_spec_comparison.pdf").c_str());



}
void false_spectrum_comparison(TTree *reco_tree, TTree *truth_tree, std::string name){

  TH1F *matched_events=new TH1F("matchedreco","matched reco",distribution_maps[name].bins,distribution_maps[name].binning);
  TH1F *total_events=new TH1F("total_reco","total reco",distribution_maps[name].bins,distribution_maps[name].binning);;
  TH1F *fakes=new TH1F("fakes","fakes",distribution_maps[name].bins,distribution_maps[name].binning);
  
  float data;
  double weight;
  float inSR;
  int eventNumber;
  float AverageMu;

  reco_tree->SetBranchAddress("AverageMu",&AverageMu);
  reco_tree->SetBranchAddress("weight",&weight);
  reco_tree->SetBranchAddress("eventNumber",&eventNumber);

  reco_tree->SetBranchAddress(name.c_str(),&data);
  reco_tree->SetBranchAddress("inSR",&inSR);
  bool TruthMatch=false;
  std::unordered_map<int,int> truth_map=FillRunNumber(truth_tree,"event_number",true);

  int nReco=reco_tree->GetEntries();
  for (int i=0;i<nReco;i++){
    reco_tree->GetEvent(i);
    if(inSR!=1) continue;
    if(AverageMu<40) continue;
    TruthMatch=IsInMap(truth_map,eventNumber);
    if(TruthMatch) matched_events->Fill(data,weight);
    else fakes->Fill(data,weight);
    total_events->Fill(data,weight);
  }


  total_events->SetMinimum(0);


  matched_events->SetName("MatchedReco");
  matched_events->SetTitle("Matched Reco");

  fakes->SetTitle("Fakes");
  total_events->SetTitle((name+" reco").c_str());

  fakes->SetLineColor(kRed);
  
    
  total_events->SetLineColor(kBlue);


  matched_events->SetLineColor(kBlack);

  TH1F* fakes_ratio=(TH1F*)fakes->Clone("fakes_ratio");

  fakes_ratio->Divide(total_events);

  
  TCanvas * cav=new TCanvas();
  cav->cd();
  TPad* pad1_reco = new TPad("pad1_r", "pad1_r",0.,.3,1.,1);
  pad1_reco->Draw();
  pad1_reco->cd();
  pad1_reco->SetBottomMargin(0);
  total_events->Draw();
  matched_events->Draw("SAME");
  fakes->Draw("SAME");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");

  cav->cd();
  TPad* pad2_reco = new TPad("pad2_r", "pad2_r",0,0.05,1,0.3);
  pad2_reco->SetTopMargin(0.0);
  pad2_reco->SetBottomMargin(0.25);
  pad2_reco->SetGridx(); // vertical grid
  pad2_reco->Draw();
  pad2_reco->cd();
  fakes_ratio->SetTitle(" Fakes Ratio To Total");
  fakes_ratio->GetYaxis()->SetRangeUser(0,.4);
  fakes_ratio->Draw();
  //draw_ratio(reco,fakes,name.c_str());
  cav->Print((name+"fakes_comparison.pdf").c_str());
  ofstream fakes_ratio_file;
  fakes_ratio_file.open("fakes_ratio.txt",std::ios::app);
  fakes_ratio_file<<name<<"\n";
  for(int i=0;i< fakes_ratio->GetNbinsX();i++){
    fakes_ratio_file<< fakes_ratio->GetBinContent(i+1)<<",";
  }
  fakes_ratio_file<<"\n";
  fakes_ratio_file.close();
}


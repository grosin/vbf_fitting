#include <vector>
#include "TH1F.h"
#include "TFile.h"
#include <TMath.h>
#include <TTree.h>
#include <string.h>
#include <unordered_map>
#include <iostream>
#include "merge.h"
using namespace TMath;


std::unordered_map<int,int> FillRunNumber(TTree * recoTree,const char * event_name,bool truth){
  std::cout<<recoTree<<"\n";
  recoTree->ResetBranchAddresses();
  std::unordered_map<int,int> EventToRunNumber;
  int duplicates=0;
  if(truth){
    unsigned long long runNum;
    std::cout<<"setting address\n";
    recoTree->SetBranchAddress(event_name,&runNum);
    std::cout<<"getting n entries\n";
      int nEvents=recoTree->GetEntries();
      //EventToRunNumber.reserve(nEvents);
      std::cout<<"looping over truth event numbers\n";
      
      for(int i=0; i < nEvents ;i++){
        recoTree->GetEvent(i);
        if(IsInMap(EventToRunNumber,(int)runNum)){
          duplicates++;
        }
        EventToRunNumber[(int)runNum]=i;
      }
      recoTree->ResetBranchAddresses();
      std::cout<<"total duplicates "<<duplicates<<"\n";

      return EventToRunNumber;
  }
  else{
    int runNum;
    std::cout<<"setting address\n";
    recoTree->SetBranchAddress(event_name,&runNum);
    std::cout<<"getting n entries\n";
      int nEvents=recoTree->GetEntries();
      //EventToRunNumber.reserve(nEvents);
      std::cout<<"looping over reco event numbers\n";

      for(int i=0; i < nEvents ;i++){
        recoTree->GetEvent(i);
        if(IsInMap(EventToRunNumber,(int)runNum)){
          duplicates++;
        }
        EventToRunNumber[(int)runNum]=i;
      }
      recoTree->ResetBranchAddresses();
      std::cout<<"total duplicates "<<duplicates<<"\n";
        
      return EventToRunNumber;
  }

}

bool IsInMap(std::unordered_map<int,int> &mymap, int input){
  return !(mymap.find (input) == mymap.end());
}

bool IsInMap(std::unordered_map<std::string,std::string> &mymap, std::string input){
  return !(mymap.find (input) == mymap.end());
}



#ifndef UNFOLD
#define UNFOLD

#include <algorithm>
#include <vector>
#include "TRandom3.h"
#include "TH1F.h"
#include "TH1.h"
#include <TH2F.h>
#include "TFile.h"
#include "TNtuple.h"
#include "TGraph.h"
#include "RooUnfoldResponse.h"
#include <iostream>
#include "TCanvas.h"
#include "TLegend.h"
#include "TROOT.h"
#include "TMatrixD.h"
#include "TCut.h"
#include "TTree.h"
#include <map>
#include <string>
#include <tuple>
#include <unordered_map>



/****** create response matrices *************/
RooUnfoldResponse create_response(TTree * truth_tree, TTree * reco_tree, std::string truth_distribution, std::string reco_distribution);

/******************define binning ******************/

struct distribution_container 
{  
  double* binning;
  int bins;
};

extern std::vector<std::string> scalars;
extern std::vector<std::string> leps;
extern std::vector<std::string> jets;

extern std::map<std::string,distribution_container> distribution_maps;

extern double binning_jet_deta[];extern int jet_deta_bins;
extern double binning_jet_dphi[];extern int jet_dphi_bins;
extern double binning_jet_dr[];extern int jet_dr_bins;
extern double binning_lead_jet_eta[];extern int lead_jet_eta_bins;
extern double binning_lead_jet_phi[];extern int lead_jet_phi_bins;
extern double binning_lead_jet_pt[];extern int jet_pt_bins;


extern double binning_lead_lep_pt[];extern int lep_pt_bins;
extern double binning_lep_deta[];extern int lep_deta_bins;
extern double binning_lep_dphi[];extern int lep_dphi_bins;
extern double binning_lep_dr[];extern int lep_dr_bins;
extern double binning_met[];extern int met_bins;
extern double binning_mjj[];extern int mjj_bins;
extern double binning_mll[];extern int mll_bins;
#endif


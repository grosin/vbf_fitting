#include <vector>
#include "TH1F.h"
#include "TH1.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TGraph.h"
#include "TVectorD.h"
#include "RooUnfoldResponse.h"
#include <iostream>
#include "TCanvas.h"
#include "TLegend.h"
#include "TROOT.h"
#include "unfold.h"
#include "TCut.h"
#include <cstring>
#include "TMath.h"
#include <fstream>
#include <TTree.h>
#include <string>
#include <tuple>
#include "merge.h"
#include <unordered_map>
using namespace TMath;
using namespace std;

std::vector<std::string> scalars ={"Mjj","Mll","MET","Detall","DPhill","DRll","DEtajj","DPhijj","DRjj","DYjj","DYll","higgs_pt","higgs_phi","higgs_eta","lep_pt_0","lep_pt_1","jet_pt_0","jet_pt_1","Ptll","MTll","ptTot","nJets","cosTheta"};
std::vector<std::string> vecs={"lead_lep_pt","sub_lep_pt","lead_jet_pt","sub_jet_pt"};

string to_upper(string st){
  transform(st.begin(), st.end(), st.begin(), ::toupper);
  return st;
}

void itoa(float number, char* str) {
   sprintf(str, "%.1f", number); 
}


RooUnfoldResponse create_response(TTree * truth_tree, TTree * reco_tree, std::string truth_distribution, std::string reco_distribution){
  std::unordered_map<int,int> truth_map=FillRunNumber(truth_tree,"event_number",true);
  std::unordered_map<int,int> reco_map=FillRunNumber(reco_tree,"eventNumber",false);
  float truth_data;
  float detector_data;
  float Mjj_truth=0;
  float DPhill_truth=0;
  float Mjj_reco=0;
  float DPhill_reco=0;
    
  double truth_weight;
  double reco_weight;
  float inSR;
  std::cout<<"truth distribution "<<truth_distribution<<"\n";
  std::cout<<"reco distribution "<<reco_distribution<<"\n";

  truth_tree->SetBranchAddress(truth_distribution.c_str(), &truth_data);
  if(truth_distribution.compare("Mjj_truth")!=0)
    truth_tree->SetBranchAddress("Mjj_truth", &Mjj_truth);
  if(truth_distribution.compare("DPhill_truth")!=0)
    truth_tree->SetBranchAddress("DPhill_truth", &DPhill_truth);

  reco_tree->SetBranchAddress(reco_distribution.c_str(), &detector_data);
  truth_tree->SetBranchAddress("weight", &truth_weight);
  reco_tree->SetBranchAddress("weight",&reco_weight);
  if(reco_distribution.compare("Mjj")!=0)
    reco_tree->SetBranchAddress("Mjj",&Mjj_reco);
  if(reco_distribution.compare("DPhill")!=0)
  reco_tree->SetBranchAddress("DPhill",&DPhill_reco);

  reco_tree->SetBranchAddress("inSR",&inSR);
  std::cout<<"true tree"<<truth_tree->GetEntries()<<"truth map"<<truth_map.size()<<"\n";
  std::cout<<" reco tree"<<reco_tree->GetEntries()<<"reco map"<<reco_map.size()<<"\n";

  TH1F *hist=new TH1F(truth_distribution.c_str(),truth_distribution.c_str(),distribution_maps[reco_distribution].bins,distribution_maps[reco_distribution].binning);
  RooUnfoldResponse response (hist,hist);
  
  response.UseOverflow(false);
  float corr=1000;
  bool TruthMatch=false;
  bool RecoMatch = false;
  int missed_events=0;
  int matched_events=0;
  int fake_events=0;
  for (auto & truth_event :truth_map ){
    truth_tree->GetEvent(truth_event.second);

    TruthMatch=true;
    
    if(Mjj_truth <450000 && truth_distribution.compare("Mjj_truth")!=0) continue;
    if(DPhill_truth>1.4 &&  truth_distribution.compare("DPhill")!=0) continue;
    if(truth_data<450000 && truth_distribution.compare("Mjj")==0) continue;
    if(truth_data >1.4 && truth_distribution.compare("DPhill")==0) continue;
    
    int eventNumber=truth_event.first;
    RecoMatch=IsInMap(reco_map,eventNumber);
    if(RecoMatch){
      reco_tree->GetEvent(reco_map[truth_event.first]);
      
      if((int)inSR!=1) continue;
      if(Mjj_reco <450000 && reco_distribution.compare("Mjj")!=0) continue;
      if(DPhill_reco>1.4 &&  reco_distribution.compare("DPhill")!=0) continue;
      if(detector_data<450000 && reco_distribution.compare("Mjj")==0) continue;
      if(detector_data >1.4 && reco_distribution.compare("DPhill")==0) continue;
      
      response.Fill(detector_data,truth_data,reco_weight);
      matched_events++;
    }
    else{
      response.Miss(truth_data,truth_weight);
      missed_events++;
    }
  }
  for (auto & reco_event :reco_map ){
    RecoMatch=true;
    reco_tree->GetEvent(reco_event.second);
    
    if((int)inSR!=1) continue;
    if(Mjj_reco <450000 && reco_distribution.compare("Mjj")!=0) continue;
    if(DPhill_reco>1.4 &&  reco_distribution.compare("DPhill")!=0) continue;
    if(detector_data<450000 && reco_distribution.compare("Mjj")==0) continue;
    if(detector_data >1.4 && reco_distribution.compare("DPhill")==0) continue;
   
    int eventNumber=reco_event.first;
    TruthMatch=IsInMap(truth_map,eventNumber);
    if(!TruthMatch){
      response.Fake(detector_data,reco_weight);
      fake_events++;
    }
  }
  truth_map.clear();
  reco_map.clear();
  truth_tree->ResetBranchAddresses();
  reco_tree->ResetBranchAddresses();
  return response;
}

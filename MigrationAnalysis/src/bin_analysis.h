#ifndef BIN_ANALYSIS
#define BIN_ANALYSIS
#include <iostream>
#include <string>
#include <vector>
#include <TTree.h>
#include <TTree.h>
#include "RooUnfoldResponse.h"

TH1F*  purity(RooUnfoldResponse &response,TH1F * reco);

TH1F* effeciency_plots(RooUnfoldResponse &response,TH1F * truth);

TH1F * misses_plot(RooUnfoldResponse &response, TH1F* truth,std::string name);
TH1F * fakes_plot(RooUnfoldResponse &response, TH1F* reco);
void false_spectrum_comparison(RooUnfoldResponse &response, std::string name);
void false_spectrum_comparison(TTree *reco_tree, TTree *truth_tree, std::string name);
void draw_false_spectrum(TH1*total_reco, TH1* misses, TH1*fakes);
#endif //BIN_ANALYSIS

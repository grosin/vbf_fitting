import uproot
import numpy as np
import matplotlib.pyplot as plt
name_map={"Leading_lep":"$Leading \; Lepton_{Pt}$",
          "sub_lep":"$Subleading \; Lepton_{Pt}$",
          "Leading_jet":"$Leading \; Jet_{Pt}$",
          "sub_jet":"$Subleading \; Jet_{Pt}$",
          "higgs_pt":"$H_{Pt}$",
          "Mjj":"$M_{jj}$",
          "Mll":"$M_{ll}$",
          "DYll":"$\Delta Y_{ll}$",
          "DYjj":"$\Delta Y_{jj}$",
          "costhetastar":r"Cos($\theta$*)",
          "DPhill":"$\Delta \phi_{ll}$",
          "DPhijj":"$\Delta \phi_{jj}$"}
def plot_c_factors(reco_th1,truth_th1,name,obs):
    edges=reco_th1.edges
    reco_values=reco_th1.values
    truth_values=truth_th1.values
    values=reco_values / truth_values
    gev=1
    GeV=""
    if(edges[-1] > 1000):
        gev=1000
        GeV="GeV"
    x=(edges[1:]+edges[:-1])/(2*gev)
    xerr=(edges[1:]-edges[:-1])/(2*gev)
    plt.errorbar(x,values,xerr=xerr,fmt="r,")
    plt.title(f"{name}",usetex=True)
    plt.ylabel("bin c factor")
    plt.ylim(0.25,0.55)
    plt.xlabel(f"{name} {GeV}",usetex=True)
    plt.savefig(f"{obs}_cfactors_plot.pdf")
    plt.cla()
    plt.close()

def plot_reco_truth(reco_th1,truth_th1,name,obs):
    edges=reco_th1.edges
    reco_values=reco_th1.values
    truth_values=truth_th1.values
    gev=1
    GeV=""
    if(edges[-1] > 1000):
        gev=1000
        GeV="GeV"
    x=(edges[1:]+edges[:-1])/(2*gev)
    xerr=(edges[1:]-edges[:-1])/(2*gev)
    plt.errorbar(x,reco_values,xerr=xerr,fmt="r,",label="Reco")
    plt.errorbar(x,truth_values,xerr=xerr,fmt="b,",label="Truth")
    plt.ylim(bottom=0)
    plt.title(f"{name}",usetex=True)
    plt.ylabel("Expected Events")
    #plt.ylim(0.35,0.6)
    plt.xlabel(f"{name} {GeV}",usetex=True)
    plt.savefig(f"{obs}_truth_reco_plot.pdf")
    plt.legend()
    #plt.show()
    plt.cla()
    plt.close()
    
def print_bins(reco_th1,truth_th1,name):
    edges=reco_th1.edges
    reco_values=reco_th1.values
    truth_values=truth_th1.values
    values=reco_values / truth_values
    gev=1
    GeV=""
    if(edges[-1] > 1000):
        gev=1000
        GeV="$[GeV]$"
    with open("observable_cfactors.txt","a") as config_file:
        config_file.write(name+"\n")
        for i in range(len(edges)-1):
            line=f"c factor bin {edges[i]/gev} - {edges[i+1]/gev}: {round(values[i],3):.3f} , {round(139/truth_values[i],3):.3f}, {round(truth_values[i] / 139,3):.3f}\n"
            print(line)
            config_file.write(line)

def print_data(reco_th1,truth_th1):
    edges=reco_th1.edges
    reco_values=reco_th1.values
    truth_values=truth_th1.values
    values=reco_values / truth_values
    gev=1
    GeV=""
    if(edges[-1] > 1000):
        gev=1000
    GeV="$[GeV]$"
    for i in range(len(edges)-1):
        print(f"c factor bin {edges[i]/gev} - {edges[i+1]/gev}: {round(values[i],3):.3f} , {round(truth_values[i],3):.3f}, {round(reco_values[i]):.3f} ")
        
th1_file=uproot.open("c_factor_hists.root")
hists={}
for key,th1 in th1_file.items():
    if 'Ratio' in str(key):
        continue
    level="Truth"
    if "reco" in str(key):
        level="Reco"
    name=""
    if "Leading" in str(key) or "sub" in str(key) or "higgs" in str(key):
        name="_".join(str(key.decode()).split("_")[:2])
    else:
        name=str(key.decode()).split("_")[0]
    if name in hists:
        hists[name][level]=th1
    else:
        hists[name]={level:th1}


for name,data in hists.items():
    #plot_reco_truth(data["Reco"],data["Truth"],name_map[name],name)
    data=hists[name]
    print(name)
    print_bins(data["Reco"],data["Truth"],name)
    #plot_c_factors(data["Reco"],data["Truth"],name_map[name],name)
    print("--------------------")

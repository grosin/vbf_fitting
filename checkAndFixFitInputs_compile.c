#include "tools.h"
#include <stdexcept>
bool isDiff=true;
string topWWDscir="bdt_TopWWAll";
string vbfDsc="bdt_vbf";
bool clone_regions=false;
//string topWWDscir="bdt_TopWWAll2";
int nBinsDiff=10;

void use_unsmoothed(TFile *f,TH1F *h,string disc);

std::string remove_extension(const std::string& filename,string ext=";") {
    size_t lastdot = filename.find_last_of(ext);
    if (lastdot == std::string::npos) return filename;
    cout<<" removing from "<<filename <<" to "<<filename.substr(0, lastdot)<<endl;
    return filename.substr(0, lastdot); 
}

string whichObs(std::string & mainStr, const std::string & toErase){
  if( mainStr.find(toErase)==string::npos) return "unknown";

  string sbtr=mainStr.substr(mainStr.find(toErase)+toErase.length(),mainStr.length());
  return sbtr;

}
void check_safety(TH1F* h,std::map<string, vector <string>> &checkMap, string sample, string region){
  int safe=0;

  for(std::map<string, vector<string>>::iterator itC=checkMap.begin(); itC!=checkMap.end(); itC++){
    cout<<"iterating map\n";
    cout<< (*itC).first<<"\n";
    vector <string> lregions=(*itC).second;
    cout<<sample<<"\n";
    cout<<"gotten l regions\n";
    if( sample.compare( (*itC).first )==0 ){
      cout<<"comparing saftey\n";

      for( vector<string>::iterator itA=lregions.begin(); itA!=lregions.end(); itA++){
  cout<<"iterating regions\n";
  if( region.compare( *itA)==0  ){
    cout<<"Safeness "<<h->GetName()<<" against  "<< (*itC).first<<" and "<< (*itA)<<endl;
    safe++;
  }
      }
      if(safe==0){
  cout<<" histogram "<< h->GetName()<<" is not safe "<<endl;
  for( int i=0; i<(int)h->GetNbinsX()+2; i++){
    h->SetBinContent(i,0);
    h->SetBinError(i,0);
  }
      }
    }
  }
}
int split(std::string full_string,std::string delim="_"){
  std::vector<std::string> splits;
  auto start = 0U;
  auto end = full_string.find(delim);
  splits.push_back(full_string.substr(start, end - start));
  while (end != std::string::npos)
    {
      start = end + delim.length();
      end = full_string.find(delim, start);
      splits.push_back(full_string.substr(start, end - start));

    }
  
  return std::stoi(splits[1]);
  
}

void rescale_theory(TH1F * hist,TFile *unsmoothed_hists){
  if(unsmoothed_hists==NULL){ throw std::invalid_argument( "can't scale, null unsmoothed theory file");}
  TH1F* unsmoothed=(TH1F*)unsmoothed_hists->Get(hist->GetName());
  if(unsmoothed_hists==NULL){ throw std::invalid_argument( "can't find unsmoothed theory to scale");}
  if(hist->Integral()!=0) 
    hist->Scale(unsmoothed->Integral()/hist->Integral());
  delete unsmoothed;

}
string eraseSubStr(std::string & mainStr, const std::string & toErase)
{
  string ret=mainStr;
  // Search for the substring in string
  size_t pos = ret.find(toErase);
 
  if (pos != std::string::npos)
  {
    // If found then erase it from string
    ret.erase(pos, toErase.length());
  }
    return ret;
}
string eraseSubStrToEnd(std::string & mainStr, const std::string & toErase)
{
  string ret=mainStr;
  // Search for the substring in string
  size_t pos = ret.find(toErase);
 
  if (pos != std::string::npos)
  {
      //cout<<"Ereasing from "<<toErase<<" to "<<ret<<endl;
      // If found then erase it from string
    ret.erase(pos, ret.length());
  }
    return ret;
}


string whichRegion(string name, vector<string>vec){
  string region;

  for( vector<string>::iterator it=vec.begin(); it!=vec.end(); it++){
    string thisRegionTest="_"+(*it)+"_";
    if(TString(name.c_str()).Contains( thisRegionTest.c_str())){
      region=(*it);
      break;
    }
  }
  return region;
}
void use_nominal(TH1F* hnom,TH1F* hup, TH1F *hdown){
  for(int i=0;i<=hnom->GetNbinsX();i++){
    hup->SetBinContent(i,hnom->GetBinContent(i));
    hup->SetBinError(i,hnom->GetBinError(i));
    hdown->SetBinContent(i,hnom->GetBinContent(i));
    hdown->SetBinError(i,hnom->GetBinError(i));
  }

}
string whichSample(string name, vector<string>vec,vector<string>vec2){
  //cout<<"Searching for sample for "<<name<<endl;
  
  string sample="";
  //remove useless parts
  //if(TString(name.c_str()).Contains("Nom"))
  //name=eraseSubStrToEnd(name,"Nom");

  bool containsOther=false;
  for( vector<string>::iterator it=vec2.begin(); it!=vec2.end(); it++){
    string thisSampleTest=(*it);
    //cout<<"Testing (shortlist) "<<thisSampleTest<<" compared to "<<name<<endl;
    if( (TString(name.c_str())).Contains(thisSampleTest.c_str())){
      containsOther=true;
      sample=(*it);
      //      cout<<"Sample is (shortlist) "<<sample<<endl;
      return sample;
    }
  }
  
  for( vector<string>::iterator it2=vec.begin(); it2!=vec.end(); it2++){
    string thisSampleTest2= (*it2) ;
    //cout<<"Testing "<<thisSampleTest2<<" compared to "<<name<<endl;
    if( (TString(name.c_str())).Contains(thisSampleTest2.c_str())){
      sample=(*it2);
      //cout<<"Sample is "<<sample<<endl;
      return sample ;
    }
  }

  //cout<<"Sample is not found "<<sample<<endl;
  return sample;
}

string whichVariation(string name="",string sampleName="",string rName=""){
  cout<<"checking which variation\n";
  if ( name.find("Nom")!=string::npos) return "nominal";
  name=eraseSubStr(name, sampleName);
  if( name.find("High_")!=string::npos)
    name=eraseSubStrToEnd(name,"High");
  else if (name.find("Low_")!=string::npos)
    name=eraseSubStrToEnd(name,"Low");
  else
    return "unkown";
  return name; 
}
void symmetrize_two_point(TH1F * hnom, TH1F *hup, TH1F *hdown){
  int nbins=hnom->GetNbinsX();
  for(int i=1;i<=nbins;i++){
    double prev=hup->GetBinContent(i);
    hup->SetBinContent(i,2*hnom->GetBinContent(i)-hdown->GetBinContent(i));
    hup->SetBinError(i,hup->GetBinError(i)*hup->GetBinContent(i)/prev);

  }

}
void symmetrize_hist(TH1F* hnom, TH1F * hup, TH1F* hdown){
  
  for(int bin=1; bin<(int)hnom->GetNbinsX()+1; bin++){

    double nomVal=hnom->GetBinContent(bin);
    double highVal=hup->GetBinContent(bin);
    double lowVal=hdown->GetBinContent(bin);
    if(highVal==lowVal){
      highVal=2*nomVal-lowVal;
    }
    //double symmUncert=0.5*fabs(  fabs(1-highVal/nomVal)  + fabs(1-lowVal/nomVal));                                                                                                   
    double symmUncert=0.5*fabs( highVal - lowVal )/nomVal ;
    if(nomVal==0)
      symmUncert=0;
    cout<<" Symm rel uncertainty "<<symmUncert<<endl;

    //double direction= 0;
    double directionBinLow = hdown->GetBinContent(bin -1) > hnom->GetBinContent(bin-1)  ? 1:-1;
    double directionBinHigh= hup->GetBinContent(bin+1) > hnom->GetBinContent(bin+1) ? 1:-1;
    double direction = hup->GetBinContent(bin) >= hnom->GetBinContent(bin)  ? 1:-1;
    /*
    if((hnom->GetBinContent(bin-1)+hnom->GetBinContent(bin+1)) ==0)
      direction=1;
    else
      direction= 1/(hnom->GetBinContent(bin-1)+hnom->GetBinContent(bin+1)) * (directionBinLow*hnom->GetBinContent(bin-1) + directionBinHigh*hnom->GetBinContent(bin+1));
    //cout<<"Directions up: "<<directionBinHigh<<" down: "<<directionBinLow<<" average direction "<<direction<<endl;                                                                   
    if(direction >0) direction=1;
    else direction =-1;
    */
    highVal=nomVal + direction*symmUncert*nomVal;
    lowVal=nomVal -  direction*symmUncert*nomVal;

    hdown->SetBinContent(bin,lowVal);    
    hup->SetBinContent(bin,highVal);
    
    if(lowVal<0 && highVal <0){
      direction=2*(highVal>lowVal)-1;
      hup->SetBinContent(bin,nomVal*(1+direction*abs(symmUncert)));
      hdown->SetBinContent(bin,nomVal*(1-direction*abs(symmUncert)));

    }
    else if( hup->GetBinContent(bin) < 0 ) { 
      hup->SetBinContent(bin,0+0.005*hnom->GetBinContent(bin)); 
      hdown->SetBinContent(bin,nomVal*(2-0.005));
    }
    else if( hdown->GetBinContent(bin) < 0) { 
      hdown->SetBinContent(bin,0+0.005*hnom->GetBinContent(bin)); 
      hup->SetBinContent(bin,nomVal*(2-0.005));
    }
  }

}
double calculate_chi2(TH1F* hnom, TH1F* hvar){
  double chisqr=0;
  int nbins=hnom->GetNbinsX();
  bool isTheor=false;
  double sigma=1;
  if(TString(hvar->GetName()).Contains("theo"))
    isTheor=true;
  for(int i=1;i<=nbins;i++){
    if(isTheor)
      sigma=hnom->GetBinError(i);
    else
      sigma=pow ( abs( pow(hnom->GetBinError(i),2) - pow(hvar->GetBinError(i),2) ),0.5 );
    chisqr+=pow(hnom->GetBinContent(i)-hvar->GetBinContent(i),2) / sigma;
    std::cout<<"relative errors: "<<hnom->GetBinContent(i)-hvar->GetBinContent(i)<<" "<<hnom->GetBinError(i)<<"\n";
  }

  
  return  chisqr/((double)nbins-1.0);
}
void smooth_histogram(TH1F * hist,double smoothing_parameter=0.70){
  int nbins=hist->GetNbinsX();
  double mean_bin_width=0;
  for(int i=1;i<nbins;i++){
    mean_bin_width+=hist->GetBinWidth(i);
  }
  mean_bin_width=mean_bin_width/(double)nbins;
  double smooth_sigma=mean_bin_width*smoothing_parameter;
  double kernel;
  std::vector<double> smoothed_data(nbins);
  double kernel_sum;
  for(int i=1;i<=nbins;i++){
    kernel_sum=0;
    for(int j=1;j<=nbins;j++){
      kernel=TMath::Exp(-1*pow(hist->GetBinCenter(i)-hist->GetBinCenter(j),2)/pow(2*smooth_sigma,2));
      kernel_sum+=kernel;
      smoothed_data[i-1]+=hist->GetBinContent(j)*kernel;
    }
    smoothed_data[i-1]=smoothed_data[i-1]/kernel_sum;
  }
  double bin_content;
  double bin_error;
  for(int i=1;i<=nbins;i++){
    bin_content=smoothed_data[i-1];
    bin_error=hist->GetBinError(i)*bin_content;
    hist->SetBinContent(i,bin_content);
    hist->SetBinError(i,bin_error);
  }

}


void smooth_ratio_histogram(TH1F * hist,TH1F * hnom,double smoothing_parameter=0.70){
  int nbins=hist->GetNbinsX();
  double mean_bin_width=0;
  for(int i=1;i<nbins;i++){
    mean_bin_width+=hist->GetBinWidth(i);
  }
  mean_bin_width=mean_bin_width/(double)nbins;
  double smooth_sigma=mean_bin_width*smoothing_parameter;
  double kernel;
  std::vector<double> smoothed_data(nbins);
  double kernel_sum;
  for(int i=1;i<=nbins;i++){
    kernel_sum=0;
    for(int j=1;j<=nbins;j++){
      kernel=TMath::Exp(-1*pow(hist->GetBinCenter(i)-hist->GetBinCenter(j),2)/pow(2*smooth_sigma,2));
      kernel_sum+=kernel;
      if(hnom->GetBinContent(j)>0)
  smoothed_data[i-1]+=(hist->GetBinContent(j)-hnom->GetBinContent(j))/hnom->GetBinContent(j)*kernel;
      else
  smoothed_data[i-1]+=0;

    }
    smoothed_data[i-1]=smoothed_data[i-1]/kernel_sum;      
  }
  double bin_content;
  double bin_error;
  for(int i=1;i<=nbins;i++){
    bin_content=hnom->GetBinContent(i)*(1+smoothed_data[i-1]);
    bin_error=hist->GetBinError(i)*bin_content;
    hist->SetBinContent(i,bin_content);
    hist->SetBinError(i,bin_error);
  }
    
}

void fix_theory_histogram(TFile* histfile,string smoothed_nom_name,TH1F* theory_variation){
  string name=std::string(theory_variation->GetName());
  cout<<"fixing theory histogram "<<name<<"\n";
  
  TH1F *smoothed_nominal=(TH1F*)histfile->Get( smoothed_nom_name.c_str());
  if(smoothed_nominal==NULL)
    return;
  if(TString(theory_variation->GetName()).Contains("Nom_"))
    return;
 
  if(TString(smoothed_nom_name).Contains("CRZjets")){
    smoothed_nominal->Rebin(2);
  }
/*
  //assuming hzjets1 is the EWK samples
  if(TString(theory_variation->GetName()).Contains("hZjets1")){
    cout<<"fixing hzjets1 for \n";
    cout<<theory_variation->GetName()<<"\n";

    for(int i=0;i<=smoothed_nominal->GetNbinsX();i++){
      theory_variation->SetBinContent(i,smoothed_nominal->GetBinContent(i));
      theory_variation->SetBinError(i,smoothed_nominal->GetBinError(i));
    }
    return;
}*/
  if(TString(theory_variation->GetName()).Contains("Low_")){
    int index = name.find("Low_", 0);
    name.replace(index, 3, "Nom");
  }else{
    int index = name.find("High_", 0);
    name.replace(name.begin()+index,name.begin()+index+4, "Nom");
  }
  TH1F* unsmoothed_nominal=(TH1F*)histfile->Get(name.c_str());
  cout<<name<<"\n";
  cout<<unsmoothed_nominal<<"\n";
  cout<<histfile->GetName()<<"\n";
  cout<<smoothed_nominal<<"\n";
  //smooth_histogram(unsmoothed_nominal);
  if(smoothed_nominal->GetNbinsX()!=theory_variation->GetNbinsX()){
    cout<<smoothed_nominal->GetName()<<"\n";
    cout<<theory_variation->GetName()<<"\n";
    cout<<smoothed_nominal->GetNbinsX()<<"\n";
    cout<<theory_variation->GetNbinsX()<<"\n";

    throw std::invalid_argument( "theory and smoothed nominal have different binning");
  }
  //extract norm:
  double theory_integral = theory_variation->Integral();
  double unsmoothed_nom_integral = unsmoothed_nominal->Integral();

  //normalize to unsmoothed nominal to get shape only
  theory_variation->Scale(unsmoothed_nom_integral/theory_integral);

  if(TString(theory_variation->GetName()).Contains("CRZjets")&&TString(theory_variation->GetName()).Contains("ztautau_gene")) {
    cout<<"before norm, theo integral:"<<theory_integral<<endl;
    cout<<"theo integral: "<<theory_variation->Integral()<<"; unsmoothed nom integral: "<<unsmoothed_nom_integral <<endl;
  }   

  for(int i=0;i<=smoothed_nominal->GetNbinsX();i++){
    //cout<<"calculating variation\n";
    /*
    if(!(unsmoothed_nominal->GetBinContent(i)>0.0001)){
      theory_variation->SetBinContent(i,0.0001);
      theory_variation->SetBinError(i,theory_variation->GetBinError(i)*0.0001);
      continue; 
    }
    if(!(smoothed_nominal->GetBinContent(i)>0.001)){
      theory_variation->SetBinContent(i,0.0001);
      theory_variation->SetBinError(i,theory_variation->GetBinError(i)*0.0001);
      continue;
    }
    if(variation < 0.0001){
      variation=0.0001;
    }
    */
    double variation=theory_variation->GetBinContent(i)/unsmoothed_nominal->GetBinContent(i);
    double error=theory_variation->GetBinError(i);
    //cout<<"propogating to nominal\n";
    double new_bin_content=smoothed_nominal->GetBinContent(i)*variation;
    error=error*new_bin_content/theory_variation->GetBinContent(i);
    theory_variation->SetBinContent(i,new_bin_content);
    theory_variation->SetBinError(i,error);

    if(TString(theory_variation->GetName()).Contains("CRZjets")&&TString(theory_variation->GetName()).Contains("ztautau_gene"))    
     cout<<"checkBin"<<i<<": "<<variation<<endl;

  }
  //normalize back to original theory variation norm
  theory_variation->Scale(theory_integral/unsmoothed_nom_integral);
  if(TString(theory_variation->GetName()).Contains("CRZjets")&&TString(theory_variation->GetName()).Contains("ztautau_gene")) {
    for(int i=0;i<=theory_variation->GetNbinsX();i++){
      cout<<"checkRenormBin"<<i<<": "<< theory_variation->GetBinContent(i)<<endl;
    }
    
  }   
     
  

  delete smoothed_nominal;
  delete unsmoothed_nominal;

}



void fix_theory_histogram(TFile* histfile,TH1F* smoothed_nominal,TH1F* theory_variation){

  string name=std::string(theory_variation->GetName());
  cout<<"fixing theory histogram "<<name<<"\n";
  if(smoothed_nominal==NULL)
    return;
  if(TString(theory_variation->GetName()).Contains("Nom_"))
    return;
  if(TString(theory_variation->GetName()).Contains("Low_")){
    int index = name.find("Low_", 0);
    name.replace(index, 3, "Nom");
  }else{
    int index = name.find("High_", 0);
    name.replace(name.begin()+index,name.begin()+index+4, "Nom");
  }
  TH1F* unsmoothed_nominal=(TH1F*)histfile->Get(name.c_str());
  /*
  if(TString(theory_variation->GetName()).Contains("hZjets1")){
    for(int i=0;i<=smoothed_nominal->GetNbinsX();i++){
      theory_variation->SetBinContent(i,smoothed_nominal->GetBinContent(i));
      theory_variation->SetBinError(i,smoothed_nominal->GetBinError(i));
    }
    return;
  }
  */
  cout<<name<<"\n";
  cout<<unsmoothed_nominal<<"\n";
  cout<<histfile->GetName()<<"\n";
  cout<<smoothed_nominal<<"\n";
  //smooth_histogram(unsmoothed_nominal);                                                                                                                                                      
  if(smoothed_nominal->GetNbinsX()!=theory_variation->GetNbinsX()){
    cout<<smoothed_nominal->GetName()<<"\n";
    cout<<theory_variation->GetName()<<"\n";
    throw std::invalid_argument( "theory and smoothed nominal have different binning");
  }
  for(int i=0;i<=smoothed_nominal->GetNbinsX();i++){
    if(!unsmoothed_nominal->GetBinContent(i)>0)
      continue;

    cout<<"calculating variation\n";
    double variation=theory_variation->GetBinContent(i)/unsmoothed_nominal->GetBinContent(i);
    double error=theory_variation->GetBinError(i);
    cout<<"propogating to nominal\n";
    double new_bin_content=smoothed_nominal->GetBinContent(i)*variation;
    error=error*new_bin_content/theory_variation->GetBinContent(i);
    theory_variation->SetBinContent(i,new_bin_content);
    theory_variation->SetBinError(i,error);
  }
  cout<<"finished loop\n";
  delete unsmoothed_nominal;

}
void fix_exp_histogram(TH1F* unsmoothed_nominal,TH1F* exp_variation){
  TH1F * smoothed_nominal=(TH1F*)unsmoothed_nominal->Clone("smoothed_nominal");
  // smooth_histogram(smoothed_nominal);
  for(int i=0;i<=smoothed_nominal->GetNbinsX();i++){
    double variation=exp_variation->GetBinContent(i)-smoothed_nominal->GetBinContent(i);
    double error=exp_variation->GetBinError(i);
    double new_bin_content=unsmoothed_nominal->GetBinContent(i)+variation;
    error=error*new_bin_content/exp_variation->GetBinContent(i);
    exp_variation->SetBinContent(i,new_bin_content);
    exp_variation->SetBinError(i,error);
  }
  delete smoothed_nominal;

}
void fix_smooth_nominal(TH1F* unsmoothed_nominal,TH1F* variation){
  TH1F* to_smoothed_nominal =(TH1F*)unsmoothed_nominal->Clone("to_smoothed_nominal");
  //smooth_histogram(to_smoothed_nominal);
  for(int i=0;i<=to_smoothed_nominal->GetNbinsX();i++){
    double new_bin_content=to_smoothed_nominal->GetBinContent(i)+variation->GetBinContent(i)-unsmoothed_nominal->GetBinContent(i);
    double error=variation->GetBinError(i);
    error=error*new_bin_content/variation->GetBinContent(i);
    variation->SetBinContent(i,new_bin_content);
    //variation->SetBinError(i,error);
  }
  delete to_smoothed_nominal;

}
void use_unsmoothed(TFile *f,TH1F *h,string disc){
  if(disc.compare(topWWDscir)!=0)
    return;
  cout<<"using unsmoothed with "<<h->GetName()<<"\n";
  string unsmoothed_hist_name=string(h->GetName())+"_InputHis__"+disc;
  cout<<"and "<<unsmoothed_hist_name<<"\n";
  TH1F * unsmoothed_hist=(TH1F*)f->Get(unsmoothed_hist_name.c_str());
  if(h==NULL)
    throw std::invalid_argument( "smoothed histogram is null");
  if(unsmoothed_hist==NULL)
    throw std::invalid_argument( "unsmoothed histogram is null");
  for(int i=0;i<=h->GetNbinsX();i++){
    h->SetBinContent(i,unsmoothed_hist->GetBinContent(i));
    h->SetBinError(i,unsmoothed_hist->GetBinError(i));
  }
  delete unsmoothed_hist;

}
void sliding_window_smoothing(TH1F * hnom, TH1F * var_hist,int window=1){
  int nbins=hnom->GetNbinsX();
  std::vector<double> averaged(nbins);

  for(int i=1;i<=nbins;i++){
    double window_average=(var_hist->GetBinContent(i)-hnom->GetBinContent(i))/hnom->GetBinContent(i);
    double norm=1;
    for(int back=0;back<window;back++){
      if(i-(back+1) >0){
  window_average+=(var_hist->GetBinContent(i-(back+1))-hnom->GetBinContent(i-(back+1))/hnom->GetBinContent(i-(back+1)));
  norm+=1;
      }
    }for(int forw=0;forw<window;forw++){
     if(i+forw+1 <nbins){
       window_average+=(var_hist->GetBinContent(i+forw+1)-hnom->GetBinContent(i+(forw+1))/hnom->GetBinContent(i+(forw+1)));
       norm+=1;
     }
  }
    window_average=window_average/norm;
    averaged[i-1]=window_average;
  }
  for(int j=1;j<=nbins;j++)
    var_hist->SetBinContent(j,hnom->GetBinContent(j)+hnom->GetBinContent(j)*averaged[j-1]);
}

bool maybe_rebin(TH1F * hnom){
  if(hnom->GetBinContent(1) > 0.1)
    return false;
  hnom->Rebin(2);
  return true;

}
void checkAndFixFitInputs(string tname,string variable, bool make_clones=false){
  // Load the file
  //TFile *f=new TFile(tname.c_str(),"UPDATE");
  ofstream chi_file;
  chi_file.open("chi_squared.txt");

  clone_regions=make_clones;
  string file_mode="READ";
  vbfDsc+="_"+variable;
  if(make_clones)
    file_mode="UPDATE";
  TFile *f=TFile::Open(tname.c_str(),file_mode.c_str());
  TFile *fnew=new TFile(Form("%s.1.root",tname.c_str()),"RECREATE");

  // take the histogram map
  TKey *keyP=nullptr;
  TIter nextP(f->GetListOfKeys());
  int nTrees=0;
  vector <string> TH1FNames;
  vector <string> TH1FNamesNorm;
  vector <string> TH1FNamesNormTF;
  vector <string> TH1FNamesNormRaw;
  vector <string> TH1FTheorNames;
  vector <string> TH1FTheorNominals;

  vector <bool> hasRegion={false,false,false};

  bool RooKeysInput=true; // determine whenever input were made using RooKeysPdf golbal variable

  nBinsDiff=0;

  vector <string> inNames;
  
  while ((keyP=(TKey*)nextP())) {
    if (strcmp(keyP->GetClassName(),"TH1")) {

      if(TString(keyP->GetName()).Contains("InputHis")) RooKeysInput=true; 
      
      if( TString(keyP->GetName()).Contains("InputHis_")) continue ;
      if(  TString(keyP->GetName()).Contains("Norm") ) continue;
      if ( !(TString(keyP->GetName()).Contains("_obs"))) continue; 
      //if( !TString(keyP->GetName()).Contains("EL_EFF_ID_CorrUncertaintyNP5") && !TString(keyP->GetName()).Contains("Nom")) continue;

      
      //bool isnew=true;
      //for(std::vector<string>::iterator it=TH1FNames.begin(); it!=TH1FNames.end(); it++){
      //if( (*it).compare(ttname)==0) 
      //  {   isnew=false; break; } 
      //}
      
      string ttname=remove_extension(keyP->GetName());
      inNames.push_back(ttname);
    }}
  
  // remove possible duplicates 
  sort( inNames.begin(), inNames.end() );
  inNames.erase( unique( inNames.begin(), inNames.end() ), inNames.end() );
  
  for(std::vector<string>::iterator it=inNames.begin(); it!=inNames.end(); it++){
    
    //bool isnew=true;
    //for(std::vector<string>::iterator it=TH1FNames.begin(); it!=TH1FNames.end(); it++){
    //  if( (*it).compare(ttname)==0) 
    //    {   isnew=false; break; } 
    //}
    //cout<<"Adding "<<ttname<<endl;
    string ttname=(*it);

    if( !TString(ttname.c_str()).Contains("Norm") && TString(ttname.c_str()).Contains("SRVBF") && TString(ttname.c_str()).Contains("htopNom"))
      nBinsDiff++; 
    TH1FNames.push_back(ttname);
    if(!TString(ttname.c_str()).Contains("Norm")){
      TH1FNamesNormRaw.push_back(ttname);
  
    }
    if(TString(ttname.c_str()).Contains("Norm") && TString(ttname.c_str()).Contains("_obs"))
      TH1FNamesNorm.push_back(ttname);
    if(TString(ttname.c_str()).Contains("Norm") && !TString(ttname.c_str()).Contains("_obs")){
      TH1FNamesNormTF.push_back(ttname);
    }
    if(TString(ttname.c_str()).Contains("theo"))
      TH1FTheorNames.push_back(ttname);
    
    if(TString(ttname.c_str()).Contains("Region1")) hasRegion[0]=true;
    if(TString(ttname.c_str()).Contains("Region2")) hasRegion[1]=true;
    if(TString(ttname.c_str()).Contains("Region3")) hasRegion[2]=true;
    
  }
  if(variable.compare("inc")==0)
    nBinsDiff=1;
  

  // vectors of bin edges of each observable 
  std::map<string,vector<double>> xbinsMap;
  

  const double emptyReplace=1e-12;
  // list of regions 
  vector <string> regions;
  regions.push_back("CRGGF1");
  regions.push_back("CRGGF2");
  regions.push_back("CRGGF3");
  regions.push_back("CRWW");
  regions.push_back("CRZjets");
  if(!isDiff){
    regions.push_back("CRTop");
    regions.push_back("SRVBF");
  }
  //for( int i=0; i<(int)15; i++)
  //regions.push_back(Form("SRVBF_%d",i));
  
  
  vector <string> samples; 
  //samples.push_back("hvbf0");
  samples.push_back("hdiboson");
  samples.push_back("htop");
  samples.push_back("hggf");
  //samples.push_back("hZjets");
  samples.push_back("hZjets0");
  //samples.push_back("hZjets1");
  samples.push_back("hVgamma");
  samples.push_back("hFakes");
  samples.push_back("hdata");
  samples.push_back("hvh");
  samples.push_back("htt");

 

  /*
  samples.push_back("hZjets1");
  samples.push_back("hdiboson1");
  samples.push_back("htop1");
  samples.push_back("hggf1");
  samples.push_back("hggf2");
  samples.push_back("hggf3");
  */
  
  vector <string> samples2; 
  samples2.push_back("hZjets1");
  samples2.push_back("hdiboson1");
  samples2.push_back("hdiboson2");
  samples2.push_back("hdiboson3");
  samples2.push_back("htop1");
  samples2.push_back("htop2");
  samples2.push_back("htop3");
  samples2.push_back("hggf1");
  samples2.push_back("hggf2");
  samples2.push_back("hggf3");
  if(!isDiff)
    samples2.push_back("hvbf0");


  std::map<string, vector <string>> whichSamplesDecorrRegions;
  whichSamplesDecorrRegions["Region1"]={"hZjets1","hdiboson1","hggf1","htop1","hdiboson2","hggf2","htop2","hdiboson3","hggf3","htop3"};
  whichSamplesDecorrRegions["Region2"]={"hdiboson2","hdiboson1","hggf1","htop1","hdiboson2","hggf2","htop2","hdiboson3","hggf3","htop3"};
  whichSamplesDecorrRegions["Region3"]={"hdiboson1","hggf1","htop1","hdiboson2","hggf2","htop2","hdiboson3","hggf3","htop3"};

  std::map<string,vector<string>> samplesPerRegion;
  if(!isDiff){
    samplesPerRegion["SRVBF"]=samples;
    samplesPerRegion["CRTop"]=samples;
  }

  //  else{
  //samplesPerRegion[]=;
  //}
  
  // get all the nominal histograms 
  std::map<string, std::pair<string,string>> nominalMap;
  std::map<string, std::pair<string,string>> normMap;
  std::map<string, std::pair<string,string>> tfMap;
  std::map<string, std::vector<std::pair<string,string>>> sysVarsMap;
  std::map<string, std::vector<std::pair<string,string>>> sysVarsNormMap;
  std::map<string, std::vector<std::pair<string,string>>> tfSysNormMap;

  //std::map<string, std::vector<std::pair<string,bool>> sysVarsIncludeInCheck;
  std::map<string, bool> sysNomZeroDoNotCheck;
  
  std::map<string, std::vector<string>> considerSys;

  // map of normalisation regions for each sample
  std::map<string, vector<string>> sampleNormMap;
  if(!isDiff){
    sampleNormMap["hvbf0"].push_back("SRVBF");
    sampleNormMap["hvbf0"].push_back("CRTop");
    //sampleNormMap["hvbf0"].push_back("CRWW");
    sampleNormMap["htop"].push_back("SRVBF");
    sampleNormMap["htop"].push_back("CRTop");
    sampleNormMap["hdiboson"].push_back("SRVBF");
    sampleNormMap["hdiboson"].push_back("CRTop");
    //sampleNormMap["hdiboson"].push_back("CRWW");
  }
  std::map<string,std::map<int,std::vector<int>>> CRTop_Norms;
  CRTop_Norms["Mjj"][0]={0,1};
  CRTop_Norms["Mjj"][1]={0,1};
  CRTop_Norms["Mjj"][2]={2,3};
  CRTop_Norms["Mjj"][3]={2,3};
  CRTop_Norms["Mjj"][4]={4,5};
  CRTop_Norms["Mjj"][5]={4,5};

  CRTop_Norms["inc"][0]={0};

  CRTop_Norms["Mll"][0]={0,1,2};
  CRTop_Norms["Mll"][1]={0,1,2};
  CRTop_Norms["Mll"][2]={0,1,2};
  CRTop_Norms["Mll"][3]={3,4};
  CRTop_Norms["Mll"][4]={3,4};

  CRTop_Norms["pt_H"][0]={0};
  CRTop_Norms["pt_H"][1]={1,2,3};
  CRTop_Norms["pt_H"][2]={1,2,3};
  CRTop_Norms["pt_H"][3]={1,2,3};
  CRTop_Norms["pt_H"][4]={4};



  CRTop_Norms["DYll"][0]={0,1,2};
  CRTop_Norms["DYll"][1]={0,1,2};
  CRTop_Norms["DYll"][2]={0,1,2};
  CRTop_Norms["DYll"][3]={3,4};
  CRTop_Norms["DYll"][5]={4,5};

  CRTop_Norms["DYjj"][0]={0,1};
  CRTop_Norms["DYjj"][1]={0,1};
  CRTop_Norms["DYjj"][2]={2,3};
  CRTop_Norms["DYjj"][3]={2,3};
  CRTop_Norms["DYjj"][4]={4,5};
  CRTop_Norms["DYjj"][5]={4,5};

  CRTop_Norms["DPhill"][0]={0,1,2};
  CRTop_Norms["DPhill"][1]={0,1,2};
  CRTop_Norms["DPhill"][2]={0,1,2};
  CRTop_Norms["DPhill"][3]={3,4};
  CRTop_Norms["DPhill"][4]={3,4};

  CRTop_Norms["SignedDPhijj"][0]={0,1};
  CRTop_Norms["SignedDPhijj"][1]={0,1};
  CRTop_Norms["SignedDPhijj"][2]={2,3};
  CRTop_Norms["SignedDPhijj"][3]={2,3};

  CRTop_Norms["Ptll"][0]={0,1,2};
  CRTop_Norms["Ptll"][1]={0,1,2};
  CRTop_Norms["Ptll"][2]={0,1,2};
  CRTop_Norms["Ptll"][3]={3,4};
  CRTop_Norms["Ptll"][4]={3,4};

  CRTop_Norms["costhetastar"][0]={0,1,2};
  CRTop_Norms["costhetastar"][1]={0,1,2};
  CRTop_Norms["costhetastar"][2]={0,1,2};
  CRTop_Norms["costhetastar"][3]={3,4};
  CRTop_Norms["costhetastar"][4]={3,4};



  CRTop_Norms["jet0_pt"][0]={0,1,2};
  CRTop_Norms["jet0_pt"][1]={0,1,2};
  CRTop_Norms["jet0_pt"][2]={0,1,2};
  CRTop_Norms["jet0_pt"][3]={3,4};
  CRTop_Norms["jet0_pt"][4]={3,4};


  CRTop_Norms["jet1_pt"][0]={0,1,2};
  CRTop_Norms["jet1_pt"][1]={0,1,2};
  CRTop_Norms["jet1_pt"][2]={0,1,2};
  CRTop_Norms["jet1_pt"][3]={3,4};
  CRTop_Norms["jet1_pt"][4]={3,4};


  CRTop_Norms["lep1_pt"][0]={0,1,2};
  CRTop_Norms["lep1_pt"][1]={0,1,2};
  CRTop_Norms["lep1_pt"][2]={0,1,2};
  CRTop_Norms["lep1_pt"][3]={3,4};
  CRTop_Norms["lep1_pt"][4]={3,4};

  CRTop_Norms["lep0_pt"][0]={0,1};
  CRTop_Norms["lep0_pt"][1]={0,1};
  CRTop_Norms["lep0_pt"][2]={2,3};
  CRTop_Norms["lep0_pt"][3]={2,3};
  CRTop_Norms["lep0_pt"][4]={4,5};
  CRTop_Norms["lep0_pt"][5]={4,5};

  //for( int i=0; i<(int)15; i++)
  //sampleNormMap["htop"].push_back(Form("SRVBF_%d",i));
  //sampleNormMap["htop"].push_back("CRWW");
  
  //else {
  //for( int i=0; i<(int)15; i++)
  //  sampleNormMap["hdiboson"].push_back(Form("SRVBF_%d",i));
  //}
  
  //sampleNormMap["hZjets0"].push_back("CRGGF1");
  sampleNormMap["hZjets0"].push_back("CRZjets");
  //sampleNormMap["hZjets1"].push_back("CRZjets");
  sampleNormMap["hZjets1"].push_back("CRGGF1");
  
  //sampleNormMap["htop1"].push_back("CRZjets");
  sampleNormMap["htop2"].push_back("CRGGF2");
  sampleNormMap["htop3"].push_back("CRGGF1");

  //sampleNormMap["hdiboson1"].push_back("CRZjets");
  sampleNormMap["hdiboson2"].push_back("CRGGF2");
  sampleNormMap["hdiboson3"].push_back("CRGGF1");
    
  sampleNormMap["hggf1"].push_back("CRGGF1");
  sampleNormMap["hggf"].push_back("CRGGF1");
  //sampleNormMap["hggf2"].push_back("CRZjets");
  sampleNormMap["hggf3"].push_back("CRGGF3");


  if(!isDiff){
    sampleNormMap["hggf"].push_back("SRVBF");
    sampleNormMap["hggf"].push_back("CRTop");
  //sampleNormMap["hggf"].push_back("CRWW");
  }

  // observables per region
  std::map<string, string> obsRegions;
  obsRegions["SRVBF"]=vbfDsc;
  //for( int i=0; i<(int)15; i++)
  //obsRegions[Form("SRVBF_%d",i)]="bdt_vbf";
  obsRegions["CRTop"]=topWWDscir;
  obsRegions["CRGGF3"]="bdt_ggFCR3_CutDPhill";
  obsRegions["CRGGF2"]="bdt_ggFCR2";
  obsRegions["CRGGF1"]="bdt_ggFCR1";
  obsRegions["CRZjets"]="MT";
  obsRegions["CRWW"]="bdt_TopWW";
  //obsRegions["SRGGF"]="bdt_vbfggf";
  
  //sampleNormMap["hggf"].push_back("CRGGF1");
   if(isDiff){
     for(int i=0; i<nBinsDiff; i++){
       cout<<Form("SRVBF_%d",i)<<"\n";
       regions.push_back(Form("SRVBF_%d",i));
       samples.push_back(Form("hvbf0_%d",i));
       samples2.push_back(Form("hvbf0_%d",i));
       sampleNormMap[Form("hvbf0_%d",i)].push_back(Form("SRVBF_%d",i));
       sampleNormMap["hdiboson"].push_back(Form("SRVBF_%d",i));
       sampleNormMap["htop"].push_back(Form("SRVBF_%d",i));
       sampleNormMap["hZjets0"].push_back(Form("SRVBF_%d",i));
       //sampleNormMap["hZjets1"].push_back(Form("SRVBF_%d",i));
       sampleNormMap["hggf"].push_back(Form("SRVBF_%d",i));
       sampleNormMap["hggf1"].push_back(Form("SRVBF_%d",i));
       obsRegions[Form("SRVBF_%d",i)]=vbfDsc;
       obsRegions[Form("CRTop_%d",i)]=topWWDscir;
     }

     for(int i=0; i<nBinsDiff; i++){
       cout<<Form("SRVBF_%d",i)<<"\n";
       regions.push_back(Form("CRTop_%d",i));
       sampleNormMap[Form("hvbf0_%d",i)].push_back(Form("CRTop_%d",i));
       sampleNormMap["htop"].push_back(Form("CRTop_%d",i));
       sampleNormMap["hdiboson"].push_back(Form("CRTop_%d",i));
       sampleNormMap["hggf"].push_back((Form("CRTop_%d",i)));
       sampleNormMap["hggf1"].push_back((Form("CRTop_%d",i)));
       sampleNormMap["hZjets0"].push_back(Form("CRTop_%d",i));
       //sampleNormMap["hZjets1"].push_back(Form("CRTop_%d",i));


     }
   }else{
     samples2.push_back("hvbf0");
   }
  
  

  // build map of tranfer factors for the samples for each systematic
  // inntegral for nominal and all variations 
  // save a raw version of the histogrmas 
   
  vector <string> namesToSplit;
  namesToSplit.push_back("htop");
  namesToSplit.push_back("hdiboson");
  namesToSplit.push_back("hggf");
  namesToSplit.push_back("hZjets0"); 
  // here add the missing one per region
  vector <string> addedNames;

  // samples where events should be 0
  std::map<string, vector <string>> checkMap;
  checkMap["hggf1"]={"CRGGF1"};
  checkMap["hggf2"]={"CRZjets"};
  checkMap["hggf3"]={"CRGGF3"};


  checkMap["htop1"]={"CRZjets"};
  checkMap["htop2"]={"CRGGF2"};
  checkMap["htop3"]={"CRGGF1"};

  checkMap["hdiboson1"]={"CRZjets"};
  checkMap["hdiboson2"]={"CRGGF2"};
  checkMap["hdiboson3"]={"CRGGF1"};
  
  checkMap["hZjets1"]={"CRGGF3"};
  checkMap["hZjets1"]={"CRGGF2"};
  checkMap["hZjets1"].push_back("CRGGF1");
  //checkMap["hZjets1"].push_back("CRZjets");


  if(isDiff){
    for( int i=0; i<(int)nBinsDiff; i++){
      checkMap[Form("hvbf0_%d",i)].push_back(Form("SRVBF_%d",i));
      checkMap[Form("hvbf0_%d",i)].push_back(Form("CRTop_%d",i));
      //checkMap["hZjets1"].push_back("SRVBF_%d");
      //checkMap["hZjets1"].push_back("CRTop_%d");
      //checkMap[Form("hvbf0_%d",i)].push_back("CRZjets");
      //checkMap[Form("hvbf0_%d",i)].push_back("CRWW");
      //checkMap[Form("hvbf0_%d",i)].push_back("CRGGF1");
      //checkMap[Form("hvbf0_%d",i)].push_back("CRGGF2");
      //checkMap[Form("hvbf0_%d",i)].push_back("CRGGF3");
    }
    checkMap["hvbf0_0"].push_back("CRGGF1");
    checkMap["hvbf0_0"].push_back("CRGGF2");
    checkMap["hvbf0_0"].push_back("CRGGF3");
    checkMap["hvbf0_0"].push_back("CRZjets");
  }
  
  checkMap["hvbf0"].push_back("SRVBF");
  checkMap["hvbf0"].push_back("CRTop");
  checkMap["hvbf0"].push_back("CRZjets");
  checkMap["hvbf0"].push_back("CRWW");
  checkMap["hvbf0"].push_back("CRGGF1");
  checkMap["hvbf0"].push_back("CRGGF2");
  checkMap["hvbf0"].push_back("CRGGF3");
  
  

    //checkMap["htop"].push_back("CRZjets");
    //checkMap["hdiboson"].push_back("CRZjets");
    if(isDiff){
      for( int i=0; i<(int)nBinsDiff; i++){
  checkMap["htop"].push_back(Form("SRVBF_%d",i));
  checkMap["hdiboson"].push_back(Form("SRVBF_%d",i));
  checkMap["htop"].push_back(Form("CRTop_%d",i));
  checkMap["hdiboson"].push_back(Form("CRTop_%d",i));
      }}
    else {
      checkMap["htop"].push_back("SRVBF");
      checkMap["hdiboson"].push_back("SRVBF");
      checkMap["htop"].push_back("CRTop");
      checkMap["hdiboson"].push_back("CRTop");
    }
  

   checkMap["hZjets0"].push_back("CRZjets");
   //checkMap["hZjets0"].push_back("CRGGF2");
   //checkMap["hZjets0"].push_back("CRGGF1");
   if(isDiff){
     for( int i=0; i<(int)nBinsDiff; i++){
       checkMap["hZjets0"].push_back(Form("SRVBF_%d",i));
       checkMap["hZjets0"].push_back(Form("CRTop_%d",i));
     }}
   else{
      checkMap["hZjets0"].push_back("SRVBF");
      checkMap["hZjets0"].push_back("CRTop");
   }
   
  
   //checkMap["hggf"].push_back("CRZjets");
  if(isDiff){
    for( int i=0; i<(int)nBinsDiff; i++){
      checkMap["hggf"].push_back(Form("SRVBF_%d",i));
      checkMap["hggf"].push_back(Form("CRTop_%d",i));
    }
  }
  if(!isDiff){
    checkMap["hggf"].push_back("SRVBF");
    checkMap["hggf"].push_back("CRTop");
  }
  
  //create new GGF category for crzjets

  


  for(vector<string>::iterator ik=TH1FTheorNames.begin(); ik!=TH1FTheorNames.end(); ik++){
    string htname=(*ik);

    TH1F *h=(TH1F*)f->Get( htname.c_str());
    cout<<"Checking "<<h->GetName()<<"  "<< (*ik)<<endl;
    TString name=h->GetName();
    if(!name.Contains("hvbf")) continue;
    //if(name.Contains("CRGGF")) continue;
    //if(name.Contains("CRZjets")) continue;
    cout<<"cloning "<<(*ik)<<"\n";
    cout<<nBinsDiff<<"\n";
    
    for(int i=0;i<nBinsDiff;i++){
      TH1F * hClone=(TH1F*)h->Clone( (TString( (*ik).c_str() ).ReplaceAll("hvbf0", Form("%s_%d","hvbf0",i))).Data());
      if(clone_regions){
  cout<<"saving clone "<<hClone->GetName()<<"\n";
  //hClone->Smooth(2);
  f->cd();
  hClone->Write();
  addedNames.push_back(hClone->GetName());
      }

    }
  }

  for(vector<string>::iterator ik=TH1FTheorNames.begin(); ik!=TH1FTheorNames.end(); ik++){
    string htname=(*ik);
    TH1F *h=(TH1F*)f->Get( htname.c_str()); 
    cout<<"Checking "<<h->GetName()<<"  "<< (*ik)<<endl;
    for( int m=1; m<4 ; m++){
      for( vector <string>::iterator il=namesToSplit.begin(); il!=namesToSplit.end(); il++){
        if( TString( (*ik).c_str() ).Contains( (*il).c_str() )){
    
          TH1F *hClone=nullptr;
          // Somehow it does not work oddly 
          if(TString( (*ik).c_str() ).Contains("hZjets")){
            string newName=(TString( (*ik).c_str() ).ReplaceAll( "hZjets0" , Form("hZjets%d",m))).Data();
      hClone=(TH1F*)h->Clone( (TString( (*ik).c_str() ).ReplaceAll("hZjets0", Form("hZjets%d",m))).Data());
      cout<<m <<" new name "<<TString( (*ik).c_str() ).ReplaceAll("hZjets0", Form("hZjets%d",m))<<"\n";
       //hClone=(TH1F*)h->Clone( newName.c_str());
            cout<<"Adding histogram "<<hClone->GetName()<<endl;
      /*
      string name=std::string(hClone->GetName());
      string sample=whichSample(name,samples,samples2);
      string region=whichRegion(name,regions);;
      string smoothed_nom_name=sample+"Nom_"+region+"_obs_"+obsRegions[region];
      TH1F * smoothed_nom=(TH1F*)f->Get(smoothed_nom_name.c_str());
      cout<<smoothed_nom_name<<"\n";
      if(smoothed_nom!=NULL){
        if(region.compare("CRZjets")==0){
    smoothed_nom->Rebin(2);
        }
        for(int i=0;i<=smoothed_nom->GetNbinsX();i++){
    hClone->SetBinContent(i,smoothed_nom->GetBinContent(i));
    hClone->SetBinError(i,smoothed_nom->GetBinError(i));
        }
      }
      delete smoothed_nom;
      */
      if(clone_regions){
        f->cd();
        //hClone->Smooth(2);
        hClone->Write();
        addedNames.push_back(hClone->GetName());
      }

            delete hClone;
            continue;
          }
            else 
              hClone=(TH1F*)h->Clone( (TString( (*ik).c_str() ).ReplaceAll((*il).c_str(), Form("%s%d",(*il).c_str(),m))).Data());
          
          cout<<"Adding histogram "<<hClone->GetName()<<endl;

    if(clone_regions){
      f->cd();
      //hClone->Smooth(2);
      hClone->Write();
      addedNames.push_back(hClone->GetName());
    }

          // Add here histograms for the thoery variations in regions
          const string sample=whichSample(htname,samples,samples2);
          const string region=whichRegion(htname,regions);
          const string variation= whichVariation( htname ,sample,region);
          if( sample.compare("hZjets1")==0 || sample.compare("hdiboson1")==0 || sample.compare("hdiboson2")==0  || sample.compare("hdiboson3")==0 ||
              sample.compare("htop1")==0 || sample.compare("htop2")==0 || sample.compare("htop3")==0){

            //TH1F *hregion=(TH1F*)hClone->Clone(TString(hClone->GetName()).ReplaceAll(variation.c_str(),Form("%s_Region%d",variation.c_str(),m)));
            //hregion->Write();
            //addedNames.push_back(hregion->GetName());
            //delete hregion;
          }
          
          
          delete hClone; 
        }
      }
    }
    delete h; 
  }
  
  if(make_clones)
    return;
  for(vector<string>::iterator it=addedNames.begin(); it!=addedNames.end(); it++){
    cout<<"Adding "<<(*it)<<endl;
    TH1FNames.push_back( (*it));
  }

 
/* . */
/*   /\* */
/*   // make interpolation for all histograms */
/*    for(vector<string>::iterator it=TH1FNames.begin(); it!=TH1FNames.end(); it++){ */
/*     TString hname(*it); */
    
/*     // selection criteria */

/*     // select only histograms corresponding to an observable  */
/*     if(!hname.Contains("obs")) continue;  */
    
    
/*     TH1F *hnew=(TH1F*)f->Get(hname.Data()); */
/*     if ( hnew==nullptr) continue;  */
/*     TH1F *horig=(TH1F*)hnew->Clone(Form("%s_noInterpo",hname.Data()));  */
/*     horig->SetName(Form("%s_noInterpo",hname.Data())); */
/*     vector <double> x; */
/*     vector <double> y; */

/*     bool toInterp=false; */
    
/*     if(!RooKeysInput){ */
/*       for(int b=1; b<(int)horig->GetNbinsX()+1; b++){ */
        
/*         if(horig->GetBinContent(b) < 0.1 || horig->GetBinError(b) > sqrt(TMath::Abs(horig->GetBinContent(b)))) */
/*           continue;  */
/*         x.push_back(horig->GetBinContent(b)); */
/*         y.push_back(horig->GetBinContent(b)/horig->GetBinWidth(b)); */
        
/*         if(horig->GetNbinsX()>5) */
/*           toInterp=true;  */
/*       } */
      
/*       toInterp=false; */
/*       TGraph *spline=new TGraph(x.size(),&x[0],&y[0]); */
      
/*       //cout<<"Spline for "<<hname.Data()<<endl; */
/*       if(!RooKeysInput){ */
/*         for( int b=1;b<(int)hnew->GetNbinsX()+1; b++){ */
          
/*           cout<<" orig bin "<<b<<" center "<<hnew->GetBinCenter(b)<<" val "<<hnew->GetBinContent(b)<<" err "<<hnew->GetBinError(b)<<endl; */
          
/*           double val=horig->GetBinContent(b); */
/*           double error=horig->GetBinError(b); */

/*           if( toInterp){ */
/*             val=horig->GetBinWidth(b)*spline->Eval(hnew->GetBinCenter(b),0,"S"); */
/*             error=1/sqrt(horig->GetEntries())*val; */
/*             if( horig->GetEntries()< horig->GetNbinsX()*0.5 || horig->GetEntries() < 10 || horig->Integral() < 1 ){ */
/*               val=1.0/(2.0*horig->GetNbinsX()); */
/*               error=2*val; */
/*             } */
/*           } */
          
/*           else if( !toInterp && val==0) { */
/*             val=horig->Integral()/(2.0*horig->GetNbinsX()); */
/*             error=2*val; */
/*           } */
          
/*           hnew->SetBinContent(b,val); */
/*           hnew->SetBinError(b,error); */
          
/*           //cout<<" interp bin "<<b<<" center "<<hnew->GetBinCenter(b)<<" val "<<hnew->GetBinContent(b)<<" err "<<hnew->GetBinError(b)<<endl; */
/*         }} */
      
/*       //if( 1/sqrt(horig->GetEntries()) > 0.1) */
/*       //hnew->Smooth(2); */
      
/*       delete spline; */
/*     } */
    
/*     hnew->Write(); */
/*     delete hnew; */
/*     horig->Write();  */
/*     delete horig;  */
/*    } */
   
/*    f->Close(); */
/*    delete f; */

/*    fnew->Close(); */
/*    delete fnew; */
/*    f=TFile::Open(Form("%s.1.root",tname.c_str())); */
/*    fnew=new TFile(Form("%s.2.root",tname.c_str()),"RECREATE"); */
/*   *\/ */

  
  
   for(vector<string>::iterator it=TH1FNames.begin(); it!=TH1FNames.end(); it++){ 
     TString hname(*it); 

     // remove spourious histograms 
    //if( !T String( (*it).c_str() ).Contains( obsRegions[whichRegion( (*it),regions )])) continue ;
    //if(  whichRegion( (*it),regions).compare(obsRegions[whichRegion( (*it),regions)])!=0)  continue ;
    if( hname.Contains("Nom") && hname.Contains("theo") ) {
      TH1FTheorNominals.push_back(hname.Data());
      continue; 
    } 

    // if this is the nominal file process 
    if(hname.Contains("Nom") && !hname.Contains("Norm") ){
      nominalMap[(*it)]=make_pair(whichRegion((*it),regions),whichSample((*it),samples,samples2));
      
      std::vector<std::pair<string,string>> tmp;
      sysVarsMap[(*it)]=tmp;
    }
    else if (hname.Contains("Nom") && hname.Contains("Norm") && hname.Contains("_obs")) {
      normMap[(*it)]=make_pair(whichRegion((*it),regions),whichSample((*it),samples,samples2));
    }
  }

  // print out all nominal maps
  for(std::map<string, std::pair<string,string>>::iterator it=nominalMap.begin(); it!=nominalMap.end() ;it++){
    cout<<(*it).first<<" region "<<(*it).second.first<<" sample "<<(*it).second.second<<endl;
  }
  cout<<endl;


  std::map<string, vector<string>> processedSys; 

  // process the nominal 
   for(vector<string>::iterator it=TH1FNamesNormTF.begin(); it!=TH1FNamesNormTF.end(); it++){
     TString hname(*it);
     

     if(!hname.Contains("Norm")) continue;
     if( hname.Contains("obs")) continue;
     if(!hname.Contains("Nom")) continue;

     // remove spourious histograms
     if( !TString( (*it).c_str() ).Contains( obsRegions[whichRegion( (*it),regions )])) continue ;
     
     string sample=whichSample((*it),samples,samples2);
     tfMap[*it]=make_pair("all",sample);
   }

   // print nominal norm tf nmaps
   for(std::map<string, std::pair<string,string>>::iterator it=tfMap.begin(); it!=tfMap.end() ;it++){
     cout<<(*it).first<<" region "<<(*it).second.first<<" sample "<<(*it).second.second<<endl;
   }
   cout<<endl;
   
   
   // fill in the systematics
   for(vector<string>::iterator it2=TH1FNamesNormTF.begin(); it2!=TH1FNamesNormTF.end(); it2++){

     TString hname(*it2);


     if(!hname.Contains("Norm")) continue;
     if( hname.Contains("obs")) continue;
     if(hname.Contains("Nom")) continue;

      // remove spourious histograms
     if( !TString( (*it2).c_str() ).Contains( obsRegions[whichRegion( (*it2),regions )])) continue ;
     
     string sample=whichSample( (*it2), samples,samples2);
     //cout <<"Sample is "<<sample<<endl;
     // find the corresponding nomial:
     string nominal="";
     for(std::map<string, std::pair<string,string>>::iterator it3=tfMap.begin(); it3!=tfMap.end(); it3++){
       string tsample=(*it3).second.second;
       if( sample.compare(tsample)==0) { nominal=(*it3).first; break; } 
     }
     if(nominal.size()==0) {cout<<"Sample not found" <<endl; continue; } 
     
     TString hnameUp(*it2);
     TString hnameDown( *it2);
     
     if( hnameUp.Contains("Low")) hnameUp.ReplaceAll("Low","High");
     if( hnameDown.Contains("High")) hnameDown.ReplaceAll("High","Low");
     tfSysNormMap[nominal].push_back(make_pair(hnameUp.Data(),hnameDown.Data()));
     //cout<<"Added uncertainty for "<<nominal<<" "<<hnameUp.Data()<<" <-> "<<hnameDown.Data()<<endl;
   }
   
  
  // now fill the systematic variations 
  for(vector<string>::iterator it=TH1FNamesNormRaw.begin(); it!=TH1FNamesNormRaw.end(); it++){
    TString hname(*it);

    // if this is the nominal file process 
    if(hname.Contains("Nom") || hname.Contains("Norm")) continue; 

    string upVar;
    string lowVar;

    string normUpVar;
    string normLowVar;

    TString hnameUp(*it);
    TString hnameDown( *it);

    if( hname.Contains("High_") ) {
      hnameDown.ReplaceAll("High_","Low_");
    }
    else if (hname.Contains("Low_")){
      hnameUp.ReplaceAll("Low_","High_");
    }
      
    //find the correponding norm histogram
    

    
    TString hnormNameUp=hnameUp;
    TString hnormNameDown=hnameDown;

    
    hnormNameUp=Form("%s%s",hnormNameUp.Data(),"Norm");
    hnormNameDown=Form("%s%s",hnormNameDown.Data(),"Norm");
    

    // find the nominal 
    string nomName;
    const string thisSample=whichSample((*it),samples,samples2);
    const string thisRegion=whichRegion((*it),regions);
    string nBsObs=(*it);
    const string thisObs=whichObs( nBsObs,  "obs_");
    cout<<"Info: sample "<<thisSample<<" Region: "<<thisRegion<<" observable: "<<thisObs<<" for "<<(*it)<<endl;

    if ( thisObs.compare(obsRegions[thisRegion])!=0) {
      cout<<"Unused observable for this region"<<endl;
      continue; 
    } 
    
    for( std::map<string, std::pair<string,string>>::iterator it2=nominalMap.begin(); it2!=nominalMap.end(); it2++){
      string nn=(*it2).first; 
      string nomObs=whichObs( nn ,"obs_");
      //cout<<" ----> "<<nn<<"  ---- " <<nomObs<<endl;
      if( thisRegion.compare( (*it2).second.first)==0 && thisSample.compare( (*it2).second.second) == 0 &&
          nomObs.compare( thisObs ) == 0 ){
        nomName=(*it2).first;
        break; 
      }
    }
    if(nomName.size()==0) continue; 

    // remove spourious histograms
    //if( !TString( (*it).c_str() ).Contains( obsRegions[whichRegion( (*it),regions )])) continue ;
    if(  thisRegion.compare(whichRegion(nomName,regions)) !=0 ||
         thisSample.compare(whichSample(nomName,samples,samples2))!=0 ||
         thisObs.compare(whichObs(nomName,"obs_"))!=0 )
      continue ;
    
    bool allreadyProcessed=false;
    vector<string> pvars=processedSys[nomName];
    for(vector<string>::iterator lp=pvars.begin(); lp!=pvars.end(); lp++){
      if ( (*lp).compare(hnameUp.Data())==0) {allreadyProcessed=true; break; }
    }

    if(allreadyProcessed) continue; 

    cout<<hnameUp.Data()<<" <---> "<<hnameDown.Data()<<" || "<<hnormNameUp.Data()<<" <---> "<<hnormNameDown.Data()<<endl;
    //GUY::HERE
    processedSys[nomName].push_back(hnameUp.Data()); 
    sysVarsMap[nomName].push_back(make_pair(hnameUp.Data(),hnameDown.Data()));
    sysVarsNormMap[nomName].push_back(make_pair(hnormNameUp.Data(),hnormNameDown.Data()));
    
    
    cout<<"Norm Name "<<nomName<<" has "<<sysVarsMap[nomName].size()<<" systematics for nom and for norm "<<endl;
  }


  // Peform smart rebbinning step
  // observables to rebin 
  // bdt_vbf
  std::map<string, vector<string>> hVecMap;
  std::cout<<"looping over hvec \n";
  for(vector<string>::iterator it=TH1FNames.begin(); it!=TH1FNames.end(); it++){
    std::cout<<*it<<" \n";
    /*
    TH1F *h=(TH1F*)f->Get( (*it).c_str());
    //    cout<<" Checkhing histogram "<<(*it)<<endl;
    if( h==nullptr) continue;
    // keep only the obervable ones 
    if( ( !(TString(h->GetName())).Contains("obs"))) continue; 
    // keep only the nomninals 
    if( !TString(h->GetName()).Contains("Nom")) continue ; 
    // Keep only the regions we care about
    if (!TString(h->GetName()).Contains("SRVBF")) continue; 
    // keep only distribution we want to look at 
    if( TString(h->GetName()).Contains("obs_bdt_vbf") && !TString(h->GetName()).Contains("obs_bdt_vbfggf")){
      const string region=whichRegion( h->GetName(),regions);
      hVecMap[region].push_back(h->GetName());
    }
    */

    string iname=(*it);
    std::cout<<"contains obs"<<(TString(iname.c_str() )).Contains("obs")<<"\n";
    std::cout<<"contains Nom"<<(TString(iname.c_str() )).Contains("Nom")<<"\n";
    std::cout<<"contains SRVBF"<<TString(iname.c_str()).Contains("SRVBF")<<"\n";
    std::cout<<"contains obs bdt vbf"<<TString(iname.c_str()).Contains("obs_bdt_vbf")<<"\n";
    // keep only the obervable ones 
    if( ( !(TString(iname.c_str() )).Contains("obs"))) continue; 
    // keep only the nomninals 
    if( !TString(iname.c_str()).Contains("Nom")) continue ; 
    // Keep only the regions we care about
    if (!TString(iname.c_str()).Contains("SRVBF")) continue; 
    // keep only distribution we want to look at 
    if( TString(iname.c_str()).Contains("obs_bdt_vbf") && !TString(iname.c_str()).Contains("obs_bdt_vbfggf")){
      const string region=whichRegion( iname,regions);
      std::cout<<"pushing region"<<region<<" with"<<iname<<"\n";
      hVecMap[region].push_back(iname);
    }
    
  }
  for(std::map<string,vector<string>>::iterator reg_map=hVecMap.begin(); reg_map!=hVecMap.end(); reg_map++){
    std::cout<<"region :"<<(*reg_map).first<<" histogram "<<(*reg_map).second[0]<<"\n";
  }
  // now loop over all regions to consider

  
  for(std::map<string, vector<string>>::iterator it= hVecMap.begin(); it!=hVecMap.end(); it++){
    // start from a base rebinning
    vector <double> xbins;
    xbins={0.5,0.7,0.86,0.94,1};
    

    // loop over all histograms
    for(std::vector<string>::iterator h2=(*it).second.begin(); h2!=(*it).second.end(); h2++){
      if(TString(*h2).Contains("SRVBF_0")&& (variable.compare("Mjj")==0))
        xbins={0.5,0.7,0.86,1};
  

      if( xbins.size()-1 < 5) {
        break ;
      }

      TH1F *hh=(TH1F*)f->Get( (*h2).c_str());
      double prebinned_integral=hh->Integral();
      //if(TString(hh->GetName()).Contains("SRVBF_0"))
      //xbins={0.5,0.7,0.86,1};
      if(hh==NULL)
  continue;
      cout<<"Rebinning region "<<(*it).first<<" h name "<< hh->GetName()<<" to "<<xbins.size()-1<<" bins "<<endl;
      vector <double> xbins2;
     

      TH1F *hRebin=(TH1F*)hh->Rebin(xbins.size()-1,Form("%s_%s",hh->GetName(),"TMPRBH"),&xbins[0]);
      xbins2.push_back(hRebin->GetBinLowEdge(1)); 
      for( int b=1; b<(int)hRebin->GetNbinsX()+1; b++){
        double val=hRebin->GetBinContent(b);
        if( val > 0 && ( fabs(hRebin->GetBinError(b) / hRebin->GetBinContent(b)) < 1.0 ) ){
          if( xbins2[0] != hRebin->GetBinLowEdge(b)){
            xbins2.push_back(hRebin->GetBinLowEdge(b));
          }}
        else
          continue; 
      }
      xbins2.push_back(1);
      
      if( xbins2.size() < xbins.size() && xbins2.size()>4) {
        xbins.clear();
        xbins=xbins2;
      }

      delete hRebin; 
    }
    
    xbinsMap[(*it).first]=xbins;
    cout<<"Edges :";
    for(int i=0; i<(int)xbins.size(); i++){
      cout<<" "<<xbins[i];
    }
    
    cout<<endl; 
    cout<<"Rebinning Region "<<(*it).first<<" has "<<xbins.size()<<" bins "<<endl;
  }  

  ofstream myfileBins;
  myfileBins.open(Form("%s_binning.txt",eraseSubStr(tname,".root").c_str()));
  for(std::map<string,vector<double>>::iterator it=xbinsMap.begin(); it!=xbinsMap.end(); it++){
    vector <double> xbins=(*it).second;
    myfileBins<<(*it).first<<" "<<xbins.size()-1;
      for(int k=0; k<(int)xbins.size(); k++)
        myfileBins<<" "<<xbins[k];
      myfileBins<<endl;
  }
  myfileBins.close();
  
  

  

  cout<<"Checking for empty bins  "<<endl;

// loop over all histograms
  for(vector<string>::iterator it=TH1FNames.begin(); it!=TH1FNames.end(); it++){
    //load the TH1F
    cout<<"loading "<<(*it).c_str()<<"\n";
    TH1F *h=(TH1F*)f->Get( (*it).c_str());
    cout<<"loading "<<h->GetName()<<"\n";
    if(h==NULL)
      continue;
    TString tnamed_hist=TString((*it));
    if(!tnamed_hist.Contains("theo") && tnamed_hist.Contains("SRVBF") && tnamed_hist.Contains("hZjets")){
      smooth_histogram(h,5.5);
    } if(!tnamed_hist.Contains("theo") && tnamed_hist.Contains("CRTop") && tnamed_hist.Contains("hZjets")){
      smooth_histogram(h,3.0);
    }


    if(tnamed_hist.Contains("theo")&&!tnamed_hist.Contains("artificial")){
      string name=std::string(h->GetName());
      string sample=whichSample(name,samples,samples2);
      string region=whichRegion(name,regions);;
      string smoothed_nom_name=sample+"Nom_"+region+"_obs_"+obsRegions[region];
      cout<<"smoothed nominal name "<<smoothed_nom_name<<"\n";
      fix_theory_histogram(f,smoothed_nom_name,h);
    }else if(!tnamed_hist.Contains("theo") && tnamed_hist.Contains("CRZjets")){
      h->Rebin(2);
    }




    std::cout<<"loading "<<h->GetName()<<"\n";
    bool overwriten=false; 

    //    cout<<" Checkhing histogram "<<(*it)<<endl;
    if( h==nullptr) continue;
    //if( (TString(h->GetName())).Contains("Norm") && !(TString(h->GetName())).Contains("obs")) continue; 
    if( ( !(TString(h->GetName())).Contains("obs"))) continue; 
    
    bool HasEmptyBins=false;
    if( TString(h->GetName()).Contains("Nom") && h->Integral()!=0){
      for(int b=-1; b<(int)h->GetNbinsX()+2; b++){
        // remove any NaN
        double xx=h->GetBinContent(b);
        if( xx != xx) h->SetBinContent(b,0); 
      }
    }
    else {
      for(int b=-1; b<(int)h->GetNbinsX()+2; b++){
        double xx=h->GetBinContent(b);
        if( xx != xx ||  h->GetBinContent(b)==0 )
          h->SetBinContent(b,0); 
      }
    }
    //h->Smooth(1,"R")
    
    bool Smooth=false; 

    /*
    if(TString(h->GetName()).Contains("htop") && TString(h->GetName()).Contains("SRVBF") && TString(h->GetName()).Contains("theo") )
      Smooth=true; 
    if(TString(h->GetName()).Contains("hhtt"))
      Smooth=true;
    if(TString(h->GetName()).Contains("hvh"))
      Smooth=true;
    if(TString(h->GetName()).Contains("hvbf0") && !TString(h->GetName()).Contains("SRVBF"))
      Smooth=true;
    if( (TString(h->GetName()).Contains("hZjets") || TString(h->GetName()).Contains("hdiboson") )  && TString(h->GetName()).Contains("theo") && (TString(h->GetName()).Contains("SRVBF") || TString(h->GetName()).Contains("CRTop")))
      Smooth=true;
    //if(TString(h->GetName()).Contains("hdiboson") && TString(h->GetName()).Contains("theo"))
    //Smooth=true;
    if(TString(h->GetName()).Contains("hVgamma"))
      Smooth=true;
    */

    if(Smooth){
      //double lowRange=h->GetBinLowEdge(1);
      //double highRange=h->GetBinLowEdge(h->GetNbinsX()-3);
      //h->GetXaxis()->SetRangeUser(lowRange,highRange);
      //h->Smooth(1,"R");
      //h->GetXaxis()->SetRangeUser(h->GetBinLowEdge(1),h->GetBinLowEdge(h->GetNbinsX()+1));
      h->Smooth(1);
    }


    string sample=whichSample(h->GetName(),samples,samples2);
    string region=whichRegion((*it),regions);
    
    double pre_rebinned_integral=h->Integral();

    if(TString(h->GetName()).Contains("CRGGF3") ) {
      if(h->GetNbinsX()==8){
  h->Rebin(2);
      }
    }if(TString(h->GetName()).Contains("CRTop") && (variable.compare("Mjj")==0 || variable.compare("pt_H")==0 || variable.compare("Mll")==0 || variable.compare("DPhill")==0 || variable.compare("jet0_pt")==0 || variable.compare("lep0_pt")==0 || variable.compare("jet1_pt")==0 || variable.compare("lep1_pt")==0 || variable.compare("costhetastar")==0) ) {
      h->Rebin(2);
    }

    //if(TString(h->GetName()).Contains("CRTop_") ) {
    //  h->Rebin(2);
    // }


    // if(TString(h->GetName()).Contains("CRTop_3") || TString(h->GetName()).Contains("CRTop_2") || TString(h->GetName()).Contains("CRTop_1")){
    //  h->Rebin(2);
    // }
    // Rebin the vbf signal region histograms 
    if(TString(h->GetName()).Contains("obs_bdt_vbf") && !TString(h->GetName()).Contains("obs_bdt_vbfggf")){
      vector <double> xbins;
      cout<<"rebinning\n";
      xbins={0.5,0.7,0.86,0.94,1.0};


      //vector <double> xbins={0.5,0.7,0.86,0.88,0.90,0.92,0.94,0.96,0.98,1};
      if(TString(region).Contains("SRVBF_0")&& (variable.compare("Mjj")==0) ) xbins={ 0.5,0.7,0.86,1.0};

      int nBins=xbins.size()-1;
      const string region=whichRegion(h->GetName(),regions);
      if(isDiff){
        xbins.clear();
        cout<<"getting n bins\n";
        //xbins={0.5,0.7,0.86,0.96,1};
        xbins=xbinsMap[region]; 
        //std::cout<<"total number of bins !!!!!!!!!!!!!"<<xbins.size()<<"\n";
        nBins=xbins.size()-1;
      }

      string oldName=h->GetName();
      h->SetName(Form("%s_UnRB",h->GetName()));
      //std::cout<<"rebinning histogram "<<oldName<<"\n";
      TH1F *hrwb=(TH1F*)h->Rebin(nBins,(oldName+"_RBR").c_str(),&xbins[0]);
      
      // define the remmapped histogram
      TH1F *hnew=new TH1F(oldName.c_str(),oldName.c_str(),nBins,0.5,1);
      hnew->GetXaxis()->SetTitle("#it{D}_{VBF}");
      hnew->GetYaxis()->SetTitle(h->GetYaxis()->GetTitle());
      for( int i=0; i<(int)hrwb->GetNbinsX()+1; i++){
        if(hrwb->GetBinContent(i) > 0)
          hnew->SetBinContent(i,hrwb->GetBinContent(i));
        else{
          hnew->SetBinContent(i,0.0001);
          hrwb->SetBinContent(i,0.0001);
        }
        hnew->SetBinError(i,hrwb->GetBinError(i));
      }
      
      if( TString(hnew->GetName()).Contains("Nom") && !TString(hnew->GetName()).Contains("Norm"))
        //sysNomZeroDoNotCheck[oldName]=true;
        sysNomZeroDoNotCheck[oldName]= hnew->Integral() > 0 ? true:false; 
      cout<<"writing rebinned thing\n";
      hnew->Scale(pre_rebinned_integral/hnew->Integral());
      hrwb->Scale(pre_rebinned_integral/hrwb->Integral());
      hnew->Write();
      delete hnew;

      hrwb->Write();
      delete hrwb;
      cout<<overwriten<<" "<<h->GetName()<<"\n";
      overwriten=true;
      cout<<overwriten<<" "<<h->GetName()<<"\n";
    }
    
    if (HasEmptyBins)
      cout<<" histogram "<<h->GetName()<<" has empty bins "<<endl;
      cout<<"about to write h\n";
      cout<<overwriten<<" "<<h->GetName()<<"\n";
    if(!overwriten){
      cout<<h<<"\n";
      h->Write();
      if( TString(h->GetName()).Contains("Nom") && !TString(h->GetName()).Contains("Norm")){
        //sysNomZeroDoNotCheck[h->GetName()]=true;
        cout<<"doing some weird check on nominal\n";
        sysNomZeroDoNotCheck[h->GetName()]= h->Integral() > 0 ? true:false; 
      }
    }
    delete h; 

  }

  cout<<"closing file\n";


  fnew->Close();
  cout<<"deleting fnew\n";
  delete fnew;
  f=TFile::Open(Form("%s.1.root",tname.c_str()));
  fnew=new TFile(Form("%s.2.root",tname.c_str()),"RECREATE");



  cout<<"Starting the loop over systematics now "<<endl;
  cout<<"Make clones of the raw Histograms for all systematics"<<endl;
  
  for( std::map<string, std::vector<std::pair<string,string>>>::iterator it=sysVarsMap.begin(); it!=sysVarsMap.end(); it++){
    TH1F *hnom=(TH1F*)f->Get( (*it).first.c_str());

    if(hnom==nullptr || hnom->Integral()==0) { 
      cout<<"nominal or empy histogram "<<(*it).first<<" is a null pointer "<<endl;
      if( hnom!=nullptr) { hnom->Write(); delete hnom;} 
      continue;
    }

    int nRebin=2;
    while ( !(hnom->GetNbinsX() % nRebin == 0) )
        nRebin++;
    cout<<"-- nRebinning will be "<<nRebin<<" for "<<hnom->GetNbinsX()<<endl;
    
    //if( sysNomZeroDoNotCheck[ (*it).first ]){
    vector <std::pair<string, string>> vars=(*it).second;

    for( vector<std::pair<string, string>>::iterator it2=vars.begin(); it2!=vars.end(); it2++){
     

      TH1F *hup=nullptr;
      hup=(TH1F*)f->Get((*it2).first.c_str());
      if(hup ==nullptr) cout<<(*it2).first<<" is null "<<endl; 
      TH1F *hdown=nullptr;
      hdown=(TH1F*)f->Get((*it2).second.c_str());
      if(hdown ==nullptr) cout<<(*it2).second<<" is null "<<endl; 
      // chechk if up and down variations exist 
      
      if( hup==nullptr && hdown!=nullptr) {
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        cout<<"Hup did not exist replacing it by downvariation for"<<(*it2).first<<endl;
      }
      
      else if ( hup!=nullptr && hdown==nullptr){
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"Hdown does not exist replacing it bu upvariation for "<<(*it2).second<<endl;
      }
      else if (hup==nullptr && hdown==nullptr){
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"Norm Both variations do not exist, created ad hoc variation"<<endl;
      }
      if(TString(hup->GetName()).Contains("tautau_generator")){
  chi_file<<"line 1732\n";
        chi_file<<hup->GetName()<<"\n";
        chi_file<<hdown->GetName()<<"\n";
        for(int i=0;i<=hnom->GetNbinsX();i++){
          chi_file<<hup->GetBinContent(i)<<" "<<hdown->GetBinContent(i)<<"\n";
        }
        chi_file<<"-------------------------------------------------------\n";
      }



      // make some fixes that need to be propaged to both 
      for(int bin=-1; bin<(int)hnom->GetNbinsX()+2; bin++){
        double nomVal=hnom->GetBinContent(bin);
        double highVal=hup->GetBinContent(bin);
        double lowVal=hdown->GetBinContent(bin);

        if( nomVal !=nomVal || nomVal==0){
          nomVal=0;
          highVal=0;
          lowVal=0;
        }
        else {
          if( highVal != highVal) { highVal=nomVal; }
          if( lowVal != lowVal) { lowVal=nomVal; }
        }
        //GUY edit
        hdown->SetBinContent(bin,lowVal);
        hup->SetBinContent(bin,highVal);
      }

      hdown->Write();
      hup->Write();
      
      TH1F *hrup=(TH1F*)hup->Clone(Form("%s%s",(*it2).first.c_str(),"_RAW"));
      TH1F *hrdown=(TH1F*)hdown->Clone(Form("%s%s",(*it2).second.c_str(),"_RAW"));

      TH1F *hrupRB=(TH1F*)hup->Clone(Form("%s%s",(*it2).first.c_str(),"_RB"));
      hrupRB->Rebin(nRebin);

      TH1F *hdownRB=(TH1F*)hdown->Clone(Form("%s%s",(*it2).second.c_str(),"_RB"));
      hdownRB->Rebin(nRebin);
            
      hrup->Write();
      delete hrup;
      delete hup;

      hrdown->Write();
      delete hrdown;
      delete hdown;

      hrupRB->Write();
      delete hrupRB;
      
      hdownRB->Write();
      delete hdownRB;
      
    }
    //}
    //else {
    //for( int lk=-1;lk<hnom->GetNbinsX()+2;lk++){
    //  hnom->SetBinContent(lk,0);
    //}
    //}

    
    hnom->Write();

    TH1F *hrNom=(TH1F*)hnom->Clone(Form("%s%s",(*it).first.c_str(),"_RAW"));
    hrNom->Write();

    TH1F *hrNomRB=(TH1F*)hnom->Clone(Form("%s%s",(*it).first.c_str(),"_RB"));
    hrNomRB->Rebin(nRebin);
    hrNomRB->Write();

    delete hrNomRB;
    delete hrNom;
    delete hnom;
  }

  f->Close();

  delete f;
  //f=new TFile(tname.c_str(),"UPDATE");
  fnew->Close();
  delete fnew;
  f=TFile::Open(Form("%s.2.root",tname.c_str()));
  fnew=new TFile(Form("%s.3.root",tname.c_str()),"RECREATE");
  
  

  // Smoothing and else
  //const string sample=whichSample( h->GetName() ,samples,samples2); 
  //const string region=whichRegion( (*it).first,regions);
  //string variation= whichVariation( h->GetName() ,sample,region);
  unordered_set <string> nomSet;
  for( std::map<string, std::vector<std::pair<string,string>>>::iterator it=sysVarsMap.begin(); it!=sysVarsMap.end(); it++){

    cout<<endl;
    cout<<" Iterating on "<<(*it).first<<endl;
    const string sample=whichSample( (*it).first,samples,samples2); 
    const string region=whichRegion( (*it).first,regions);
    string nnName=(*it).first;
    const string thisObs=whichObs( nnName , "obs_");
    vector <string> nnormRegions;
    bool hasAbnormalValues=false; 

    TH1F *hnom=(TH1F*)f->Get( (*it).first.c_str());
    TH1F *hnomRB=(TH1F*)f->Get(Form("%s_RB",(*it).first.c_str()));

    /*
      if(TString(region).Contains("CRGGF3")){
  smooth_histogram(hnom,0.3);
      }
    */
    if(hnom==nullptr || hnom->Integral()==0) { 
      cout<<"nominal or empy histogram "<<(*it).first<<" is a null pointer "<<endl;
      if( hnom!=nullptr) { hnom->Write(); delete hnom;} 

      continue;
    }
   
    TString nominal_name(hnom->GetName());  

    vector <std::pair<string, string>> vars=(*it).second;
   
    for( vector<std::pair<string, string>>::iterator it2=vars.begin(); it2!=vars.end(); it2++){
      
      string variation= whichVariation( (*it2).second ,sample,region);
      cout<<"Variation is "<<variation<<endl;
      cout<<"   with histograms    "<<(*it2).first<<" up "<<(*it2).second<<" down  "<<(*it).first<<" nominal "<<endl;
      if(variation.compare("unknown")==0){
        cout<<"       Unkown variation  .. skipping "<<endl;
        continue ;
      }
      cout<<"Sample is: "<<sample;

      TH1F *hup=nullptr;
      hup=(TH1F*)f->Get((*it2).first.c_str());
      TH1F *hupRB=nullptr;
      hupRB=(TH1F*)f->Get(Form("%s_RB",(*it2).first.c_str()));
      
      TH1F *hdown=nullptr;
      hdown=(TH1F*)f->Get((*it2).second.c_str());
      TH1F *hdownRB=nullptr;
      hdownRB=(TH1F*)f->Get(Form("%s_RB",(*it2).second.c_str()));
      
      
      // chechk if up and down variations exist 
      if( hup==nullptr && hdown!=nullptr) {
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        cout<<"       Hup did not exist replacing it by downvariation for"<<(*it2).first<<endl;
      }
      
      else if ( hup!=nullptr && hdown==nullptr){
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"       Hdown does not exist replacing it bu upvariation for "<<(*it2).second<<endl;
      }

      else if (hup==nullptr && hdown==nullptr){
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"       Norm Both variations do not exist, created ad hoc variation"<<endl;
      }
      
      if(hup->Integral()==0 || hnom->Integral()==0) {
        delete hup;
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        cout<<"       up variation had 0 integral replacing it with nonminal"<<endl;
      }
      
      if(hdown->Integral()==0 || hnom->Integral()==0){
        delete hdown;
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"       down variation had 0 integral replacing it with nonminal"<<endl;
      }
      if(TString(variation).Contains("tautau_generator")){
  chi_file<<"line 1909 \n";
        chi_file<<hup->GetName()<<"\n";
        chi_file<<hdown->GetName()<<"\n";
        for(int i=0;i<=hnom->GetNbinsX();i++){
          chi_file<<hup->GetBinContent(i)<<" "<<hdown->GetBinContent(i)<<"\n";
        }
        chi_file<<"-------------------------------------------------------\n";
      }


      if( sysNomZeroDoNotCheck[ (*it).first ]){

  /*
        if( TString(variation).Contains("ttbar_matching") || TString(variation).Contains("ztautau_generator")  ){// ||nominal_name.Contains("CRGGF2")){                                                                                                                                                                                                                                                                                 
          double integralup_before=hup->Integral();
          double integraldown_before=hdown->Integral();
          smooth_histogram(hup,hnom,0.75);
          smooth_histogram(hdown,hnom,0.75);
          if(integralup_before >20)
      hup->Scale(integralup_before/hup->Integral());
    if(integraldown_before > 20)
      hdown->Scale(integraldown_before/hdown->Integral());

        }
  */

        // now loop over the nominal sample bins 
        if(hup!=nullptr && hdown!=nullptr && hnom!=nullptr){
        
        cout<<"Histogram names "<<hnom->GetName()<<" upVar "<<hup->GetName()<<" -- down "<<hdown->GetName()<<endl;
        cout<<"Input integrals: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<" "<<hnom->Integral()<<" nominal "<<endl;
        
        double integralup=hup->Integral();
        double integraldown=hdown->Integral();
  bool symmetrise=false;
  //Potential problem:: here Guy::
  /*
  if(TString(variation.c_str()).Contains("theo")){
    fix_theory_histogram(f,hnom,hup);
    fix_theory_histogram(f,hnom,hdown);
  }
  
  fnew->cd();
  */
        // symmetrise if normalisation uncertaintiy is lower than 1per mille 
        if( fabs(1- hup->Integral()/hnom->Integral()) < 0.001 || fabs(1- hdown->Integral()/hnom->Integral()) < 0.001)
          symmetrise=true; 


        // symmetrize also if one side is larger than the other by more than 50%
        if( fabs(fabs(1- hup->Integral()/hnom->Integral())  - fabs(1- hdown->Integral()/hnom->Integral())) > 0.5)
          symmetrise=true;
  // GUY EDit

       

        if(hnom->Integral() > 0){

    for(int bin=1; bin<(int)hnom->GetNbinsX()+1; bin++){

      double nomVal=hnom->GetBinContent(bin);
      double highVal=hup->GetBinContent(bin);
      double lowVal=hdown->GetBinContent(bin);
          
          
          if( nomVal==0 || nomVal !=nomVal ){
            nomVal=0;
            highVal=0;
            lowVal=0;
          }
          
          cout<<"    Original values: Bin "<<bin<<" nom "<<nomVal<<" high "<<highVal<<" low "<<lowVal<<endl;

          double nomValRB=hnomRB->GetBinContent(hnomRB->FindBin(hnom->GetBinCenter(bin)));
          double highValRB=hupRB->GetBinContent(hnomRB->FindBin(hnom->GetBinCenter(bin)));
          double lowValRB=hdownRB->GetBinContent(hnomRB->FindBin(hnom->GetBinCenter(bin)));

          //double nomValRB=hnom->GetBinContent(bin);
          //double highValRB=hup->GetBinContent(bin);
          //double lowValRB=hdown->GetBinContent(bin)
          
          if( nomValRB==0 || nomValRB !=nomValRB ){
            nomValRB=0;
            highValRB=0;
            lowValRB=0;
           }

          
          // Use Rebinned Values for certain variations:
          // Zjets theory
          bool useRebin=false;
          bool useAverage=false; 
          // conditions for when to use the rebinned vlaues
          
          if( !RooKeysInput ){
            //if( (sample.compare("hZjets0")==0 || sample.compare("hZjets1")==0) && TString(variation.c_str()).Contains("theo"))
            //if( !TString( hnom->GetName() ).Contains("obs_MT") && !TString( hnom->GetName() ).Contains("obs_bdt_vbf"))
            //useRebin=true;
            
            //if( TString(variation.c_str()).Contains("theo") && TString( hnom->GetName() ).Contains("obs_bdt") && !TString( hnom->GetName() ).Contains("obs_bdt_vbf"))
            // useRebin=true;
          }
          
          //if( TString(variation.c_str()).Contains("JET"))
          //{ useRebin=true; useAverage=true; } 
          
          //determine if the variation is higher by the average variations excluding this bin, if yes user the rebin
          double lhighVal=0;
          double llowVal=0;

          double RMShigh=0;
          double RMSLow=0;

    if(TString(variation.c_str()).Contains("theo")){
      useAverage=true;
    }
          for(int lbin=1; lbin<(int)hnom->GetNbinsX()+1; lbin++){
            if(lbin==bin) continue; 
              lhighVal+=fabs(1-hup->GetBinContent(lbin)/hnom->GetBinContent(lbin));
              llowVal+=fabs(1-hdown->GetBinContent(lbin)/hnom->GetBinContent(lbin));
          }
          
          lhighVal=lhighVal/(hnom->GetNbinsX()-1);
          llowVal=llowVal/(hnom->GetNbinsX()-1);
          
          
          for(int lbin=1; lbin<(int)hnom->GetNbinsX()+1; lbin++){
            if(lbin==bin) continue;
            //cout<<"RMSLow "<<RMSLow<<" RMSHigh" <<RMShigh<<endl;
            RMShigh+=pow(  lhighVal   -   1-hup->GetBinContent(lbin)/hnom->GetBinContent(lbin),2);
            RMSLow+=pow(   llowVal    -   1-hdown->GetBinContent(lbin)/hnom->GetBinContent(lbin),2);
            
          }
          //cout<<"RMSLow "<<RMSLow<<" RMSHigh" <<RMShigh<<endl; 
          
          
          RMSLow=sqrt( 1  / (hnom->GetNbinsX()-1) * RMSLow  );
          RMShigh=sqrt( 1 / (hnom->GetNbinsX()-1) * RMShigh );
          
          //cout<<" RMS "<<lhighVal <<" highVal "<<1-highVal/nomVal<<" RMS high " <<RMShigh<<" ----  llow "
          //  <<llowVal <<" highVal "<<1-lowVal/nomVal<<" RMS high " <<RMSLow<<endl;
          
          if( fabs( lhighVal  -  1-highVal/nomVal)  / RMShigh  > 2.5 || fabs( llowVal  -  1-lowVal/nomVal)  / RMSLow  > 2.5 ){
            cout<<" RMS "<<lhighVal <<" highVal "<<1-highVal/nomVal<<" RMS high " <<RMShigh<<" ----  llow "
                <<llowVal <<" highVal "<<1-lowVal/nomVal<<" RMS high " <<RMSLow<<endl;
            cout<<"Uncertainty higher than 1.5 sigma of the average using rebinned values lhigh "<<lhighVal/(hnom->GetNbinsX()-1)<<" highVal "<<highVal/nomVal<<" RMS high " <<RMShigh<<" ----  llow "
                <<llowVal/(hnom->GetNbinsX()-1)<<" highVal "<<lowVal/nomVal<<" RMS high " <<RMSLow<<endl;
            useAverage=true;
          }
          
          // !!! ATTENTION 
          //useRebin=false;
          //useAverage=false;
            
          if( useRebin){
            //cout<<"Using rebinned high :" << (nomValRB - highValRB)/nomValRB<< " * "<<nomVal<<" --> "<<(nomValRB - highValRB)/nomValRB * nomVal<<endl;
            //cout<<"Using rebinned low :" << (nomValRB - lowValRB)/nomValRB<< " * "<<nomVal<<" --> "<<(nomValRB - lowValRB)/nomValRB * nomVal<<endl;

            highVal= (nomValRB - highValRB)/nomValRB * nomVal;
            lowVal =  (nomValRB - lowValRB)/nomValRB * nomVal ;
          }

          if(useAverage) {
            cout<<" Setting to average error high "<< (1 - lhighVal) << " * "<<nomVal<<endl;
            cout<<" Setting to average error low "<<  (1 - llowVal) << " * "<<nomVal<<endl;
            double direction= 0; 
            double directionBinLow = hdown->GetBinContent(bin -1) > hnom->GetBinContent(bin-1)  ? 1:-1;
            double directionBinHigh= hup->GetBinContent(bin+1) > hnom->GetBinContent(bin+1) ? 1:-1; 
      if(hnom->GetBinContent(bin-1)+hnom->GetBinContent(bin+1)==0)
        direction=1;
      else
        direction= 1/(hnom->GetBinContent(bin-1)+hnom->GetBinContent(bin+1)) * (directionBinLow*hnom->GetBinContent(bin-1) + directionBinHigh*hnom->GetBinContent(bin+1)); 
            //cout<<"Directions up: "<<directionBinHigh<<" down: "<<directionBinLow<<" average direction "<<direction<<endl;
            
            //highVal= nomVal  + direction*fabs(lhighVal)* nomVal;
            //lowVal = nomVal  - direction*fabs(llowVal)* nomVal;
            
            highVal= nomVal  + direction*0.5*fabs(highVal-lowVal);
            lowVal = nomVal  - direction*0.5*fabs(highVal-lowVal);
          }
        
         

          bool symmetriseBin=false;

          //if( fabs( 1-highVal/nomVal) > 0.3 || fabs(1-lowVal/nomVal) > 0.3) 
          //symmetriseBin=true; 
          
          //both variations go into the same direction, symmetrise 
          if( ( 1-highVal/nomVal > 0 && 1 - lowVal/nomVal > 0) ||  (1-highVal/nomVal < 0 && 1 - lowVal/nomVal <0))
            symmetriseBin=true; 

          if( (fabs( 1-highVal/nomVal) > 0.3 || fabs(1-lowVal/nomVal) > 0.3) && (( 1-highVal/nomVal > 0 && 1 - lowVal/nomVal > 0) ||  (1-highVal/nomVal < 0 && 1 - lowVal/nomVal <0)))
            symmetriseBin=true;
          //GUY edit

    if(TString(variation.c_str()).Contains("theo")){
       symmetrise=true;

    }

          // fix one sided variations GUY EDIT
          if(  false && (symmetriseBin || symmetrise)  ) {
            cout<<"Both variations go into the same direction symmetrising bins"<<endl;
            //double symmUncert=0.5*fabs(  fabs(1-highVal/nomVal)  + fabs(1-lowVal/nomVal));
            double symmUncert=0.5*fabs( highVal - lowVal )/nomVal ;
      if(nomVal==0)
        symmUncert=0;
            cout<<" Symm rel uncertainty "<<symmUncert<<endl;

            double direction= 0; 
            double directionBinLow = hdown->GetBinContent(bin -1) > hnom->GetBinContent(bin-1)  ? 1:-1;
            double directionBinHigh= hup->GetBinContent(bin+1) > hnom->GetBinContent(bin+1) ? 1:-1; 
      if((hnom->GetBinContent(bin-1)+hnom->GetBinContent(bin+1)) ==0)
        direction=1;
      else
        direction= 1/(hnom->GetBinContent(bin-1)+hnom->GetBinContent(bin+1)) * (directionBinLow*hnom->GetBinContent(bin-1) + directionBinHigh*hnom->GetBinContent(bin+1)); 
            //cout<<"Directions up: "<<directionBinHigh<<" down: "<<directionBinLow<<" average direction "<<direction<<endl;
      if(direction >0) direction=1;
      else direction =-1;
            highVal=nomVal + direction*symmUncert*nomVal;
            lowVal=nomVal -  direction*symmUncert*nomVal; 

          }

          if(highVal<0) { highVal=0.0+0.005*nomVal; }
          if(lowVal < 0) { lowVal=0.0+0.005*nomVal; }
          //GUY EDIT
        
          if((!TString(variation).Contains("MET_SoftTr")&& !TString(variation).Contains("MUON_SCALE") )&& false && (symmetriseBin || symmetrise || useAverage || useRebin)){
            // change only of the new value is smaller than the old one
            //if( fabs(1 - hdown->GetBinContent(bin)/hnom->GetBinContent(bin)) > fabs(1- lowVal/hnom->GetBinContent(bin)))
      hdown->SetBinContent(bin,lowVal);

        //if( fabs(1 - hup->GetBinContent(bin)/hnom->GetBinContent(bin)) > fabs(1- highVal/hnom->GetBinContent(bin)))
      hup->SetBinContent(bin,highVal);


            //cout<<"    New val Bin "<<bin<<" nom "<<nomVal<<" high "<<highVal<<" low "<<lowVal<<endl;
          }
    

          // avoid negative pdfs at 1 simga of the uncertainty 
    //GUY EDIT
          //if( hup->GetBinContent(bin) < 0 ) { hup->SetBinContent(bin,0+0.005*hnom->GetBinContent(bin)); }
          //if( hdown->GetBinContent(bin) < 0) { hdown->SetBinContent(bin,0+0.005*hnom->GetBinContent(bin)); } 

          
          
        }// end loop on bins 
    if(TString(variation).Contains("tautau_generator")){
      chi_file<<"line 2160 \n";
      chi_file<<hup->GetName()<<"\n";
      chi_file<<hdown->GetName()<<"\n";
      for(int i=0;i<=hnom->GetNbinsX();i++){
        chi_file<<hup->GetBinContent(i)<<" "<<hdown->GetBinContent(i)<<"\n";
      }
      chi_file<<"-------------------------------------------------------\n";
    }
  
        cout<<"    Integrals after fluctuations replacement: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;
        cout<<"    Normalisation uncertainty : "<<hnom->Integral()<<" up: "<<(1-hup->Integral()/hnom->Integral())*100 <<" %  down: "<<(1-hdown->Integral()/hnom->Integral())*100<<endl;
  }
  }
        
        hdown->Write();
        hup->Write();

        TH1F *hupC=(TH1F*)hup->Clone(Form("%s%s",hup->GetName(),"_RAW2"));
        TH1F *hdownC=(TH1F*)hdown->Clone(Form("%s%s",hdown->GetName(),"_RAW2"));
        hupC->Write();
        delete hupC;
        hdownC->Write();
        delete hdownC; 
        
        TH1F *hupD=(TH1F*)f->Get(Form("%s%s",hup->GetName(),"_RAW"));
        hupD->Write();
        delete hupD;

        TH1F *hdownD=(TH1F*)f->Get(Form("%s%s",hdown->GetName(),"_RAW"));
        hdownD->Write();
        delete hdownD;
        
        // write there that this sample is affected by this systematic 
        if(hdown!=nullptr)delete hdown;
        if(hup!=nullptr) delete hup;
        hdownRB->Write();
        hupRB->Write();
        delete hdownRB;
        delete hupRB;
      }
    }
    


    TH1F *hnomC=(TH1F*)hnom->Clone(Form("%s%s",hnom->GetName(),"_RAW2"));
    //hnom->SetName(Form("%s_Orig",hnom->GetName()));
    hnom->Write();
    hnomC->Write();
   delete hnom;
    delete hnomC; 
    //hnomRB->Write();
    //delete hnomRB;

    //TH1F *hnomD=(TH1F*)f->Get(Form("%s%s",hnom->GetName(),"_RAW"));
    //hnomD->Write();
    //delete hnomD;
  }

  f->Close();
  delete f;
  //f=new TFile(tname.c_str(),"UPDATE");
  fnew->Close();
  delete fnew;
  f=TFile::Open(Form("%s.3.root",tname.c_str()));
  fnew=new TFile(Form("%s.4.root",tname.c_str()),"RECREATE");

  /*
  // add all the relevant nominal normalisations
  for(std::map<string, vector<string>>::iterator it=sampleNormMap.begin(); it!=sampleNormMap.end(); it++){
    double integral=0;
    string sample=(*it).first;
    vector<string> nnormRegions=(*it).second;

    for(vector<string>::iterator lm=nnormRegions.begin(); lm!=nnormRegions.end(); lm++){
      string nomHisCR=sample+"Nom_"+ (*lm) + "_obs_"+obsRegions[ (*lm)];
      cout<<"!!!!! "<<nomHisCR<<" from file "<<f->GetName()<<endl;
      //TH1F *hnomHisCR=(TH1F*)f->Get(Form("%s%s",nomHisCR.c_str(),"_RAW2"));
      TH1F *hnomHisCR=(TH1F*)f->Get(Form("%s",nomHisCR.c_str()));
      integral+=hnomHisCR->Integral();
    }
    // here add a norminal normalised histogram
    string iname=(*it).first;
    iname+="Nom";
    iname+="_";
    for(int ik=0; ik<(int)nnormRegions.size(); ik++)
      iname+=nnormRegions.at(ik);
    iname+="Norm";
    TH1F *hhnom=new TH1F(iname.c_str(),"",1,0.5,1);
    hhnom->SetBinContent(1,integral);
    //hhnom->Write();
    delete hhnom;
  }*/
  
 
  // Now loop again over all systematics for normalisation 
  //std::map<string, std::vector<std::pair<string,string>>> sysVarsMap;

  for( std::map<string, std::vector<std::pair<string,string>>>::iterator it=sysVarsMap.begin(); it!=sysVarsMap.end(); it++){

    cout<<endl;
    cout<<" Iterating on "<<(*it).first<<endl;
    const string sample=whichSample( (*it).first,samples,samples2); 
    const string region=whichRegion( (*it).first,regions);
    string nnName=(*it).first;
    const string thisObs=whichObs( nnName , "obs_");
    vector <string> nnormRegions;
    bool hasAbnormalValues=false; 
    if(sample.compare("")==0)
      continue;
    TH1F *hnom=(TH1F*)f->Get( (*it).first.c_str());
    //TH1F *hnomRB=(TH1F*)f->Get(Form("%s_RB",(*it).first.c_str()));
    if(TString(hnom->GetName()).Contains("hZjets1")){
      chi_file<<"line 2272 \n";
      chi_file<<hnom->GetName()<<"\n";
      for(int i=0;i<=hnom->GetNbinsX();i++){
  chi_file<<hnom->GetBinContent(i)<<"\n";
      }
      chi_file<<"-------------------------------------------------------\n";
    }

    if(hnom==nullptr || hnom->Integral()==0) { 
      cout<<"nominal or empy histogram "<<(*it).first<<" is a null pointer "<<endl;
      if( hnom!=nullptr) { hnom->Write(); delete hnom;} 
      continue;
    }
    
    // determine if sample is to be normalised to their TF
    bool toNorm=false;
    bool doggfABCD=false;
    for(std::map<string,vector<string>>::iterator il=sampleNormMap.begin(); il!=sampleNormMap.end(); il++){
      //cout<<"------->Comparing "<<(*il).first<<" to "<<sample<<endl;
      if( (*il).first.compare("hZjets0") == 0){
  toNorm=true;
  break;
      }
      if( (*il).first.compare(sample) == 0  ) { 
  if(std::find(sampleNormMap[sample].begin(), sampleNormMap[sample].end(), region) != sampleNormMap[sample].end()){
    toNorm=true; break; 
  }
      }
    }
  
    cout<<"Sample is: "<<sample;
    
    if(toNorm){
      //guy edit
      if((TString(region).Contains("CRTop")||TString(region).Contains("SRVBF"))&&(sample.compare("htop")==0 || sample.compare("hdiboson")==0) && isDiff){
  std::cout<<" region "<<region<<"\n";
  int bin= split(region);
  std::cout<<"found bin "<<bin<<"\n";
  std::cout<<"using "<<sampleNormMap[sample][bin]<<"\n";
  std::cout<<"and "<<sampleNormMap[sample][bin+nBinsDiff]<<"\n"; 
  for(auto &reg :CRTop_Norms[variable][bin]){
   
    nnormRegions.push_back(sampleNormMap[sample][reg]);
    nnormRegions.push_back(sampleNormMap[sample][reg+nBinsDiff]);
  }
      //nnormRegions={sampleNormMap[sample][bin],sampleNormMap[sample][bin+nBinsDiff]};
      }
      // else if(TString(region).Contains("SRVBF")&&(sample.find("hggf") != std::string::npos)){
      //  nnormRegions={"CRGGF1","CRGGF2","CRGGF3"};
      //  doggfABCD=true;

      //}
      else{
  nnormRegions=sampleNormMap[sample];
      }
      /*
      if( sample.compare("htop")!=0 && sample.compare("hdiboson")!=0)
        nnormRegions=sampleNormMap[sample];
      else {
        for(vector<string>::iterator lop=sampleNormMap[sample].begin(); lop!=sampleNormMap[sample].end(); lop++){
          if ( region.compare((*lop))==0){
            if( TString(region.c_str()).Contains("CRTop")){
              nnormRegions.push_back( (*lop));
              nnormRegions.push_back( (TString( (*lop).c_str())).ReplaceAll("CRTop","SRVBF").Data());
            }
            else if( TString(region.c_str()).Contains("SRVBF")){
              nnormRegions.push_back( (*lop));
              nnormRegions.push_back( (TString( (*lop).c_str())).ReplaceAll("SRVBF","CRTop").Data());
            }
          }
        }
      }
       */
      
      cout<<" with normalisation to: ";


      for(vector<string>::iterator lm=nnormRegions.begin(); lm!=nnormRegions.end(); lm++){
        cout<<" "<<(*lm);
      }
      cout<<endl;
    }
    else
      cout<<" normalised to theory "<<endl; 
    
    vector <std::pair<string, string>> vars=(*it).second;
    
      for( vector<std::pair<string, string>>::iterator it2=vars.begin(); it2!=vars.end(); it2++){
      
      string variation= whichVariation( (*it2).second ,sample,region);
      cout<<"Variation is "<<variation<<endl;
      cout<<"   with histograms    "<<(*it2).first<<" up "<<(*it2).second<<" down  "<<(*it).first<<" nominal "<<endl;
      if(variation.compare("unknown")==0){
        cout<<"       Unkown variation  .. skipping "<<endl;
        continue ;
      }

      cout<<"Sample is: "<<sample;
      if(toNorm){
        cout<<" with normalisation to: ";
        for(vector<string>::iterator lm=nnormRegions.begin(); lm!=nnormRegions.end(); lm++){
          cout<<" "<<(*lm);
  }
        cout<<endl;
      }
      else
        cout<<" normalised to theory "<<endl; 
      

      TH1F *hup=nullptr;
      hup=(TH1F*)f->Get((*it2).first.c_str());
      //TH1F *hupRB=nullptr;
      //hupRB=(TH1F*)f->Get(Form("%s_RB",(*it2).first.c_str()));
      
      
      TH1F *hdown=nullptr;
      hdown=(TH1F*)f->Get((*it2).second.c_str());
      //TH1F *hdownRB=nullptr;
      //hdownRB=(TH1F*)f->Get(Form("%s_RB",(*it2).second.c_str()));
      
      if(hnom->Integral() == 0 || (hnom->Integral()!=0 && ( (hup!=nullptr && hup->Integral()==0) || (hdown!=nullptr && hdown->Integral()==0))) || hdown==nullptr || hup==nullptr ) {
        // normalise to nominal 
        if(hup!=nullptr) delete hup;
        hup=(TH1F*)hnom->Clone( (*it2).first.c_str());
  chi_file<<hup->GetName()<<"\n";
  chi_file<<"hup is null\n";
        if( hdown!=nullptr) delete hdown;
        hdown=(TH1F*)hnom->Clone( (*it2).second.c_str());
  chi_file<<"hdown is null\n";
      }
      std::vector<double> VnormRegions={1,1,1};
      double integralUp=1;
      double integralDown=1;
      double integralNom=1;

      if(TString(variation).Contains("tautau_generator")){
  chi_file<<" line 2394 \n";
  chi_file<<hup->GetName()<<"\n";
  chi_file<<hdown->GetName()<<"\n";
  for(int i=0;i<=hnom->GetNbinsX();i++){
    chi_file<<hup->GetBinContent(i)<<" "<<hdown->GetBinContent(i)<<"\n";
  }
  chi_file<<"-------------------------------------------------------\n";
      }


      if( sysNomZeroDoNotCheck[ (*it).first ]){
      // now loop over the nominal sample bins 
        if(hup!=nullptr && hnom->Integral()>0) {
        
        cout<<"Histogram names "<<hnom->GetName()<<" upVar "<<hup->GetName()<<" -- down "<<hdown->GetName()<<endl;
        cout<<"Input integrals: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<" "<<hnom->Integral()<<" nominal "<<endl;

        
        
        bool isSignalInSignalRegion = TString(variation.c_str()).Contains("theo") && sample.compare("hvbf0")==0 && ( region.compare("SRVBF")==0 || region.compare("CRTop")==0 || region.compare("CRWW")==0 ); 
        if(isDiff) {
          isSignalInSignalRegion=TString(variation.c_str()).Contains("theo") && TString(sample.c_str()).Contains("hvbf0") && ( TString(region.c_str()).Contains("SRVBF_") || TString(region.c_str()).Contains("CRTop_") || TString(region.c_str()).Contains("CRWW") );
        }
        
        double integralup=hup->Integral();
        double integraldown=hdown->Integral();

        //smooth histograms

  if(TString(variation).Contains("tautau_generator")){
    chi_file<<hup->GetName()<<"\n";
    chi_file<<hdown->GetName()<<"\n";
    for(int i=0;i<=hnom->GetNbinsX();i++){
      chi_file<<hup->GetBinContent(i)<<" "<<hdown->GetBinContent(i)<<"\n";
    }
  }
  chi_file<<"-------------------------------------------------------\n";


        for(int bin=1; bin<(int)hnom->GetNbinsX()+1; bin++){

          double nomVal=hnom->GetBinContent(bin);
          double highVal=hup->GetBinContent(bin);
          double lowVal=hdown->GetBinContent(bin);

          //cout<<"    Original values: Bin "<<bin<<" nom "<<nomVal<<" high "<<highVal<<" low "<<lowVal<<endl;

          //double nomValRB=hnomRB->GetBinContent(hnomRB->FindBin(hnom->GetBinCenter(bin)));
          //          double highValRB=hupRB->GetBinContent(hnomRB->FindBin(hnom->GetBinCenter(bin)));
          //double lowValRB=hdownRB->GetBinContent(hnomRB->FindBin(hnom->GetBinCenter(bin)));
          
          //hdown->SetBinContent(bin,lowVal);
          //hup->SetBinContent(bin,highVal);
          //cout<<"    New val Bin "<<bin<<" nom "<<nomVal<<" high "<<highVal<<" low "<<lowVal<<endl;

          // check for abnormalites 
          if(  fabs(1-lowVal/nomVal) > 2 * fabs(1-highVal/nomVal) || fabs( 1-highVal/nomVal) > 2* fabs(1-lowVal/nomVal) ) 
            hasAbnormalValues=true;
          if ( fabs(1-lowVal/nomVal) > 1.5 * sqrt(nomVal)  || fabs( 1-highVal/nomVal) > 1.5 * sqrt(nomVal) )
            hasAbnormalValues=true; 
        }


        //if( hnom->Integral() == 0 ) {
        // normalise to nominal 
        //  hup->Scale(0.0);
        //hdown->Scale(0.0); 
        //}
        
        cout<<"    Integrals after fluctuations replacement: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;
        cout<<"    Integrals after re normalisations of fluctuations replacement: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;
        
        // normalise the samples according to their tranfer factors

        if( toNorm || isSignalInSignalRegion ){
          
    integralUp=0;
    integralDown=0;
    integralNom=0;

          cout<<" Normalising to (control) regions: "<<endl;
          for(vector<string>::iterator lm=nnormRegions.begin(); lm!=nnormRegions.end(); lm++)
            cout<<" "<<(*lm);
          cout<<endl;
          
          // Get the raw variations
    
          if(doggfABCD){
      std::map<string,TH1F*>ggfnomMap;
      std::map<string,TH1F*>ggfupMap;
      std::map<string,TH1F*>ggfdownMap;
      for(auto &reg:nnormRegions){
        string histToNomUp=sample+variation+"High_"+ reg + "_obs_"+obsRegions[reg];
        string histToNomDown=sample+variation+"Low_"+ reg + "_obs_"+obsRegions[reg];
        string nomHisCR=sample+"Nom_"+ reg + "_obs_"+obsRegions[reg];
        ggfnomMap[reg]=(TH1F*)f->Get(Form("%s",nomHisCR.c_str()));
        ggfdownMap[reg]=(TH1F*)f->Get(Form("%s",histToNomDown.c_str()));
        ggfupMap[reg]=(TH1F*)f->Get(Form("%s",histToNomUp.c_str()));
      }
    
      
      if(ggfnomMap["GGFCR1"]!=NULL && ggfnomMap["GGFCR2"]!=NULL && ggfnomMap["GGFCR3"]!=NULL ){
        integralNom+=ggfnomMap["GGFCR1"]->Integral();//*ggfnomMap["GGFCR2"]->Integral()/ggfnomMap["GGFCR3"]->Integral();
        integralUp+=ggfupMap["GGFCR1"]->Integral();//*ggfupMap["GGFCR2"]->Integral()/ggfupMap["GGFCR3"]->Integral();
        integralDown+=ggfdownMap["GGFCR1"]->Integral();//*ggfdownMap["GGFCR2"]->Integral()/ggfdownMap["GGFCR3"]->Integral();
      }
    }
    else{
      for(vector<string>::iterator lm=nnormRegions.begin(); lm!=nnormRegions.end(); lm++){
        cout<<"Processing region "<< (*lm)<<endl;

        string histToNomUp=sample+variation+"High_"+ (*lm) + "_obs_"+obsRegions[ (*lm)];
        string histToNomDown=sample+variation+"Low_"+ (*lm) + "_obs_"+obsRegions[ (*lm)];
        string nomHisCR=sample+"Nom_"+ (*lm) + "_obs_"+obsRegions[ (*lm)];
        
        //cout<<"Getting hist "<<Form("%s%s",nomHisCR.c_str(),"_RAW2")<<endl;
        //TH1F *hnomHisCR=(TH1F*)f->Get(Form("%s%s",nomHisCR.c_str(),"_RAW2"));
        cout<<"Getting hist "<<Form("%s%s",nomHisCR.c_str(),"")<<endl;
        TH1F *hnomHisCR=(TH1F*)f->Get(Form("%s",nomHisCR.c_str()));
        //cout<<"    Nominal norm hist is "<<hnomHisCR->GetName()<<"  "<<hnomHisCR->Integral() <<endl;
        if(hnomHisCR==nullptr)
    continue; 
        if(hnomHisCR->Integral()==0 )
    continue;
        integralNom+=hnomHisCR->Integral();
        //cout<<"    Requesing "<< Form("%s%s",histToNomUp.c_str(),"_RAW2")<<endl;
            
        TH1F *hRawUp=(TH1F*)f->Get(Form("%s%s",histToNomUp.c_str(),"_RAW2"));
        //TH1F *hRawUp=(TH1F*)f->Get(Form("%s",histToNomUp.c_str()));
        if(hRawUp==NULL){
    continue;
        }
        integralUp+=hRawUp->Integral();
        //cout<<"    Requesing "<< Form("%s%s",histToNomDown.c_str(),"_RAW2")<<endl;
        TH1F *hRawDown=(TH1F*)f->Get(Form("%s%s",histToNomDown.c_str(),"_RAW2"));
        if(hRawDown==NULL){
    continue;
        }
        //TH1F *hRawDown=(TH1F*)f->Get(Form("%s",histToNomDown.c_str()));
        integralDown+=hRawDown->Integral(); 
        
        //cout<<"    Up and down norm regions "<<hRawUp->GetName()<<" "<< hRawUp->Integral()<<" "<<hRawDown->GetName()<<" "<< hRawDown->Integral()<<endl;
        
        std::vector <pair<string, string>> rrs;
        rrs.push_back(make_pair("htop","hdiboson"));
        for(int lk=1; lk < 4 ; lk++)
    rrs.push_back(make_pair(Form("htop%d",lk),Form("hdiboson%d",lk)));
        
        bool isrrs=false;
        pair <string,string> thisrrS; 
        for(std::vector<pair<string,string>>::iterator ilkm = rrs.begin(); ilkm!=rrs.end(); ilkm++){
    if( sample.compare( (*ilkm).first) ==0 || sample.compare( (*ilkm).second) ==0 ) {
      isrrs=true;
      thisrrS=(*ilkm);
    }
        }
        
            
        // for WW and ttbar normalise to the sum of WW+ttbar in the given region
        //if( sample.compare( "htop") ==0 || sample.compare("hdiboson")==0 ) {
        if( isrrs){

          // find the integral of the other sample and add it to this onne
          TString otherName=nomHisCR.c_str() ;
          TString otherNameUp=histToNomUp.c_str();
          TString otherNameDown=histToNomDown.c_str();
          TString thisName=nomHisCR.c_str();
          TH1F *hnomotherRaw=nullptr; 
          TH1F * hnomthisRaw=nullptr;
          //if(sample.compare("htop")==0) {
          if( sample.compare( thisrrS.first)==0){
            //otherName.ReplaceAll("htop","hdiboson");
            otherName.ReplaceAll(thisrrS.first,thisrrS.second);
            otherNameUp.ReplaceAll(thisrrS.first,thisrrS.second);
            otherNameDown.ReplaceAll(thisrrS.first,thisrrS.second);
            
            
          }
          //else if (sample.compare("hdiboson")==0) {
          else if( sample.compare(thisrrS.second)==0){
            //otherName.ReplaceAll("hdiboson","htop");
            otherName.ReplaceAll(thisrrS.second,thisrrS.first);
            otherNameUp.ReplaceAll(thisrrS.second,thisrrS.first);
            otherNameDown.ReplaceAll(thisrrS.second,thisrrS.first);
          }
          
          cout<<"Getting "<<otherName.Data()<<endl;
          //hnomotherRaw=(TH1F*)f->Get(Form("%s%s",otherName.Data(),"_RAW"));
          hnomotherRaw=(TH1F*)f->Get(Form("%s%s",otherName.Data(),"_RAW2"));
          cout<<otherNameUp.Data()<<" "<<otherNameDown.Data()<<"\n";
          if(hnomotherRaw!=NULL){
          //cout<<" Adding to integral nom "<<hnomotherRaw->Integral()<<" events "<<endl; 
            integralNom+=hnomotherRaw->Integral();
            //integralNom+=hnomthisRaw->Integral();
            integralUp+=hnomotherRaw->Integral();
            //integralUp+=hnomthisRaw->Integral();
            integralDown+=hnomotherRaw->Integral();
            //integralDown+=hnomthisRaw->Integral();
          }
    
        
          delete hnomotherRaw;
        }
        cout<<"    Up and down norm regions "<<hRawUp->GetName()<<" "<< hRawUp->Integral()<<" "<<hRawDown->GetName()<<" "<< hRawDown->Integral()<<endl;
        
        //hup->Scale( hnom->Integral() / hup->Integral() * hRawUp->Integral() / hnomHisCR->Integral());
        //hdown->Scale( hnom->Integral() / hup->Integral() * hRawUp->Integral() / hnomHisCR->Integral());
        
        //delete hnomHisCR; 
        delete hRawUp;
        delete hRawDown;
        }
    }
    if(TString(variation).Contains("tautau_generator")){
      chi_file<<hup->GetName()<<"\n";
      chi_file<<hdown->GetName()<<"\n";
      for(int i=0;i<=hnom->GetNbinsX();i++){
        chi_file<<hup->GetBinContent(i)<<" "<<hdown->GetBinContent(i)<<"\n";
      }
    }
    chi_file<<"-------------------------------------------------------\n";
    

    cout<<"Total TF variation is "<<integralUp<<" "<<integralDown<<" nominal integral "<<integralNom<<endl;

    double regIntUp=hup->Integral();
    double regIntDown=hdown->Integral();
    double regNomInt=hnom->Integral(); 
    
    if(toNorm){
      if( hnom->Integral() < 0.1 ) {
        // normalise to nominal 
        hup->Scale(0.0);
        hdown->Scale(0.0); 
      }

    else {
      // normalise to nominal 
      //this is the issue here---
      if(regIntUp>0.1)
      hup->Scale(hnom->Integral()/hup->Integral()); 
      if(regIntDown>0.1)
      hdown->Scale(hnom->Integral()/hdown->Integral());

      // apply the TF variation
      if(integralNom!=0 && regIntUp!=0){
        if(regIntUp<0.1)
          hup->Scale(0.0);
        if(regIntDown < 0.1)
          hdown->Scale(0.0);
        if(regIntDown > 0.1 && regIntUp>0.1){
          hup->Scale( (integralUp/integralNom) *  (regNomInt/regIntUp ) );  
          hdown->Scale((integralDown/integralNom )* (regNomInt/ regIntDown)  ); 

        }
        // apply the TF variation 
        /*
        if( regIntUp >= regNomInt) 
          hup->Scale( 1+regIntUp/integralUp );
        else
          hup->Scale( 1-regIntUp/integralUp );  

                    if( regIntDown >= regNomInt)
                      hdown->Scale( 1+regIntDown/integralDown );  
                    else 
                      hdown->Scale( 1-regIntDown/integralDown );
        */
        // hdown->Scale( integralDown/integralNom  * hnom->Integral()/hdown->Integral()  );
                    
            //hup->Scale( hnom->Integral() / hup->Integral() *  integralNom/integralUp   );
            //hdown->Scale( hnom->Integral() / hdown->Integral() *  integralNom/integralDown );
        }
        else {
          for( int kl=-1;kl<hnom->GetNbinsX()+2; kl++){
            //GUY EDIT
            hup->SetBinContent(kl,hnom->GetBinContent(kl));
            hdown->SetBinContent(kl,hnom->GetBinContent(kl));
          }
        }
      }
    }

    if(TString(variation).Contains("tautau_generator")){
      chi_file<<hup->GetName()<<"\n";
      chi_file<<hdown->GetName()<<"\n";
      for(int i=0;i<=hnom->GetNbinsX();i++){
        chi_file<<hup->GetBinContent(i)<<" "<<hdown->GetBinContent(i)<<"\n";
      }
    }
    chi_file<<"-------------------------------------------------------\n";

    
        }
        cout<<"    Itegrals after normalisation :"<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;

  }
      
      cout<<"    Normalisation uncertainty : "<<hnom->Integral()<<" up: "<<(1-hup->Integral()/hnom->Integral())*100 <<" %  down: "<<(1-hdown->Integral()/hnom->Integral())*100<<endl;
      if( hasAbnormalValues || ( fabs( 1- hup->Integral()/hnom->Integral()) > 0.20 || fabs( 1- hdown->Integral()/hnom->Integral()) > 0.2))
        cout<<"ATTENTION! systematic "<<variation<<" for "<<sample<<" in "<<region<<" has abnormal values "<<endl;
      
      /*
      //cout<<"Safety check for "<<h->GetName()<<endl;
      for(std::map<string, vector<string>>::iterator itC=checkMap.begin(); itC!=checkMap.end(); itC++){
      vector <string> lregions=(*itC).second;
      if( sample.compare( (*itC).first )==0 ){
        //cout<<" Safety "<<h->GetName()<<" contains "<< (*itC).first<<endl;
        for( vector<string>::iterator itA=lregions.begin(); itA!=lregions.end(); itA++){
          if( region.compare( *itA)==0  ){
            //cout<<"Safeness "<<h->GetName()<<" against  "<< (*itC).first<<" and "<< (*itA)<<endl;
            safe++;
          }
        }
        if(safe==0){
          cout<<" histogram "<< h->GetName()<<" is not safe "<<endl;
          for( int i=0; i<(int)h->GetNbinsX()+2; i++){
            h->SetBinContent(i,0);
            h->SetBinError(i,0);
          }
        }
      }
    }
      */  
      }
      
      //hdown->Write();
      //hup->Write();
      if(TString(variation).Contains("tautau_generator")){
  chi_file<<hup->GetName()<<"\n";
  chi_file<<hdown->GetName()<<"\n";
  for(int i=0;i<=hnom->GetNbinsX();i++){
    chi_file<<hup->GetBinContent(i)<<" "<<hdown->GetBinContent(i)<<"\n";
  }
      }
      chi_file<<"\n";
      VnormRegions[0]=integralNom;
      VnormRegions[1]=integralUp;
      VnormRegions[2]=integralDown;

      if(TString(variation).Contains("MET_SoftTrk_ResoP")){
  symmetrize_hist(hnom,hup,hdown);
      }



      TH1F *hNormUp=(TH1F*)hup->Clone(Form("%sNorm",hup->GetName()));
      hNormUp->Write();
      TH1F *hNormDown=(TH1F*)hdown->Clone(Form("%sNorm",hnom->GetName()));
      hNormDown->Write();

      TH1F * integral_sum=new TH1F(Form("%s_%s_%s_%s_saved_integral",sample.c_str(),region.c_str(),obsRegions[region].c_str(),variation.c_str()),Form("%s %s %s",variation.c_str(),sample.c_str(),region.c_str()),9,1,9);
      integral_sum->SetBinContent(1,VnormRegions[0]);
      integral_sum->SetBinContent(2,VnormRegions[1]);
      integral_sum->SetBinContent(3,VnormRegions[2]);
      integral_sum->SetBinContent(4,hnom->Integral());
      integral_sum->SetBinContent(5,hup->Integral());
      integral_sum->SetBinContent(6,hdown->Integral());
    

      if(hdown->Integral()>0.001)
  hdown->Scale(hnom->Integral()/ hdown->Integral());
      if(hup->Integral()>0.001)
  hup->Scale(hnom->Integral()/ hup->Integral());
      if( TString(hup->GetName()).Contains("CRZjets") && TString(variation).Contains("tautau_generator")){
    //sliding_window_smoothing(hnom,hup);
      //sliding_window_smoothing(hnom,hdown);
  } 
  if(!TString(variation).Contains("MUON_SCALE")&& !TString(variation).Contains("EG_SCALE_ALL")){
    symmetrize_hist(hnom,hup,hdown);
  }


      /*
      double chi2=calculate_chi2(hnom,hup);
      
      double cutoff=0.1;
      chi_file<<sample<<","<<region<<","<<variation<<","<<chi2<<"\n";
      
      if(chi2<cutoff){
  for(int i=0;i<=hup->GetNbinsX();i++){
    hup->SetBinContent(i,hnom->GetBinContent(i));
          hdown->SetBinContent(i,hnom->GetBinContent(i)); 
        }
      } 
      */
      //guy::here
      check_safety(hdown,checkMap,sample,region);
      check_safety(hup,checkMap,sample,region);
      check_safety(hnom,checkMap,sample,region);
  
      hdown->Write();
      hup->Write();
      integral_sum->SetBinContent(7,hnom->Integral());
      integral_sum->SetBinContent(8,hup->Integral());
      integral_sum->SetBinContent(9,hdown->Integral());
      //if(sampleNormMap.count(sample)){
      //  if(std::find(sampleNormMap[sample].begin(), sampleNormMap[sample].end(), region) != sampleNormMap[sample].end()){
      integral_sum->Write();
      //}
      //}

      // here add the Region variations
      /*
      for( int io=1; io<4; io++){
        if( hasRegion[io-1] ) continue ;
        
        vector<string> isamples=whichSamplesDecorrRegions[Form("Region%d",io)];
        for(vector<string>::iterator ilp=isamples.begin(); ilp!=isamples.end(); ilp++){
          if(sample.compare( (*ilp))==0){
            TString nvarUp(variation.c_str());
            //nvarUp.ReplaceAll("Low","High");
            
            TH1F *hupRegionUp=(TH1F*)hup->Clone(Form("%s%sRegion%dHigh_%s_obs_%s",sample.c_str(),nvarUp.Data(),io,region.c_str(),thisObs.c_str()));
            hupRegionUp->Write();
            TH1F *hupRegionUpNorm=(TH1F*)hupRegionUp->Clone(Form("%sNorm",hupRegionUp->GetName()));
            hupRegionUpNorm->Write();
            delete hupRegionUpNorm;
            delete hupRegionUp;

            TString nvarDown(variation.c_str());
            //nvarDown.ReplaceAll("High","Low");
            TH1F *hupRegionDown=(TH1F*)hdown->Clone(Form("%s%sRegion%dLow_%s_%s",sample.c_str(),nvarDown.Data(),io,region.c_str(),thisObs.c_str()));
            hupRegionDown->Write();
            TH1F *hupRegionDownNorm=(TH1F*)hupRegionDown->Clone(Form("%sNorm",hupRegionDown->GetName()));
            hupRegionDownNorm->Write();
            delete hupRegionDownNorm;
            delete hupRegionDown; 
          }
        }
      }
     
      */
      delete hNormUp;
      delete hNormDown;
      
      // write there that this sample is affected by this systematic 
      if(hdown!=nullptr)delete hdown;
      if(hup!=nullptr) delete hup;
      //delete hdownRB;
      //delete hupRB;

      }
    

      if(hnom!=nullptr) {
  hnom->Write();
  TH1F *hNormNom=(TH1F*)hnom->Clone(Form("%sNorm",hnom->GetName()));
  hNormNom->Write();
  delete hNormNom; 
  delete hnom;
  //delete hnomRB;
      
      }
  }

  

  

  
  /*
  cout<<"Starting the loop over norm systematics now "<<endl;
  // Now loop again over all systematics 
  for( std::map<string, std::vector<std::pair<string,string>>>::iterator it=sysVarsNormMap.begin(); it!=sysVarsNormMap.end(); it++){
    continue ; 
    TH1F *hnom=(TH1F*)f->Get( (*it).first.c_str());
    if(hnom==nullptr) {
      cout<<"nominal histogram "<<(*it).first<<" is a null pointer "<<endl;
      continue;
    }
    
    //for(int b=1; b<(int)hnom->GetNbinsX()+1; b++){
    //if(hnom->GetBinContent(b)==0) hnom->SetBinContent(b,emptyReplace); 
    //}

    cout<<"Nominal is "<<hnom->GetName()<<endl;
    // loop over all the variations 

    vector <std::pair<string, string>> vars=(*it).second;
    for( vector<std::pair<string, string>>::iterator it2=vars.begin(); it2!=vars.end(); it2++){
      //continue ;
      cout<<"Norm Variation is "<<(*it2).first<<" "<<(*it2).second<<" nominal "<<(*it).first<<endl;

     
      
      TH1F *hup=nullptr;
      hup=(TH1F*)f->Get((*it2).first.c_str());
      TH1F *hdown=nullptr;
      hdown=(TH1F*)f->Get((*it2).second.c_str());
      // chechk if up and down variations exist 

      if( hup==nullptr && hdown!=nullptr) {
        //hup=(TH1F*)hdown->Clone((*it2).first.c_str());
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        cout<<"Hup did not exist replacing it by downvariation for "<<(*it2).first<<endl;
      }
      
      else if ( hup!=nullptr && hdown==nullptr){
        //hdown=(TH1F*)hup->Clone((*it2).second.c_str());
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"Norm Hdown does not exist replacing it by upvariation for "<<(*it2).second<<endl;
      }
      else if (hup==nullptr && hdown==nullptr){
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"Norm Both variations do not exist, created ad hoc variation"<<endl;
      }

      cout<<"Histogram "<<hnom->GetName()<<" upVar "<<hup->GetName()<<" -- down "<<hdown->GetName()<<endl;
      cout<<"Old integrals nominal (0) : "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;
      // now loop over the nominal sample bins 

      if(hup->Integral()==0) {
        delete hup;
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
      }
      
      if(hdown->Integral()==0){
        delete hdown;
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
      }
      
      

      if(hup!=nullptr && hdown!=nullptr && hnom!=nullptr){

        // sort out the integrals 
        //if( TString(hnom->GetName()).Contains("hdiboson")) {
        //hdown->Scale(hnom->Integral());
        //hup->Scale(hnom->Integral());
        //}
        
        for(int bin=1; bin<(int)hnom->GetNbinsX()+1; bin++){

          double nomVal=hnom->GetBinContent(bin);
          double highVal=hup->GetBinContent(bin);
          double lowVal=hdown->GetBinContent(bin);

          cout<<"Original values: Bin "<<bin<<" nom "<<nomVal<<" high "<<highVal<<" low "<<lowVal<<endl;
          
          if( nomVal==0){
            nomVal=emptyReplace; 
          }
          

          //highVal=nomVal*1.0001;
          //lowVal=nomVal*0.9999;

//          // check abnormal sys
//         if (  highVal==0 || fabs( (highVal-nomVal)/highVal ) > 1   ){
//            cout<<"------> "<<hdown->GetName()<<"Has abnormal systematic for High "<<lowVal<<" for nominal " <<nomVal<<" high "<<highVal<<endl;
//           if( bin > 1 && hup->Integral() > 0 )
//              //highVal=hup->GetBinContent(bin-1)/hnom->GetBinContent(bin-1)*nomVal;
//              highVal=nomVal;
//            else 
//              highVal=nomVal;
//          }
//          if(  lowVal ==0 || fabs( (lowVal-nomVal)/highVal) > 1 ){
//            cout<<"------> "<<hdown->GetName()<<"Has abnormal systematic for low "<<lowVal<<" for nominal " <<nomVal<<" high "<<highVal<<endl;
//            if( bin > 1 && hdown->Integral() > 0 )
//              //lowVal=hdown->GetBinContent(bin-1)/hnom->GetBinContent(bin-1)*nomVal;
//              lowVal=nomVal;
//            else 
//              lowVal=nomVal;
//          }
//          
//          if( highVal==lowVal) {
//            lowVal=nomVal;
//            }

          if( fabs(highVal-nomVal) < hnom->GetBinError(bin))
            highVal=nomVal; 
          
          if( fabs(lowVal-nomVal) < hnom->GetBinError(bin))
            lowVal=nomVal; 
          

          if( nomVal==emptyReplace) {
            highVal=emptyReplace;
            lowVal=emptyReplace;
          }

          cout<<" New val Bin "<<bin<<" nom "<<nomVal<<" high "<<highVal<<" low "<<lowVal<<endl;
          hdown->SetBinContent(bin,lowVal);
          hup->SetBinContent(bin,highVal);
        }
      }

      //hup->Smooth(1);
      //hdown->Smooth(1);
      
      // sort out the integrals 
      cout<<"Old integrals nominal: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;

//      if(hdown->Integral()!=0) 
//       hdown->Scale(hnom->Integral()/hdown->Integral());
//     if(hup->Integral()!=0)
//       hup->Scale(hnom->Integral()/hup->Integral());

      cout<<"Norm new integrals nominal: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;
      
      // write there that this sample is affected by this systematic 
      if(hdown!=nullptr){
        hdown->Write();
        delete hdown;
      }
      if(hup!=nullptr) {
        hup->Write();
        delete hup;
      }
    }
    if(hnom!=nullptr) {
      hnom->Write();
      delete hnom;
    }
  }*/

  
  /*
  // sortout the normalisation coefficients
  for( std::map<string, std::vector<std::pair<string,string>>>::iterator it=tfSysNormMap.begin(); it!=tfSysNormMap.end(); it++){
    continue; 
    TH1F *hnom=(TH1F*)f->Get( (*it).first.c_str());
    if(hnom==nullptr) {
      cout<<"nominal histogram "<<(*it).first<<" is a null pointer "<<endl;
      continue;
    }

    vector <std::pair<string, string>> vars=(*it).second;
    for( vector<std::pair<string, string>>::iterator it2=vars.begin(); it2!=vars.end(); it2++){
      TH1F *hup=nullptr;
      hup=(TH1F*)f->Get((*it2).first.c_str());
      TH1F *hdown=nullptr;
      hdown=(TH1F*)f->Get((*it2).second.c_str());

      if( hup==nullptr && hdown!=nullptr) {
        //hup=(TH1F*)hdown->Clone((*it2).first.c_str());
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        cout<<"Hup did not exist replacing it by downvariation for"<<(*it2).first<<endl;
      }
      
      else if ( hup!=nullptr && hdown==nullptr){
        //hdown=(TH1F*)hup->Clone((*it2).second.c_str());
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"Hdown does not exist replacing it bu upvariation for "<<(*it2).second<<endl;
      }
      else if (hup==nullptr && hdown==nullptr){
        cout<<"Both variations do not exist, skipping this systematic"<<endl;
        continue; 
      }
      // sort out the integrals 
      cout<<"Norm overall Names "<<hnom->GetName()<<" up "<<hup->GetName()<<" down "<<hdown->GetName()<<endl;
      cout<<"Old integrals nominal: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;

      if (hdown->Integral()<=0){
        for( int b=1; b<(int)hnom->GetNbinsX()+1; b++)
        hdown->SetBinContent(b,hnom->GetBinContent(b)); 
      }
      
      if (hup->Integral()<=0){
        for( int b=1; b<(int)hnom->GetNbinsX()+1; b++)
          hup->SetBinContent(b,hnom->GetBinContent(b)); 
      }
      
      // make some checks
      if( hup->Integral()==hdown->Integral())
        hdown->Scale(hnom->Integral()/hdown->Integral());

      if( fabs(1- hdown->Integral() / hnom->Integral()) > 3 ) {
        cout<<"Differnece greater than 3 suppressing the down systematic "<<hdown->Integral()<<" relative"<< hdown->Integral()/hnom->Integral()<<endl;
        hdown->Scale(hnom->Integral()/hdown->Integral());
      }
      if( fabs(1- hup->Integral() / hnom->Integral()) > 3 ){
        cout<<"Differnece greater than 3 suppressing the up systematic "<<hup->Integral()<<" relative"<< hup->Integral()/hnom->Integral()<<endl;
        hup->Scale(hnom->Integral()/hup->Integral());
      }
      
      
      cout<<"New integrals nominal: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;
      hdown->Write();
      hup->Write();
      if(hdown!=nullptr)delete hdown;
      if(hup!=nullptr) delete hup;
      
    }
    if(hnom!=nullptr) {
      hnom->Write();
      delete hnom;
    }
  }*/

  /*
  // Add the theory uncertainties from histograms
  TFile *ftheor=TFile::Open("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/October2019_withSys/LauraHF/vbf.root","READ");
  TKey *keyP2=nullptr;
  TIter nextP2(ftheor->GetListOfKeys());
  vector <string> TH1FNamesTH;
  vector <string> TH1FNamesNormTH;
  vector <string> TH1FNamesNormTFTH;
  vector <string> TH1FNamesNormRawTH;

 while ((keyP2=(TKey*)nextP2())) {
    if (strcmp(keyP2->GetClassName(),"TH1")) {
      string ttname=remove_extension(keyP2->GetName());
      bool isnew=true;
      for(std::vector<string>::iterator it=TH1FNamesTH.begin(); it!=TH1FNamesTH.end(); it++){
        if( (*it).compare(ttname)==0) 
          {   isnew=false; break; } 
      }
      if(isnew){
        TH1FNamesTH.push_back(ttname);
        if(!TString(ttname.c_str()).Contains("Norm"))
          TH1FNamesNormRawTH.push_back(ttname);
        if(TString(ttname.c_str()).Contains("Norm") && TString(ttname.c_str()).Contains("_obs"))
          TH1FNamesNormTH.push_back(ttname);
        if(TString(ttname.c_str()).Contains("Norm") && !TString(ttname.c_str()).Contains("_obs"))
          TH1FNamesNormTFTH.push_back(ttname);
      }
    }
 }
  */
  fnew->Close();
  delete fnew;
  chi_file.close();

  // Final cleanup
  /*
  TFile *final=new TFile(tname.c_str(),"RECREATE");
  vector <string> savedHists;
  
  for( int i=5; i>0 ; i--){
    TFile *fin=TFile::Open(Form("%s.%d.root",tname.c_str(),i));
    TKey *keyP2=nullptr;
    TIter nextP2(fin->GetListOfKeys());
    while ((keyP2=(TKey*)nextP2())) {
      if (strcmp(keyP2->GetClassName(),"TH1")) {
        string ttnameX=remove_extension(keyP2->GetName());
        TH1F *h=(TH1F*)fin->Get(ttnameX.c_str());
        bool exist=false;
        for( vector<string>::iterator lp=savedHists.begin() ; lp!=savedHists.end(); lp++) {
          if( ttnameX.compare( (*lp))==0) { exist=true;  break; }
        }
        if( h!=nullptr && !exist) {
          final->cd();
          h->Write();
          savedHists.push_back(ttnameX);
        }
      }
    }
    delete fin; 
  }  
  final->Close();
  */

}


int main (int argc, char* argv[]){
  
  string name=argv[1];
  string var =argv[2];
  bool clone=false;
  if(TString(argv[3]).Contains("true"))
    clone=true;
  checkAndFixFitInputs(name,var,clone);

}

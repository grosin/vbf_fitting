void smooth_histogram(TH1F * hist,double smoothing_parameter,double removal_coef){
  bool debug=false;
  //if ( TString(hist->GetName()).Contains("ztautau_generator") && TString(hist->GetName()).Contains("High_")) debug=true;
  //if(debug)cout<<hist->GetName()<<"\n";
  int nbins=hist->GetNbinsX();
  double mean_bin_width=0;
  for(int i=1;i<nbins;i++){
    mean_bin_width+=hist->GetBinWidth(i);
  }
  mean_bin_width=mean_bin_width/(double)nbins;
  double smooth_sigma=mean_bin_width*smoothing_parameter;
  double kernel;

  std::vector<double> smoothed_data(nbins);
  std::vector<int> skip_bins;

  //find which bins have weird outliers to ignore
  double nn_mean;
  double n_neighs;
  ofstream skipped_bins;
  

  for(int i=1;i<=nbins;i++){
    nn_mean=0;
    n_neighs=0;
    if(i>1){
      nn_mean+=hist->GetBinContent(i-1); 
      n_neighs+=1;
    }if(i>2){
      nn_mean+=hist->GetBinContent(i-2);
      n_neighs+=1;
    }if(i<nbins){
      nn_mean+=hist->GetBinContent(i+1);  
      n_neighs+=1;
    }if(i<nbins-1){
      nn_mean+=hist->GetBinContent(i+2);
      n_neighs+=1;
    }
    
    nn_mean=nn_mean/n_neighs;
    if((hist->GetBinContent(i) > (nn_mean+0.2)*removal_coef) || ((hist->GetBinContent(i)+.2)*removal_coef < nn_mean) ||(hist->GetBinContent(i)<0)){
      skip_bins.push_back(1);
      if(debug){
	cout<<"skipping "<< i <<" nn mean" <<nn_mean<<" bin content "<<hist->GetBinContent(i)<<"\n";
      }
    }else{
      skip_bins.push_back(0);
    }

  }


  double kernel_sum;
  cout<<"unsmoothed bin content \n";
  for(int i=1;i<=nbins;i++){cout<<hist->GetBinContent(i)<<" ";}
  cout<<"\n";
  for(int i=1;i<=nbins;i++){
    kernel_sum=0;
    for(int j=1;j<=nbins;j++){
      if(skip_bins[j-1]){ 
	continue;
      }
      else{
	kernel=TMath::Exp(-1*pow(hist->GetBinCenter(i)-hist->GetBinCenter(j),2)/pow(2*smooth_sigma,2));
	kernel_sum+=kernel;
	smoothed_data[i-1]+=hist->GetBinContent(j)*kernel;
	cout<<"bin "<<i<<" kernel from "<<j<<" is "<<kernel<<" with orginal bin content "<<hist->GetBinContent(j)<<"\n";
      }
    }
    cout<<"bin "<<i<<" smoothed data"<<smoothed_data[i-1]<<" kernel sum "<<kernel_sum<<"\n";
    smoothed_data[i-1]=smoothed_data[i-1]/kernel_sum;
  }
  double bin_content;
  double bin_error;
  for(int i=1;i<=nbins;i++){
    bin_content=smoothed_data[i-1];
    bin_error=hist->GetBinError(i)*bin_content/hist->GetBinContent(i);
    hist->SetBinContent(i,bin_content);
    hist->SetBinError(i,bin_error);
  }

}

//change theory shape to be the same as nominal, keep only norm
void pruneShape(TFile *file,TH1F * hist){

  TString nominal_name;
  if (TString(hist->GetName()).Contains("High_"))  nominal_name = TString(hist->GetName()).ReplaceAll("High_","Nom_");
  if (TString(hist->GetName()).Contains("Low_"))  nominal_name = TString(hist->GetName()).ReplaceAll("Low_","Nom_");

  TH1F* nominal=(TH1F*)file->Get(nominal_name.Data());
  double theo_integral = hist->Integral();
  hist->Scale(nominal->Integral()/theo_integral);

  for(int i=1;i<=nominal->GetNbinsX();i++){
    hist->SetBinContent(i,nominal->GetBinContent(i));
  }  
  hist->Scale(theo_integral/hist->Integral());

}

bool isTwoPoint(const char * name){
  if (TString(name).Contains("shower") ){
    return true;
  }if(TString(name).Contains("maching")){
    return true;
  }if(TString(name).Contains("generator")){
    return true;
  }
  return false;


}
void symmetrize_two_point(TFile *unsmoothed_file,TFile *smoothed_file,string nominal_systematic){
  TH1F* nominal=(TH1F*)smoothed_file->Get(nominal_systematic.c_str());
  int index;
  string low_name=nominal_systematic.c_str();
  string high_name=nominal_systematic.c_str();
  index = low_name.find("Nom_", 0);
  low_name.replace(index, 3, "Low");
 
  index = high_name.find("Nom_", 0);
  high_name.replace(high_name.begin()+index,high_name.begin()+index+3, "High");
  cout<<"nominal hist "<<nominal->GetName()<<"\n";


  TH1F *high_hist=(TH1F*)smoothed_file->Get(high_name.c_str());
  cout<<" high hist "<<high_hist->GetName()<<"\n";
  TH1F* low_hist=(TH1F*)unsmoothed_file->Get(low_name.c_str());
  cout<<" low hist "<<low_hist->GetName()<<"\n";
  double high_integral=high_hist->Integral()+0.0001;
  double low_integral=low_hist->Integral()+0.0001;
  high_hist->Scale(nominal->Integral()/high_integral);
  low_hist->Scale(nominal->Integral()/low_integral);
  if(TString(low_name).Contains("CRTop"))
    low_hist->Rebin(2);
  for(int i=1;i<=nominal->GetNbinsX();i++){
    double current_content=low_hist->GetBinContent(i);
    low_hist->SetBinContent(i,2*nominal->GetBinContent(i)-high_hist->GetBinContent(i));
    cout<<" current low "<<current_content<<"\n";
    cout<<" nominal "<<nominal->GetBinContent(i)<<"\n";
    cout<<" high "<<high_hist->GetBinContent(i)<<"\n";
    cout<<" new low "<<low_hist->GetBinContent(i)<<"\n";
    low_hist->SetBinError(i,low_hist->GetBinContent(i)/current_content*low_hist->GetBinError(i));
  }
  cout<<" low integral "<<low_integral<<"\n";
  cout<<" high integral "<<high_integral<<"\n";
  high_hist->Scale(high_integral/(high_hist->Integral()+0.0001));
  if(low_integral>0 )
    low_hist->Scale(low_integral/(low_hist->Integral()+0.0001));
  smoothed_file->cd();
  low_hist->Write();
  delete low_hist;
  delete high_hist;
  delete nominal;
}
bool combine_diff_bins(TH1F * hist, TFile *f){
  if(TString(hist->GetName()).Contains("SRVBF_6") || TString(hist->GetName()).Contains("CRTop_6"))
    return true;
  if(TString(hist->GetName()).Contains("SRVBF_5") ){
    string this_name=hist->GetName();
    this_name.replace(this_name.find("SRVBF_5"), 7, "SRVBF_6");
    TH1F *last_bin=(TH1F*)f->Get(this_name.c_str());
    hist->Sumw2();
    last_bin->Sumw2();
    hist->Add(last_bin);
    delete last_bin;
  } if(TString(hist->GetName()).Contains("CRTop_5") ){
    string this_name=hist->GetName();
    this_name.replace(this_name.find("CRTop_5"), 7, "CRTop_6");
    TH1F *last_bin=(TH1F*)f->Get(this_name.c_str());
    hist->Sumw2();
    last_bin->Sumw2();
    hist->Add(last_bin);
    delete last_bin;
  }
  return false;

}
void process_theories(string all_theory_hists,string obs){
  TFile * input_file=TFile::Open(all_theory_hists.c_str(),"READ");
  TFile * output_file=TFile::Open((obs+"_smoothed_theory.root").c_str(),"RECREATE");
  TIter next(input_file->GetListOfKeys());
  TKey* key;
  double integral=0;
  double smoothing_par=3.0;
  vector<string> two_point_systematics={};
  while ((key=(TKey*)next())){
    TString name=TString(key->GetName());
    //if( !name.Contains("ztautau"))continue;
    TH1F * hist;
    if(string(key->GetClassName()).compare("TH1F")!=0)
      continue;
    hist=(TH1F*)input_file->Get(key->GetName());
    if(combine_diff_bins(hist,input_file)) //for DYjj combine the last two bins
      continue;
    if( !name.Contains("CRGGF")  && TString(name).Contains("Low_")&&isTwoPoint(key->GetName())) {
      delete hist;
      cout<<"skippinng "<<key->GetName()<<"\n";
      continue;
    }
    integral=hist->Integral();
    cout<<"hist "<<key->GetName()<<" Integral before "<<integral<<"\n";
    if(name.Contains("CRTop")){
      smooth_histogram(hist,1.85,50);
      hist->Rebin(2);
    }else if(name.Contains("SRVBF")){

      if(name.Contains("ztautau_generator"))
	//smooth_histogram(hist,4.5,3.0);
	cout<<"do not smooth"<<endl;
      else
	smooth_histogram(hist,3.0,3.0);
    }
    else if(name.Contains("CRGGF")){
      smooth_histogram(hist,0.2,100);
    }else if(name.Contains("CRZjets")&&name.Contains("ztautau_generator")&&!name.Contains("Nom_")){
      pruneShape(input_file, hist);
      //cout<<name.Data()<<endl;
      //smooth_histogram(hist,2.0,100);
      //hist->Rebin(2);
    }

    if (hist->Integral()<1e-8){
      for(int i=1; i<=hist->GetNbinsX();i++){

        hist->SetBinContent(i,1e-9);
      }
    }

    if(integral>0)
      hist->Scale(integral/(hist->Integral()));
    output_file->cd();
    hist->Write();
    delete hist;
    if(!name.Contains("CRGGF")   && isTwoPoint(key->GetName()) && TString(key->GetName()).Contains("Nom_")){
      two_point_systematics.push_back(key->GetName());
    }
  }
  for(auto &sys:two_point_systematics){
    symmetrize_two_point(input_file,output_file,sys);
  }
  input_file->Close();
  output_file->Close();
}


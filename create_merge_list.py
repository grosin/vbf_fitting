import os
from subprocess import Popen, PIPE
import sys
def create_merge_files(folder,sample=''):
    #source_dir="/eos/user/g/grosin/"+folder
    #source_dir="/eos/user/a/asteraki/SagarSpace/"
    #source_dir="/eos/user/c/ckitsaki/analysis/DumpednTuples_RecoSystematics/"
    #source_dir="/eos/user/c/ckitsaki/nominalHWW/fullrun2/"
    source_dir = "/atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/makeInputs/submission/jobsSamplesAndSys_%s/run"%sys.argv[3]
    data_dirs=source_dir#+"/run/"
    data_list=os.listdir(data_dirs)
    files_to_merge=[]
    for root,directory,files in os.walk(data_dirs):
        for f in files:
            if ".root" in f and sample in f:
                file_path=root+"/"+f
                files_to_merge.append(file_path)


    with open(folder+"_files_to_merge.txt","a") as merge_files:
        for fil in files_to_merge:
            merge_files.write(fil)
            merge_files.write("\n");

def merge_files():
    command=".x scripts/mergesubsamples.c(\"files_to_merge.txt\")"
    p = Popen(['root',"-l"],stdin=PIPE)
    p.stdin.write(command.encode())
    p.communicate(".q".encode())

    
def move_file():
    with open("files_to_merge.txt","r") as merge_files:
        full_path=merge_files.readline()
        file_name=full_path.split("/")[-1]
        file_renamed=file_name.split("_Partial")[0]
        file_renamed+=".root"
        os.system("mv output.root "+file_renamed)
        os.system("mv "+file_renamed+" data/")
def create_sys_list():
    sample_syst={}
    with open("files_to_merge.txt") as merge_files:
        for fil in merge_files:
            line=fil.split("/")
            data=line[2]
            line_data=data.split("_")
            sample=line_data[0]
            syst="_".join(line_data[1:])

            if sample in sample_syst:
                sample_syst[sample].add(syst)
            else:
                sample_syst[sample]=set((syst,))
    total_syst=sample_syst["ggf"]
    for key,value in sample_syst.items():
        if key=="fakes":
            continue
        print(total_syst)
        total_syst=total_syst | value
    print(total_syst) 
    with open("sysVars_v21_merge.txt",'a') as sys_file:
        for syst in total_syst:
            sys_file.write(syst)
            sys_file.write("\n")
args=sys.argv
if(len(args)==1):
    exit
command=args[1]
if command=="trees":
    folder=args[2]
    samples="top vgamma diboson zjets vbf Zjets0 vbf0 ggf data vh htt fakes".split(" ")
    for sample in samples:
        create_merge_files(folder,sample)
elif command=="create":
    folder=args[2]
    create_merge_files(folder)
elif command=="move":
    move_file()

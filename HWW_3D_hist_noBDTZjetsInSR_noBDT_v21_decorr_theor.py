from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from SystematicsUtils import appendIfMatchName
import commands
from copy import deepcopy
from os import sys, listdir
from os.path import isfile, join
from math import sqrt
from optparse import OptionParser
import ROOT
from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
import shutil
import re


doTheorErrors=True
buildSystematics=True
readSystematics=True
topWWBins=4
ZjetsBins=5
ggf1Bins=4
ggf2Bins=3
ggf3Bins=4
#if( buildSystematics ) :
#    readSystematics = True
reBinnedInputs=False
hasggFCR=True

vbf_stat=True
back_stat=True
histo_mode="histoSys"
#version="v19_r21_3DStef_v3_0_fit_v1_testing"
#version="v19_r21_3DStef_v3_0_fit_v2_dbg"
#version="v19_r21_3DStef_v3_0_fit_v2_binning3D10x10x2"
#version="v19_r21_3DStef_v3_0_fit_v2fixedBDTCR_noSysggF"
#version="v19_r21_3DStef_v3_0_fit_v2fixedBDTCR_noSysggF_fix_debug6" #all files there with separation for bckgs in 0-1 region 
#version="v19_r21_3DStef_v3_0_fit_v2fixedBDTCR_noSysggF_fix_debug6_commonWWTop" #all files there without separation for bckgs in 0-1 region
#version="v20_mix_v19_r21_3DStef_v3_0_fit_v2fixedBDTCR_noSysggF_fix_db2" #all files there without separation for bckgs in 0-1 region, 0-1 jets from v19 
#version="v19_r21_3DStefOpt_04_02_2020_TopWW_fit_v2fixedBDTCR_commonWWTop"
#version="v19_r21_10_02_2020_TopWW_fit_v2fixedBDTCR_commonWWTop_FidXs"
#version="v19_r21_10_02_2020_WW_fit_v2fixedBDTCR_commonWWTop"
#version="v19_r21_10_02_2020_WW_fit_v2fixedBDTCR_commonWWTop_testGGFBin" 
#version="v19_r21_24_02_2020_CommonPar_DBG"
#version="v20_r21_09_03_2020_First"
#version="v20_r21_26_03_2020_v2_JER"
#version="v20_r21_26_03_2020_v2"
#version="v20_r21_26_03_2020_v2_JER"
#version="v20_r21_27_03_2020_v2_JER"
#version="v20_r21_02_04_2020_v2_JER"
#version="v20_r21_10_04_2020_v2_JER"
#version="v20_r21_14_04_2020_v2_bdtJets2"
#version="v20_r21_14_04_2020_v2_bdtJets3"
#version="v20_r21_14_05_2020_v2_bdtJets4" #Uses MT for Zjets and sample split
#version="v20_r21_14_04_2020_v2_bdtJets5" #BDT for Zjets and NO sample split
#version="v20_r21_29_04_2020_v2_noBDTCut" #Cut on BDTZjets in SR and CR, MT in Zjets CR
#version="v21_r21_26_05_2020_v1_noBDTCut" #Cut on BDTZjets in SR and CR, MT in Zjets CR
#version="v21_r21_10_06_2020_v1_noBDTCut" #Cut on BDTZjets in SR and CR, MT in Zjets CR
#version="v21_r21_16_06_2020_v1_noBDTCut" #Cut on BDTZjets in SR and CR, MT in Zjets CR
#version="v21_r21_19_06_2020_v1_noBDTCut" # including uncertainties on topX
##Differntial versions
version="v21_r21_21_06_2020_v1_noBDTCutMjj" # including uncertainties on topX
#version="v21_r21_21_06_2020_v1_noBDTCut" # including uncertainties on topX
#version="v21_r21_11_09_2020_v1_noBDTCut"
#version="v21_r21_21_09_2020_v1_noBDTCutNewBins"
version="v21_r21_28_09_2020_v1_noBDTCut"
#version="v21_r21_19_10_2020_v1_noBDTCut"
doABCD=True
doZeroOne=True
is1D=True
isCounting3D=False
commonTopWWParameter=True
hasTopCR=True
topCRInSR=True
wwCRinSR=False
addggFDiscr=False #True #!!!!!!!!!
decorrelateABCDSystematics=True
doFidCrossSec=True
samplePrefix="" #"Cut2Jet_v2_"
samplePrefixSysList="" #"Cut2Jet_v2_"
GeVtoMeV=1e3
useTransSR="" #"_trans"
#binEdges=[200*GeVtoMeV,500*GeVtoMeV,1000*GeVtoMeV,1500*GeVtoMeV,2000*GeVtoMeV,13000*GeVtoMeV]
binEdges=[200*GeVtoMeV, 450*GeVtoMeV, 700.0*GeVtoMeV,950.0*GeVtoMeV,1200.0*GeVtoMeV,1500.0*GeVtoMeV, 2200.0*GeVtoMeV,3000.0*GeVtoMeV,5000*GeVtoMeV]
cFactorsDiff=[0.449,0.459,0.469,0.475,0.481,0.474,0.470,0.470]
OneOverCDiff=[1.79989, 1.78463, 2.40636, 3.41163, 4.17634, 3.43792, 8.13463, 17.5494]
DiffXsec=[0.555589, 0.56034, 0.415566, 0.293115, 0.239444, 0.290873, 0.122931, 0.0569821 ]
#InclusiveCFactor=
variable="inc"
isDiff=True
pathPrefix="/Users/gaetano/eos_cernBox"
#pathPrefix="/eos"
mypath=pathPrefix+"/user/l/lbergste/merged_sys/Cut2Jet_v2/16a/with_sys/"
binningFile="observable_cfactors.txt"

#inList="list_all_local_v21_v2.txt"
#topSampleList="list_all_local_v21_v2_top.txt"

inList="inputFiles.txt"
topSampleList="inputFiles_top.txt"
#localNominal
#inList="list_all_local_v21_v1_preJERFix.txt"
#topSampleList="list_all_local_v21_v1_preJERFix.txt"

ReadLocal=False
normAllBckTotheory=False
onlyHistoSys=False # do not normalise systematics 
reReadInputs=False  #Used to re read the (modified) inputs directly from the histograms stored in the cache file, the cache file needs to be provided separetly
#singleSysListFile="sysVars_v20_v2.txt" #"sysVars_v20_v2_noJER.txt" #"sysVars_v20_v2.txt" #"sysVars_v20_v2_noJER.txt" #"sysVars_v20_v2.txt" #"sysVars_test.txt" #"sysVars_v20_v2.txt" #"sysVars_v20_final.txt" #"sysVars_v20.txt"
#singleSysListFile="sysVars_v21.txt"
singleSysListFile="sysVars_v21_test.txt"
UseSingleSysFile=True
SplitZeroOnejetSamples=True
SplitggFCRTopWWSamples=True
manualNorm=False

if topCRInSR :
    hasTopCR=True
if isCounting3D :
    hasTopCR=False 

sys1JetSuffix=""
sys2JetSuffix=""
sys3JetSuffix=""
sys1JetSuffixTop=""
sys2JetSuffixTop=""
sys3JetSuffixTop=""


#if decorrelateABCDSystematics :
#    sys1JetSuffix="Region1"
#    sys2JetSuffix="Region2"
#    sys3JetSuffix="Region3"
#    sys1JetSuffixTop="Region3"
#    sys2JetSuffixTop="Region2"
#    sys3JetSuffixTop="Region1"
    
myUserArgs = configMgr.userArg.split(' ')
#check for a user argument given with -u
print myUserArgs
mysamples=[]
samplesUsed=""
regions=""
considerSys=[]

for sam in myUserArgs :
    print sam
    if "variable" in sam :
        for s in sam.split(":") :
            if not "variable" in s :
                variable=s
        print "variable is " + variable
    if "samples" in sam :
        for s in sam.split("_") :
            if not "samples" in s :
                mysamples.append(s)
                if "top" in s :
                    inList=topSampleList
                samplesUsed+=s+"_"
                onlyHistoSys=True
    if "regions" in sam:
        for s in sam.split("_") :
            if not "regions" in s :
                regions+=s
                samplesUsed+=s+"_"
                onlyHistoSys=True
    if "syst" in sam:
        for s in sam.split("_") :
            if not "syst" in s :
                considerSys.append(s)
                onlyHistoSys=True
    if "reread" in sam :
        print "will be re reading inputs"
        reReadInputs=True
        reBinnedInputs=True
        manualNorm=True


#Discrimannt names in the ntuples:
#topWWDiscriminant="bdt_TopWWAll2"
topWWDiscriminant="bdt_TopWWAll"

bdt_vbFLow=0.5
bdt_vbFLowggF=0;
bdt_vbFLowInc=-1.0
bdt_vbfHigh=+1.0

sample_list=[]
samplec_list=[]
if not mysamples :
#    sample_list=["top","Vgamma","diboson","Zjets0","vbf0","ggf","vh","htt","Fakes","data"]
    sample_list=["top","Vgamma","diboson","Zjets0","vbf0","ggf","htt","vh","Fakes"]
#    sample_list=["top","diboson","Zjets0","vbf0","ggf","Fakes"]
else:
    sample_list=mysamples 
    print "Using partial dataset and only samples "
    print mysamples
    print regions 
    print samplesUsed
    
print "Will be running only on these sys "    
print considerSys 
myInputParser = OptionParser()
myInputParser.add_option('', '--SR', dest = 'SR', default = 'SRL')
myInputParser.add_option("--doSignalPlots", action='store_true', default=False, help="do signal plots")
(options, args) = myInputParser.parse_args(myUserArgs)
whichSR = options.SR
doSignalPlots = options.doSignalPlots


#---------------------------------------------------------------------------------------------
# Some flags for overridding normal execution and telling ROOT to shut up... use with caution!
#---------------------------------------------------------------------------------------------
#gROOT.ProcessLine("gErrorIgnoreLevel=10001;")
#configMgr.plotHistos = True

#---------------------------------------
# Flags to control which fit is executed
#---------------------------------------
#useStat=True
doValidation=False #use or use not validation regions to check exptrapolation to signal regions
#whattoplot=["C1"]

#-------------------------------
# Parameters for hypothesis test
#------------------------------
#configMgr.doHypoTest=False
#configMgr.nTOYs=1000
#configMgr.calculatorType=2
#configMgr.testStatType=3
#configMgr.nPoints=20
#configMgr.scanRange = (0., 2.)

#--------------------------------
# Now we start to build the model
#--------------------------------



anaName="hist_"
# First define HistFactory attributes
if is1D :
    anaName = anaName+ "v2_StefBinning_"+version+"_"+whichSR
    configMgr.analysisName = "hist_test"+whichSR
else   :
    anaName = anaName + "3D_v2_StefBinning_"+version+"_"+whichSR

if hasggFCR :
    anaName = anaName + "_ggFCR"

if doZeroOne:
    anaName = anaName + "ZeroOneJet"

if not buildSystematics :
    anaName = anaName + "_noSys"

if doSignalPlots:
    anaName = "Signal_" + anaName 

if doABCD:
    anaName = anaName+ "_ABCD"

if topCRInSR :
    anaName = anaName+ "_TopCRinSR"

if wwCRinSR :
    anaName =anaName + "_WWbin"

if doSignalPlots:
    anaName = anaName+ "Signal"+whichSR
    
if normAllBckTotheory :
    anaName = anaName+"_NoBckFloating"

if decorrelateABCDSystematics :    
    anaName = anaName +"_decorr1JetSys"

if isDiff :
    anaName+="_"+variable
    
configMgr.analysisName = anaName
    


##some overrrides
if isDiff :
    addggFDiscr=False

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 139 # Luminosity of input TTree after weighting
configMgr.outputLumi = 139 # Luminosity required for output histograms
configMgr.setLumiUnits("fb-1")
configMgr.blindSR=False
configMgr.blindCR=False
configMgr.useSignalInBlindedData=False #False per UpperLimit or exclusion

configMgr.prun = False #True
configMgr.prunThreshold = 0.0001
if reReadInputs :
    configMgr.prun = True #True
    configMgr.prunThreshold = 0.005
    configMgr.prunMethod = 1
configMgr.keepSignalRegionType = True
configMgr.writeXML = False
bkgFiles = []
sigFiles =[]

#activate using of background histogram cache file to speed up processes
if reReadInputs : 
    configMgr.useCacheToTreeFallback = False #enable the fallback to trees
    configMgr.useHistBackupCacheFile = False # enable the use of an alternate data file
    configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"
    configMgr.histBackupCacheFile =  "data/"+configMgr.analysisName+"_use"+".root"
    configMgr.readFromTree=False

else : 
    #activate using of background histogram cache file to speed up processes
    configMgr.useCacheToTreeFallback = False #enable the fallback to trees
    configMgr.useHistBackupCacheFile = False # enable the use of an alternate data file
    if not mysamples :
        configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"
    else :
        configMgr.histCacheFile = "data/"+configMgr.analysisName+"/"+configMgr.analysisName+"_Partial_"+samplesUsed+".root"
    configMgr.readFromTree=True
#    configMgr.histBackupCacheFile =  "data/"+configMgr.analysisName+"_use"+".root"


if configMgr.readFromTree: 
    if not ReadLocal : 
        finFiles=open(inList)
        linesF=finFiles.read().split("\n")
        for i in range(len(linesF)-1):
            if(linesF[i].find("#")>-1) :
                continue 
            bkgFiles.append(linesF[i])
#else:
#    bkgFiles = [configMgr.histCacheFile]
#    pass
#else :
#    bkgFiles.append("PxAOD_v19_r21_Laura_Sys_fitProc_3DStef_v3_0/nominal.root")
#if doZeroOne :
#    bkgFiles.append("PxAOD_v19_r21_Laura_Sys_fitProc_3DStef_v3_0/pxAODv18_Laura_sys_merged_Stef3Dv1_nominal_0_1jet.root") #check this!!
#else : 
#    bkgFiles = [configMgr.histCacheFile]
#    pass


nCats_dict={}
nCats=40
#nCats=10
#nCats=26
#nCats=95
if is1D :
    nCats = 25
    if reBinnedInputs :
        nCats=9
fit_list = []
strarray = []
all_regions_list=[]
topRegions=[]
wwRegions=[]
vbfRegions=[]
ggFregion1=[]
ggFregion2=[]
ggFregion3=[]
ggFregion4=[]
ggFRegions=[]

#myfile=open("cuts_PxAOD_v19_r21_Laura_Sys_fitProc_3DStefv2_0_v3_0.txt")
#myfile=open("cuts_PxAOD_v19_r21_Laura_Sys_fitProc_3DStef_0_v3_0.txt")
#myfile=open("StefBinning_v19_topWWVBFGGF_v1.txt")

#thenumbers=[]
#cvbf_b=[]
#cvbf_t=[]
#cww_b=[]
#cww_t=[]

#for i in range(0,nCats):
# nbr=myfile.readline().split(" ")
# thenumbers=[float(e) for e in nbr]
# cvbf_b.append(thenumbers[0])
# cvbf_t.append(thenumbers[1])
# cww_b.append(thenumbers[2])
# cww_t.append(thenumbers[3])


##### here need to define C factors and cross sections based on the input file
if isDiff : 
    observable_dict={}
    with open("observable_cfactors.txt") as cfactor_file:
        current_obs=''
        for line in cfactor_file:
            if line[0:3] not in ('c f','---'):
                current_obs=line.strip()
                print(current_obs)
                observable_dict[current_obs]={"binning":[],"cfactors":[],"oneOver":[],"diffXsec":[]}
            else:
                line_data=re.findall(r"-?\d+\.?\d+",line)
                print(line_data)
                line_data=[float(i) for i in line_data]
                if line_data==[]:
                    continue
                SF=1
                if  current_obs in ("Mjj","Mll","lep0_pt","lep1_pt","jet0_pt","jet1_pt","pt_H","Ptll"): 
                    SF=GeVtoMeV
                    pass
                print(line_data)
                if observable_dict[current_obs]["binning"]==[]:
                    observable_dict[current_obs]["binning"].append(line_data[0]*SF)
                    observable_dict[current_obs]["binning"].append(line_data[1]*SF)
                    observable_dict[current_obs]["cfactors"].append(line_data[2])
                    observable_dict[current_obs]["oneOver"].append(line_data[3])
                    observable_dict[current_obs]["diffXsec"].append(line_data[4])
                else:
                    observable_dict[current_obs]["binning"].append(line_data[1]*SF)
                    observable_dict[current_obs]["cfactors"].append(line_data[2])
                    observable_dict[current_obs]["oneOver"].append(line_data[3])
                    observable_dict[current_obs]["diffXsec"].append(line_data[4])


    binEdges=observable_dict[variable]["binning"]
    cFactorsDiff=observable_dict[variable]["cfactors"]
    OneOverCDiff=observable_dict[variable]["oneOver"]
    DiffXsec=observable_dict[variable]["diffXsec"]
    print "Bin edges"
    print  binEdges
    print  cFactorsDiff                                                  
    print  OneOverCDiff 
    print  DiffXsec

    if reBinnedInputs :
        print("ncats dict:")
        print("data/"+configMgr.analysisName+"_binning.txt")
        nCats_dict={}
        with open("data/"+configMgr.analysisName+"_binning.txt") as cat_file:
            for line in cat_file:
                print(line)
                nCats_dict[line.split()[0]]= int(line.split()[1])
        print nCats_dict
    else:
        nCats_dict={"SRVBF_"+str(i):25 for i in range(len(binEdges)-1)}
    for i in range(len(binEdges)-1):
        crtop="CRTop_"+str(i)
        if crtop in nCats_dict:
            continue
        else:
            nCats_dict[crtop]=topWWBins
else:
    if reBinnedInputs:
        print(line)
        nCats_dict={ line.split()[0] : int(line.split()[1]) for line in open("data/"+configMgr.analysisName+"_binning.txt") }


for s in binEdges :
    print "binEdgesMap[\""+variable+"\"].push_back("+ str(s) +");"
#exit()

if isCounting3D : 
    linesC=myfile.read().split("\n")
    nCats=len(linesC)
    for i in range(len(linesC)-1):
        print(linesC[i].replace('\"',''))
        configMgr.cutsDict["SRVBF"+str(i)]='('+linesC[i].replace('\"','')+')'
        entry = ("SRVBF"+str(i),"cuts")
        strtmp = ("SRVBF"+str(i))
        fit_list.append(entry)
        all_regions_list.append(entry)
        strarray.append(strtmp)
        ggFregions.append(entry)
        vbfRegions.append(entry)
        if topCRInSR :
            topRegions.append(entry)
        wwRegions.append(entry)

    print ("Will consider " , nCats , " singal region categories")

    iCount=0
    densX=0.01
    densY=1
    densZ=1

                
print ("Will consider " , nCats , " singal region categories")

#GUY:: get rid of this later
print(binEdges)


if is1D :
    if not topCRInSR :
        if addggFDiscr :
            if isDiff :
               for ibin in range(0 ,len(binEdges)-1) :
                    configMgr.cutsDict["SRVBF_"+str(ibin)]="inSR==1 && Mjj>450000&&DPhill<1.4&&bdt_vbf_"+variable+">"+str(bdt_vbFLow)+"&&"+variable+">="+ str(binEdges[ibin])+"&&"+varible+"<"+str(binEdges[ibin+1])
            else :
                configMgr.cutsDict["SRVBF"]="inSR==1 && bdt_vbf>"+str(bdt_vbFLow)
        else :
            if isDiff :
               for ibin in range(0 ,len(binEdges)-1) :
                    configMgr.cutsDict["SRVBF_"+str(ibin)]="inSR==1 && Mjj>450000&&DPhill<1.4&&"+str(bdt_vbFLow)+"&&"+variable+">="+ str(binEdges[ibin])+"&&"+varible+"<"+str(binEdges[ibin+1])
            else :
                configMgr.cutsDict["SRVBF"]="inSR==1"
    else :
         if addggFDiscr :
            if isDiff :
                for ibin in range(0 ,len(binEdges)-1) :
                     configMgr.cutsDict["SRVBF_"+str(ibin)]="inSR==1 && Mjj>450000&&DPhill<1.4&"+str(bdt_vbFLow)+"&&"+variable+">="+ str(binEdges[ibin])+"&&"+varible+"<"+str(binEdges[ibin+1])
            else :
                configMgr.cutsDict["SRVBF"]="inSR==1"
         else :
             if isDiff :
               for ibin in range(0 ,len(binEdges)-1) :
                    configMgr.cutsDict["SRVBF_"+str(ibin)]="inSR==1 && Mjj>450000&&DPhill<1.4&& bdt_vbf_"+variable+">"+str(bdt_vbFLow)+"&&"+variable+">="+ str(binEdges[ibin])+"&&"+variable+"<"+str(binEdges[ibin+1])
             else :
                 configMgr.cutsDict["SRVBF"]="inSR==1 && Mjj>450000&&DPhill<1.4&& bdt_vbf >"+str(bdt_vbFLow)
    if isDiff :
       for ibin in range(0 ,len(binEdges)-1) :
            vbfRegions.append(("SRVBF_"+str(ibin),"bdt_vbf_"+variable+useTransSR))
            fit_list.append(("SRVBF_"+str(ibin),"bdt_vbf_"+variable+useTransSR))
            all_regions_list.append(("SRVBF_"+str(ibin),"bdt_vbf_"+variable+useTransSR))
            if addggFDiscr :
                ggFRegions.append(("SRVBF_"+str(ibin),"bdt_vbf_"+variable+useTransSR))
            if topCRInSR :
                    topRegions.append((("SRVBF_"+str(ibin),"bdt_vbf_"+variable+useTransSR)))
                    wwRegions.append((("SRVBF_"+str(ibin),"bdt_vbf_"+variable+useTransSR)))
            strarray.append("SRVBF_"+str(ibin))
    else :
        vbfRegions.append(("SRVBF","bdt_vbf_"+useTransSR))
        fit_list.append(("SRVBF","bdt_vbf_"+useTransSR))
        all_regions_list.append(("SRVBF","bdt_vbf_"+useTransSR))
        if addggFDiscr :
            ggFRegions.append(("SRVBF","bdt_vbf_"+useTransSR))
        if topCRInSR :
            topRegions.append((("SRVBF","bdt_vbf_"+useTransSR)))
            wwRegions.append((("SRVBF","bdt_vbf_"+useTransSR)))
        strarray.append("SRVBF")
    if addggFDiscr :
        configMgr.cutsDict["SRGGF"] = "inSR==1 && bdt_vbf_"+variable+">-0.5 && bdt_vbf_"+variable+"<="+str(bdt_vbFLow)
        vbfRegions.append(("SRGGF","bdt_vbfggf"+useTransSR))
        fit_list.append(("SRGGF","bdt_vbfggf"+useTransSR))
        ggFRegions.append(("SRGGF","bdt_vbfggf"+useTransSR))
        if topCRInSR :
            topRegions.append((("SRGGF","bdt_vbfggf"+useTransSR)))
            wwRegions.append((("SRGGF","bdt_vbfggf"+useTransSR)))
else : #I think this bit needs fixing... # not implemented in the differential part 
    if not isCounting3D  :
        if not topCRInSR : 
            configMgr.cutsDict["SRVBF"]="inSR==1"
        else :
            if not wwCRinSR: 
                configMgr.cutsDict["SRVBF"]="inSR==1  && Mjj>450000&&DPhill<1.4&&bdt_vbf_"+variable+">"+str(bdt_vbFLow)
            else :
                configMgr.cutsDict["SRVBF"]="inSR==1&& Mjj>450000&&DPhill<1.4&& bdt_vbf_"+variable+">"+str(bdt_vbFLow)
        fit_list.append(("SRVBF","bdtProj"))
        all_regions_list.append(("SRVBF","bdtProj"))
        vbfRegions.append(("SRVBF","bdt_vbf_"+useTransSR))
        strarray.append("SRVBF")
#        ggFRegions.append(("SRVBF","bdtProj"))

#
# Background regions 
#


if not normAllBckTotheory :
#    configMgr.cutsDict["CRZjets"] = "inSR==1 && bdt_Zjets >= 0.0"
    configMgr.cutsDict["CRZjets"] = "inZjetsCR==1 && Mjj>450000"
#    entry2=("CRZjets","bdt_Zjets")
    entry2=("CRZjets","MT")
    all_regions_list.append(entry2)

    if not topCRInSR and hasTopCR :
        configMgr.cutsDict["CRTop"] = "inTopCR==1 && Mjj>450000&&DPhill<1.4&&"
        entry3=("CRTop","cuts")
        topRegions.append(entry3)
        all_regions_list.append(entry3)
    elif not isCounting3D and ( hasTopCR or topCRInSR ) :
        if isDiff :
           for ibin in range(0 ,len(binEdges)-1) :
                entry3=("CRTop_"+str(ibin),topWWDiscriminant)
                fit_list.append(entry3)
                wwRegions.append(entry3)
                topRegions.append(entry3)
                vbfRegions.append(entry3)
                all_regions_list.append(entry3)
                if not wwCRinSR:
                    if addggFDiscr :
                        configMgr.cutsDict["CRTop_"+str(ibin)] = "inSR==1 && Mjj>450000&&DPhill<1.4&& bdt_vbf_"+variable+"<=-0.5"+"&&"+variable+">="+ str(binEdges[ibin])+"&&"+variable+"<"+str(binEdges[ibin+1])
                    else:
                        configMgr.cutsDict["CRTop_"+str(ibin)] = "inSR==1 && Mjj>450000&&DPhill<1.4&& bdt_vbf_"+variable+"<="+str(bdt_vbFLow)+"&&"+variable+">="+ str(binEdges[ibin])+"&&"+variable+"<"+str(binEdges[ibin+1])
                else :
                    if addggFDiscr :
                        configMgr.cutsDict["CRTop_"+str(ibin)] = "inSR==1  && Mjj>450000&&DPhill<1.4&&  bdt_vbf_"+variable+"<=-0.5 &&"+ topWWDiscriminant +">=0.4 &&"+ topWWDiscriminant +"<1.0 && (bdt_TopWW*0.7+0.75)<="+topWWDiscriminant+"&&"+variable+">="+ str(binEdges[ibin])+"&&"+variable+"<"+str(binEdges[ibin+1])
                        configMgr.cutsDict["CRWW_"+str(ibin)] = "inSR==1 && bdt_vbf_"+variable+"<=-0.5 &&" + topWWDiscriminant + ">=0.4 &&"+ topWWDiscriminant + "<1.0 && (bdt_TopWW*0.7+0.75)>"+ topWWDiscriminant+"&&"+variable+">="+ str(binEdges[ibin])+"&&"+variable+"<"+str(binEdges[ibin+1])
                    else :
                        configMgr.cutsDict["CRTop_"+str(ibin)] = "inSR==1  && Mjj>450000&&DPhill<1.4&& bdt_vbf_"+variable+"<="+str(bdt_vbFLow) + "&&" + topWWDiscriminant +">=0.4 &&" + topWWDiscriminant + "<1.0 && (bdt_TopWW*0.7+0.75)<="+ topWWDiscriminant+"&&"+variable+">="+ str(binEdges[ibin])+"&&"+variable+"<"+str(binEdges[ibin+1])
                        configMgr.cutsDict["CRWW_"+str(ibin)] = "inSR==1  && bdt_vbf_"+variable+"<="+str(bdt_vbFLow) +  "&&" + topWWDiscriminant + ">=0.4 &&" + topWWDiscriminant + "<1.0 && (bdt_TopWW*0.7+0.75)>" + topWWDiscriminant+"&&"+variable+">="+ str(binEdges[ibin])+"&&"+variable+"<"+str(binEdges[ibin+1])
                    entry4=("CRWW_"+str(ibin),"bdt_TopWW")
                    topRegions.append(entry4)
                    wwRegions.append(entry4)
                    all_regions_list.append(entry4)
                    vbfRegions.append(entry4)
                    fit_list.append(entry4)
        else :
            entry3=("CRTop",topWWDiscriminant)
            fit_list.append(entry3)
            wwRegions.append(entry3)
            topRegions.append(entry3)
            vbfRegions.append(entry3)
            all_regions_list.append(entry3)
            if not wwCRinSR:
                if addggFDiscr :
                    configMgr.cutsDict["CRTop"] = "inSR==1 && bdt_vbf<=-0.5&& Mjj>450000&&DPhill<1.4&&"
                else:
                    configMgr.cutsDict["CRTop"] = "inSR==1  && Mjj>450000&&DPhill<1.4&&bdt_vbf_"+variable+"<="+str(bdt_vbFLow)
            else :
                if addggFDiscr :
                    configMgr.cutsDict["CRTop"] = "inSR==1  && Mjj>450000&&DPhill<1.4&&  bdt_vbf_"+variable+"<=-0.5 &&"+ topWWDiscriminant +">=0.4 &&"+ topWWDiscriminant +"<1.0 && (bdt_TopWW*0.7+0.75)<="+topWWDiscriminant
                    configMgr.cutsDict["CRWW"] = "inSR==1 && bdt_vbf<=-0.5 &&" + topWWDiscriminant + ">=0.4 &&"+ topWWDiscriminant + "<1.0 && (bdt_TopWW*0.7+0.75)>"+ topWWDiscriminant
                else :
                    configMgr.cutsDict["CRTop"] = "inSR==1  && Mjj>450000&&DPhill<1.4&&  bdt_vbf_"+variable+"<="+str(bdt_vbFLow) + "&&" + topWWDiscriminant +">=0.4 &&" + topWWDiscriminant + "<1.0 && (bdt_TopWW*0.7+0.75)<="+ topWWDiscriminant
                    configMgr.cutsDict["CRWW"] = "inSR==1  && bdt_vbf_"+variable+"<="+str(bdt_vbFLow) +  "&&" + topWWDiscriminant + ">=0.4 &&" + topWWDiscriminant + "<1.0 && (bdt_TopWW*0.7+0.75)>" + topWWDiscriminant
            entry4=("CRWW","bdt_TopWW")
            topRegions.append(entry4)
            wwRegions.append(entry4)
            all_regions_list.append(entry4)
            vbfRegions.append(entry4)
            fit_list.append(entry4)

            
if hasggFCR : 
    entry4=("CRGGF1","bdt_ggFCR1"+useTransSR)
    all_regions_list.append(entry4)
    ggFRegions.append(entry4)
    ggFregion1.append(entry4)
    configMgr.cutsDict["CRGGF1"] = "inggFCR1==1 && DPhill<1.4"
    
    entry5=("CRGGF2","bdt_ggFCR2"+useTransSR)
    all_regions_list.append(entry5)
    ggFRegions.append(entry5)
    ggFregion2.append(entry5)
    configMgr.cutsDict["CRGGF2"] = "inggFCR2==1&&DPhill<1.4"

if doABCD or doZeroOne:
    entry6=("CRGGF3","bdt_ggFCR3_CutDPhill")
    all_regions_list.append(entry6)
    ggFRegions.append(entry6)
    ggFregion3.append(entry6)
    configMgr.cutsDict["CRGGF3"] = "inggFCR3==1&&DPhill<1.4"

print(configMgr.cutsDict)




binEdgesFile=open("binEdgesFile.txt","w")
for s in binEdges :
    print "binEdgesMap[\""+variable+"\"].push_back("+ str(s) +");"
    binEdgesFile.write( "binEdgesMap[\""+variable+"\"].push_back("+ str(s) +");" +"\n")
binEdgesFile.close()

CutConfigFile=open( configMgr.analysisName+"_CutConfig.txt","w")
for x in configMgr.cutsDict :
    print str(x) + " " + configMgr.cutsDict[x]
    CutConfigFile.write(str(x) + " " + configMgr.cutsDict[x]+"\n")
CutConfigFile.close()

#exit(0)

weights = ["weight"]
configMgr.weights = (weights)
# List of samples and their plotting colours



systematicsPerSample={}
sys_in_samples=set()

systematicsPerSample1jet1={}
sys_in_samples1jet1=set()

systematicsPerSample1jet2={}
sys_in_samples1jet2=set()

systematicsPerSample1jet3={}
sys_in_samples1jet3=set()


ThsystematicsPerSample={}
ThsystematicsPerSample1jet1={}
ThsystematicsPerSample1jet2={}
ThsystematicsPerSample1jet3={}

for a in sample_list :
    if not readSystematics :
        continue
    if a == "data" :
        continue 
    systematicThisSample=[]
    systematicThisSampleDiffSig={}
    for ibin in range(0 ,len(binEdges)-1) :
        systematicThisSampleDiffSig[samplePrefix+"vbf0_"+str(ibin)]=[]
    systematicThisSample1jet1=[]
    systematicThisSample1jet2=[]
    systematicThisSample1jet3=[]
    print "Checking sample "+a 
#    if reReadInputs:
#        fil=open("data/sysList_"+samplePrefixSysList+a+".txt")
#    else :
    fil=open(singleSysListFile)
#        if UseSingleSysFile :
#            fil=open(singleSysListFile)
#        else : 
#            fil=open("sysList_"+samplePrefixSysList+a+".txt")
    line=fil.read().split("\n")
    for i in range(len(line)-1):
#        continue
        if(line[i].find("#")>-1) :
            continue
        if(line[i].find("nominal")>-1) :
            continue
        if("_FF_" in line[i] and a!="Fakes"):
            print(line[i])
            continue
        if(a=="Fakes" and "_FF_" not in line[i]):
            continue
#        if ( line[i].find("JET_EffectiveNP_Statistical4")>-1) : #hZjets0
#            continue
#        if ( line[i].find("JET_EtaIntercalibration_NonClosure_posEta")>-1) :#hZjets0
#            continue 
#        if (line[i].find("JET_JER_EffectiveNP_5")>-1) :#hZjets0
#            continue
#        if( line[i].find("EL_EFF_ID_CorrUncertaintyNP4")>-1): #htop
#            continue
#        if( line[i].find("EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13")>-1): #htop
#            continue
        if( line[i].find("EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9")>-1) : #hggf
            continue
#        if (line[i].find("FT_EFF_Eigen_Light_3")>-1) : #hggf
#            continue
#        if( line[i].find("JET_EffectiveNP_Mixed3")>-1) : #hggf
#            continue
#        if( line[i].find("JET_EtaIntercalibration_NonClosure_negEta")>-1) : #hggf
#            continue
#        if( line[i].find("JET_PunchThrough_MC16")>-1 ) : #hggf
#            continue
#        if( line[i].find("JET_JER_DataVsMC_MC16")>-1) : #hggf3
#            continue
#        if( line[i].find("JET_JER_EffectiveNP")>-1):
#            continue
#        if( line[i].find("EG_SCALE_AF2")>-1) :
#            continue
#        if (line[i].find("EG_SCALE")>-1) :
#            continue
#        if (line[i].find("EL_EFF_ID_CorrUncertaintyNP0")>-1) :
#            continue
#        if (line[i].find("EL_EFF_ID_CorrUncertaintyNP10")>-1) :
#            continue
#        if (line[i].find("EL_EFF_ID_CorrUncertaintyNP11")>-1) :
#            continue
#        if (line[i].find("EL_EFF_ID_CorrUncertaintyNP12")>-1) :
#            continue
#        if (line[i].find("EL_EFF_ID_CorrUncertaintyNP13")>-1) :
#            continue
#        if (line[i].find("EL_EFF_ID_CorrUncertaintyNP14")>-1) :
#            continue
#        if (line[i].find("EL_EFF_ID_CorrUncertainty")>-1) :
#            continue
#        if (line[i].find("EL_EFF_ID_SIMPLIFIED_UncorrUncertainty")>-1) :
#            continue
#        if (line[i].find("EL_EFF_")>-1) :
#            continue
#        if (line[i].find("FT_EFF_Eigen_")>-1) :
#            continue
#        if (line[i].find("FT_EFF_extrapolation")>-1) :
#            continue
#        if (line[i].find("JET_BJES_")>-1) :
#            continue
#        if (line[i].find("JET_EffectiveNP_")>-1) :
#            continue
#        if (line[i].find("JET_EtaIntercalibration")>-1) :
#            continue
#        print "reading sys " +line[i]
#        if("EL_EFF_ID_CorrUncertaintyNP5" not in  line[i]) :
#            print "considering "+line[i]
#            continueno
        if normAllBckTotheory or onlyHistoSys:
            if( a == "vbf0" and isDiff ) :
#                sys_in_samples.add(a)
                for ibin in range(0 ,len(binEdges)-1) :
                    systematicThisSampleDiffSig[samplePrefix+"vbf0_"+str(ibin)].append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample[samplePrefix+"vbf0_"+str(ibin)]=systematicThisSampleDiffSig[samplePrefix+"vbf0_"+str(ibin)]
                    sys_in_samples.add("vbf0_"+str(ibin))
                    print "Adding systematic " + systematicThisSampleDiffSig[samplePrefix+"vbf0_"+str(ibin)][-1].name + " to " + samplePrefix+"vbf0_"+str(ibin) + " for "+a 
            else :
                systematicThisSample.append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                systematicsPerSample[a]=systematicThisSample
                sys_in_samples.add(a)
            if( a == "diboson" or a == "top" ) :
                if ( SplitZeroOnejetSamples and doZeroOne ) :
                    systematicThisSample1jet1.append(Systematic(samplePrefix+line[i]+sys1JetSuffixTop,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet1[a+"1"]=systematicThisSample1jet1
                    sys_in_samples1jet1.add(a+"1")
                if SplitggFCRTopWWSamples :
                    systematicThisSample1jet2.append(Systematic(samplePrefix+line[i]+sys2JetSuffixTop,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet2[a+str(2)]=systematicThisSample1jet2
                    sys_in_samples1jet2.add(a+str(2))

                    systematicThisSample1jet3.append(Systematic(samplePrefix+line[i]+sys3JetSuffixTop,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet3[a+str(3)]=systematicThisSample1jet3
                    sys_in_samples1jet3.add(a+str(3))
            if( a == "ggf") :
                if doABCD :
                    systematicThisSample1jet1.append(Systematic(samplePrefix+line[i]+sys1JetSuffix,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet1[a+str(1)]=systematicThisSample1jet1
                    sys_in_samples1jet1.add(a+str(1))
                    
                    systematicThisSample1jet2.append(Systematic(samplePrefix+line[i]+sys2JetSuffix,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet2[a+str(2)]=systematicThisSample1jet2
                    sys_in_samples1jet2.add(a+str(2))
                    
                    systematicThisSample1jet3.append(Systematic(samplePrefix+line[i]+sys3JetSuffix,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet3[a+str(3)]=systematicThisSample1jet3
                    sys_in_samples1jet3.add(a+str(3))




            if( a == "Zjets0") :
                if doZeroOne and SplitZeroOnejetSamples :
                    systematicThisSample1jet1.append(Systematic(samplePrefix+line[i]+sys1JetSuffix,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet1["Zjets1"]=systematicThisSample1jet1 #GUY::EDIT
                    sys_in_samples1jet1.add("Zjets1")
            if(a=="Fakes"):
                systematicThisSample.append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                systematicsPerSample[a]=systematicThisSample
                sys_in_samples.add(a)


        else :
            if (a == "diboson" ) :
                systematicThisSample.append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                systematicsPerSample[a]=systematicThisSample
                sys_in_samples.add(a)
                if SplitZeroOnejetSamples and doZeroOne :
                    systematicThisSample1jet1.append(Systematic(samplePrefix+line[i]+sys1JetSuffixTop,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet1[a+"1"]=systematicThisSample1jet1
                    sys_in_samples1jet1.add(a+"1")
                if SplitggFCRTopWWSamples :    
                    systematicThisSample1jet2.append(Systematic(samplePrefix+line[i]+sys2JetSuffixTop,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet2[a+str(2)]=systematicThisSample1jet2
                    sys_in_samples1jet2.add(a+str(2))

                    systematicThisSample1jet3.append(Systematic(samplePrefix+line[i]+sys3JetSuffixTop,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet3[a+str(3)]=systematicThisSample1jet3
                    sys_in_samples1jet3.add(a+str(3))

            if (a == "top" ) :
                systematicThisSample.append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                systematicsPerSample[a]=systematicThisSample
                sys_in_samples.add(a)
                if SplitZeroOnejetSamples and doZeroOne :
                    systematicThisSample1jet1.append(Systematic(samplePrefix+line[i]+sys1JetSuffix,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet1[a+"1"]=systematicThisSample1jet1
                    sys_in_samples1jet1.add(a+"1")
                if SplitggFCRTopWWSamples:    
                    systematicThisSample1jet2.append(Systematic(samplePrefix+line[i]+sys2JetSuffix,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet2[a+str(2)]=systematicThisSample1jet2
                    sys_in_samples1jet2.add(a+str(2))

                    systematicThisSample1jet3.append(Systematic(samplePrefix+line[i]+sys3JetSuffix,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet3[a+str(3)]=systematicThisSample1jet3
                    sys_in_samples1jet3.add(a+str(3))
            if (a == "vbf" or a == "vbf0"):
                if( isDiff ) :
                     for ibin in range(0 ,len(binEdges)-1) :
                        sys_in_samples.add("vbf0_"+str(ibin))
                        systematicThisSampleDiffSig[samplePrefix+"vbf0_"+str(ibin)].append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                        systematicsPerSample[samplePrefix+"vbf0_"+str(ibin)]=systematicThisSampleDiffSig[samplePrefix+"vbf0_"+str(ibin)]
                        sys_in_samples.add(samplePrefix+"vbf0_"+str(ibin))
                        print "Adding systematic " + systematicThisSampleDiffSig[samplePrefix+"vbf0_"+str(ibin)][-1].name + " to " + samplePrefix+"vbf0_"+str(ibin) + " for "+a 
                else :
                    systematicThisSample.append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample[a]=systematicThisSample
                    sys_in_samples.add(a)
            if( a == "Vgamma" ):
                continue 
                systematicThisSample.append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                systematicsPerSample[a]=systematicThisSample
                sys_in_samples.add(a)
            if( a == "vh" ):
                continue
                systematicThisSample.append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                systematicsPerSample[a]=systematicThisSample
                sys_in_samples.add(a)
            if( a == "htt" ):
                continue
                systematicThisSample.append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                systematicsPerSample[a]=systematicThisSample
                sys_in_samples.add(a)
            if( a == "Fakes" ):
                systematicThisSample.append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                systematicsPerSample[a]=systematicThisSample
                sys_in_samples.add(a)

 
            if( a == "ggf") :
                systematicThisSample.append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                systematicsPerSample[a]=systematicThisSample
                sys_in_samples.add(a)
                if doABCD :
                    systematicThisSample1jet1.append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
#                    systematicThisSample1jet1.append(Systematic(samplePrefix+line[i]+sys1JetSuffix,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet1[a+str(1)]=systematicThisSample1jet1
                    sys_in_samples1jet1.add(a+str(1))
                    
                    systematicThisSample1jet2.append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
#                    systematicThisSample1jet2.append(Systematic(samplePrefix+line[i]+sys2JetSuffix,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet2[a+str(2)]=systematicThisSample1jet2
                    sys_in_samples1jet2.add(a+str(2))
                                            
                    systematicThisSample1jet3.append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
#                    systematicThisSample1jet3.append(Systematic(samplePrefix+line[i]+sys3JetSuffix,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet3[a+str(3)]=systematicThisSample1jet3
                    sys_in_samples1jet3.add(a+str(3))

            if( a == "Zjets0") :
                systematicThisSample.append(Systematic(samplePrefix+line[i],"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                systematicsPerSample[a]=systematicThisSample
                sys_in_samples.add(a)
                if doZeroOne and SplitZeroOnejetSamples :
                    systematicThisSample1jet1.append(Systematic(samplePrefix+line[i]+sys1JetSuffix,"_nominal","_"+line[i]+"__1up","_"+line[i]+"__1down","tree",histo_mode))
                    systematicsPerSample1jet1["Zjets1"]=systematicThisSample1jet1
                    sys_in_samples1jet1.add("Zjets1")
print systematicsPerSample
#Add theory uncertainties 

#vbfTheor=['theo_VBF_shower', 'theo_VBF_qcd_muRmuF', 'theo_VBF_pdf4lhc', 'theo_VBF_alphas']
#ggfTheor=['theo_ggf_pdf4lhc', 'theo_ggf_alphas', 'theo_ggf_shower', 'theo_ggf_qcd_mu', 'theo_ggf_qcd_res', 'theo_ggf_qcd_mig01', 'theo_ggf_qcd_mig12','theo_ggf_qcd_qmt', 'theo_ggf_qcd_pTH', 'theo_ggf_qcd_vbf2j', 'theo_ggf_qcd_vbf3j']
#ggfTheor=['theo_ggf_alphas', 'theo_ggf_shower', 'theo_ggf_qcd_mu', 'theo_ggf_qcd_res', 'theo_ggf_qcd_mig01', 'theo_ggf_qcd_mig12','theo_ggf_qcd_qmt', 'theo_ggf_qcd_pTH', 'theo_ggf_qcd_vbf2j', 'theo_ggf_qcd_vbf3j']
#ZjetsTheor = [ 'theo_ztautau_pdf', 'theo_ztautau_alphas', 'theo_ztautau_generator', 'theo_ztautau_scale' ]
#dibosonTheor = [ 'theo_ww_scale', 'theo_ww_alphas', 'theo_ww_pdf' ]
#topTheor = [ 'theo_ttbar_scale', 'theo_ttbar_shower', 'theo_ttbar_matching', 'theo_ttbar_isr', 'theo_ttbar_fsr', 'theo_ttbar_pdf' ]

#For the inclusive 
vbfTheor=['theo_vbf_shower', 'theo_vbf_scale', 'theo_vbf_pdf4lhc','theo_vbf_generator',"theo_vbf_alphas"]
ggfTheor=["theo_ggF_qcd_wg1_mu","theo_ggF_qcd_wg1_res","theo_ggF_qcd_wg1_mig01","theo_ggF_qcd_wg1_mig12","theo_ggF_qcd_wg1_vbf2j","theo_ggF_qcd_wg1_vbf3j","theo_ggF_qcd_wg1_qm_t","theo_ggF_qcd_wg1_pTH","theo_ggF_pdf4lhc","theo_ggF_alphas","theo_ggF_shower"]
ZjetsTheor = ['theo_ztautau_generator', 'theo_ztautau_pdf', 'theo_ztautau_alphas', 'theo_ztautau_scale']#,"theo_zjets_artificial"]
dibosonTheor = [ 'theo_ww_scale', 'theo_ww_alphas', 'theo_ww_pdf',"theo_ww_CSSKIN","theo_ww_shower","theo_ww_CKKW" ,"theo_ww_QSF"]#,"theo_ww_artificial"]
topTheor = ["theo_ttbar_matching",'theo_ttbar_scale', 'theo_ttbar_isr', 'theo_ttbar_fsr', 'theo_ttbar_pdf',"theo_ttbar_shower"]#,"theo_top_artificial" ]

#For the differential 
#vbfTheor=[ ]
#ggfTheor=[]
#ZjetsTheor = [ ]
#dibosonTheor = [ ]
#topTheor = [ ] 

if doTheorErrors :
    if(reReadInputs and buildSystematics) :
        ThsystematicThisSample=[]
        ThsystematicThisSample1jet1=[]
        ThsystematicThisSample1jet2=[]
        ThsystematicThisSample1jet3=[]
        for sys in vbfTheor :
            if (isDiff ):
                ThsystematicThisSample.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
                for ibin in range(0 ,len(binEdges)-1) :
                    ThsystematicsPerSample[samplePrefix+"vbf0_"+str(ibin)]=ThsystematicThisSample;
            else :
                ThsystematicThisSample.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
                ThsystematicsPerSample["vbf0"]=ThsystematicThisSample;
                #        sys_in_samples.add("vbf0")
        print("vbf theory systematics")
        #print([s.name for s in ThsystematicsPerSample["vbf0_0"]])
        ThsystematicThisSample=[]
        ThsystematicThisSample1jet1=[]
        ThsystematicThisSample1jet2=[]
        ThsystematicThisSample1jet3=[]
        for sys in ggfTheor :
            ThsystematicThisSample.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
            ThsystematicsPerSample["ggf"]=ThsystematicThisSample
            #        sys_in_samples.add("ggf")
            print("doing ggf theory")
            if doABCD :
                ThsystematicThisSample1jet1.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
                ThsystematicsPerSample1jet1["ggf"+str(1)]=ThsystematicThisSample1jet1
                print("added ggf1 theory")
                #            sys_in_samples1jet1.add("ggf"+str(1))
        
                ThsystematicThisSample1jet2.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
                ThsystematicsPerSample1jet2["ggf"+str(2)]=ThsystematicThisSample1jet2
                #            sys_in_samples1jet2.add("ggf"+str(2))
            
                ThsystematicThisSample1jet3.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
                ThsystematicsPerSample1jet3["ggf"+str(3)]=ThsystematicThisSample1jet3

                #            sys_in_samples1jet3.add("ggf"+str(3))
        ThsystematicThisSample=[]
        ThsystematicThisSample1jet1=[]
        ThsystematicThisSample1jet2=[]
        ThsystematicThisSample1jet3=[]   
        for sys in ZjetsTheor :
            ThsystematicThisSample.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
            ThsystematicsPerSample["Zjets0"]=ThsystematicThisSample
            sys_in_samples.add("Zjets0") 
            if doZeroOne and SplitZeroOnejetSamples :
                ThsystematicThisSample1jet1.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
                ThsystematicsPerSample1jet1["Zjets"+str(1)]=ThsystematicThisSample1jet1
                sys_in_samples1jet1.add("Zjets"+str(1))
        ThsystematicThisSample=[]
        ThsystematicThisSample1jet1=[]
        ThsystematicThisSample1jet2=[]
        ThsystematicThisSample1jet3=[]
        for sys in dibosonTheor :
            ThsystematicThisSample.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
            ThsystematicsPerSample["diboson"]=ThsystematicThisSample

            if SplitZeroOnejetSamples and doZeroOne :
                ThsystematicThisSample1jet1.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
                ThsystematicsPerSample1jet1["diboson"+str(1)]=ThsystematicThisSample1jet1
                
            if SplitggFCRTopWWSamples: 
                ThsystematicThisSample1jet2.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
                ThsystematicsPerSample1jet2["diboson"+str(2)]=ThsystematicThisSample1jet2
        
                ThsystematicThisSample1jet3.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
                ThsystematicsPerSample1jet3["diboson"+str(3)]=ThsystematicThisSample1jet3

        ThsystematicThisSample=[]
        ThsystematicThisSample1jet1=[]
        ThsystematicThisSample1jet2=[]
        ThsystematicThisSample1jet3=[]
        for sys in topTheor :
            ThsystematicThisSample.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
            ThsystematicsPerSample["top"]=ThsystematicThisSample

            if SplitZeroOnejetSamples and doZeroOne :
                ThsystematicThisSample1jet1.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
                ThsystematicsPerSample1jet1["top"+str(1)]=ThsystematicThisSample1jet1
                
            if SplitggFCRTopWWSamples: 
                ThsystematicThisSample1jet2.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
                ThsystematicsPerSample1jet2["top"+str(2)]=ThsystematicThisSample1jet2

       
                ThsystematicThisSample1jet3.append(Systematic(samplePrefix+sys,"_nominal","_"+sys+"__1up","_"+sys+"__1down","tree",histo_mode))
                ThsystematicsPerSample1jet3["top"+str(3)]=ThsystematicThisSample1jet3

        
        
configMgr.nomName = "_nominal"

print (fit_list)
VBFssamples=[]
if "vbf0" in sample_list : 
    if isDiff :
        for ibin in range(0 ,len(binEdges)-1) :
            VBF =  Sample(samplePrefix+"vbf0_"+str(ibin),kRed)
            VBF.prefixTreeName="vbf"
            #VBF.additionalCuts=variable+">="+ str(binEdges[ibin])+"&&"+variable+"<"+str(binEdges[ibin+1]) #GUY::fix this
            samplec_list.append(VBF)
            VBFssamples.append(VBF)
            if doFidCrossSec :
                VBF.addNormFactor("mu_vbf",2.068,2.068,2.068,False)
#                VBF.addNormFactor("mu_vbf_bin_"+str(ibin),DiffXsec[ibin]/(GeVtoMeV*( binEdges[ibin+1]-binEdges[ibin])),20,0,False)
                VBF.addNormFactor("mu_vbf_bin_"+str(ibin),DiffXsec[ibin],20,0,False)
                VBF.addNormFactor("OneOverC_bin_"+str(ibin),OneOverCDiff[ibin],-1*OneOverCDiff[ibin],OneOverCDiff[ibin],True)
 #               VBF.addNormFactor("BinWidth_bin_"+str(ibin),GeVtoMeV*(binEdges[ibin+1]-binEdges[ibin]),GeVtoMeV*(binEdges[ibin+1]-binEdges[ibin]),GeVtoMeV*(binEdges[ibin+1]-binEdges[ibin]),True)
                VBF.addNormFactor("matrix_bin+"+str(ibin),-1,1,True)
            else :
                VBF.addNormFactor("mu_vbf",1,1,1,False)
                VBF.addNormFactor("mu_vbf_bin_"+str(ibin),1,5,-5,False)
            VBF.setStatConfig(vbf_stat)
            if not manualNorm :
                VBF.setNormRegions(vbfRegions)
            else :
                VBF.setNormByTheory()
    else :
        VBF = Sample(samplePrefix+"vbf0",kRed)
        VBF.prefixTreeName="vbf"
        samplec_list.append(VBF)
        if doFidCrossSec :
            VBF.addNormFactor("mu_vbf",2.068,20,0,False)
    #        VBF.addNormFactor("lumi_sig",139,139,139,True);
            VBF.addNormFactor("OneOverC",0.48355,-0.48355,0.48355,True)
        else :
            VBF.addNormFactor("mu_vbf",1,5,0,False)
        VBF.setStatConfig(vbf_stat)
        if not manualNorm :
            VBF.setNormRegions(vbfRegions)
        else :
            VBF.setNormByTheory()
        VBFssamples.append(VBF)

if "diboson" in sample_list :
    WW = Sample(samplePrefix+"diboson",kBlue)
    samplec_list.append(WW)
    if SplitZeroOnejetSamples and not SplitggFCRTopWWSamples : 
        WW.additionalCuts="inggFCR3!=1"
    elif SplitZeroOnejetSamples and  SplitggFCRTopWWSamples :
        WW.additionalCuts="inggFCR3!=1 && inggFCR2!=1 && inggFCR1!=1"
    elif SplitggFCRTopWWSamples and not SplitZeroOnejetSamples :
        WW.additionalCuts="inggFCR2!=1 && inggFCR1!=1"
    if normAllBckTotheory :
        WW.setNormByTheory()
    else :
        if commonTopWWParameter:
            WW.addNormFactor("mu_ww",1.,1,1.,True)
            WW.addNormFactor("mu_wwTop",1.,5,-5,False)
        else : 
            WW.setNormFactor("mu_ww",1.,-5,5.)
        WW.setStatConfig(back_stat)
        if not manualNorm :
            WW.setNormRegions(wwRegions)
        else :
            WW.setNormByTheory() 
if "ggf" in sample_list: 
    GGF = Sample(samplePrefix+"ggf",kGreen)
    samplec_list.append(GGF)
    if doABCD :
        if SplitZeroOnejetSamples :
            GGF.additionalCuts="inggFCR1!=1 && inggFCR2!=1 && inggFCR3!=1 && inSR==1"
        else :
            GGF.additionalCuts="inggFCR1!=1 && inggFCR2!=1 && inggFCR3!=1"
        if normAllBckTotheory :
            GGF.setNormByTheory()
        else :
            if hasggFCR :
                if doABCD:
#                    ggFRegions=vbfRegions
#                    GGF.addNormFactor("mu_ggf",1.,100.,-100.,False)
                    GGF.addNormFactor("mu_ggf",1.,1.,+1.,False)
                    GGF.addNormFactor("mu_ggf_2",1.,100.,-100.,False)
                    GGF.addNormFactor("mu_ggf_1",1.,100.,-100.,False)
                    GGF.addNormFactor("mu_ggf_3",1.,100.,-100.,False)
#                    GGF.addNormFactor("mu_ggf_1",1.,100.,-100.)
#                    GGF.addNormFactor("mu_ggf_2",1.,100.,-100.)
                else :
                    GGF.setNormFactor("mu_ggf",1.,-5,5.)
                if not manualNorm :
                    GGF.setNormRegions(ggFRegions)
                else :
                    GGF.setNormByTheory()
    else :
        GGF.setNormByTheory()
        #GGF.setNormFactor("mu_ggf",1.,1.0,1.0)
    GGF.setStatConfig(back_stat)
    #GGF.setNormRegions(fit_list)
    #GGF.setNormRegions(all_regions_list)
    #GGF.addSampleSpecificWeight("0.039491")
    #GGF.addSampleSpecificWeight("139./36.1")
    #GGF.setStatConfig(useStat)
    if doABCD :

        GGF1 = Sample("ggf1",kGreen)
        samplec_list.append(GGF1)
        GGF1.additionalCuts="inggFCR1==1"
        GGF1.prefixTreeName="ggf"
#        GGF1.addNormFactor("mu_ggf",1.,5,-5.,False)
        GGF1.addNormFactor("mu_ggf_1",1.,5,-5.,False)
#        GGF1.addNormFactor("mu_ggf_2",1.,100,-100.,False)
        GGF1.setStatConfig(back_stat)
        if not manualNorm :
            GGF1.setNormRegions(ggFregion1)
        else :
            GGF1.setNormByTheory()

        GGF2 = Sample("ggf2",kGreen)
        GGF2.additionalCuts="inZjetsCR==1"
        samplec_list.append(GGF2)
        GGF2.prefixTreeName="ggf"
        GGF2.setStatConfig(back_stat)
        GGF2.setNormByTheory()


        GGF3 = Sample("ggf3",kGreen)
        samplec_list.append(GGF3)

        GGF3.additionalCuts= "inggFCR3==1"
#       Patch for now
        GGF3.prefixTreeName="ggf"
#        GGF3.addNormFactor("mu_ggf",1.,5.,-5.)
        GGF3.addNormFactor("mu_ggf_1",1.,5,-5,False)
        GGF3.addNormFactor("mu_ggf_3",1.,5.,-5.)
        GGF3.setStatConfig(back_stat)
        if not manualNorm :
            GGF3.setNormRegions(ggFregion3)
        else :
            GGF3.setNormByTheory()
            


if "Zjets0" in sample_list: 
    Zjets = Sample(samplePrefix+"Zjets0",kPink)
    Zjets.prefixTreeName="Zjets"
    samplec_list.append(Zjets)
    Zjets.setStatConfig(back_stat)
    if SplitZeroOnejetSamples : 
        Zjets.additionalCuts="inggFCR1!=1 && inggFCR2!=1 && inggFCR3!=1"
    if normAllBckTotheory : 
        Zjets.setNormByTheory()
    else :
        Zjets.setNormFactor("mu_Zjets",1.,0.,5.)
#        Zjets.setNormRegions([("CRZjets","bdt_Zjets")])
#        Zjets.setNormRegions(fit_list)
        #Zjets.setNormRegions(all_regions_list)
        if not manualNorm :
            Zjets.setNormRegions([("CRZjets","MT")])
        else :
            Zjets.setNormByTheory()
        
        

if "top" in sample_list :     
    Top = Sample(samplePrefix+"top",kMagenta)
    samplec_list.append(Top)
    if SplitZeroOnejetSamples and not SplitggFCRTopWWSamples : 
        Top.additionalCuts="inggFCR3!=1"
    elif SplitZeroOnejetSamples and  SplitggFCRTopWWSamples :
        Top.additionalCuts="inggFCR3!=1 && inggFCR2!=1 && inggFCR1!=1"
    elif SplitggFCRTopWWSamples and not SplitZeroOnejetSamples :
        Top.additionalCuts="inggFCR2!=1 && inggFCR1!=1"
    if normAllBckTotheory : 
        Top.setNormByTheory()
    else :
        if commonTopWWParameter :
            Top.addNormFactor("mu_top",1.,1,1.,True)
            Top.addNormFactor("mu_wwTop",1.,5.,-5.,False)
        else :
            Top.setNormFactor("mu_top",1.,-5.,+5.,False)
        if not manualNorm :
            Top.setNormRegions(topRegions)
        else :
            Top.setNormByTheory()
    Top.setStatConfig(back_stat)
#Top.setNormRegions(fit_list)
#Top.setNormRegions(all_regions_list)
#Top.setNormRegions([("CRTop","bdt_WW_trans"),fit_list[0]])
#Top.setNormRegions([("CRTop","bdt_WW_trans")])
#Top.setNormByTheory()
#Top.setNormRegions([("CRZjets","MT"),("CRTop","DYjj"),("SRWW","bdt_ww"),("SRVBF","bdt_vbf_"),("SRGGF","bdt_ggf")])'
#Top.setNormRegions([("SRVBF000","cuts")])
#Top.setNormRegions(all_regions_list)
#Top.addSampleSpecificWeight("0.079887")
#Top.addSampleSpecificWeight("139./36.1")
    
if "vh" in sample_list: 
    vh = Sample("vh",kRed+4)
    samplec_list.append(vh)
    vh.setStatConfig(back_stat)
    vh.setNormByTheory()

if "htt" in sample_list :     
    htt = Sample("htt",kRed+6)
    samplec_list.append(htt)
    htt.setStatConfig(back_stat)
    htt.setNormByTheory()

if "Vgamma" in sample_list: 
    Vgamma = Sample(samplePrefix+"Vgamma",kYellow)
    samplec_list.append(Vgamma)
    Vgamma.setStatConfig(back_stat)
    Vgamma.setNormByTheory()
    #Vgamma.setNormRegions(all_regions_list)
    #Vgamma.addSampleSpecificWeight("0.039785")
    #Vgamma.addSampleSpecificWeight("139./36.1")
    #Vgamma.setNormRegions(fit_list)

if "fakeE" in sample_list :
    FakeE = Sample("FakeE",kOrange+2)
    samplec_list.append(FakeE)
    FakeE.setStatConfig(back_stat)
    FakeE.setNormByTheory()

if "fakeM" in sample_list :
    FakeM = Sample("FakeM",kOrange+4)
    samplec_list.append(FakeM)
    FakeM.setStatConfig(back_stat)
    FakeM.setNormByTheory()
    
if "Fakes" in sample_list :
    Fakes = Sample("Fakes",kPink+4)
    samplec_list.append(Fakes)
    Fakes.setStatConfig(back_stat)
    Fakes.setNormByTheory()

if "data" in sample_list :
    data = Sample(samplePrefix+"data_nominal",kBlack)
    samplec_list.append(data)
    data.setData()

# Insert MC estimates for top and diboson for the 0-1 jet regions
if doZeroOne and SplitZeroOnejetSamples :
    if "diboson" in sample_list :
        WW1 = Sample(samplePrefix+"diboson1",kMagenta)
        samplec_list.append(WW1)
        WW1.additionalCuts="inZjetsCR==1"
        WW1.prefixTreeName="diboson"
#        WW1.setNormByTheory()    
        WW1.setStatConfig(back_stat)

        WW1.setNormByTheory()
        #WW1.setStatConfig(back_stat)


        
    if "top" in sample_list :     
        Top1 = Sample(samplePrefix+"top1",kMagenta)
        samplec_list.append(Top1)
        Top1.additionalCuts="inZjetsCR==1"
        Top1.prefixTreeName="top"
        Top1.setNormByTheory()    
        Top1.setStatConfig(back_stat)

        
    if "Zjets0" in sample_list :    
        Zjets1 = Sample(samplePrefix+"Zjets1",kMagenta)
        samplec_list.append(Zjets1)
        Zjets1.additionalCuts="inggFCR1==1"
        Zjets1.prefixTreeName="Zjets"
        Zjets1.addNormFactor("mu_Zjets_CRGGF1",1.,5.,-5.,False)
        Zjets1.setNormByTheory()    
        Zjets1.setStatConfig(back_stat)

if SplitggFCRTopWWSamples :
    if "diboson" in sample_list :
        WW2 = Sample(samplePrefix+"diboson2",kMagenta)
        samplec_list.append(WW2)
        WW2.additionalCuts="inggFCR2==1"
        WW2.prefixTreeName="diboson"
        if normAllBckTotheory :
            WW2.setNormByTheory()
        else :
            if commonTopWWParameter:
                WW2.addNormFactor("mu_ww2",1.,1,1.,True)
                WW2.addNormFactor("mu_wwTop2",1.,5,-5,False)
            else : 
                WW2.setNormFactor("mu_ww2",1.,-5,5.)
        WW2.setStatConfig(back_stat)
        if not manualNorm :
            WW2.setNormRegions(ggFregion2)
        else :
            WW2.setNormByTheory()


        WW3 = Sample(samplePrefix+"diboson3",kMagenta)
        samplec_list.append(WW3)
        WW3.additionalCuts="inggFCR1==1"
        WW3.prefixTreeName="diboson"
        if normAllBckTotheory :
            WW3.setNormByTheory()
        else :
            if commonTopWWParameter:
                WW3.addNormFactor("mu_ww3",1.,1,1.,True)
                WW3.addNormFactor("mu_wwTop3",1.,5,-5,False)
            else : 
                WW3.setNormFactor("mu_ww3",1.,-5,5.)
        WW3.setStatConfig(back_stat)
        if not manualNorm :
            WW3.setNormRegions(ggFregion1)
        else :
            WW2.setNormByTheory()

    if "top" in sample_list :     
        Top2 = Sample(samplePrefix+"top2",kMagenta)
        samplec_list.append(Top2)
        Top2.additionalCuts="inggFCR2==1"
        Top2.prefixTreeName="top"
        Top2.setStatConfig(back_stat)
        if normAllBckTotheory : 
            Top2.setNormByTheory()
        else :
            if commonTopWWParameter :
                Top2.addNormFactor("mu_top2",1.,1,1.,True)
                Top2.addNormFactor("mu_wwTop2",1.,5.,-5.,False)
            else :
                Top2.setNormFactor("mu_top2",1.,-5.,+5.,False)
                if not manualNorm :
                    Top2.setNormRegions(ggFregion2)
                else :
                    Top2.setNormByTheory()
        Top2.setStatConfig(back_stat)

        Top3 = Sample(samplePrefix+"top3",kMagenta)
        samplec_list.append(Top3)
        Top3.additionalCuts="inggFCR1==1"
        Top3.prefixTreeName="top"
        if normAllBckTotheory : 
            Top3.setNormByTheory()
        else :
            if commonTopWWParameter :
                Top3.addNormFactor("mu_top3",1.,1,1.,True)
                Top3.addNormFactor("mu_wwTop3",1.,5.,-5.,False)
            else :
                Top3.setNormFactor("mu_top3",1.,-5.,+5.,False)
                if not manualNorm :
                    Top3.setNormRegions(ggFregion1)
                else :
                    Top3.setNormByTheory()
        Top3.setStatConfig(back_stat)
        
# set the file from which the samples should be taken
    
for sam in samplec_list :
    sam.addInputs(bkgFiles) 

#************
#Bkg only fit
#************

bkt = configMgr.addFitConfig("BkgOnly")
if back_stat:
    bkt.statErrThreshold=0.0
else:
    bkt.statErrThreshold=None


#if not mysamples :
#    if doABCD : 
#        bkt.addStheoamples([GGF1,GGF2,GGF3,GGF,Vgamma,Zjets,Top,WW,Top1,WW1,Zjets1,VBF,data])
#        #    bkt.addSamples([GGF1,GGF2,GGF3,GGF,Vgamma,Zjets,Top,WW,VBF,data])
#    else :
#        if doZeroOne : 
#            bkt.addSamples([GGF,Vgamma,Zjets,Top,WW,Top1,WW1,Zjets1,VBF,data])
#            #        bkt.addSamples([GGF,Vgamma,Zjets,Top,WW,VBF,data])
#        else :
#            bkt.addSamples([GGF,Vgamma,Zjets,Top,WW,VBF,data])
#else :

bkt.addSamples(samplec_list)

meas=bkt.addMeasurement(name="NormalMeasurement",lumi=1,lumiErr=0.017)
#meas=bkt.addMeasurement(name="NormalMeasurement")
meas.addPOI("mu_vbf")
#meas.addPOI("mu_ww")
#meas.addPOI("mu_ggf")
#meas.addParamSetting("C",True,0.43)
#meas.addParamSetting("C",True,0.016756032)
#meas.addParamSetting("OneOverC",37356250)
meas.addParamSetting("Lumi",True,1)
#meas.addParamSetting("Lumi_sig",True,139)
if buildSystematics and readSystematics: 
    for sam in sys_in_samples :
        print "Analysing sample " + sam
        if ( sam.find("data") > -1 ) :
            continue 
#            print (s.name +" for "+sam)
#        if ( (sam == "vbf0" or sam == "vbf") and isDiff ):
#                for ibin in range(0 ,len(binEdges)-1) :
#                   bkt.getSample(samplePrefix+"vbf0_"+str(ibin)).addSystematic(s)
#        else :
	print systematicsPerSample
        for s in systematicsPerSample[sam] :
            print "processing sample of "+sam + " for " + s.name
            bkt.getSample(samplePrefix+sam).addSystematic(s)
    for sam in sys_in_samples1jet1 :
        for s in systematicsPerSample1jet1[sam] :
            print (s.name +" for "+sam)
            bkt.getSample(samplePrefix+sam).addSystematic(s)
    for sam in sys_in_samples1jet2 :
        for s in systematicsPerSample1jet2[sam] :
            print (s.name +" for "+sam)
            bkt.getSample(samplePrefix+sam).addSystematic(s)
    for sam in sys_in_samples1jet3 :
        for s in systematicsPerSample1jet3[sam] :
            print (s.name +" for "+sam)
            bkt.getSample(samplePrefix+sam).addSystematic(s)

    if reReadInputs and doTheorErrors:
        #add the systematics for all vbf bins
        if(vbfTheor):
                if ( isDiff ):
                    for ibin in range(0 ,len(binEdges)-1) :
                        for s in ThsystematicsPerSample["vbf0_"+str(ibin)] : 
                            print(s.name)
                            print(samplePrefix+"vbf0_"+str(ibin))
                            bkt.getSample(samplePrefix+"vbf0_"+str(ibin)).addSystematic(s)
                else :
                    for s in ThsystematicsPerSample["vbf0"] : 
                        bkt.getSample(samplePrefix+"vbf0").addSystematic(s)
        if(ggfTheor):
            for s in ThsystematicsPerSample["ggf"] :
                bkt.getSample(samplePrefix+"ggf").addSystematic(s)
        
        if(doABCD and ggfTheor) :
            for s in ThsystematicsPerSample1jet1["ggf1"] :
                bkt.getSample(samplePrefix+"ggf1").addSystematic(s)
            for s in ThsystematicsPerSample1jet2["ggf2"] :
                bkt.getSample(samplePrefix+"ggf2").addSystematic(s)
            for s in ThsystematicsPerSample1jet3["ggf3"] :
                bkt.getSample(samplePrefix+"ggf3").addSystematic(s)


        if(ZjetsTheor):
            for s in ThsystematicsPerSample["Zjets0"] :
                bkt.getSample(samplePrefix+"Zjets0").addSystematic(s)
            if doZeroOne and SplitZeroOnejetSamples :
                for s in ThsystematicsPerSample1jet1["Zjets1"] :
                    bkt.getSample(samplePrefix+"Zjets1").addSystematic(s) 
        if(dibosonTheor):
            for s in ThsystematicsPerSample["diboson"] :
                bkt.getSample(samplePrefix+"diboson").addSystematic(s)
            if SplitZeroOnejetSamples and doZeroOne :
                for s in ThsystematicsPerSample1jet1["diboson1"] :
                    bkt.getSample(samplePrefix+"diboson1").addSystematic(s)
            if SplitggFCRTopWWSamples:
                for s in ThsystematicsPerSample1jet2["diboson2"] :
                    bkt.getSample(samplePrefix+"diboson2").addSystematic(s)
                for s in ThsystematicsPerSample1jet3["diboson3"] :
                    bkt.getSample(samplePrefix+"diboson3").addSystematic(s)

        if(topTheor):
            for s in ThsystematicsPerSample["top"] :
                bkt.getSample(samplePrefix+"top").addSystematic(s)
            if SplitZeroOnejetSamples and doZeroOne :
                for s in ThsystematicsPerSample1jet1["top1"] :
                    bkt.getSample(samplePrefix+"top1").addSystematic(s)
            if SplitggFCRTopWWSamples:
                for s in ThsystematicsPerSample1jet2["top2"] :
                    bkt.getSample(samplePrefix+"top2").addSystematic(s)
                for s in ThsystematicsPerSample1jet3["top3"] :
                    bkt.getSample(samplePrefix+"top3").addSystematic(s)
CRTophistt= []
CRWWhistt=  []
backHistLists= []

# esempio shape fit 
if not normAllBckTotheory :
#    CRZjetshist = bkt.addChannel("bdt_Zjets",["CRZjets"],4,0,1)
    CRZjetshist = bkt.addChannel("MT",["CRZjets"],ZjetsBins,40*GeVtoMeV,150*GeVtoMeV)
    CRZjetshist.hasB = False   # no selection cuts implying b-tagging 
    CRZjetshist.hasBQCD = False
    CRZjetshist.useOverflowBin = True  
    backHistLists.append(CRZjetshist)
    if hasTopCR : 
        if not topCRInSR : 
            CRTophistt = bkt.addChannel("cuts",["CRTop"],1,0.5,1.5)
            CRTophistt.append(CRTophist)
            backHistLists.append(CRTophist)
        else :
            if isDiff :
               for ibin in range(0 ,len(binEdges)-1) :
                    CRTophist = bkt.addChannel(topWWDiscriminant,["CRTop_"+str(ibin)],nCats_dict["CRTop_"+str(ibin)],-1,+1)
                    CRTophist.useOverflowBin = True
                    if wwCRinSR :
                        CRWWhist = bkt.addChannel("bdt_TopWW",["CRWW_"+str(ibin)],5,0.4,+1)
                        CRWWhist.hasB = False   # no selection cuts implying b-tagging 
                        CRWWhist.hasBQCD = False
                        CRWWhist.useOverflowBin = True
                        CRWWhistt.append(CRWWhist)
                        backHistLists.append(CRWWhist)
                        CRTophist.hasB = False   # no selection cuts implying b-tagging 
                        CRTophist.hasBQCD = False
                        CRTophist.useOverflowBin = True
                        CRTophistt.append(CRTophist)
                        backHistLists.append(CRWWhist)
                        
            else :
                CRTophist = bkt.addChannel(topWWDiscriminant,["CRTop"],topWWBins,-1,+1)
                CRTophist.useOverflowBin = True
                if wwCRinSR :
                    CRWWhist = bkt.addChannel("bdt_TopWW",["CRWW"],5,0.4,+1)
                    CRWWhist.hasB = False   # no selection cuts implying b-tagging 
                    CRWWhist.hasBQCD = False
                    CRWWhist.useOverflowBin = True
                    CRWWhistt.append(CRWWhist)
                    backHistLists.append(CRWWhist)
                CRTophist.hasB = False   # no selection cuts implying b-tagging 
                CRTophist.hasBQCD = False
                CRTophist.useOverflowBin = True
                CRTophistt.append(CRTophist)

        

    if hasggFCR : 
        CRGGF1hist = bkt.addChannel("bdt_ggFCR1"+useTransSR,["CRGGF1"],ggf1Bins,-1,1)
        CRGGF1hist.hasB = False
        CRGGF1hist.hasBQCD = False
        CRGGF1hist.useOverflowBin = True
        backHistLists.append(CRGGF1hist)
        
        CRGGF2hist = bkt.addChannel("bdt_ggFCR2"+useTransSR,["CRGGF2"],ggf2Bins,-1,1)
        CRGGF2hist.hasB = False
        CRGGF2hist.hasBQCD = False
        CRGGF2hist.useOverflowBin = True
        backHistLists.append(CRGGF2hist)
        if doABCD  or doZeroOne:
            CRGGF3hist = bkt.addChannel("bdt_ggFCR3_CutDPhill",["CRGGF3"],ggf3Bins,-1,+1)
            CRGGF3hist.hasB = False
            CRGGF3hist.hasBQCD = False
            CRGGF3hist.useOverflowBin = True
            backHistLists.append(CRGGF3hist)
if addggFDiscr: 
    SRGGFhistt=bkt.addChannel("bdt_vbfggf",["SRGGF"],6,-1,+1)
    backHistLists.append(SRGGFhistt)
    #        SRVBFhist.append(SRGGFhistt) 

SRVBFhist =[]


if is1D : 
    if isDiff :
       for ibin in range(0 ,len(binEdges)-1) :
            if topCRInSR :
                SRVBFhistt=bkt.addChannel("bdt_vbf_"+variable,["SRVBF_"+str(ibin)],nCats_dict["SRVBF_"+str(ibin)],bdt_vbFLow,bdt_vbfHigh)
            else :
                SRVBFhistt=bkt.addChannel("bdt_vbf_"+variable,["SRVBF_"+str(ibin)],nCats_dict["SRVBF_"+str(ibin)],bdt_vbFLowInc,bdt_vbfHigh)
                SRVBFhist.append(SRVBFhistt)
    else :
        if topCRInSR :
            SRVBFhistt=bkt.addChannel("bdt_vbf_",["SRVBF"],nCats_dict["SRVBF"],bdt_vbFLow,bdt_vbfHigh)
        else :
            SRVBFhistt=bkt.addChannel("bdt_vbf_",["SRVBF"],nCats_dict["SRVBF"],bdt_vbFLowInc,bdt_vbfHigh)
            SRVBFhist.append(SRVBFhistt)
else :
    if not isCounting3D:
        SRVBFhistt=bkt.addChannel("bdtProj",["SRVBF"],nCats,1,nCats+1)
        SRVBFhist.append(SRVBFhistt)
    else :
        for i in range(0,len(strarray)):
            SRVBFhistt = bkt.addChannel("cuts",[strarray[i]],1,0.5,1.5)
            SRVBFhist.append(SRVBFhistt)


bkt.setSignalSample("vbf")
ll=len(SRVBFhist)
print('ellallalunghezza ',ll,' ella strarray ',  strarray[len(strarray)-1])
bkt.addBkgConstrainChannels(backHistLists)
#if not normAllBckTotheory :
#    if hasggFCR :
#        if doABCD or doZeroOne:
#            if hasTopCR :
#                if wwCRinSR :
#                    if addggFDiscr :
#                        bkt.addBkgConstrainChannels([CRTophistt,CRWWhistt,CRZjetshist,CRGGF1hist,CRGGF2hist,CRGGF3hist,SRGGFhistt])
#                    else :
#                        bkt.addBkgConstrainChannels([CRTophistt,CRWWhistt,CRZjetshist,CRGGF1hist,CRGGF2hist,CRGGF3hist])
#                else :
#                    if addggFDiscr :
#                        bkt.addBkgConstrainChannels([CRTophistt,CRZjetshist,CRGGF1hist,CRGGF2hist,CRGGF3hist,SRGGFhistt])
#                    else :
#                        bkt.addBkgConstrainChannels([CRTophistt,CRZjetshist,CRGGF1hist,CRGGF2hist,CRGGF3hist])
#            else :
#                if addggFDiscr :
#                    bkt.addBkgConstrainChannels([CRZjetshist,CRGGF1hist,CRGGF2hist,CRGGF3hist,SRGGFhistt])
#                else:
#                    bkt.addBkgConstrainChannels([CRZjetshist,CRGGF1hist,CRGGF2hist,CRGGF3hist])
#        else :
#            if hasTopCR :
#                if wwCRinSR:
#                    if addggFDiscr :
#                        bkt.addBkgConstrainChannels([CRTophistt,CRWWhistt,CRZjetshist,CRGGF1hist,CRGGF2hist,SRGGFhistt])
#                    else :
#                        bkt.addBkgConstrainChannels([CRTophistt,CRWWhistt,CRZjetshist,CRGGF1hist,CRGGF2hist])
#                else :
#                    if addggFDiscr :
#                        bkt.addBkgConstrainChannels([CRTophistt,CRZjetshist,CRGGF1hist,CRGGF2hist,SRGGFhistt])
#                    else :
#                        bkt.addBkgConstrainChannels([CRTophistt,CRZjetshist,CRGGF1hist,CRGGF2hist])
#            else :
#                if addggFDiscr :
#                    bkt.addBkgConstrainChannels([CRZjetshist,CRGGF1hist,CRGGF2hist,SRGGFhistt])
#                else :
#                    bkt.addBkgConstrainChannels([CRZjetshist,CRGGF1hist,CRGGF2hist])
#    else :
#        if hasTopCR :
#            if wwCRinSR :
#                if addggFDiscr :
#                    bkt.addBkgConstrainChannels([CRTophistt,CRWWhistt,CRZjetshist,SRGGFhistt])
#                else :
#                    bkt.addBkgConstrainChannels([CRTophistt,CRWWhistt,CRZjetshist])
#            else :
#                if addggFDiscr :
#                    bkt.addBkgConstrainChannels([CRTophistt,CRZjetshist,SRGGFhistt])
#                else:
#                    bkt.addBkgConstrainChannels([CRTophistt,CRZjetshist])
#        else :
#            if addggFDiscr :
#                bkt.addBkgConstrainChannels([CRZjetshist,SRGGFhistt])
#            else :
#                bkt.addBkgConstrainChannels([CRZjetshist])
                    

for i in range(0,len(SRVBFhist)):
    bkt.addSignalChannels([SRVBFhist[i]])




for s in binEdges :
    print "binEdgesMap[\""+variable+"\"].push_back("+ str(s) +");"

#for  in configMgr.cutsDict :
#    print (x)
#    for y in configMgr.cutsDict[x]:
#        print y + " " + configMgr.cutsDict[x][y] 
    

###################
#    Example new cosmetics     
###################

# Set global plotting colors/styles
#if "data" in sample_list :
#    bkt.dataColor = data.color
#bkt.totalPdfColor = kBlack
#bkt.errorFillColor = kWhite
#bkt.errorFillStyle = 3004
#bkt.errorLineStyle = kDashed
#bkt.errorLineColor = kWhite
#bkt.setLogy=True

#--------------------------------------------------------------
# Validation regions - not necessarily statistically independent
#--------------------------------------------------------------

  

# Create TLegend (AK: TCanvas is needed for that, but it gets deleted afterwards)
#c = TCanvas()
#compFillStyle = 1001 # see ROOT for Fill styles
#leg = TLegend(0.6,0.55,0.92,0.9,"")
## leg = TLegend(0.55,0.6,0.94,0.92,"")
## leg = TLegend(0.51,0.50,0.94,0.92,"")
#leg.SetFillStyle(0)
#leg.SetFillColor(0)
#leg.SetBorderSize(0)#
#entry = TLegendEntry()
#entry = leg.AddEntry("","#font[42]{Data}","p") 
#entry.SetMarkerColor(bkt.dataColor)
#entry.SetMarkerStyle(20)
#
#entry = leg.AddEntry("","#font[42]{Total}","l") 
#entry.SetLineColor(bkt.totalPdfColor)
#entry.SetLineWidth(2)
#entry.SetFillColor(bkt.errorFillColor)
#entry.SetFillStyle(bkt.errorFillStyle)

#entry = leg.AddEntry("","VBF","F") 
#entry.SetLineColor(kRed)
#entry.SetLineWidth(2)
#entry.SetFillColor(kRed)



#entry = leg.AddEntry("","Dibosons","F") 
#entry.SetLineColor(kBlue)
#entry.SetLineWidth(2)
#entry.SetFillColor(kBlue)



#entry = leg.AddEntry("","ggF","F") 
#entry.SetLineColor(kGreen)
#entry.SetLineWidth(2)
#entry.SetFillColor(kGreen)


#entry = leg.AddEntry("","Top","F") 
#entry.SetLineColor(kMagenta)
#entry.SetLineWidth(2)
#entry.SetFillColor(kMagenta)


#entry = leg.AddEntry("","V#gamma","F") 
#entry.SetLineColor(kYellow)
#entry.SetLineWidth(2)
#entry.SetFillColor(kYellow)


#entry = leg.AddEntry("","#it{Z}+jets","F") 
#entry.SetLineColor(kPink)
#entry.SetLineWidth(2)
#entry.SetFillColor(kPink)

# Set legend for fitConfig
#bkt.tLegend = leg
#c.Close()
#Copy this configuration file to results directory
shutil.copy("HWW_3D_hist_noBDTZjetsInSR_noBDT_v21_decorr_mJJ.py","results/"+configMgr.analysisName+"/")

for s in binEdges :
    print "binEdgesMap[\""+variable+"\"].push_back("+ str(s) +");"

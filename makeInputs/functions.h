#ifndef functions_h
#define functions_h

#include "TFile.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include "TH1.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TF1.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TF1.h"
#include <RooCategory.h>
#include <RooWorkspace.h>
#include <RooMinimizer.h>
#include <RooAddition.h>
#include <RooStats/AsymptoticCalculator.h>
#include "TRandom3.h"
#include <RooFitResult.h>
#include <RooArgSet.h>
#include <RooArgList.h>
#include "TLegend.h"
#include "TTree.h"
#include "TTreeFormula.h"
#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooGaussModel.h"
#include "RooConstVar.h"
#include "RooDecay.h"
#include "RooLandau.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooPlot.h"
#include "TAxis.h"
#include <RooMomentMorph.h>
#include "minitree.h"
#include "Math/Math.h"
#include "Math/QuantFuncMathCore.h"
//#include "SpecFuncCephes.h"
#include <limits>
#include "TMVA/Reader.h"
#include "TMVA/Tools.h"
#include <chrono>



using namespace std;


vector<string> split(const char *str, char c  , vector <string> skip)
{
    vector<string> result;

    do
    {
        const char *begin = str;

        while(*str != c && *str)
            str++;
        bool match=false;
        for( vector<string>::iterator it=skip.begin(); it!=skip.end(); it++)
          if(string(begin, str).find( (*it) ) !=string::npos ) match=true;

        if(!match)
          result.push_back(string(begin, str));
        
    } while (0 != *str++);
    
    return result;
}

vector <string> cleanChainFiles(std::map<string,string> RunOverTrees, string chainFiles=""){
  vector <string> listFiles; 
  //if (chainFiles.find(".txt")==string::npos){
  //cout<<"File does not seem to be a list cannnot handle this at this point "<<endl;
  //return listFiles;
  //}

  ifstream file2(chainFiles.c_str());
  string line2="";

  while ( getline (file2,line2) ){
    //cout<<"Reading line "<<line2<<endl;
    if( line2[0] == '#' ) continue;
    TFile *f=TFile::Open(line2.c_str());
    if( f==nullptr ) { cout<<"file "<<line2<<" is null skipping "<<endl; continue;} 
    if( f->IsZombie() ) { cout<<"file "<<line2<<" is a zombie skipping "<<endl; continue;} 

    for(std::map<string,string>::iterator it=RunOverTrees.begin(); it!=RunOverTrees.end(); it++){
      TTree *t=nullptr;
      t=(TTree*)f->Get((*it).second.c_str());
      if( t!=nullptr && t->GetEntries()>0){
        bool present=false; 
        for( vector <string>::iterator  ll=listFiles.begin(); ll!=listFiles.end(); ll++){
          /*cout<<"Searching for duplicates in list "<<(*ll)<<" "<<line2<<endl;*/
          if( (*ll).compare(line2)==0) {cout<<"Found duplicate"<<endl; present = true; break; } 
        }
        if(!present){
          listFiles.push_back(line2);
          cout<<"Adding file "<<line2<<endl;
        }
      }
      delete t;
    }
    f->Close();
    delete f;
  }

  file2.close();

  return listFiles;
}

/*
std::string remove_extension(const std::string& filename) {
    size_t lastdot = filename.find_last_of(";");
    if (lastdot == std::string::npos) return filename;
    cout<<" removing from "<<filename <<" to "<<filename.substr(0, lastdot)<<endl;
    return filename.substr(0, lastdot); 
}
*/
void eraseAllSubStr(std::string & mainStr, const std::string & toErase)
{
size_t pos = std::string::npos;
// Search for the substring in string in a loop untill nothing is found
while ((pos  = mainStr.find(toErase) )!= std::string::npos)
  {
    // If found then erase it from string
    mainStr.erase(pos, toErase.length());
  }
}



std::string remove_extension(const std::string& filename) {
    size_t lastdot = filename.find_last_of(";");
    if (lastdot == std::string::npos) return filename;
    cout<<" removing from "<<filename <<" to "<<filename.substr(0, lastdot)<<endl;
    return filename.substr(0, lastdot); 
}

string whichObs(std::string & mainStr, const std::string & toErase){
  if( mainStr.find(toErase)==string::npos) return "unknown";

  string sbtr=mainStr.substr(mainStr.find(toErase)+toErase.length(),mainStr.length());
  return sbtr;

}

string eraseSubStr(std::string & mainStr, const std::string & toErase)
{
  string ret=mainStr;
	// Search for the substring in string
	size_t pos = ret.find(toErase);
 
	if (pos != std::string::npos)
	{
		// If found then erase it from string
		ret.erase(pos, toErase.length());
	}
    return ret;
}
string eraseSubStrToEnd(std::string & mainStr, const std::string & toErase)
{
  string ret=mainStr;
	// Search for the substring in string
	size_t pos = ret.find(toErase);
 
	if (pos != std::string::npos)
	{
      //cout<<"Ereasing from "<<toErase<<" to "<<ret<<endl;
      // If found then erase it from string
		ret.erase(pos, ret.length());
	}
    return ret;
}







#endif

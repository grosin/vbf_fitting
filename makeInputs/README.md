# process tree (ntuples)

This script bins ntuples (nominal and systematics) into histograms. The binning of each differential observable is given by the corresponding "cut config" in config/; the discriminant binning of each region/category are given in the processTree.c script itself (some binning is finner than actual binning, which will be rebinned in the following workspace-making steps).  The output will have the unsmoothed histograms and smoothed histograms using RooKeysPdf (named "\*InputHis").

# 1. run processTrees standalone


Set up environment:
```
setupATLAS
asetup AnalysisBase,21.2.91,here
./processTrees <nominal ntuple .root> <output .root> <configs/CutConfig.txt> -var <observable>
```
example:
```
./processTrees ../../../reco_ntuples/vbf_nominal_tree.root vbf_inc.root configs/inclusive_regions_CutConfig.txt -var inc
```

To re-compile the processTrees.c, one needs to:
```
make 
```


# 2. submit jobs to condor

Submit jobs to condor and split jobs for each process (vbf, top, ...) and each systematics (nominal, JER, MET, ...):
```
source subJobsSamplesAndSys.sh <config> <job directory prefix> <observable>
```

example:
```
source subJobsSamplesAndSys.sh ../configs/inclusive_regions_CutConfig.txt test inc
```

Some input for the job submission that might need modification:
* 
* `$listOfFiles` variable (see [here](https://gitlab.cern.ch/chenj/vbfhww_fitting/-/blob/master/makeInputs/submission/subJobsSamplesAndSys.sh#L20)), takes the list of all the nutples path; used for the `-chain` argument for processTree;
* `$ref` (see [here](https://gitlab.cern.ch/chenj/vbfhww_fitting/-/blob/master/makeInputs/submission/subJobsSamplesAndSys.sh#L52)) is the first ntuple file in `$listOfFiles` (typically a shell ntuple, with no event entry);
* `$listOfSys` variable (see [here](https://gitlab.cern.ch/chenj/vbfhww_fitting/-/blob/master/makeInputs/submission/subJobsSamplesAndSys.sh#L17)), takes the list of all the systematics name to be run over; each systematics name used for the `-pattern` argument for processTree;

Also note, the output root file is first saved to `/tmp/`, and get `hadd -T` to the `run/` directory to remove the copy of the tree to save space. The submission script will remove the `/tmp/` directory after hadd.  

The jobs can be checked for any error by
```
python validate_condor.py jobsSamplesAndSys_cleaned_up_Fakes
```

The ouput histograms needs to be added into one root file for the following step. First make a list of all the histogram files to be hadd:
```
python ../create_merge_list.py create <txt name> <jobsubmission dir name>
```
with `source_dir` set to the `run/` directory level. Next, hadd by:
```
python ../hadd_files.py <txtname>.txt <output root file name>
```






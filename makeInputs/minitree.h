//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Nov  5 11:54:48 2019 by ROOT version 6.18/04
// from TTree Cut2Jet_v2_ggf_nominal/HWW_em
// found on file: with_sys/eval_sys_merge_ggf_part0.root
//////////////////////////////////////////////////////////

#ifndef minitree_h
#define minitree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

enum ReadMode { FULL=-1, PATCH3D=0, READBDT=1 } ;

class minitree {
public :
  int m_readmode; // readmode of minitree
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
  Int_t           channel;
  Double_t        bdtProj;
  Double_t        bdtProj_trans;
  Float_t         mcChannelNumber;
   Float_t         Mjj;
   Float_t         inWWCR;
   Float_t         inTopCR;
   Float_t         inZjetsCR;
   Float_t         inggFCR1;
   Float_t         inggFCR2;
   Float_t         inggFCR3;
   Float_t         inSR;
   Float_t         DPhil0j0;
   Float_t         DPhil0j1;
   Float_t         DPhil1j0;
   Float_t         DPhil1j1;
   Float_t         DEtal0j0;
   Float_t         DEtal0j1;
   Float_t         DEtal1j0;
   Float_t         DEtal1j1;
   Float_t         DPhiTSTtrk;
   Float_t         DPhiCSTtrk;
   Float_t         TrackMETx;
   Float_t         TrackMETy;
   Float_t         DPhillMETcst;
   Float_t         ptTotTrk;
   Float_t         DPhijMET;
   Float_t         METx;
   Float_t         DPhillMET;
   Float_t         METy;
   Float_t         METRel_noJets;
   Float_t         METRel_withJets;
   Float_t         TrackMETRel_noJets;
   Float_t         TrackMETRel_withJets;
   Float_t         MET_CST;
   Float_t         MET_TST;
   Float_t         runNumber;
   Float_t         nJetsTight;
   Float_t         nBJetsSubMV2c10;
   Float_t         centralJetVetoLeadpT;
   Float_t         mtt;
   Float_t         mZ;
   Float_t         OLV;
   Float_t         MET;
   Float_t         MT2_1Jet;
   Float_t         nJetsFJVT3030;
   Float_t         Ml0j0;
   Float_t         Ml0j1;
   Float_t         Ml1j0;
   Float_t         Ml1j1;
   Float_t         jet0_pt;
   Float_t         jet1_pt;
   Float_t         jet2_pt;
   Float_t         nJets;
   Float_t         jet0_eta;
   Float_t         jet1_eta;
   Float_t         jet2_eta;
   Float_t         lep0_eta;
   Float_t         lep1_eta;
   Float_t         SEtajj;
   Float_t         DPhijj;
   Float_t         DEtajj;
   Float_t         DYjj;
   Float_t         Mll;
   Float_t         DYll;
   Float_t         DPhill;
   Float_t         MT;
   Float_t         ptTot;
   Float_t         TrackMET;
   Float_t         lep0_pt;
   Float_t         lep1_pt;
   Float_t         lep0_truthType;
   Float_t         lep1_truthType;
   Float_t         lep0_truthOrigin;
   Float_t         lep1_truthOrigin;
   Float_t         lep0_E;
   Float_t         lep1_E;
   Float_t         jet0_E;
   Float_t         jet1_E;
   Float_t         jet2_E;
   Float_t         lep0_phi;
   Float_t         lep1_phi;
   Float_t         jet0_phi;
   Float_t         jet1_phi;
   Float_t         jet2_phi;
   Float_t         lep1_is_m;
   Float_t         lep1_is_e;
   Float_t         lep0_is_m;
   Float_t         lep0_is_e;
   Double_t        weight;
  std::vector<double>  *bdtMltClass;
   Double_t        bdt_vbf;
   Double_t        bdt_WW;
   Double_t        bdt_ggF;
   Double_t        bdt_vbf_trans;
   Double_t        bdt_WW_trans;
   Double_t        bdt_ggF_trans;
   Double_t        bdt_ggFCR1;
   Double_t        bdt_ggFCR1_trans;
   Double_t        bdt_ggFCR2;
   Double_t        bdt_ggFCR2_trans;
   Double_t        bdt_Top_trans;
   Double_t        bdt_Top;
   Double_t        bdt_Zjets;
   Double_t        bdt_Zjets_trans;

   // List of branches
    TBranch        *b_channel;   //!
   TBranch        *b_bdtProj;   //!
  TBranch        *b_bdtProj_trans;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_Mjj;   //!
   TBranch        *b_inWWCR;   //!
   TBranch        *b_inTopCR;   //!
   TBranch        *b_inZjetsCR;   //!
   TBranch        *b_inggFCR1;   //!
   TBranch        *b_inggFCR2;   //!
   TBranch        *b_inggFCR3;   //!
   TBranch        *b_inSR;   //!
   TBranch        *b_DPhil0j0;   //!
   TBranch        *b_DPhil0j1;   //!
   TBranch        *b_DPhil1j0;   //!
   TBranch        *b_DPhil1j1;   //!
   TBranch        *b_DEtal0j0;   //!
   TBranch        *b_DEtal0j1;   //!
   TBranch        *b_DEtal1j0;   //!
   TBranch        *b_DEtal1j1;   //!
   TBranch        *b_DPhiTSTtrk;   //!
   TBranch        *b_DPhiCSTtrk;   //!
   TBranch        *b_TrackMETx;   //!
   TBranch        *b_TrackMETy;   //!
   TBranch        *b_DPhillMETcst;   //!
   TBranch        *b_ptTotTrk;   //!
   TBranch        *b_DPhijMET;   //!
   TBranch        *b_METx;   //!
   TBranch        *b_DPhillMET;   //!
   TBranch        *b_METy;   //!
   TBranch        *b_METRel_noJets;   //!
   TBranch        *b_METRel_withJets;   //!
   TBranch        *b_TrackMETRel_noJets;   //!
   TBranch        *b_TrackMETRel_withJets;   //!
   TBranch        *b_MET_CST;   //!
   TBranch        *b_MET_TST;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_nJetsTight;   //!
   TBranch        *b_nBJetsSubMV2c10;   //!
   TBranch        *b_centralJetVetoLeadpT;   //!
   TBranch        *b_mtt;   //!
   TBranch        *b_mZ;   //!
   TBranch        *b_OLV;   //!
   TBranch        *b_MET;   //!
   TBranch        *b_MT2_1Jet;   //!
   TBranch        *b_nJetsFJVT3030;   //!
   TBranch        *b_Ml0j0;   //!
   TBranch        *b_Ml0j1;   //!
   TBranch        *b_Ml1j0;   //!
   TBranch        *b_Ml1j1;   //!
   TBranch        *b_jet0_pt;   //!
   TBranch        *b_jet1_pt;   //!
   TBranch        *b_jet2_pt;   //!
   TBranch        *b_nJets;   //!
   TBranch        *b_jet0_eta;   //!
   TBranch        *b_jet1_eta;   //!
   TBranch        *b_jet2_eta;   //!
   TBranch        *b_lep0_eta;   //!
   TBranch        *b_lep1_eta;   //!
   TBranch        *b_SEtajj;   //!
   TBranch        *b_DPhijj;   //!
   TBranch        *b_DEtajj;   //!
   TBranch        *b_DYjj;   //!
   TBranch        *b_Mll;   //!
   TBranch        *b_DYll;   //!
   TBranch        *b_DPhill;   //!
   TBranch        *b_MT;   //!
   TBranch        *b_ptTot;   //!
   TBranch        *b_TrackMET;   //!
   TBranch        *b_lep0_pt;   //!
   TBranch        *b_lep1_pt;   //!
   TBranch        *b_lep0_truthType;   //!
   TBranch        *b_lep1_truthType;   //!
   TBranch        *b_lep0_truthOrigin;   //!
   TBranch        *b_lep1_truthOrigin;   //!
   TBranch        *b_lep0_E;   //!
   TBranch        *b_lep1_E;   //!
   TBranch        *b_jet0_E;   //!
   TBranch        *b_jet1_E;   //!
   TBranch        *b_jet2_E;   //!
   TBranch        *b_lep0_phi;   //!
   TBranch        *b_lep1_phi;   //!
   TBranch        *b_jet0_phi;   //!
   TBranch        *b_jet1_phi;   //!
   TBranch        *b_jet2_phi;   //!
   TBranch        *b_lep1_is_m;   //!
   TBranch        *b_lep1_is_e;   //!
   TBranch        *b_lep0_is_m;   //!
   TBranch        *b_lep0_is_e;   //!
   TBranch        *b_weight;   //!
   TBranch        *b_bdtMltClass;   //!
   TBranch        *b_bdt_vbf;   //!
   TBranch        *b_bdt_WW;   //!
   TBranch        *b_bdt_ggF;   //!
   TBranch        *b_bdt_vbf_trans;   //!
   TBranch        *b_bdt_WW_trans;   //!
   TBranch        *b_bdt_ggF_trans;   //!
   TBranch        *b_bdt_ggFCR1;   //!
   TBranch        *b_bdt_ggFCR1_trans;   //!
   TBranch        *b_bdt_ggFCR2;   //!
   TBranch        *b_bdt_ggFCR2_trans;   //!
   TBranch        *b_bdt_Top_trans;   //!
   TBranch        *b_bdt_Top;   //!
   TBranch        *b_bdt_Zjets;   //!
   TBranch        *b_bdt_Zjets_trans;   //!

  minitree(TTree *tree=0,int readmode=0);
   virtual ~minitree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
  virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

//#endif

//#ifdef minitree_cxx
inline minitree::minitree(TTree *tree,int readmode):
   fChain(0) 
{
  m_readmode=readmode;
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
     std::cout<<"No tree given as input "<<std::endl;
   }
   Init(tree);
}

inline minitree::~minitree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

inline Int_t minitree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
inline Long64_t minitree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

inline void minitree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   bdtMltClass = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchStatus("*",0);

   if( m_readmode==ReadMode::PATCH3D){

     fChain->SetBranchStatus("*",1);
     fChain->SetBranchAddress("channel", &channel, &b_channel);
     fChain->SetBranchAddress("bdtProj", &bdtProj, &b_bdtProj);
     fChain->SetBranchAddress("bdtProj_trans", &bdtProj_trans, &b_bdtProj_trans);
     fChain->SetBranchAddress("inSR", &inSR, &b_inSR);
     fChain->SetBranchAddress("weight", &weight, &b_weight);
     fChain->SetBranchAddress("Mjj", &Mjj, &b_Mjj);
     fChain->SetBranchAddress("inWWCR", &inWWCR, &b_inWWCR);
     fChain->SetBranchAddress("inTopCR", &inTopCR, &b_inTopCR);
     fChain->SetBranchAddress("inZjetsCR", &inZjetsCR, &b_inZjetsCR);
     fChain->SetBranchAddress("inggFCR1", &inggFCR1, &b_inggFCR1);
     fChain->SetBranchAddress("inggFCR2", &inggFCR2, &b_inggFCR2);
     fChain->SetBranchAddress("inggFCR3", &inggFCR3, &b_inggFCR3);
     fChain->SetBranchAddress("MT", &MT, &b_MT);
     fChain->SetBranchAddress("Mll", &Mll, &b_Mll);
     fChain->SetBranchAddress("bdt_vbf", &bdt_vbf, &b_bdt_vbf);
     fChain->SetBranchAddress("bdt_WW", &bdt_WW, &b_bdt_WW);
     fChain->SetBranchAddress("bdt_ggF", &bdt_ggF, &b_bdt_ggF);
     fChain->SetBranchAddress("bdt_vbf_trans", &bdt_vbf_trans, &b_bdt_vbf_trans);
     fChain->SetBranchAddress("bdt_WW_trans", &bdt_WW_trans, &b_bdt_WW_trans);
     fChain->SetBranchAddress("bdt_ggF_trans", &bdt_ggF_trans, &b_bdt_ggF_trans);
     fChain->SetBranchAddress("bdt_ggFCR1", &bdt_ggFCR1, &b_bdt_ggFCR1);
     fChain->SetBranchAddress("bdt_ggFCR1_trans", &bdt_ggFCR1_trans, &b_bdt_ggFCR1_trans);
     fChain->SetBranchAddress("bdt_ggFCR2", &bdt_ggFCR2, &b_bdt_ggFCR2);
     fChain->SetBranchAddress("bdt_ggFCR2_trans", &bdt_ggFCR2_trans, &b_bdt_ggFCR2_trans);
     fChain->SetBranchAddress("bdt_Top_trans", &bdt_Top_trans, &b_bdt_Top_trans);
     fChain->SetBranchAddress("bdt_Top", &bdt_Top, &b_bdt_Top);
     fChain->SetBranchAddress("bdt_Zjets", &bdt_Zjets, &b_bdt_Zjets);
     fChain->SetBranchAddress("bdt_Zjets_trans", &bdt_Zjets_trans, &b_bdt_Zjets_trans);
     Notify();
     return ;
   }

   
   // only read branches correspondig to what is needed.
   // only read branches correspondig to what is needed.
   fChain->SetBranchStatus("weight",1);
   fChain->SetBranchStatus("lep0_phi",1);
   fChain->SetBranchStatus("lep1_phi", 1);
   fChain->SetBranchStatus("jet0_phi", 1);
   fChain->SetBranchStatus("jet1_phi", 1);
   fChain->SetBranchStatus("jet2_phi", 1);
   fChain->SetBranchStatus("lep0_is_m", 1);
   fChain->SetBranchStatus("jet0_pt", 1);
   fChain->SetBranchStatus("jet1_pt", 1);
   fChain->SetBranchStatus("jet2_pt", 1);
   fChain->SetBranchStatus("inWWCR", 1);
   fChain->SetBranchStatus("inTopCR",1);
   fChain->SetBranchStatus("inZjetsCR",1);
   fChain->SetBranchStatus("inggFCR1",1);
   fChain->SetBranchStatus("inggFCR2", 1);
   fChain->SetBranchStatus("inggFCR3", 1);
   fChain->SetBranchStatus("inSR", 1);
   fChain->SetBranchStatus("jet0_E", 1);
   fChain->SetBranchStatus("jet1_E", 1);
   fChain->SetBranchStatus("jet2_E", 1);
   fChain->SetBranchStatus("MT", 1);
   fChain->SetBranchStatus("Mll", 1);
   fChain->SetBranchStatus("Mjj", 1);
   
   fChain->SetBranchAddress("weight", &weight, &b_weight);
   fChain->SetBranchAddress("lep0_phi", &lep0_phi, &b_lep0_phi);
   fChain->SetBranchAddress("lep1_phi", &lep1_phi, &b_lep1_phi);
   fChain->SetBranchAddress("jet0_phi", &jet0_phi, &b_jet0_phi);
   fChain->SetBranchAddress("jet1_phi", &jet1_phi, &b_jet1_phi);
   fChain->SetBranchAddress("jet2_phi", &jet2_phi, &b_jet2_phi);
   fChain->SetBranchAddress("lep0_is_m", &lep0_is_m, &b_lep0_is_m);
   fChain->SetBranchAddress("jet0_pt", &jet0_pt, &b_jet0_pt);
   fChain->SetBranchAddress("jet1_pt", &jet1_pt, &b_jet1_pt);
   fChain->SetBranchAddress("jet2_pt", &jet2_pt, &b_jet2_pt);
   fChain->SetBranchAddress("inWWCR", &inWWCR, &b_inWWCR);
   fChain->SetBranchAddress("inTopCR", &inTopCR, &b_inTopCR);
   fChain->SetBranchAddress("inZjetsCR", &inZjetsCR, &b_inZjetsCR);
   fChain->SetBranchAddress("inggFCR1", &inggFCR1, &b_inggFCR1);
   fChain->SetBranchAddress("inggFCR2", &inggFCR2, &b_inggFCR2);
   fChain->SetBranchAddress("inggFCR3", &inggFCR3, &b_inggFCR3);
   fChain->SetBranchAddress("inSR", &inSR, &b_inSR);
   fChain->SetBranchAddress("jet0_E", &jet0_E, &b_jet0_E);
   fChain->SetBranchAddress("jet1_E", &jet1_E, &b_jet1_E);
   fChain->SetBranchAddress("jet2_E", &jet2_E, &b_jet2_E);
   fChain->SetBranchAddress("MT", &MT, &b_MT);
   fChain->SetBranchAddress("Mjj", &Mjj, &b_Mjj);
   fChain->SetBranchAddress("Mll", &Mll, &b_Mll);
   
    
   if( m_readmode >=ReadMode::READBDT || m_readmode == ReadMode::FULL){
     // BDT
     fChain->SetBranchStatus("nJets", 1);
     fChain->SetBranchStatus("jet0_eta", 1);
     fChain->SetBranchStatus("jet1_eta", 1);
     fChain->SetBranchStatus("jet2_eta", 1);
     fChain->SetBranchStatus("lep0_eta", 1);
     fChain->SetBranchStatus("lep1_eta", 1);
     fChain->SetBranchStatus("SEtajj", 1);
     fChain->SetBranchStatus("DPhijj", 1);
     fChain->SetBranchStatus("DEtajj",1);
     fChain->SetBranchStatus("DYjj", 1);
     
    
     fChain->SetBranchStatus("DYll", 1);
     fChain->SetBranchStatus("DPhill", 1);
    
     fChain->SetBranchStatus("ptTot", 1);
     fChain->SetBranchStatus("lep0_pt", 1);
     fChain->SetBranchStatus("lep1_pt", 1);
     
     // Zjets bdt
     fChain->SetBranchStatus("MET",1);
     fChain->SetBranchStatus("TrackMET", 1);
     fChain->SetBranchStatus("METRel_withJets", 1);
     fChain->SetBranchStatus("TrackMETRel_withJets", 1);
     
     // ggFCR1
     fChain->SetBranchStatus("mtt", 1);
     fChain->SetBranchStatus("OLV",1);
     // BDT
     fChain->SetBranchAddress("nJets", &nJets, &b_nJets);
     fChain->SetBranchAddress("jet0_eta", &jet0_eta, &b_jet0_eta);
     fChain->SetBranchAddress("jet1_eta", &jet1_eta, &b_jet1_eta);
     fChain->SetBranchAddress("jet2_eta", &jet2_eta, &b_jet2_eta);
     fChain->SetBranchAddress("lep0_eta", &lep0_eta, &b_lep0_eta);
     fChain->SetBranchAddress("lep1_eta", &lep1_eta, &b_lep1_eta);
     fChain->SetBranchAddress("SEtajj", &SEtajj, &b_SEtajj);
     fChain->SetBranchAddress("DPhijj", &DPhijj, &b_DPhijj);
     fChain->SetBranchAddress("DEtajj", &DEtajj, &b_DEtajj);
     fChain->SetBranchAddress("DYjj", &DYjj, &b_DYjj);
     
     fChain->SetBranchAddress("DYll", &DYll, &b_DYll);
     fChain->SetBranchAddress("DPhill", &DPhill, &b_DPhill);
    
     fChain->SetBranchAddress("ptTot", &ptTot, &b_ptTot);
     fChain->SetBranchAddress("lep0_pt", &lep0_pt, &b_lep0_pt);
     fChain->SetBranchAddress("lep1_pt", &lep1_pt, &b_lep1_pt);
     
    // Zjets bdt 
     fChain->SetBranchAddress("MET", &MET, &b_MET);
     fChain->SetBranchAddress("TrackMET", &TrackMET, &b_TrackMET);
     fChain->SetBranchAddress("METRel_withJets", &METRel_withJets, &b_METRel_withJets);
     fChain->SetBranchAddress("TrackMETRel_withJets", &TrackMETRel_withJets, &b_TrackMETRel_withJets);
     
     // ggFCR1
     fChain->SetBranchAddress("mtt", &mtt, &b_mtt);
     fChain->SetBranchAddress("OLV", &OLV, &b_OLV);
   }

    // ggFCR2

    //TopCR
    
   if( m_readmode == ReadMode::FULL || m_readmode >  ReadMode::READBDT){
     fChain->SetBranchStatus("*",1);
    // all the rest 
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("DPhil0j0", &DPhil0j0, &b_DPhil0j0);
   fChain->SetBranchAddress("DPhil0j1", &DPhil0j1, &b_DPhil0j1);
   fChain->SetBranchAddress("DPhil1j0", &DPhil1j0, &b_DPhil1j0);
   fChain->SetBranchAddress("DPhil1j1", &DPhil1j1, &b_DPhil1j1);
   fChain->SetBranchAddress("DEtal0j0", &DEtal0j0, &b_DEtal0j0);
   fChain->SetBranchAddress("DEtal0j1", &DEtal0j1, &b_DEtal0j1);
   fChain->SetBranchAddress("DEtal1j0", &DEtal1j0, &b_DEtal1j0);
   fChain->SetBranchAddress("DEtal1j1", &DEtal1j1, &b_DEtal1j1);
   fChain->SetBranchAddress("DPhiTSTtrk", &DPhiTSTtrk, &b_DPhiTSTtrk);
   fChain->SetBranchAddress("DPhiCSTtrk", &DPhiCSTtrk, &b_DPhiCSTtrk);
   fChain->SetBranchAddress("TrackMETx", &TrackMETx, &b_TrackMETx);
   fChain->SetBranchAddress("TrackMETy", &TrackMETy, &b_TrackMETy);
   fChain->SetBranchAddress("DPhillMETcst", &DPhillMETcst, &b_DPhillMETcst);
   fChain->SetBranchAddress("ptTotTrk", &ptTotTrk, &b_ptTotTrk);
   fChain->SetBranchAddress("DPhijMET", &DPhijMET, &b_DPhijMET);
   fChain->SetBranchAddress("METx", &METx, &b_METx);
   fChain->SetBranchAddress("DPhillMET", &DPhillMET, &b_DPhillMET);
   fChain->SetBranchAddress("METy", &METy, &b_METy);
   fChain->SetBranchAddress("METRel_noJets", &METRel_noJets, &b_METRel_noJets);
   fChain->SetBranchAddress("TrackMETRel_noJets", &TrackMETRel_noJets, &b_TrackMETRel_noJets);
   fChain->SetBranchAddress("MET_CST", &MET_CST, &b_MET_CST);
   fChain->SetBranchAddress("MET_TST", &MET_TST, &b_MET_TST);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("nJetsTight", &nJetsTight, &b_nJetsTight);
   fChain->SetBranchAddress("nBJetsSubMV2c10", &nBJetsSubMV2c10, &b_nBJetsSubMV2c10);
   fChain->SetBranchAddress("centralJetVetoLeadpT", &centralJetVetoLeadpT, &b_centralJetVetoLeadpT);
   fChain->SetBranchAddress("mZ", &mZ, &b_mZ);
   fChain->SetBranchAddress("MT2_1Jet", &MT2_1Jet, &b_MT2_1Jet);
   fChain->SetBranchAddress("nJetsFJVT3030", &nJetsFJVT3030, &b_nJetsFJVT3030);
   fChain->SetBranchAddress("Ml0j0", &Ml0j0, &b_Ml0j0);
   fChain->SetBranchAddress("Ml0j1", &Ml0j1, &b_Ml0j1);
   fChain->SetBranchAddress("Ml1j0", &Ml1j0, &b_Ml1j0);
   fChain->SetBranchAddress("Ml1j1", &Ml1j1, &b_Ml1j1);
   
   fChain->SetBranchAddress("lep0_truthType", &lep0_truthType, &b_lep0_truthType);
   fChain->SetBranchAddress("lep1_truthType", &lep1_truthType, &b_lep1_truthType);
   fChain->SetBranchAddress("lep0_truthOrigin", &lep0_truthOrigin, &b_lep0_truthOrigin);
   fChain->SetBranchAddress("lep1_truthOrigin", &lep1_truthOrigin, &b_lep1_truthOrigin);
   fChain->SetBranchAddress("lep0_E", &lep0_E, &b_lep0_E);
   fChain->SetBranchAddress("lep1_E", &lep1_E, &b_lep1_E);
   
   fChain->SetBranchAddress("lep0_is_e", &lep0_is_e, &b_lep0_is_e);
   fChain->SetBranchAddress("lep1_is_m", &lep1_is_m, &b_lep1_is_m);
   fChain->SetBranchAddress("lep1_is_e", &lep1_is_e, &b_lep1_is_e);

   // disabled 
   //fChain->SetBranchAddress("bdtMltClass", &bdtMltClass, &b_bdtMltClass);
   
   }
   
   Notify();
}

inline Bool_t minitree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

inline void minitree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
inline Int_t minitree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef minitree_cxx

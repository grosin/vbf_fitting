#!/bin/bash 

CNFG=$1
PREFIX=$2
VAR=$3 # here we define the extra arguments like the variable we are fitting 
DIR=${PWD}

#samples="vbf"
#samples="top Vgamma diboson Zjets vbf ggf"
samples="top Vgamma diboson Zjets0 vbf ggf data vh htt Fakes"


CODEDIR=/atlasgpfs01/usatlas/data/jennyz/vbfhww_fitting/vbfhww_fitting/makeInputs/
mkdir -p jobsSamplesAndSys_"$PREFIX"/error 
mkdir -p jobsSamplesAndSys_"$PREFIX"/log
mkdir -p jobsSamplesAndSys_"$PREFIX"/output
#listOfSys=$PWD/sysVars_v20_v2.txt
#listOfSys=$PWD/../AllSystematics.txt
listOfSys=$PWD/../AllSystematics.txt
#listOfSys=$PWD/../missing_syst.txt
#listOfSys=$PWD/../MET_systematics.txt

#listOfSys=$PWD/../nom.txt
#listOfFiles=$PWD/../list_all_local_v21_v2.txt
listOfFiles=$PWD/../input_files_2jets.txt
#listOfFiles=$PWD/../input_files_2jets_vbfa.txt

for s in ${samples} ; do :
    while read sys ; do : 
	if [[ $sys == *"#"* ]] ; then
            continue;
        fi
	if [[ $s == *"Fakes"* && $sys != *"nominal"* ]] ; then 
	   continue; 
	fi
	
	if [[ $s == *"data"* && $sys != *"nominal"* ]]; then
            continue;
        fi

	mkdir -p jobsSamplesAndSys_"$PREFIX"/run/"$s"_"$sys"
	
	echo "Submitting for $file $s for $sys"
	#name of job .sh
	bb="$s"_"$sys"
	# create the executable                                                                                                                                                                                 
     	echo "#!/bin/bash" > jobsSamplesAndSys_"$PREFIX"/$bb.sh
	echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	echo "alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	echo "asetup AnalysisBase,21.2.91,here" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	#echo "echo 'password' | kinit username@CERN.CH" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	#echo "lsetup \"root 6.08.06-HiggsComb-x86_64-slc6-gcc49-opt\" " >> $bb.sh
	#echo "source /afs/cern.ch/user/g/grosin/HWWAnalysis/HistFitter/setup.sh" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	echo "cd $DIR/jobsSamplesAndSys_"$PREFIX"/run/$bb" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	echo "echo $sys > $DIR/jobsSamplesAndSys_"$PREFIX"/run/$bb/sysVars_v21.txt" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
#	cat $listOfFiles | grep $s > $DIR/jobsSamplesAndSys_"$PREFIX"/run/$bb/list.txt
	cat $listOfFiles  > $DIR/jobsSamplesAndSys_"$PREFIX"/run/$bb/list.txt
	ref=`head -n 1 $DIR/jobsSamplesAndSys_"$PREFIX"/run/$bb/list.txt`
	echo "cd $DIR/jobsSamplesAndSys_"$PREFIX"/run/$bb/" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	echo "mkdir -p /tmp/jennyz/"$DIR"/jobsSamplesAndSys_"$PREFIX"/run/"$bb"/" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	if [[ $sys == *"nominal"* ]] ; then 
	    #echo "$CODEDIR/processTrees $ref "$DIR"/jobsSamplesAndSys_"$PREFIX"/run/"$bb"/output_"$PREFIX".root $DIR/$CNFG -chain $DIR/jobsSamplesAndSys_"$PREFIX"/run/$bb/list.txt -pattern $s"_"$sys -var $VAR" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	    echo "$CODEDIR/processTrees $ref "/tmp/jennyz/"$DIR"/jobsSamplesAndSys_"$PREFIX"/run/"$bb"/output_"$PREFIX".root" $DIR/$CNFG -chain $DIR/jobsSamplesAndSys_"$PREFIX"/run/$bb/list.txt -pattern $s"_"$sys -var $VAR" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	else  
	    #echo "$CODEDIR/processTrees $ref "$DIR"/jobsSamplesAndSys_"$PREFIX"/run/"$bb"/output_"$PREFIX".root $DIR/$CNFG -chain $DIR/jobsSamplesAndSys_"$PREFIX"/run/$bb/list.txt -pattern $s"_"$sys"__" -var $VAR" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
            echo "$CODEDIR/processTrees $ref "/tmp/jennyz/"$DIR"/jobsSamplesAndSys_"$PREFIX"/run/"$bb"/output_"$PREFIX".root" $DIR/$CNFG -chain $DIR/jobsSamplesAndSys_"$PREFIX"/run/$bb/list.txt -pattern $s"_"$sys"__" -var $VAR" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	fi
	echo " hadd -T "$DIR"/jobsSamplesAndSys_"$PREFIX"/run/"$bb"/output_"$PREFIX".root /tmp/jennyz/"$DIR"/jobsSamplesAndSys_"$PREFIX"/run/"$bb"/output_"$PREFIX".root" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	echo "rm -fr /tmp/jennyz/"$DIR"/jobsSamplesAndSys_"$PREFIX"/run/"$bb"/" >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	echo "echo All Done " >> jobsSamplesAndSys_"$PREFIX"/$bb.sh
	chmod +x jobsSamplesAndSys_"$PREFIX"/$bb.sh

	#create the submit file                                                                                                                                                                                 
	echo "executable    = $DIR/jobsSamplesAndSys_"$PREFIX"/$bb.sh" > jobsSamplesAndSys_"$PREFIX"/$bb.sub
	#echo "requirements = (OpSysAndVer =?= \"CentOS7\")" >> jobsSamplesAndSys_"$PREFIX"/$bb.sub
	#    echo "arguments             = $(ClusterId) $(ProcId)" >> jobsSamplesAndSys_"$PREFIX"/$bb.sub                                                                                                                                            
	echo "output                = $DIR/jobsSamplesAndSys_"$PREFIX"/output/$bb.sh.out" >> jobsSamplesAndSys_"$PREFIX"/$bb.sub
	echo "error                 = $DIR/jobsSamplesAndSys_"$PREFIX"/error/$bb.sh.err" >> jobsSamplesAndSys_"$PREFIX"/$bb.sub
	echo "log                   = $DIR/jobsSamplesAndSys_"$PREFIX"/log/$bb.sh.log" >> jobsSamplesAndSys_"$PREFIX"/$bb.sub
	echo "getenv          = True" >> jobsSamplesAndSys_"$PREFIX"/$bb.sub
	echo "accounting_group = group_atlas.bnl" >> jobsSamplesAndSys_"$PREFIX"/$bb.sub
#	if [[ $s == *"diboson"* ]] ; then  
#	    echo "+JobFlavour = \"tomorrow\"" >> jobsSamplesAndSys_"$PREFIX"/$bb.sub
#	    echo "MY.JobFlavour = \"tomorrow\"" >> jobsSamplesAndSys_"$PREFIX"/$bb.sub
#       else
	echo "+JobFlavour = \"longlunch\"" >> jobsSamplesAndSys_"$PREFIX"/$bb.sub
        echo "MY.JobFlavour = \"longlunch\"" >> jobsSamplesAndSys_"$PREFIX"/$bb.sub
#	fi 
	echo "queue" >> jobsSamplesAndSys_"$PREFIX"/$bb.sub
	#       condor_submit jobsSamplesAndSys_"$PREFIX"/$bb.sub
	
#	if [[ $s == *"Fakes"* ]] ; then 
#	    break 
#	fi
	
#	if [[ $s == *"data"* ]] ; then
#            break
#        fi

    done < $listOfSys
done

cd $DIR/jobsSamplesAndSys_"$PREFIX"
echo "executable    = $DIR/jobsSamplesAndSys_"$PREFIX"/\$(type)" > $DIR/jobsSamplesAndSys_"$PREFIX"/all.sub
#echo "requirements = (OpSysAndVer =?= \"CentOS7\")" >> $DIR/jobsSamplesAndSys_"$PREFIX"/all.sub
echo "arguments             = \"\$(type)\" " >> $DIR/jobsSamplesAndSys_"$PREFIX"/all.sub 
echo "output                = $DIR/jobsSamplesAndSys_"$PREFIX"/output/\$(type).out" >> $DIR/jobsSamplesAndSys_"$PREFIX"/all.sub
echo "error                 = $DIR/jobsSamplesAndSys_"$PREFIX"/error/\$(type).err" >> $DIR/jobsSamplesAndSys_"$PREFIX"/all.sub
echo "log                   = $DIR/jobsSamplesAndSys_"$PREFIX"/log/\$(type).log" >> $DIR/jobsSamplesAndSys_"$PREFIX"/all.sub
echo "getenv          = True" >> $DIR/jobsSamplesAndSys_"$PREFIX"/all.sub
echo "accounting_group = group_atlas.bnl" >> $DIR/jobsSamplesAndSys_"$PREFIX"/all.sub
echo "+JobFlavour = \"tomorrow\"" >> $DIR/jobsSamplesAndSys_"$PREFIX"/all.sub
echo "MY.JobFlavour = \"tomorrow\"" >> $DIR/jobsSamplesAndSys_"$PREFIX"/all.sub
echo "queue type matching *.sh" >> $DIR/jobsSamplesAndSys_"$PREFIX"/all.sub
condor_submit all.sub
cd $DIR

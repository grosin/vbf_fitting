#!/bin/bash 
dir=$1
for file in $dir/output/*.out ; do :
#    if [[ $file == *".sh.out"* ]] ; then 
#	continue
#    fi
#    if [[ `tail -n 1 $file` == *"File is more tha"*  || `tail -n 1 $file` == *"Check your AFS maximum file size limit for example"*  || `tail -n 1 $file` = *"file probably overwritten: stopping reporting error messages"* ]] ; then 
	#	continue
    status=`tail -n 1 $file` 
    if [[ $status != *"All Done"* ]] ; then 
	echo $file ; 
	tail -n 1 $file
	echo "Re submitting" 
#	rm $dir/run/
	job=${file%.sh.out}
	job=`basename $job`
#	rm "$dir"/run/"$job"/output*root 
	a=`cat "$dir"/log/"$job".sh.log | grep "due to wall time exceeded allowed max"`
	echo $a
	if [[ $a == *"due to wall time exceeded allowed max"* ]] ; then 
	    tail -n 3 "$dir"/"$job".sub
	    sed -i 's/longlunch/workday/' "$dir"/"$job".sub
	    echo "Changed to " 
	    tail -n 3 "$dir"/"$job".sub
	fi
	rm "$dir"/run/"$job"/output*root
	rm "$dir"/output/"$job".sh.out
	rm  "$dir"/log/"$job".sh.log
	rm "$dir"/error/"$job".sh.err
	job=`echo "$dir/$job".sub`
	echo "Re submit $job"
	condor_submit $job
    fi 
done 

for file in $dir/log/*.sh.log ; do :
    a=`cat $file | grep "due to wall time exceeded allowed max"`
    job=${file%.sh.log}
    job=`basename $job`
    if [[ $a == *"due to wall time exceeded allowed max"* ]] ; then
	tail -n 3 "$dir"/"$job".sub
        sed -i 's/longlunch/workday/' "$dir"/"$job".sub
        echo "Changed to "
        tail -n 3 "$dir"/"$job".sub
	rm "$dir"/run/"$job"/output*.root
	rm "$dir"/run/"$job"/output*root                                                                                                                                                                                                                                                                                                                                                                                             
	rm "$dir"/output"$job".sh.out                                                                                                                                                                                                                                                                                                                                                                                           
	rm  "$dir"/log/"$job".sh.log                                                                                                                                                                                                                                                                                                                                                                                           
	rm "$dir"/error/"$job".sh.err  
	echo "Re submit $job"
	job=`echo "$dir/$job".sub`
	condor_submit $job
    fi 
done 

for file in $dir/error/*.sh.err ; do :
    a=`cat $file | grep "There was a crash"`
    job=${file%.sh.err}
    job=`basename $job`
    if [[ $a == *"There was a crash."* ]] ; then
        tail -n 3 "$dir"/"$job".sub
       rm "$dir"/run/"$job"/output*root                                                                                                                                                                                                                                                                                                                                                                                             
       rm "$dir"/output/"$job".sh.out                                                                                                                                                                                                                                                                                                                                                                                           
       rm  "$dir"/log/"$job".sh.log                                                                                                                                                                                                                                                                                                                                                                                             
       rm "$dir"/error/"$job".sh.err      
        job=`echo "$dir/$job".sub`
	echo "Re submit $job"
        condor_submit $job
    fi
done

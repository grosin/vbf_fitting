/* Macro to perofrm a clean up 
 *
 *
 */

#include "functions.h"
#include "TFile.h"
#include "TTree.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include "TH1.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TF1.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TF1.h"


using namespace std;
using namespace std::chrono;




//int main cleanUpTrees(string fname="",string foutName="clean"){
int main (int argc, char* argv[]){

  string fname=argv[1];
  string foutName=argv[2];
  

  TFile *fin=TFile::Open(fname.c_str());
  string finalName=foutName+ "_" + fname; 
  TFile *fout=new TFile(finalName.c_str(),"RECREATE");
  

  // loop over all the keys of the file
  std::map <string,string> AllTrees;
  std::map<string,string> RunOverTrees;
  
  TKey *keyP=nullptr;
  TIter nextP(fin->GetListOfKeys());
  int nTrees=0;

  vector <string> removePatterns;
  removePatterns.push_back("JET_JER_DataVsMC_MC162");
  removePatterns.push_back("JET_JER_EffectiveNP_12");
  removePatterns.push_back("JET_JER_EffectiveNP_22");
  removePatterns.push_back("JET_JER_EffectiveNP_32");
  removePatterns.push_back("JET_JER_EffectiveNP_42");
  removePatterns.push_back("JET_JER_EffectiveNP_52");
  removePatterns.push_back("JET_JER_EffectiveNP_62");
  removePatterns.push_back("JET_JER_EffectiveNP_72");
  removePatterns.push_back("JET_JER_EffectiveNP_82");
  removePatterns.push_back("JET_JER_EffectiveNP_92");
  removePatterns.push_back("JET_JER_EffectiveNP_102");
  removePatterns.push_back("JET_JER_EffectiveNP_112");
  removePatterns.push_back("JET_JER_EffectiveNP_12restTerm2");


  removePatterns.push_back("JET_JER_DataVsMC_MC16");
  removePatterns.push_back("JET_JER_EffectiveNP_1");
  removePatterns.push_back("JET_JER_EffectiveNP_2");
  removePatterns.push_back("JET_JER_EffectiveNP_3");
  removePatterns.push_back("JET_JER_EffectiveNP_4");
  removePatterns.push_back("JET_JER_EffectiveNP_5");
  removePatterns.push_back("JET_JER_EffectiveNP_6");
  removePatterns.push_back("JET_JER_EffectiveNP_7");
  removePatterns.push_back("JET_JER_EffectiveNP_8");
  removePatterns.push_back("JET_JER_EffectiveNP_9");
  removePatterns.push_back("JET_JER_EffectiveNP_10");
  removePatterns.push_back("JET_JER_EffectiveNP_11");
  removePatterns.push_back("JET_JER_EffectiveNP_12restTerm");
  
  vector <string> vars;
  vars.push_back("__1up");
  vars.push_back("__1down");
  
  while ((keyP=(TKey*)nextP())) {
    if (strcmp(keyP->GetClassName(),"TTree")) continue;
    string ttname=remove_extension(keyP->GetName());

    AllTrees[ttname]=ttname;
    // here remove trees that are not needed
    bool skip=false; 

    for(vector<string>::iterator v=vars.begin(); v!=vars.end(); v++){
    for(vector<string>::iterator l=removePatterns.begin(); l!=removePatterns.end(); l++){
      string patten= (*l)+ (*v);
      if (TString(ttname.c_str()).Contains( patten.c_str()))
        { skip=true; break; } 
    }}
    
    if(!skip) 
      RunOverTrees[ttname]=ttname;
    
  }

  // now create a new file
  for(std::map<string,string>::iterator it=RunOverTrees.begin(); it!=RunOverTrees.end(); it++){
    fout->cd();
    TTree *tin=(TTree*)fin->Get( (*it).second.c_str());
    cout<<"Reading tree "<< (*it).second<<endl;
    TTree *tout=(TTree*)tin->CloneTree();
    tout->Write();
    delete tout;
    delete tin;

  }

  fout->Close();

  return 0;

}

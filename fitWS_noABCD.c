#include "tools.h"
bool doSignificance=false;
bool doRanking=false;
bool isBBB=false;
bool addOtherTheory=false;
bool normTheoryPredtoYR4=true;
bool addSeparateImpactParameterForNorm=false;
bool addSeparateImpactParameterForNormAndDecorr=false;
bool addSeparateImpactParameterForNormAndDecorrPattern=true;
bool addCorrectLumiUncert=true;
std::map<string, vector<string>> lumiMap;

string matrix_file="matrix_projections_with_fakes.txt";
//string matrix_file="matrix_projection_MG.txt";
string VBFNLO_FILE="VBFNLO_new_Phase_Space_symErr.root";
string MGHw7_FILE="MGHw7_allObs_MjjDPhill.root";
string mg_ws="/eos/user/g/grosin/hist_fitter_output/ws_2021/stat/mg_ws/ws_Mjj_powpy.root";


double lumiErr=0.017;
  
vector <string> shapeAndNormDecorrPattern={"theo"};
bool exactDecorrNPPatterns=true;

//string topWWDscir="bdt_TopWWAll";
//string topWWDscir="bdt_TopWWAll2";
//string vbdDiscr="bdt_vbf"
//string topWWDscir="Mll";
//string vbfDscir="DPhill";

//string topWWDscir="bdt_TopWWAll";
//string vbfDscir="DPhill";

//string topWWDscir="bdt_TopWWAll";
//string vbfDscir="MT";

string topWWDscir="bdt_TopWWAll";
string vbfDscir="bdt_vbf";

bool doReg=false;
double  tauD=0.5;
bool isDiff=true;
bool compareToOtherInput=false;
bool useDedicatedDiscrDiff=true; 

string migrationTheoryUncertFile="/eos/user/s/sagar/ATLAS/TheorySystematics/Mjj_NewGGFBinning/Mjj_vbf0_histos.root";
bool addSigMigrationUncert=true;
vector <string> migUncertainties={"generator","shower","pdf4lhc","alphas","scale"};
string baseNameNP="theo_vbf_";//"hvbf0theo_vbf_";

RooArgSet FindConstrainedParameters(RooWorkspace *w,RooArgSet inNPs,ModelConfig *model,RooSimultaneous *pdf,RooAbsData *data,int &iter,double threshold){

  if( iter >0) {  
    RooAbsReal *mNll= pdf->createNLL( *data,
				      NumCPU(3,2),
				      RooFit::Constrain(*model->GetNuisanceParameters()), 
				      RooFit::GlobalObservables(*model->GetGlobalObservables()),
				      RooFit::Offset(true), 
				      Extended(true),
				      RooFit::Verbose(kFALSE));
    
    RooMinimizer *msys= new RooMinimizer(*mNll);;
    msys->setEps(0.01);
    msys->optimizeConst(2);
    msys->setStrategy(2);
    msys->minimize("Minuit2");
    //msys->setEps(1e-3);
    //msys->minimize("Minuit2");
    msys->hesse();
    iter++;
    delete msys;
    delete mNll;
  }
  
  RooArgSet outNps;
  TIter iterNps=inNPs.createIterator();
  RooRealVar *tmpVar=nullptr;
  int nFixed=0;
  int nTotal=0;
  while( (tmpVar=(RooRealVar*)iterNps.Next())){
    // threshofld to be fixed or not;
    if( tmpVar->getError() < threshold){
      tmpVar->setConstant(false);
      outNps.add(*tmpVar);
    }
    else {
      tmpVar->setConstant(true);
      nFixed++;
    }
    nTotal++;
  }

  
  
  if( iter > -1 && iter  < 4 &&  nTotal-nFixed > 0) 
    return FindConstrainedParameters(w,outNps,model,pdf,data,iter,threshold);
  else {
    cout<<"List of Nps constrained more than "<<threshold<<" after "<<iter<<" iterations is "<<endl;
    outNps.Print();
    TIter tt=outNps.createIterator();
    RooRealVar *n=nullptr;
    cout<<"{";
    while( (n=(RooRealVar*)tt.Next())){
      cout<<"\""<<n->GetName()<<"\", ";
    }
    cout<<"}"<<endl;
    return outNps;
  }
}

vector <double> GetSymmMigUncertainty(TFile *fin=nullptr, string variation="",string distr="",int bin=0,string basesigname="hvbf0theo_vbf"){
  vector <double> uncert=vector<double>(0);
  vector <double> empty=vector <double>(0);
  // template of naming 
  //hvbf0theo_vbf_generatorHigh_Migration_obs_
  // y axis truth
  // x axis reco 
  std::map<string, TH2F*> varHistMap;
  std::map<string, TH1F*> varHistMapFakes;
  vector <string> aggVars={"Nom","Low","High"};

  for(vector<string>::iterator is=aggVars.begin(); is!=aggVars.end(); is++){
    string hname=Form("%s_%s%s_Migration_obs_",basesigname.c_str(),variation.c_str(),(*is).c_str());
    varHistMap[(*is)]=(TH2F*)fin->Get(hname.c_str());
    if( varHistMap[(*is)] == nullptr) {
      cout<<"ERROR: Signal theory variation with name: "<<hname<<" was not found returning empty vector 0 "<<endl;
      for(std::map<string,TH2F*>::iterator it=varHistMap.begin(); it!=varHistMap.end(); it++)
	delete (*it).second; 
      return empty;
    }
  }
  // for reco bin with value bin get all migrations of the y axis

  for(int j=1; j<(int)varHistMap["Nom"]->GetNbinsY(); j++){
    double nomval = varHistMap["Nom"]->GetBinContent(bin+1,j);
    double highval= varHistMap["High"]->GetBinContent(bin+1,j);
    double lowval= varHistMap["Low"]->GetBinContent(bin+1,j);

    
    // symmetrised uncertainty 
    double thisUncert= (TMath::Abs(highval-lowval)*0.5)/nomval; 
    // Double check the other directions
    double lowerr=(nomval-lowval)/nomval;
    double higherr=(nomval-highval)/nomval;
    double assymmetry=TMath::Abs( TMath::Abs(lowerr) - TMath::Abs(higherr));
    if( assymmetry >=0 ) { cout<<"WARNING: migration uncertianty for reco "<<bin<<" fiducial bin "<<j-1<<" is assymmetric "<<assymmetry<<endl;}
    cout<<"--> migration uncertainty for reco bin "<<bin<<" fiducilal bin "<<j-1<<" is "<<thisUncert<<endl;

    if( nomval ==0 || (nomval!=0 && highval==0) || (nomval!=0 && highval==0)){
      uncert.push_back(0.0);
      cout<<"Nominal is <=0 using 0 as uncertainty "<<endl;
      continue;
    }

    // set the correct signed impact
    if(lowval > highval )
      thisUncert = -1.0 * thisUncert ; 
    
    uncert.push_back(thisUncert); 
  }
  // Add the fakes
  // check naming!!!
  for(vector<string>::iterator is=aggVars.begin(); is!=aggVars.end(); is++){
    string hname=Form("%s_%s%s_RPTF_obs_",basesigname.c_str(),variation.c_str(),(*is).c_str());
    varHistMapFakes[(*is)]=(TH1F*)fin->Get(hname.c_str());
    if( varHistMapFakes[(*is)] == nullptr) {
      cout<<"ERROR: Signal theory variation with name: "<<hname<<" was not found returning empty vector 0 "<<endl;
      for(std::map<string,TH1F*>::iterator it=varHistMapFakes.begin(); it!=varHistMapFakes.end(); it++)
	delete (*it).second; 
      return empty;
    }
  }
  
  double nomvalF = varHistMapFakes["Nom"]->GetBinContent(bin+1);
  double highvalF= varHistMapFakes["High"]->GetBinContent(bin+1);
  double lowvalF= varHistMapFakes["Low"]->GetBinContent(bin+1);
  
  double thisUncertF= (TMath::Abs(highvalF-lowvalF)*0.5)/nomvalF; 
  // Double check the other directions
  double lowerrF=(nomvalF-lowvalF)/nomvalF;
  double higherrF=(nomvalF-highvalF)/nomvalF;
  double assymmetryF=TMath::Abs( TMath::Abs(lowerrF) - TMath::Abs(higherrF));
  if( assymmetryF >=0 ) { cout<<"WARNING: fakes  uncertianty for reco "<<bin<<" fiducial bin "<<assymmetryF<<endl;}
  cout<<"--> fakes uncertainty for reco bin "<<bin<<" "<<thisUncertF<<endl;

  // set the correct signed impact
  if(lowvalF > highvalF )
    thisUncertF = -1.0 * thisUncertF ; 
  
  if( nomvalF ==0 || (nomvalF!=0 && highvalF==0) || (nomvalF!=0 && highvalF==0)){
    uncert.push_back(0.0);
    cout<<"Nominal fakes is <=0 using 0 as uncertainty "<<endl;
  }
  else {
    uncert.push_back(thisUncertF); 
  }
  
  
  for(std::map<string,TH2F*>::iterator it=varHistMap.begin(); it!=varHistMap.end(); it++)
    delete (*it).second; 
  
  for(std::map<string,TH1F*>::iterator it=varHistMapFakes.begin(); it!=varHistMapFakes.end(); it++)
    delete (*it).second; 
  return uncert;
  
}


std::map<string,vector<double>> ScanAsimovVar(RooWorkspace *w=nullptr,
                                              RooWorkspace *w2=nullptr,
                                              RooAbsPdf *NewPdf=nullptr,
                                              string parName="",double val=1.0,string newName=""){

 std:map<string, vector<double>> fitResults;
 
 
  ModelConfig *model=(ModelConfig*)w->obj("ModelConfig");
  RooCategory *channelCat = (RooCategory*)w->cat("channelCat" );
  RooSimultaneous *pdf=(RooSimultaneous*)model->GetPdf();
  RooArgSet *global=(RooArgSet*)model->GetGlobalObservables();
  RooRealVar *mu_var=(RooRealVar*)w->var(parName.c_str());
  mu_var->setVal(val);

  //ModelConfig *altModel=(ModelConfig*)w->obj("altModel");
  ModelConfig *altModel=(ModelConfig*)w2->obj("atlModel");
  //RooAbsPdf *NewPdf=altModel->GetPdf();

  //const RooArgSet *nomSnap=w->getSnapshot("prefit");
  cout<<"Generating data for point "<<val<<endl;

  
  //RooAbsData *data=AsymptoticCalculator::MakeAsimovData(*model,*nomSnap,*global);
  RooAbsData *data=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
  //RooAbsData *data=(RooAbsData*)w->data("asimovDataFullModel");

    
  //cout<<"Scanning point 8"<<val<<endl;
  //mu_var->SetName(newName.c_str());
  //w->import(*mu_var);
  //cout<<"Scanning point 9"<<val<<endl;

  string oldName=mu_var->GetName();
  mu_var->SetName(newName.c_str());

  
  
  cout<<"Fitting "<<endl;
  RooAbsReal *mNll= NewPdf->createNLL( *data,
                                       //RooFit::Constrain(*model->GetNuisanceParameters()), 
                                       //RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                       RooFit::Offset(kTRUE), 
                                       Extended(true),
                                       RooFit::Verbose(kFALSE));

  RooMinimizer *msys= new RooMinimizer(*mNll);;
  msys->setEps(0.1);
  msys->optimizeConst(2);
  msys->setStrategy(1);
  msys->minimize("Minuit2");
  
  RooFitResult *fitRes=msys->save("tmpFit");
  RooArgList parsFitr=fitRes->floatParsFinal();
  
  TIter it = parsFitr.createIterator();
  RooRealVar *tmpV=nullptr;
  while( (tmpV=(RooRealVar*)it.Next())){
    vector <double> rr;
    rr.push_back(tmpV->getVal());
    rr.push_back(tmpV->getError());
    fitResults[tmpV->GetName()]=rr;
  }
  
  mu_var->SetName(oldName.c_str());
  mu_var->setVal(1.0);
  
  delete fitRes;
  delete msys;
  delete mNll;
  delete data;

  model->GetPdf()->Print("V");
  
  return fitResults;
}


// isBBB = is bin by bin; 
void fitWS(string fname="BkgOnly_combined_NormalMeasurement_model_afterFit_asimovData.root",bool patchDiff=true, string currentDistribution="Mjj",
	   bool fractionalABCD=false,bool freeTopPerBin=false,bool decorrNPs=false,bool removeTopCR=false, bool removeggFRegion3=false, bool removeggFRegion2=false,bool commonmuAB=false,bool noABCD=true,string wsname="w"){
  /*
   * fractionalABCD do actual ratio for ggF ABCD
   * freeTopPerBin normalise top o each bin of mJJ and have one DOF for each bin 
   * decorrNPs decorrlate nuisance paramters based on a scheme defined in the code 
   * removeTopCR remove all topCR and keep only SRVBF 
   *
   */

  lumiMap["CRZjets"]={"vh","htt","ggf","top","diboson","vbf","Vgamma"};
  lumiMap["CRGGF1"]={"vh","htt","vbf","Vgamma","Zjets","top","diboson"};
  lumiMap["CRGGF2"]={"vh","htt","vbf","Vgamma","Zjets","top","diboson"};
  lumiMap["CRGGF3"]={"vh","htt","vbf","Vgamma","Zjets","top","diboson"};
  lumiMap["SRVBF"]={"vh","htt","vbf","Vgamma","Zjets","top","diboson"};
  lumiMap["CRTop"]={"vh","htt","vbf","Vgamma","Zjets","top","diboson"};
  
  if(currentDistribution.compare("inc")==0) {
    cout<<"Inclusive fit "<<endl;
    isDiff=false;
    addOtherTheory=false;
  }
  ROOT::Math::MinimizerOptions::SetDefaultStrategy(2);
  //ROOT::Math::MinimizerOptions::SetDefaultPrecision(0.0000001);
  ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");

  int freeTopParams=2;

  if(currentDistribution.compare("jet1_pt")==0)
    freeTopParams=3;
  if(currentDistribution.compare("DYll")==0)
    freeTopParams=4;


  //TFile *f=TFile::Open("combined.root");
  TFile *f=TFile::Open(fname.c_str());
  RooWorkspace *w=(RooWorkspace*)f->Get(wsname.c_str());
  w->Print();
  ModelConfig *model=(ModelConfig*)w->obj("ModelConfig");
  RooCategory *channelCat = (RooCategory*)w->cat("channelCat" );
  RooSimultaneous *pdf=(RooSimultaneous*)model->GetPdf();
  //RooSimultaneous *pdf=(RooSimultaneous*)w->pdf("simPdf");
  if(pdf==nullptr) { cout<<"pdf is null"<<endl; return ; } 
  pdf->Print();

  RooArgSet *global=(RooArgSet*)model->GetGlobalObservables();
  RooRealVar *poi=(RooRealVar*)w->var("mu_vbf");
  poi->Print();
  /*
  if(isDiff){
    poi->setRange(0.0,5);
    poi->setVal(1.0);
    poi->setConstant(true);
   
  }
  */
  poi->Print();
  

  TH2F *hConfusionMatrix=nullptr;
  
  RooRealVar *mu_top=(RooRealVar*)w->var("mu_top");
  //mu_top->setConstant(false);
  //mu_top->setRange(0,5);

  RooRealVar *mu_ww=(RooRealVar*)w->var("mu_ww");
  //if(mu_ww!=nullptr)
  //mu_ww->setConstant(false);
  //mu_ww->setRange(0,5);
  
  RooRealVar *mu_wwTop=(RooRealVar*)w->var("mu_wwTop");
  mu_wwTop->setConstant(false);
  
  
  RooRealVar *mu_ggf=(RooRealVar*)w->var("mu_ggf");
  if(noABCD){
    w->allVars().selectByName("mu_ggf*")->setAttribAll("Constant",true);
    mu_ggf->setConstant(false);
    w->allVars().selectByName("mu_wwTop4*")->setAttribAll("Constant",true);
  }
  else 
    mu_ggf->setConstant(true);
  //mu_ggf->setRange(-5,+5);
  
  //mH->setVal(125);
  //RooAbsData *data=(RooAbsData*)w->data("asimovData");

  TLatex *lat=new TLatex();
  lat->SetTextSize(22);
  lat->SetTextFont(43);
    
  w->allVars().selectByName("mu_top*")->setAttribAll("Constant",true);
  w->allVars().selectByName("mu_ww")->setAttribAll("Constant",true);
  w->allVars().selectByName("mu_ww1")->setAttribAll("Constant",true);
  w->allVars().selectByName("mu_ww2")->setAttribAll("Constant",true);
  w->allVars().selectByName("mu_ww3")->setAttribAll("Constant",true);
  w->allVars().selectByName("mu_ww4")->setAttribAll("Constant",true);
   
  
  w->allVars().selectByName("mu_wwTop2*")->setAttribAll("Constant",false);
  w->allVars().selectByName("mu_wwTop3*")->setAttribAll("Constant",false);
  w->allVars().selectByName("mu_wwTop4*")->setAttribAll("Constant",false);
  

  w->allVars().selectByName("OneOverC_bin_")->setAttribAll("Constant",true);
  w->allVars().selectByName("matrix_bin+")->setAttribAll("Constant",true);
   
  isBBB=w->var("sigma_bin0")==nullptr ? true:false;


  w->allVars().selectByName("*matrix*")->setAttribAll("Constant",true);
  w->allVars().selectByName("*OneOver*")->setAttribAll("Constant",true);
   
  RooRealVar *lumi=(RooRealVar*)w->var("Lumi");
  lumi->Print();
  //lumi->setRange(139,139);
  //lumi->setVal(139);
  
   // patch of OneOverSigma
  if(!isBBB && !patchDiff) {
    double totReco2=0;
    for(int i=0; i<(int)1000; i++){
      RooRealVar *xx=(RooRealVar*)w->var(Form("sigma_bin%d",i));
      if( xx != nullptr){
	RooRealVar *oneOver=(RooRealVar*)w->var(Form("OneOverC_bin_%d",i));
	RooFormulaVar *migMat = (RooFormulaVar*)w->obj(Form("migMatProj_SRVBF_bin%d",i));
	//notFid_bin0_incl
	RooRealVar *notFid=(RooRealVar*)w->var(Form("notFid_bin%d_incl",i));
	double origNotFid=notFid->getVal();
	RooRealVar *muVbfBin=(RooRealVar*)w->var(Form("mu_vbf_bin_%d",i));
	cout<<"Orignal values"<<endl;
	muVbfBin->Print();
	//oneOver->Print();
	migMat->Print();

	//notFid->setVal(0.0);
	oneOver->setRange(-10000,+10000);
	oneOver->setVal(1/migMat->evaluate());
	//notFid->setVal(origNotFid);
	

	muVbfBin->setRange(-1000,+1000);
	muVbfBin->setVal(1.0);
	
	totReco2+=migMat->evaluate();
	cout<<"Changed  values"<<endl;
	muVbfBin->Print();
	//oneOver->Print();
	migMat->Print();
	cout<<endl;
      }
    }
    RooRealVar *vbfGlobNorm=(RooRealVar*)w->var("mu_vbf");
    vbfGlobNorm->setRange(-1e3,1e3);
    //vbfGlobNorm->setVal(1.0);
    vbfGlobNorm->Print();
  }

  
  //RooAbsData *data=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
  RooArgSet allVarsNominal; /*allVarsNominal.add(*model->GetSnapshot());*/
  RooArgSet allGlobs; allGlobs.add(*model->GetGlobalObservables());
  RooAbsData *data=AsymptoticCalculator::MakeAsimovData(*model, allVarsNominal,allGlobs);
  ////RooAbsData *data=AsymptoticCalculator::MakeAsimovData(*model,RooArgSet(*xs,mH), *global);
  ////RooAbsData *data=(RooAbsData*)w->data("asimovDataFullModel");

  data->Print();

  
  RooAbsData *data2=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
  data2->Print();
  //return;

  /*
  // Here open an other wosrkpace and extract the data
  TFile *falt=TFile::Open(Form("/Users/gaetano/Documents/universita/Atlas/Analysis/HiggsWW/Analysis/Fit/October2019_withSys/LauraHF/results/hist_v2_StefBinning_v21_r21_07_01_2021_v1_noBDTCut_SRL_ggFCRZeroOneJet_ABCD_TopCRinSR_decorr1JetSys_Mjj/inclusive/%s",fname.c_str()));

  //TFile *falt=TFile::Open(Form("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/October2019_withSys/LauraHF/results/hist_v2_StefBinning_v21_r21_07_01_2021_v1_noBDTCut_SRL_ggFCRZeroOneJet_ABCD_TopCRinSR_decorr1JetSys_Mjj/highPUFWD/%s",fname.c_str()));

  //TFile *falt=TFile::Open(Form("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/October2019_withSys/LauraHF/results/hist_v2_StefBinning_v21_r21_07_01_2021_v1_noBDTCut_SRL_ggFCRZeroOneJet_ABCD_TopCRinSR_decorr1JetSys_Mjj/lowPUFWD/%s",fname.c_str()));
  RooWorkspace *walt=(RooWorkspace*)falt->Get(wsname.c_str());
  ModelConfig *modelother=(ModelConfig*)walt->obj("ModelConfig");
  RooCategory *channelCat2 = (RooCategory*)walt->cat("channelCat" );
  RooSimultaneous *pdfother=(RooSimultaneous*)modelother->GetPdf();
  //RooAbsData *data=(RooAbsData*)walt->data("asimovDataFullModel");
  RooAbsData *data=AsymptoticCalculator::GenerateAsimovData(*pdfother,*modelother->GetObservables());
  */
   
  /*
  // Make two worksapces with both settings on
  mu_ww->setConstant(true);
  mu_top->setConstant(true);
  mu_wwTop->setConstant(false);
  RooAbsData *data1=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
  data1->SetName("asimovData_floatingTopWW");
  w->import(*data1,RecycleConflictNodes());
  w->writeToFile("XS_singleParWWTop_v10_04_2020.root");
  //w->writeToFile("singleParWWTop_v10_04_2020.root");
  */
  /*
  // Make two worksapces with both settings on
  mu_ww->setConstant(false);
  mu_top->setConstant(false);
  mu_wwTop->setConstant(true);
  RooAbsData *data2=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
  //data2->SetName("asimovData_commonTopWW");
  w->import(*data2,RecycleConflictNodes());
  w->writeToFile("XS_sepWWTopParams.root_v10_04_2020.root");
  //w->writeToFile("sepWWTopParams.root_v10_04_2020.root");
  //return ;
  */

 
  

    std::map<string,double> UnitsScaling;
   UnitsScaling["ab"]=1e3;
   UnitsScaling["fb"]=1;
   std::map<string,string> UnitsMap;
   UnitsMap["Mjj"]="ab";
   UnitsMap["Mll"]="ab";
   UnitsMap["pt_H"]="ab";
   UnitsMap["lep0_pt"]="ab";
   UnitsMap["lep1_pt"]="ab";
   UnitsMap["jet0_pt"]="ab";
   UnitsMap["jet1_pt"]="ab";
   UnitsMap["DYll"]="fb";
   UnitsMap["DYjj"]="fb";
   UnitsMap["SignedDPhijj"]="fb";
   UnitsMap["DPhill"]="fb";
   UnitsMap["costhetastar"]="fb";

   std::map<string,string> VarUnitMap;
   VarUnitMap["Mjj"]="[GeV]";
   VarUnitMap["Mll"]="[GeV]";
   VarUnitMap["pt_H"]="[GeV]";
   VarUnitMap["lep0_pt"]="[GeV]";
   VarUnitMap["lep1_pt"]="[GeV]";
   VarUnitMap["jet0_pt"]="[GeV]";
   VarUnitMap["jet1_pt"]="[GeV]";
   VarUnitMap["DYll"]="";
   VarUnitMap["DYjj"]="";
   VarUnitMap["costhetastar"]="";
   VarUnitMap["SignedDPhijj"]="[rad]";
   VarUnitMap["DPhill"]="[rad]";
   std::map<string,int> nBinsDiff;

   std::map<string, vector<double>> binEdesMap;
   double GeVtoMeV=1;
   std::map<string, string> unitsMap;
   unitsMap["Mjj"]="ab/GeV";
   unitsMap["Mll"]="ab/GeV";
   unitsMap["pt_H"]="ab/GeV";
   unitsMap["lep0_pt"]="ab/GeV";
   unitsMap["lep1_pt"]="ab/GeV";
   unitsMap["jet0_pt"]="ab/GeV";
   unitsMap["jet1_pt"]="ab/GeV";
   unitsMap["DYll"]="fb";
   unitsMap["DYjj"]="fb";
   unitsMap["costhetastar"]="fb";
   unitsMap["SignedDPhijj"]="fb/rad";
   unitsMap["DPhill"]="fb/rad";

   std::map<string,string> labelMap;
   labelMap["Mjj"]="#it{m}_{#it{jj}}";
   labelMap["Mll"]="#it{m}_{#it{ll}}";
   labelMap["pt_H"]="#it{H}_{#it{pt}}";
   labelMap["lep0_pt"]="#it{Leading Lepton}_{#it{pt}}";
   labelMap["lep1_pt"]="#it{Subleading Lepton}_{#it{pt}}";
   labelMap["jet0_pt"]="#it{Leading Jet}_{#it{pt}}";
   labelMap["jet1_pt"]="#it{Subleading Jet}_{#it{pt}}";
   labelMap["DYll"]="#it{#Deltay}_{ll}";
   labelMap["DYjj"]="#it{#Deltay}_{jj}";
   labelMap["costhetastar"]="#it{cos(#theta *)}";
   labelMap["SignedDPhijj"]="#it{#Delta}#varphi_{jj}";
   labelMap["DPhill"]="#it{#Delta}#varphi_{ll}";



   std::map<string, vector<double>> binEdgesMap;
   binEdgesMap["Mjj"]={450*GeVtoMeV, 700.0*GeVtoMeV,950.0*GeVtoMeV,1200.0*GeVtoMeV,1500.0*GeVtoMeV, 2200.0*GeVtoMeV,5000*GeVtoMeV};
   binEdgesMap["Mll"]={10*GeVtoMeV,20*GeVtoMeV,25*GeVtoMeV,30*GeVtoMeV,35*GeVtoMeV,40*GeVtoMeV,45*GeVtoMeV, 55*GeVtoMeV,200*GeVtoMeV};
   binEdgesMap["pt_H"]={0,45*GeVtoMeV,80*GeVtoMeV,120*GeVtoMeV,160*GeVtoMeV,200*GeVtoMeV,260*GeVtoMeV,1000*GeVtoMeV};
   binEdgesMap["DYll"]={0,0.2,0.4,0.6,0.8,1.0,2.5};
   binEdgesMap["DYjj"]={2.1,3.5,4.0,4.375,5,5.5,6.25,7,9.0};
   binEdgesMap["SignedDPhijj"]={-3.1416,-2.36,-1.57,-0.786,0,0.786,1.57,2.36,3.1416};
   binEdgesMap["DPhill"]={0,0.2,0.4,0.6,0.8,1.0,1.4,3.1416};
   binEdgesMap["lep0_pt"]={22*GeVtoMeV,30*GeVtoMeV,40*GeVtoMeV,50*GeVtoMeV,60*GeVtoMeV,70*GeVtoMeV,100*GeVtoMeV,125*GeVtoMeV,500*GeVtoMeV};
   binEdgesMap["lep1_pt"]={15*GeVtoMeV,22*GeVtoMeV,30*GeVtoMeV,40*GeVtoMeV,50*GeVtoMeV,60*GeVtoMeV,200*GeVtoMeV};
   binEdgesMap["jet0_pt"]={30*GeVtoMeV,60*GeVtoMeV,90*GeVtoMeV,120*GeVtoMeV,160*GeVtoMeV,220*GeVtoMeV,700*GeVtoMeV};
   binEdgesMap["jet1_pt"]={30*GeVtoMeV,45*GeVtoMeV,60*GeVtoMeV,90*GeVtoMeV,120*GeVtoMeV,350*GeVtoMeV};
   binEdgesMap["costhetastar"]={0,0.0625,0.125,0.1875,0.25,0.3125,0.375,0.5,1.0};
  
  
   // resize the distributions
   nBinsDiff[currentDistribution]=binEdgesMap[currentDistribution].size()-1; 


  if(useDedicatedDiscrDiff)
    vbfDscir+="_"+currentDistribution;
  
  // observables per region
  std::map<string, string> obsRegions;
  obsRegions["SRVBF"]=vbfDscir;
  obsRegions["CRTop"]=topWWDscir;

  //obsRegions["CRGGF3"]="MT";

  //obsRegions["CRGGF3"]="bdt_ggFCR3_CutDPhill";
  //obsRegions["CRGGF2"]="bdt_ggFCR2_CutDPhill";
  //obsRegions["CRGGF1"]="bdt_ggFCR1_CutDPhill";
  
  obsRegions["CRGGF3"]="bdt_ggFCR3_CutDPhill";
  obsRegions["CRGGF2"]="bdt_ggFCR2";
  obsRegions["CRGGF1"]="bdt_ggFCR1";
  
  //obsRegions["CRGGF3"]="bdt_ggFCR3";
  //obsRegions["CRGGF2"]="bdt_ggFCR2";
  //obsRegions["CRGGF1"]="bdt_ggFCR1";

  obsRegions["CRZjets"]="MT";
  obsRegions["CRWW"]="bdt_TopWW";
  //obsRegions["SRGGF"]="bdt_vbfggf";
  
  
  if(isDiff){
    for(int i=0; i<nBinsDiff[currentDistribution]; i++){
      obsRegions[Form("SRVBF_%d",i)]=vbfDscir;
      obsRegions[Form("CRTop_%d",i)]=topWWDscir;
    }
  }   
    
  string xsecVar="sigma_bin";
  if(isBBB)
    xsecVar="mu_vbf_bin_"; 
  

   // Fit a MG dataset
  vector <double> xSecOther;
  vector <double> xSecOtherErr;

  TFile *falt = TFile::Open(mg_ws.c_str());
  //TFile *falt=TFile::Open("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/October2019_withSys/LauraHF/results/GuyLatestWS_12_04_2021/stat/ws_Mjj_MG.root");
  //TFile *falt=TFile::Open("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/October2019_withSys/LauraHF/results/GuyLatestWS_12_04_2021/stat/ws_jet0_pt_MG.root");
  //TFile *falt=TFile::Open(fname.c_str());
  RooWorkspace *walt=(RooWorkspace*)falt->Get(wsname.c_str());
  ModelConfig *modelother=(ModelConfig*)walt->obj("ModelConfig");
  RooCategory *channelCat2 = (RooCategory*)walt->cat("channelCat" );
  RooSimultaneous *pdfother=(RooSimultaneous*)modelother->GetPdf();
  //ptJ0
  //vector <double> properMGPred={2.6310e-01 , 5.6893e-01 , 4.7500e-01 , 4.0247e-01 , 2.7048e-01 , 1.7889e-01};
  // mJJ
  vector <double> properMGPred={6.2224e-01, 4.6586e-01 , 3.2543e-01 ,  2.6532e-01 , 3.0872e-01 , 1.7178e-01};  
  double IntegralPrefit=0;
  for( int i=0; i<(int)properMGPred.size(); i++) IntegralPrefit+=properMGPred[i];
  
  walt->allVars().selectByName("mu_*")->setAttribAll("Constant",true);
  walt->allVars().selectByName("gamma_*")->setAttribAll("Constant",true);
  walt->allVars().selectByName("alpha_*")->setAttribAll("Constant",true);
  walt->allVars().selectByName("sigma_bin*")->setAttribAll("Constant",false);

  RooRealVar *inmu=(RooRealVar*)walt->var("mu_vbf");
  inmu->Print();
  inmu->setRange(0,5);
  inmu->setVal(1.0);
  
  double totPrefit=0;
  double totReco=0;

  for(int i=0; i<(int)nBinsDiff[currentDistribution]; i++){
    RooRealVar *xx=(RooRealVar*)walt->var(Form("%s%d",xsecVar.c_str(),i));
    if( xx != nullptr){
      double binWidth=binEdgesMap[currentDistribution][i+1]-binEdgesMap[currentDistribution][i];
      totPrefit+=xx->getVal();
      cout<<"PreFit fit value "<<xx->getVal()<<" fb "<< xx->getVal()/binWidth <<" ab / GeV "<<endl;
      cout<<" Setting value to "<< properMGPred[i]<<endl;
      RooRealVar *oneOver=(RooRealVar*)walt->var(Form("OneOverC_bin_%d",i));
      cout<<"Valule of 1/C "<<oneOver->getVal()<<" inv "<<1/oneOver->getVal()<<endl;
      RooFormulaVar *migMat = (RooFormulaVar*)walt->obj(Form("migMatProj_SRVBF_bin%d",i));
      migMat->Print();
      xx->setVal(properMGPred[i]);
      
      //oneOver->setVal(1/properMGPred[i]);
      oneOver->setRange(-10000,+10000);
      oneOver->setVal(1/migMat->evaluate());
      totReco+=migMat->evaluate();
      //oneOver->setVal(1.0);
      //oneOver->setVal(1/IntegralPrefit);
      //oneOver->setVal(IntegralPrefit/properMGPred[i]);
      //oneOver->setVal(1/properMGPred[i]);
      RooRealVar *muVbfBin=(RooRealVar*)walt->var(Form("mu_vbf_bin_%d",i));
      muVbfBin->Print(); 
    }
  }
  inmu->setVal(IntegralPrefit);
  //inmu->setVal(totReco);
  inmu->Print();
  cout<<"Total cross section "<<totPrefit<<endl;
  

  if(compareToOtherInput){
    delete data;
    data=AsymptoticCalculator::GenerateAsimovData(*pdfother,*modelother->GetObservables());
    
    pdfother->fitTo(*data,SumW2Error(kTRUE),Constrain(*modelother->GetNuisanceParameters()),GlobalObservables(*modelother->GetGlobalObservables()),Extended(true));
    // this xsec
    double totXsOther=0;
    for(int i=0; i<(int)nBinsDiff[currentDistribution]; i++){
      RooRealVar *xx=(RooRealVar*)walt->var(Form("%s%d",xsecVar.c_str(),i));
      if( xx != nullptr){
	double binWidth=binEdgesMap[currentDistribution][i+1]-binEdgesMap[currentDistribution][i];
	xSecOther.push_back(xx->getVal()/binWidth);
	totXsOther+=xx->getVal();
	cout<<"Post fit value "<<xx->getVal()<<" pm "<<xx->getError()<<" fb "<<xSecOther.back()<<" ab / GeV "<<endl;
      }
  }
    cout<<"Total cross section post fit:" <<totXsOther<<endl;
  }
  
  RooRealVar *mu_Zjets=(RooRealVar*)w->var("mu_Zjets");
  //mu_Zjets->setConstant(true);
  
  pdf->Print("V");

  //return ;
  RooArgSet allPars;
  //w->allVars().selectByName("alpha_*")->setAttribAll("Constant",true);

  //TIterator *tnpak=((RooArgSet*)w->allVars().selectByName("alpha_*"))->createIterator();
  ///RooRealVar *npelement=nullptr;
  //while(( npelement=(RooRealVar*)tnpak->Next())){
  //  npelement->setRange(-1,+1);
  //}

  
  //w->allVars().selectByName("alpha_*theo*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("alpha_*JER*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("alpha_*EG*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("alpha_*JET*")->setAttribAll("Constant",true);
  w->allVars().selectByName("alpha_*fJVT*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("mu_ggf_*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("mu_wwTop*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("mu_wwTop_bin*")->setAttribAll("Constant",false);
  /*
    poi->setConstant(true); 
    std::map<string,RooRealVar *> diffXsPois;
    for( int i=0; i<(int)nBinsDiff[currentDistribution]; i++){
    diffXsPois[Form("mu_vbf_bin_%d",i)]=(RooRealVar*)w->var(Form("mu_vbf_bin_%d",i));
    diffXsPois[Form("mu_vbf_bin_%d",i)]->setConstant(false);
    diffXsPois[Form("mu_vbf_bin_%d",i)]->setRange(-0.5,+3);
    
    
    }
    //diffXsPois[Form("mu_vbf_bin_%d",0)]->setConstant(true);
    */
  
  /*
    RooArgSet *sset=(RooArgSet*)w->allVars().selectByName("mu_wwTop*");
    TIter ssetIter_=sset->createIterator();
    RooRealVar *tmpAllPars_set=nullptr;
    while( (tmpAllPars_set=(RooRealVar*)ssetIter_.Next())){
    tmpAllPars_set->SetName("mu_wwTop");
    //w->renameSet(tmpAllPars_set->GetName(),"mu_wwTop");
    }
  */

  //if(isDiff)
  //poi->setVal(1.0); 


  /*
    const RooArgSet *sset2=model->GetNuisanceParameters();
    TIter ssetIter2_=sset2->createIterator();
    RooRealVar *tmpAllPars2_set=nullptr;
    while( (tmpAllPars2_set=(RooRealVar*)ssetIter2_.Next())){
    tmpAllPars2_set->SetName( (TString(tmpAllPars2_set->GetName()).ReplaceAll("Region1","")).Data());
    tmpAllPars2_set->SetName( (TString(tmpAllPars2_set->GetName()).ReplaceAll("Region2","")).Data());
    tmpAllPars2_set->SetName( (TString(tmpAllPars2_set->GetName()).ReplaceAll("Region3","")).Data());
    }
  
    const RooArgSet *sset3=model->GetGlobalObservables();
    TIter ssetIter3_=sset3->createIterator();
    RooRealVar *tmpAllPars3_set=nullptr;
    while( (tmpAllPars3_set=(RooRealVar*)ssetIter3_.Next())){
    tmpAllPars3_set->SetName( (TString(tmpAllPars3_set->GetName()).ReplaceAll("Region1","")).Data());
    tmpAllPars3_set->SetName( (TString(tmpAllPars3_set->GetName()).ReplaceAll("Region2","")).Data());
    tmpAllPars3_set->SetName( (TString(tmpAllPars3_set->GetName()).ReplaceAll("Region3","")).Data());
    }
  */

  
  const RooArgSet *sset4=model->GetNuisanceParameters();
  TIter ssetIter4_=sset4->createIterator();
  RooRealVar *tmpAllPars4_set=nullptr;
  while( (tmpAllPars4_set=(RooRealVar*)ssetIter4_.Next())){
    if( TString(tmpAllPars4_set->GetName()).Contains("alpha")){
      tmpAllPars4_set->setError(1.0);
      tmpAllPars4_set->setVal(0.0);
    }
    
  }
  
  
  // In case of a differential workspace, loop over all channels and add a cross section for each bin
  if(patchDiff) {
    // open file with matrices
    std::map<string, vector<vector<double>>>  rawMatrix;
    //std::map<string, vector<vector<double>>>  normMatrix;
    ifstream matrixfile(matrix_file.c_str());
    //ifstream matrixfile("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/October2019_withSys/LauraHF/matrix_projections_with_fakes_new_phase.txt");
    //ifstream matrixfile("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/October2019_withSys/LauraHF/matrix_projections_with_fakes.txt");
    ////ifstream matrixfile("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/October2019_withSys/LauraHF/matrix_projection_with_fakes.txt");
    
    string line="";
    string whichVar="";
    int binCounterx=0;
    int binCountery=0;
    std::map<string, std::pair<int,int>> counters;

    for(std::map<string,int>::iterator it=nBinsDiff.begin(); it!=nBinsDiff.end(); it++){
      vector <double> empty;
      for( int lk=0; lk<(nBinsDiff[currentDistribution]+1); lk++)
        empty.push_back(0);
      vector <vector<double>> emptyY; 
      for(int lk=0; lk<(int)nBinsDiff[currentDistribution]; lk++)
        emptyY.push_back(empty);
      rawMatrix[ (*it).first]=emptyY;
      counters[ (*it).first] = make_pair(0,0); 
    }
    
    while ( getline (matrixfile,line) ){
      if(line.size()==0) continue; 
      if(line.size()>0 && line[0]==' ') continue ; // emptyline, skip; 

      string newVar="";
      string restline;
      string binS;

      vector <string> tokens; 
      int itoken=0;
      tokens.push_back("");
      for( int ic=0;ic<(int)line.size(); ic++){
        if(line[ic]==',') { tokens.push_back(""); itoken++; continue; }
        tokens.at(itoken)+=line[ic];
        //cout<<"token "<<tokens.at(itoken)<<" "<<itoken<<endl;
      }
     
      
      newVar=tokens.at(0);
      binS=tokens.at(1);
      if(newVar.compare(currentDistribution)!=0) continue; 
      //cout<<"New var "<<newVar<<endl;
      
      
      //process the file
      binCounterx=0;
      binCountery=counters[newVar].first;

      for( int ic=2; ic<(int)tokens.size(); ic++){
        cout<<"counter "<<ic<<endl;
        cout<<"value "<<tokens.at(ic)<<endl;
        double migs=std::stod(tokens.at(ic));
        (rawMatrix[newVar])[binCountery][binCounterx]=migs;
        //(normMatrix[newVar])[binCountery][binCounterx]=migs;
        cout<< newVar<<" x,y "<< binCountery<<" - "<< binCounterx<<" "<<(rawMatrix[newVar])[binCountery][binCounterx]<<endl;
        counters[newVar].second=binCounterx;
        binCounterx++;
      }
      counters[newVar].first=counters[newVar].first+1;
    }
    matrixfile.close();

    
    // loop over all categories
    int iCat=0;
    //for (const auto nameIdx : *channelCat) {
    TIterator *iterCat = channelCat->typeIterator();
    RooCatType *tt=nullptr; 
    while ( (tt= (RooCatType*)iterCat->Next())){
      RooProdPdf *channelPdf=(RooProdPdf*)pdf->getPdf(tt->GetName() );
      //replace things in the SRVBF and CR Top
      if( TString(tt->GetName() ).Contains("SRVBF") || TString(tt->GetName()).Contains("CRTop")){
        channelPdf->Print();
        const RooArgList pdfList=channelPdf->pdfList();

        TIterator *iterPdf=pdfList.createIterator();
        RooAbsPdf *ipdf=nullptr;
        while( ( ipdf = (RooAbsPdf*)iterPdf->Next())) {
          if( TString(ipdf->GetName() ).Contains("SRVBF") || TString(ipdf->GetName()).Contains("CRTop")){
            ipdf->Print();

          }
        }
        cout<<endl;
      }
      iCat++;
    }
  
  


    // regions to apply the matrix to
    vector <pair<string,string>> matRegions;
    matRegions.push_back(make_pair("SRVBF",vbfDscir));
    matRegions.push_back(make_pair("CRTop",topWWDscir));

    vector <double> norms;
    
    
    for(int y=0; y<(int)nBinsDiff[currentDistribution]; y++){
      double norm=0;
      for(int x=0; x<(int)nBinsDiff[ currentDistribution]; x++){
	norm+=(rawMatrix[currentDistribution])[y][x];
      }
      norms.push_back(norm);
      //for(int x=0; x<(int)nBinsDiff[ (*mMat).first]; x++){
      //normMatrix[ (*mMat).first ][y][x]=1/norm * normMatrix[ (*mMat).first ][y][x];
      //}
    }
    
    
   
    TFile *fnew=new TFile(Form("ws_%s.root",currentDistribution.c_str()),"RECREATE");
    fnew->cd();
    RooWorkspace *w2=new RooWorkspace("w","w");
    //RooRealVar *dummy=new RooRealVar("dummy","dummy",2,0,100);
    //w2->import(*dummy);
    
    if(hConfusionMatrix!=nullptr) delete hConfusionMatrix;
    fnew->cd();
    hConfusionMatrix=new TH2F("hConfusionMatrix","",nBinsDiff[currentDistribution],0,nBinsDiff[currentDistribution],nBinsDiff[currentDistribution],0,nBinsDiff[currentDistribution]);
    hConfusionMatrix->SetDirectory(0);
    hConfusionMatrix->SetStats(0);
    hConfusionMatrix->GetXaxis()->SetTitle("Reco");
    hConfusionMatrix->GetYaxis()->SetTitle("True");

    RooRealVar *sigma=new RooRealVar("sigma","sigma",0);
    sigma->setConstant(true);
    
    std::map<int, RooRealVar*> sigmaMap;
    std::map<int, RooRealVar*> muMap;

    RooArgSet argElements;
    // vbf0_0_SRVBF_0_bdt_vbf_overallNorm_x_sigma_epsilon
    for( int ik=0 ; ik<(int)nBinsDiff[currentDistribution]; ik++){
      RooRealVar *mu_bin=(RooRealVar*)w->var(Form("mu_vbf_bin_%d",ik));
      mu_bin->setConstant(true);
      RooRealVar *sigma_bin=new RooRealVar(Form("sigma_bin%d",ik),Form("sigma_bin%d",ik),mu_bin->getVal(),0,20,"fb");
      sigma_bin->Print();
      w2->import(*sigma_bin);
      mu_bin->setVal(1.0);
      mu_bin->setConstant(true);
      w2->import(*mu_bin);
      sigmaMap[ik]=sigma_bin;
      muMap[ik]=mu_bin;
      argElements.add(*sigma_bin);
      sigma->setVal(sigma->getVal()+sigma_bin->getVal());
    }
    w2->import(*sigma);


    // change the ggF notmalisation 
    RooRealVar *muA=new RooRealVar("mu_ggfA","mu_ggfA",1,-5,+5);
    muA->setConstant(true);
    //w2->import(*muA);
    RooRealVar *muB=new RooRealVar("mu_ggfB","mu_ggfB",1,-5,+5);
    muB->setConstant(true);
    //w2->import(*muB);
    RooRealVar *muC=new RooRealVar("mu_ggfC","mu_ggfC",1,-5,+5);
    muC->setConstant(true);
    //w2->import(*muC);
    RooRealVar *muInc=new RooRealVar("mu_ggf","mu_ggf",1,-5,+5);
    muInc->setConstant(true);

    RooRealVar *muggFOther=new RooRealVar("mu_ggf_other","mu_ggf_other",1,-5,+5);
    muggFOther->setConstant(true);
    
    RooArgList argABCD; argABCD.add(*muA); argABCD.add(*muC); argABCD.add(*muB); 
    RooFormulaVar *muD=new RooFormulaVar("mu_ggfD","@0*@1/@2",argABCD);
    //w2->import(*muD);


    //const size_t nCats=channelCat->size(); 
    //RooCategory channelCat2=&channelCat; 
    //for (const auto  nameIdx :  channelCat) {
    //for( size_t ik=0; ik<nCats; ik++){
    //channelCat->setIndex(ik);
    //string channel=channelCat->getCurrentLabel(); 
    vector<string> componentsToManipulate={"ggf","vbf"};

    for(std::map<string, string>::iterator ik=obsRegions.begin(); ik!=obsRegions.end(); ik++){
      cout<<"Looping over channel "<<(*ik).first<<endl;
      
      string channel= (*ik).first; 
      string sampleName="";

      if( channel.compare("CRGGF1")==0){
	sampleName="ggf1";
      }
      else if (channel.compare("CRGGF2")==0){
	sampleName="ggf2";
      }
      else if (channel.compare("CRGGF3")==0){
	sampleName="ggf3";
	  }
      else {
	sampleName="ggf";
      }
    
      RooProduct *prodtmp=(RooProduct*)w->obj(Form("%s_%s_%s_overallNorm_x_sigma_epsilon",sampleName.c_str(),channel.c_str(),obsRegions[channel].c_str()));
      cout<<"Trying to load "<<Form("%s_%s_%s_overallNorm_x_sigma_epsilon",sampleName.c_str(),channel.c_str(),obsRegions[channel].c_str())<<endl;
      if(prodtmp==nullptr) continue; 
      cout<<" Old product "<<endl;
      prodtmp->Print();

      RooArgList componentstmp=prodtmp->components();

      if( channel.compare("CRGGF1")==0){
        componentstmp.add(*muA);
      }
      else if (channel.compare("CRGGF2")==0){
        componentstmp.add(*muB);
      }
      else if (channel.compare("CRGGF3")==0){
        componentstmp.add(*muC);
      }
      else if (TString(channel.c_str()).Contains("SRVBF") || TString(channel.c_str()).Contains("CRTop")) {
        componentstmp.add(*muD);
      }
      else
	componentstmp.add(*muggFOther);

      // add the inclusive muggF if not there
      if( componentstmp.find(muInc->GetName())==nullptr) 
	componentstmp.add(*muInc);
      
      //prodtmp->SetName(Form("%sold",prodtmp->GetName()));
      RooProduct *prodtmp2=new RooProduct(Form("%s_%s_%s_overallNorm_x_sigma_epsilon",sampleName.c_str(),channel.c_str(),obsRegions[channel].c_str()),Form("%s_%s_%s_overallNorm_x_sigma_epsilon",sampleName.c_str(),channel.c_str(),obsRegions[channel].c_str()),componentstmp);
      cout<<" New product "<<endl;
      prodtmp2->Print();
      
      w2->import(*prodtmp2);
    }

  // fix the vbf components outside the signal region to a fixed value
  TIterator *iterCatVO = channelCat->typeIterator();
  RooCatType *ttVO=nullptr; 
  while ( (ttVO= (RooCatType*)iterCatVO->Next())){
    RooProdPdf *channelPdf=(RooProdPdf*)pdf->getPdf(ttVO->GetName() );
    cout<<"Channel "<<ttVO->GetName()<<endl;
    if( TString(ttVO->GetName()).Contains("CRTop") || TString(ttVO->GetName()).Contains("SRVBF")) continue ;
    // find the components
    //Vgamma_CRGGF1_bdt_ggFCR1_overallSyst_x_Exp
    for( int i=0; i<(int)nBinsDiff[currentDistribution]; i++){
      string sampleName=Form("vbf0_%d",i);
      RooProduct *tmpprod=(RooProduct*)w->obj(Form("%s_%s_overallNorm_x_sigma_epsilon",sampleName.c_str(),ttVO->GetName()));
      cout<<"Requested "<<Form("%s_%s_overallNorm_x_sigma_epsilon",sampleName.c_str(),ttVO->GetName())<<endl;
      if( tmpprod==nullptr) continue ;
      tmpprod->Print();
      // loop over all components of the RooProduct and build a new one replacing the names we care about 
      RooArgList components=tmpprod->components();
      RooArgList newComponents;
      TIter iterComp=components.createIterator();
      RooRealVar *icVar=nullptr;
      while ((RooRealVar*)icVar==iterComp.Next()){
	if( TString(icVar->GetName()).Contains("mu_vbf") || TString(icVar->GetName()).Contains("OneOverC_bin_") || TString(icVar->GetName()).Contains("matrix_bin"))
	  continue ;
	else
	  newComponents.add(*icVar);
      }
      // add the correct normalisation
      RooRealVar mu_vbf_other("mu_vbf_other","mu_vbf_other",1,-5,+5);
      mu_vbf_other.setConstant(true);
      w2->import(mu_vbf_other,RecycleConflictNodes());
      newComponents.add(mu_vbf_other);
      RooProduct prodnew(tmpprod->GetName(),tmpprod->GetTitle(),newComponents);
      cout<<"New components "<<endl;
      prodnew.Print();
      w2->import(prodnew,RecycleConflictNodes());
    }
  }
  
  
    
    std::map<string,RooRealVar*> matElementV;
    std::map<string,RooRealVar*> matMigErrorsV;
    //np and globs defined on the fly
    std::map<string,RooFormulaVar*> matMigFormulaV;
    std::vector <string> newNPNames;
      
    RooArgSet normMapArgs;
    string normMapString="(";
    
    for( int ik=0 ; ik<(int)nBinsDiff[currentDistribution]; ik++){
      //intial dump:
      cout<<" Initial norm map string for bin "<<ik<<" "<<normMapString<<endl;
      

      RooRealVar *notFid=new RooRealVar(Form("notFid_bin%d_incl",ik),Form("notFid_bin%d_incl",ik),0);
      notFid->setConstant(true);
      //w2->import(*notFid);
          
      // here load the migration uncerainties
      std::map<string,vector<double>> thisBinMigUncertainties;
      if( addSigMigrationUncert) {
	TFile *fmig=TFile::Open(migrationTheoryUncertFile.c_str());
	for(std::vector<string>::iterator migV=migUncertainties.begin(); migV!=migUncertainties.end(); migV++){
	  thisBinMigUncertainties[*migV] =GetSymmMigUncertainty(fmig,*migV,currentDistribution,ik);
	}
	fmig->Close();
	delete fmig;
      }
          
      for(vector<pair<string,string>>::iterator it=matRegions.begin(); it!=matRegions.end(); it++){
	//intial dump:
	cout<<" Initial norm map string for bin and region "<<ik<<" "<<(*it).second<<" "<<normMapString<<endl;
	
	// Perform the decorrelation of the topWW paramerters
	cout<<" --> "<<Form("top_%s_%d_%s_overallNorm_x_sigma_epsilon",(*it).first.c_str(),ik,(*it).second.c_str())<<endl;
	RooProduct *prodTop=(RooProduct*)w->obj(Form("top_%s_%d_%s_overallNorm_x_sigma_epsilon",(*it).first.c_str(),ik,(*it).second.c_str()));
	RooRealVar *muTopWWTMP=new RooRealVar(Form("mu_wwTop_bin%d",ik),Form("mu_wwTop_bin%d",ik),1,-5,+5);

	if( prodTop != nullptr) {
	  prodTop->Print();
	  RooArgList componentsTop=prodTop->components();
	  muTopWWTMP->setConstant(true);
	  componentsTop.add(*muTopWWTMP);
	  w2->import(*muTopWWTMP);
	  //prodTop->SetName(Form("%sold",prodTop->GetName()));
	  RooProduct *prodTop2=new RooProduct(Form("top_%s_%d_%s_overallNorm_x_sigma_epsilon",(*it).first.c_str(),ik,(*it).second.c_str()),Form("top_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()),componentsTop);
	  w2->import(*prodTop2,RecycleConflictNodes());
	}
            
	// WW part 
	// Perform the decorrelation of the topWW paramerters
	RooProduct *prodWW=(RooProduct*)w->obj(Form("diboson_%s_%d_%s_overallNorm_x_sigma_epsilon",(*it).first.c_str(),ik,(*it).second.c_str()));
	if( prodWW != nullptr ) {
	  prodWW->Print();
	  RooArgList componentsWW=prodWW->components();
	  componentsWW.add(*muTopWWTMP);
	  //prodWW->SetName(Form("%sold",prodWW->GetName()));
	  RooProduct *prodWW2=new RooProduct(Form("diboson_%s_%d_%s_overallNorm_x_sigma_epsilon",(*it).first.c_str(),ik,(*it).second.c_str()),Form("diboson_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()),componentsWW);
	  w2->import(*prodWW2,RecycleConflictNodes());
	}

	/*
	// ggF ABCD 
	cout<<"----> "<<Form("ggf_%s_%d_%s_overallNorm_x_sigma_epsilon",(*it).first.c_str(),ik,(*it).second.c_str())<<endl;
	RooProduct *prodggF=(RooProduct*)w->obj(Form("ggf_%s_%d_%s_overallNorm_x_sigma_epsilon",(*it).first.c_str(),ik,(*it).second.c_str()));
	if( prodggF != nullptr){
	prodggF->Print();
	RooArgList compggF=prodggF->components();
	compggF.add(*muD);
	prodggF->SetName(Form("%sold",prodggF->GetName()));
	RooProduct *pordggF2=new RooProduct(Form("ggf_%s_%d_%s_overallNorm_x_sigma_epsilon",(*it).first.c_str(),ik,(*it).second.c_str()),Form("ggf_%s_%d_%s_overallNorm_x_sigma_epsilon",(*it).first.c_str(),ik,(*it).second.c_str()),compggF);
	pordggF2->Print();
	w2->import(*pordggF2);
	}
	else {
	cout<<"Not present "<<endl;
	}*/
            
            
	//Signal and matrix part 
	cout<<"Getting "<<Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str())<<endl;
	RooProduct *prod=(RooProduct*)w->obj(Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()));
	if(prod == nullptr ){
	  w2->allVars().selectByName(Form("*sigma_bin%d*",ik))->setAttribAll("Constant",true);
	  w2->allVars().selectByName(Form("*sigma_bin%d*",ik))->setAttribAll("Constant",true);
	  continue ;
	}
	prod->Print();
	RooArgList components=prod->components();

	string matElement="(";

	RooArgSet thisArgElements;
	thisArgElements.add(argElements);
	    
	string uncertFormula="";
	RooArgSet uncertFormulaArgs;
	    
	    
	for( int iy=0 ; iy<(int)nBinsDiff[currentDistribution]; iy++){
	    
	  if(matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)]!=nullptr) delete matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)];

	  matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)]=new RooRealVar(Form("eff_bin%d_inclbin%d_incl",ik,iy),Form("eff_bin%d_inclbin%d_incl",ik,iy),1/norms.at(iy)*(rawMatrix[currentDistribution])[ik][iy],1/norms.at(iy)*(rawMatrix[currentDistribution])[ik][iy],1/norms.at(iy)*(rawMatrix[currentDistribution])[ik][iy]);

	  thisArgElements.add(*matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)]);

	  matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)]->setConstant(true);
	  cout<<"Mat element V"<<endl;
	  matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)]->Print();
	  w2->import(*matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)],RecycleConflictNodes());

	  // Construct here the migration uncertainties component
	  uncertFormula="(1";
	  if( addSigMigrationUncert){
	    for(std::vector<string>::iterator migV=migUncertainties.begin(); migV!=migUncertainties.end(); migV++){
	      // protection against bad stuff 
	      if( thisBinMigUncertainties[*migV].at(iy) <=0) continue;

	      if(matMigErrorsV[Form("migUncert_bin%d_inclbin%d_incl_%s",ik,iy,(*migV).c_str())]!=nullptr) delete matMigErrorsV[Form("migUncert_bin%d_inclbin%d_incl_%s",ik,iy,(*migV).c_str())];
	      
	      matMigErrorsV[Form("migUncert_bin%d_inclbin%d_incl_%s",ik,iy,(*migV).c_str())] = new RooRealVar(Form("migUncert_bin%d_inclbin%d_incl_%s",ik,iy,(*migV).c_str()),
													      Form("migUncert_bin%d_inclbin%d_incl_%s",ik,iy,(*migV).c_str()),
													      thisBinMigUncertainties[*migV].at(iy),thisBinMigUncertainties[*migV].at(iy),thisBinMigUncertainties[*migV].at(iy));
	      matMigErrorsV[Form("migUncert_bin%d_inclbin%d_incl_%s",ik,iy,(*migV).c_str())]->setConstant(true);
	      w2->import(*matMigErrorsV[Form("migUncert_bin%d_inclbin%d_incl_%s",ik,iy,(*migV).c_str())],RecycleConflictNodes());
		
	      string npName="alpha_"+baseNameNP+(*migV);
	      string globName="nom_alpha_"+baseNameNP+(*migV);
	      RooRealVar *np=(RooRealVar*)w->var(npName.c_str());
	      RooRealVar *glob=(RooRealVar*)w->var(globName.c_str());

	      // if it does not exist create a new one 
	      if ( np==nullptr) {
		np = new RooRealVar(npName.c_str(),npName.c_str(),0,-5,+5);
		np->setConstant(false);
		//if( w2->var(npName.c_str())==nullptr)
		//w2->import(*np,RecycleConflictNodes());
		newNPNames.push_back(baseNameNP+(*migV));
	      }
	      if( glob==nullptr){
		glob = new RooRealVar(globName.c_str(),globName.c_str(),0,-5,+5);
		glob->setConstant(true);
		//if( w2->var(globName.c_str())==nullptr)
		//w2->import(*glob,RecycleConflictNodes());
	      }
	      
		
	      uncertFormula+=Form("+%s*%s",matMigErrorsV[Form("migUncert_bin%d_inclbin%d_incl_%s",ik,iy,(*migV).c_str())]->GetName(),npName.c_str());
	      uncertFormulaArgs.add(*matMigErrorsV[Form("migUncert_bin%d_inclbin%d_incl_%s",ik,iy,(*migV).c_str())]);
	      uncertFormulaArgs.add(*np);

	      //thisArgElements.add(*matMigErrorsV[Form("migUncert_bin%d_inclbin%d_incl_%s",ik,iy,(*migV).c_str())]);
	      //thisArgElements.add(*np);
		
	    }}
	  uncertFormula+=" )";
	  if(matMigFormulaV[Form("migUncertSum_bin%d_inclbin%d_incl_%s",ik,iy,"sum")]!=nullptr) delete matMigFormulaV[Form("migUncertSum_bin%d_inclbin%d_incl_%s",ik,iy,"sum")];
	  
	  matMigFormulaV[Form("migUncertSum_bin%d_inclbin%d_incl_%s",ik,iy,"sum")]=new RooFormulaVar(Form("migUncertSum_bin%d_inclbin%d_incl_%s",ik,iy,"sum"),uncertFormula.c_str(),uncertFormulaArgs);
	  thisArgElements.add(*matMigFormulaV[Form("migUncertSum_bin%d_inclbin%d_incl_%s",ik,iy,"sum")]);
	  cout<<"Migration uncertainty is "<<endl;
	  matMigFormulaV[Form("migUncertSum_bin%d_inclbin%d_incl_%s",ik,iy,"sum")]->Print();
	  w2->import(* matMigFormulaV[Form("migUncertSum_bin%d_inclbin%d_incl_%s",ik,iy,"sum")],RecycleConflictNodes());
	    
	    
	  //matElement+=Form("%lf*%s+",1/norms.at(iy)*(rawMatrix[currentDistribution])[ik][iy],sigmaMap[iy]->GetName());
	  matElement+=Form("%s*%s*%s+",matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)]->GetName(),matMigFormulaV[Form("migUncertSum_bin%d_inclbin%d_incl_%s",ik,iy,"sum")]->GetName(),sigmaMap[iy]->GetName());
	  //matElement+=Form("%s*%s+",matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)]->GetName(),sigmaMap[iy]->GetName());
	  
	  hConfusionMatrix->SetBinContent(ik+1,iy+1,1/norms.at(iy)*(rawMatrix[currentDistribution])[ik][iy]);
	  hConfusionMatrix->GetXaxis()->SetBinLabel(ik+1,Form("sigma_bin%d",ik));
	  hConfusionMatrix->GetYaxis()->SetBinLabel(ik+1,Form("sigma_bin%d",ik));
	}
	matElement.pop_back();
          

	// add here the fakes
	  
	notFid->setVal(rawMatrix[currentDistribution][ik].back() *1/norms.at(ik)*sigmaMap[ik]->getVal());
	cout<<"Value "<<notFid->getVal()<<" raw  "<<rawMatrix[currentDistribution][ik].back()<< " norm "<<1/norms.at(ik)<<" sigma "<<sigmaMap[ik]->getVal()<<endl;
	if(w2->var(notFid->GetName())==nullptr)
	  w2->import(*notFid,RecycleConflictNodes());
	RooArgSet uncertFormulaArgsFakes;
	uncertFormula="(1 ";
	if(addSigMigrationUncert){
	  // Construct here the migration uncertainties component
	  for(std::vector<string>::iterator migV=migUncertainties.begin(); migV!=migUncertainties.end(); migV++){
	    // protection against bad stuff 
	    if( thisBinMigUncertainties[*migV].back() <=0 || thisBinMigUncertainties[*migV].size()==0) continue; 

	    if( matMigErrorsV[Form("migIn_bin%d_inclbin_incl_%s",ik,(*migV).c_str())]!=nullptr) delete matMigErrorsV[Form("migIn_bin%d_inclbin_incl_%s",ik,(*migV).c_str())];
	    
	    matMigErrorsV[Form("migIn_bin%d_inclbin_incl_%s",ik,(*migV).c_str())] = new RooRealVar(Form("migIn_bin%d_inclbin_incl_%s",ik,(*migV).c_str()),
												      Form("migIn_bin%d_inclbin_incl_%s",ik,(*migV).c_str()),
												      thisBinMigUncertainties[*migV].back(),thisBinMigUncertainties[*migV].back(),thisBinMigUncertainties[*migV].back());
	      
	    matMigErrorsV[Form("migIn_bin%d_inclbin_incl_%s",ik,(*migV).c_str())]->setConstant(true);
	    w2->import(*matMigErrorsV[Form("migIn_bin%d_inclbin_incl_%s",ik,(*migV).c_str())],RecycleConflictNodes());
	      
	    string npName="alpha_"+baseNameNP+(*migV);
	    string globName="nom_alpha_"+baseNameNP+(*migV);
	    RooRealVar *np=(RooRealVar*)w->var(npName.c_str());
	    RooRealVar *glob=(RooRealVar*)w->var(globName.c_str());
	    // if it does not exist create a new one 
	    if ( np==nullptr || glob==nullptr) {
	      np = new RooRealVar(npName.c_str(),npName.c_str(),0,-5,+5);
	      np->setConstant(false);
	      //if( w2->var(npName.c_str())==nullptr)
	      //w2->import(*np,RecycleConflictNodes());
	      
	      glob = new RooRealVar(globName.c_str(),globName.c_str(),0,-5,+5);
	      glob->setConstant(true);
	      //if( w2->var(globName.c_str())==nullptr)
		//w2->import(*glob,RecycleConflictNodes());
	      newNPNames.push_back(baseNameNP+(*migV));
	    }
	      
	    uncertFormula+=Form("+%s*%s",matMigErrorsV[Form("migIn_bin%d_inclbin_incl_%s",ik,(*migV).c_str())]->GetName(),npName.c_str());
	    uncertFormulaArgsFakes.add(*matMigErrorsV[Form("migIn_bin%d_inclbin_incl_%s",ik,(*migV).c_str())]);
	    uncertFormulaArgsFakes.add(*np);
	    //thisArgElements.add(*matMigErrorsV[Form("migIn_bin%d_inclbin_incl_%s",ik,(*migV).c_str())]);
	    //thisArgElements.add(*np);
	  }}
	  
	uncertFormula+=")";

	if(matMigFormulaV[Form("migUncertInSum_bin%d_inclbin_incl_%s",ik,"sum")]!=nullptr) delete matMigFormulaV[Form("migUncertInSum_bin%d_inclbin_incl_%s",ik,"sum")];
	matMigFormulaV[Form("migUncertInSum_bin%d_inclbin_incl_%s",ik,"sum")]=new RooFormulaVar(Form("migUncertInSum_bin%d_inclbin_incl_%s",ik,"sum"),uncertFormula.c_str(),uncertFormulaArgsFakes);
	matMigFormulaV[Form("migUncertInSum_bin%d_inclbin_incl_%s",ik,"sum")]->Print();
	thisArgElements.add(*matMigFormulaV[Form("migUncertInSum_bin%d_inclbin_incl_%s",ik,"sum")]);
	w2->import(*matMigFormulaV[Form("migUncertInSum_bin%d_inclbin_incl_%s",ik,"sum")],RecycleConflictNodes());
	  
	  
	matElement+=Form("+%s*%s",notFid->GetName(),matMigFormulaV[Form("migUncertInSum_bin%d_inclbin_incl_%s",ik,"sum")]->GetName());
	thisArgElements.add(*notFid);
	thisArgElements.add(*matMigFormulaV[Form("migUncertInSum_bin%d_inclbin_incl_%s",ik,"sum")]);
	matElement+=")";

	cout<<" Mat element string "<<matElement<<endl;
	
	RooFormulaVar *migMat=new RooFormulaVar(Form("migMatProj_%s_bin%d", (*it).first.c_str(),ik),matElement.c_str(),thisArgElements);
	cout<<"----------"<<endl;
	migMat->Print();
	//migMat->Print("V");
	cout<<"----------"<<endl;
	  
	w2->import(*migMat,RecycleConflictNodes());

	
	migMat->Print();
	//migMat->Print("V");
	RooRealVar *oneOver=(RooRealVar*)w->var(Form("OneOverC_bin_%d",ik));
	oneOver->setRange(-1e4,1e4);
	oneOver->setVal(1/migMat->evaluate());
	// Would not work 
	//RooFormulaVar *oneOver =new RooFormulaVar(Form("OneOverC_bin_%d",ik),"1/@0",RooArgSet(*migMat));
	w2->import(*oneOver,RecycleConflictNodes());

	cout<<"BBB cross section "<<sigmaMap[ik]->getVal()<<" formula "<<migMat->getVal()<<endl;

	//Attention Lumi set manually here 
	RooRealVar *LumiSig=new RooRealVar("LumiSig","LumiSig",139,139,139);
	LumiSig->setConstant(true);
	if( (RooRealVar*)w2->var("LumiSig")==nullptr)
	  w2->import(*LumiSig,RecycleConflictNodes());
	  
	normMapString+=Form("%s*%s+",LumiSig->GetName(),migMat->GetName());
	normMapArgs.add(*LumiSig);
	normMapArgs.add(*migMat);
          
	components.add(*migMat);
	//components.add(*LumiSig);
         
	RooProduct *prod2=new RooProduct(Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()),
					 Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()),
					 components);
          
	prod2->Print();
	w2->import(*prod2,RecycleConflictNodes());
	string sreplace="prod::"+string(Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()))+"(";
	string sreplace2; 
	TIterator *ttb=components.createIterator();
	RooAbsArg *element=nullptr;
	while(( element=(RooAbsArg*)ttb->Next())){
	  sreplace+=element->GetName();
	  sreplace+=",";
	  sreplace2+=element->GetName();
	  sreplace2+=",";
	}
	sreplace.pop_back();
	sreplace+=")";
	sreplace2.pop_back(); 
	cout<<" s replace "<<sreplace<<endl;
	cout<<" Norm map string temp:"<<normMapString<<endl;
      }
	  
      normMapString.pop_back();
      // add here the Fakes
      //notFid->setVal(rawMatrix[currentDistribution][ik].back()*1/norms.at(ik)*sigmaMap[ik]->getVal());
      //cout<<"Value "<<notFid->getVal()<<" input "<<rawMatrix[currentDistribution][ik].back()*1/norms.at(ik)*sigmaMap[ik]->getVal()<<endl;
      //w2->import(*notFid); 
      //normMapString+=Form("+139*%s",notFid->GetName());
      //normMapArgs.add(*notFid); 
      normMapString+=")";

      cout<<" Norm map string "<<normMapString<<endl;
      RooFormulaVar *normMapV= new RooFormulaVar(Form("norm_bin%d_incl",ik),normMapString.c_str(),normMapArgs);
      cout<<"Normalisation of given bin is "<<endl;
      normMapV->Print();
      cout<<endl;
      w2->import(*normMapV);
      //delete notFid;  
      normMapString="(";
    }
    cout<<"Bye "<<endl;
    //return ;
    if( freeTopPerBin){
      w2->allVars().selectByName("mu_wwTop")->setAttribAll("Constant",true);
      w2->allVars().selectByName("mu_wwTop_bin*")->setAttribAll("Constant",false);
      RooArgList mu_ww_vars;
      for(int g=0;g<nBinsDiff[currentDistribution];g++){
	int name=g/freeTopParams;
	w2->var(Form("mu_wwTop_bin%d",g))->SetName(Form("mu_wwTop_bin%d",name));
      }
    }else{
      w2->allVars().selectByName("mu_wwTop")->setAttribAll("Constant",false);
      w2->allVars().selectByName("mu_wwTop_bin*")->setAttribAll("Constant",true);
    }


    RooArgSet newNPsArg;
    RooArgSet newGlobsArg;
    RooCategory *newChannelCat=new RooCategory("channelCat","channelCat"); 
    RooSimultaneous *simPDF2=new RooSimultaneous(pdf->GetName(),"",*newChannelCat);
    
    std::sort(newNPNames.begin(),newNPNames.end());
    newNPNames.erase(std::unique(newNPNames.begin(),newNPNames.end()),newNPNames.end());
    // for all the Nuisance paramters for which there were no constraints in add a new one
    // lopp again to add the relavant constraints;
    int iCat2=0;
    TIterator *iterCatT = channelCat->typeIterator();
    RooCatType *ttT=nullptr; 
    while ( (ttT= (RooCatType*)iterCatT->Next())){
      newChannelCat->defineType(ttT->GetName(),iCat2);

      RooProdPdf *channelPdf=(RooProdPdf*)pdf->getPdf(ttT->GetName() );
      cout<<"Channel pdf " <<channelPdf->GetName()<<endl;
      const RooArgList pdfList=channelPdf->pdfList();
      RooArgSet newPdfList;

    TIterator *iterPdfNewTheory=pdfList.createIterator();
    // this is the RooProdPdf of the channel 
    bool found=false;
    RooAbsPdf *ipdfNewTheory=nullptr;
       while( ( ipdfNewTheory = (RooAbsPdf*)iterPdfNewTheory->Next())) {
	 newPdfList.add(*ipdfNewTheory);
	 if( !found && TString(ipdfNewTheory->GetName()).Contains("Constraint")){
	   found=true; 
	   for(std::vector<string>::iterator it2=newNPNames.begin(); it2!=newNPNames.end(); it2++){

	     // Check if Gaussian is alredy in set 
	     RooAbsArg *parg2=newPdfList.find(Form("alpha_%sConstraint",(*it2).c_str()));
	     if( parg2!=nullptr && newPdfList.contains(*parg2)) continue; 
	     
	     RooCustomizer(*ipdfNewTheory,"");
	     string npName="alpha_"+(*it2);
	     string globName="nom_alpha_"+(*it2);
	     RooRealVar *np=new RooRealVar(npName.c_str(),npName.c_str(),0,-5,5);
	     np->setConstant(false);
	     w2->import(*np,RecycleConflictNodes());
	     RooRealVar *glob=new RooRealVar(globName.c_str(),globName.c_str(),0,-5,5);
	     glob->setConstant(true);
	     w2->import(*glob,RecycleConflictNodes());
	     RooGaussian *gauss=new RooGaussian(Form("alpha_%sConstraint",(*it2).c_str()),Form("alpha_%sConstraint",(*it2).c_str()),*np,*glob,RooConst(1));
	     cout<<"Adding Gaussian "<<endl;
	     gauss->Print();
	     w2->import(*gauss,RecycleConflictNodes());
	     newPdfList.add(*gauss);
	     newNPsArg.add(*np);
	     newGlobsArg.add(*glob);
	   }
	   
	 }
       }
       RooProdPdf *newProdPdf=new RooProdPdf(channelPdf->GetName(),channelPdf->GetTitle(),newPdfList);
       cout<<"Final channel pdf"<<endl;
       newProdPdf->Print();
       w2->import(*newProdPdf,RecycleConflictNodes());
       simPDF2->addPdf(*newProdPdf,ttT->GetName());
       iCat2++;
    }
    
    w2->import(*newChannelCat);
    
    simPDF2->Print();
    
    
    string pdfName=pdf->GetName();
    //RooSimultaneous* simPDF2=(RooSimultaneous*)pdf->Clone(pdfName.c_str());
    w2->import(*simPDF2,RecycleConflictNodes());
    //w2->import(*simPDF2,RecycleConflictNodes());
    w2->import(*data,RecycleConflictNodes());
    w2->merge(*w);

    ModelConfig *model2=new ModelConfig(model->GetName(),w2);
    //(ModelConfig*)model->Clone("ModelConfig");
    model2->SetWorkspace(*w2);
    model2->SetPdf(*simPDF2);
    
    model2->SetGlobalObservables(*global);
    model2->SetObservables(*model->GetObservables());

    if(model->GetParametersOfInterest()!=nullptr)
      model2->SetParametersOfInterest(argElements);
    
    if(model->GetConstraintParameters()!=nullptr)
      model2->SetConstraintParameters(*model->GetConstraintParameters());

    if(model->GetConditionalObservables()!=nullptr)
      model2->SetConditionalObservables(*model->GetConditionalObservables());
    // determine the new nuisanace paramters
   
    TIter npList=(model->GetNuisanceParameters())->createIterator();
    RooRealVar *nnV=nullptr;
    while (( nnV=(RooRealVar*)npList.Next())){
      newNPsArg.add(*nnV);
    }
    model2->SetNuisanceParameters(newNPsArg);
    
    TIter npList2=(model->GetGlobalObservables())->createIterator();
    nnV=nullptr;
    while (( nnV=(RooRealVar*)npList2.Next())){
      newGlobsArg.add(*nnV);
    }
    model2->SetGlobalObservables(newGlobsArg);


    //if(model->GetNuisanceParameters()!=nullptr)
    //model2->SetNuisanceParameters(*model->GetNuisanceParameters());
    //model2->SetPdf(pdfName.c_str());
    
    
    
    
    model2->Print("V");
    model2->SetName(model->GetName());
    model2->SetWorkspace(*w2);
    w2->import(*model2);
    RooAbsData *dataAsym=AsymptoticCalculator::GenerateAsimovData(*simPDF2,*model2->GetObservables());
    dataAsym->SetName("asimovData");
    w2->import(*dataAsym);
    model2->SetWorkspace(*w2);
    


    //std::list< TObject * > lobj=w->allGenericObjects();
    //    for(std::list<TObject*>::iterator it=lobj.begin(); it!=lobj.end(); it++)
    //w2->import( *(*it));
    //w->saveSnapshot("prefit",allPars);
    //w2->SetName("w");
    w2->writeToFile(Form("ws_%s.root",currentDistribution.c_str()),true);
    //model2->Write();
    //cout<<" pdf is "<<model2->GetPdf()->GetName()<<endl;

        
    fnew->Close();
    TFile *fnew2=new TFile(Form("matrixHistos_%s",currentDistribution.c_str()),"RECREATE");
    
    hConfusionMatrix->SetDirectory(fnew2);
    hConfusionMatrix->Write();
    fnew2->Close();
    
    //delete w2;
    return ; 
  }

  if(decorrNPs || removeTopCR || removeggFRegion3 || removeggFRegion2 || commonmuAB){
    
    string addedundm="";
    if(decorrNPs)
      addedundm="decorr";
    if( removeTopCR)
      addedundm="noTopReg";
    if (removeggFRegion3)
      addedundm="noGGF3"; 
    if( removeggFRegion2)
      addedundm="noGGF2";
    if( commonmuAB)
      addedundm="commonmuAB";
    TFile *fnew=new TFile(Form("ws_%s_%s.root",addedundm.c_str(),currentDistribution.c_str()),"RECREATE");
    fnew->cd();
    
    // what to decorrelate
    //samples
    std::vector<string> Rsamples;
    if( decorrNPs){
      Rsamples.push_back("all");
      //Rsamples.push_back("top");
      //Rsamples.push_back("diboson");
    }
    vector<string> npPatterns;
    if(decorrNPs){
      //npPatterns.push_back("JET_JER");
      // npPatterns.push_back("_theo_");
      //npPatterns.push_back("theo_vbf_generator");
      //List of Nps constrained more than 0.8 after 4 iterations is 
      //RooArgSet:: = (alpha_FT_EFF_Eigen_B_0,alpha_JET_Flavor_Composition,alpha_MUON_SCALE,alpha_theo_ttbar_fsr,alpha_theo_ttbar_matching,alpha_theo_ztautau_generator)
      //npPatterns={"alpha_FT_EFF_Eigen_B_0","alpha_JET_Flavor_Composition","alpha_MUON_SCALE","alpha_theo_ttbar_fsr","alpha_theo_ttbar_matching","alpha_theo_ztautau_generator"};
      //npPatterns={"alpha_JET_JER_EffectiveNP_1", "alpha_JET_JER_EffectiveNP_4", "alpha_JET_Pileup_OffsetMu", "alpha_JET_JER_EffectiveNP_9", "alpha_theo_ztautau_generator", "alpha_JET_EffectiveNP_Modelling1", "alpha_JET_JER_EffectiveNP_3", "alpha_JET_Pileup_RhoTopology", "alpha_theo_ttbar_fsr", "alpha_JET_JER_EffectiveNP_2", "alpha_JET_JER_EffectiveNP_6", "alpha_JET_EtaIntercalibration_Modelling", "alpha_theo_ww_alphas", "alpha_theo_ww_pdf", "alpha_theo_ztautau_alphas", "alpha_theo_ztautau_pdf", "alpha_theo_ztautau_scale", "alpha_FT_EFF_Eigen_B_0", "alpha_MUON_SCALE", "alpha_theo_ttbar_isr", "alpha_theo_ttbar_pdf", "alpha_theo_ttbar_scale", "alpha_FT_EFF_Eigen_B_1" };

      // theory only:

      npPatterns={"alpha_theo_ztautau_generator", "alpha_theo_ttbar_fsr", "alpha_theo_ww_alphas", "alpha_theo_ww_scale","alpha_theo_ww_pdf", "alpha_theo_ztautau_alphas", "alpha_theo_ztautau_pdf", "alpha_theo_ztautau_scale", "alpha_theo_ttbar_isr", "alpha_theo_ttbar_matching", "alpha_theo_ttbar_pdf", "alpha_theo_ttbar_shower","alpha_theo_vbf_shower", "alpha_theo_vbf_scale", "alpha_theo_vbf_pdf4lhc","alpha_theo_vbf_generator","alpha_theo_vbf_alphas","alpha_theo_ggF_qcd_wg1_mu","alpha_theo_ggF_qcd_wg1_res","alpha_theo_ggF_qcd_wg1_mig01","alpha_theo_ggF_qcd_wg1_mig12","alpha_theo_ggF_qcd_wg1_vbf2j","alpha_theo_ggF_qcd_wg1_vbf3j","alphatheo_ggF_qcd_wg1_qm_t","alpha_theo_ggF_qcd_wg1_pTH","alpha_theo_ggF_pdf4lhc","alpha_theo_ggF_alphas","alpha_theo_ggF_shower"};

    }

    //vector <string> regPatterns;
    //regPatterns.push_back("PartRegions"); //  one NP for each for the CR and a split between CRTop and SRVBF 

    int multSplit=(int) nBinsDiff[currentDistribution]/2;
    if( nBinsDiff[currentDistribution] > 6)
      multSplit=(int)nBinsDiff[currentDistribution]/2;
    if (  nBinsDiff[currentDistribution] > 9)
      multSplit=(int)nBinsDiff[currentDistribution]/3; 
    
    std::map<string, vector<string>> regPatterns;
    int multSplitCounter=0; 
    for( int i=0; i<nBinsDiff[currentDistribution];i++){
      
      //if(decorrNPs){
      //// inclusive division 
      //regPatterns[Form("SRVBF_%d_%s",i,obsRegions[Form("SRVBF_%d",i)].c_str())]={"RegVBF"};
      //regPatterns[Form("CRTop_%d_%s",i,obsRegions[Form("CRTop_%d",i)].c_str())]={"RegTop"};
      //}
      
      // divide by multiples of 3
      if( i % multSplit == 0 ) {
	multSplitCounter++;
      }

      if(decorrNPs){
	regPatterns[Form("SRVBF_%d_%s",i,obsRegions[Form("SRVBF_%d",i)].c_str())]={Form("RegVBF_%d",multSplitCounter)};
	regPatterns[Form("CRTop_%d_%s",i,obsRegions[Form("CRTop_%d",i)].c_str())]={Form("RegTop_%d",multSplitCounter)};
      }
      else{
	regPatterns[Form("SRVBF_%d_%s",i,obsRegions[Form("SRVBF_%d",i)].c_str())]={""};
	regPatterns[Form("CRTop_%d_%s",i,obsRegions[Form("CRTop_%d",i)].c_str())]={""};
      }
    }
    
    if(decorrNPs){
      regPatterns["CRGGF1_"+obsRegions["CRGGF1"]]={"RegGGFA"};
      regPatterns["CRGGF2_"+obsRegions["CRGGF2"]]={"RegGGFB"};
      regPatterns["CRGGF3_"+obsRegions["CRGGF3"]]={"RegGGFC"};
      regPatterns["CRZjets_"+obsRegions["CRZjets"]]={"RegZjets"};
    }
    //else {
    //regPatterns["CRGGF1_"+obsRegions["CRGGF1"]]={""};
    //regPatterns["CRGGF2_"+obsRegions["CRGGF2"]]={""};
    //regPatterns["CRGGF3_"+obsRegions["CRGGF3"]]={""};
    //regPatterns["CRZjets_"+obsRegions["CRZjets"]]={""};
    // }
    
    
    RooWorkspace *wtest=new RooWorkspace("w","w");
    RooArgSet newPdfsSim;
    RooCategory *newChannelCat=new RooCategory("channelCat","channelCat"); 
    RooSimultaneous *simPDF3=new RooSimultaneous(pdf->GetName(),"",*newChannelCat);
    RooArgSet newNps;
    RooArgSet newGlobs;
    vector <RooProduct*> newProducts;
    std::map<string,RooFormulaVar*> newImpacts;
    //map of modified constraints
    std::map<string,vector<string>> existingConstrains;
    std::map<string,vector<string>> removedConstraints;
    std::map<string,vector<string>> newConstraints; 
    std::map<string,vector <string>> channelConstraints;
    std::map<string,std::map<string,vector<string>>> modifiedConstraints; 

    // perform a decorrelation of uncertainties
    int iCat2=0;
    //for (const auto nameIdx : *channelCat) {
    TIterator *iterCat2 = channelCat->typeIterator();
    RooCatType *tt2=nullptr;


    if(removeggFRegion2){
      RooRealVar *muA=new RooRealVar("mu_ggfA","mu_ggfA",1,-5,+5);
      muA->setConstant(true);
      RooRealVar *muC=new RooRealVar("mu_ggfC","mu_ggfC",1,-5,+5);
      muC->setConstant(true);
      //w2->import(*muC);
      RooArgSet argABCD; argABCD.add(*muA); argABCD.add(*muC);
      RooRealVar *muB=(RooRealVar*)w->var("mu_ggfB");
      muB->SetName("muA");
      wtest->import(*muB);

      RooFormulaVar *muD=new RooFormulaVar("mu_ggfD","@mu_ggfA/mu_ggfC",argABCD);
      wtest->import(*muD,RecycleConflictNodes());
      
    }
    while ( (tt2= (RooCatType*)iterCat2->Next())){
      RooProdPdf *channelPdf=(RooProdPdf*)pdf->getPdf(tt2->GetName() );
      cout<<"Channel pdf " <<endl;
      channelPdf->Print();
      

      if(removeTopCR && TString(tt2->GetName()).Contains("CRTop")) continue; 
      if (removeggFRegion3 && TString(tt2->GetName()).Contains("CRGGF3")) continue ;
      if( removeggFRegion2 && TString(tt2->GetName()).Contains("CRGGF2")) continue;

      newChannelCat->defineType(tt2->GetName(),iCat2);
      channelPdf->Print();
      const RooArgList pdfList=channelPdf->pdfList();
      RooArgSet newPdfList;

      // map of changed parameters 
      std::map<string,vector<string>> changedParameters; 

      TIterator *iterPdf=pdfList.createIterator();
      // this is the RooProdPdf of the channel 
      RooAbsPdf *ipdf=nullptr;
      cout<<"components of channel"<<endl;
      while( ( ipdf = (RooAbsPdf*)iterPdf->Next())) {
	cout<<"THis channel PDF MODEL"<<endl;
	ipdf->Print();

	
	
	
	if( TString(ipdf->GetName()).Contains("Constraint") || TString(ipdf->GetName()).Contains("constraint")){
	  existingConstrains[tt2->GetName()].push_back(ipdf->GetName());
	  //continue ;
	  if( TString(ipdf->GetName()).Contains("lumiConstraint") || TString(ipdf->GetName()).Contains("gamma")){
	    if(TString(ipdf->GetName()).Contains("gamma"))
	      channelConstraints[tt2->GetName()].push_back((TString(ipdf->GetName()).ReplaceAll("constraint","")).Data());
	    else
	      channelConstraints[tt2->GetName()].push_back((TString(ipdf->GetName()).ReplaceAll("Constraint","")).Data());
	  }
	  //wtest->import(*ipdf,RecycleConflictNodes());
	  //newPdfList.add(*ipdf);
	  continue; 
	}
	



	if( TString(ipdf->GetName()).Contains("model")){

	  
	  // this is a roolrealSumpdf sum of all samples 
	  RooArgList ilist=((RooRealSumPdf*) ipdf)->funcList();
	  TIter ilistIter=ilist.createIterator();
	  RooAbsArg *ilistarg=nullptr;

	 
	  
	  while (( ilistarg = (RooAbsArg*) ilistIter.Next())){

	    if( TString(ilistarg->GetName()).Contains("binWidth")) {
	      //wtest->import(*ilistarg,RecycleConflictNodes());
	      continue;
	    }

	    RooProduct *prodcomp=(RooProduct*)ilistarg;
	    prodcomp->Print(); 

	    RooArgSet customisedLeafs;
	    RooArgSet allLeafs;
	    RooCustomizer cust(*prodcomp,"");
	    
	    RooArgSet inArgs;
	    RooArgSet outArg;

	    
	    
	    RooRealVar *parmitem=nullptr;
	    TIter paramListIter=w->allVars().selectByName("alpha_*")->createIterator();
	    
	    while (( parmitem=(RooRealVar*)paramListIter.Next())){
	      if( !TString(parmitem->GetName()).Contains("alpha_")) continue ;

	      if( !prodcomp->dependsOn(*parmitem)) { cout<<"DOES NOT DEPEND on "<<parmitem->GetName()<<endl; continue ; } 
	      
	      //parmitem->Print();
	      inArgs.add(*parmitem); 
	      string newName="";
	      // perform the selection
		    
		  // split by sample
		  for(vector<string>::iterator vs=Rsamples.begin(); vs!=Rsamples.end(); vs++){
		    if( TString(prodcomp->GetName()).Contains( (*vs).c_str()) || (*vs).compare("all")==0 ){
		      //split by NP
		      for(vector<string>::iterator vn=npPatterns.begin(); vn!=npPatterns.end(); vn++){
			bool consider=TString(parmitem->GetName()).Contains( (*vn).c_str());
			if(exactDecorrNPPatterns) consider= (*vn).compare(parmitem->GetName())==0 ? true:false; 

			if(  consider || (*vn).compare("all")==0){
			  if( (*vs).compare("all")!=0)
			      newName=(*vs);
			  // split by region 
			  if( regPatterns[tt2->GetName()].size() > 0) 
			    newName+=regPatterns[tt2->GetName()].at(0);
			}
		      }}
		  }
		  
		  //if(newName.size()==0){
		  //channelConstraints[tt2->GetName()].push_back(parmitem->GetName());
		  //continue;
		  //}
		  
		  RooRealVar *newVarAlpha=new RooRealVar(Form("%s%s",parmitem->GetName(),newName.c_str()),"",0,-5,5);
		  newNps.add(*newVarAlpha);
		  outArg.add(*newVarAlpha);
		  changedParameters[parmitem->GetName()].push_back( newVarAlpha->GetName());
		  channelConstraints[tt2->GetName()].push_back(newVarAlpha->GetName());
		  wtest->import(*newVarAlpha,RecycleConflictNodes());
		  // Global observable
		  RooRealVar *globOld=(RooRealVar*)w->var(Form("nom_%s",parmitem->GetName()));
		  RooRealVar *globNew=new RooRealVar(Form("%s%s",globOld->GetName(),newName.c_str()),"",0,-5,5);
		  globNew->setConstant(true);
		  newGlobs.add(*globNew);
		  wtest->import(*globNew,RecycleConflictNodes());
		  cust.replaceArg(*parmitem,*newVarAlpha);
		  cust.replaceArg(*globOld,*globNew);

		  modifiedConstraints[tt2->GetName()][Form("%sConstraint",parmitem->GetName())].push_back(newVarAlpha->GetName());
		}


		auto outpdf=cust.build(kTRUE);
		cout<<"New customized compoent "<<endl;
		outpdf->Print();
		wtest->import(*outpdf,RecycleConflictNodes());
		cout<<"After replacement "<<endl;
		outpdf->Print();
	  }
	  //wtest->import(*ipdf,RecycleConflictNodes());
	  newPdfList.add( *ipdf);
	}
      }

      cout<<"New list of pdfs "<<endl;
      newPdfList.Print();
      
      //cout<<"OK "<<endl;
      // here loop on new constraints
      //TIterator *iterPdfDeterm=pdfList.createIterator();
      //RosoAbsPdf *ipdfNew=nullptr;
      //while( ( ipdfNew = (RooAbsPdf*)iterPdfDeterm->Next())) {
      //if( !TString(ipdfNew->GetName()).Contains("Constraint"))
      //  continue ;
      //existingConstrains[tt2->GetName()].push_back( ipdfNew->GetName()); 
      //}
      
      // lopp again to add the relavant constraints;
      TIterator *iterPdfNew=pdfList.createIterator();
      // this is the RooProdPdf of the channel 
      RooAbsPdf *ipdfNew=nullptr;

      while( ( ipdfNew = (RooAbsPdf*)iterPdfNew->Next())) {

	// add the gamma constraints and continue 
	if( TString(ipdfNew->GetName()).Contains("gamma") && TString(ipdfNew->GetName()).Contains("constraint")){
	  wtest->import(*ipdfNew,RecycleConflictNodes());
	  newPdfList.add(*ipdfNew);
	  continue ;
	}
	
	if( !TString(ipdfNew->GetName()).Contains("Constraint"))
	  continue;
	


	// this is not needed right ? 
	//newPdfList.add(*ipdfNew);
	//for(std::map<string,vector<string>>::iterator it=changedParameters.begin(); it!=changedParameters.end(); it++){
	//for(std::map<string,vector<string>>::iterator it=channelConstraints.begin(); it!=channelConstraints.end(); it++){

	  cout<<"Looping over stuff"<<endl;
	  vector<string> outNames=channelConstraints[tt2->GetName()]; //(*it).second;
	  std::sort(outNames.begin(),outNames.end());
	  outNames.erase(std::unique(outNames.begin(),outNames.end()),outNames.end());
	  
	  for(std::vector<string>::iterator it2=outNames.begin(); it2!=outNames.end(); it2++){
	    cout<<"Treating NP "<<(*it2)<<endl;
	    string tmpName=(*it2)+"Constraint";
	    //if( tmpName.compare(ipdfNew->GetName())!=0) continue;
	    if( (*it2).compare("lumi")==0){
	      RooGaussian *dd=(RooGaussian*)w->obj("lumiConstraint");
	      newPdfList.add(*dd);
	      continue; 
	    }
	    if( TString((*it2).c_str()).Contains("gamma")){
	      RooGaussian *dd=(RooGaussian*)w->obj(Form("%sconstraint",(*it2).c_str()));
	      cout<<"Adding gamma constraint "<<endl;
	      dd->Print();
	      wtest->import(*dd,RecycleConflictNodes());
	      newPdfList.add(*dd);
	      continue ;
	    }
	    
	    RooAbsArg *isThere =newPdfList.find(tmpName.c_str());
	    if(isThere!=nullptr && newPdfList.contains(*isThere))
	      continue; 
	    
	    RooRealVar *globOld=(RooRealVar*)w->var(Form("nom_%s",(*it2).c_str()));
	    if( globOld ==nullptr)
	      globOld=(RooRealVar*)wtest->var(Form("nom_%s",(*it2).c_str()));
	    if( globOld == nullptr)
	      globOld= new RooRealVar(Form("nom_%s",(*it2).c_str()),"",0,-5,5);
	    RooRealVar *npold=(RooRealVar*)w->var((*it2).c_str());
	    if( npold ==nullptr)
	      npold=(RooRealVar*)wtest->var((*it2).c_str());
	    if( npold ==nullptr)
	      npold=new RooRealVar((*it2).c_str() ,"",0,-5,5);
	    
	    RooGaussian *gauss=(RooGaussian*)w->obj(Form("%sConstraint",(*it2).c_str()));
	    if( gauss == nullptr) 
	      gauss=new RooGaussian(Form("%sConstraint",(*it2).c_str()),Form("%sConstraint",(*it2).c_str()),*npold,*globOld,RooConst(1));
	    cout<<"Adding Gaussian "<<endl;
	    gauss->Print();
	    wtest->import(*gauss,RecycleConflictNodes());
	    newPdfList.add(*gauss);
	    
	    //npold->setConstant(true);
	    //wtest->import(*npold,RecycleConflictNodes());
	  }
	  //cout<<"End loop"<<endl;
	  //}
      }
      cout<<"Here"<<endl;
      cout<<"Old list of pdfs with constraints"<<endl;
      pdfList.Print();
      cout<<"New list of PDFS with constraints" <<endl;
      newPdfList.Print();
      
    

      //RooProdPdf *newProdPdf=custConstr.build(kTRUE);
      RooProdPdf *newProdPdf=new RooProdPdf(channelPdf->GetName(),channelPdf->GetTitle(),newPdfList);
      wtest->import(*newProdPdf,RecycleConflictNodes());
      cout<<"New RooProdPdf "<<endl;
      newProdPdf->Print();
      newPdfsSim.add(*newProdPdf);

      
      if(removeTopCR || removeggFRegion3){
	if( removeTopCR) {
	  if(!TString(tt2->GetName()).Contains("CRTop_")){
	    wtest->import(*newProdPdf,RecycleConflictNodes());
	    simPDF3->addPdf(*newProdPdf,tt2->GetName());
	  }
	}
	if (removeggFRegion3) {
	  if(!TString(tt2->GetName()).Contains("CRGGF3_")){
	    wtest->import(*newProdPdf,RecycleConflictNodes());
	    simPDF3->addPdf(*newProdPdf,tt2->GetName());
	  }
	}
      }
	
      else{
	wtest->import(*newProdPdf,RecycleConflictNodes());
	simPDF3->addPdf(*newProdPdf,tt2->GetName());
      }
      cout<<endl;
      iCat2++;
    }
    wtest->import(*newChannelCat,RecycleConflictNodes());
    
     
    string pdfName=pdf->GetName();
    
    //RooSimultaneous* simPDF3=(RooSimultaneous*)pdf->Clone(pdfName.c_str());
    wtest->import(*simPDF3,RecycleConflictNodes());
    wtest->import(*data,RecycleConflictNodes());
    wtest->merge(*w);
     
    ModelConfig *model3=new ModelConfig(model->GetName(),wtest);
    model3->SetWorkspace(*wtest);
    model3->SetPdf(*simPDF3);
    
    RooArgSet *newObs=(RooArgSet*)wtest->allVars().selectByName("nom_*");
     
    model3->SetGlobalObservables(*newObs);
    RooArgSet newObsAdd;
    TIterator *itNewObs=model->GetObservables()->createIterator();
    RooRealVar *newVarObs=nullptr;
    while( (newVarObs=(RooRealVar*)itNewObs->Next())) {
      if(removeTopCR && TString(newVarObs->GetName()).Contains(topWWDscir.c_str())) continue;
      else if ( removeggFRegion3 && TString(newVarObs->GetName()).Contains("bdt_ggFCR3")) continue; 
      else
	newObsAdd.add( *newVarObs);
    }
     
    model3->SetObservables(newObsAdd);

    if(model->GetParametersOfInterest()!=nullptr)
      model3->SetParametersOfInterest(*model->GetParametersOfInterest());
    if(model->GetConstraintParameters()!=nullptr)
      model3->SetConstraintParameters(*model->GetConstraintParameters());

    newGlobs.add( *(RooArgSet*)wtest->allVars().selectByName("nom_*gamma*"));
    newNps.add( *(RooArgSet*)wtest->allVars().selectByName("gamma*"));

    model3->SetGlobalObservables(newGlobs);
    model3->SetNuisanceParameters(newNps);

    model3->Print("V");
    model3->SetName(model->GetName());
    model3->SetWorkspace(*wtest);
    wtest->import(*model3);
    RooAbsData *dataAsym3=AsymptoticCalculator::GenerateAsimovData(*simPDF3,*model3->GetObservables());
    dataAsym3->SetName("asimovData");
    wtest->import(*dataAsym3);
    model3->SetWorkspace(*wtest);
    //string addedundm="";
    //if(decorrNPs)
    // addedundm="decorr";
    //if( removeTopCR)
    //addedundm="noTopReg";
    wtest->writeToFile(Form("ws_%s_%s.root",addedundm.c_str(),currentDistribution.c_str()),true);

    cout<<"Wrote "<<Form("ws_%s_%s.root",addedundm.c_str(),currentDistribution.c_str())<<endl;

    return;
  }


  

  if (addSeparateImpactParameterForNorm){
    TFile *refFile=TFile::Open("data/hist_v2_StefBinning_v21_r21_28_09_2020_v1_noBDTCut_SRL_ggFCRZeroOneJet_ABCD_TopCRinSR_decorr1JetSys_Mjj.root");
    TFile *fnew=new TFile(Form("%s_SepNP_%s.root",f->GetName(),currentDistribution.c_str()),"RECREATE");
    fnew->cd();
    RooWorkspace *w3=new RooWorkspace("w","w");
    // Loop over all Categories
    vector <RooProduct*> newProducts;
    std::map<string,RooFormulaVar*> newImpacts;

    //RooArgSet newPdfsSim;
    // RooCategory *newChannelCat=new RooCategory("channelCat","channelCat"); 
    //RooSimultaneous *simPdf2N=new RooSimultaneous(pdf->GetName(),"",*newChannelCat);
    
    RooArgSet newNps;
    RooArgSet newGlobs;
    /*
    // perform a decorrelation of uncertainties
    std::map<string, vector<pair<string,double>> varMaps; 
    // loop over all the keys of the file
    TKey *keyP=nullptr;
    TIter nextP(refFile->GetListOfKeys());
    while ((keyP=(TKey*)nextP())) {
      if( !TString(keyp->GetName()).Contains("saved_integral")) continue ;
      TH1F *hvar=(TH1F*)refFile->Get(keyP);
      //hvbf0_5_SRVBF_5_bdt_vbf_Mjj_MUON_EFF_TTVA_STAT_saved_integral 
      
    }
    */
    
    RooAbsCollection *mset3=w->allFunctions().selectByName("L_x_*_*_*_*_overallSyst_x_StatUncert");
    TIterator *msetIter=mset3->createIterator();
    RooProduct *mprod=nullptr;
    RooArgSet newGlobsArg;
    RooArgSet newNPsArg;
    std::map<string,vector<string>> addedNPs;
    std::vector <string> addedNPsAll;
    while( ( mprod=(RooProduct*)msetIter->Next())){
      // skip the vbf normalisation part, as it is handled by the matrix; 
      //if( (TString(mprod->GetName()).Contains("SRVBF") || TString(mprod->GetName()).Contains("CRTop")) && TString(mprod->GetName()).Contains("vbf0_")) continue ;
      mprod->Print();
      TString name=TString(mprod->GetName());
      //name.ReplaceAll("_overallNorm_x_sigma_epsilon","");
      name.ReplaceAll("L_x_","");
      name.ReplaceAll("_overallSyst_x_StatUncert","");
      cout<<name.Data()<<endl;
      RooArgList mcomps=mprod->components();
      // Loop over all the systematics available in the file 
      // loop over all the keys of the file
      TKey *keyP=nullptr;
      TIter nextP(refFile->GetListOfKeys());
      std::map<string, double> normSys;
      string namePattern="h"+string(name.Data())+"_";
      RooArgSet normCoeffsVar;
      string normUncertForm="(1";
      RooArgSet formParam;
      while ((keyP=(TKey*)nextP())) {
	if( !TString(keyP->GetName()).Contains(namePattern.c_str())) continue;
	// Get the variation name;
	TString var=keyP->GetName();
	// remove also some spurious stuff:
	if( TString(var).Contains("hZjets01")) continue; 
	string tname="h"+string(name.Data())+"_";
	//cout<<"Tname "<<tname<<endl;
	var.ReplaceAll("_saved_integral","");
	var.ReplaceAll(tname.c_str(),"");
	cout<<"VAR:" <<var.Data()<<" name pattern "<<namePattern<<endl;
	//if( !var.Contains("theo")) continue; 
	//if( !var.Contains("theo_ggF_qcd_wg1_vbf3j")) continue;
	// cleaning
	if(var.Contains("1theo_") || var.Contains("2theo_") || var.Contains("3theo_")) continue; 
	/// skip the vbf normalisation part, as it is handled by the matrix; 
	if( (TString(mprod->GetName()).Contains("SRVBF") || TString(mprod->GetName()).Contains("CRTop")) && TString(mprod->GetName()).Contains("vbf0_") && var.Contains("theo_")) continue; 

	

	//if( !var.Contains("theo_ttbar_fsr") ) continue; 
	bool isOneSided=false; 

	TH1F *hvar=(TH1F*)refFile->Get(keyP->GetName());
	double sys=0.5*fabs(hvar->GetBinContent(5)-hvar->GetBinContent(6))/hvar->GetBinContent(4);
	if( (hvar->GetBinContent(5) - hvar->GetBinContent(4)) > 0 && (hvar->GetBinContent(6) - hvar->GetBinContent(4)) < 0 )
	  sys=sys;
	else if (  (hvar->GetBinContent(5) - hvar->GetBinContent(4)) < 0 && (hvar->GetBinContent(6) - hvar->GetBinContent(4)) > 0 )
	  sys=-1.0*sys;
	else if ( (hvar->GetBinContent(5) - hvar->GetBinContent(4)) > 0 && (hvar->GetBinContent(6) - hvar->GetBinContent(4)) > 0 )
	  cout<<" One sided variation !!!"<<endl;
	  
	if( sys <=0) continue ;
	normSys[var.Data()]=sys;
	
	RooRealVar *impact=new RooRealVar(Form("normSysImp_%s_%sF",namePattern.c_str(),var.Data()),Form("normSysImp_%s_%sF",namePattern.c_str(),var.Data()),sys);
	impact->setConstant(true);
	w3->import(*impact,RecycleConflictNodes());
	string suffix="";
	if(addSeparateImpactParameterForNormAndDecorr)
	  suffix="totNorm";
	if( addSeparateImpactParameterForNormAndDecorrPattern){
	  for (std::vector<string>::iterator ik=shapeAndNormDecorrPattern.begin(); ik!=shapeAndNormDecorrPattern.end(); ik++){
	    if( var.Contains((*ik).c_str())){
	      suffix="totNorm";
	    }
	  }}
	string npName="alpha_"+string(var.Data())+suffix;
	string globName="nom_alpha_"+string(var.Data())+suffix;
	RooRealVar *np=new RooRealVar (npName.c_str(),npName.c_str(),0,-5,+5);
	np->Print();
	np->setConstant(false); 
	RooRealVar *glob=new RooRealVar(globName.c_str(),globName.c_str(),0,-5,+5);
	glob->setConstant(true); 
	w3->import(*np,RecycleConflictNodes());
	w3->import(*glob,RecycleConflictNodes()); 
	//RooProduct *npTimesImpac=new RooProduct(Form("norm_np_x_impact_%s_%s",namePattern.c_str(),var.Data()) ,Form("norm_np_x_impact_%s_%s",namePattern.c_str(),var.Data()),RooArgSet(*np,*impact));
	//w3->import(*npTimesImpac); 
	//normCoeffsVar.add(*npTimesImpac);
	
	formParam.add(*impact);
	formParam.add(*np); 
	if (isOneSided)
	  normUncertForm+=Form("+%s*TMath::Abs(%s)",impact->GetName(),npName.c_str());
	else 
	  normUncertForm+=Form("+%s*%s",impact->GetName(),npName.c_str());
	
	addedNPsAll.push_back(string(var.Data())+suffix);
	addedNPs[mprod->GetName()].push_back(string(var.Data())+suffix);
	cout<<"hname "<<hvar->GetName()<<" systematic "<<sys*100<<" %"<<endl;
	delete hvar;
      }
      if( addCorrectLumiUncert) {
	bool toAdd=false;
	for(std::map<string,vector<string>>::iterator ilk=lumiMap.begin(); ilk!=lumiMap.end(); ilk++){
	  if( name.Contains((*ilk).first.c_str())) {
	    vector <string> thisRegSamples=(*ilk).second;
	    for(std::vector<string>::iterator ilC=thisRegSamples.begin(); ilC!=thisRegSamples.end(); ilC++){
	      if( name.Contains( (*ilC).c_str())) {
		toAdd=true ; break;
	      }
	    }
	  }
	}

	RooRealVar *npL=nullptr;
	RooRealVar *impactL=nullptr;
	
	if(toAdd){
	  TString var="atlas_lumi";
	  impactL=new RooRealVar(Form("normSysImp_%s_%sF",namePattern.c_str(),var.Data()),Form("normSysImp_%s_%sF",namePattern.c_str(),var.Data()),lumiErr);
	  impactL->setConstant(true);
	  w3->import(*impactL);
	  string suffix="";
	  string npName="alpha_"+string(var.Data())+suffix;
	  string globName="nom_alpha_"+string(var.Data())+suffix;

	  npL=new RooRealVar (npName.c_str(),npName.c_str(),0,-5,+5);
	  npL->Print();
	  npL->setConstant(false); 
	  w3->import(*npL,RecycleConflictNodes());

	  RooRealVar *globL=new RooRealVar(globName.c_str(),globName.c_str(),0,-5,+5);
	  globL->setConstant(true); 
	  w3->import(*globL,RecycleConflictNodes());
	  
	  formParam.add(*impactL);
	  formParam.add(*npL); 
	    
      	  
	  normUncertForm+=Form("+%s*%s",impactL->GetName(),npL->GetName());
	  
	  addedNPsAll.push_back(string(var.Data()));
	  addedNPs[mprod->GetName()].push_back(string(var.Data()));
	  
	  // in case old workpapce constaints a lumi NP, fix it;
	  RooRealVar *oldLumi=(RooRealVar*)w->obj("Lumi");
	  if(oldLumi!=nullptr) {
	    oldLumi->setConstant(true);
	    w3->import(*oldLumi,RecycleConflictNodes());
	  }
	  
	  // in case old workpapce constaints a lumi NP, fix it;
	  RooRealVar *GoldLumi=(RooRealVar*)w->obj("nominalLumi");
	  if(GoldLumi!=nullptr) {
	    GoldLumi->setConstant(true);
	    w3->import(*GoldLumi,RecycleConflictNodes());
	  }
	}
      }

      
      normUncertForm+=")";
      
      RooFormulaVar *ProdofSys=new RooFormulaVar(Form("prodOfNormSys_%sF",namePattern.c_str()),normUncertForm.c_str(),formParam);
      ProdofSys->Print();
      w3->import(*ProdofSys,RecycleConflictNodes());
      cout<<"Old product "<<endl;
      mprod->Print();
      RooArgSet finalProd;
      finalProd.add(mprod->components());
      finalProd.add(*ProdofSys);
      string mprodName=mprod->GetName();
      //if(mprod!=nullptr) delete mprod;
      RooProduct *newProd=new RooProduct(mprodName.c_str(),"",finalProd);
      //mprod=newProd;
      
      newProducts.push_back(newProd);
      newImpacts[newProd->GetName()]=ProdofSys;
      cout<<"New prod"<<endl;
      newProd->Print();
      w3->import(*newProd,RecycleConflictNodes());
      cout<<"New prod"<<endl;
      newProd->Print();
      cout<<"done with this component"<<endl;
    }
    RooArgSet newPdfsSim;
    RooCategory *newChannelCat=new RooCategory("channelCat","channelCat"); 
    RooSimultaneous *simPdf2N=new RooSimultaneous(pdf->GetName(),"",*newChannelCat);
    
    //addedNPsAll
    std::sort(addedNPsAll.begin(),addedNPsAll.end());
    addedNPsAll.erase(std::unique(addedNPsAll.begin(),addedNPsAll.end()),addedNPsAll.end());
    int iCat2=0;
    //for (const auto nameIdx : *channelCat) {
      TIterator *iterCat2 = channelCat->typeIterator();
      RooCatType *tt2=nullptr; 
      while ( (tt2= (RooCatType*)iterCat2->Next())){
	RooProdPdf *channelPdf=(RooProdPdf*)pdf->getPdf(tt2->GetName() );
	cout<<"Channel pdf " <<endl;
	channelPdf->Print();
	newChannelCat->defineType(tt2->GetName(),iCat2);
	const RooArgList pdfList=channelPdf->pdfList();

	RooArgSet newPdfList;
	
	// this is the RooProdPdf of the channel 
	RooAbsPdf *ipdf=nullptr;
	vector <string> npListChannel;

	// buggy and useless ?

	
	// remove duplicates 
	std::sort(npListChannel.begin(),npListChannel.end());
	npListChannel.erase(std::unique(npListChannel.begin(),npListChannel.end()),npListChannel.end());
	
	cout<<"components of channel"<<endl;
	vector <string> constrainedNps;
	vector <string> unconstrainedNpsl;
	TIterator *iterPdf=pdfList.createIterator();
	while( ( ipdf = (RooAbsPdf*)iterPdf->Next())) {
	  if( addCorrectLumiUncert && TString(ipdf->GetName()).Contains("lumiConstraint")) continue ;
	  ipdf->Print();
	  newPdfList.add(*ipdf);
	  if( !TString(ipdf->GetName()).Contains("Constraint")) continue; 
	  cout<<"Adding NPs: ";
	  
	  for(vector<string>::iterator ib=addedNPsAll.begin(); ib!=addedNPsAll.end(); ib++){
	    RooRealVar *np=(RooRealVar*)w->var(Form("alpha_%s",(*ib).c_str()));
	    if( np==nullptr) {
	      cout<<(*ib)<<" ";
	      unconstrainedNpsl.push_back(*ib);
	      continue;
	    }
	    RooAbsArg *parg=pdfList.find(Form("alpha_%sConstraint",(*ib).c_str()));
	    if( parg!=nullptr && pdfList.contains(*parg))
	      continue;
	    //if(channelPdf->dependsOn(*np))
	    //constrainedNps.push_back(*ib);
	    else
	      unconstrainedNpsl.push_back(*ib);
	    cout<<(*ib)<<" ";
	  }
	  cout<<endl;
	  // find out if component is in constraints
	}

	for(std::vector<string>::iterator ib=unconstrainedNpsl.begin(); ib!=unconstrainedNpsl.end(); ib++){

	  // check if NP and constraint is alredy in set
	  RooAbsArg *parg=pdfList.find(Form("alpha_%sConstraint",(*ib).c_str()));
	  if( parg!=nullptr && pdfList.contains(*parg)) {
	    cout<<"Constraint term alredy present in old set:"<<endl;
	    parg->Print();
	    w3->import(*parg,RecycleConflictNodes());
	    continue;
	  }

	  RooAbsArg *parg2=newPdfList.find(Form("alpha_%sConstraint",(*ib).c_str()));
	  if( parg2!=nullptr && newPdfList.contains(*parg2)){
	    cout<<"Constraint term alredy present in new set:"<<endl;
	    parg2->Print();
	    w3->import(*parg2,RecycleConflictNodes());
	    continue;
	  }
	  
	  string npName="alpha_"+(*ib);
	  string globName="nom_alpha_"+(*ib);
	  RooRealVar *np=new RooRealVar(npName.c_str(),npName.c_str(),0,-5,5);
	  np->setConstant(false);
	  w3->import(*np,RecycleConflictNodes());;
	  RooRealVar *glob=new RooRealVar(globName.c_str(),globName.c_str(),0,-5,5);
	  glob->setConstant(true);
	  w3->import(*glob,RecycleConflictNodes());
	  
	  RooGaussian *gauss=new RooGaussian(Form("alpha_%sConstraint",(*ib).c_str()),Form("alpha_%sConstraint",(*ib).c_str()),*np,*glob,RooConst(1));
	  cout<<"Adding Gaussian "<<endl;
	  gauss->Print();
	  w3->import(*gauss,RecycleConflictNodes());

	  newPdfList.add(*gauss);
	  newNPsArg.add(*np);
	  newGlobsArg.add(*glob);
	}
	
	
	RooProdPdf *newProdPdf=new RooProdPdf(channelPdf->GetName(),channelPdf->GetTitle(),newPdfList);
	cout<<"Final channel pdf"<<endl;
	newProdPdf->Print();
	w3->import(*newProdPdf,RecycleConflictNodes());
	simPdf2N->addPdf(*newProdPdf,tt2->GetName());
	iCat2++;
      }
      w3->import(*newChannelCat,RecycleConflictNodes());
      
      
      w3->merge(*w);
      
      ModelConfig *model2=new ModelConfig(model->GetName(),w3);
      model2->SetWorkspace(*w3);
      
      
      model2->SetPdf(*simPdf2N);
      
      model2->SetGlobalObservables(*model->GetGlobalObservables());
      model2->SetObservables(*model->GetObservables());
      
      if(model->GetParametersOfInterest()!=nullptr)
	model2->SetParametersOfInterest(*model->GetParametersOfInterest());
      
      if(model->GetConstraintParameters()!=nullptr)
	model2->SetConstraintParameters(*model->GetConstraintParameters());
      
      if(model->GetConditionalObservables()!=nullptr)
	model2->SetConditionalObservables(*model->GetConditionalObservables());
      // determine the new nuisanace paramters
      
      TIter npList=(model->GetNuisanceParameters())->createIterator();
      RooRealVar *nnV=nullptr;
      while (( nnV=(RooRealVar*)npList.Next())){
	newNPsArg.add(*nnV);
      }
      model2->SetNuisanceParameters(newNPsArg);
      
      TIter npList2=(model->GetGlobalObservables())->createIterator();
      nnV=nullptr;
      while (( nnV=(RooRealVar*)npList2.Next())){
	newGlobsArg.add(*nnV);
      }
      model2->SetGlobalObservables(newGlobsArg);

      
    //if(model->GetNuisanceParameters()!=nullptr)
    //model2->SetNuisanceParameters(*model->GetNuisanceParameters());
    //model2->SetPdf(pdfName.c_str());
      
    model2->Print("V");
    model2->SetName(model->GetName());
    model2->SetWorkspace(*w3);
    w3->import(*model2);
    RooAbsData *dataAsym=AsymptoticCalculator::GenerateAsimovData(*simPdf2N,*model2->GetObservables());
    dataAsym->SetName("asimovData");
    w3->import(*dataAsym);
    model2->SetWorkspace(*w3);
    w3->writeToFile(Form("%s_SepNP_%s.root",f->GetName(),currentDistribution.c_str()),true);
    
    
    return ;
    }

  


  

  
  string pdfName=pdf->GetName();
  //w->import(*pdf,RecycleConflictNodes());
  //delete pdf;
  //pdf->SetName(Form("%s_PreFix",pdf->GetName()));
  //w->import(*pdf);
  //pdf=(RooSimultaneous*)w->pdf(pdfName.c_str());
  
  //model->SetPdf(*pdf);
  //model->SetWS(*w);
  ///w->import(*model->GetGlobalObservables());
  //w->import(*model->GetNuisanceParameters()); 
  
  if(model->GetNuisanceParameters()!=nullptr)
    allPars.add(*model->GetNuisanceParameters());
  if(model->GetParametersOfInterest()!=nullptr)
    allPars.add(*model->GetParametersOfInterest());

  allPars.add(*(RooArgSet*)w->allVars().selectByName("mu*"));
  
  //w->saveSnapshot("prefit",allPars);
  //w->writeToFile("ws.root");

  //return ; 
  ModelConfig * bModel = (ModelConfig*) model->Clone();
  // load the correct snapshot
  bModel->SetName("BModel");      
  double origVal=poi->getVal();
  poi->setVal(0);
  bModel->SetSnapshot( *poi  );
  poi->setVal(origVal);
  


  RooArgSet minosPOi;

  std::map<string,TH1*> theoryLables;
  // theory
  vector <double> xSecTheory;
  double totXs=0;
  for(int i=0; i<(int)nBinsDiff[currentDistribution]; i++){
    RooRealVar *xx=(RooRealVar*)w->var(Form("%s%d",xsecVar.c_str(),i));
    if( xx != nullptr){
      minosPOi.add(*xx);
      double binWidth=binEdgesMap[currentDistribution][i+1]-binEdgesMap[currentDistribution][i];
      if( compareToOtherInput)
	xSecTheory.push_back(xSecOther[i]);
      else 
	xSecTheory.push_back(xx->getVal()/binWidth);
      totXs+=xx->getVal()/binWidth;
    }
  }
  
  TH1D *hvbfNLO=nullptr; 
  if(addOtherTheory){

    //TFile *ftheor1=TFile::Open(Form("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/October2019_withSys/LauraHF/MGHw7_%s_noTheo.root",currentDistribution.c_str()));
    //TFile *ftheor1=TFile::Open(Form("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/October2019_withSys/LauraHF/MGHw7_%s_noTheo.root","allobs"));
    TFile *ftheor1=TFile::Open(MGHw7_FILE.c_str());
    TH1D *hvbfMG=nullptr;
    hvbfMG=(TH1D*)ftheor1->Get(currentDistribution.c_str());
    hvbfMG->SetDirectory(0);
    hvbfMG->SetFillColor(kMagenta);
    hvbfMG->SetLineColor(kMagenta);
    hvbfMG->SetFillStyle(3140);
    hvbfMG->SetStats(0);
    hvbfMG->SetMarkerStyle(28);
    hvbfMG->SetMarkerColor(28); 
    if(normTheoryPredtoYR4)
      hvbfMG->Scale(totXs/hvbfMG->Integral());

    for(int i=1; i<(int)hvbfMG->GetNbinsX()+1; i++){
      hvbfMG->SetBinError(i,UnitsScaling[UnitsMap[currentDistribution]]*hvbfMG->GetBinError(i));
      hvbfMG->SetBinContent(i,UnitsScaling[UnitsMap[currentDistribution]]*hvbfMG->GetBinContent(i));
    }
    if(normTheoryPredtoYR4)
      theoryLables["MG_aMC@NLO  (YR4)"]=hvbfMG;
    else 
      theoryLables["MG_aMC@NLO"]=hvbfMG;
    
    ftheor1->Close(); 

    
    //TFile *ftheorO=TFile::Open(Form("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/October2019_withSys/LauraHF/VBFNLO_NLO_%s.root",currentDistribution.c_str()));
    TFile *ftheorO=TFile::Open(VBFNLO_FILE.c_str());
    TH1D *hvbfNLO=nullptr;

    if(currentDistribution.compare("DPhijj")==0)
      hvbfNLO=(TH1D*)hvbfMG->Clone("fakeVBFNLO");
    else 
      hvbfNLO=(TH1D*)ftheorO->Get(currentDistribution.c_str());
    
    hvbfNLO->SetDirectory(0);
    hvbfNLO->SetFillColor(kBlue);
    hvbfNLO->SetLineColor(kBlue);
    hvbfNLO->SetFillStyle(3144);
    if(normTheoryPredtoYR4)
      hvbfNLO->Scale(totXs/hvbfNLO->Integral());
    
    for(int i=1; i<(int)hvbfNLO->GetNbinsX()+1; i++){
      hvbfNLO->SetBinError(i,UnitsScaling[UnitsMap[currentDistribution]]*hvbfNLO->GetBinError(i));
      hvbfNLO->SetBinContent(i,UnitsScaling[UnitsMap[currentDistribution]]*hvbfNLO->GetBinContent(i));
    }
    if(normTheoryPredtoYR4)
      theoryLables["VBFNLO@NLO  (YR4)"]=hvbfNLO;
    else 
      theoryLables["VBFNLO@NLO,Fixed order"]=hvbfNLO;
    ftheorO->Close();
    
    
  }
  




  RooRealVar *tau=new RooRealVar("tau","#lambda",tauD,-10,+10);
  tau->setVal(tauD);
  tau->setConstant(true);
  RooArgList *argsFormula=new RooArgList();
  //string formulaSum="TMath::Exp(tau *(";
  string formulaSum="(tau *(";
  argsFormula->add(*tau);

  int nPois=isBBB ? 0:nBinsDiff[currentDistribution];
  vector <RooRealVar*> *poisV=new vector <RooRealVar*>();
  TIter tp=(model->GetParametersOfInterest())->createIterator();
  RooRealVar *rtmp=nullptr;
  int lp=0;
  while((rtmp=(RooRealVar*)tp.Next())){
    if( (string(rtmp->GetName())).find("sigma_bin")!=string::npos || TString(rtmp->GetName()).Contains("mu_bin")){
      poisV->push_back(rtmp);
      lp++;
    }
  }
  
  
  
  for(int k=1;k<nPois-1;k++){

    /*
      formulaSum+=Form("((%s/%.3lf-%s/%.3lf) - (%s/%.3lf-%s/%.3lf)*(%s/%.3lf-%s/%.3lf))",
      poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),
      poisV->at(k)->GetName(),poisV->at(k)->getVal(),
      poisV->at(k)->GetName(),poisV->at(k)->getVal(),
      poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal(),
      poisV->at(k)->GetName(),poisV->at(k)->getVal(),
      poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal());
    */

    /*
      formulaSum+=Form("((-%s/%.3lf + 2*%s/%.3lf - %s/%.3lf)*(-%s/%.3lf + 2*%s/%.3lf -%s/%.3lf))",
      poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),
      poisV->at(k)->GetName(),poisV->at(k)->getVal(),
      poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal(),
      poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),
      poisV->at(k)->GetName(),poisV->at(k)->getVal(),
      poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal());
      if(k<nPois-2) formulaSum+=" + ";
      argsFormula->add(*poisV->at(k+1));
      argsFormula->add(*poisV->at(k));
      argsFormula->add(*poisV->at(k-1));
      argsFormula->add(*poisV->at(k+1));
      argsFormula->add(*poisV->at(k));
      argsFormula->add(*poisV->at(k-1));
    */

    
    formulaSum+=Form("TMath::Power( (%s/%.3lf -%s/%.3lf) - (%s/%.3lf -%s/%.3lf),2) ",
                     poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal());
    
    if(k<nPois-2) formulaSum+=" + ";
    
    argsFormula->add(*poisV->at(k+1));
    argsFormula->add(*poisV->at(k));
    argsFormula->add(*poisV->at(k));
    argsFormula->add(*poisV->at(k-1));
    
  } 
  formulaSum+="))";
  
  
  
  RooFormulaVar *regSum=new RooFormulaVar("regSum","regSum",formulaSum.c_str(),*argsFormula);
  regSum->Print();
  RooGenericPdf *regPdf= new RooGenericPdf("regPdf","regPdf","@0*@1",RooArgList(*pdf,*regSum));
  cout<<"Print pdf "<<endl;
  pdf->Print("v");

 
  
  // First Stat+Sys fit

  /*
    std::vector<std::string> EFT_coeff = {"cHW","cHWtil","cHB","cHBtil","cHWB","cHWBtil","cHDD","cHbox","cHl3","cll1"};
    for(auto coeff : EFT_coeff){
    RooRealVar *var_coeff = (RooRealVar*)w->var(coeff.c_str());
    var_coeff->setConstant(true);
    }
    w->allVars().selectByName("cHW")->setAttribAll("Constant",false);
    w->allVars().selectByName("sigma_bin*")->setAttribAll("Constant",true);
  */

  

  if(fractionalABCD){
    w->allVars().selectByName("mu_ggf_*")->setAttribAll("Constant",true);
    //mu_ggf->setConstant(false);
    //mu_ggf->setRange(-5,+5);
    w->allVars().selectByName("mu_ggfA")->setAttribAll("Constant",false);
    w->allVars().selectByName("mu_ggfB")->setAttribAll("Constant",false);
    w->allVars().selectByName("mu_ggfC")->setAttribAll("Constant",false);
    
    RooRealVar *muA=(RooRealVar*)w->var("mu_ggfA");
    muA->setConstant(false);
    muA->setRange(-5,+5);
    muA->Print();
    
    RooRealVar *muB=(RooRealVar*)w->var("mu_ggfB");
    if(muB!=nullptr){
      muB->SetName("mu_ggfA");
      muB->setConstant(false);
      muB->setRange(-5,+5);
    }
    
    RooRealVar *muC=(RooRealVar*)w->var("mu_ggfC");
    muC->setConstant(false);
    muC->setRange(-5,+5);

    allPars.add(*muA);
    if(muB!=nullptr)
      allPars.add(*muB);

    allPars.add(*muC);
    //w->allVars().selectByName("mu_ggf*")->setAttribAll("Constant",true);
  }

  if( noABCD) {
    w->allVars().selectByName("mu_ggf*")->setAttribAll("Constant",true);
    RooRealVar *mu_ggfInc=(RooRealVar*)w->var("mu_ggf");
    mu_ggfInc->setConstant(false);
    mu_ggfInc->setRange(-5,+5); 
  }

  if( freeTopPerBin){
    w->allVars().selectByName("mu_wwTop")->setAttribAll("Constant",true);
    w->allVars().selectByName("mu_wwTop_bin*")->setAttribAll("Constant",false);
    //RooRealVar *topX=(RooRealVar*)w->var("mu_wwTop_bin7");
    //topX->SetName("mu_wwTop_bin6");
    allPars.add(*w->allVars().selectByName("mu_wwTop_bin*"));
  }
  
  //w->allVars().selectByName("sigma_bin*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("mu_Zjets*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("alpha*JET*")->setAttribAll("Constant",true);
  //RooRealVar *muZjets=(RooRealVar*)w->var("mu_Zjets");
  //muZjets->setVal(1.0);
  
  // FreeOrNot
  w->allVars().selectByName("mu")->setAttribAll("Constant",true);
  //w->allVars().selectByName("*mu*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("*mu_vbf_bin*")->setAttribAll("Constant",false);
  //w->allVars().selectByName("alpha_*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("alpha*JET_JER*")->setAttribAll("Constant",false);
  //w->allVars().selectByName("mu_ggf*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("gamma*")->setAttribAll("Constant",false);
  
  //w->allVars().selectByName("*sigma_bin*")->setAttribAll("Constant",false);
  //w->allVars().selectByName("*mu*bin*")->setAttribAll("Constant",false);

  /*
  w->allVars().selectByName("*gamma*")->setAttribAll("Constant",true);
  w->allVars().selectByName("alpha*")->setAttribAll("Constant",true);
  w->allVars().selectByName("alpha*theo*")->setAttribAll("Constant",true);
  w->allVars().selectByName("alpha*theo*Reg*")->setAttribAll("Constant",false);
  */

  w->allVars().selectByName("mu_wwTop2")->setAttribAll("Constant",false);
  w->allVars().selectByName("mu_wwTop3")->setAttribAll("Constant",false);
  w->allVars().selectByName("mu_wwTop4")->setAttribAll("Constant",false);
  //w->allVars().selectByName("mu_ggf*")->setAttribAll("Constant",true);
  //RooRealVar *mutMP=(RooRealVar*)w->var("mu_wwTop2");
  //mutMP->SetName("mu_wwTop3");

  //w->allVars().selectByName("*sigma*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("*mu*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("mu_vbf")->setAttribAll("Constant",false);
  
  cout<<"Prefit values"<<endl;
  w->allVars().selectByName("*sigma*")->Print();
  TIterator *tps=w->allVars().selectByName("*sigma*")->createIterator();
  RooRealVar *ntps=nullptr;
  while ((ntps=(RooRealVar*)tps->Next())){
    ntps->Print();
  }
  cout<<endl;
  

  
  if(isDiff){
    poi->setRange(0.0,5);
    //poi->setVal(1.0);
    poi->setConstant(true);
  }
  poi->Print();

// fit first to create an asinmov out of the nominal values:
/*
 RooAbsReal *mNllIn=pdf->createNLL(*data,NumCPU(3,2),
				   //RooFit::Constrain(*model->GetNuisanceParameters()), 
                                    RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                    RooFit::Offset(true), 
                                    Extended(true),
				   RooFit::Verbose(kFALSE));

 RooMinimizer *mysIn=new RooMinimizer(*mNllIn);
 mysIn->setEps(0.01);
 mysIn->optimizeConst(true);
 mysIn->setStrategy(2);
 mysIn->minimize("Minuit2");
 //mysIn->minimize("Minuit2");
 mysIn->hesse();
 w->saveSnapshot("prefitAsimov",allPars);

 delete data;
 data=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
 */
  
 if(!isBBB && !patchDiff) {
    double totReco2=0;
    for(int i=0; i<(int)1000; i++){
      RooRealVar *xx=(RooRealVar*)w->var(Form("sigma_bin%d",i));
      if( xx != nullptr){
	RooRealVar *oneOver=(RooRealVar*)w->var(Form("OneOverC_bin_%d",i));
	RooFormulaVar *migMat = (RooFormulaVar*)w->obj(Form("migMatProj_SRVBF_bin%d",i));
	
	RooRealVar *muVbfBin=(RooRealVar*)w->var(Form("mu_vbf_bin_%d",i));
	muVbfBin->setVal(1.0);
	totReco2+=migMat->evaluate();
	cout<<"Prex change"<<endl;
	oneOver->Print();
	//
	//oneOver->setVal(1/migMat->evaluate());
	//cout<<"After change"<<endl;
	//oneOver->Print();
	muVbfBin->Print();
	//oneOver->Print();
	migMat->Print();
	cout<<endl;
      }
    }
    RooRealVar *vbfGlobNorm=(RooRealVar*)w->var("mu_vbf");
    ///vbfGlobNorm->setVal(2.067);
    vbfGlobNorm->Print();
 }

 delete data;
 data=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
 
 
  // find the prediction:
  RooAbsReal *mNll= pdf->createNLL( *data,
				    NumCPU(3,2),
                                    RooFit::Constrain(*model->GetNuisanceParameters()), 
                                    RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                    RooFit::Offset(true), 
                                    Extended(true),
                                    RooFit::Verbose(kFALSE));

  RooAddition *NllMod=new RooAddition("nllSum","nullSum",RooArgList(*mNll,*regSum));

  
  RooMinimizer *msys= new RooMinimizer(doReg ? *NllMod:*mNll);;
  msys->setEps(0.01);
  msys->optimizeConst(true);
  msys->setStrategy(2);
  msys->minimize("Minuit2");
  //msys->setEps(1e-3);
  //msys->minimize("Minuit2");
  msys->hesse();

  //msys->minos(*poi);
  //msys->minos();
  msys->minos(minosPOi);
  w->saveSnapshot("postfit",allPars);
  model->SetSnapshot(*poi);
  RooFitResult *rnom=msys->save();
  double minNll = rnom->minNll();
  cout<<"MinNLL for nominal "<<minNll<<endl;
  //GUY:: systeamatics fit
  cout<<"return systematic fit\n";
  return;

  RooArgSet mlist;
  
  mlist.add(*w->allVars().selectByName("alpha_*")); 
  int iter=0;
  RooArgSet outlist;
  iter=-1;
  outlist.add(FindConstrainedParameters(w,mlist,model,pdf,data,iter,0.96));
  //RooArgSet outlist=FindConstrainedParameters(w,mlist,model,pdf,data,iter,0.8);
  //RooArgSet outlist=FindConstrainedParameters(w,mlist,model,pdf,data,iter,0.96);

  outlist.Print();
  
  vector <double> xSec;
  vector <double> xSecUp;
  vector <double> xSecDown;
  vector <double> xbins;
  for( int i=0; i<(int) nBinsDiff[currentDistribution]; i++){
    RooRealVar *xP=(RooRealVar*)w->var(Form("%s%d",xsecVar.c_str(),i));
    double binWidth=binEdgesMap[currentDistribution][i+1]-binEdgesMap[currentDistribution][i];

    xSec.push_back(UnitsScaling[UnitsMap[currentDistribution]]*xP->getVal()/binWidth);
    xSecUp.push_back(UnitsScaling[UnitsMap[currentDistribution]]*fabs(xP->getErrorHi()/binWidth));
    xSecDown.push_back(UnitsScaling[UnitsMap[currentDistribution]]*fabs(xP->getErrorLo()/binWidth));
    xbins.push_back(binEdgesMap[currentDistribution][i]+0.5*binWidth);
  }


  if(!isDiff){
    xSec.push_back(poi->getVal());
    xSecUp.push_back(poi->getErrorHi());
    xSecDown.push_back(poi->getErrorLo());
  }

  
  vector <double> normsV;
  vector <double> normsVErrs;
  vector <string> normsVNames;
  vector <double> normsVCount;
  std::map<string,double> normVFactor;
  // For plottng:
  //w->allVars().selectByName("alpha_*")->setAttribAll("Constant",true);

  // backgrounds to plot as separate components
  vector <string> bckComps={"Vgamma","Fakes","ggf","Zjets","diboson","top","vbf"};
  //{"top","diboson","Zjets","Fakes","Vgamma","ggF","vbf"};
  string prefix="";
  std::map<string,int> colorMap;
  colorMap[prefix+"Vgamma"]=kGreen+2;
  colorMap[prefix+"Zjets"]=kGreen;
  colorMap[prefix+"Zjets0"]=kGreen;
  colorMap[prefix+"Zjets1"]=kGreen;
  colorMap[prefix+"diboson"]=kYellow+2;
  colorMap[prefix+"diboson1"]=kYellow+2;
  colorMap[prefix+"diboson2"]=kYellow+2;
  colorMap[prefix+"diboson3"]=kYellow+2;
  
  colorMap[prefix+"top"]=kMagenta+2;
  colorMap[prefix+"top1"]=kMagenta+2;
  colorMap[prefix+"top2"]=kMagenta+2;
  colorMap[prefix+"top3"]=kMagenta+2;
  colorMap[prefix+"top4"]=kMagenta+2;

  colorMap[prefix+"ggf"]=kBlue+2;
  colorMap[prefix+"ggf1"]=kBlue+2;
  colorMap[prefix+"ggf2"]=kBlue+2;
  colorMap[prefix+"ggf3"]=kBlue+2;
  colorMap[prefix+"ggf4"]=kBlue+2;
  
  colorMap[prefix+"vh"]=kBlue+3;
  colorMap[prefix+"htt"]=kBlue+4;
  colorMap[prefix+"Fakes"]=kYellow+4;
    
  colorMap[prefix+"Zjets1"]=kGreen;
  colorMap[prefix+"diboson1"]=kYellow+2;

  colorMap[prefix+"vbf"]=kRed;
  colorMap[prefix+"vbf0"]=kRed;
  
  std::map<string,string> legendMap;
  legendMap[prefix+"ggf"]="ggF";
  legendMap[prefix+"ggf1"]="ggF";
  legendMap[prefix+"ggf2"]="ggF";
  legendMap[prefix+"ggf3"]="ggF";
  
  // Make a plot for each category
  std::map<string, TCanvas*> cObsFitCanvas;
  // loop over all observables
  RooArgSet *observables=(RooArgSet*)model->GetObservables();
  TIter obsIt=observables->createIterator();

  RooRealVar *tmpObs=nullptr;
  std::map<string, THStack*> stackMap;
  std::map<string, std::map<string, TH1F*>> templMap; 

  RooRealVar *npDraw=nullptr;
  TIter itNpdDr=outlist.createIterator();
 
  while( (npDraw=(RooRealVar*)itNpdDr.Next())){
    cout<<"Drawing impact of "<<npDraw->GetName()<<endl;
    npDraw->Print();
    TIter obsIt2=observables->createIterator();
    RooRealVar *tmpObs2=nullptr;
    while( (tmpObs2=(RooRealVar*)obsIt2.Next())){
      if(TString(tmpObs2->GetName()).Contains(channelCat->GetName())) continue; 
    // need to find the caterogy corresponding to this observable
    cout<<" Plotting for observable "<<tmpObs2->GetName()<<endl;
    // loop over all categories 
    int tCat=0;
    //for (const auto nameIdx : *channelCat) {
    TIterator *iterCatPlot = channelCat->typeIterator();
    RooCatType *itPlot=nullptr; 


    while ( (itPlot= (RooCatType*)iterCatPlot->Next())){
      // if the channel does not contain the observable continue
      if( !TString(tmpObs2->GetName()).Contains(itPlot->GetName())) continue; 
      // Roofit method. 
      RooPlot *xplot=tmpObs2->frame(Title(Form("Fit for %s for %s",tmpObs2->GetName(),itPlot->GetName())));

      TCanvas *cTmpCanvas= new TCanvas(Form("fitRes_%s_%s_%s",tmpObs2->GetName(),itPlot->GetName(),npDraw->GetName()),"");{     
	TCanvas *c=cTmpCanvas;
	SetCanvasDefaults(c);
	c->cd();
	
	data->plotOn(xplot,Invisible(),DataError(RooAbsData::Poisson),Cut(Form("%s==%s::%s",channelCat->GetName(),channelCat->GetName(),itPlot->GetName())));
	
	// shift the NP to +1 1 sigma and re draw:
	double fitVal=npDraw->getVal();
	npDraw->setVal(+1);
	pdf->plotOn(xplot,Name("fullpdf"),Slice(*channelCat,itPlot->GetName()),ProjWData(*channelCat,*data),LineColor(kBlue),LineWidth(2),LineStyle(1));
	npDraw->setVal(-1);
	pdf->plotOn(xplot,Name("fullpdf"),Slice(*channelCat,itPlot->GetName()),ProjWData(*channelCat,*data),LineColor(kBlue),LineWidth(2),LineStyle(2));
	npDraw->setVal(0);
	pdf->plotOn(xplot,Name("fullpdf"),Slice(*channelCat,itPlot->GetName()),ProjWData(*channelCat,*data),LineColor(kRed),LineWidth(2),LineStyle(2));
	npDraw->setVal(fitVal);
	pdf->plotOn(xplot,Name("fullpdf"),Slice(*channelCat,itPlot->GetName()),ProjWData(*channelCat,*data),LineColor(kBlack),LineWidth(2));
	
	data->plotOn(xplot,DataError(RooAbsData::Poisson),Cut(Form("%s==%s::%s",channelCat->GetName(),channelCat->GetName(),itPlot->GetName())));
	
	xplot->Draw();
      }
      cTmpCanvas->Print(Form("npEffects/%s.pdf",cTmpCanvas->GetName()),"pdf");
      delete xplot;
      delete cTmpCanvas;
      
    }
    }
  }// end loop over constrained NPs;
  return ;
  // Normal Plot;

  
  while( (tmpObs=(RooRealVar*)obsIt.Next())){
    if(TString(tmpObs->GetName()).Contains(channelCat->GetName())) continue; 
    // need to find the caterogy corresponding to this observable
    cout<<" Plotting for observable "<<tmpObs->GetName()<<endl;
    // loop over all categories 
    int tCat=0;
    //for (const auto nameIdx : *channelCat) {
    TIterator *iterCatPlot = channelCat->typeIterator();
    RooCatType *itPlot=nullptr; 
    while ( (itPlot= (RooCatType*)iterCatPlot->Next())){
      // if the channel does not contain the observable continue
      if( !TString(tmpObs->GetName()).Contains(itPlot->GetName())) continue; 
      //if( !TString(itPlot->GetName()).Contains("CRZjets")) continue;

      stackMap[itPlot->GetName()]=new THStack(Form("StackFor_%s",itPlot->GetName()),"");
      
      cObsFitCanvas[Form("%s_%s",tmpObs->GetName(),itPlot->GetName())]=new TCanvas(Form("fitRes_%s_%s",tmpObs->GetName(),itPlot->GetName()),"");{
	SetCanvasDefaults(cObsFitCanvas[Form("%s_%s",tmpObs->GetName(),itPlot->GetName())]);

	TCanvas *c=cObsFitCanvas[Form("%s_%s",tmpObs->GetName(),itPlot->GetName())];
	c->cd();


	// Roofit method. 
	RooPlot *xplot=tmpObs->frame(Title(Form("Fit for %s for %s",tmpObs->GetName(),itPlot->GetName())));

	RooPlot *xplotFake=tmpObs->frame(Title(Form("Fit for %s for %s",tmpObs->GetName(),itPlot->GetName())));
	xplotFake->SetName(Form("Frame_%s",itPlot->GetName()));
	
	data->plotOn(xplotFake,DataError(RooAbsData::Poisson),Cut(Form("%s==%s::%s",channelCat->GetName(),channelCat->GetName(),itPlot->GetName())));
	pdf->plotOn(xplotFake,Name("fullpdf"),Slice(*channelCat,itPlot->GetName()),ProjWData(*channelCat,*data),LineColor(kRed),LineWidth(2));
	
	RooDataSet *tmpData=(RooDataSet*)data->reduce(Cut(Form("%s==%s::%s",channelCat->GetName(),channelCat->GetName(),itPlot->GetName())));
	templMap[itPlot->GetName()]["data"]=(TH1F*)tmpData->createHistogram(Form("data_%s",itPlot->GetName()),*tmpObs);
	if( templMap[itPlot->GetName()]["data"]==nullptr) continue; 

	SetDef(templMap[itPlot->GetName()]["data"]);
	//templMap[itPlot->GetName()]["data"]->Draw("PE");
	//xplot->addObject( templMap[itPlot->GetName()]["data"],"PE");
	data->plotOn(xplot,Invisible(),DataError(RooAbsData::Poisson),Cut(Form("%s==%s::%s",channelCat->GetName(),channelCat->GetName(),itPlot->GetName())));
	
	pdf->plotOn(xplot,Name("fullpdf"),Slice(*channelCat,itPlot->GetName()),ProjWData(*channelCat,*data),LineColor(kRed),LineWidth(2));
	

	for(int ikp=bckComps.size()-1; ikp>=0; ikp--){
	  string plotString="";
	  for(int iki=ikp; iki>=0; iki--){
	    plotString+="*"+bckComps[iki]+"*";
	    if( iki>0) plotString+=",";
	  }
	  //plotString+="";
	  //plotString="*top*";
	  //plotString="*"+bckComps[ikp-1]+"*";
	  //if( !TString(bckComps[ikp].c_str()).Contains("Fakes")) continue; 

	  //plotString="*"+bckComps[ikp]+"*";
	  cout<<"Plot String "<<plotString<<" Component "<<bckComps[ikp]<<" color "<<colorMap[bckComps[ikp]]<<endl;
	  pdf->plotOn(xplot,Name(Form("comp_%d",ikp)),Slice(*channelCat,itPlot->GetName()),ProjWData(*channelCat,*data),Components(plotString.c_str()),VLines(),DrawOption("F"),FillColor(colorMap[bckComps[ikp]]),LineColor(kBlack));

	  

	  pdf->plotOn(xplotFake,/*Invisible(),*/Name(Form("comp_%s",bckComps[ikp].c_str())),Slice(*channelCat,itPlot->GetName()),ProjWData(*channelCat,*data),DrawOption("A"),Components(plotString.c_str()));
	  xplotFake->Draw();
	  xplotFake->Print();

	  RooCurve* thist=(RooCurve*)xplotFake->getCurve(Form("comp_%s",bckComps[ikp].c_str()));
	  if(thist==nullptr){
	    cout<<"Curve with name "<<Form("comp_%s",bckComps[ikp].c_str())<<" not found "<<endl;
	    continue;
	  }
	  thist->Print("V");
	  
	  templMap[itPlot->GetName()][bckComps[ikp]] = new TH1F(Form("Histo_%s_%s",itPlot->GetName(),bckComps[ikp].c_str()),"",templMap[itPlot->GetName()]["data"]->GetNbinsX(),templMap[itPlot->GetName()]["data"]->GetXaxis()->GetXbins()->GetArray());
	  
	  for(int ip=0; ip<thist->GetN(); ip++){
	    double x,y;
	    thist->GetPoint(ip,x,y);
	    int ibin=-1;
	    ibin = templMap[itPlot->GetName()][bckComps[ikp]]->FindBin(x);
	    templMap[itPlot->GetName()][bckComps[ikp]]->SetBinContent(ibin,y+templMap[itPlot->GetName()][bckComps[ikp]]->GetBinContent(ibin));
	    ibin++;
	    
	  }
	  
	  //templMap[itPlot->GetName()][bckComps[ikp]]=(TH1F*) thist->GetHistogram();
	  
	  cout<<"Integral of events "<<templMap[itPlot->GetName()][bckComps[ikp]]->Integral()<<endl;
	  cout<<"N bins "<<templMap[itPlot->GetName()][bckComps[ikp]]->GetNbinsX()<<endl;
	  
	  SetDef(templMap[itPlot->GetName()][bckComps[ikp]]);
	  templMap[itPlot->GetName()][bckComps[ikp]]->SetFillColor(colorMap[bckComps[ikp]]);
	  templMap[itPlot->GetName()][bckComps[ikp]]->SetLineColor(kBlack);
	  stackMap[itPlot->GetName()]->Add(templMap[itPlot->GetName()][bckComps[ikp]]);
	  
	  //xplot->addObject(stackMap[itPlot->GetName()],"hist same");
	  
	  //if(ikp==bckComps.size())
	  //pdf->plotOn(xplot,Name(Form("comp_%d",ikp)),Slice(*channelCat,itPlot->GetName()),ProjWData(*channelCat,*data),Components(plotString.c_str()),VLines(),DrawOption("F"),FillColor(colorMap[bckComps[ikp]]),LineColor(kBlack));
	  //else
	  //pdf->plotOn(xplot,Name(Form("comp_%d",ikp)),AddTo(Form("comp_%d",ikp+1)),Slice(*channelCat,itPlot->GetName()),ProjWData(*channelCat,*data),Components(plotString.c_str()),VLines(),DrawOption("F"),FillColor(colorMap[bckComps[ikp]]),LineColor(kBlack));


	  //pdf->plotOn(xplot,Name(Form("comp_%d",ikp)),ProjWData(*channelCat,*data),Components(plotString.c_str()),VLines(),DrawOption("F"),FillColor(colorMap[bckComps[ikp]]),LineColor(kBlack));
	}
	
	//pdf->plotOn(xplot,Slice(*channelCat,itPlot->GetName()),ProjWData(*channelCat,*data),Components("*vbf*"),VLines(),DrawOption("F"),FillColor(kRed),LineColor(kBlack));
	data->plotOn(xplot,DataError(RooAbsData::Poisson),Cut(Form("%s==%s::%s",channelCat->GetName(),channelCat->GetName(),itPlot->GetName())));
	if(compareToOtherInput)
	  pdfother->plotOn(xplot,Name("fullpdfOther"),Slice(*channelCat2,itPlot->GetName()),ProjWData(*channelCat2,*data),LineColor(kBlue),LineWidth(2));
	xplot->Draw();
	
	delete xplotFake; 
      }
    }
  }

 
  
  // Get the correlation matrix for floating non nuisance paramters
  RooArgSet corrPars;
  corrPars.add(minosPOi);
  RooAbsCollection *collection=w->allVars().selectByName("mu_*");
  corrPars.add(*collection);

  RooArgSet corrParsX;
  corrParsX.add(corrPars);
  
  TIterator *corrIterX=corrParsX.createIterator();
  TIterator *corrIterY=corrPars.createIterator();
  RooRealVar *corrX=nullptr;
  RooRealVar *corrY=nullptr;
  vector <vector <double>> doubleXY;
  int corrSize=0;
  while (( corrX=(RooRealVar*)corrIterX->Next())){
    if( corrX==nullptr) continue;
    if( corrX->isConstant()) continue;

   
    
    vector <double> ycors;
    while (( corrY=(RooRealVar*)corrIterY->Next())){
      if( corrY==nullptr || corrX==nullptr) continue;
      if( corrY->isConstant() || corrX->isConstant()) continue;
      ycors.push_back(rnom->correlation(*corrY,*corrX));
    }
    corrIterY->Reset();
    doubleXY.push_back(ycors);
    corrSize++;
  }
  corrIterX->Reset();
  corrIterY->Reset();
  
  TH2F *hcorr=new TH2F("hcorr","",corrSize,0,corrSize,corrSize,0,corrSize);
  int icorrx=1;
  int icorry=1;

  while (( corrX=(RooRealVar*)corrIterX->Next())){
    if( corrX==nullptr) continue;
    if( corrX->isConstant()) continue;
    icorry=1;

    while (( corrY=(RooRealVar*)corrIterY->Next())){
      if( corrY==nullptr || corrX==nullptr) continue;
      if( corrY->isConstant() || corrX->isConstant()) continue;

      hcorr->SetBinContent(icorrx,icorry,rnom->correlation(*corrY,*corrX));

      hcorr->GetYaxis()->SetBinLabel(icorry,corrY->GetName());
      hcorr->GetXaxis()->SetBinLabel(icorrx,corrX->GetName());
      icorry++;
    }
    corrIterY->Reset();
    icorrx++;
  }
  hcorr->SetStats(0);
  hcorr->GetZaxis()->SetRangeUser(-1,+1);
  hcorr->GetYaxis()->SetTitleOffset(1.2);
  hcorr->GetYaxis()->SetTitleOffset(0.9);
  
  gStyle->SetPaintTextFormat("4.2f");
  string regType="matrix";
  if(doReg) regType+="_reg";
  TCanvas *cCorr=new TCanvas( Form("cCorr_%s_%s", string(isBBB ? "bbb" : regType).c_str(), currentDistribution.c_str()),"");{
    cCorr->cd();
    hcorr->Draw("colz text");
  }
  cCorr->Print(Form("%s.eps",cCorr->GetName()),"eps");
  
  // Correlation of all parameters with more than 20% correlation
  vector <vector <double>> corrAllX;
  vector <string> corrAllName; 
  
  
  
  TIter allParsIterSysX=allPars.createIterator();
  TIter allParsIterSysY=allPars.createIterator();
  RooRealVar *allParsCorrtmpX=nullptr;
  RooRealVar *allParsCorrtmpY=nullptr;
  while (( allParsCorrtmpY = (RooRealVar*)allParsIterSysY.Next())){
    if( allParsCorrtmpY==nullptr) continue;
    if (allParsCorrtmpY->isConstant()) continue; 

    if( !TString( allParsCorrtmpY->GetName()).Contains("vbf") && !allParsCorrtmpY->isConstant() && !TString( allParsCorrtmpY->GetName()).Contains("sigma")
        && !TString( allParsCorrtmpY->GetName()).Contains("alpha") && !TString( allParsCorrtmpY->GetName()).Contains("matrix") && !TString( allParsCorrtmpY->GetName()).Contains("gamma")
        && !TString( allParsCorrtmpY->GetName()).Contains("OneOver")){

      double nfactor= 1.0;
      if(  allParsCorrtmpY->getError() < 0.1)
        nfactor=10.;
      if( allParsCorrtmpY->getError() < 0.01)
        nfactor=100.;
      if (allParsCorrtmpY->getError() < 0.001)
        nfactor=1e3;
      if (allParsCorrtmpY->getError() < 0.0001)
        nfactor=1e4;
      normVFactor[allParsCorrtmpY->GetName()]=nfactor;
      normsV.push_back( allParsCorrtmpY->getVal());
      normsVErrs.push_back( nfactor * allParsCorrtmpY->getError());
      normsVNames.push_back(allParsCorrtmpY->GetName());
      normsVCount.push_back(normsV.size());
    }
    
    if(TString( allParsCorrtmpY->GetName()).Contains("matrix") || TString( allParsCorrtmpY->GetName()).Contains("OneOver")) continue;
    
    allParsCorrtmpX=nullptr;
    vector <double> tmpVec;
    while (( allParsCorrtmpX = (RooRealVar*)allParsIterSysX.Next())){
     
      if( allParsCorrtmpX==nullptr) continue; 
      if( allParsCorrtmpX->isConstant()) continue;
      if(TString( allParsCorrtmpX->GetName()).Contains("matrix") || TString( allParsCorrtmpX->GetName()).Contains("OneOver")) continue; 
      
      if( fabs(rnom->correlation(*allParsCorrtmpX,*allParsCorrtmpY)) > 0.2) {
	
        corrAllName.push_back( (TString(allParsCorrtmpY->GetName()).ReplaceAll("alpha_","")).Data());
        tmpVec.push_back( rnom->correlation(*allParsCorrtmpX,*allParsCorrtmpY));
        //cout<<"Correlation "<<allParsCorrtmpX->GetName()<<" "<<allParsCorrtmpY->GetName()<<" "<<rnom->correlation(*allParsCorrtmpX,*allParsCorrtmpY)<<endl;
	
      }
    }
    corrAllX.push_back(tmpVec);
    allParsIterSysX.Reset();
  }

  TH2F *hcorrAllSys=new TH2F("hcorrAllSys","",corrAllX.size(),0,corrAllX.size(),corrAllX.size(),0,corrAllX.size());
  hcorrAllSys->SetStats(0);
  hcorrAllSys->GetZaxis()->SetRangeUser(-1,+1);
  hcorrAllSys->GetYaxis()->SetTitleOffset(1.2);
  hcorrAllSys->GetYaxis()->SetTitleOffset(0.9);
  
  gStyle->SetPaintTextFormat("4.2f");

  for( int i=0;  i<(int)corrAllX.size(); i++){
    for( int j=0;  j<(int)corrAllX.size(); j++){

      hcorrAllSys->SetBinContent(i+1,j+1,corrAllX.at(i)[j]);
      hcorrAllSys->GetXaxis()->SetBinLabel(i+1,corrAllName[i].c_str());
      hcorrAllSys->GetYaxis()->SetBinLabel(j+1,corrAllName[j].c_str());

    }
  }

  TCanvas *cCorrNpAll=new TCanvas( Form("cCorrNpAll_%s_%s", string(isBBB ? "bbb" : regType).c_str(), currentDistribution.c_str()),"");{
    cCorrNpAll->cd();
    hcorrAllSys->Draw("colz text");
  }
  cCorrNpAll->Print(Form("%s.eps",cCorrNpAll->GetName()),"eps");
  

  // Draw the pulls of the nuisance paramters
  vector <string> npsnames;
  vector <double> npx;
  vector <double> npy;
  vector <double> npy_e;
  
  //TIterator *itNPsUncond= ((RooArgSet*)w->allVars().selectByName("alpha_*"))->createIterator();
  TIterator *itNPsUncond= model->GetNuisanceParameters()->createIterator();
  RooRealVar *tmpVNPShiftUncd=nullptr;

  while (( tmpVNPShiftUncd=(RooRealVar*)itNPsUncond->Next())){
    if (tmpVNPShiftUncd->isConstant()) continue;
    if( !TString(tmpVNPShiftUncd->GetName()).Contains("alpha_") ) continue; 
    npsnames.push_back( (TString(tmpVNPShiftUncd->GetName()).ReplaceAll("alpha_","")).Data());
    npx.push_back( npsnames.size() +0.5) ;
    npy.push_back( tmpVNPShiftUncd->getVal());
    npy_e.push_back(tmpVNPShiftUncd->getError());
  }
  TGraphErrors *npsGraph=new TGraphErrors((int)npsnames.size(),&npx[0],&npy[0],0,&npy_e[0]);
  npsGraph->SetMarkerStyle(8);
  npsGraph->SetLineColor(kBlack);
  
  TH1F *hNps=new TH1F(Form("hNps_%s_%s", string(isBBB ? "bbb" : regType).c_str(), currentDistribution.c_str()),"",npsnames.size(),1.0,npsnames.size()+1);
  SetDef(hNps);
  hNps->GetXaxis()->SetTitle("");
  hNps->GetYaxis()->SetTitle("#hat{#theta}-#theta/#delta#hat{#theta}");
  hNps->GetYaxis()->SetRangeUser(-2,+2);
  for(int i=0; i<(int)npsnames.size(); i++){
    hNps->GetXaxis()->SetBinLabel(i+1,npsnames[i].c_str());
  }
    
  TCanvas *cCNp=new TCanvas( Form("cCNp_%s_%s", string(isBBB ? "bbb" : regType).c_str(), currentDistribution.c_str()),"",1800,800);{
    SetCanvasDefaults(cCNp);
    cCNp->cd();
    cCNp->SetBottomMargin(0.6);
    cCNp->SetLeftMargin(0.02);
    hNps->Draw();
    npsGraph->Draw("PE");
  }
  cCNp->Print(Form("%s.eps",cCNp->GetName()),"eps");
  

  // Draw the confusion matrix
  TFile *fmatrices=nullptr;
  TCanvas *cConfMatrix=nullptr;

  if( hConfusionMatrix!=nullptr && !isBBB && isDiff) {

    fmatrices=TFile::Open(Form("matrixHistos_%s",currentDistribution.c_str()));
    hConfusionMatrix = (TH2F*)fmatrices->Get("hConfusionMatrix");

    //SetDef(hConfusionMatrix);
    hConfusionMatrix->GetZaxis()->SetRangeUser(-1,+1);
    hConfusionMatrix->GetYaxis()->SetTitleOffset(1.2);
    hConfusionMatrix->GetYaxis()->SetTitleOffset(0.9);
    
    cConfMatrix=new TCanvas(Form("cConfMatrix_%s",currentDistribution.c_str()),"");{
      cConfMatrix->cd();
      cConfMatrix->SetBottomMargin(0.1);
      cConfMatrix->SetTopMargin(0.1);
      hConfusionMatrix->Draw("colz text2");
    }
    cConfMatrix->Print(Form("%s.eps",cConfMatrix->GetName()),"eps");
  }

  
  

  // Stat Only fit
  w->allVars().selectByName("alpha_*")->setAttribAll("Constant",true);
  w->allVars().selectByName("*gamma*")->setAttribAll("Constant",true);

  RooAbsReal *mNllStat= pdf->createNLL( *data,
                                        //RooFit::Constrain(*model->GetNuisanceParameters()), 
                                        //RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                        //                                        RooFit::Offset(kTRUE), 
					Extended(true),
                                        RooFit::Verbose(kFALSE));

  RooAddition *NllModStat=new RooAddition("nllSum","nullSum",RooArgList(*mNllStat,*regSum));
  
  RooMinimizer *msysStat= new RooMinimizer(doReg ? *NllModStat:*mNllStat);;
  msysStat->setEps(1e-5);
  msysStat->optimizeConst(true);
  msysStat->setStrategy(2);
  msysStat->minimize("Minuit2");
  msysStat->hesse();
  //msysStat->minos(*poi);
  //msysStat->minos();
  msysStat->minos(minosPOi);
  w->saveSnapshot("postfitStat",allPars);
  //model->SetSnapshot(*poi);
  RooFitResult *rnomStat=msys->save();
  double minNllStat = rnomStat->minNll();
  cout<<"MinNLL for nominal stat only "<<minNllStat<<endl;

  
  

  vector <double> normsVStat;
  vector <double> normsVErrsStat;
  
  // Correlation of all parameters from stat Only 
  vector <vector <double>> corrAllXStat;
  vector <string> corrAllNameStat; 

  TIter allParsIterStatX=allPars.createIterator();
  TIter allParsIterStatY=allPars.createIterator();
  allParsCorrtmpX=nullptr;
  allParsCorrtmpY=nullptr;
  while (( allParsCorrtmpY = (RooRealVar*)allParsIterStatY.Next())){
    if(allParsCorrtmpY==nullptr) continue; 
    if (allParsCorrtmpY->isConstant()) continue; 
    
    if( !TString( allParsCorrtmpY->GetName()).Contains("vbf") && !allParsCorrtmpY->isConstant() && !TString( allParsCorrtmpY->GetName()).Contains("sigma")
        && !TString( allParsCorrtmpY->GetName()).Contains("alpha") && !TString( allParsCorrtmpY->GetName()).Contains("matrix") && !TString( allParsCorrtmpY->GetName()).Contains("gamma")
        && !TString( allParsCorrtmpY->GetName()).Contains("OneOver")){
      normsVStat.push_back( allParsCorrtmpY->getVal());
      normsVErrsStat.push_back( normVFactor[allParsCorrtmpY->GetName()]*allParsCorrtmpY->getError());
    }
    
    if(TString( allParsCorrtmpY->GetName()).Contains("matrix") || TString( allParsCorrtmpY->GetName()).Contains("OneOver")) continue;
    allParsCorrtmpX=nullptr;

    vector <double> tmpV;
    while (( allParsCorrtmpX = (RooRealVar*)allParsIterStatX.Next())){
      if(allParsCorrtmpX==nullptr) continue; 
      if( allParsCorrtmpX->isConstant()) continue;
      
      if(TString( allParsCorrtmpX->GetName()).Contains("matrix") || TString( allParsCorrtmpX->GetName()).Contains("OneOver")) continue; 
      
      tmpV.push_back(rnomStat->correlation(*allParsCorrtmpX,*allParsCorrtmpY));
      corrAllNameStat.push_back( (TString(allParsCorrtmpY->GetName()).ReplaceAll("alpha_","")).Data());
      
      //cout<<"Correlation "<<allParsCorrtmpX->GetName()<<" "<<allParsCorrtmpY->GetName()<<" "<<rnomStat->correlation(*allParsCorrtmpX,*allParsCorrtmpY)<<endl;
    }
    
    allParsIterStatX.Reset();
    corrAllXStat.push_back(tmpV); 
  }

  TH2F *hcorrAllStat=new TH2F("hcorrAllStat","",corrAllXStat.size(),0,corrAllXStat.size(),corrAllXStat.size(),0,corrAllXStat.size());
  hcorrAllStat->SetStats(0);
  hcorrAllStat->GetZaxis()->SetRangeUser(-1,+1);
  hcorrAllStat->GetYaxis()->SetTitleOffset(1.2);
  hcorrAllStat->GetYaxis()->SetTitleOffset(0.9);
  gStyle->SetPaintTextFormat("4.2f");
  
  for( int i=0;  i<(int)corrAllXStat.size(); i++){
    hcorrAllStat->GetXaxis()->SetBinLabel(i+1,corrAllNameStat[i].c_str());
    for( int j=0;  j<(int)corrAllXStat.size(); j++){
      hcorrAllStat->SetBinContent(i,j,corrAllXStat.at(i)[j]);
      hcorrAllStat->GetYaxis()->SetBinLabel(j+1,corrAllNameStat[j].c_str());
    }
  }
  TCanvas *cCorrNpAllStat=new TCanvas( Form("cCorrNpAllStat_%s_%s", string(isBBB ? "bbb" : regType).c_str(), currentDistribution.c_str()),"");{
    cCorrNpAllStat->cd();
    hcorrAllStat->Draw("colz text");
  }
  cCorrNpAllStat->Print(Form("%s.eps",cCorrNpAllStat->GetName()),"eps");


  // Draw the norm factors results
  TH2F *hNormRes=new TH2F(Form("hNormRes_%s_%s", string(isBBB ? "bbb" : regType).c_str(), currentDistribution.c_str()),"",50,0,+2.5,normsV.size()*2,0.5,normsV.size()*2+0.5);
  SetDef(hNormRes); 
  hNormRes->SetTitle("");
  hNormRes->SetStats(0);
  hNormRes->SetFillColor(kWhite);
  for( int i=0; i<(int)normsVNames.size(); i++){
    hNormRes->GetYaxis()->SetBinLabel(i+1,normsVNames[i].c_str());
  }
  hNormRes->GetYaxis()->SetRangeUser(0,normsV.size()+5);
  hNormRes->GetXaxis()->SetTitle("#it{N}^{Data}/#it{N}^{MC}"); 
  

  TGraphErrors *normsGraphSys=new TGraphErrors( (int)normsV.size(),&normsV[0],&normsVCount[0],&normsVErrs[0],0);
  normsGraphSys->SetMarkerStyle(8);
  normsGraphSys->SetLineColor(kBlack);
  TGraphErrors *normsGraphStat=new TGraphErrors( (int)normsV.size(),&normsVStat[0],&normsVCount[0],&normsVErrsStat[0],0);
  normsGraphStat->SetMarkerStyle(8);
  normsGraphStat->SetLineColor(kBlack);

  TCanvas *cResNorms=new TCanvas( Form("cResNorms_%s_%s", string(isBBB ? "bbb" : regType).c_str(), currentDistribution.c_str()),"");{
    cResNorms->cd();
    SetCanvasDefaults(cResNorms);
    cResNorms->SetLeftMargin(0.25);
    hNormRes->Draw();
    normsGraphSys->Draw("PE same");
    normsGraphStat->Draw("[] same");

    for( int i=0; i<(int)normsV.size(); i++){
      lat->DrawLatex(2.0,hNormRes->GetYaxis()->GetBinCenter(i+1),Form("#times %.0lf",normVFactor[normsVNames.at(i)]));;
      //hNormRes->GetYaxis()->SetBinLabel(i+1,normsVNames[i].c_str());
      
    }
    //for(std::map<string, double>::iterator it=normVFactor.begin(); it!=normVFactor.end(); it++){
    //lat->DrawLatex(2.0,1.5*ipos+hNormRes->GetYaxis()->GetBinWidth(1),Form("#times %.0lf",(*it).second));
    //ipos++;
    //}
    
    lat->DrawLatex(0.1,hNormRes->GetYaxis()->GetBinCenter(normsV.size()+4.5), "#it{#bf{ATLAS}} Internal");
    lat->DrawLatex(0.1,hNormRes->GetYaxis()->GetBinCenter(normsV.size()+3.8),Form("#scale[1.0]{ #sqrt{#it{s}} = 13 TeV, %.1f fb^{-1}}",139.1));
    if(isBBB)
      lat->DrawLatex(0.1,hNormRes->GetYaxis()->GetBinCenter(normsV.size()+2.8),"bin by bin");
    else
      if (doReg)
	lat->DrawLatex(0.1,hNormRes->GetYaxis()->GetBinCenter(normsV.size()+2.8),Form("reg. matrix #tau=%.2lf",tauD));
      else 
	lat->DrawLatex(0.1,hNormRes->GetYaxis()->GetBinCenter(normsV.size()+2.8),"unreg. likelihood matrix.");
    
    
  }
  cResNorms->Print(Form("%s.eps",cResNorms->GetName()),"eps");
  
  

  vector <double> xSecStat;
  vector <double> xSecUpStat;
  vector <double> xSecDownStat;
  

  RooRealVar *xPStat=nullptr;
  if(!isDiff){
    xSecStat.push_back(poi->getVal());
    xSecUpStat.push_back(poi->getErrorHi());
    xSecDownStat.push_back(poi->getErrorLo());
  }

  TGraphAsymmErrors *gComp2=nullptr; 
  TGraphAsymmErrors *gComp3=nullptr;
  TPad*    stack3 =nullptr;
  TPad*    comparison =nullptr;
  TCanvas *cDiff=nullptr;
  TLegend *leg=nullptr;
  double canX=800;
  double canY=800;
  TH1F *hDummyRatio=nullptr;
  TH1F *hDummy=nullptr;
  TGraphAsymmErrors *gComp1=nullptr;
  TGraphAsymmErrors *gCompDataStat=nullptr;
  TGraphAsymmErrors *gCompData=nullptr;
  TGraphAsymmErrors *statPsys=nullptr;
  TGraphAsymmErrors *statPstat=nullptr;
  TGraphAsymmErrors *theory1=nullptr;
  TH1F *htheory1=nullptr;
  TH1F *htheory2=nullptr;
  TH1F *htheory3=nullptr;
  if( isDiff){
  
    for( int i=0; i<(int) nBinsDiff[currentDistribution]; i++){
      RooRealVar *xP=(RooRealVar*)w->var(Form("%s%d",xsecVar.c_str(),i));
      double binWidth=binEdgesMap[currentDistribution][i+1]-binEdgesMap[currentDistribution][i];
      xSecStat.push_back(xP->getVal()/binWidth*UnitsScaling[UnitsMap[currentDistribution]]);
      xSecUpStat.push_back(UnitsScaling[UnitsMap[currentDistribution]]*fabs(xP->getErrorHi()/binWidth));
      xSecDownStat.push_back(UnitsScaling[UnitsMap[currentDistribution]]*fabs(xP->getErrorLo()/binWidth));
      cout<<" bin "<<xSecStat.back()<<" "<<xSecUpStat.back()<<" bin "<<xbins.at(i)<<endl;
    }
    
    statPsys=new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSec[0],0,0,&xSecUp[0],&xSecDown[0]);
    statPsys->SetMarkerStyle(8);

    statPstat=new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSecStat[0],0,0,&xSecUpStat[0],&xSecDownStat[0]);
    statPstat->SetMarkerStyle(8);

    theory1=new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSecTheory[0],0,0,0,0);
    theory1->SetLineColor(kRed);
    theory1->SetLineWidth(2);

    // Print out the resutls
    if(!isDiff){
      for(int i=0; i<(int)xSec.size(); i++){
	cout<<"bin "<<i+1<<" $\\sigma = "<<xSec[i]<<"^{+"<<fabs(xSecUp[i])<<"}_{-"<<fabs(xSecDown[i])<<"} \\text{fb} = "<<xSec[i]<<"^{+"<<fabs(xSecUpStat[i])<<"}_{-"<<fabs(xSecDownStat[i])<<"} \\ \\text{(stat)} ^{+"<<sqrt(xSecUp[i]*xSecUp[i] - xSecUpStat[i]*xSecUpStat[i])<<"}_{-"<<sqrt(xSecUp[i]*xSecUp[i] - xSecDownStat[i]*xSecDownStat[i])<<"} \\ \\text{(sys)} \\text{fb}$"<<endl;
      }}

    

    htheory1=new TH1F("htheory1","",nBinsDiff[currentDistribution],&binEdgesMap[currentDistribution][0]);
    SetDef(htheory1);
    htheory1->SetLineColor(kRed);
    htheory1->SetLineWidth(2);
    htheory1->SetMarkerStyle(25);
    for( int i=0; i<(int)nBinsDiff[currentDistribution]; i++)
      htheory1->SetBinContent(i+1,UnitsScaling[UnitsMap[currentDistribution]]*xSecTheory[i]);


    vector <double> xSecRatioData;
    vector <double> xSecRatioDataUp;
    vector <double> xSecRatioDataDown;

    vector <double> xSecRatioDataUpStat;
    vector <double> xSecRatioDataDownStat;
    
    
    vector <double> xSecRatioth1;
    vector <double> xSecRatioth1Up;
    vector <double> xSecRatioth1Down;
    
    vector <double> xSecRatioth2;
    vector <double> xSecRatioth2Up;
    vector <double> xSecRatioth2Down;
    
    
    vector <double> xSecRatioth3;
    vector <double> xSecRatioth3Up;
    vector <double> xSecRatioth3Down;
    

    // Fill the theory comparisons
    if(isDiff){
      for ( int i=0; i<(int)xSec.size(); i++){

	xSecRatioData.push_back(1.0);
	xSecRatioDataUp.push_back(xSecUp[i]/xSec[i]);
	xSecRatioDataDown.push_back(xSecDown[i]/xSec[i]);
	xSecRatioDataUpStat.push_back(  xSecUpStat[i]/xSec[i]);
	xSecRatioDataDownStat.push_back( xSecDownStat[i]/xSec[i]);
       
	
	//xSecRatioData.push_back(xSecTheory[i]/xSec[i]);
	//xSecRatioDataUp.push_back(xSecTheory[i]/xSecUp[i]);
	//xSecRatioDataDown.push_back(xSecTheory[i]/xSecDown[i]);
	//xSecRatioDataUpStat.push_back( xSecTheory[i]/ xSecUpStat[i]);
	//xSecRatioDataDownStat.push_back( xSecTheory[i]/xSecDownStat[i]);

	

	  xSecRatioth1.push_back( htheory1->GetBinContent(i+1)  /xSec[i]);
	  xSecRatioth1Up.push_back( xSecRatioth1.back() * htheory1->GetBinError(i+1) / htheory1->GetBinContent(i+1));
	  xSecRatioth1Down.push_back(xSecRatioth1.back() *  htheory1->GetBinError(i+1) / htheory1->GetBinContent(i+1));
	  
	  if(addOtherTheory){
	    htheory2=(TH1F*) (normTheoryPredtoYR4 ? theoryLables["VBFNLO@NLO  (YR4)"] :   theoryLables["VBFNLO@NLO,Fixed order"]);
	    htheory3=(TH1F*) (normTheoryPredtoYR4? theoryLables["MG_aMC@NLO  (YR4)"] : theoryLables["MG_aMC@NLO"]);
	    
	    if(normTheoryPredtoYR4){
	      xSecRatioth2.push_back(theoryLables["VBFNLO@NLO  (YR4)"]->GetBinContent(i+1) / xSec[i]);
	      xSecRatioth3.push_back(theoryLables["MG_aMC@NLO  (YR4)"]->GetBinContent(i+1) / xSec[i]);
	      
	    }
	    else{
	      xSecRatioth2.push_back(1/xSec.at(i)/theoryLables["VBFNLO@NLO,Fixed order"]->GetBinContent(i+1));
	      xSecRatioth3.push_back(1/xSec.at(i)/theoryLables["MG_aMC@NLO"]->GetBinContent(i+1));
	    }
	    
	    xSecRatioth2Up.push_back(xSecRatioth2.back() * htheory2->GetBinError(i+1) / htheory2->GetBinContent(i+1));
	    xSecRatioth2Down.push_back(xSecRatioth2.back() * htheory2->GetBinError(i+1) / htheory2->GetBinContent(i+1));
	    
	    xSecRatioth3Up.push_back(xSecRatioth3.back() * htheory3->GetBinError(i+1) / htheory3->GetBinContent(i+1));
	    xSecRatioth3Down.push_back(xSecRatioth3.back() * htheory3->GetBinError(i+1) / htheory3->GetBinContent(i+1));

	  cout<<" MG "<<xSecRatioth3.back()<<endl;
	}
      }}


    gCompData= new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSecRatioData[0],0,0,&xSecRatioDataUp[0],&xSecRatioDataDown[0]);
    gCompData->SetMarkerStyle(8);
    gCompData->SetMarkerColor(kBlack);
    gCompData->SetLineColor(kBlack);
    gCompData->SetFillStyle(3144);
    gCompData->SetLineWidth(2);


    gCompDataStat= new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSecRatioData[0],0,0,&xSecRatioDataUpStat[0],&xSecRatioDataDownStat[0]);
    gCompDataStat->SetMarkerStyle(8);
    gCompDataStat->SetMarkerColor(kBlack);
    gCompDataStat->SetLineColor(kBlack);
    gCompDataStat->SetFillStyle(3144);
    gCompDataStat->SetLineWidth(2);
  

    gComp1= new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSecRatioth1[0],0,0,&xSecRatioth1Up[0],&xSecRatioth1Down[0]);
    gComp1->SetMarkerStyle(htheory1->GetMarkerStyle());
    gComp1->SetMarkerColor(htheory1->GetMarkerColor());
    gComp1->SetLineColor(htheory1->GetLineColor());
    gComp1->SetLineWidth(2);
  
 
  
  
  
    if(addOtherTheory){
      gComp2=new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSecRatioth2[0],0,0,&xSecRatioth2Up[0],&xSecRatioth2Down[0]);
      gComp3=new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSecRatioth3[0],0,0,&xSecRatioth3Up[0],&xSecRatioth3Down[0]);
    }
  
    if(normTheoryPredtoYR4 && addOtherTheory){
      gComp2->SetMarkerStyle(theoryLables["VBFNLO@NLO  (YR4)"]->GetMarkerStyle());
      gComp2->SetMarkerColor(theoryLables["VBFNLO@NLO  (YR4)"]->GetMarkerColor());
      gComp2->SetLineColor(theoryLables["VBFNLO@NLO  (YR4)"]->GetLineColor());
      gComp2->SetFillStyle(theoryLables["VBFNLO@NLO  (YR4)"]->GetFillStyle());
      gComp2->SetFillColor(theoryLables["VBFNLO@NLO  (YR4)"]->GetFillColor());
      gComp2->SetLineWidth(2);


      gComp3->SetMarkerStyle(theoryLables["MG_aMC@NLO  (YR4)"]->GetMarkerStyle());
      gComp3->SetMarkerColor(theoryLables["MG_aMC@NLO  (YR4)"]->GetMarkerColor());
      gComp3->SetLineColor(theoryLables["MG_aMC@NLO  (YR4)"]->GetLineColor());
      gComp3->SetFillStyle(theoryLables["MG_aMC@NLO  (YR4)"]->GetFillStyle());
      gComp3->SetFillColor(theoryLables["MG_aMC@NLO  (YR4)"]->GetFillColor());
      gComp3->SetLineWidth(2);
    }
    else if (addOtherTheory){
      gComp2->SetMarkerStyle(theoryLables["VBFNLO@NLO,Fixed order"]->GetMarkerStyle());
      gComp2->SetMarkerColor(theoryLables["VBFNLO@NLO,Fixed order"]->GetMarkerColor());
      gComp2->SetLineColor(theoryLables["VBFNLO@NLO,Fixed order"]->GetLineColor());
      gComp2->SetFillStyle(theoryLables["VBFNLO@NLO,Fixed order"]->GetFillStyle());
      gComp2->SetFillColor(theoryLables["VBFNLO@NLO,Fixed order"]->GetFillColor());
      gComp2->SetLineWidth(2);


      gComp3->SetMarkerStyle(theoryLables["MG_aMC@NLO"]->GetMarkerStyle());
      gComp3->SetMarkerColor(theoryLables["MG_aMC@NLO"]->GetMarkerColor());
      gComp3->SetLineColor(theoryLables["MG_aMC@NLO"]->GetLineColor());
      gComp3->SetFillStyle(theoryLables["MG_aMC@NLO"]->GetFillStyle());
      gComp3->SetFillColor(theoryLables["MG_aMC@NLO"]->GetFillColor());
      gComp3->SetLineWidth(2);
    
    
    }

    // Make a nice plot
    hDummy=new TH1F("hDummy","",nBinsDiff[currentDistribution],&binEdgesMap[currentDistribution][0]);
    SetDef(hDummy);
    double plotmax=0;
    double plotmin=-0.1;
    for(std::vector<double>::iterator it=xSec.begin(); it!=xSec.end(); it++)
      if( (*it) > plotmax) plotmax=(*it);
    plotmax=1.90*plotmax;
	
    hDummy->GetYaxis()->SetRangeUser(plotmin,plotmax);
    hDummy->GetXaxis()->SetTitle(Form("%s %s",labelMap[currentDistribution].c_str(),VarUnitMap[currentDistribution].c_str()));
    hDummy->GetYaxis()->SetTitle(Form("d#it{#sigma} / d%s [%s]",labelMap[currentDistribution].c_str(),unitsMap[currentDistribution].c_str()));
    hDummy->GetYaxis()->SetTitleOffset(1.4);
    hDummy->SetLineColor(kWhite);
    hDummy->SetFillColor(kWhite);

    hDummyRatio=(TH1F*)hDummy->Clone("hDummyRatio");
    hDummyRatio->GetYaxis()->SetTitle("Pred / Data");
    //hDummyRatio->GetYaxis()->SetRangeUser(-0.24,2.24);
    hDummyRatio->GetYaxis()->SetRangeUser(0.354,1.756);
    hDummyRatio->GetYaxis()->CenterTitle(true);
    hDummyRatio->GetXaxis()->SetTitleOffset(3.0);
    hDummyRatio->GetYaxis()->SetTitleOffset(1.4);

    
    leg=DefLeg(0.5,0.6,0.89,0.9);
    if(compareToOtherInput)
      leg->SetHeader("Powheg model");
    leg->AddEntry(statPsys,"Data stat#oplussys","PE");
    leg->AddEntry(statPstat,"Data stat only","E");
    if(compareToOtherInput)
      leg->AddEntry(htheory1,"MG prediction");
    else 
      leg->AddEntry(htheory1,"Powheg+Pythia8 (YR4)","PLE");
    
    if(addOtherTheory)
      for(std::map<string,TH1*>::iterator it=theoryLables.begin(); it!=theoryLables.end(); it++)
        leg->AddEntry((*it).second,(*it).first.c_str(),"PFLE");

   
    

    
    cDiff=new TCanvas(Form("cDiff_%s_%s",string(isBBB ? "bbb" : regType).c_str(),currentDistribution.c_str()),"",canX,canY);{
      cDiff->cd();
      SetCanvasDefaults(cDiff);
      
      cDiff->SetBottomMargin(0);
      
      stack3= new TPad(Form("stack3_%s",currentDistribution.c_str()), "", 0, 0.4, 1, 1);   
      comparison= new TPad(Form("comp3_%s",currentDistribution.c_str()), "", 0,0, 1, 0.4);
      
      stack3->SetRightMargin(0.02);
      stack3->SetLeftMargin(0.16);
      stack3->SetTopMargin(0.02);
      stack3->SetBottomMargin(0.001);
      
      comparison->SetRightMargin(0.02);
      comparison->SetTopMargin(0);
      comparison->SetBottomMargin(0.45);
      comparison->SetLeftMargin(0.16);
      
      stack3->SetTicks();
      comparison->SetTicks();
      
      cDiff->cd();
      stack3->Draw();
      comparison->Draw();   
      
      stack3->cd();
      
      
      hDummy->Draw("hist");
      for(std::map<string,TH1*>::iterator it=theoryLables.begin(); it!=theoryLables.end(); it++)
        (*it).second->Draw("PE2 same");
      
      htheory1->Draw("hist same ");
      
      
      statPstat->Draw("[] same");
      statPsys->Draw("PE");
      leg->Draw();
      lat->DrawLatex(binEdgesMap[currentDistribution][0]+0.4*binEdgesMap[currentDistribution][0],plotmax*0.95,"#it{#bf{ATLAS}} Internal");
      lat->DrawLatex(binEdgesMap[currentDistribution][0]+0.4*binEdgesMap[currentDistribution][0],plotmax*0.92,Form("#scale[1.0]{ #sqrt{#it{s}} = 13 TeV, %.1f fb^{-1}}",139.1));
      if(isBBB)
        lat->DrawLatex(binEdgesMap[currentDistribution][0]+0.4*binEdgesMap[currentDistribution][0],plotmax*0.8,"bin by bin");
      else
        if (doReg)
          lat->DrawLatex(binEdgesMap[currentDistribution][0]+0.4*binEdgesMap[currentDistribution][0],plotmax*0.8,Form("reg. matrix #tau=%.2lf",tauD));
        else 
          lat->DrawLatex(binEdgesMap[currentDistribution][0]+0.4*binEdgesMap[currentDistribution][0],plotmax*0.8,"unreg. likelihood matrix.");
      
      ///ATLASLabel(lat,binEdgesMap[currentDistribution][0]+0.1*binEdgesMap[currentDistribution][0],4.5,false,2,139.0);
      comparison->cd();
      hDummyRatio->Draw("hist");
      gCompData->Draw("e same");
      gCompDataStat->Draw("[] same");
      gComp1->Draw("pe same");
      if(addOtherTheory){
        gComp2->Draw("pe same");
        gComp3->Draw("pe same");
      }
    }

    cDiff->Print(Form("%s.eps",cDiff->GetName()),"eps");
  }


  w->allVars().selectByName("alpha_*")->setAttribAll("Constant",false);
  
  
  if(doRanking) {
    RooAbsData *dataPostFit=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
    
    // make a ranking
    std::map<string, std::map<string,std::pair<double,double>>> ParResults_up;
    std::map<string, std::map<string,std::pair<double,double>>> ParResults_down;
    
    std::map<string, std::map<string,std::pair<double,double>>> ParResults_upPull;
    std::map<string, std::map<string,std::pair<double,double>>> ParResults_downPull;
    
    std::map<string, std::map<string,std::pair<double,double>>> ParResults_FixedZeroPrefit;
    std::map<string, std::map<string,std::pair<double,double>>> ParResults_FixedZeroPostfit;
    
    
    TIter itPars_=allPars.createIterator();
    RooRealVar *tmpAllPars_=nullptr;
    
    std::map<string,std::pair<double,double>> fitResultsNom;
    
    while( (tmpAllPars_=(RooRealVar*)itPars_.Next())){
      fitResultsNom[tmpAllPars_->GetName()]=make_pair(tmpAllPars_->getVal(),tmpAllPars_->getError());
    }
    
    ParResults_up["nominal"]=fitResultsNom;
    ParResults_down["nominal"]=fitResultsNom;
    
    ParResults_upPull["nominal"]=fitResultsNom;
    ParResults_downPull["nominal"]=fitResultsNom;
    
    ParResults_FixedZeroPrefit["nominal"]=fitResultsNom;
    ParResults_FixedZeroPostfit["nominal"]=fitResultsNom;
    
    
    
    RooArgSet *nuisanceParameters=(RooArgSet*)model->GetNuisanceParameters();
    TIter itNPs=nuisanceParameters->createIterator();
    RooRealVar *tmpVNPShift=nullptr;
    int pnCount=0;
    while( (tmpVNPShift=(RooRealVar*)itNPs.Next())){
    
      w->loadSnapshot("prefit");
      double shift[2]={0,0};

      // first do the prefit ranking 
      if( TString(tmpVNPShift->GetName()).Contains("alpha_")) { shift[0]=tmpVNPShift->getVal()-1; shift[1]=tmpVNPShift->getVal()+1;} 
      else { shift[0]=tmpVNPShift->getVal()-tmpVNPShift->getError(); shift[1]=tmpVNPShift->getVal()+tmpVNPShift->getError();}


      std::map<string,std::pair<double,double>> fitResultsDownPull;
      std::map<string,std::pair<double,double>> fitResultsUpPull;
      std::map<string,std::pair<double,double>> fitResultsBreakDownPrefit;
    
    
      for(int i=0; i<2; i++){

	tmpVNPShift->setVal(shift[i]);
	tmpVNPShift->setConstant(true);
	RooAbsReal *mNlltmp= pdf->createNLL( *data,
					     RooFit::Constrain(*model->GetNuisanceParameters()), 
					     RooFit::GlobalObservables(*model->GetGlobalObservables()),
					     RooFit::Offset(kTRUE), 
					     Extended(true),
					     RooFit::Verbose(kFALSE));
      
	RooMinimizer *msystmp= new RooMinimizer(*mNlltmp);;
	msystmp->setEps(0.01);
	msystmp->optimizeConst(2);
	msystmp->setStrategy(1);
	msystmp->minimize("Minuit2");

	TIter itPars=allPars.createIterator();
	RooRealVar *tmpAllPars=nullptr;
	while( (tmpAllPars=(RooRealVar*)itPars.Next())){
	  if(i==0)
	    fitResultsDownPull[tmpAllPars->GetName()]=make_pair(tmpAllPars->getVal(),tmpAllPars->getError());
	  else 
	    fitResultsUpPull[tmpAllPars->GetName()]=make_pair(tmpAllPars->getVal(),tmpAllPars->getError());
	}
	tmpVNPShift->setConstant(false);
	w->loadSnapshot("prefit");
	delete msystmp;
	delete mNlltmp;
      }

      ParResults_upPull[tmpVNPShift->GetName()]=fitResultsUpPull;
      ParResults_downPull[tmpVNPShift->GetName()]=fitResultsDownPull;
    
      // here make a fit with fixing the np
      w->loadSnapshot("prefit");
      tmpVNPShift->setConstant(true);
      // make the fit
      RooAbsReal *mNlltmp2= pdf->createNLL( *data,
					    RooFit::Constrain(*model->GetNuisanceParameters()), 
					    RooFit::GlobalObservables(*model->GetGlobalObservables()),
					    RooFit::Offset(kTRUE), 
					    Extended(true),
					    RooFit::Verbose(kFALSE));
      
      RooMinimizer *msystmp2= new RooMinimizer(*mNlltmp2);;
      msystmp2->setEps(0.01);
      msystmp2->optimizeConst(2);
      msystmp2->setStrategy(1);
      msystmp2->minimize("Minuit2");
    
      TIter itPars2=allPars.createIterator();
      RooRealVar *tmpAllPars2=nullptr;
      while( (tmpAllPars2=(RooRealVar*)itPars2.Next())){
        fitResultsBreakDownPrefit[tmpAllPars2->GetName()]=make_pair(tmpAllPars2->getVal(),tmpAllPars2->getError());
      }
      tmpVNPShift->setConstant(false);
      delete msystmp2;
      delete mNlltmp2;
      
      ParResults_FixedZeroPrefit[tmpVNPShift->GetName()]=fitResultsBreakDownPrefit;
      
      w->loadSnapshot("prefit");
    
      // here make the postfit 
      std::map<string,std::pair<double,double>> fitResultsDown;
      std::map<string,std::pair<double,double>> fitResultsUp;
      std::map<string,std::pair<double,double>> fitResultsBreakDownPostfit;
      w->loadSnapshot("postfit");
      
      // first do the prefit ranking 
      if( TString(tmpVNPShift->GetName()).Contains("alpha_")) { shift[0]=tmpVNPShift->getVal()-1; shift[1]=tmpVNPShift->getVal()+1;} 
      else { shift[0]=tmpVNPShift->getVal()-tmpVNPShift->getError(); shift[1]=tmpVNPShift->getVal()+tmpVNPShift->getError();}
      
      for(int i=0; i<2; i++){
      
	tmpVNPShift->setVal(shift[i]);
	tmpVNPShift->setConstant(true);
      
	//RooAbsData *datatmp=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
      
	RooAbsReal *mNlltmp= pdf->createNLL( *dataPostFit,
					     RooFit::Constrain(*model->GetNuisanceParameters()), 
					     RooFit::GlobalObservables(*model->GetGlobalObservables()),
					     RooFit::Offset(kTRUE), 
					     Extended(true),
					     RooFit::Verbose(kFALSE));
      
	RooMinimizer *msystmp= new RooMinimizer(*mNlltmp);;
	msystmp->setEps(0.01);
	msystmp->optimizeConst(2);
	msystmp->setStrategy(1);
	msystmp->minimize("Minuit2");

	TIter itPars=allPars.createIterator();
	RooRealVar *tmpAllPars=nullptr;
	while( (tmpAllPars=(RooRealVar*)itPars.Next())){
	  if(i==0)
	    fitResultsDown[tmpAllPars->GetName()]=make_pair(tmpAllPars->getVal(),tmpAllPars->getError());
	  else 
	    fitResultsUp[tmpAllPars->GetName()]=make_pair(tmpAllPars->getVal(),tmpAllPars->getError());
	}
	delete msystmp;
	delete mNlltmp;
	//delete datatmp;
	tmpVNPShift->setConstant(false);
	w->loadSnapshot("postfit");
      }

      ParResults_up[tmpVNPShift->GetName()]=fitResultsUp;
      ParResults_down[tmpVNPShift->GetName()]=fitResultsDown;


      w->loadSnapshot("postfit");
      tmpVNPShift->setConstant(true);
      // make the fit
      RooAbsReal *mNlltmp4= pdf->createNLL( *dataPostFit,
					    RooFit::Constrain(*model->GetNuisanceParameters()), 
					    RooFit::GlobalObservables(*model->GetGlobalObservables()),
					    RooFit::Offset(kTRUE), 
					    Extended(true),
					    RooFit::Verbose(kFALSE));
      
      RooMinimizer *msystmp4= new RooMinimizer(*mNlltmp4);;
      msystmp4->setEps(0.01);
      msystmp4->optimizeConst(2);
      msystmp4->setStrategy(1);
      msystmp4->minimize("Minuit2");
    
      TIter itPars3=allPars.createIterator();
      RooRealVar *tmpAllPars3=nullptr;
      while( (tmpAllPars3=(RooRealVar*)itPars3.Next())){
        fitResultsBreakDownPostfit[tmpAllPars3->GetName()]=make_pair(tmpAllPars3->getVal(),tmpAllPars3->getError());
      }

      
      delete msystmp4;
      delete mNlltmp4;
      ParResults_FixedZeroPostfit[tmpVNPShift->GetName()]=fitResultsBreakDownPostfit;

      tmpVNPShift->setConstant(false);
      w->loadSnapshot("prefit");

      //if(pnCount > 5) break;
      pnCount++;
    }


    // Order the results
    vector <string> rankingOrder;
    for( auto & parIter : ParResults_upPull )
      rankingOrder.push_back(parIter.first);
    std::sort(rankingOrder.begin(), rankingOrder.end(), [&ParResults_upPull,&ParResults_downPull](string a,string b){
	double up_a=( (ParResults_upPull[a])["mu_vbf"].first - (ParResults_upPull["nominal"])["mu_vbf"].first)/((ParResults_upPull["nominal"])["mu_vbf"].first);
	double down_a= ( (ParResults_downPull[a])["mu_vbf"].first - (ParResults_downPull["nominal"])["mu_vbf"].first)/((ParResults_downPull["nominal"])["mu_vbf"].first);
	double err_a=0.5*(fabs(up_a)+fabs(down_a));

	double up_b=( (ParResults_upPull[b])["mu_vbf"].first - (ParResults_upPull["nominal"])["mu_vbf"].first)/((ParResults_upPull["nominal"])["mu_vbf"].first);
	double down_b= ( (ParResults_downPull[b])["mu_vbf"].first - (ParResults_downPull["nominal"])["mu_vbf"].first)/((ParResults_downPull["nominal"])["mu_vbf"].first);
	double err_b=0.5*(fabs(up_b)+fabs(down_b));

	return a < b ; 
      });
  
  
    // order with respect to the postfit ranking
    //for( auto & parIter : ParResults_up ){
    //}

    // print the ranking
    cout<<endl;
    cout<<endl;
    cout<<"Variation & POI sigma up [%]& POI sigma down [%] NP val & NP err & breakdown uncertainty" <<endl;
    //for( auto & parIter : ParResults_upPull ){
    //string variation=parIter.first;
    for(vector<string>::iterator it=rankingOrder.begin(); it!=rankingOrder.end(); it++){
      string variation=(*it);
      cout<<variation<<" & "<<
	( (ParResults_upPull[variation])["mu_vbf"].first - (ParResults_upPull["nominal"])["mu_vbf"].first)/((ParResults_upPull["nominal"])["mu_vbf"].first)*100 <<" & "<<
	( (ParResults_downPull[variation])["mu_vbf"].first - (ParResults_downPull["nominal"])["mu_vbf"].first)/((ParResults_downPull["nominal"])["mu_vbf"].first)*100
	  <<" & "<<(ParResults_upPull["nominal"])[variation].first<<" & +/- "<<(ParResults_upPull["nominal"])[variation].second<<" & "<<
	sqrt( pow(ParResults_FixedZeroPrefit["nominal"]["mu_vbf"].second,2) - pow(ParResults_FixedZeroPrefit[variation]["mu_vbf"].second,2))<<endl;
    }

    cout<<endl;
    cout<<endl;

    // print the ranking
    cout<<"Postfit "<<endl;
    cout<<"Variation & POI sigma up [%]& POI sigma down [%] NP val & NP err & breakdown "<<endl;
    //for( auto & parIter : ParResults_up ){
    //string variation=parIter.first;
    for(vector<string>::iterator it=rankingOrder.begin(); it!=rankingOrder.end(); it++){
      string variation=(*it);
      cout<<variation<<" & "<<
	( (ParResults_up[variation])["mu_vbf"].first - (ParResults_up["nominal"])["mu_vbf"].first)/((ParResults_up["nominal"])["mu_vbf"].first)*100 <<"   &   " << 
	( (ParResults_down[variation])["mu_vbf"].first - (ParResults_down["nominal"])["mu_vbf"].first)/((ParResults_down["nominal"])["mu_vbf"].first)*100<<"  &  "
	  <<" & "<<(ParResults_upPull["nominal"])[variation].first<<" & +/- "<<(ParResults_upPull["nominal"])[variation].second<<" & "<<
	sqrt( pow(ParResults_FixedZeroPostfit["nominal"]["mu_vbf"].second,2) - pow(ParResults_FixedZeroPostfit[variation]["mu_vbf"].second,2))<<endl;
    
    }

  
  

    /*
    // create output tree
    TFile *ffitResults=new new TFile("ffitResults","RECREATE");
    TTree *t=new TTree("fit","fits");
    vector <TString*> 
    t->Branch(
    */
    //return ;
  }
  
  if( doSignificance){
    // Get the significance
    //return ;
    w->loadSnapshot("prefit");

    //if(fractionalABCD){
    //RooRealVar *muA=(RooRealVar*)w->var("mu_ggfA");
    //muA->setConstant(true);
    //muA->setRange(-2,+2);
    
    //RooRealVar *muB=(RooRealVar*)w->var("mu_ggfB");
    //muB->setConstant(true);
    //muB->setRange(-2,+2);
    
    //RooRealVar *muC=(RooRealVar*)w->var("mu_ggfC");
    //muC->setConstant(false);
    //muC->setRange(-2,+2);
    //}
    
    
    //w->loadSnapshot("prefit");
    // create the AsymptoticCalculator from data,alt model, null model
    //AsymptoticCalculator  ac(*data, *model, *bModel);
    //ac.SetOneSidedDiscovery(true);  // for one-side discovery test
    //ac.SetPrintLevel(-1);  // to suppress print level 
    
    
    // Make a manual fit;

    //RooAbsPdf *bPdf=bModel->GetPdf();
    poi->setVal(0.0);
    poi->setConstant(true); 
    for( int ik=0 ; ik<(int)nBinsDiff[currentDistribution]; ik++){
      RooRealVar *mu_bin=(RooRealVar*)w->var(Form("mu_vbf_bin_%d",ik));
      mu_bin->setVal(0.0);
      mu_bin->setConstant(true);
      RooRealVar *sigma_bin=(RooRealVar*)w->var(Form("sigma_bin%d",ik));
      sigma_bin->setVal(0.0);
      sigma_bin->setConstant(true);
    }

    RooAbsReal *mNllSig= pdf->createNLL( *data,
					 RooFit::Constrain(*model->GetNuisanceParameters()), 
                                         RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                         RooFit::Offset(true), 
                                         Extended(true),
                                         RooFit::Verbose(kFALSE));
    
    RooMinimizer *msysSig= new RooMinimizer(*mNllSig);
    msysSig->setEps(1e-5);
    msysSig->optimizeConst(2);
    msysSig->setStrategy(2);
    msysSig->minimize("Minuit2");
    
    // print the results
    RooFitResult *rsig=msysSig->save();
    double minNllAlt = rsig->minNll();
    cout << "NLL after minimisation for alternative hypothesis: " << setprecision(15) << minNllAlt<<endl;
    
    int ndf = 1; 
    //double tmu = 2. * (fabs(minNllAlt) - fabs(minNll));
    double tmu = 2. * (fabs(minNllAlt - minNll));
    double p = TMath::Prob(tmu, ndf);
    double z = ROOT::Math::gaussian_quantile(1. - p / 2., 1);


    // run the calculator
    //HypoTestResult * asResult = ac.GetHypoTest();
    //asResult->Print();
    
    cout << "Significance: " << setprecision(15) << z << " sigma" <<endl;
    cout << "p-value: " << setprecision(15) << p << "  " <<endl;
    cout << "Significance: " << setprecision(15) << PValueToSignificance(p) << " sigma" <<endl;


    poi->setVal(xSec[0]);
    poi->setConstant(false); 
    w->loadSnapshot("postfit");
  } 
  

  return ;

  RooWorkspace *w2=new RooWorkspace("w2");
  ModelConfig *altModel=(ModelConfig*)model->Clone("AltModel");
  //RooRealVar *mu_top=(RooRealVar*)w->var("mu_top");
  RooAbsPdf *altpdfmd=altModel->GetPdf();
  mu_top->SetName("mu_ww");
  RooAbsPdf *atlpdf=(RooAbsPdf*)altpdfmd->Clone("altpdfmd");
  altModel->SetPdf(*atlpdf);
  cout<<"New pdf"<<endl;
  altModel->Print("V");
  cout<<"---"<<endl;
  altModel->SetWorkspace(*w2);
  //mu_top->SetName("mu_top");
  w2->renameSet("mu_top","mu_ww");

  //msys->migrad();
  //msys->minos(*model->GetParametersOfInterest()->find("mH"));
  /*

  // set mu_top to 1.2 and create asimov

  RooRealVar *mu_top=(RooRealVar*)w->var("mu_top");
  mu_top->setVal(1.0);

  RooRealVar *mu_ww=(RooRealVar*)w->var("mu_ww");
  mu_ww->setVal(1.2);
  
  RooAbsData *dataTop=AsymptoticCalculator::MakeAsimovData(*model,*model->GetObservables(),*global);
  mu_top->SetName("mu_ww");
  */
  
  RooAbsReal *mNll2= atlpdf->createNLL( *data,
                                        RooFit::Constrain(*model->GetNuisanceParameters()), 
                                        RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                        RooFit::Offset(kTRUE), 
                                        Extended(true),
                                        RooFit::Verbose(kFALSE));
  
  RooMinimizer *msys2= new RooMinimizer(*mNll2);;
  msys2->setEps(0.01);
  msys2->optimizeConst(2);
  msys2->setStrategy(1);
  msys2->minimize("Minuit2");

  return ;

  vector <double> valVars;
  valVars.push_back(0.8); valVars.push_back(0.9); valVars.push_back(1.0); valVars.push_back(1.1); valVars.push_back(1.2);

  vector <double> mu_topVars;
  vector <double> mu_wwVars;
  vector <double> mu_vbfVars;
  vector <double> mu_ZjetsVars;


  vector <double> mu_topVars_e;
  vector <double> mu_wwVars_e;
  vector <double> mu_vbfVars_e;
  vector <double> mu_ZjetsVars_e;
  
  for( int i=0; i<(int) valVars.size(); i++) {
    w->loadSnapshot("prefit");
    
    std::map<string, vector <double>> fitRes=ScanAsimovVar(w,w2,atlpdf, "mu_top",valVars[i],"mu_ww");
    
    mu_vbfVars.push_back(fitRes["mu_vbf"].at(0));
    mu_vbfVars_e.push_back(fitRes["mu_vbf"].at(1));

    mu_wwVars.push_back(fitRes["mu_ww"].at(0));
    mu_wwVars_e.push_back(fitRes["mu_ww"].at(1));
    
  }
  

  cout<<endl;
  cout<<endl;
  for( int i=0; i<(int) valVars.size(); i++) {
    cout<<"Input mu_top "<<valVars[i]<<" output mu_vbf "<<mu_vbfVars[i]<<" +/- " <<mu_vbfVars_e[i]<<endl;
  }
  

  
}

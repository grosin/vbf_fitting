#include "minitree.h"
#include <TCanvas.h>
#include <TH2.h>
#include <vector>
int BbinX=0, BbinY=0, TbinX=0, TbinY=0;
//-------------------------
float split_threshold=1.1; //  10% variation
float margin=10;    //percantage of margin for splitting.
double signoisthresh=0.50; // Sig/Noise
int step_max=7; //
//-------------------------
void BinFrame(double botx, double boty, double topx, double topy, int teps){
    
    
    //Hist bin area delimitation
     BbinX= botx*(pow(2,teps))+1;
     BbinY= boty*(pow(2,teps))+1;
     TbinX= topx*(pow(2,teps));
     TbinY= topy*(pow(2,teps));
}

bool Comparison(double Sig, double Nig, double sig, double nig){
    //checks if the condition of threshold splitting is met
    bool t=0;
    if (Sig>0 && Nig>0 && sig>0 && nig>0){
        double SNig=(double)Sig/Nig;
        double snig=(double)sig/nig;
        //float new_ratio = sig/(nig+sqrt(nig));
        //if(new_ratio>(split_threshold*SNig)) t=1;
	//tif(SNig>(snig+snig*margin/100)) t=1;
   // if(SNig>(snig+snig*margin/100)+nif) t=1;
        t=1;
        cout << " ratio:" << SNig/(snig+snig*margin/100) << endl;
    }
    if (sig==0) t=1;
    return t;
}

double BinCharger(TH2D ua){
    double carica=0;
    //cout <<"bins"<< ua.GetNbinsX() << endl;
    //cout <<"charging from (" << BbinX <<","<< BbinY << ") to (" << TbinX <<"," << TbinY << ")" << endl;
    for (int p=BbinY;p<=TbinY;p++){
        for (int q=BbinX;q<=TbinX;q++) carica+=ua.GetBinContent(q,p);
    }
    return carica;
}

void Fit3B_noloop(){
    //events and noise events
    int nnb=0, nb=0, wb=0;
    // list of trees to consider;
    
    vector <string> ltrees;
    ltrees.push_back("VBF");
    ltrees.push_back("WW");
    ltrees.push_back("GGF");
    ltrees.push_back("Zjets");
    ltrees.push_back("FakeE");
    ltrees.push_back("FakeM");
    ltrees.push_back("Vgamma");
    ltrees.push_back("vh");
    ltrees.push_back("htt");
    
    TFile *f=TFile::Open("merged.root");
    std::vector<TH2D> vbf_ev;
    std::vector<TH2D> WW_ev;
    std::vector<TH2D> ns_ev;
    for(int j=0;j<=step_max;j++){
        TString a("vbf_ev"), b("b"), aa("ns_"), aaa("WW_"), ab("null"), nab("null"), wab("null");
        ostringstream tail;
        int res= pow(4,j);
        tail << res;
        int nbinx=pow(2,j);
        int nbiny=pow(2,j);
        b=tail.str();
        ab=a+b;
        nab=aa+b;
        wab=aaa+b;
        cout << wab << " histo created" << endl;
        TH2D *hk = new TH2D(ab, "histo eventi "+ab, nbinx, 0, 1, nbiny, 0, 1);
        vbf_ev.push_back(*hk);
        TH2D *nk = new TH2D(nab, "histo eventi "+nab, nbinx, 0, 1, nbiny, 0, 1);
        ns_ev.push_back(*nk);
        TH2D *wk = new TH2D(wab, "histo eventi "+wab, nbinx, 0, 1, nbiny, 0, 1);
        WW_ev.push_back(*wk);
        delete hk;
        delete nk;
        delete wk;
    }
    cout <<"wa size: " << WW_ev.size() << endl;
  
    for(std::vector<string>::iterator s=ltrees.begin(); s!=ltrees.end(); s++){
        cout<<"Reading tree "<<*s<<endl;
        TTree *tin=(TTree*)f->Get((*s).c_str());
        minitree *mini=new minitree(tin);
        mini->Init(tin);
        
        Float_t         EventNumber;
        Float_t         MCEventNumber;
        Float_t         METRel_noJets;
        Float_t         METRel_withJets;
        Float_t         TrackMETRel_noJets;
        Float_t         TrackMETRel_withJets;
        Float_t         MET_CST;
        Float_t         MET_TST;
        Float_t         runNumber;
        Float_t         nJetsTight;
        Float_t         Mjj;
        Float_t         nBJetsSubMV2c10;
        Float_t         centralJetVetoLeadpT;
        Float_t         mtt;
        Float_t         mZ;
        Float_t         OLV;
        Float_t         MET;
        Float_t         MT2_1Jet;
        Float_t         nJetsFJVT3030;
        Float_t         Ml0j0;
        Float_t         Ml0j1;
        Float_t         Ml1j0;
        Float_t         Ml1j1;
        Float_t         jet0_pt;
        Float_t         jet1_pt;
        Float_t         jet2_pt;
        Float_t         nJets;
        Float_t         jet0_eta;
        Float_t         jet1_eta;
        Float_t         jet2_eta;
        Float_t         lep0_eta;
        Float_t         lep1_eta;
        Float_t         SEtajj;
        Float_t         DPhijj;
        Float_t         DEtajj;
        Float_t         DYjj;
        Float_t         Mll;
        Float_t         DYll;
        Float_t         DPhill;
        Float_t         MT;
        Float_t         ptTot;
        Float_t         TrackMET;
        Float_t         lep0_pt;
        Float_t         lep1_pt;
        Float_t         lep0_truthType;
        Float_t         lep1_truthType;
        Float_t         lep0_truthOrigin;
        Float_t         lep1_truthOrigin;
        Float_t         lep0_E;
        Float_t         lep1_E;
        Float_t         jet0_E;
        Float_t         jet1_E;
        Float_t         jet2_E;
        Float_t         lep0_phi;
        Float_t         lep1_phi;
        Float_t         jet0_phi;
        Float_t         jet1_phi;
        Float_t         jet2_phi;
        Float_t         lep1_is_m;
        Float_t         lep1_is_e;
        Float_t         lep0_is_m;
        Float_t         lep0_is_e;
        Double_t        weight;
        vector<double>  *bdtMltClass;
        Double_t        bdt_vbf;
        Double_t        bdt_WW;
        Double_t        bdt_ggf;
        Int_t           region;
        
        TBranch        *b_EventNumber;   //!
        TBranch        *b_MCEventNumber;   //!
        TBranch        *b_METRel_noJets;   //!
        TBranch        *b_METRel_withJets;   //!
        TBranch        *b_TrackMETRel_noJets;   //!
        TBranch        *b_TrackMETRel_withJets;   //!
        TBranch        *b_MET_CST;   //!
        TBranch        *b_MET_TST;   //!
        TBranch        *b_runNumber;   //!
        TBranch        *b_nJetsTight;   //!
        TBranch        *b_Mjj;   //!
        TBranch        *b_nBJetsSubMV2c10;   //!
        TBranch        *b_centralJetVetoLeadpT;   //!
        TBranch        *b_mtt;   //!
        TBranch        *b_mZ;   //!
        TBranch        *b_OLV;   //!
        TBranch        *b_MET;   //!
        TBranch        *b_MT2_1Jet;   //!
        TBranch        *b_nJetsFJVT3030;   //!
        TBranch        *b_Ml0j0;   //!
        TBranch        *b_Ml0j1;   //!
        TBranch        *b_Ml1j0;   //!
        TBranch        *b_Ml1j1;   //!
        TBranch        *b_jet0_pt;   //!
        TBranch        *b_jet1_pt;   //!
        TBranch        *b_jet2_pt;   //!
        TBranch        *b_nJets;   //!
        TBranch        *b_jet0_eta;   //!
        TBranch        *b_jet1_eta;   //!
        TBranch        *b_jet2_eta;   //!
        TBranch        *b_lep0_eta;   //!
        TBranch        *b_lep1_eta;   //!
        TBranch        *b_SEtajj;   //!
        TBranch        *b_DPhijj;   //!
        TBranch        *b_DEtajj;   //!
        TBranch        *b_DYjj;   //!
        TBranch        *b_Mll;   //!
        TBranch        *b_DYll;   //!
        TBranch        *b_DPhill;   //!
        TBranch        *b_MT;   //!
        TBranch        *b_ptTot;   //!
        TBranch        *b_TrackMET;   //!
        TBranch        *b_lep0_pt;   //!
        TBranch        *b_lep1_pt;   //!
        TBranch        *b_lep0_truthType;   //!
        TBranch        *b_lep1_truthType;   //!
        TBranch        *b_lep0_truthOrigin;   //!
        TBranch        *b_lep1_truthOrigin;   //!
        TBranch        *b_lep0_E;   //!
        TBranch        *b_lep1_E;   //!
        TBranch        *b_jet0_E;   //!
        TBranch        *b_jet1_E;   //!
        TBranch        *b_jet2_E;   //!
        TBranch        *b_lep0_phi;   //!
        TBranch        *b_lep1_phi;   //!
        TBranch        *b_jet0_phi;   //!
        TBranch        *b_jet1_phi;   //!
        TBranch        *b_jet2_phi;   //!
        TBranch        *b_lep1_is_m;   //!
        TBranch        *b_lep1_is_e;   //!
        TBranch        *b_lep0_is_m;   //!
        TBranch        *b_lep0_is_e;   //!
        TBranch        *b_weight;   //!
        TBranch        *b_bdtMltClass;   //!
        TBranch        *b_bdt_vbf;   //!
        TBranch        *b_bdt_WW;   //!
        TBranch        *b_bdt_ggf;   //!
        TBranch        *b_region;   //!
        
        tin->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
        tin->SetBranchAddress("MCEventNumber", &MCEventNumber, &b_MCEventNumber);
        tin->SetBranchAddress("METRel_noJets", &METRel_noJets, &b_METRel_noJets);
        tin->SetBranchAddress("METRel_withJets", &METRel_withJets, &b_METRel_withJets);
        tin->SetBranchAddress("TrackMETRel_noJets", &TrackMETRel_noJets, &b_TrackMETRel_noJets);
        tin->SetBranchAddress("TrackMETRel_withJets", &TrackMETRel_withJets, &b_TrackMETRel_withJets);
        tin->SetBranchAddress("MET_CST", &MET_CST, &b_MET_CST);
        tin->SetBranchAddress("MET_TST", &MET_TST, &b_MET_TST);
        tin->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
        tin->SetBranchAddress("nJetsTight", &nJetsTight, &b_nJetsTight);
        tin->SetBranchAddress("Mjj", &Mjj, &b_Mjj);
        tin->SetBranchAddress("nBJetsSubMV2c10", &nBJetsSubMV2c10, &b_nBJetsSubMV2c10);
        tin->SetBranchAddress("centralJetVetoLeadpT", &centralJetVetoLeadpT, &b_centralJetVetoLeadpT);
        tin->SetBranchAddress("mtt", &mtt, &b_mtt);
        tin->SetBranchAddress("mZ", &mZ, &b_mZ);
        tin->SetBranchAddress("OLV", &OLV, &b_OLV);
        tin->SetBranchAddress("MET", &MET, &b_MET);
        tin->SetBranchAddress("MT2_1Jet", &MT2_1Jet, &b_MT2_1Jet);
        tin->SetBranchAddress("nJetsFJVT3030", &nJetsFJVT3030, &b_nJetsFJVT3030);
        tin->SetBranchAddress("Ml0j0", &Ml0j0, &b_Ml0j0);
        tin->SetBranchAddress("Ml0j1", &Ml0j1, &b_Ml0j1);
        tin->SetBranchAddress("Ml1j0", &Ml1j0, &b_Ml1j0);
        tin->SetBranchAddress("Ml1j1", &Ml1j1, &b_Ml1j1);
        tin->SetBranchAddress("jet0_pt", &jet0_pt, &b_jet0_pt);
        tin->SetBranchAddress("jet1_pt", &jet1_pt, &b_jet1_pt);
        tin->SetBranchAddress("jet2_pt", &jet2_pt, &b_jet2_pt);
        tin->SetBranchAddress("nJets", &nJets, &b_nJets);
        tin->SetBranchAddress("jet0_eta", &jet0_eta, &b_jet0_eta);
        tin->SetBranchAddress("jet1_eta", &jet1_eta, &b_jet1_eta);
        tin->SetBranchAddress("jet2_eta", &jet2_eta, &b_jet2_eta);
        tin->SetBranchAddress("lep0_eta", &lep0_eta, &b_lep0_eta);
        tin->SetBranchAddress("lep1_eta", &lep1_eta, &b_lep1_eta);
        tin->SetBranchAddress("SEtajj", &SEtajj, &b_SEtajj);
        tin->SetBranchAddress("DPhijj", &DPhijj, &b_DPhijj);
        tin->SetBranchAddress("DEtajj", &DEtajj, &b_DEtajj);
        tin->SetBranchAddress("DYjj", &DYjj, &b_DYjj);
        tin->SetBranchAddress("Mll", &Mll, &b_Mll);
        tin->SetBranchAddress("DYll", &DYll, &b_DYll);
        tin->SetBranchAddress("DPhill", &DPhill, &b_DPhill);
        tin->SetBranchAddress("MT", &MT, &b_MT);
        tin->SetBranchAddress("ptTot", &ptTot, &b_ptTot);
        tin->SetBranchAddress("TrackMET", &TrackMET, &b_TrackMET);
        tin->SetBranchAddress("lep0_pt", &lep0_pt, &b_lep0_pt);
        tin->SetBranchAddress("lep1_pt", &lep1_pt, &b_lep1_pt);
        tin->SetBranchAddress("lep0_truthType", &lep0_truthType, &b_lep0_truthType);
        tin->SetBranchAddress("lep1_truthType", &lep1_truthType, &b_lep1_truthType);
        tin->SetBranchAddress("lep0_truthOrigin", &lep0_truthOrigin, &b_lep0_truthOrigin);
        tin->SetBranchAddress("lep1_truthOrigin", &lep1_truthOrigin, &b_lep1_truthOrigin);
        tin->SetBranchAddress("lep0_E", &lep0_E, &b_lep0_E);
        tin->SetBranchAddress("lep1_E", &lep1_E, &b_lep1_E);
        tin->SetBranchAddress("jet0_E", &jet0_E, &b_jet0_E);
        tin->SetBranchAddress("jet1_E", &jet1_E, &b_jet1_E);
        tin->SetBranchAddress("jet2_E", &jet2_E, &b_jet2_E);
        tin->SetBranchAddress("lep0_phi", &lep0_phi, &b_lep0_phi);
        tin->SetBranchAddress("lep1_phi", &lep1_phi, &b_lep1_phi);
        tin->SetBranchAddress("jet0_phi", &jet0_phi, &b_jet0_phi);
        tin->SetBranchAddress("jet1_phi", &jet1_phi, &b_jet1_phi);
        tin->SetBranchAddress("jet2_phi", &jet2_phi, &b_jet2_phi);
        tin->SetBranchAddress("lep1_is_m", &lep1_is_m, &b_lep1_is_m);
        tin->SetBranchAddress("lep1_is_e", &lep1_is_e, &b_lep1_is_e);
        tin->SetBranchAddress("lep0_is_m", &lep0_is_m, &b_lep0_is_m);
        tin->SetBranchAddress("lep0_is_e", &lep0_is_e, &b_lep0_is_e);
        tin->SetBranchAddress("weight", &weight, &b_weight);
        tin->SetBranchAddress("bdtMltClass", &bdtMltClass, &b_bdtMltClass);
        tin->SetBranchAddress("bdt_vbf", &bdt_vbf, &b_bdt_vbf);
        tin->SetBranchAddress("bdt_WW", &bdt_WW, &b_bdt_WW);
        tin->SetBranchAddress("bdt_ggf", &bdt_ggf, &b_bdt_ggf);
        tin->SetBranchAddress("region", &region, &b_region);
       
       for(long int i=0; i<(int)tin->GetEntries(); i++){
           mini->GetEntry(i);
           if (*s=="VBF"){
              // cout << " i " << i << " " << bdt_vbf << " " << bdt_WW << endl;
               if(region==0) {
                   nb++;
                   for (int j=0;j<step_max;j++) vbf_ev[j].Fill(bdt_vbf,bdt_WW,weight);
               }
            }
           else if (*s=="WW"){
               if(region==0) {
                   wb++;
                   for (int j=0;j<step_max;j++) WW_ev[j].Fill(bdt_vbf,bdt_WW,weight);
               }
           }
               else{
               if(region==0){
                   nnb++;
                   for (int j=0;j<step_max;j++) ns_ev[j].Fill(bdt_vbf,bdt_WW,weight);
               }
           }
       }
    cout << "nb:" << nb << endl;
    cout << "nnb:" << nnb << endl;
    cout << "wb:" << wb << endl;
    }
  //----------------------------------
    
    
    std::vector<double> bx;
    std::vector<double> bX;
    std::vector<double> by;
    std::vector<double> bY;
    std::vector<double> bv;
    std::vector<double> bw;
    std::vector<double> bnv;
    std::vector<int> active;
    std::vector<double> sgf;
    
    for (int y=0;y<=step_max;y++){
        double sgnf=0;
        for (int u=1;u<=pow(2,y);u++){
            for (int v=1;v<=pow(2,y);v++){
                double vws=vbf_ev[y].GetBinContent(u,v)+WW_ev[y].GetBinContent(u,v);
                double bkg=ns_ev[y].GetBinContent(u,v);
                if(vws>0 && bkg>0) sgnf+=pow(vws/sqrt(bkg),2);
            }
        }
        sgf.push_back(sqrt(sgnf));
    }
    
    BinFrame(0,0,0.5,0.5,3);
    cout << "events:" << BinCharger(vbf_ev[3]) << endl;
    cout << "eventz:" << vbf_ev[0].GetBinContent(1,1) << endl;
    
    //initializing
    bv.push_back(vbf_ev[0].GetBinContent(1,1));
    bw.push_back(WW_ev[0].GetBinContent(1,1));
    bnv.push_back(ns_ev[0].GetBinContent(1,1));
    active.push_back(1);
    bx.push_back(0);
    bX.push_back(1);
    by.push_back(0);
    bY.push_back(1);
    
    
    int step_i=1;
    //Splitting in VBF-----------------------
    
        for (step_i=1;step_i<step_max;step_i++){
            cout << "step " << step_i << endl;
            cout << "significance " << sgf[step_i] << endl;
            sgn_new=0;
            for (int u=0;u<bx.size();u++){
                //Peeking new bins
                double midx=(bx[u]+bX[u])/2;
                BinFrame(bx[u],by[u],midx,bY[u],step_i);
                double v_bina=BinCharger(vbf_ev[step_i]);
                double w_bina=BinCharger(WW_ev[step_i]);
                double n_bina=BinCharger(ns_ev[step_i]);
                BinFrame(midx,by[u],bX[u],bY[u],step_i);
                double v_binb=BinCharger(vbf_ev[step_i]);
                double w_binb=BinCharger(WW_ev[step_i]);
                double n_binb=BinCharger(ns_ev[step_i]);
                
                bool split_me=0;
                double  signi=sgf[step_i]-sgf[step_i-1];
                cout<<"iteration "<<step_i<<" significance pre "<<sgf[step_i]<<" significance post "<<sgf[step_i-1]<<" diff "<<signi<<endl;
                if(signi > 0.1 )
                    //split_me =1;
                    if((Comparison(bv[u]+bw[u],bnv[u],v_bina,n_bina)==1) ||(Comparison(bv[u]+bw[u],bnv[u],v_binb,n_binb)==1)) split_me=1;
                cout << "vbf+WW:" << bv[u]+bw[u] << " noise:" << bnv[u] << " split:" << split_me << " active:" << active[u] << endl;
                if((active[u]==0) || (bv[u]==0) || (bnv[u]==0) || (split_me==0) || ((bv[u]+bw[u])/bnv[u])<signoisthresh){
                    float area=((bX[u]-bx[u])*(bY[u]-by[u]))*(pow(2,step_i-1));
                    cout << "homogeneous area: X["<< bx[u] << ";" << bX[u] <<"] Y["<< by[u] << ";" << bY[u] <<"] "<< endl;
                    active[u]=0;
                }
                
                if ((bv[u]>0 && bnv[u]>0 && active[u]==1 && split_me==1 && ((bv[u]+bw[u])/bnv[u])>=signoisthresh)){
                cout << "splitting bin" << endl;
                cout <<"replacing cut X[" << bx[u] << ";" << bX[u] <<"] Y["<< by[u] << ";" << bY[u] <<"] ";
                bx.insert(bx.begin()+u+1,midx);
                bX.insert(bX.begin()+u,midx);
                by.insert(by.begin()+u,by[u]);
                bY.insert(bY.begin()+u,bY[u]);
                cout << "with cuts X[" << bx[u] << ";" << bX[u] << "]  Y["<< by[u] << ";" << bY[u] <<"]  and [" << bx[u+1] << ";" << bX[u+1] << "] Y["<< by[u+1] << ";" << bY[u+1] <<"]" << endl;
                cout <<"BIN:" << bv[u] << " ";
                bv.insert(bv.begin()+u,v_bina);
                bv[u+1]=v_binb;
                bw.insert(bw.begin()+u,w_bina);
                bw[u+1]=w_binb;
                bnv.insert(bnv.begin()+u,n_bina);
                bnv[u+1]=n_binb;
                active.insert(active.begin()+u,1);
                active[u+1]=1;
                cout << "v_bina:" << bv[u] << "  v_binb:" << bv[u+1] << endl;
                u++;
                }
                cout << endl;
            }
            cout << "-------------------------------"<< endl;
        }
    step_i--;
    cout << "step_i:"<< step_i << endl;
    for (int u=0;u<bv.size();u++) active[u]=1; //reactivating bins
    
    //Splitting horizontally--------------------------------------------------------
    for (int step_y=1;step_y<step_i;step_y++){
         cout << "step " << step_y << endl;
        for (int u=0;u<bw.size();u++){
        
        //Peeking
        double midy=(by[u]+bY[u])/2;
        BinFrame(bx[u],by[u],bX[u],midy,step_i);
        double v_bina=BinCharger(vbf_ev[step_i]);
        double w_bina=BinCharger(WW_ev[step_i]);
        double n_bina=BinCharger(ns_ev[step_i]);
        BinFrame(bx[u],midy,bX[u],bY[u],step_i);
        double v_binb=BinCharger(vbf_ev[step_i]);
        double w_binb=BinCharger(WW_ev[step_i]);
        double n_binb=BinCharger(ns_ev[step_i]);
        
        bool split_me=0;
        double   signi=sgf[step_y]-sgf[step_y-1];
            if(signi > 0.1 )
            //    split_me =1;
            //last parameter of comparison function: signi
            if((Comparison(bw[u]+bv[u],bnv[u],w_bina,n_bina)==1) ||(Comparison(bw[u]+bv[u],bnv[u],w_binb,n_binb))==1) split_me=1;
        cout << "WW:" << bw[u] << " noise:" << bnv[u] << endl;
        
        if( (active[u]==0) || (bw[u]<1) || (bnv[u]<1) || (split_me==0) || ((bw[u]+bv[u])/bnv[u])<signoisthresh){
            float area=((bX[u]-bx[u])*(bY[u]-by[u]))*(pow(2,step_i-1));
            cout << "homogeneous area: X["<< bx[u] << ";" << bX[u] <<"] Y["<< by[u] << ";" << bY[u] <<"] " << endl;
            active[u]=0;
        }
        if ((bw[u]>0 && bnv[u]>0 && active[u]==1 && split_me==1 && ((bw[u]+bv[u])/bnv[u])>=signoisthresh)){
        cout << "splitting bin" << endl;
        cout <<"replacing cut X[" << bx[u] << ";" << bX[u] <<"] Y["<< by[u] << ";" << bY[u] <<"] ";
        by.insert(by.begin()+u+1,midy);
        bY.insert(bY.begin()+u,midy);
        bx.insert(bx.begin()+u,bx[u]);
        bX.insert(bX.begin()+u,bX[u]);
        cout << "with cuts X[" << bx[u] << ";" << bX[u] << "]  Y["<< by[u] << ";" << bY[u] <<"]  and [" << bx[u+1] << ";" << bX[u+1] << "] Y["<< by[u+1] << ";" << bY[u+1] <<"]" << endl;
        cout <<"BIN:" << bw[u] << " ";
        bv.insert(bv.begin()+u,v_bina);
        bv[u+1]=v_binb;
        bw.insert(bw.begin()+u,w_bina);
        bw[u+1]=w_binb;
        bnv.insert(bnv.begin()+u,n_bina);
        bnv[u+1]=n_binb;
        active.insert(active.begin()+u,1);
        active[u+1]=1;
        cout << "v_bina:" << bw[u] << "  v_binb:" << bw[u+1] << endl;
        //cout << "noise_a:" << bnv[u] << "noise_b:" << bnv[u+1] << endl;
        u++;
        }
        cout << endl;
    }
        cout <<"WW----------------------" << endl;
    }
    /*
    //---------------------------------
        int no_zero=0;
        for(int u=0; u<bv.size(); u++){
        cout << "X:[" << bx[u] << ";" << bX[u] <<"] " << "Y:[" << by[u] << ";" << bY[u] << "] " << bv[u] << endl;
            if((bv[u]>0)) no_zero++;
        }
        cout << "N cuts:" << bv.size() <<  " No zero:" << no_zero << endl;
        
    cout << endl;
    
    */
    //Histograms creation -----------------------------------------
    
    TH1D *VBFf = new TH1D("VBF_f","VBF events",bv.size(),0,bv.size());
    for(int u=0; u<bv.size(); u++) VBFf->SetBinContent(u+1,bv[u]);
    VBFf->SetFillColor(kRed);
    
    TH1D *WWf = new TH1D("WW_f","WW events",bw.size(),0,bw.size());
    for(int u=0; u<bw.size(); u++) WWf->SetBinContent(u+1,bw[u]);
    WWf->SetFillColor(kGreen);
    
    TH1D *noisef = new TH1D("noise_f","noise events",bnv.size(),0,bnv.size());
    for(int u=0; u<bnv.size(); u++) noisef->SetBinContent(u+1,bnv[u]);
    noisef->SetFillColor(kGray);
    
    THStack *stak =new THStack("all", "Events Binned");
    stak->Add(VBFf);
    stak->Add(WWf);
    stak->Add(noisef);
    
    TLegend *leg =new TLegend(0.1,0.3,0.4,0.8);
    leg->AddEntry(VBFf,"VBF","f");
    leg->AddEntry(WWf,"WW","f");
    leg->AddEntry(noisef,"noise","f");
    
    //---------------------------------------

    TH1D *VBFfsn = new TH1D("VBF_fsn","VBF s/n",bv.size(),0,bv.size());
    for(int u=0; u<bv.size(); u++){
        if(bnv[u]==0){
            if(bv[u]>0) VBFfsn->SetBinContent(u+1,100);
        }
        else
            VBFfsn->SetBinContent(u+1,bv[u]/bnv[u]);
    }
    VBFfsn->SetFillColor(kRed);
    
    TH1D *WWfsn = new TH1D("WW_fsn","WW s/n",bw.size(),0,bw.size());
    for(int u=0; u<bw.size(); u++){
        if(bnv[u]==0){
            if(bw[u]>0) VBFfsn->SetBinContent(u+1,100);
        }
        else WWfsn->SetBinContent(u+1,bw[u]/bnv[u]);
    }
    WWfsn->SetFillColor(kGreen);
    
    THStack *staks =new THStack("all", "S/n Binned");
    staks->Add(VBFfsn);
    staks->Add(WWfsn);
    
    TCanvas *d1 = new TCanvas("d1");
    VBFfsn->Draw();
    TCanvas *d2 = new TCanvas("d2");
    WWfsn->Draw();
    
    TCanvas *c1 = new TCanvas("c1");
    stak->Draw();
    leg->Draw();
    
    std::vector<TBox*> tiles;
    TCanvas * c = new TCanvas("c_ref","grid",200,10,600,600);

    ofstream of;
    of.open("cuts.txt");
    for(int u=0; u<bv.size(); u++){
        cout << bx[u] << " " << bX[u] <<" "  << by[u] << " " << bY[u] << " " << endl;
        if (bv[u]>0 || bnv[u]>0 || bw[u]>0)  of << bx[u] << " " << bX[u] <<" "  << by[u] << " " << bY[u] << "\n";
        TBox *boxxa = new TBox(bx[u],by[u],bX[u],bY[u]);
        boxxa->SetLineColor(kBlack);
        boxxa->SetLineWidth(3);
        tiles.push_back(boxxa);
        boxxa->Draw("l");
        //delete boxxa;
    }
    of.close();
}

        





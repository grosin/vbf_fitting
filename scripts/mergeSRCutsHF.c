#include "tools.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"

std::string remove_extension(const std::string& filename) {
    size_t lastdot = filename.find_last_of(";");
    if (lastdot == std::string::npos) return filename;
    cout<<" removing from "<<filename <<" to "<<filename.substr(0, lastdot)<<endl;
    return filename.substr(0, lastdot); 
}

string eraseSubStr(std::string & mainStr, const std::string & toErase)
{
  string ret=mainStr;
	// Search for the substring in string
	size_t pos = ret.find(toErase);
 
	if (pos != std::string::npos)
	{
		// If found then erase it from string
		ret.erase(pos, toErase.length());
	}
    return ret;
}


void mergeSRCutsHF(string list="list.txt"){
  // fix the out name; 
  TFile *fout=new TFile("SRVBF.root","RECREATE");
  
  vector <string> fnames;
  ifstream file(list.c_str());
  string line="";
  while (getline(file,line)){
    fnames.push_back(line);
  }

  vector <string> types;
  types.push_back("RooHist");
  types.push_back("TH1F");
  types.push_back("RooCurve");

  // define new objects merging the results from all this
  RooHist *h_asimovData=new RooHist(1);
  
  h_asimovData->SetName("h_asimovData");
  vector <string> curveNames;
  curveNames.push_back("h_total_error_band");
  curveNames.push_back("h_rel_error_band");
  std::map<string, RooCurve*> curves;
  for(vector<string>::iterator it=curveNames.begin(); it!=curveNames.end(); it++){
    curves[*it]=new RooCurve();
    curves[*it]->SetName((*it).c_str());
    curves[*it]->SetTitle((*it).c_str());
  }

  
  vector <string> roohistsNames;
  roohistsNames.push_back("h_ratio_excl_sig");
  roohistsNames.push_back("h_ratio");
  roohistsNames.push_back("h_asimovData");
  std::map<string, RooHist*> roohists;
  for(vector<string>::iterator it=roohistsNames.begin(); it!=roohistsNames.end(); it++){
    roohists[*it]=new RooHist(1,1);
    roohists[*it]->SetName((*it).c_str());
    roohists[*it]->SetTitle((*it).c_str());
  }

  /*

  std::map<string, <vector <double>> x;
  std::map<string, <vector <double>> y;
  std::map<string, <vector <double>> x_up;
  std::map<string, <vector <double>> x_low;
  std::map<string, <vector <double>> y_up;
  std::map<string, <vector <double>> y_low;
  */

  vector <string> histsComps;
  histsComps.push_back("SM_total");
  histsComps.push_back("ggf");
  histsComps.push_back("Zjets");
  histsComps.push_back("top");
  histsComps.push_back("diboson");
  histsComps.push_back("vbf");
  histsComps.push_back("Vgamma");

  std::map<string, TH1F*> hists;
  for(vector<string>::iterator it=histsComps.begin(); it!=histsComps.end(); it++){
    hists[*it]= new TH1F((*it).c_str(),(*it).c_str(),fnames.size(),0,fnames.size()+0.5);
  }


  // loop over all objects of the files and fill the correponding elements
  
  for(unsigned int i=0; i<fnames.size(); i++){

    TFile *f=TFile::Open(fnames.at(i).c_str());
    if(f==nullptr) {
      cout<<"File "<<fnames.at(i)<<" is null"<<endl;
      continue;
    }
        
    cout<<"Opening "<<f->GetName()<<endl;
    TKey *keyP=nullptr;
    TIter nextP(f->GetListOfKeys());
    while ((keyP=(TKey*)nextP())) {
      if ( keyP == 0 ) continue;
      if( keyP->GetClassName() == nullptr || keyP->GetName() == nullptr ) continue; 

      cout<<"Analysing "<<keyP->GetClassName() <<" "<<keyP->GetName()<<endl;

      if (strcmp(keyP->GetClassName(),"TH1F")==0) {
        if ( keyP->GetName() == nullptr ) continue; 
        string ttname=remove_extension(keyP->GetName());
        TH1F *h=(TH1F*)f->Get(ttname.c_str());
        hists[ttname.c_str()]->SetBinContent(i+1,h->GetBinContent(1));
        hists[ttname.c_str()]->SetBinError(i+1,h->GetBinError(1));
        delete h;
      }

      if (strcmp(keyP->GetClassName(),"RooCurve")==0) {
        if ( keyP->GetName() == nullptr ) continue; 
        string ttname=remove_extension(keyP->GetName());
        //RooCurve *curve=(RooCurve*)f->Get(ttname.c_str());
        TGraph *curve = (TGraph*)f->Get(ttname.c_str());
        for(Int_t k=0; k<curve->GetN(); k++){
          double px=0; double py=0;
          dynamic_cast <TGraph*> (curve)->GetPoint(k,px,py);
          curves[ttname]->addPoint(i+px,py);
        }
        delete curve; 
      }}
    /*
    if (strcmp(keyP->GetClassName(),"RooHist")==0) {
    if ( keyP->GetName() == nullptr ) continue; 
      string ttname=remove_extension(keyP->GetName());
      //RooHist *curve=(RooHist*)f->Get(ttname.c_str());
      TGraphAsymmErrors *curve = (TGraphAsymmErrors*)f->Get(ttname.c_str());
        // buggy. 
      for(Int_t k=0; k<curve->GetN(); k++){
        double px=0; double py=0;
        dynamic_cast <TGraph*> (curve)->GetPoint(k,px,py);
        dynamic_cast <TGraph*> (curves[ttname])->SetPoint(i,px,py);
        dynamic_cast <TGraphAsymmErrors*> (curves[ttname])->SetPointError(i,i+((TGraphAsymmErrors*)curve)->GetErrorXlow(k),i+((TGraphAsymmErrors*)curve)->GetErrorXhigh(k),
                                                                          ((TGraphAsymmErrors*)curve)->GetErrorYlow(k),i+((TGraphAsymmErrors*)curve)->GetErrorYhigh(k));
      }
      delete curve; 
      }*/
        
       
    f->Close();
  }

  fout->cd();
  for(auto t : hists)
    t.second->Write();
  for(auto t : roohists)
    t.second->Write();
  for ( auto t : curves)
    t.second->Write();
  fout->Close();
  
}

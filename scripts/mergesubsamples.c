#include "tools.h"
#include "TLatex.h"

bool exists(std::map<string, TH1F*> map, const string& name) {
  std::map<string,TH1F*>::const_iterator it = map.find(name);
  return it!=map.end();
}

bool existsV(vector<TH1F*> *hv,string name){
  for(vector<TH1F*>::iterator it=hv->begin(); it!=hv->end(); it++)
    if( string((*it)->GetName()).compare(name) == 0) return true;
  return false;
}

std::string remove_extension(const std::string& filename) {
  size_t lastdot = filename.find_last_of(";");
    if (lastdot == std::string::npos) return filename;
    cout<<" removing from "<<filename <<" to "<<filename.substr(0, lastdot)<<endl;
    return filename.substr(0, lastdot); 
}



void mergesubsamples(string inlist,string obs){
  // input list of files

  // output file
  string filename=string("hist_v2_StefBinning_v21_r21_28_09_2020_v1_noBDTCut_SRL_ggFCRZeroOneJet_ABCD_TopCRinSR_decorr1JetSys_")+obs+".root.exp";
  TFile *fout=new TFile(filename.c_str(),"RECREATE"); 
  ifstream file(inlist);
  string line="";
  int i=1;
  vector <string> files; 

  while ( getline (file,line) ){
    //cout<<"Reading root file  "<<line<<endl;
    files.push_back(line);
  }
  file.close();


  std::map<string, TH1F*> histMap;
  vector <TH1F*> hvec;
  
  for(vector<string>::iterator it=files.begin(); it!=files.end(); it++){
    cout<<"Reading file " <<(*it)<<endl;
    //if( TString( (*it).c_str()).Contains("data_") && !TString( (*it).c_str()).Contains("data_nominal")) continue;
    
    // open the file and loop over all keys
    TFile *f=TFile::Open( (*it).c_str());
    
    TKey *keyP=nullptr;
    TIter nextP(f->GetListOfKeys());
    int nTrees=0;
    vector <string> TH1FNames;
    
    while ((keyP=(TKey*)nextP())) {
      if (strcmp(keyP->GetClassName(),"TH1")) {
	string ttname=remove_extension(keyP->GetName());
	bool isnew=true;
	for(std::vector<string>::iterator it2=TH1FNames.begin(); it2!=TH1FNames.end(); it2++){
	  if( (*it2).compare(ttname)==0) 
	    {   isnew=false; break; } 
	}
	if(isnew){
	  TH1FNames.push_back(ttname);
	}
      }
    }
    // loop over the list of histograms 
    for(vector<string>::iterator it2=TH1FNames.begin(); it2!=TH1FNames.end(); it2++){
      if(i%5000 == 0){
	for(std::map<string, TH1F*>::iterator it= histMap.begin(); it!=histMap.end(); it++){
	  fout->cd();
	  cout<<"Saving "<<(*it).first<<endl;
	  (*it).second->SetName( (*it).first.c_str());
	  (*it).second->Write( TString((*it).first.c_str()).ReplaceAll("_tpm",""));
	  f->cd();
	}

	histMap.clear();
      }
      //std::cout<<"read histogram " <<(*it2)<<" "<<i<<"\n";
      TH1F *h=(TH1F*)f->Get( (*it2).c_str());
      if (h==nullptr) continue; 
      // decide whenever to add this histogram to the list of saved histograms 
      if( !exists(histMap,h->GetName())){
        histMap[h->GetName()] = (TH1F*)h->Clone(Form("%s_tpm",h->GetName()));
        histMap[h->GetName()]->SetDirectory(0);
        //cout<<"Added histogram "<<h->GetName()<<endl;
      }
      else {
        if( !TString(h->GetName()).Contains("Nom") && TString(h->GetName()).Contains("data") ){
          histMap[h->GetName()]->Add( h) ;
          //cout<<"hadded histogram "<<h->GetName()<<" from file "<< (*it)<<endl;
        }
       
      }
      i++;
    }
    f->Close();
  }

  // now save all the histograms
  for(std::map<string, TH1F*>::iterator it= histMap.begin(); it!=histMap.end(); it++){
    fout->cd(); 
    cout<<"Saving "<<(*it).first<<"to "<<fout->GetName()<<endl;
    (*it).second->SetName( (*it).first.c_str());
    (*it).second->Write( TString((*it).first.c_str()).ReplaceAll("_tpm",""));
  }
  fout->Close(); 


}

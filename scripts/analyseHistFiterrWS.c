#include "tools.h"
bool isCounting=false;
void analyseHistFiterrWS(string fname="BkgOnly_combined_NormalMeasurement_model_afterFit_asimovData.root",
                         string wsName="w",
                         string modelConfigName="ModelConfig"){
  TFile *f=TFile::Open(fname.c_str());
  RooWorkspace *w=(RooWorkspace*)f->Get(wsName.c_str());
  w->Print();
  ModelConfig *model=(ModelConfig*)w->obj(modelConfigName.c_str());
  model->Print();
  RooArgSet *obsrvables=(RooArgSet*)model->GetObservables();
  cout<<"Observables"<<endl;
  obsrvables->Print();

  
  std::map<string, RooRealVar*> obsMap;
  TIter it2 = obsrvables->createIterator();
  RooRealVar *t2=nullptr;
  while(t2=(RooRealVar*)it2.Next()){
    obsMap[t2->GetName()]=t2;
  }

  
  RooCategory *channelCat=(RooCategory*)w->cat("channelCat");
  channelCat->Print("v");

  RooAbsData *data=(RooAbsData*)w->data("obsData");
  data->Print();
  RooAbsData *asimov=(RooAbsData*)w->data("asimovData");
  asimov->Print();


  std::map<string, RooRealVar*> poisMap;
  //poisMap["mu_ggf"]=(RooRealVar*)w->var("mu_ggf");
  poisMap["mu_vbf"]=(RooRealVar*)w->var("mu_vbf");
  poisMap["mu_ww"]=(RooRealVar*)w->var("mu_ww");
  poisMap["mu_top"]=(RooRealVar*)w->var("mu_top");
  poisMap["mu_Zjets"]=(RooRealVar*)w->var("mu_Zjets");
  //poisMap["mu_Vgamma"]=(RooRealVar*)w->var("mu_Vgamma");
  
  

  RooArgSet pois;
  for(std::map<string,RooRealVar*>::iterator it=poisMap.begin(); it!=poisMap.end(); it++){
    (*it).second->Print();
    pois.add(*(*it).second);
  }

  cout<<endl;
  cout<<endl;
  // retrive the pdf 
  RooAbsPdf *pdf= model->GetPdf();
  pdf->Print("v");

  //w->allVars().selectByName("*constraint*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("*gamma*")->setAttribAll("Constant",true);

  std::map<string, RooRealVar*> normFactorsMap;
  /*
  normFactorsMap["mu_vbf"]=(RooRealVar*)w->var("VBF_SRVBF_bdt_vbf_epsilon");
  normFactorsMap["mu_top"]=(RooRealVar*)w->var("Top_SRVBF_bdt_vbf_epsilon");
  normFactorsMap["mu_Zjets"]=(RooRealVar*)w->var("Zjets_SRVBF_bdt_vbf_epsilon");

  normFactorsMap["mu_VBF"]->setVal(1.0);
  normFactorsMap["mu_top"]->setVal(1.0);
  normFactorsMap["mu_Zjets"]->setVal(1.0);
  */

  
  RooAbsData *myAsimov=asimov;//AsymptoticCalculator::GenerateAsimovData(*pdf,*obsrvables);
 
  //cout<<"Detailfs for ggF pdf" <<endl;
  //RooProdPdf*  prod=(RooProdPdf*)w->obj("model_SRGGF_Mjj");
  //prod->Print("v");
  
  //RooAbsPdf *pdf1=(RooAbsPdf*)w->obj("SRGGF_Mjj_model");
  //pdf1->Print("v");


  // fix WW and top to nominal; 
  poisMap["mu_ww"]->setConstant(false);
  poisMap["mu_top"]->setConstant(false);
  poisMap["mu_Zjets"]->setConstant(false);
  //poisMap["mu_ggf"]->setConstant(true);
  poisMap["mu_vbf"]->setConstant(false);


  // Here run the nominal fit
  
  RooAbsReal *NllNom = pdf->createNLL(*myAsimov);//
                                      //Constrain(*model->GetNuisanceParameters()));
                                      
   RooMinimizer *minimNom= new RooMinimizer(*NllNom);              ////OffSet true default
   minimNom->setEps(0.2);
   minimNom->setStrategy(2);
   minimNom->minimize("Minuit2");
   //minimNom->minos(*poisMap["mu_vbf"]);

   model->SetSnapshot(*poisMap["mu_vbf"]);
   
     // get the modelConfig (S+B) out of the file
     // and create the B model from the S+B model
   ModelConfig * bModel = (ModelConfig*) model->Clone();
   bModel->SetName("BModel");      
   poisMap["mu_vbf"]->setVal(0);
   bModel->SetSnapshot( *poisMap["mu_vbf"]  );
   
   // create the AsymptoticCalculator from data,alt model, null model
   //AsymptoticCalculator  ac(*myAsimov, *model, *bModel);
  //ac.SetOneSidedDiscovery(true);  // for one-side discovery test
  //ac.SetPrintLevel(-1);  // to suppress print level 
   ProfileLikelihoodCalculator ac(*myAsimov,*model->GetPdf(),*poisMap["mu_vbf"]);

  
  // run the calculator
  HypoTestResult * asResult = ac.GetHypoTest();
  asResult->Print();

  
  return ;
   
  // map of parameter fixed vs error
  std::map<string,double> errFix;
  
  for(std::map<string,RooRealVar*>::iterator it=poisMap.begin(); it!=poisMap.end(); it++){
    cout<<"Letting free "<<(*it).second->GetName()<<endl;
    //fix parameters
    (*it).second->setRange(0.,5.);
    (*it).second->setConstant(false);
    
    RooAbsReal *Nll2 = pdf->createNLL(*myAsimov);
    RooMinimizer *minim2 = new RooMinimizer(*Nll2);              ////OffSet true default
    minim2->setEps(0.01);
    minim2->setStrategy(2);
    minim2->minimize("Minuit2");
    
    errFix[(*it).first]=(*it).second->getError();

    delete minim2; 
    delete Nll2;
    (*it).second->setConstant(true);
    (*it).second->setVal(1.0);
  }

  cout<<"Parameter free & Parameter errror "<<endl;
  for(std::map<string,double>::iterator it=errFix.begin(); it!=errFix.end(); it++){
    cout<<(*it).first<<" "<<(*it).second<<endl;
  }

  return ;

  std::map<string,string> catObsMaps;
  if(isCounting){
    catObsMaps["obs_x_CRTop_cuts"]="CRTop_cuts";
    catObsMaps["obs_x_CRZjets_cuts"]="CRZjets_cuts";
    catObsMaps["obs_x_SRVBF_cuts"]="SRVBF_cuts";
    catObsMaps["obs_x_SRWW_cuts"]="SRWW_cuts";
    catObsMaps["obs_x_SRGGF_cuts"]="SRGGF_cuts";
    
  }
  else {
    /*
    catObsMaps["obs_x_CRTop_DYjj"]="CRTop_DYjj";
    catObsMaps["obs_x_CRZjets_MT"]="CRZjets_MT";
    catObsMaps["obs_x_SRVBF_Mjj"]="SRVBF_Mjj";
    catObsMaps["obs_x_SRWW_Mjj"]="SRWW_Mjj";
    catObsMaps["obs_x_SRGGF_Mjj"]="SRGGF_Mjj";
    */
    
    catObsMaps["obs_x_CRZjets_MT"]="CRZjets_MT";
    catObsMaps["obs_x_SRVBF_bdt_vbf"]="SRVBF_bdt_vbf";
    catObsMaps["obs_x_CRTop_DYjj"]="CRTop_DYjj";
    
  }


  std::map<string,string> catObsPoi;
  if(isCounting){
    catObsPoi["obs_x_CRTop_cuts"]="mu_top";
    catObsPoi["obs_x_CRZjets_cuts"]="mu_Zjets";
    catObsPoi["obs_x_SRVBF_cuts"]="mu_VBF";
    catObsPoi["obs_x_SRWW_cuts"]="mu_WW";
    catObsPoi["obs_x_SRGGF_cuts"]="mu_GGF";
  }
  else{
    /*
    catObsPoi["obs_x_CRTop_DYjj"]="mu_top";
    catObsPoi["obs_x_CRZjets_MT"]="mu_Zjets";
    catObsPoi["obs_x_SRVBF_Mjj"]="mu_VBF";
    catObsPoi["obs_x_SRWW_Mjj"]="mu_WW";
    catObsPoi["obs_x_SRGGF_Mjj"]="mu_GGF";
    */

    catObsPoi["obs_x_CRZjets_MT"]="mu_Zjets";
    catObsPoi["obs_x_SRVBF_bdt_vbf"]="mu_VBF";
    catObsPoi["obs_x_CRTop_DYjj"]="mu_top";
  }
    
  

  for(std::map<string,RooRealVar*>::iterator it=poisMap.begin(); it!=poisMap.end(); it++){
    (*it).second->setConstant(true);
    (*it).second->setVal(1.0);
    (*it).second->setError(0.0);
  }

  // Loop over each observable and create and independent fit
  std::map<string,RooAbsData*> asimovMap;
  std::map<string,std::map<string,double>> fitCentrMap;
  std::map<string,std::map<string,double>> fitErrMap;
  

  

  for(map<string, RooRealVar*>::iterator it=obsMap.begin(); it!=obsMap.end(); it++){
    if( (*it).first.compare("channelCat")==0 || (*it).first.compare("weightVar")==0) continue;
    
    for(std::map<string,RooRealVar*>::iterator it=poisMap.begin(); it!=poisMap.end(); it++){
      (*it).second->setConstant(true);
      (*it).second->setVal(1.0);
      (*it).second->setError(0.0);
    }


    
    poisMap[catObsPoi[(*it).first]]->setVal(1.0);
    poisMap[catObsPoi[(*it).first]]->setConstant(false);
    

    RooArgSet fitObs;
    fitObs.add(*(*it).second);

    cout<<"Generating asimov for "<<(*it).first<<endl;
    cout<<"Channel pdf "<<endl;
    RooAbsPdf* chpdf=((RooSimultaneous*)pdf)->getPdf(catObsMaps[(*it).first].c_str());
    chpdf->Print();
    
    
    RooAbsData *myAsimov_tmp=AsymptoticCalculator::GenerateAsimovData(*((RooSimultaneous*)pdf)->getPdf(catObsMaps[(*it).first].c_str()),fitObs);
    asimovMap[(*it).first]=myAsimov_tmp;

    cout<<myAsimov_tmp->numEntries()<<" and sum Entries "<<myAsimov_tmp->sumEntries()<<endl;

    RooAbsReal *Nlltmp = ((RooSimultaneous*)pdf)->getPdf(catObsMaps[(*it).first].c_str())->createNLL(*myAsimov_tmp);
    RooMinimizer *minimtmp = new RooMinimizer(*Nlltmp);              ////OffSet true default
    //minim->setPrintLevel(0);
    minimtmp->setEps(0.01);
    //minim->optimizeConst(2);
    //minim->setStrategy(2);
    minimtmp->minimize("Minuit2");
    
    for(std::map<string,RooRealVar*>::iterator it2=poisMap.begin(); it2!=poisMap.end(); it2++){
      fitCentrMap[(*it).first][(*it2).first]=(*it2).second->getVal();
      fitErrMap[(*it).first][(*it2).first]=(*it2).second->getError();
    }

    for(std::map<string,RooRealVar*>::iterator it=poisMap.begin(); it!=poisMap.end(); it++){
      (*it).second->setConstant(true);
      (*it).second->setVal(1.0);
      (*it).second->setError(0.0);
    }
    delete minimtmp;
    delete Nlltmp;
  }

  
  for(std::map<string,RooRealVar*>::iterator it=poisMap.begin(); it!=poisMap.end(); it++)
    (*it).second->setConstant(false);

  RooAbsReal *Nll = pdf->createNLL(*myAsimov);
  //RooAbsReal *Nll = pdf->createNLL(*asimov);
  RooMinimizer *minim = new RooMinimizer(*Nll);              ////OffSet true default
  //minim->setPrintLevel(0);
  minim->setEps(0.01);
  //minim->optimizeConst(2);
  //minim->setStrategy(2);
  minim->optimizeConst(2);
  minim->setStrategy(2);
  minim->minimize("Minuit2");
  
  /*
  cout<<"Parameter free & Parameter errror "<<endl;
  for(std::map<string,double>::iterator it=errFix.begin(); it!=errFix.end(); it++){
    cout<<(*it).first<<" "<<(*it).second<<endl;
  }
  */

  // Print here the the fit values of the sets;
  cout<<"Channel  ";
  for(std::map<string,RooRealVar*>::iterator it2=poisMap.begin(); it2!=poisMap.end(); it2++){
    cout<<"& "<<(*it2).first;
  }
  cout<<endl;
  for(map<string, RooRealVar*>::iterator it=obsMap.begin(); it!=obsMap.end(); it++){
    if( (*it).first.compare("channelCat")==0 || (*it).first.compare("weightVar")==0) continue;
    cout<<catObsMaps[(*it).first];
    for(std::map<string,RooRealVar*>::iterator it2=poisMap.begin(); it2!=poisMap.end(); it2++){
      //cout<<std::setprecision(3)<<" & "<<fitCentrMap[(*it).first][(*it2).first]<<" \\pm "<<fitErrMap[(*it).first][(*it2).first];
      cout<<std::setprecision(3)<<" & "<<fitErrMap[(*it).first][(*it2).first];
    }
    cout<<endl;
  }
  cout<<endl;

  return;

  RooPlot *plot_ggF=obsMap[Form("%s%s","obs_x_SRGGF_", (string(isCounting ? "cuts":"Mjj")).c_str())]->frame();
  myAsimov->plotOn(plot_ggF,DataError(RooAbsData::SumW2),Cut(Form("%s%s","channelCat==channelCat::SRGGF_",(string(isCounting ? "cuts":"Mjj")).c_str())));
  /*
  pdf->plotOn(plot_ggF,Slice(*channelCat,"SRGGF_Mjj"),Name("All"),ProjWData(*channelCat,*myAsimov),Components("*Zjets*,*Top*,*WW*,*VBF*,*GGF*"),LineColor(kBlack),FillStyle(1),VLines());
  pdf->plotOn(plot_ggF,Slice(*channelCat,"SRGGF_Mjj"),Name("GGF"),ProjWData(*channelCat,*myAsimov),Components("*Zjets*,*Top*,*WW*,*VBF*"),FillColor(kRed),FillStyle(1),VLines(),DrawOption("F"));
  pdf->plotOn(plot_ggF,Slice(*channelCat,"SRGGF_Mjj"),Name("VBF"),ProjWData(*channelCat,*myAsimov),Components("*Zjets*,*Top*,*WW*"),FillColor(kBlue),FillStyle(1),VLines(),DrawOption("F"));
  pdf->plotOn(plot_ggF,Slice(*channelCat,"SRGGF_Mjj"),Name("WW"),ProjWData(*channelCat,*myAsimov),Components("*Zjets*,*Top*,"),FillColor(kOrange),FillStyle(1),VLines(),DrawOption("F"));
  pdf->plotOn(plot_ggF,Slice(*channelCat,"SRGGF_Mjj"),Name("Top"),ProjWData(*channelCat,*myAsimov),Components("*Zjets*"),FillColor(kMagenta),FillStyle(1),VLines(),DrawOption("F"));
  */
  
  pdf->plotOn(plot_ggF,Slice(*channelCat,Form("%s%s","SRGGF_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),LineColor(kBlack));
  pdf->plotOn(plot_ggF,Slice(*channelCat,Form("%s%s","SRGGF_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*GGF*"),LineColor(kRed));
  pdf->plotOn(plot_ggF,Slice(*channelCat,Form("%s%s","SRGGF_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*VBF*"),LineColor(kBlue));
  pdf->plotOn(plot_ggF,Slice(*channelCat,Form("%s%s","SRGGF_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*WW*"),LineColor(kOrange)); 
  pdf->plotOn(plot_ggF,Slice(*channelCat,Form("%s%s","SRGGF_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*Top*"),LineColor(kGreen));
  pdf->plotOn(plot_ggF,Slice(*channelCat,Form("%s%s","SRGGF_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*Zjets*"),LineColor(kMagenta));
  
  
  TCanvas *cc=new TCanvas("cc","");{
    SetCanvasDefaults(cc);
    plot_ggF->Draw();
  }


  RooPlot *plot_VBF=obsMap[Form("%s%s","obs_x_SRVBF_",(string(isCounting ? "cuts":"Mjj")).c_str())]->frame();
  myAsimov->plotOn(plot_VBF,DataError(RooAbsData::SumW2),Cut(Form("%s%s","channelCat==channelCat::SRVBF_",(string(isCounting ? "cuts":"Mjj")).c_str())));
  pdf->plotOn(plot_VBF,Slice(*channelCat,Form("%s%s","SRVBF_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),LineColor(kBlack));
  pdf->plotOn(plot_VBF,Slice(*channelCat,Form("%s%s","SRVBF_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*GGF*"),LineColor(kRed));
  pdf->plotOn(plot_VBF,Slice(*channelCat,Form("%s%s","SRVBF_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*VBF*"),LineColor(kBlue));
  pdf->plotOn(plot_VBF,Slice(*channelCat,Form("%s%s","SRVBF_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*WW*"),LineColor(kOrange)); 
  pdf->plotOn(plot_VBF,Slice(*channelCat,Form("%s%s","SRVBF_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*Top*"),LineColor(kGreen));
  pdf->plotOn(plot_VBF,Slice(*channelCat,Form("%s%s","SRVBF_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*Zjets*"),LineColor(kMagenta));

  
  TCanvas *cc2=new TCanvas("cc2","");{
    SetCanvasDefaults(cc2);
    plot_VBF->Draw();
  }



  
  RooPlot *plot_WW=obsMap[Form("%s%s","obs_x_SRWW_",(string(isCounting ? "cuts":"Mjj")).c_str())]->frame();
  myAsimov->plotOn(plot_WW,DataError(RooAbsData::SumW2),Cut(Form("%s%s","channelCat==channelCat::SRWW_",(string(isCounting ? "cuts":"Mjj")).c_str())));
  pdf->plotOn(plot_WW,Slice(*channelCat,Form("%s%s","SRWW_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),LineColor(kBlack));
  pdf->plotOn(plot_WW,Slice(*channelCat,Form("%s%s","SRWW_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*GGF*"),LineColor(kRed));
  pdf->plotOn(plot_WW,Slice(*channelCat,Form("%s%s","SRWW_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*VBF*"),LineColor(kBlue));
  pdf->plotOn(plot_WW,Slice(*channelCat,Form("%s%s","SRWW_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*WW*"),LineColor(kOrange)); 
  pdf->plotOn(plot_WW,Slice(*channelCat,Form("%s%s","SRWW_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*Top*"),LineColor(kGreen));
  pdf->plotOn(plot_WW,Slice(*channelCat,Form("%s%s","SRWW_",(string(isCounting ? "cuts":"Mjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*Zjets*"),LineColor(kMagenta));
  
  TCanvas *cc3=new TCanvas("cc3","");{
    SetCanvasDefaults(cc3);
    plot_WW->Draw();
  }




  RooPlot *plot_Zjets=obsMap[Form("%s%s","obs_x_CRZjets_",(string(isCounting ? "cuts":"MT")).c_str())]->frame();
  myAsimov->plotOn(plot_Zjets,DataError(RooAbsData::SumW2),Cut(Form("%s%s","channelCat==channelCat::CRZjets_",(string(isCounting ? "cuts":"MT")).c_str())));
  pdf->plotOn(plot_Zjets,Slice(*channelCat,Form("%s%s","CRZjets_",(string(isCounting ? "cuts":"MT")).c_str())),ProjWData(*channelCat,*myAsimov),LineColor(kBlack));
  pdf->plotOn(plot_Zjets,Slice(*channelCat,Form("%s%s","CRZjets_",(string(isCounting ? "cuts":"MT")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*GGF*"),LineColor(kRed));
  pdf->plotOn(plot_Zjets,Slice(*channelCat,Form("%s%s","CRZjets_",(string(isCounting ? "cuts":"MT")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*VBF*"),LineColor(kBlue));
  pdf->plotOn(plot_Zjets,Slice(*channelCat,Form("%s%s","CRZjets_",(string(isCounting ? "cuts":"MT")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*WW*"),LineColor(kOrange)); 
  pdf->plotOn(plot_Zjets,Slice(*channelCat,Form("%s%s","CRZjets_",(string(isCounting ? "cuts":"MT")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*Top*"),LineColor(kGreen));
  pdf->plotOn(plot_Zjets,Slice(*channelCat,Form("%s%s","CRZjets_",(string(isCounting ? "cuts":"MT")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*Zjets*"),LineColor(kMagenta));
  
  TCanvas *cc4=new TCanvas("cc4","");{
    SetCanvasDefaults(cc4);
    plot_Zjets->Draw();
  }
  


  RooPlot *plot_Top=obsMap[Form("%s%s","obs_x_CRTop_",(string(isCounting ? "cuts":"DYjj")).c_str())]->frame();
  myAsimov->plotOn(plot_Top,DataError(RooAbsData::SumW2),Cut(Form("%s%s","channelCat==channelCat::CRTop_",(string(isCounting ? "cuts":"DYjj")).c_str())));
  pdf->plotOn(plot_Top,Slice(*channelCat,Form("%s%s","CRTop_",(string(isCounting ? "cuts":"DYjj")).c_str())),ProjWData(*channelCat,*myAsimov),LineColor(kBlack));
  pdf->plotOn(plot_Top,Slice(*channelCat,Form("%s%s","CRTop_",(string(isCounting ? "cuts":"DYjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*GGF*"),LineColor(kRed));
  pdf->plotOn(plot_Top,Slice(*channelCat,Form("%s%s","CRTop_",(string(isCounting ? "cuts":"DYjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*VBF*"),LineColor(kBlue));
  pdf->plotOn(plot_Top,Slice(*channelCat,Form("%s%s","CRTop_",(string(isCounting ? "cuts":"DYjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*WW*"),LineColor(kOrange)); 
  pdf->plotOn(plot_Top,Slice(*channelCat,Form("%s%s","CRTop_",(string(isCounting ? "cuts":"DYjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*Top*"),LineColor(kGreen));
  pdf->plotOn(plot_Top,Slice(*channelCat,Form("%s%s","CRTop_",(string(isCounting ? "cuts":"DYjj")).c_str())),ProjWData(*channelCat,*myAsimov),Components("*Zjets*"),LineColor(kMagenta));
  
  TCanvas *cc5=new TCanvas("cc5","");{
    SetCanvasDefaults(cc5);
    plot_Top->Draw();
  }



}

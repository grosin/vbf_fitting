#include "tools.h"

using namespace std;

vector <string> GetTreeNames(string treeName=""){
 vector <string> treeNames;
 if(treeName.size()==0){
   treeNames.push_back("Cut2Jet_v2_Zjets");
   treeNames.push_back("Cut2Jet_v2_top");
   treeNames.push_back("Cut2Jet_v2_diboson");
   treeNames.push_back("Cut2Jet_v2_ggf");
   treeNames.push_back("Cut2Jet_v2_vbf");
   treeNames.push_back("Cut2Jet_v2_Vgamma");
   ////treeNames.push_back("FakeE");
   ////treeNames.push_back("FakeM");
   ////treeNames.push_back("Vgamma");
   treeNames.push_back("Cut2Jet_v2_vh");
   //treeNames.push_back("data");
 }

  else {
    std::istringstream iss(treeName);  
    std::string s;                                                               
    while(getline(iss, s, ','))                                                  
      {                                                                            
        treeNames.push_back(s);                                                           
      }
  }
  return treeNames;
  
}



void add_empty_branches(string infile="",string var="sysVars.txt"){
  if (infile.size()==0 ) { cout <<"empty filename"<<endl; return; }

  TFile *f=new TFile(infile.c_str(),"UPDATE");
  vector <string> treeNames=GetTreeNames("");
  // sample, region, nominal, up, down
  std::map<string, std::map<string, double>> nom;
  // systematic sample, region, up, down
  std::map<string,std::map<string, std::map<string, double>>> up;
  std::map<string,std::map<string, std::map<string, double>>> down;
  
  vector <string> variations;
  ifstream file(var.c_str());
  //file.open();
  string line="";
  
  while ( getline (file,line) ){
    variations.push_back(line);
  }
  file.close();

  vector <string> missingTrees_nom;
  vector <string> missingTrees_var; 
  vector <string> ExistingTrees_nom;
  vector <string> ExistingTrees_var;

    // loop components
  for(std::vector<string>::iterator s=treeNames.begin(); s!=treeNames.end(); s++){

      TTree *tnom =(TTree*)f->Get(((*s)+"_nominal").c_str());
      if(tnom==nullptr){ cout<<"nominal not found for "<<tnom<<endl; missingTrees_nom.push_back(((*s)+"_nominal"));  continue;}
      else{ExistingTrees_nom.push_back(((*s)+"_nominal"));}
      for(vector<string>::iterator vars=variations.begin(); vars!=variations.end(); vars++){
        TTree *tup=(TTree*)f->Get(((*s)+"_"+(*vars)+"__1up").c_str());
        if(tup==nullptr){ cout<<"Missing tree "<<endl; missingTrees_var.push_back(((*s)+"_"+(*vars)+"__1up")); continue; } 
        delete tup;
        TTree *tdown=(TTree*)f->Get(((*s)+"_"+(*vars)+"__1down").c_str());
        if(tdown==nullptr){ cout<<"Missing tree "<<endl; missingTrees_var.push_back(((*s)+"_"+(*vars)+"__1down")); continue; } 
	else{ExistingTrees_var.push_back(((*s)+"_"+(*vars)+"__1down"));}
     	delete tdown;
	}
  }
  cout << (missingTrees_var).size() << endl;
  TTree *nom_exist=(TTree*)f->Get(ExistingTrees_nom[0].c_str());
  TTree *var_exist=(TTree*)f->Get(ExistingTrees_var[0].c_str());
//  TFile *fout=new TFile(outfile.c_str(),"RECREATE");
  for(std::vector<string>::iterator it=missingTrees_nom.begin(); it!=missingTrees_nom.end(); it++){
//  cout<<(*it)<<endl;
    TTree *tree=nom_exist->CopyTree("0==1");
    tree->SetName((*it).c_str());
  }
  for(std::vector<string>::iterator it=missingTrees_var.begin(); it!=missingTrees_var.end();it++){
    TTree *tree=var_exist->CopyTree("0==1");
    tree->SetName((*it).c_str());
    cout << *it << endl;
  } 
  f->Write();
  f->Close();   
}

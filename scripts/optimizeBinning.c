#include "minitree.h"
#include <TCanvas.h>
#include <TH2.h>
#include <vector>
int GbinX=0, GbinY=0;
//-------------------------
float split_threshold=1.0; //  10% variation
double signoisthresh=0.5; // Sig/Noise
int step_max=16,steps=16;
//-------------------------
bool Comparison(int Sig, int Nig, int sig, int nig){
  bool t=0;
  if (Sig>0 && Nig>0 && sig>0 && nig>0){
    float SNig=(float)Sig/Nig;
    float snig=(float)sig/nig;
    float rumenta = (float)sig/((float)nig+sqrt((float)nig));
    if(rumenta>(split_threshold*SNig)) t=1;
    cout << "ratio:" << (rumenta/SNig) << endl;
  }
  if (sig==0) t=1;
  return t;
}

void Splitter(int limx, int limy, int teps, int binID){
    
    
    //Hist bin pinpointing
    double xmax=limx, ymax=limy, xmin=0, ymin=0;
    bool even=1;
    for (int i=1; i<=teps;i++){
        even=!even;
        int tID=binID >> (teps-i);
        bool s= tID&1;
        //cout << s;
        int condition=s+2*even;
        switch (condition)
        {
            case 0:
                xmax=(xmax+xmin)/2;
                break;
            case 1:
                xmin=(xmax+xmin)/2;
                break;
            case 2:
                ymax=(ymax+ymin)/2;
                break;
            case 3:
                ymin=(ymax+ymin)/2;
                break;
        }
    }
    GbinX=(int)xmax;
    GbinY=(int)ymax;
}


void optimizeBinning(string fname="merged.root"){
    //events and noise events
    int nnb=0, nb=0, wb=0;
    // list of trees to consider;
    
    vector <string> ltrees;
    ltrees.push_back("VBF");
    ltrees.push_back("WW");
    ltrees.push_back("GGF");
    
    ltrees.push_back("Zjets");
    //ltrees.push_back("FakeE");
    //ltrees.push_back("FakeM");
    ltrees.push_back("Vgamma");
    ltrees.push_back("vh");
    //ltrees.push_back("htt");
    
    
    TFile *f=TFile::Open(fname.c_str());
    /*
    TH2D *h4 = new TH2D("h4", "histo eventi", 2, 0, 1, 2, 0, 1);
    TH2D *h8 = new TH2D("h8", "histo eventi", 4, 0, 1, 2, 0, 1);
    TH2D *h16 = new TH2D("h16", "histo eventi", 4, 0, 1, 4, 0, 1);
    TH2D *h32 = new TH2D("h32", "histo eventi", 8, 0, 1, 4, 0, 1);
    TH2D *h64 = new TH2D("h64", "histo eventi", 8, 0, 1, 8, 0, 1);
    TH2D *h128 = new TH2D("h128", "histo eventi", 16, 0, 1, 8, 0, 1);
    TH2D *h256 = new TH2D("h256", "histo eventi", 16, 0, 1, 16, 0, 1);
    */
    std::vector<TH2D> ha;
    std::vector<TH2D> wa;
    std::vector<TH2D> na;
    for(int j=0;j<=steps;j++){
        TString a("h"), b("b"), aa("n"), aaa("w"), ab("null"), nab("null"), wab("null");
        ostringstream tail;
        int res= pow(2,j);
        tail << res;
        int nbinx=pow(2,((j+1)/2));
        int nbiny=pow(2,(j/2));
        b=tail.str();
        ab=a+b;
        nab=aa+b;
        wab=aaa+b;
        TH2D *hk = new TH2D(ab, "histo eventi "+ab, nbinx, 0, 1, nbiny, 0, 1);
        ha.push_back(*hk);
        TH2D *nk = new TH2D(nab, "histo eventi "+nab, nbinx, 0, 1, nbiny, 0, 1);
        na.push_back(*nk);
        TH2D *wk = new TH2D(wab, "histo eventi "+wab, nbinx, 0, 1, nbiny, 0, 1);
        wa.push_back(*wk);
        delete hk;
        delete nk;
        delete wk;
    }
    cout <<"wa size: " << wa.size() << endl;
  
    for(std::vector<string>::iterator s=ltrees.begin(); s!=ltrees.end(); s++){
        cout<<"Reading tree "<<*s<<endl;
        TTree *tin=(TTree*)f->Get((*s).c_str());
        minitree *mini=new minitree(tin);
        mini->Init(tin);
        
        Float_t         EventNumber;
        Float_t         MCEventNumber;
        Float_t         METRel_noJets;
        Float_t         METRel_withJets;
        Float_t         TrackMETRel_noJets;
        Float_t         TrackMETRel_withJets;
        Float_t         MET_CST;
        Float_t         MET_TST;
        Float_t         runNumber;
        Float_t         nJetsTight;
        Float_t         Mjj;
        Float_t         nBJetsSubMV2c10;
        Float_t         centralJetVetoLeadpT;
        Float_t         mtt;
        Float_t         mZ;
        Float_t         OLV;
        Float_t         MET;
        Float_t         MT2_1Jet;
        Float_t         nJetsFJVT3030;
        Float_t         Ml0j0;
        Float_t         Ml0j1;
        Float_t         Ml1j0;
        Float_t         Ml1j1;
        Float_t         jet0_pt;
        Float_t         jet1_pt;
        Float_t         jet2_pt;
        Float_t         nJets;
        Float_t         jet0_eta;
        Float_t         jet1_eta;
        Float_t         jet2_eta;
        Float_t         lep0_eta;
        Float_t         lep1_eta;
        Float_t         SEtajj;
        Float_t         DPhijj;
        Float_t         DEtajj;
        Float_t         DYjj;
        Float_t         Mll;
        Float_t         DYll;
        Float_t         DPhill;
        Float_t         MT;
        Float_t         ptTot;
        Float_t         TrackMET;
        Float_t         lep0_pt;
        Float_t         lep1_pt;
        Float_t         lep0_truthType;
        Float_t         lep1_truthType;
        Float_t         lep0_truthOrigin;
        Float_t         lep1_truthOrigin;
        Float_t         lep0_E;
        Float_t         lep1_E;
        Float_t         jet0_E;
        Float_t         jet1_E;
        Float_t         jet2_E;
        Float_t         lep0_phi;
        Float_t         lep1_phi;
        Float_t         jet0_phi;
        Float_t         jet1_phi;
        Float_t         jet2_phi;
        Float_t         lep1_is_m;
        Float_t         lep1_is_e;
        Float_t         lep0_is_m;
        Float_t         lep0_is_e;
        Double_t        weight;
        vector<double>  *bdtMltClass;
        Double_t        bdt_vbf;
        Double_t        bdt_WW;
        Double_t        bdt_ggf;
        Int_t           region;
        /*
        TBranch        *b_EventNumber;   //!
        TBranch        *b_MCEventNumber;   //!
        TBranch        *b_METRel_noJets;   //!
        TBranch        *b_METRel_withJets;   //!
        TBranch        *b_TrackMETRel_noJets;   //!
        TBranch        *b_TrackMETRel_withJets;   //!
        TBranch        *b_MET_CST;   //!
        TBranch        *b_MET_TST;   //!
        TBranch        *b_runNumber;   //!
        TBranch        *b_nJetsTight;   //!
        TBranch        *b_Mjj;   //!
        TBranch        *b_nBJetsSubMV2c10;   //!
        TBranch        *b_centralJetVetoLeadpT;   //!
        TBranch        *b_mtt;   //!
        TBranch        *b_mZ;   //!
        TBranch        *b_OLV;   //!
        TBranch        *b_MET;   //!
        TBranch        *b_MT2_1Jet;   //!
        TBranch        *b_nJetsFJVT3030;   //!
        TBranch        *b_Ml0j0;   //!
        TBranch        *b_Ml0j1;   //!
        TBranch        *b_Ml1j0;   //!
        TBranch        *b_Ml1j1;   //!
        TBranch        *b_jet0_pt;   //!
        TBranch        *b_jet1_pt;   //!
        TBranch        *b_jet2_pt;   //!
        TBranch        *b_nJets;   //!
        TBranch        *b_jet0_eta;   //!
        TBranch        *b_jet1_eta;   //!
        TBranch        *b_jet2_eta;   //!
        TBranch        *b_lep0_eta;   //!
        TBranch        *b_lep1_eta;   //!
        TBranch        *b_SEtajj;   //!
        TBranch        *b_DPhijj;   //!
        TBranch        *b_DEtajj;   //!
        TBranch        *b_DYjj;   //!
        TBranch        *b_Mll;   //!
        TBranch        *b_DYll;   //!
        TBranch        *b_DPhill;   //!
        TBranch        *b_MT;   //!
        TBranch        *b_ptTot;   //!
        TBranch        *b_TrackMET;   //!
        TBranch        *b_lep0_pt;   //!
        TBranch        *b_lep1_pt;   //!
        TBranch        *b_lep0_truthType;   //!
        TBranch        *b_lep1_truthType;   //!
        TBranch        *b_lep0_truthOrigin;   //!
        TBranch        *b_lep1_truthOrigin;   //!
        TBranch        *b_lep0_E;   //!
        TBranch        *b_lep1_E;   //!
        TBranch        *b_jet0_E;   //!
        TBranch        *b_jet1_E;   //!
        TBranch        *b_jet2_E;   //!
        TBranch        *b_lep0_phi;   //!
        TBranch        *b_lep1_phi;   //!
        TBranch        *b_jet0_phi;   //!
        TBranch        *b_jet1_phi;   //!
        TBranch        *b_jet2_phi;   //!
        TBranch        *b_lep1_is_m;   //!
        TBranch        *b_lep1_is_e;   //!
        TBranch        *b_lep0_is_m;   //!
        TBranch        *b_lep0_is_e;   //!
        */
        TBranch        *b_weight;   //!
        TBranch        *b_bdtMltClass;   //!
        TBranch        *b_bdt_vbf;   //!
        TBranch        *b_bdt_WW;   //!
        TBranch        *b_bdt_ggf;   //!
        TBranch        *b_region;   //!
        
        /*
        tin->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
        tin->SetBranchAddress("MCEventNumber", &MCEventNumber, &b_MCEventNumber);
        tin->SetBranchAddress("METRel_noJets", &METRel_noJets, &b_METRel_noJets);
        tin->SetBranchAddress("METRel_withJets", &METRel_withJets, &b_METRel_withJets);
        tin->SetBranchAddress("TrackMETRel_noJets", &TrackMETRel_noJets, &b_TrackMETRel_noJets);
        tin->SetBranchAddress("TrackMETRel_withJets", &TrackMETRel_withJets, &b_TrackMETRel_withJets);
        tin->SetBranchAddress("MET_CST", &MET_CST, &b_MET_CST);
        tin->SetBranchAddress("MET_TST", &MET_TST, &b_MET_TST);
        tin->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
        tin->SetBranchAddress("nJetsTight", &nJetsTight, &b_nJetsTight);
        tin->SetBranchAddress("Mjj", &Mjj, &b_Mjj);
        tin->SetBranchAddress("nBJetsSubMV2c10", &nBJetsSubMV2c10, &b_nBJetsSubMV2c10);
        tin->SetBranchAddress("centralJetVetoLeadpT", &centralJetVetoLeadpT, &b_centralJetVetoLeadpT);
        tin->SetBranchAddress("mtt", &mtt, &b_mtt);
        tin->SetBranchAddress("mZ", &mZ, &b_mZ);
        tin->SetBranchAddress("OLV", &OLV, &b_OLV);
        tin->SetBranchAddress("MET", &MET, &b_MET);
        tin->SetBranchAddress("MT2_1Jet", &MT2_1Jet, &b_MT2_1Jet);
        tin->SetBranchAddress("nJetsFJVT3030", &nJetsFJVT3030, &b_nJetsFJVT3030);
        tin->SetBranchAddress("Ml0j0", &Ml0j0, &b_Ml0j0);
        tin->SetBranchAddress("Ml0j1", &Ml0j1, &b_Ml0j1);
        tin->SetBranchAddress("Ml1j0", &Ml1j0, &b_Ml1j0);
        tin->SetBranchAddress("Ml1j1", &Ml1j1, &b_Ml1j1);
        tin->SetBranchAddress("jet0_pt", &jet0_pt, &b_jet0_pt);
        tin->SetBranchAddress("jet1_pt", &jet1_pt, &b_jet1_pt);
        tin->SetBranchAddress("jet2_pt", &jet2_pt, &b_jet2_pt);
        tin->SetBranchAddress("nJets", &nJets, &b_nJets);
        tin->SetBranchAddress("jet0_eta", &jet0_eta, &b_jet0_eta);
        tin->SetBranchAddress("jet1_eta", &jet1_eta, &b_jet1_eta);
        tin->SetBranchAddress("jet2_eta", &jet2_eta, &b_jet2_eta);
        tin->SetBranchAddress("lep0_eta", &lep0_eta, &b_lep0_eta);
        tin->SetBranchAddress("lep1_eta", &lep1_eta, &b_lep1_eta);
        tin->SetBranchAddress("SEtajj", &SEtajj, &b_SEtajj);
        tin->SetBranchAddress("DPhijj", &DPhijj, &b_DPhijj);
        tin->SetBranchAddress("DEtajj", &DEtajj, &b_DEtajj);
        tin->SetBranchAddress("DYjj", &DYjj, &b_DYjj);
        tin->SetBranchAddress("Mll", &Mll, &b_Mll);
        tin->SetBranchAddress("DYll", &DYll, &b_DYll);
        tin->SetBranchAddress("DPhill", &DPhill, &b_DPhill);
        tin->SetBranchAddress("MT", &MT, &b_MT);
        tin->SetBranchAddress("ptTot", &ptTot, &b_ptTot);
        tin->SetBranchAddress("TrackMET", &TrackMET, &b_TrackMET);
        tin->SetBranchAddress("lep0_pt", &lep0_pt, &b_lep0_pt);
        tin->SetBranchAddress("lep1_pt", &lep1_pt, &b_lep1_pt);
        tin->SetBranchAddress("lep0_truthType", &lep0_truthType, &b_lep0_truthType);
        tin->SetBranchAddress("lep1_truthType", &lep1_truthType, &b_lep1_truthType);
        tin->SetBranchAddress("lep0_truthOrigin", &lep0_truthOrigin, &b_lep0_truthOrigin);
        tin->SetBranchAddress("lep1_truthOrigin", &lep1_truthOrigin, &b_lep1_truthOrigin);
        tin->SetBranchAddress("lep0_E", &lep0_E, &b_lep0_E);
        tin->SetBranchAddress("lep1_E", &lep1_E, &b_lep1_E);
        tin->SetBranchAddress("jet0_E", &jet0_E, &b_jet0_E);
        tin->SetBranchAddress("jet1_E", &jet1_E, &b_jet1_E);
        tin->SetBranchAddress("jet2_E", &jet2_E, &b_jet2_E);
        tin->SetBranchAddress("lep0_phi", &lep0_phi, &b_lep0_phi);
        tin->SetBranchAddress("lep1_phi", &lep1_phi, &b_lep1_phi);
        tin->SetBranchAddress("jet0_phi", &jet0_phi, &b_jet0_phi);
        tin->SetBranchAddress("jet1_phi", &jet1_phi, &b_jet1_phi);
        tin->SetBranchAddress("jet2_phi", &jet2_phi, &b_jet2_phi);
        tin->SetBranchAddress("lep1_is_m", &lep1_is_m, &b_lep1_is_m);
        tin->SetBranchAddress("lep1_is_e", &lep1_is_e, &b_lep1_is_e);
        tin->SetBranchAddress("lep0_is_m", &lep0_is_m, &b_lep0_is_m);
        tin->SetBranchAddress("lep0_is_e", &lep0_is_e, &b_lep0_is_e);
        */
        tin->SetBranchAddress("weight", &weight, &b_weight);
        tin->SetBranchAddress("bdtMltClass", &bdtMltClass, &b_bdtMltClass);
        tin->SetBranchAddress("bdt_vbf", &bdt_vbf, &b_bdt_vbf);
        tin->SetBranchAddress("bdt_WW", &bdt_WW, &b_bdt_WW);
        tin->SetBranchAddress("bdt_ggF", &bdt_ggf, &b_bdt_ggf);
        tin->SetBranchAddress("region", &region, &b_region);
       
       for(long int i=0; i<(int)tin->GetEntries(); i++){
           mini->GetEntry(i);
           if (*s=="VBF"){
             // cout << " i " << i << " " << bdt_vbf << " " << bdt_WW << endl;
             if(region==0) {
               nb++;
               for (int j=0;j<steps;j++) ha[j].Fill(bdt_vbf,bdt_WW,weight);
               }
           }
           else if (*s=="WW"){
             if(region==0) {
               wb++;
               for (int j=0;j<steps;j++) wa[j].Fill(bdt_vbf,bdt_WW,weight);
               }
           }
               else{
               if(region==0){
                 nnb++;
                 for (int j=0;j<steps;j++) na[j].Fill(bdt_vbf,bdt_WW,weight);
               }
           }
       }
    cout << "nb:" << nb << endl;
    cout << "nnb:" << nnb << endl;
    cout << "wb:" << wb << endl;
    }
  //----------------------------------
    
    
    std::vector<double> bx;
    std::vector<double> bX;
    std::vector<double> by;
    std::vector<double> bY;
    std::vector<double> bv;
    std::vector<double> bw;
    std::vector<double> bnv;
    std::vector<int> activ;
    
    for (int u=0;u<2;u++){
        for (int j=0;j<2;j++){
            bv.push_back(ha[2].GetBinContent(u+1,j+1));
            bw.push_back(ha[2].GetBinContent(u+1,j+1));
            bnv.push_back(na[2].GetBinContent(u+1,j+1));
            activ.push_back(1);
            bx.push_back(u/2.);
            bX.push_back((u+1)/2.);
            by.push_back(j/2.);
            bY.push_back((j+1)/2.);
        }
    }
    for (int u=0;u<4;u++)
        cout << "X[" << bx[u] << " ; " << bX[u] <<"] " << "Y[" << by[u] << " ; " << bY[u] <<"] " << bv[u] << endl;
    cout << endl;
    
    cout << "comp " << Comparison(1000,1200,600,400) << endl;
    
    for(int step_i=3;step_i<step_max;step_i++){
    cout << pow(2,(step_i-1)) << " to " << pow(2,(step_i)) << endl;
    int k=0;
    cout << "x" << pow(2,((step_i+1)/2)) << "y" << pow(2,(step_i/2)) << endl;
    for(int u=0; u<bv.size(); u++){
        cout << "so:" << bv[u] << " noise o " << bnv[u] << endl;
        Splitter(pow(2,((step_i+1)/2)), pow(2,(step_i/2)), step_i, 2*k);
        int sva=ha[step_i].GetBinContent(GbinX,GbinY);
        int nva=na[step_i].GetBinContent(GbinX,GbinY);
        cout << " saa(" <<GbinX << "," << GbinY << "):" << sva << endl;
        Splitter(pow(2,((step_i+1)/2)), pow(2,(step_i/2)), step_i, 2*k+1);
        int svb=ha[step_i].GetBinContent(GbinX,GbinY);
        int nvb=na[step_i].GetBinContent(GbinX,GbinY);
        cout << " sbb(" <<GbinX << "," << GbinY << "):" << svb << endl;
        bool splittami=0;
        if((Comparison(bv[u],bnv[u],sva,nva)==1) ||(Comparison(bv[u],bnv[u],svb,nvb))==1) splittami=1;
        
        if( (activ[u]==0) || (bv[u]<1) || (bnv[u]<1) || (splittami==0) || (bv[u]/bnv[u])<signoisthresh){
            float area=((bX[u]-bx[u])*(bY[u]-by[u]))*(pow(2,step_i-1));
            cout << "homogeneous area: X["<< bx[u] << ";" << bX[u] <<"] Y["<< by[u] << ";" << bY[u] <<"] (" << area <<")" << endl;
            k+=area;
            activ[u]=0;
        }
        if (bv[u]>0 && bnv[u]>0 && activ[u]==1 && splittami==1 && (bv[u]/bnv[u])>=signoisthresh){
            cout << "splitting bin" << endl;
            cout <<"replacing cut X[" << bx[u] << ";" << bX[u] <<"] Y["<< by[u] << ";" << bY[u] <<"] ";
            bx.insert(bx.begin()+u+1,(bx[u]+bX[u])/2);
            bX.insert(bX.begin()+u,(bx[u]+bX[u])/2);
            by.insert(by.begin()+u,by[u]);
            bY.insert(bY.begin()+u,bY[u]);
            cout << "with cuts X[" << bx[u] << ";" << bX[u] << "]  Y["<< by[u] << ";" << bY[u] <<"]  and [" << bx[u+1] << ";" << bX[u+1] << "] Y["<< by[u+1] << ";" << bY[u+1] <<"]" << endl;
            
            cout <<"replacing bin content: " << bv[u];
            Splitter(pow(2,((step_i+1)/2)), pow(2,(step_i/2)), step_i, 2*k);
            cout << " with bins " << GbinX << " " << GbinY << " of "<< ha[step_i].GetBinContent(GbinX,GbinY);
            bv.insert(bv.begin()+u,ha[step_i].GetBinContent(GbinX,GbinY));
            bw.insert(bw.begin()+u,wa[step_i].GetBinContent(GbinX,GbinY));
            bnv.insert(bnv.begin()+u,na[step_i].GetBinContent(GbinX,GbinY));
            activ.insert(activ.begin()+u,1);
            Splitter(pow(2,((step_i+1)/2)),pow(2,(step_i/2)),step_i,2*k+1);
            cout << " and " << GbinX << " " << GbinY << " of " << ha[step_i].GetBinContent(GbinX,GbinY) << endl;
            bv[u+1]=ha[step_i].GetBinContent(GbinX,GbinY);
            bw[u+1]=wa[step_i].GetBinContent(GbinX,GbinY);
            bnv[u+1]=na[step_i].GetBinContent(GbinX,GbinY);
            activ[u+1]=1;
            //cout <<"noise:" << bnv[u]<< ", " << bnv[u+1] << endl;
            u++;
            k++;
        }
        cout << endl;
    }
    cout << "sizes " << bv.size() << " " << bx.size() << " " << by.size() << " " << bw.size() << " " << activ.size() << endl;
    for(int u=0; u<bv.size(); u++)
        cout << "X:[" << bx[u] << ";" << bX[u] <<"] " << "Y:[" << by[u] << ";" << bY[u] << "] " << bv[u] << endl;
    cout << endl;
    step_i++;
    
    cout << pow(2,(step_i-1)) << " to " << pow(2,(step_i)) << endl;
    k=0;
    for(int u=0; u<bw.size(); u++){
        cout << "so:" << bw[u] << " noise o " << bnv[u] << endl;
        Splitter(pow(2,((step_i+1)/2)), pow(2,(step_i/2)), step_i, 2*k);
        int sva=wa[step_i].GetBinContent(GbinX,GbinY);
        int nva=na[step_i].GetBinContent(GbinX,GbinY);
        cout << " sa(" <<GbinX << "," << GbinY << "):" << sva << endl;
        Splitter(pow(2,((step_i+1)/2)), pow(2,(step_i/2)), step_i, 2*k+1);
        int svb=wa[step_i].GetBinContent(GbinX,GbinY);
        int nvb=na[step_i].GetBinContent(GbinX,GbinY);
        cout << " sb(" <<GbinX << "," << GbinY << "):" << svb << endl;
        bool splittami=0;
        if((Comparison(bw[u],bnv[u],sva,nva)==1) ||(Comparison(bw[u],bnv[u],svb,nvb))==1) splittami=1;
        
        if((activ[u]==0) || (bw[u]<1) || (bnv[u]<1) || (splittami==0) || ((bw[u]/bnv[u])<signoisthresh)){
            float area=((bX[u]-bx[u])*(bY[u]-by[u]))*(pow(2,step_i-1));
            cout << "homogeneous area: area: X["<< bx[u] << ";" << bX[u] <<"] Y["<< by[u] << ";" << bY[u] <<"] (" << area <<")" << endl;
            k+=area;
            activ[u]=0;
        }
        if(bw[u]>0 && bnv[u]>0 && activ[u]==1 && splittami==1 && (bw[u]/bnv[u])>=signoisthresh){
            cout << "splitting bin" << endl;
            cout <<"replacing cut X[" << bx[u] << ";" << bX[u] <<"] Y["<< by[u] << ";" << bY[u] <<"] ";
            by.insert(by.begin()+u+1,(by[u]+bY[u])/2);
            bY.insert(bY.begin()+u,(by[u]+bY[u])/2);
            bx.insert(bx.begin()+u,bx[u]);
            bX.insert(bX.begin()+u,bX[u]);
            cout << "with cuts X[" << bx[u] << ";" << bX[u] << "]  Y["<< by[u] << ";" << bY[u] <<"]  and [" << bx[u+1] << ";" << bX[u+1] << "] Y["<< by[u+1] << ";" << bY[u+1] <<"]" << endl;
            
            cout <<"replacing bin content: " << bw[u];
            Splitter(pow(2,((step_i)/2)), pow(2,(step_i/2)), step_i, 2*k);
            cout << " with bins " << GbinX << " " << GbinY << " of "<< wa[step_i].GetBinContent(GbinX,GbinY);
            bv.insert(bv.begin()+u,ha[step_i].GetBinContent(GbinX,GbinY));
            bw.insert(bw.begin()+u,wa[step_i].GetBinContent(GbinX,GbinY));
            bnv.insert(bnv.begin()+u,na[step_i].GetBinContent(GbinX,GbinY));
            activ.insert(activ.begin()+u,1);
            Splitter(pow(2,((step_i)/2)),pow(2,(step_i/2)),step_i,2*k+1);
            cout << " and " << GbinX << " " << GbinY << " of " << wa[step_i].GetBinContent(GbinX,GbinY) << endl;
            bv[u+1]=ha[step_i].GetBinContent(GbinX,GbinY);
            bw[u+1]=wa[step_i].GetBinContent(GbinX,GbinY);
            bnv[u+1]=na[step_i].GetBinContent(GbinX,GbinY);
            activ[u+1]=1;
            u++;
            k++;
        }
        cout << endl;
    }
    cout << "sizes " << bv.size() << " " << bx.size() << " " << by.size() << " " << bw.size() << " " << activ.size() << endl;

        int no_zero=0;
        for(int u=0; u<bv.size(); u++){
        cout << "X:[" << bx[u] << ";" << bX[u] <<"] " << "Y:[" << by[u] << ";" << bY[u] << "] " << bv[u] << endl;
            if((bv[u]>0)) no_zero++;
        }
        cout << "N cuts:" << bv.size() <<  " No zero:" << no_zero << endl;
        
    cout << endl;
    }
    
    /*
    TCanvas *c2 = new TCanvas("c1");
    TH2D *hh = new TH2D();
    *hh=ha[ha.size()-2];
    hh->Draw("box");
    c2->Update();
    */
    
    //-----------------------------------------
    
    TH1D *VBFf = new TH1D("VBF_f","VBF events",bv.size(),0,bv.size());
    for(int u=0; u<bv.size(); u++) VBFf->SetBinContent(u+1,bv[u]);
    VBFf->SetFillColor(kRed);
    
    TH1D *WWf = new TH1D("WW_f","WW events",bw.size(),0,bw.size());
    for(int u=0; u<bw.size(); u++) WWf->SetBinContent(u+1,bw[u]);
    WWf->SetFillColor(kGreen);
    
    TH1D *noisef = new TH1D("noise_f","noise events",bnv.size(),0,bnv.size());
    for(int u=0; u<bnv.size(); u++) noisef->SetBinContent(u+1,bnv[u]);
    noisef->SetFillColor(kGray);
    
    THStack *stak =new THStack("all", "Events Binned");
    stak->Add(noisef);
    stak->Add(WWf);
    stak->Add(VBFf);
    
   
    
    TLegend *leg =new TLegend(0.1,0.3,0.4,0.8);
    leg->AddEntry(VBFf,"VBF","f");
    leg->AddEntry(WWf,"WW","f");
    leg->AddEntry(noisef,"noise","f");
    
    //---------------------------------------
    
    TH1D *VBFfsn = new TH1D("VBF_fsn","VBF s/n",bv.size(),0,bv.size());
    for(int u=0; u<bv.size(); u++){
        if(bnv[u]==0){
            if(bv[u]>0) VBFfsn->SetBinContent(u+1,100);
        }
        else
            VBFfsn->SetBinContent(u+1,bv[u]/bnv[u]);
    }
    VBFfsn->SetFillColor(kRed);
    
    TH1D *WWfsn = new TH1D("WW_fsn","WW s/n",bw.size(),0,bw.size());
    for(int u=0; u<bw.size(); u++){
        if(bnv[u]==0){
            if(bw[u]>0) VBFfsn->SetBinContent(u+1,100);
        }
        else WWfsn->SetBinContent(u+1,bw[u]/bnv[u]);
    }
    WWfsn->SetFillColor(kGreen);
    
    THStack *staks =new THStack("all", "S/n Binned");
    staks->Add(VBFfsn);
    staks->Add(WWfsn);
    
    TCanvas *d1 = new TCanvas("d1");
    VBFfsn->Draw();
    TCanvas *d2 = new TCanvas("d2");
    WWfsn->Draw();
    
    TCanvas *c1 = new TCanvas("c1");
    stak->Draw();
    leg->Draw();
    
    std::vector<TBox*> tiles;
    TCanvas * c = new TCanvas("c_ref","c_title",200,10,600,600);{
    c->cd();
    TH2D *hh;
    *hh=ha[ha.size()-1];
    hh->Draw("box");
    wa[0].Draw("colz");
                   
   // ha[8].Draw();
   // c->Update();
    //TCanvas firm= TCanvas();
    for(int u=0; u<bv.size(); u++){
        cout << bx[u] << " " << bX[u] <<" "  << by[u] << " " << bY[u] << " " << endl;
        TBox *boxxa = new TBox(bx[u],by[u],bX[u],bY[u]);
        boxxa->SetLineColor(kBlack);
        boxxa->SetLineWidth(3);
        tiles.push_back(boxxa);
        //boxxa->Draw("l same");
        //delete boxxa;
    }
    }
//TCanvas * c = new TCanvas("c_ref","c_title",200,10,600,600);
//for(int u=0; u<bv.size(); u++) tiles[u]->Draw("l");
  //  tiles.at(0)->Draw("l");
  //  c->Update();
    
}

        





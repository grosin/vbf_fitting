#include "tools.h"

void compareYields(){
  std::map<string,double> lumi;
  lumi["old"]=36.1;
  lumi["new"]=36.1;
    
  std::map<string, TFile*> files;
  files["old"]=TFile::Open("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/BDT/2jetfileswithMulticlass/NtuplesWithCuts/eval_3D_AllSamps_ggF_0-1jet_Jan18.root");
  //files["old"]=TFile::Open("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/BDT/2jetfileswithMulticlass/eval_3D_AllSamps_2jet_Dec17.root");
  //files["new"]=TFile::Open("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/June2019SS_withSyss/total_merged_2jet_noFakes.root");
  //files["new"]=TFile::Open("~/eos/atlas/user/g/gabarone/higgs/ww/recoNtuples/PxAOD_v18_r21_Laura_Sys_fitProc/merged.root");
  //files["new"]=TFile::Open("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/June2019SS_withSys/pxAODv18_Laura_sys_merged.root");

  files["new"]=TFile::Open("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/June2019SS_withSys/total_merged_0jet_noFakes_slimmed_patched_Stef3Dv1.root");
  
  std::map<string, string> tnames;
  tnames["Zjets"]="Zjets_nominal";
  tnames["Top"]="top_nominal";
  tnames["WW"]="diboson_nominal";
  tnames["GGF"]="ggf_nominal";
  tnames["VBF"]="vbf_nominal";
  tnames["Vgamma"]="Vgamma_nominal";

  //string selectionnew="inSR==1";
  string selectionnew="inggFCR3==1";
  //string selection="nJetsTight>=2 && nBJetsSubMV2c10<1 && centralJetVetoLeadpT<20000 && OLV==1 && mtt/1000<mZ/1000-25";
  //string selection="nJetsTight>=2 && nBJetsSubMV2c10<1 && centralJetVetoLeadpT<20000 && OLV==1 && mtt/1000<mZ/1000-25 && Mjj>160000";
  string selection="nJetsTight<2 && nBJetsSubMV2c10<1";


  for(std::map<string,string>::iterator it=tnames.begin(); it!=tnames.end(); it++){
    string oldName=(*it).first;
    string newName=(*it).second;

    TTree *told=(TTree*)files["old"]->Get(oldName.c_str());
    if(told==nullptr) { cout<<oldName<<" not found "<<endl; continue; } 
    TTree *tnew=(TTree*)files["new"]->Get(newName.c_str());
    if(tnew==nullptr) { cout<<newName<<" not found "<<endl; continue; }

    TH1F *hold=new TH1F("hold","hold",100,-555,+555);
    told->Draw("bdt_vbf>>hold",Form("weight*(%s)",selection.c_str()),"");
    
    TH1F *hnew=new TH1F("hnew","hnew",100,-555,+555);
    tnew->Draw("bdt_vbf>>hnew",Form("weight*(%s)",selectionnew.c_str()),"");
    
    cout<<newName<<" "<<"hold (scaled) "<<hold->Integral()*lumi["new"]/lumi["old"]<<" hnew "<<hnew->Integral()<<" scaling "<<
      hold->Integral()/hnew->Integral()*lumi["new"]/lumi["old"]<<endl;

    cout<<newName<<" "<<Form("addSampleSpecificWeight(%lf)",hold->Integral()/hnew->Integral()*lumi["new"]/lumi["old"])<<endl;
      

    delete hnew;
    delete hold;
    
  }


  
  


}

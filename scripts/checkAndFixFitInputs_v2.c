#include "tools.h"
bool isDiff=true;
string topWWDscir="bdt_TopWWAll";
//string topWWDscir="bdt_TopWWAll2";

std::string remove_extension(const std::string& filename) {
    size_t lastdot = filename.find_last_of(";");
    if (lastdot == std::string::npos) return filename;
    cout<<" removing from "<<filename <<" to "<<filename.substr(0, lastdot)<<endl;
    return filename.substr(0, lastdot); 
}

string whichObs(std::string & mainStr, const std::string & toErase){
  if( mainStr.find(toErase)==string::npos) return "unknown";

  string sbtr=mainStr.substr(mainStr.find(toErase)+toErase.length(),mainStr.length());
  return sbtr;

}

string eraseSubStr(std::string & mainStr, const std::string & toErase)
{
  string ret=mainStr;
	// Search for the substring in string
	size_t pos = ret.find(toErase);
 
	if (pos != std::string::npos)
	{
		// If found then erase it from string
		ret.erase(pos, toErase.length());
	}
    return ret;
}
string eraseSubStrToEnd(std::string & mainStr, const std::string & toErase)
{
  string ret=mainStr;
	// Search for the substring in string
	size_t pos = ret.find(toErase);
 
	if (pos != std::string::npos)
	{
      //cout<<"Ereasing from "<<toErase<<" to "<<ret<<endl;
      // If found then erase it from string
		ret.erase(pos, ret.length());
	}
    return ret;
}


string whichRegion(string name, vector<string>vec){
  string region;
  for( vector<string>::iterator it=vec.begin(); it!=vec.end(); it++){
    string thisRegionTest="_"+(*it)+"_";
    if(TString(name.c_str()).Contains( thisRegionTest.c_str())){
      region=(*it);
      break;
    }
  }
  return region;
}
string whichSample(string name, vector<string>vec,vector<string>vec2){
  //cout<<"Searching for sample for "<<name<<endl;
  
  string sample="";
  //remove useless parts
  //if(TString(name.c_str()).Contains("Nom"))
  //name=eraseSubStrToEnd(name,"Nom");

  bool containsOther=false;
  for( vector<string>::iterator it=vec2.begin(); it!=vec2.end(); it++){
    string thisSampleTest=(*it);
    //cout<<"Testing (shortlist) "<<thisSampleTest<<" compared to "<<name<<endl;
    if( (TString(name.c_str())).Contains(thisSampleTest.c_str())){
      containsOther=true;
      sample=(*it);
      //      cout<<"Sample is (shortlist) "<<sample<<endl;
      return sample;
    }
  }
  
  for( vector<string>::iterator it2=vec.begin(); it2!=vec.end(); it2++){
    string thisSampleTest2= (*it2) ;
    //cout<<"Testing "<<thisSampleTest2<<" compared to "<<name<<endl;
    if( (TString(name.c_str())).Contains(thisSampleTest2.c_str())){
      sample=(*it2);
      //cout<<"Sample is "<<sample<<endl;
      return sample ;
    }
  }

  //cout<<"Sample is not found "<<sample<<endl;
  return sample;
}

string whichVariation(string name="",string sampleName="",string rName=""){
  if ( name.find("Nom")!=string::npos) return "nominal";
  name=eraseSubStr(name, sampleName);
  if( name.find("High_")!=string::npos)
    name=eraseSubStrToEnd(name,"High");
  else if (name.find("Low_")!=string::npos)
    name=eraseSubStrToEnd(name,"Low");
  else
    return "unkown";
  return name; 
}


void checkAndFixFitInputs(string tname=""){
  // Load the file
  //TFile *f=TFile::Open(tname.c_str());//new TFile(tname.c_str(),"UPDATE");
  //TFile *fnew=new TFile("fnew.root","RECREATE");

  TFile *f=new TFile(tname.c_str(),"UPDATE");

  
  // take the histogram map
  TKey *keyP=nullptr;
  TIter nextP(f->GetListOfKeys());
  int nTrees=0;
  vector <string> TH1FNames;
  vector <string> TH1FNamesNorm;
  vector <string> TH1FNamesNormTF;
  vector <string> TH1FNamesNormRaw;
  vector <string> TH1FTheorNames;
  vector <string> TH1FTheorNominals;
  
  while ((keyP=(TKey*)nextP())) {
    if (strcmp(keyP->GetClassName(),"TH1")) {
      string ttname=remove_extension(keyP->GetName());
      bool isnew=true;
      for(std::vector<string>::iterator it=TH1FNames.begin(); it!=TH1FNames.end(); it++){
        if( (*it).compare(ttname)==0) 
          {   isnew=false; break; } 
      }
      if(isnew){
        //cout<<"Adding "<<ttname<<endl;
        TH1FNames.push_back(ttname);
        if(!TString(ttname.c_str()).Contains("Norm"))
          TH1FNamesNormRaw.push_back(ttname);
        if(TString(ttname.c_str()).Contains("Norm") && TString(ttname.c_str()).Contains("_obs"))
          TH1FNamesNorm.push_back(ttname);
        if(TString(ttname.c_str()).Contains("Norm") && !TString(ttname.c_str()).Contains("_obs"))
          TH1FNamesNormTF.push_back(ttname);
        if(TString(ttname.c_str()).Contains("theo"))
          TH1FTheorNames.push_back(ttname); 
      }
    }
  }


  // vectors of bin edges of each observable 
  std::map<string,vector<double>> xbinsMap;
  

  const double emptyReplace=1e-6;
  // list of regions 
  vector <string> regions;
  regions.push_back("CRGGF1");
  regions.push_back("CRGGF2");
  regions.push_back("CRGGF3");
  regions.push_back("CRWW");
  regions.push_back("CRZjets");
  if(!isDiff){
    regions.push_back("CRTop");
    regions.push_back("SRVBF");
  }
  //for( int i=0; i<(int)15; i++)
  //regions.push_back(Form("SRVBF_%d",i));
  
  
  vector <string> samples; 
  samples.push_back("hvbf0");
  samples.push_back("hdiboson");
  samples.push_back("htop");
  samples.push_back("hggf");
  //samples.push_back("hZjets");
  samples.push_back("hZjets0");
  samples.push_back("hVgamma");
  samples.push_back("Fakes");
  samples.push_back("hdata");
  samples.push_back("hvh");
  samples.push_back("htt");

 

  /*
  samples.push_back("hZjets1");
  samples.push_back("hdiboson1");
  samples.push_back("htop1");
  samples.push_back("hggf1");
  samples.push_back("hggf2");
  samples.push_back("hggf3");
  */
  
  vector <string> samples2; 
  samples2.push_back("hZjets1");
  samples2.push_back("hdiboson1");
  samples2.push_back("hdiboson2");
  samples2.push_back("hdiboson3");
  samples2.push_back("htop1");
  samples2.push_back("htop2");
  samples2.push_back("htop3");
  samples2.push_back("hggf1");
  samples2.push_back("hggf2");
  samples2.push_back("hggf3");

  // get all the nominal histograms 
  std::map<string, std::pair<string,string>> nominalMap;
  std::map<string, std::pair<string,string>> normMap;
  std::map<string, std::pair<string,string>> tfMap;
  std::map<string, std::vector<std::pair<string,string>>> sysVarsMap;
  std::map<string, std::vector<std::pair<string,string>>> sysVarsNormMap;
  std::map<string, std::vector<std::pair<string,string>>> tfSysNormMap;

  std::map<string, std::vector<string>> considerSys;

  // map of normalisation regions for each sample
  std::map<string, vector<string>> sampleNormMap;
  if(!isDiff){
    sampleNormMap["hvbf0"].push_back("SRVBF");
    sampleNormMap["hvbf0"].push_back("CRTop");
    //sampleNormMap["hvbf0"].push_back("CRWW");
  
  //for( int i=0; i<(int)15; i++){
  //sampleNormMap[Form("hvbf0_%d",i)].push_back(Form("SRVBF_%d",i));
  //sampleNormMap[Form("hvbf0_%d",i)].push_back("CRTop");
  //}
    
  sampleNormMap["htop"].push_back("SRVBF");
  //for( int i=0; i<(int)15; i++)
  //sampleNormMap["htop"].push_back(Form("SRVBF_%d",i));
  sampleNormMap["htop"].push_back("CRTop");
 //sampleNormMap["htop"].push_back("CRWW");

  sampleNormMap["hdiboson"].push_back("SRVBF");
  for( int i=0; i<(int)15; i++)
    sampleNormMap["hdiboson"].push_back(Form("SRVBF_%d",i));

  sampleNormMap["hdiboson"].push_back("CRTop");
  //sampleNormMap["hdiboson"].push_back("CRWW");
  }
  
  sampleNormMap["hZjets0"].push_back("CRZjets");
  sampleNormMap["hZjets1"].push_back("CRGGF3");

  sampleNormMap["htop1"].push_back("CRGGF3");
  sampleNormMap["htop2"].push_back("CRGGF2");
  sampleNormMap["htop3"].push_back("CRGGF1");

  sampleNormMap["hdiboson1"].push_back("CRGGF3");
  sampleNormMap["hdiboson2"].push_back("CRGGF2");
  sampleNormMap["hdiboson3"].push_back("CRGGF1");
  
  sampleNormMap["hggf1"].push_back("CRGGF1");
  sampleNormMap["hggf2"].push_back("CRGGF2");
  sampleNormMap["hggf3"].push_back("CRGGF3");
  sampleNormMap["hggf"].push_back("SRVBF");
  sampleNormMap["hggf"].push_back("CRTop");
  //sampleNormMap["hggf"].push_back("CRWW");


  // observables per region
  std::map<string, string> obsRegions;
  obsRegions["SRVBF"]="bdt_vbf";
  //for( int i=0; i<(int)15; i++)
  //obsRegions[Form("SRVBF_%d",i)]="bdt_vbf";
  obsRegions["CRTop"]=topWWDscir;
  obsRegions["CRGGF3"]="bdt_ggFCR3";
  obsRegions["CRGGF2"]="bdt_ggFCR2";
  obsRegions["CRGGF1"]="bdt_ggFCR1";
  obsRegions["CRZjets"]="MT";
  obsRegions["CRWW"]="bdt_TopWW";
  //obsRegions["SRGGF"]="bdt_vbfggf";
  
  
   if(isDiff){
     for(int i=0; i<20; i++){
      regions.push_back(Form("SRVBF_%d",i));
      regions.push_back(Form("CRTop_%d",i));
      samples.push_back(Form("hvbf0_%d",i));
      samples2.push_back(Form("hvbf0_%d",i));
      sampleNormMap[Form("hvbf0_%d",i)].push_back(Form("SRVBF_%d",i));
      sampleNormMap[Form("hvbf0_%d",i)].push_back(Form("CRTop_%d",i));
      sampleNormMap["hdiboson"].push_back(Form("CRTop_%d",i));
      obsRegions[Form("SRVBF_%d",i)]="bdt_vbf";
      obsRegions[Form("CRTop_%d",i)]=topWWDscir;
      
     }

  }
  
  

  // build map of tranfer factors for the samples for each systematic
  // inntegral for nominal and all variations 
  // save a raw version of the histogrmas 
  


  vector <string> namesToSplit;
  namesToSplit.push_back("htop");
  namesToSplit.push_back("hdiboson");
  namesToSplit.push_back("hggf");
  namesToSplit.push_back("hZjets0"); 
  // here add the missing one per region
  vector <string> addedNames;
  
  for(vector<string>::iterator ik=TH1FTheorNames.begin(); ik!=TH1FTheorNames.end(); ik++){
    string htname=(*ik);
    
    TH1F *h=(TH1F*)f->Get( htname.c_str()); 
    cout<<"Checking "<<h->GetName()<<"  "<< (*ik)<<endl;
    for( int m=1; m<4 ; m++){
      for( vector <string>::iterator il=namesToSplit.begin(); il!=namesToSplit.end(); il++){
        if( TString( (*ik).c_str() ).Contains( (*il).c_str() )){

          TH1F *hClone=nullptr;
          // Somehow it does not work oddly 
          if(TString( (*ik).c_str() ).Contains("hZets0")){
            string newName=(TString( (*ik).c_str() ).ReplaceAll( "hZjets0" , Form("hZjets%d",m))).Data();
            hClone=(TH1F*)h->Clone( newName.c_str());
            cout<<"Adding histogram "<<hClone->GetName()<<endl;
            hClone->Write();
            addedNames.push_back(hClone->GetName());
            delete hClone;
            continue;
          }
            else 
              hClone=(TH1F*)h->Clone( (TString( (*ik).c_str() ).ReplaceAll((*il).c_str(), Form("%s%d",(*il).c_str(),m))).Data());
          
          cout<<"Adding histogram "<<hClone->GetName()<<endl;
          hClone->Write();
          addedNames.push_back(hClone->GetName());
          // Add here histograms for the thoery variations in regions
          const string sample=whichSample(htname,samples,samples2);
          const string region=whichRegion(htname,regions);
          const string variation= whichVariation( htname ,sample,region);
          if( sample.compare("hZjets1")==0 || sample.compare("hdiboson1")==0 || sample.compare("hdiboson2")==0  || sample.compare("hdiboson3")==0 ||
              sample.compare("htop1")==0 || sample.compare("htop2")==0 || sample.compare("htop3")==0){

            //TH1F *hregion=(TH1F*)hClone->Clone(TString(hClone->GetName()).ReplaceAll(variation.c_str(),Form("%s_Region%d",variation.c_str(),m)));
            //hregion->Write();
            //addedNames.push_back(hregion->GetName());
            //delete hregion;
          }
          
          
          delete hClone; 
        }
      }
    }
    delete h; 
  }
  
  for(vector<string>::iterator it=addedNames.begin(); it!=addedNames.end(); it++){
    cout<<"Adding "<<(*it)<<endl;
    TH1FNames.push_back( (*it));
  }

  
  for(vector<string>::iterator it=TH1FNames.begin(); it!=TH1FNames.end(); it++){
    TString hname(*it);

    // remove spourious histograms
    //if( !TString( (*it).c_str() ).Contains( obsRegions[whichRegion( (*it),regions )])) continue ;
    //if(  whichRegion( (*it),regions).compare(obsRegions[whichRegion( (*it),regions)])!=0)  continue ;
    if( hname.Contains("Nom") && hname.Contains("theo") ) {
      TH1FTheorNominals.push_back(hname.Data());
      continue; 
    } 
    
    // if this is the nominal file process 
    if(hname.Contains("Nom") && !hname.Contains("Norm") ){
      nominalMap[(*it)]=make_pair(whichRegion((*it),regions),whichSample((*it),samples,samples2));
      std::vector<std::pair<string,string>> tmp;
      sysVarsMap[(*it)]=tmp;
    }
    else if (hname.Contains("Nom") && hname.Contains("Norm") && hname.Contains("_obs")) {
      normMap[(*it)]=make_pair(whichRegion((*it),regions),whichSample((*it),samples,samples2));
    }
  }

  // print out all nominal maps
  for(std::map<string, std::pair<string,string>>::iterator it=nominalMap.begin(); it!=nominalMap.end() ;it++){
    cout<<(*it).first<<" region "<<(*it).second.first<<" sample "<<(*it).second.second<<endl;
  }
  cout<<endl;


  std::map<string, vector<string>> processedSys; 

  // process the nominal 
   for(vector<string>::iterator it=TH1FNamesNormTF.begin(); it!=TH1FNamesNormTF.end(); it++){
     TString hname(*it);
     

     if(!hname.Contains("Norm")) continue;
     if( hname.Contains("obs")) continue;
     if(!hname.Contains("Nom")) continue;

     // remove spourious histograms
     if( !TString( (*it).c_str() ).Contains( obsRegions[whichRegion( (*it),regions )])) continue ;
     
     string sample=whichSample((*it),samples,samples2);
     tfMap[*it]=make_pair("all",sample);
   }

   // print nominal norm tf nmaps
   for(std::map<string, std::pair<string,string>>::iterator it=tfMap.begin(); it!=tfMap.end() ;it++){
     cout<<(*it).first<<" region "<<(*it).second.first<<" sample "<<(*it).second.second<<endl;
   }
   cout<<endl;
   
   
   // fill in the systematics
   for(vector<string>::iterator it2=TH1FNamesNormTF.begin(); it2!=TH1FNamesNormTF.end(); it2++){

     TString hname(*it2);


     if(!hname.Contains("Norm")) continue;
     if( hname.Contains("obs")) continue;
     if(hname.Contains("Nom")) continue;
     
      // remove spourious histograms
     if( !TString( (*it2).c_str() ).Contains( obsRegions[whichRegion( (*it2),regions )])) continue ;
     
     string sample=whichSample( (*it2), samples,samples2);
     //cout <<"Sample is "<<sample<<endl;
     // find the corresponding nomial:
     string nominal="";
     for(std::map<string, std::pair<string,string>>::iterator it3=tfMap.begin(); it3!=tfMap.end(); it3++){
       string tsample=(*it3).second.second;
       if( sample.compare(tsample)==0) { nominal=(*it3).first; break; } 
     }
     if(nominal.size()==0) {cout<<"Sample not found" <<endl; continue; } 
     
     TString hnameUp(*it2);
     TString hnameDown( *it2);
     
     if( hnameUp.Contains("Low")) hnameUp.ReplaceAll("Low","High");
     if( hnameDown.Contains("High")) hnameDown.ReplaceAll("High","Low");
     tfSysNormMap[nominal].push_back(make_pair(hnameUp.Data(),hnameDown.Data()));
     //cout<<"Added uncertainty for "<<nominal<<" "<<hnameUp.Data()<<" <-> "<<hnameDown.Data()<<endl;
   }
   
  
  // now fill the systematic variations 
  for(vector<string>::iterator it=TH1FNamesNormRaw.begin(); it!=TH1FNamesNormRaw.end(); it++){
    TString hname(*it);

    // if this is the nominal file process 
    if(hname.Contains("Nom") || hname.Contains("Norm")) continue; 

    string upVar;
    string lowVar;

    string normUpVar;
    string normLowVar;

    TString hnameUp(*it);
    TString hnameDown( *it);

    if( hname.Contains("High_") ) {
      hnameDown.ReplaceAll("High_","Low_");
    }
    else if (hname.Contains("Low_")){
      hnameUp.ReplaceAll("Low_","High_");
    }
      
    //find the correponding norm histogram
    

    
    TString hnormNameUp=hnameUp;
    TString hnormNameDown=hnameDown;

    
    hnormNameUp=Form("%s%s",hnormNameUp.Data(),"Norm");
    hnormNameDown=Form("%s%s",hnormNameDown.Data(),"Norm");
    
    
    // find the nominal 
    string nomName;
    const string thisSample=whichSample((*it),samples,samples2);
    const string thisRegion=whichRegion((*it),regions);
    string nBsObs=(*it);
    const string thisObs=whichObs( nBsObs,  "obs_");
    cout<<"Info: sample "<<thisSample<<" Region: "<<thisRegion<<" observable: "<<thisObs<<" for "<<(*it)<<endl;

    if ( thisObs.compare(obsRegions[thisRegion])!=0) {
      cout<<"Unused observable for this region"<<endl;
      continue; 
    } 
    
    for( std::map<string, std::pair<string,string>>::iterator it2=nominalMap.begin(); it2!=nominalMap.end(); it2++){
      string nn=(*it2).first; 
      string nomObs=whichObs( nn ,"obs_");
      //cout<<" ----> "<<nn<<"  ---- " <<nomObs<<endl;
      if( thisRegion.compare( (*it2).second.first)==0 && thisSample.compare( (*it2).second.second) == 0 &&
          nomObs.compare( thisObs ) == 0 ){
        nomName=(*it2).first;
        break; 
      }
    }
    if(nomName.size()==0) continue; 

    // remove spourious histograms
    //if( !TString( (*it).c_str() ).Contains( obsRegions[whichRegion( (*it),regions )])) continue ;
    if(  thisRegion.compare(whichRegion(nomName,regions)) !=0 ||
         thisSample.compare(whichSample(nomName,samples,samples2))!=0 ||
         thisObs.compare(whichObs(nomName,"obs_"))!=0 )
      continue ;
    
    bool allreadyProcessed=false;
    vector<string> pvars=processedSys[nomName];
    for(vector<string>::iterator lp=pvars.begin(); lp!=pvars.end(); lp++){
      if ( (*lp).compare(hnameUp.Data())==0) {allreadyProcessed=true; break; }
    }

    if(allreadyProcessed) continue; 

    cout<<hnameUp.Data()<<" <---> "<<hnameDown.Data()<<" || "<<hnormNameUp.Data()<<" <---> "<<hnormNameDown.Data()<<endl;
    
    processedSys[nomName].push_back(hnameUp.Data()); 
    sysVarsMap[nomName].push_back(make_pair(hnameUp.Data(),hnameDown.Data()));
    sysVarsNormMap[nomName].push_back(make_pair(hnormNameUp.Data(),hnormNameDown.Data()));
    
    cout<<"Norm Name "<<nomName<<" has "<<sysVarsMap[nomName].size()<<" systematics for nom and for norm "<<endl;
  }

  for(vector<string>::iterator it=samples.begin(); it!=samples.end(); it++){
    vector <string> loadedSystematics; 
    // here loop over the saved variations
    ofstream myfile;
    cout<<"SAMPLE "<< (*it) <<endl;
    myfile.open("sysList_"+eraseSubStr((*it),"h")+".txt");
    // loop over all systematics found in this sample
    std::vector<std::pair<string,string>> vars;
    for(std::map<string, std::vector<std::pair<string,string>>>::iterator it2=sysVarsMap.begin(); it2!=sysVarsMap.end(); it2++){
      
      if( TString((*it2).first).Contains((*it).c_str())){
        vars=(*it2).second;
        break ;
      }}
    for(std::vector<std::pair<string,string>>::iterator it2=vars.begin(); it2!=vars.end(); it2++){
      if(  !TString((*it2).first.c_str()).Contains("High_")) continue; 
      string var=eraseSubStr((*it2).first,*it);
      var=eraseSubStrToEnd(var,"High_");
      bool isThere=false;
      for(vector<string>::iterator it3=loadedSystematics.begin(); it3!=loadedSystematics.end(); it3++){
        if( (*it3).compare(var)==0) { isThere=true; break; } 
      }
      if(!isThere){
        myfile << var.c_str()<<endl;
        loadedSystematics.push_back(var);
      }
    }      
    myfile.close();
  }

  // Peform smart rebbinning step
  // observables to rebin 
  // bdt_vbf
  std::map<string, vector<string>> hVecMap;

  for(vector<string>::iterator it=TH1FNames.begin(); it!=TH1FNames.end(); it++){
     TH1F *h=(TH1F*)f->Get( (*it).c_str());
    //    cout<<" Checkhing histogram "<<(*it)<<endl;
    if( h==nullptr) continue;
    // keep only the obervable ones 
    if( ( !(TString(h->GetName())).Contains("obs"))) continue; 
    // keep only the nomninals 
    if( !TString(h->GetName()).Contains("Nom")) continue ; 
    // Keep only the regions we care about
    if (!TString(h->GetName()).Contains("SRVBF")) continue; 
    // keep only distribution we want to look at 
    if( TString(h->GetName()).Contains("obs_bdt_vbf") && !TString(h->GetName()).Contains("obs_bdt_vbfggf")){
      const string region=whichRegion( h->GetName(),regions);
      hVecMap[region].push_back(h->GetName());
    }
  }
  // now loop over all regions to consider

  
  for(std::map<string, vector<string>>::iterator it= hVecMap.begin(); it!=hVecMap.end(); it++){
    // start from a base rebinning
    vector <double> xbins={0.5,0.7,0.86,0.88,0.90,0.92,0.94,0.96,0.98,1};
    // loop over all histograms
   
    for(std::vector<string>::iterator h2=(*it).second.begin(); h2!=(*it).second.end(); h2++){
      if( xbins.size()-1 < 5) {
        break ;
      }
      TH1F *hh=(TH1F*)f->Get( (*h2).c_str());
      
      cout<<"Rebinning region "<<(*it).first<<" h name "<< hh->GetName()<<" to "<<xbins.size()-1<<" bins "<<endl;
      vector <double> xbins2;
     
      TH1F *hRebin=(TH1F*)hh->Rebin(xbins.size()-1,Form("%s_%s",hh->GetName(),"TMPRBH"),&xbins[0]);
      xbins2.push_back(hRebin->GetBinLowEdge(1)); 
      for( int b=1; b<(int)hRebin->GetNbinsX()+1; b++){
        double val=hRebin->GetBinContent(b);
        if( val > 0 && ( fabs(hRebin->GetBinError(b) / hRebin->GetBinContent(b)) < 1.0 ) ){
          if( xbins2[0] != hRebin->GetBinLowEdge(b)){
            xbins2.push_back(hRebin->GetBinLowEdge(b));
          }}
        else
          continue; 
      }
      xbins2.push_back(1);
      
      //if( xbins2.size() > 5) {
      xbins.clear();
      xbins=xbins2;
      //}
      delete hRebin; 
    }
    
    xbinsMap[(*it).first]=xbins;
    cout<<"Edges :";
    for(int i=0; i<(int)xbins.size(); i++)
      cout<<" "<<xbins[i];
    cout<<endl; 
    cout<<"Rebinning Region "<<(*it).first<<" has "<<xbins.size()<<" bins "<<endl;
  }  

  ofstream myfileBins;
  myfileBins.open(Form("%s_binning.txt",eraseSubStr(tname,".root").c_str()));
  for(std::map<string,vector<double>>::iterator it=xbinsMap.begin(); it!=xbinsMap.end(); it++){
    vector <double> xbins=(*it).second;
    myfileBins<<(*it).first<<" "<<xbins.size()-1;
      for(int k=0; k<(int)xbins.size(); k++)
        myfileBins<<" "<<xbins[k];
      myfileBins<<endl;
  }
  myfileBins.close();
  
  

  
  
cout<<"Checking for empty bins  "<<endl;

// loop over all histograms
  for(vector<string>::iterator it=TH1FNames.begin(); it!=TH1FNames.end(); it++){
    //load the TH1F
    TH1F *h=(TH1F*)f->Get( (*it).c_str());
    //    cout<<" Checkhing histogram "<<(*it)<<endl;
    if( h==nullptr) continue;
    //if( (TString(h->GetName())).Contains("Norm") && !(TString(h->GetName())).Contains("obs")) continue; 
    if( ( !(TString(h->GetName())).Contains("obs"))) continue; 
    
    bool HasEmptyBins=false;

    /*
    // replace empty bins with fixed value 
    for(int b=1; b<(int)h->GetNbinsX()+1; b++){

      //if(h->GetBinContent(b)<0)
      //h->GetBinContent(b,0);
      
      if ( h->GetBinContent(b)==0 ){

        //HasEmptyBins=true;
        //h->SetBinContent(b,emptyReplace);

        if(b>1){
          int lowNonEmpty=b;
          for(int bb=b-1; bb>0 ; bb--){
            if(h->GetBinContent(bb)!=0){
              lowNonEmpty=bb; break;
            }}
          int highNonEmpty=b;
          for( int bb=b+1; bb<(int)h->GetNbinsX()+1; bb++){
            if(h->GetBinContent(bb)!=0){
              highNonEmpty=bb; break;
            }}
          
          if(highNonEmpty!=b && lowNonEmpty!=b)
            h->SetBinContent(b,0.5*(h->GetBinContent(lowNonEmpty)+h->GetBinContent(highNonEmpty)));
          else if (highNonEmpty==b && lowNonEmpty!=b)
            h->SetBinContent(b,h->GetBinContent(lowNonEmpty));
          else if (highNonEmpty!=b && lowNonEmpty==b)
            h->SetBinContent(b,h->GetBinContent(highNonEmpty));
        }

        else if (b<1){
          // find the first non empty bin
          for(int bb=b+1; bb<(int)h->GetNbinsX()+1; bb++){
            if (h->GetBinContent(bb)!=0){
              h->SetBinContent(b,h->GetBinContent(bb));
              break;
            }}
        }
        
      }
    }
    */

    //if( h->GetNbinsX()>2 &&  h->GetEntries() / h->GetNbinsX() < 100.00  ) 
    //h->Smooth(1);
    /*
    for(int b=1; b<(int)h->GetNbinsX()+1; b++){
      if ( h->GetBinContent(b)==0 ){
        h->SetBinContent(b,emptyReplace);
        HasEmptyBins=true;
      }
    }
    */

    

    /*
    if(!TString(h->GetName()).Contains("Norm")){
      if( TString(h->GetName()).Contains("JER") && !TString(h->GetName()).Contains("Region")){
        if(h->GetNbinsX()>3) 
          h->Smooth(2,"R");
        else
          h->Smooth(1,"R");
      }}
    //else
    //h->Smooth(1,"R")
    */

    bool Smooth=false; 

    /*
    if(TString(h->GetName()).Contains("htop") && TString(h->GetName()).Contains("SRVBF") && TString(h->GetName()).Contains("theo") )
      Smooth=true; 
    if(TString(h->GetName()).Contains("hhtt"))
      Smooth=true;
    if(TString(h->GetName()).Contains("hvh"))
      Smooth=true;
    if(TString(h->GetName()).Contains("hvbf0") && !TString(h->GetName()).Contains("SRVBF"))
      Smooth=true;
    if( (TString(h->GetName()).Contains("hZjets") || TString(h->GetName()).Contains("hdiboson") )  && TString(h->GetName()).Contains("theo") && (TString(h->GetName()).Contains("SRVBF") || TString(h->GetName()).Contains("CRTop")))
      Smooth=true;
    //if(TString(h->GetName()).Contains("hdiboson") && TString(h->GetName()).Contains("theo"))
    //Smooth=true;
    if(TString(h->GetName()).Contains("hVgamma"))
      Smooth=true;
    */

    if(Smooth){
      //double lowRange=h->GetBinLowEdge(1);
      //double highRange=h->GetBinLowEdge(h->GetNbinsX()-3);
      //h->GetXaxis()->SetRangeUser(lowRange,highRange);
      //h->Smooth(1,"R");
      //h->GetXaxis()->SetRangeUser(h->GetBinLowEdge(1),h->GetBinLowEdge(h->GetNbinsX()+1));
      h->Smooth(1);
    }
    

    // Rebin the vbf signal region histograms 
    if( TString(h->GetName()).Contains("obs_bdt_vbf") && !TString(h->GetName()).Contains("obs_bdt_vbfggf")){
      int nBins=9;
      vector <double> xbins={0.5,0.7,0.86,0.88,0.90,0.92,0.94,0.96,0.98,1};
      const string region=whichRegion(h->GetName(),regions);
      if(isDiff){
        //nBins=4;
        //xbins.clear();
        //xbins={0.5,0.7,0.86,0.96,1};
        xbins.clear();
        xbins=xbinsMap[region]; 
        nBins=xbins.size()-1;
      }
      string oldName=h->GetName();
      h->SetName(Form("%s_UnRB",h->GetName()));
      TH1F *hrwb=(TH1F*)h->Rebin(nBins,(oldName+"_RBR").c_str(),&xbins[0]);
      //for( int lk=1; lk<(int)h->GetNbinsX()+1; lk++)
      //hrwb->SetBinContent(lk,hrwb->GetBinContent(lk) / hrwb->GetBinWidth(lk) * (h->GetBinLowEdge(h->GetNbinsX()+1) - h->GetBinLowEdge(1)) / hrwb->GetNbinsX() );
      // define the remmapped histogram
      TH1F *hnew=new TH1F(oldName.c_str(),oldName.c_str(),nBins,0.5,1);
      hnew->GetXaxis()->SetTitle("#{D}_{VBF}");
      hnew->GetYaxis()->SetTitle(h->GetYaxis()->GetTitle());
      for( int i=0; i<(int)hrwb->GetNbinsX()+1; i++){
        hnew->SetBinContent(i,hrwb->GetBinContent(i));
        hnew->SetBinError(i,hrwb->GetBinError(i));
      }
      hnew->Write();
      delete hnew;
      hrwb->Write();
      delete hrwb;
      //h->Rebin(5);
    }
    
    if (HasEmptyBins)
      cout<<" histogram "<<h->GetName()<<" has empty bins "<<endl;
    
    h->Write();

    
    delete h; 
  }




  cout<<"Starting the loop over systematics now "<<endl;
  cout<<"Make clones of the raw Histograms for all systematics"<<endl;
  for( std::map<string, std::vector<std::pair<string,string>>>::iterator it=sysVarsMap.begin(); it!=sysVarsMap.end(); it++){
   

    TH1F *hnom=(TH1F*)f->Get( (*it).first.c_str());
    if(hnom==nullptr) { 
      cout<<"nominal histogram "<<(*it).first<<" is a null pointer "<<endl;
      continue;
    }

    int nRebin=2;
    while ( !(hnom->GetNbinsX() % nRebin == 0) )
      {
        nRebin++;
	if (hnom->GetNbinsX() <2)
	  break;
      }
    cout<<"-- nRebinning will be "<<nRebin<<" for "<<hnom->GetNbinsX()<<endl;
    
    vector <std::pair<string, string>> vars=(*it).second;
    for( vector<std::pair<string, string>>::iterator it2=vars.begin(); it2!=vars.end(); it2++){
     

      TH1F *hup=nullptr;
      hup=(TH1F*)f->Get((*it2).first.c_str());
      if(hup ==nullptr) cout<<(*it2).first<<" is null "<<endl; 
      TH1F *hdown=nullptr;
      hdown=(TH1F*)f->Get((*it2).second.c_str());

      if(hdown ==nullptr) cout<<(*it2).second<<" is null "<<endl; 
      // chechk if up and down variations exist 

      if( hup==nullptr && hdown!=nullptr) {
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        cout<<"Hup did not exist replacing it by downvariation for"<<(*it2).first<<endl;
      }
      
      else if ( hup!=nullptr && hdown==nullptr){
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"Hdown does not exist replacing it bu upvariation for "<<(*it2).second<<endl;
      }
      else if (hup==nullptr && hdown==nullptr){
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"Norm Both variations do not exist, created ad hoc variation"<<endl;
      }

      // make some fixes that need to be propaged to both 
      for(int bin=1; bin<(int)hnom->GetNbinsX()+1; bin++){
        double nomVal=hnom->GetBinContent(bin);
        double highVal=hup->GetBinContent(bin);
        double lowVal=hdown->GetBinContent(bin);
        if( nomVal==0){
          nomVal=emptyReplace; 
        }
        // fix one sided variations to be fixed further down the line 
        //if( (highVal/nomVal > 1 && lowVal/nomVal >1) ||  (highVal/nomVal < 1 && lowVal/nomVal < 1)  ) {
        //double symmUncert=0.5*fabs(  highVal - lowVal ) ;
        //highVal=nomVal+symmUncert;
        //lowVal=nomVal-symmUncert; 
        //}
        //hdown->SetBinContent(bin,lowVal);
        //hup->SetBinContent(bin,highVal);
      }
      
      TH1F *hrup=(TH1F*)hup->Clone(Form("%s%s",(*it2).first.c_str(),"_RAW"));
      TH1F *hrdown=(TH1F*)hdown->Clone(Form("%s%s",(*it2).second.c_str(),"_RAW"));

      TH1F *hrupRB=(TH1F*)hup->Clone(Form("%s%s",(*it2).first.c_str(),"_RB"));

      hrupRB->Rebin(nRebin);

      TH1F *hdownRB=(TH1F*)hdown->Clone(Form("%s%s",(*it2).second.c_str(),"_RB"));
      hdownRB->Rebin(nRebin);
            
      hrup->Write(Form("%s%s",(*it2).first.c_str(),"_RAW"));
      delete hrup;
      delete hup;

      hrdown->Write(Form("%s%s",(*it2).second.c_str(),"_RAW"));
      delete hrdown;
      delete hdown;

      hrupRB->Write(Form("%s%s",(*it2).first.c_str(),"_RB"));
      delete hrupRB;
      
      hdownRB->Write(Form("%s%s",(*it2).second.c_str(),"_RB"));
      delete hdownRB;
      
    }

    TH1F *hrNom=(TH1F*)hnom->Clone(Form("%s%s",(*it).first.c_str(),"_RAW"));
    hrNom->Write(Form("%s%s",(*it).first.c_str(),"_RAW"));

    TH1F *hrNomRB=(TH1F*)hnom->Clone(Form("%s%s",(*it).first.c_str(),"_RB"));
    hrNomRB->Rebin(nRebin);
    hrNomRB->Write(Form("%s%s",(*it).first.c_str(),"_RB"));

    delete hrNomRB;
    delete hrNom;
    delete hnom;
  }

  f->Close();
  delete f;
  f=new TFile(tname.c_str(),"UPDATE");


  // Smoothing and else
  //const string sample=whichSample( h->GetName() ,samples,samples2); 
  //const string region=whichRegion( (*it).first,regions);
  //string variation= whichVariation( h->GetName() ,sample,region);
  for( std::map<string, std::vector<std::pair<string,string>>>::iterator it=sysVarsMap.begin(); it!=sysVarsMap.end(); it++){

    cout<<endl;
    cout<<" Iterating on "<<(*it).first<<endl;
    const string sample=whichSample( (*it).first,samples,samples2); 
    const string region=whichRegion( (*it).first,regions);
    string nnName=(*it).first;
    const string thisObs=whichObs( nnName , "obs_");
    vector <string> nnormRegions;
    bool hasAbnormalValues=false; 

    TH1F *hnom=(TH1F*)f->Get( (*it).first.c_str());
    TH1F *hnomRB=(TH1F*)f->Get(Form("%s_RB",(*it).first.c_str()));

    if(hnom==nullptr) { 
      cout<<"nominal histogram "<<(*it).first<<" is a null pointer "<<endl;
      continue;
    }

    vector <std::pair<string, string>> vars=(*it).second;
    for( vector<std::pair<string, string>>::iterator it2=vars.begin(); it2!=vars.end(); it2++){
      
      string variation= whichVariation( (*it2).second ,sample,region);
      cout<<"Variation is "<<variation<<endl;
      cout<<"   with histograms    "<<(*it2).first<<" up "<<(*it2).second<<" down  "<<(*it).first<<" nominal "<<endl;
      if(variation.compare("unknown")==0){
        cout<<"       Unkown variation  .. skipping "<<endl;
        continue ;
      }
      cout<<"Sample is: "<<sample;

      TH1F *hup=nullptr;
      hup=(TH1F*)f->Get((*it2).first.c_str());
      TH1F *hupRB=nullptr;
      hupRB=(TH1F*)f->Get(Form("%s_RB",(*it2).first.c_str()));

      TH1F *hdown=nullptr;
      hdown=(TH1F*)f->Get((*it2).second.c_str());
      TH1F *hdownRB=nullptr;
      hdownRB=(TH1F*)f->Get(Form("%s_RB",(*it2).second.c_str()));
      
      
      // chechk if up and down variations exist 
      if( hup==nullptr && hdown!=nullptr) {
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        cout<<"       Hup did not exist replacing it by downvariation for"<<(*it2).first<<endl;
      }
      
      else if ( hup!=nullptr && hdown==nullptr){
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"       Hdown does not exist replacing it bu upvariation for "<<(*it2).second<<endl;
      }
      else if (hup==nullptr && hdown==nullptr){
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"       Norm Both variations do not exist, created ad hoc variation"<<endl;
      }
      
      // now loop over the nominal sample bins 
      if(hup!=nullptr && hdown!=nullptr && hnom!=nullptr){
        
        cout<<"Histogram names "<<hnom->GetName()<<" upVar "<<hup->GetName()<<" -- down "<<hdown->GetName()<<endl;
        cout<<"Input integrals: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<" "<<hnom->Integral()<<" nominal "<<endl;
        
        if(hup->Integral()==0) {
          delete hup;
          hup=(TH1F*)hnom->Clone((*it2).first.c_str());
          cout<<"       up variation had 0 integral replacing it with nonminal"<<endl;
        }
        
        if(hdown->Integral()==0){
          delete hdown;
          hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
          cout<<"       down variation had 0 integral replacing it with nonminal"<<endl;
        }

        
        double integralup=hup->Integral();
        double integraldown=hdown->Integral();

        bool symmetrise=false;
        // symmetrise if normalisation uncertaintiy is lower than 1% 
        if( fabs(1- hup->Integral()/hnom->Integral()) < 0.01 || fabs(1- hdown->Integral()/hnom->Integral()) < 0.01)
          symmetrise=true; 
        // symmetrize also if one side is larger than the other by more than 50%
        if( fabs(fabs(1- hup->Integral()/hnom->Integral())  - fabs(1- hdown->Integral()/hnom->Integral())) > 0.5)
          symmetrise=true;


        
        for(int bin=1; bin<(int)hnom->GetNbinsX()+1; bin++){

          double nomVal=hnom->GetBinContent(bin);
          double highVal=hup->GetBinContent(bin);
          double lowVal=hdown->GetBinContent(bin);

          
          if( nomVal==0){
            nomVal=emptyReplace;
            highVal=emptyReplace;
            lowVal=emptyReplace;
           }

          cout<<"    Original values: Bin "<<bin<<" nom "<<nomVal<<" high "<<highVal<<" low "<<lowVal<<endl;

          double nomValRB=hnomRB->GetBinContent(hnomRB->FindBin(hnom->GetBinCenter(bin)));
          double highValRB=hupRB->GetBinContent(hnomRB->FindBin(hnom->GetBinCenter(bin)));
          double lowValRB=hdownRB->GetBinContent(hnomRB->FindBin(hnom->GetBinCenter(bin)));

          // Use Rebinned Values for certain variations:
          // Zjets theory
          bool useRebin=false;
          bool useAverage=false; 
          if( (sample.compare("hZjets0")==0 || sample.compare("hZjets1")==0) && TString(variation.c_str()).Contains("theo"))
            if( !TString( hnom->GetName() ).Contains("obs_MT") && !TString( hnom->GetName() ).Contains("obs_bdt_vbf"))
              useRebin=true;

          if( TString(variation.c_str()).Contains("theo") && TString( hnom->GetName() ).Contains("obs_bdt") && !TString( hnom->GetName() ).Contains("obs_bdt_vbf"))
            useRebin=true;

          //determine if the variation is higher by the average variations excluding this bin, if yes user the rebin
          double lhighVal=0;
          double llowVal=0;

          double RMShigh=0;
          double RMSLow=0;
          
          for(int lbin=1; lbin<(int)hnom->GetNbinsX()+1; lbin++){
            if(lbin==bin) continue; 
            if( useRebin){
              lhighVal+=fabs(1- hupRB->GetBinContent(hupRB->FindBin(hup->GetBinLowEdge(lbin)))/ hnomRB->GetBinContent(hnomRB->FindBin(hnom->GetBinLowEdge(lbin))));
              llowVal+=fabs(1-hdownRB->GetBinContent(hdownRB->FindBin(hdownRB->GetBinLowEdge(lbin)))/ hnomRB->GetBinContent(hnomRB->FindBin(hnom->GetBinLowEdge(lbin))));
            }
            else {
              lhighVal+=fabs(1-hup->GetBinContent(lbin)/hnom->GetBinContent(lbin));
              llowVal+=fabs(1-hdown->GetBinContent(lbin)/hnom->GetBinContent(lbin));
            }
          }

          lhighVal=lhighVal/(hnom->GetNbinsX()-1);
          llowVal=llowVal/(hnom->GetNbinsX()-1);
          
          
          for(int lbin=1; lbin<(int)hnom->GetNbinsX()+1; lbin++){
            if(lbin==bin) continue;
            cout<<"RMSLow "<<RMSLow<<" RMSHigh" <<RMShigh<<endl;
            if(useRebin) {

              RMShigh+=pow(  lhighVal   -   1-hupRB->GetBinContent(hupRB->FindBin(hup->GetBinLowEdge(lbin)))/ hnomRB->GetBinContent(hnomRB->FindBin(hnom->GetBinLowEdge(lbin))),2);
              RMSLow+=pow(   llowVal     -   1-hdownRB->GetBinContent(hdownRB->FindBin(hdownRB->GetBinLowEdge(lbin)))/ hnomRB->GetBinContent(hnomRB->FindBin(hnom->GetBinLowEdge(lbin))),2);
              
            }
            else{
              RMShigh+=pow(  lhighVal   -   1-hup->GetBinContent(lbin)/hnom->GetBinContent(lbin),2);
              RMSLow+=pow(   llowVal    -   1-hdown->GetBinContent(lbin)/hnom->GetBinContent(lbin),2);
            }
          }
          cout<<"RMSLow "<<RMSLow<<" RMSHigh" <<RMShigh<<endl; 
          
          
          RMSLow=sqrt( 1  / (hnom->GetNbinsX()-1) * RMSLow  );
          RMShigh=sqrt( 1 / (hnom->GetNbinsX()-1) * RMShigh );
          
          cout<<" RMS "<<lhighVal <<" highVal "<<1-highVal/nomVal<<" RMS high " <<RMShigh<<" ----  llow "
              <<llowVal <<" highVal "<<1-lowVal/nomVal<<" RMS high " <<RMSLow<<endl;
          
          if( fabs( lhighVal  -  1-highVal/nomVal)  / RMShigh  > 2.5 || fabs( llowVal  -  1-lowVal/nomVal)  / RMSLow  > 2.5 ){
            cout<<"Uncertainty higher than 1.5 sigma of the average using rebinned values lhigh "<<lhighVal/(hnom->GetNbinsX()-1)<<" highVal "<<highVal/nomVal<<" RMS high " <<RMShigh<<" ----  llow "
                <<llowVal/(hnom->GetNbinsX()-1)<<" highVal "<<lowVal/nomVal<<" RMS high " <<RMSLow<<endl;
            useAverage=true;
      }

          if( useRebin){
            cout<<"Using rebinned high :" << (nomValRB - highValRB)/nomValRB<< " * "<<nomVal<<" --> "<<(nomValRB - highValRB)/nomValRB * nomVal<<endl;
            cout<<"Using rebinned low :" << (nomValRB - lowValRB)/nomValRB<< " * "<<nomVal<<" --> "<<(nomValRB - lowValRB)/nomValRB * nomVal<<endl;

            highVal= (nomValRB - highValRB)/nomValRB * nomVal;
            lowVal =  (nomValRB - lowValRB)/nomValRB * nomVal ;
          }

          if(useAverage) {
            cout<<" Setting to average error high "<< (1 - lhighVal) << " * "<<nomVal<<endl;
            cout<<" Setting to average error low "<<  (1 - llowVal) << " * "<<nomVal<<endl;
            double direction= 0; 
            double directionBinLow = hdown->GetBinContent(bin -1) > hnom->GetBinContent(bin-1)  ? 1:-1;
            double directionBinHigh= hup->GetBinContent(bin+1) > hnom->GetBinContent(bin+1) ? 1:-1; 
            direction= 1/(hnom->GetBinContent(bin-1)+hnom->GetBinContent(bin+1)) * (directionBinLow*hnom->GetBinContent(bin-1) + directionBinHigh*hnom->GetBinContent(bin+1)); 
            cout<<"Directions up: "<<directionBinHigh<<" down: "<<directionBinLow<<" average direction "<<direction<<endl;
            
            highVal= nomVal  + direction*fabs(lhighVal)* nomVal;
            lowVal = nomVal  - direction*fabs(llowVal)* nomVal;
          }
          
         

          bool symmetriseBin=false;

          if( fabs( 1-highVal/nomVal) > 0.3 || fabs(1-lowVal/nomVal) > 0.3) 
            symmetriseBin=true; 

          // both variations go into the same direction, symmetrise 
          if( ( 1-highVal/nomVal > 0 && 1 - lowVal/nomVal > 0) ||  (1-highVal/nomVal < 0 && 1 - lowVal/nomVal <0))
            symmetriseBin=true; 
          
          // fix one sided variations
          if(  symmetriseBin || symmetrise ) {
            cout<<"Both variations go into the same direction symmetrising bins"<<endl;
            //double symmUncert=0.5*fabs(  fabs(1-highVal/nomVal)  + fabs(1-lowVal/nomVal));
            double symmUncert=0.5*fabs( highVal - lowVal )/nomVal ;
            cout<<" Symm rel uncertainty "<<symmUncert<<endl;
            double direction= 0; 
            double directionBinLow = hdown->GetBinContent(bin -1) > hnom->GetBinContent(bin-1)  ? 1:-1;
            double directionBinHigh= hup->GetBinContent(bin+1) > hnom->GetBinContent(bin+1) ? 1:-1; 
            direction= 1/(hnom->GetBinContent(bin-1)+hnom->GetBinContent(bin+1)) * (directionBinLow*hnom->GetBinContent(bin-1) + directionBinHigh*hnom->GetBinContent(bin+1)); 
            cout<<"Directions up: "<<directionBinHigh<<" down: "<<directionBinLow<<" average direction "<<direction<<endl;

            highVal=nomVal + direction*symmUncert*nomVal;
            lowVal=nomVal -  direction* symmUncert*nomVal; 
          }
          
          
          hdown->SetBinContent(bin,lowVal);
          hup->SetBinContent(bin,highVal);
          cout<<"    New val Bin "<<bin<<" nom "<<nomVal<<" high "<<highVal<<" low "<<lowVal<<endl;
        }// end loop on bins 

        cout<<"    Integrals after fluctuations replacement: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;
        cout<<"    Normalisation uncertainty : "<<hnom->Integral()<<" up: "<<(1-hup->Integral()/hnom->Integral())*100 <<" %  down: "<<(1-hdown->Integral()/hnom->Integral())*100<<endl;
        
        hdown->Write();
        hup->Write();

        TH1F *hupC=(TH1F*)hup->Clone(Form("%s%s",hup->GetName(),"_RAW2"));
        TH1F *hdownC=(TH1F*)hdown->Clone(Form("%s%s",hdown->GetName(),"_RAW2"));
        hupC->Write();
        delete hupC;
        hdownC->Write();
        delete hdownC; 
        
        // write there that this sample is affected by this systematic 
        if(hdown!=nullptr)delete hdown;
        if(hup!=nullptr) delete hup;
        delete hdownRB;
        delete hupRB;
        
      }
    }
    TH1F *hnomC=(TH1F*)hnom->Clone(Form("%s%s",hnom->GetName(),"_RAW2"));
    hnomC->Write();
    delete hnomC; 
    
    //hnom->Write();
    delete hnom;
    delete hnomRB;
  }

  f->Close();
  delete f;
  f=new TFile(tname.c_str(),"UPDATE");

 
  // Now loop again over all systematics for normalisation 
  //std::map<string, std::vector<std::pair<string,string>>> sysVarsMap;
  for( std::map<string, std::vector<std::pair<string,string>>>::iterator it=sysVarsMap.begin(); it!=sysVarsMap.end(); it++){

    cout<<endl;
    cout<<" Iterating on "<<(*it).first<<endl;
    const string sample=whichSample( (*it).first,samples,samples2); 
    const string region=whichRegion( (*it).first,regions);
    string nnName=(*it).first;
    const string thisObs=whichObs( nnName , "obs_");
    vector <string> nnormRegions;
    bool hasAbnormalValues=false; 
    TH1F *hnom=(TH1F*)f->Get( (*it).first.c_str());
    TH1F *hnomRB=(TH1F*)f->Get(Form("%s_RB",(*it).first.c_str()));

    if(hnom==nullptr) { 
      cout<<"nominal histogram "<<(*it).first<<" is a null pointer "<<endl;
      continue;
    }
    
    // determine if sample is to be normalised to their TF
    bool toNorm=false;
    for(std::map<string,vector<string>>::iterator il=sampleNormMap.begin(); il!=sampleNormMap.end(); il++){
      cout<<"------->Comparing "<<(*il).first<<" to "<<sample<<endl;
      if( (*il).first.compare(sample) == 0  ) { toNorm=true; break; } 
    }

    cout<<"Sample is: "<<sample;
    if(toNorm){
      nnormRegions=sampleNormMap[sample];
      cout<<" with normalisation to: ";
      for(vector<string>::iterator lm=nnormRegions.begin(); lm!=nnormRegions.end(); lm++)
        cout<<" "<<(*lm);
      cout<<endl;
    }
    else
      cout<<" normalised to theory "<<endl; 
    
    vector <std::pair<string, string>> vars=(*it).second;
    for( vector<std::pair<string, string>>::iterator it2=vars.begin(); it2!=vars.end(); it2++){
      
      string variation= whichVariation( (*it2).second ,sample,region);
      cout<<"Variation is "<<variation<<endl;
      cout<<"   with histograms    "<<(*it2).first<<" up "<<(*it2).second<<" down  "<<(*it).first<<" nominal "<<endl;
      if(variation.compare("unknown")==0){
        cout<<"       Unkown variation  .. skipping "<<endl;
        continue ;
      }
      cout<<"Sample is: "<<sample;
      if(toNorm){
        cout<<" with normalisation to: ";
        for(vector<string>::iterator lm=nnormRegions.begin(); lm!=nnormRegions.end(); lm++)
          cout<<" "<<(*lm);
        cout<<endl;
      }
      else
        cout<<" normalised to theory "<<endl; 
      

      TH1F *hup=nullptr;
      hup=(TH1F*)f->Get((*it2).first.c_str());
      TH1F *hupRB=nullptr;
      hupRB=(TH1F*)f->Get(Form("%s_RB",(*it2).first.c_str()));
      
      
      TH1F *hdown=nullptr;
      hdown=(TH1F*)f->Get((*it2).second.c_str());
      TH1F *hdownRB=nullptr;
      hdownRB=(TH1F*)f->Get(Form("%s_RB",(*it2).second.c_str()));
      
      /*
      // chechk if up and down variations exist 
      if( hup==nullptr && hdown!=nullptr) {
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        cout<<"       Hup did not exist replacing it by downvariation for"<<(*it2).first<<endl;
      }
      
      else if ( hup!=nullptr && hdown==nullptr){
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"       Hdown does not exist replacing it bu upvariation for "<<(*it2).second<<endl;
      }
      else if (hup==nullptr && hdown==nullptr){
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"       Norm Both variations do not exist, created ad hoc variation"<<endl;
      }
      */
      
      // now loop over the nominal sample bins 
      if(hup!=nullptr && hdown!=nullptr && hnom!=nullptr){
        
        cout<<"Histogram names "<<hnom->GetName()<<" upVar "<<hup->GetName()<<" -- down "<<hdown->GetName()<<endl;
        cout<<"Input integrals: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<" "<<hnom->Integral()<<" nominal "<<endl;

        /*
        if(hup->Integral()==0) {
          delete hup;
          hup=(TH1F*)hnom->Clone((*it2).first.c_str());
          cout<<"       up variation had 0 integral replacing it with nonminal"<<endl;
        }
        
        if(hdown->Integral()==0){
          delete hdown;
          hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
          cout<<"       down variation had 0 integral replacing it with nonminal"<<endl;
        }
        */
        
        bool isSignalInSignalRegion = TString(variation.c_str()).Contains("theo") && sample.compare("hvbf0")==0 && ( region.compare("SRVBF")==0 || region.compare("CRTop") || region.compare("CRWW")==0 ); 
        if(isDiff) {
          isSignalInSignalRegion=TString(variation.c_str()).Contains("theo") && TString(sample.c_str()).Contains("hvbf0")==0 && ( TString(region.c_str()).Contains("SRVBF_") || TString(region.c_str()).Contains("CRTop_") || TString(region.c_str()).Contains("CRWW") );
        }
        
        double integralup=hup->Integral();
        double integraldown=hdown->Integral();

        
        
        for(int bin=1; bin<(int)hnom->GetNbinsX()+1; bin++){

          double nomVal=hnom->GetBinContent(bin);
          double highVal=hup->GetBinContent(bin);
          double lowVal=hdown->GetBinContent(bin);

          //cout<<"    Original values: Bin "<<bin<<" nom "<<nomVal<<" high "<<highVal<<" low "<<lowVal<<endl;

          double nomValRB=hnomRB->GetBinContent(hnomRB->FindBin(hnom->GetBinCenter(bin)));
          double highValRB=hupRB->GetBinContent(hnomRB->FindBin(hnom->GetBinCenter(bin)));
          double lowValRB=hdownRB->GetBinContent(hnomRB->FindBin(hnom->GetBinCenter(bin)));
          
          hdown->SetBinContent(bin,lowVal);
          hup->SetBinContent(bin,highVal);
          //cout<<"    New val Bin "<<bin<<" nom "<<nomVal<<" high "<<highVal<<" low "<<lowVal<<endl;

          // check for abnormalites 
          if(  fabs(1-lowVal/nomVal) > 2 * fabs(1-highVal/nomVal) || fabs( 1-highVal/nomVal) > 2* fabs(1-lowVal/nomVal) ) 
            hasAbnormalValues=true;
          if ( fabs(1-lowVal/nomVal) > 1.5 * sqrt(nomVal)  || fabs( 1-highVal/nomVal) > 1.5 * sqrt(nomVal) )
            hasAbnormalValues=true; 
        }

        cout<<"    Integrals after fluctuations replacement: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;
        cout<<"    Integrals after re normalisations of fluctuations replacement: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;
        
        // normalise the samples according to their tranfer factors
        if( toNorm || isSignalInSignalRegion ){
          
          vector <string> nnormRegions=sampleNormMap[sample];
          cout<<" Normalising to (control) regions: "<<endl;
          for(vector<string>::iterator lm=nnormRegions.begin(); lm!=nnormRegions.end(); lm++)
            cout<<" "<<(*lm);
          cout<<endl;
          
          // Get the raw variations
          double integralUp=0;
          double integralDown=0; 
          double integralNom=0;
          
          
          for(vector<string>::iterator lm=nnormRegions.begin(); lm!=nnormRegions.end(); lm++){
            cout<<"Processing region "<< (*lm)<<endl;
            
	    string histToNomUp=sample+variation+"High_"+ (*lm) + "_obs_"+obsRegions[ (*lm)];
	    string histToNomDown=sample+variation+"Low_"+ (*lm) + "_obs_"+obsRegions[ (*lm)];
	    string nomHisCR=sample+"Nom_"+ (*lm) + "_obs_"+obsRegions[ (*lm)];
	    std::cout<<"retrieving histogram "<<nomHisCR<<"\n";
	    TH1F *hnomHisCR=(TH1F*)f->Get(Form("%s%s",nomHisCR.c_str(),"_RAW2"));
	    if(hnomHisCR==NULL)
	      continue;
            //TH1F *hnomHisCR=(TH1F*)f->Get(Form("%s",nomHisCR.c_str()));
	    cout<<"    Nominal norm hist is "<<hnomHisCR->GetName()<<"  "<<hnomHisCR->Integral() <<endl;
	    integralNom+=hnomHisCR->Integral();
            
            cout<<"    Requesing "<< Form("%s%s",histToNomUp.c_str(),"_RAW2")<<endl;
            
            TH1F *hRawUp=(TH1F*)f->Get(Form("%s%s",histToNomUp.c_str(),"_RAW2"));
            //TH1F *hRawUp=(TH1F*)f->Get(Form("%s",histToNomUp.c_str()));
            integralUp+=hRawUp->Integral();

            cout<<"    Requesing "<< Form("%s%s",histToNomDown.c_str(),"_RAW2")<<endl;
            TH1F *hRawDown=(TH1F*)f->Get(Form("%s%s",histToNomDown.c_str(),"_RAW2"));
            //TH1F *hRawDown=(TH1F*)f->Get(Form("%s",histToNomDown.c_str()));
            integralDown+=hRawDown->Integral(); 

            cout<<"    Up and down norm regions "<<hRawUp->GetName()<<" "<< hRawUp->Integral()<<" "<<hRawDown->GetName()<<" "<< hRawDown->Integral()<<endl;

            std::vector <pair<string, string>> rrs;
            rrs.push_back(make_pair("htop","hdiboson"));
            for(int lk=1; lk < 4 ; lk++)
              rrs.push_back(make_pair(Form("htop%d",lk),Form("hdiboson%d",lk)));

            bool isrrs=false;
            pair <string,string> thisrrS; 
            for(std::vector<pair<string,string>>::iterator ilkm = rrs.begin(); ilkm!=rrs.end(); ilkm++){
              if( sample.compare( (*ilkm).first) ==0 || sample.compare( (*ilkm).second) ==0 ) {
                isrrs=true;
                thisrrS=(*ilkm);
              }
            }
            
            
            // for WW and ttbar normalise to the sum of WW+ttbar in the given region
            //if( sample.compare( "htop") ==0 || sample.compare("hdiboson")==0 ) {
            if( isrrs){
              // find the integral of the other sample and add it to this onne
              TString otherName=nomHisCR.c_str() ;
              TH1F *hnomotherRaw=nullptr; 
              
              //if(sample.compare("htop")==0) {
              if( sample.compare( thisrrS.first)==0){
                //otherName.ReplaceAll("htop","hdiboson");
                otherName.ReplaceAll(thisrrS.first,thisrrS.second);
                
              }
              //else if (sample.compare("hdiboson")==0) {
              else if( sample.compare(thisrrS.second)==0){
                //otherName.ReplaceAll("hdiboson","htop");
                otherName.ReplaceAll(thisrrS.second,thisrrS.first);
              }

              cout<<"Getting "<<otherName.Data()<<endl;
              hnomotherRaw=(TH1F*)f->Get(Form("%s%s",otherName.Data(),"_RAW"));
              cout<<" Adding to integral nom "<<hnomotherRaw->Integral()<<" events "<<endl; 
              integralNom+=hnomotherRaw->Integral();
              integralUp+=hnomotherRaw->Integral();
              integralDown+=hnomotherRaw->Integral();
              
              delete hnomotherRaw;
            }
            cout<<"    Up and down norm regions "<<hRawUp->GetName()<<" "<< hRawUp->Integral()<<" "<<hRawDown->GetName()<<" "<< hRawDown->Integral()<<endl;


            
            //hup->Scale( hnom->Integral() / hup->Integral() * hRawUp->Integral() / hnomHisCR->Integral());
            //hdown->Scale( hnom->Integral() / hup->Integral() * hRawUp->Integral() / hnomHisCR->Integral());
            
            delete hnomHisCR; 
            delete hRawUp;
            delete hRawDown;
	  }

          cout<<"Total TF variation is "<<integralUp<<" "<<integralDown<<" nominal integral "<<integralNom<<endl;

          double regIntUp=hup->Integral();
          double regIntDown=hdown->Integral();
          double regNomInt=hnom->Integral(); 
          
          if(toNorm && !isSignalInSignalRegion){
            // normalise to nominal 
            hup->Scale(hnom->Integral()/hup->Integral());
            hdown->Scale(hnom->Integral()/hdown->Integral()); 
            // apply the TF variation 
            hup->Scale(  integralUp/integralNom *  regNomInt/regIntUp      );
            hdown->Scale( integralDown/integralNom  * regNomInt/ regIntDown  );
            //hup->Scale( hnom->Integral() / hup->Integral() *  integralNom/integralUp   );
            //hdown->Scale( hnom->Integral() / hdown->Integral() *  integralNom/integralDown );
          }
          else if( isSignalInSignalRegion) {
            
            // normalise the signal in the signal regions
            // NB need to add the other regions in case they exist 
            //if( TString(variation.c_str()).Contains("theo") && sample.compare("hvbf0")==0 && ( region.compare("SRVBF")==0 || region.compare("CRTop")==0 ) ) {
              cout<<"Signal being normalised to the signal region extracting the C factor uncertainties "<<endl;
              TH1F *hupC=nullptr;
              TString baseNameUp=hup->GetName();
              baseNameUp.ReplaceAll(thisObs.c_str(),"bdt_ggFCR1");
              baseNameUp.ReplaceAll(region,"SRVBF");
              cout<<"Asking for "<<baseNameUp.Data()<<endl;
              hupC=(TH1F*)f->Get(Form("%s_Fid",baseNameUp.Data()));
              
              TH1F *hdownC=nullptr;
              TString baseNameDown=hdown->GetName();
              baseNameDown.ReplaceAll(thisObs.c_str(),"bdt_ggFCR1");
              baseNameDown.ReplaceAll(region,"SRVBF");
              cout<<"Asking for "<<baseNameDown.Data()<<endl;
              hdownC=(TH1F*)f->Get(Form("%s_Fid",baseNameDown.Data()));
              
              TH1F *hnomC=nullptr;
              TString nomCName=hupC->GetName();
              nomCName.ReplaceAll("High","Nom");
              
              hnomC=(TH1F*)f->Get(nomCName.Data());
              
              if( hdownC == nullptr || hupC==nullptr || hnomC==nullptr) {
                if( hdownC ==nullptr) cout<<"Down is null ";
                if( hupC==nullptr ) cout<<"Up is null ";
                if( hnomC==nullptr) cout<<" Nom is null ";
                cout<<"one histogram is null, skipping"<<endl;
                
                delete hdownC; delete hupC; delete hnomC;
                continue ;
              }

              double CfactorNom=integralNom/hnomC->Integral();
              double CfactorUp=integralUp/hupC->Integral();
              double CfactorLow=integralDown/hdownC->Integral();

              double relShiftUp=CfactorUp/CfactorNom;
              double relShiftLow=CfactorLow/CfactorNom;

              double weightUp=hup->Integral() / integralUp ;
              double weightDown=hdown->Integral() / integralDown; 
              double weighNom=hnom->Integral() / integralNom; 

              cout<<"  Before  Cfactor   Normalisation uncertainty : "<<hnom->Integral()<<" up: "<<(1-hup->Integral()/hnom->Integral())*100 <<" %  down: "<<(1-hdown->Integral()/hnom->Integral())*100<<endl;
              
              hup->Scale( hnom->Integral()/hup->Integral() *  CfactorUp/CfactorNom);
              hdown->Scale( hnom->Integral()/hdown->Integral()  * CfactorLow/CfactorNom);
              
              cout<<"C factors nominal "<<CfactorNom<<" up: "<<CfactorUp<<" ("<< (1-CfactorUp/CfactorNom)*100<<"%) down: "<<CfactorLow<<" ("<<(1-CfactorLow/CfactorNom)*100<<"%)"<<endl;
              cout<<" Weights nom "<<hnom->Integral() / integralNom <<" up "<<weightUp<<" down "<<weightDown<<endl;
              cout<<"  After Cfactor   Normalisation uncertainty : "<<hnom->Integral()<<" up: "<<(1-hup->Integral()/hnom->Integral())*100 <<" %  down: "<<(1-hdown->Integral()/hnom->Integral())*100<<endl;
              if( fabs( 1- hup->Integral()/hnom->Integral()) > 0.20 || fabs( 1- hdown->Integral()/hnom->Integral()) > 0.2 )
                cout<<"          ATTENTION! C factor systematic "<<variation<<" for "<<sample<<" in "<<region<<" has abnormal values "<<endl;
              
              delete hupC;
              delete hdownC;
              delete hnomC; 
              //}
              
          }
        } 
        cout<<"    Itegrals after normalisation :"<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;
      }
      
      cout<<"    Normalisation uncertainty : "<<hnom->Integral()<<" up: "<<(1-hup->Integral()/hnom->Integral())*100 <<" %  down: "<<(1-hdown->Integral()/hnom->Integral())*100<<endl;
      if( hasAbnormalValues || ( fabs( 1- hup->Integral()/hnom->Integral()) > 0.20 || fabs( 1- hdown->Integral()/hnom->Integral()) > 0.2))
        cout<<"ATTENTION! systematic "<<variation<<" for "<<sample<<" in "<<region<<" has abnormal values "<<endl;
      
      
      hdown->Write();
      hup->Write();

      TH1F *hNormUp=(TH1F*)hup->Clone(Form("%sNorm",hup->GetName()));
      hNormUp->Write();
      delete hNormUp;
      
      TH1F *hNormDown=(TH1F*)hdown->Clone(Form("%sNorm",hnom->GetName()));
      hNormDown->Write();
      delete hNormDown;
      
      // write there that this sample is affected by this systematic 
      if(hdown!=nullptr)delete hdown;
      if(hup!=nullptr) delete hup;
      delete hdownRB;
      delete hupRB;

    }

    if(hnom!=nullptr) {
      hnom->Write();
      TH1F *hNormNom=(TH1F*)hnom->Clone(Form("%sNorm",hnom->GetName()));
      hNormNom->Write();
      delete hNormNom; 
      delete hnom;
      delete hnomRB;
      
    }
  }

 

  cout<<"Starting the loop over norm systematics now "<<endl;
  // Now loop again over all systematics 
  for( std::map<string, std::vector<std::pair<string,string>>>::iterator it=sysVarsNormMap.begin(); it!=sysVarsNormMap.end(); it++){
    continue ; 
    TH1F *hnom=(TH1F*)f->Get( (*it).first.c_str());
    if(hnom==nullptr) {
      cout<<"nominal histogram "<<(*it).first<<" is a null pointer "<<endl;
      continue;
    }
    
    //for(int b=1; b<(int)hnom->GetNbinsX()+1; b++){
    //if(hnom->GetBinContent(b)==0) hnom->SetBinContent(b,emptyReplace); 
    //}

    cout<<"Nominal is "<<hnom->GetName()<<endl;
    // loop over all the variations 

    vector <std::pair<string, string>> vars=(*it).second;
    for( vector<std::pair<string, string>>::iterator it2=vars.begin(); it2!=vars.end(); it2++){
      //continue ;
      cout<<"Norm Variation is "<<(*it2).first<<" "<<(*it2).second<<" nominal "<<(*it).first<<endl;

     
      
      TH1F *hup=nullptr;
      hup=(TH1F*)f->Get((*it2).first.c_str());
      TH1F *hdown=nullptr;
      hdown=(TH1F*)f->Get((*it2).second.c_str());
      // chechk if up and down variations exist 

      if( hup==nullptr && hdown!=nullptr) {
        //hup=(TH1F*)hdown->Clone((*it2).first.c_str());
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        cout<<"Hup did not exist replacing it by downvariation for "<<(*it2).first<<endl;
      }
      
      else if ( hup!=nullptr && hdown==nullptr){
        //hdown=(TH1F*)hup->Clone((*it2).second.c_str());
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"Norm Hdown does not exist replacing it by upvariation for "<<(*it2).second<<endl;
      }
      else if (hup==nullptr && hdown==nullptr){
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"Norm Both variations do not exist, created ad hoc variation"<<endl;
      }

      cout<<"Histogram "<<hnom->GetName()<<" upVar "<<hup->GetName()<<" -- down "<<hdown->GetName()<<endl;
      cout<<"Old integrals nominal (0) : "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;
      // now loop over the nominal sample bins 

      if(hup->Integral()==0) {
        delete hup;
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
      }
      
      if(hdown->Integral()==0){
        delete hdown;
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
      }
      
      

      if(hup!=nullptr && hdown!=nullptr && hnom!=nullptr){

        // sort out the integrals 
        //if( TString(hnom->GetName()).Contains("hdiboson")) {
        //hdown->Scale(hnom->Integral());
        //hup->Scale(hnom->Integral());
        //}
        
        for(int bin=1; bin<(int)hnom->GetNbinsX()+1; bin++){

          double nomVal=hnom->GetBinContent(bin);
          double highVal=hup->GetBinContent(bin);
          double lowVal=hdown->GetBinContent(bin);

          cout<<"Original values: Bin "<<bin<<" nom "<<nomVal<<" high "<<highVal<<" low "<<lowVal<<endl;
          
          if( nomVal==0){
            nomVal=emptyReplace; 
          }
          
          /*
          //highVal=nomVal*1.0001;
          //lowVal=nomVal*0.9999;

          // check abnormal sys
          if (  highVal==0 || fabs( (highVal-nomVal)/highVal ) > 1   ){
            cout<<"------> "<<hdown->GetName()<<"Has abnormal systematic for High "<<lowVal<<" for nominal " <<nomVal<<" high "<<highVal<<endl;
            if( bin > 1 && hup->Integral() > 0 )
              //highVal=hup->GetBinContent(bin-1)/hnom->GetBinContent(bin-1)*nomVal;
              highVal=nomVal;
            else 
              highVal=nomVal;
          }
          if(  lowVal ==0 || fabs( (lowVal-nomVal)/highVal) > 1 ){
            cout<<"------> "<<hdown->GetName()<<"Has abnormal systematic for low "<<lowVal<<" for nominal " <<nomVal<<" high "<<highVal<<endl;
            if( bin > 1 && hdown->Integral() > 0 )
              //lowVal=hdown->GetBinContent(bin-1)/hnom->GetBinContent(bin-1)*nomVal;
              lowVal=nomVal;
            else 
              lowVal=nomVal;
          }
          
          if( highVal==lowVal) {
            lowVal=nomVal;
            }*/

          if( fabs(highVal-nomVal) < hnom->GetBinError(bin))
            highVal=nomVal; 
          
          if( fabs(lowVal-nomVal) < hnom->GetBinError(bin))
            lowVal=nomVal; 
          

          if( nomVal==emptyReplace) {
            highVal=emptyReplace;
            lowVal=emptyReplace;
          }

          cout<<" New val Bin "<<bin<<" nom "<<nomVal<<" high "<<highVal<<" low "<<lowVal<<endl;
          hdown->SetBinContent(bin,lowVal);
          hup->SetBinContent(bin,highVal);
        }
      }

      //hup->Smooth(1);
      //hdown->Smooth(1);
      
      // sort out the integrals 
      cout<<"Old integrals nominal: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;
      /*
      if(hdown->Integral()!=0) 
        hdown->Scale(hnom->Integral()/hdown->Integral());
      if(hup->Integral()!=0)
        hup->Scale(hnom->Integral()/hup->Integral());
      */
      cout<<"Norm new integrals nominal: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;

      // write there that this sample is affected by this systematic 
      if(hdown!=nullptr){
        hdown->Write();
        delete hdown;
      }
      if(hup!=nullptr) {
        hup->Write();
        delete hup;
      }
    }
    if(hnom!=nullptr) {
      hnom->Write();
      delete hnom;
    }
  }

  // sortout the normalisation coefficients
  for( std::map<string, std::vector<std::pair<string,string>>>::iterator it=tfSysNormMap.begin(); it!=tfSysNormMap.end(); it++){
    continue; 
    TH1F *hnom=(TH1F*)f->Get( (*it).first.c_str());
    if(hnom==nullptr) {
      cout<<"nominal histogram "<<(*it).first<<" is a null pointer "<<endl;
      continue;
    }

    vector <std::pair<string, string>> vars=(*it).second;
    for( vector<std::pair<string, string>>::iterator it2=vars.begin(); it2!=vars.end(); it2++){
      TH1F *hup=nullptr;
      hup=(TH1F*)f->Get((*it2).first.c_str());
      TH1F *hdown=nullptr;
      hdown=(TH1F*)f->Get((*it2).second.c_str());

      if( hup==nullptr && hdown!=nullptr) {
        //hup=(TH1F*)hdown->Clone((*it2).first.c_str());
        hup=(TH1F*)hnom->Clone((*it2).first.c_str());
        cout<<"Hup did not exist replacing it by downvariation for"<<(*it2).first<<endl;
      }
      
      else if ( hup!=nullptr && hdown==nullptr){
        //hdown=(TH1F*)hup->Clone((*it2).second.c_str());
        hdown=(TH1F*)hnom->Clone((*it2).second.c_str());
        cout<<"Hdown does not exist replacing it bu upvariation for "<<(*it2).second<<endl;
      }
      else if (hup==nullptr && hdown==nullptr){
        cout<<"Both variations do not exist, skipping this systematic"<<endl;
        continue; 
      }
      // sort out the integrals 
      cout<<"Norm overall Names "<<hnom->GetName()<<" up "<<hup->GetName()<<" down "<<hdown->GetName()<<endl;
      cout<<"Old integrals nominal: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;

      if (hdown->Integral()<=0){
        for( int b=1; b<(int)hnom->GetNbinsX()+1; b++)
        hdown->SetBinContent(b,hnom->GetBinContent(b)); 
      }
      
      if (hup->Integral()<=0){
        for( int b=1; b<(int)hnom->GetNbinsX()+1; b++)
          hup->SetBinContent(b,hnom->GetBinContent(b)); 
      }
      
      // make some checks
      if( hup->Integral()==hdown->Integral())
        hdown->Scale(hnom->Integral()/hdown->Integral());

      if( fabs(1- hdown->Integral() / hnom->Integral()) > 3 ) {
        cout<<"Differnece greater than 3 suppressing the down systematic "<<hdown->Integral()<<" relative"<< hdown->Integral()/hnom->Integral()<<endl;
        hdown->Scale(hnom->Integral()/hdown->Integral());
      }
      if( fabs(1- hup->Integral() / hnom->Integral()) > 3 ){
        cout<<"Differnece greater than 3 suppressing the up systematic "<<hup->Integral()<<" relative"<< hup->Integral()/hnom->Integral()<<endl;
        hup->Scale(hnom->Integral()/hup->Integral());
      }
      
      
      cout<<"New integrals nominal: "<<hnom->Integral()<<" up "<<hup->Integral()<<" down "<<hdown->Integral()<<endl;
      hdown->Write();
      hup->Write();
      if(hdown!=nullptr)delete hdown;
      if(hup!=nullptr) delete hup;
      
    }
    if(hnom!=nullptr) {
      hnom->Write();
      delete hnom;
    }
 }

  /*
  // Add the theory uncertainties from histograms
  TFile *ftheor=TFile::Open("/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/October2019_withSys/LauraHF/vbf.root","READ");
  TKey *keyP2=nullptr;
  TIter nextP2(ftheor->GetListOfKeys());
  vector <string> TH1FNamesTH;
  vector <string> TH1FNamesNormTH;
  vector <string> TH1FNamesNormTFTH;
  vector <string> TH1FNamesNormRawTH;

 while ((keyP2=(TKey*)nextP2())) {
    if (strcmp(keyP2->GetClassName(),"TH1")) {
      string ttname=remove_extension(keyP2->GetName());
      bool isnew=true;
      for(std::vector<string>::iterator it=TH1FNamesTH.begin(); it!=TH1FNamesTH.end(); it++){
        if( (*it).compare(ttname)==0) 
          {   isnew=false; break; } 
      }
      if(isnew){
        TH1FNamesTH.push_back(ttname);
        if(!TString(ttname.c_str()).Contains("Norm"))
          TH1FNamesNormRawTH.push_back(ttname);
        if(TString(ttname.c_str()).Contains("Norm") && TString(ttname.c_str()).Contains("_obs"))
          TH1FNamesNormTH.push_back(ttname);
        if(TString(ttname.c_str()).Contains("Norm") && !TString(ttname.c_str()).Contains("_obs"))
          TH1FNamesNormTFTH.push_back(ttname);
      }
    }
 }
  */

  

  
  

  //fnew->Close();
}

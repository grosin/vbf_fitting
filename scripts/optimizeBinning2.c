/* Gaetano Barone <gaetano.barone@cern.ch>
 * Laura Bergsten <lberste@cern.ch>
 */
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include "minitree.h"
#include "TH3F.h"
#include "tools.h"

double margin=1.0001;

using namespace std;


std::map<string,int> colorMap;;
std::map <string,string> bdtClasses;
std::map <string,string> ibdtClasses;
string globalCuts;

void DumpBins(vector<double> vec){
  for(vector<double>::iterator il=vec.begin(); il!=vec.end(); il++)
    cout<<(*il)<<" ";
  cout<<endl;
}

double ComputeSplineIntegral(TSpline3 *sp=nullptr){
  if(sp==nullptr) { cout<<"Cannot compute integral"; return 0; } 
  double Integral=0.0;

  for(double x=0; x<= 1 ; x+=0.01){
    cout<<" x "<<x<<" "<<sp->Eval(x)<<endl;
    Integral+=sp->Eval(x);
  }
  return Integral;
}

double ComputeGraphIntegral(TGraphErrors *gr=nullptr, double dx=0.001){
  if(gr==nullptr) { cout<<"Cannot compute integral"; return 0; } 
  double Integral=0.0;
  //TF1 *f=new TF1("f","[0]+[1]*x[0]-[2]*x[0]*x[0]+[3]*x[0]*x[0]*x[0]+[4]*x[0]*x[0]*x[0]*x[0]+[5]*x[0]*x[0]*x[0]*x[0]*x[0]",0,1);
  //f->FixParameter(0,1);
  ////f->FixParameter(1,0);
  ////f->FixParameter(3,0);
  ////f->FixParameter(4,0);
  ////f->FixParameter(5,0);
  //gr->Fit(f);
  //f->SetLineColor(gr->GetLineColor());
  for(double x=0; x<= 1 ; x+=dx){
    Integral+=gr->Eval(x,0,"")*dx;
    //cout<<" X "<<x<<" "<<Integral<<endl;
  }
  return Integral;
}


vector <string> GetTreeNames(string treeName=""){
 vector <string> treeNames;

 /*
  if(treeName.size()==0){
    treeNames.push_back("Zjets");
    treeNames.push_back("Top");
    treeNames.push_back("WW");
    treeNames.push_back("GGF");
    treeNames.push_back("VBF");
    //treeNames.push_back("FakeE");
    //treeNames.push_back("FakeM");
    //treeNames.push_back("Vgamma");
    //treeNames.push_back("vh");
    treeNames.push_back("Data");
    }*/

 if(treeName.size()==0){
   treeNames.push_back("ggf");
   treeNames.push_back("Vgamma");
   treeNames.push_back("Zjets");
   treeNames.push_back("top");
   treeNames.push_back("diboson");
   treeNames.push_back("vbf");
   //treeNames.push_back("FakeE");
   //treeNames.push_back("FakeM");
   //treeNames.push_back("Vgamma");
   //treeNames.push_back("vh");
   //treeNames.push_back("data");
 }
  else {
    std::istringstream iss(treeName);  
    std::string s;                                                               
    while(getline(iss, s, ','))                                                  
      {                                                                            
        treeNames.push_back(s);                                                           
      }
  }
  return treeNames;
  
}

TH3F *getBackgroundHist(std::map<string, TH3F*> &base3dHistMap,string name="bckHist"){
  TH3F *hb=nullptr;

  for(std::map<string, TH3F*>::iterator it=base3dHistMap.begin(); it!=base3dHistMap.end(); it++){
    //cout<<"Looping over "<<(*it).first<<endl;
    string hname=(*it).first;
    //if( hname.compare("VBF")==0 || hname.compare("WW")==0 || hname.compare("GGF")==0 || hname.compare("Data")==0) continue;
    if( hname.compare("vbf")==0 || hname.compare("diboson")==0 || hname.compare("ggf")==0 || hname.compare("data")==0) continue;
    if( hb==nullptr) {
      //cout<<"Found background "<<(*it).first<<endl;
      hb=(TH3F*)(*it).second->Clone(name.c_str());
    }
    else {
      //cout<<"Adding background "<< (*it).first<<endl;
      hb->Add((*it).second);
    }
  }
  return hb; 
}



vector <vector <double>> optimizeAxis (int iDim,std::map<string, TH3F*> iterMap3D, vector <vector <double>> &inBinning, int iter,vector <double> &signficances,std::map<string, TTree*> treeMap,int binToSplit=0, int lastBinSplit=0){
  cout<<"Call for last bin split "<<lastBinSplit<<endl;
  
  iter++;
  
  //if( iter==1) {
  //signficances.clear();
  //signficances.push_back(0);
  //signficances.push_back(0);
  //signficances.push_back(0);
  //}
  
  if( iter == 100 ) { cout<<"Max iter reached "<<endl; return inBinning;} 
  
  
  for( int d = iDim; d<3; d++){
    //for(int i=0; i<inBinning[d].size()-1; i++){
    int i=0;
    // make the loop start from the continue statement
    vector<double>::iterator lp;
    if(binToSplit==0) lp=inBinning[d].begin();
    else{
      for( vector<double>::iterator kk=inBinning[d].begin(); kk!=inBinning[d].end(); kk++){
        cout<<" distance "<<distance(inBinning[d].begin(),kk)<<" bin to split "<<binToSplit<<" overall distance "<<distance(inBinning[d].begin(),inBinning[d].end())<<endl;
        if(distance(inBinning[d].begin(),kk) ==  lastBinSplit) { lp=kk; break;}
      }
      i=lastBinSplit;
    }

    //if( lastBinSplit >= distance(inBinning[d].begin(),inBinning[d].end())) continue; 
    ///for(vector <double>::iterator lp=inBinning[d].begin(); lp!=inBinning[d].end();  lp++){

    vector <vector <double>> tmpinBinning=inBinning;
    vector<double>::iterator pend=tmpinBinning[d].end(); //inBinning[d].end();
    for( ; i<(int)tmpinBinning[d].size(); i++){
    //for(; lp!=pend; lp++){
      cout<<" start at "<<std::distance(inBinning[d].begin(),lp)<<endl;
      //if( i <=binToSplit) {i++; continue; }
      // splity the ith bin into two 
      bool isSplittable=false;
      // perform the signifgicance test
      double significanceWW_=0;
      double signifgicanceVBF_=0;
      double signifgicanceGFF_=0;

      cout<<"Courrent binning at pos i "<<i<<endl;
      DumpBins(inBinning[d]);
      //inBinning[d].insert(lp+1,(inBinning[d][i+1]-inBinning[d][i])*0.5);
      cout<<"Inserting "<<inBinning[d][i]+(inBinning[d][i+1]-inBinning[d][i])*0.5<<" at "<<i+1<<" for last boundary "<<inBinning[d][i]<<endl;
      
      inBinning[d].insert(inBinning[d].begin()+i+1,inBinning[d][i]+(inBinning[d][i+1]-inBinning[d][i])*0.5);
      cout<<"Trying binning "<<endl;
      DumpBins(inBinning[d]);
      
      vector <double> xBins=inBinning[0];
      vector <double> yBins=inBinning[1];
      vector <double> zBins=inBinning[2];

     
    for(std::map<string, TTree*>::iterator it=treeMap.begin(); it!=treeMap.end(); it++){
      iterMap3D[(*it).first]= new TH3F(Form("base3dHist_%s_dim_%d_iter%d",(*it).first.c_str(),d,iter),Form("base3dHist_%s",(*it).first.c_str()),
                                       (int) xBins.size()-1,&xBins[0],
                                       (int) yBins.size()-1, &yBins[0],
                                       (int) zBins.size()-1, &zBins[0]); 
      
      (*it).second->Draw(Form("%s:%s:%s>>%s",bdtClasses["ggf"].c_str(),bdtClasses["diboson"].c_str(),bdtClasses["vbf"].c_str(),iterMap3D[(*it).first]->GetName()),Form("weight*(%s)",globalCuts.c_str()));
      //(*it).second->Draw(Form("%s:%s:%s>>%s",bdtClasses["ggf"].c_str(),bdtClasses["vbf"].c_str(),bdtClasses["vbf"].c_str(),iterMap3D[(*it).first]->GetName()),Form("weight*(%s)",globalCuts.c_str()));
      
    }
    TH3F *tmpbaseBckHist=getBackgroundHist(iterMap3D,Form("tmpbaseBckHist_%d",iter));
    
    for(int x=1; x<(int)iterMap3D["vbf"]->GetXaxis()->GetNbins()+1; x++){
      for(int y=1; y<(int)iterMap3D["diboson"]->GetYaxis()->GetNbins()+1; y++){
        for(int z=1; z<(int)iterMap3D["ggf"]->GetZaxis()->GetNbins()+1; z++){
          // break recursion if there is a bin with content == 0 ; 
          if( iterMap3D["diboson"]->GetBinContent(x,y,z) > 0 && iterMap3D["vbf"]->GetBinContent(x,y,z) > 0 && iterMap3D["ggf"]->GetBinContent(x,y,z)> 0 && tmpbaseBckHist->GetBinContent(x,y,z)>0) {
            significanceWW_+=pow(iterMap3D["diboson"]->GetBinContent(x,y,z) / sqrt( iterMap3D["vbf"]->GetBinContent(x,y,z)+iterMap3D["ggf"]->GetBinContent(x,y,z)+tmpbaseBckHist->GetBinContent(x,y,z)),2);
            signifgicanceVBF_+=pow(iterMap3D["vbf"]->GetBinContent(x,y,z) / sqrt( iterMap3D["diboson"]->GetBinContent(x,y,z)+iterMap3D["ggf"]->GetBinContent(x,y,z)+tmpbaseBckHist->GetBinContent(x,y,z)),2);
            signifgicanceGFF_+=pow(iterMap3D["ggf"]->GetBinContent(x,y,z) / sqrt( iterMap3D["vbf"]->GetBinContent(x,y,z)+iterMap3D["diboson"]->GetBinContent(x,y,z)+tmpbaseBckHist->GetBinContent(x,y,z)),2);
          }
          else {
            significanceWW_=0;
            signifgicanceVBF_=0;
            signifgicanceGFF_=0;
            break;
          }
          
        }}}

    cout<<" significance_ [WW] "<<significanceWW_ <<"/ "<< signficances[1]<<"  [VBF] "<<signifgicanceVBF_<<"/ "<< signficances[0]<<" [ggf] "<<signifgicanceGFF_<<"/ "<< signficances[2]<<" iter " <<iter<<" dim "<<d<<" iDim "<<iDim<<endl;
    
    if( (significanceWW_ > signficances[1] * margin || signifgicanceVBF_ > signficances[0] *margin || signifgicanceGFF_ >  signficances[2] * margin))
      isSplittable=true;
    
    if(isSplittable){
      delete tmpbaseBckHist;
      for(std::map<string, TH3F*>::iterator it2=iterMap3D.begin(); it2!=iterMap3D.end(); it2++)
        delete (*it2).second;
      
      signficances[0]=signifgicanceVBF_; signficances[1]=significanceWW_; signficances[2]=signifgicanceGFF_;
      cout<<"Splitting d"<<d<<"iDm "<<iDim<<" iter " <<iter<<" bin split is "<<((inBinning[d][i]+inBinning[d][i+1]-inBinning[d][i])*0.5)<<endl;
      //inBinning[d].insert(lp+1,(inBinning[d][i]+inBinning[d][i+1]-inBinning[d][i])*0.5);
      lastBinSplit++;
      cout<<"Binning is "<<" with last bin split "<<lastBinSplit<<endl;
      DumpBins(inBinning[d]);
      
      return optimizeAxis(d,iterMap3D,inBinning,iter,signficances,treeMap,binToSplit,lastBinSplit);
    }
    else {
      inBinning=tmpinBinning;
      delete tmpbaseBckHist;
      for(std::map<string, TH3F*>::iterator it2=iterMap3D.begin(); it2!=iterMap3D.end(); it2++)
      delete (*it2).second;
      cout<<"Continue statetment d "<<d<<"iDm "<<iDim<<" iter " <<iter<<" next call at "<<distance(inBinning[d].begin(),lp)<<" overall distance is "<<distance(inBinning[d].begin(),inBinning[d].end())<<" last bin split"<<lastBinSplit<<endl;
      if( distance(inBinning[d].begin(),lp)+2 < distance(inBinning[d].begin(),inBinning[d].end()))
        return optimizeAxis(d,iterMap3D,inBinning,iter,signficances,treeMap,distance(inBinning[d].begin(),lp)+2,lastBinSplit+1);
      else { lastBinSplit++; continue;} 
      i++;
      //lp++;
      //continue; 
    }

             //i++;
  }}
  return inBinning;
}


void optimizeBinning2(string infile="eval_VBF_WW_GGF_TOP_Data_Zjets.root", string treeName=""){
  const int nDim=3;


colorMap["vbf"]=kRed;
  colorMap["ggf"]=kBlue;
  colorMap["diboson"]=kYellow-3;
  colorMap["Zjets"]=kMagenta;
  colorMap["top"]=kCyan;
  colorMap["data"]=kBlack;
colorMap["Vgamma"]=kOrange;
  bdtClasses["vbf"]="bdt_vbf";
  bdtClasses["diboson"]="bdt_WW";
  bdtClasses["ggf"]="bdt_ggF"; 

  //bdtClasses["MT"]="MT"; 
  //bdtClasses["Mjj"]="Mjj"; 
  //bdtClasses["DPhill"]="DPhill"; 
  //bdtClasses["MET"]="MET"; 
  //bdtClasses["centralJetVetoLeadpT"]="centralJetVetoLeadpT";
  //bdtClasses["DEtajj"]="DEtajj";
  //bdtClasses["Mll"]="Mll";
  //bdtClasses["bdt_top"]="bdt_top";
  ibdtClasses["bdt_vbf"]="vbf";
  ibdtClasses["bdt_WW"]="diboson";
  ibdtClasses["bdt_ggF"]="ggf";
  ibdtClasses["MT"]="MT";
  ibdtClasses["Mjj"]="Mjj";
  ibdtClasses["DPhill"]="DPhill";
  ibdtClasses["MET"]="MET";
  ibdtClasses["centralJetVetoLeadpT"]="centralJetVetoLeadpT";
  ibdtClasses["DEtajj"]="DEtajj";
  ibdtClasses["Mll"]="Mll";
  //ibdtClasses["bdt_top"]="bdt_top";

  

  // SR CUTS:
  //string globalCuts="nJetsTight>=2 && nBJetsSubMV2c10<1 && centralJetVetoLeadpT<20000 && OLV==1 && mtt/1000<mZ/1000-25 && centralJetVetoLeadpT/1000<20";
  //string globalCuts="nJetsTight>=2 && nBJetsSubMV2c10>1 && centralJetVetoLeadpT<20000 && OLV==1 && mtt/1000<mZ/1000-25 && centralJetVetoLeadpT/1000<20";
  //string globalCuts="region==1";
  globalCuts="inSR==1";
  
  TFile *f=TFile::Open(infile.c_str());
  vector <string> treeNames=GetTreeNames(treeName);
  std::map<string,TTree*> treeMap;

  
 
  
  // Load the trees
  for( vector <string>::iterator it=treeNames.begin(); it!=treeNames.end(); it++){
    TTree *t=(TTree*)f->Get(((*it)+"_nominal").c_str());
    if( t==nullptr) { cout<<"Tree "<< *it<<" is null ptr skipping "<<endl; continue ;} 
    treeMap[*it]=t;
  }


  // Here create a TH3F for each tree
  double bdtX[2]={0,1};
  double bdtY[2]={0,1};
  double bdtZ[2]={0,1};
  vector <double> boundaries[3];
  // here run the optimization algorithm
  // NB assuming cube to begin with, all ranges are the same that of X;
  // Start with nominal boundaries;
  for(unsigned int dim=0; dim<3; dim++){
    //boundaries[dim].push_back(bdtX[0]); 
    //boundaries[dim].push_back(bdtX[1]);
    boundaries[dim].push_back(0); 
    boundaries[dim].push_back(1); 
  }


  std::map<string, TH3F*> base3dHistMap;
    
  for(std::map<string, TTree*>::iterator it=treeMap.begin(); it!=treeMap.end(); it++){
    vector <double> xBins=boundaries[0];
    vector <double> yBins=boundaries[1];
    vector <double> zBins=boundaries[2];

    base3dHistMap[(*it).first]= new TH3F(Form("base3dHist_%s",(*it).first.c_str()),Form("base3dHist_%s",(*it).first.c_str()),
                                         (int) boundaries[0].size()-1, &xBins[0],
                                         (int) boundaries[1].size()-1,  &yBins[0],
                                         (int) boundaries[2].size()-1,   &zBins[0]);
    
    (*it).second->Draw(Form("%s:%s:%s>>%s",bdtClasses["ggf"].c_str(),bdtClasses["diboson"].c_str(),bdtClasses["vbf"].c_str(),base3dHistMap[(*it).first]->GetName()),Form("weight*(%s)",globalCuts.c_str()));
    
  }


  
  TH3F *baseBckHist=getBackgroundHist(base3dHistMap,"baseBckHist");
  if(baseBckHist==nullptr) cout<<"No Background"<<endl; 
  
  double significanceWW=0;
  double signifgicanceVBF=0;
  double signifgicanceGFF=0;
  for(int x=1; x<(int)base3dHistMap["vbf"]->GetXaxis()->GetNbins()+1; x++){
    for(int y=1; y<(int)base3dHistMap["diboson"]->GetYaxis()->GetNbins()+1; y++){
      for(int z=1; z<(int)base3dHistMap["ggf"]->GetZaxis()->GetNbins()+1; z++){

        significanceWW+=pow(base3dHistMap["diboson"]->GetBinContent(x,y,z) / sqrt( base3dHistMap["vbf"]->GetBinContent(x,y,z)+base3dHistMap["ggf"]->GetBinContent(x,y,z)+baseBckHist->GetBinContent(x,y,z)),2);
        signifgicanceVBF+=pow(base3dHistMap["vbf"]->GetBinContent(x,y,z) / sqrt( base3dHistMap["diboson"]->GetBinContent(x,y,z)+base3dHistMap["ggf"]->GetBinContent(x,y,z)+baseBckHist->GetBinContent(x,y,z)),2);
        signifgicanceGFF+=pow(base3dHistMap["ggf"]->GetBinContent(x,y,z) / sqrt( base3dHistMap["vbf"]->GetBinContent(x,y,z)+base3dHistMap["diboson"]->GetBinContent(x,y,z)+baseBckHist->GetBinContent(x,y,z)),2);
      }}}


  cout<<" significance [WW] "<<significanceWW<<" [VBF] "<<signifgicanceVBF<<" [ggf] "<<signifgicanceGFF<<endl;



  bool optimalCondition = false;
  const int maxSteps=1;
  int step=0;

  std::map<string, TH3F*> iterMap3DFinal;
  TH3F *baaseBckHistFinal=nullptr;
  vector <double> Newboundaries[3];

  
  for(unsigned int dim=0; dim<nDim; dim++)
    for(unsigned int  k=0; k<boundaries[dim].size(); k++)
      Newboundaries[dim].push_back(boundaries[dim].at(k));

  // dimension significance binning 
  //vector< std::map<int,std::pair<double,vector <double>>>> allItersBinning;
  vector< vector<pair<double,vector <double> >>> allItersBinning;
  
 vector <double> iterSig;
 iterSig.push_back(0);
 iterSig.push_back(0);
 iterSig.push_back(0);
  
  while (!optimalCondition && step<maxSteps){
    // make the split of the boundaries
   
    
    for( int dim=0; dim<nDim; dim++){
      //if(dim > 0) continue;
      // make a split
          
      // make a loop over the start bin to split
      //for( int bstart=0; bstart<Newboundaries[dim].size()-1; bstart++){
      //double bstart=0.99;
      //while (bstart < Newboundaries[dim].back() ){

      /*
      for( int bstart = 0 ; bstart< 1; bstart++){
      
      vector <double> tmpBoundaries[nDim];
      // iterate over the boundaries and make split
      bool saveSplit=false;

      for(unsigned int  k=0; k<Newboundaries[dim].size()-1; k++){
        double bhigh=Newboundaries[dim][k+1];
        double blow=Newboundaries[dim][k];
        tmpBoundaries[dim].push_back(blow);
        //if( bstart > blow) {
        double bwidth= Newboundaries[dim][k+1]-Newboundaries[dim][k];
        blow=bhigh - 0.5*bwidth;
        //cout<<"Bin width "<<blow<<endl;
        bhigh=bhigh;
        tmpBoundaries[dim].push_back(blow);
        //}
        //bstart=blow;
        //break;
      }
      tmpBoundaries[dim].push_back(Newboundaries[dim].back());
      
      cout<<"tmp boundaries dim "<<dim<<": ";
      for(std::vector<double>::iterator bd=tmpBoundaries[dim].begin(); bd<tmpBoundaries[dim].end(); bd++){
          cout<<(*bd)<<" ";
      }
      cout<<endl;
      

      vector <double> xBins=Newboundaries[0];
      vector <double> yBins=Newboundaries[1];
      vector <double> zBins=Newboundaries[2];
      
      if( dim ==0 ) 
        xBins=tmpBoundaries[0];
      else if (dim == 1) 
        yBins=tmpBoundaries[1];
      else if( dim == 2) 
        zBins=tmpBoundaries[2];

      cout<<"Binning for dimension opt "<<dim<<endl;
      cout<<"X :";
      for(vector<double>::iterator il=xBins.begin(); il!=xBins.end(); il++){
        cout<<(*il)<<" ";
      }
      cout<<endl;
      cout<<"Y :";
      for(vector<double>::iterator il=yBins.begin(); il!=yBins.end(); il++){
        cout<<(*il)<<" ";
      }
      cout<<endl;
      cout<<"Z :";
      for(vector<double>::iterator il=zBins.begin(); il!=zBins.end(); il++){
        cout<<(*il)<<" ";
      }
      cout<<endl;
      
      std::map<string, TH3F*> iterMap3D;
      
        for(std::map<string, TTree*>::iterator it=treeMap.begin(); it!=treeMap.end(); it++){
         
          
          iterMap3D[(*it).first]= new TH3F(Form("base3dHist_%s_dim_%d_iter%d",(*it).first.c_str(),dim,step),Form("base3dHist_%s",(*it).first.c_str()),
                                           (int) xBins.size()-1,&xBins[0],
                                           (int) yBins.size()-1, &yBins[0],
                                           (int) zBins.size()-1, &zBins[0]); 
          
          (*it).second->Draw(Form("%s:%s:%s>>%s",bdtClasses["ggf"].c_str(),bdtClasses["diboson"].c_str(),bdtClasses["vbf"].c_str(),iterMap3D[(*it).first]->GetName()),Form("weight*(%s)",globalCuts.c_str()));
          
        }
        TH3F *tmpbaseBckHist=getBackgroundHist(iterMap3D,Form("tmpbaseBckHist_%d",step));
        TH3F *baseBckHistFinal=nullptr; // <-- This is needed f
        
        // perform the signifgicance test
        double significanceWW_=0;
        double signifgicanceVBF_=0;
        double signifgicanceGFF_=0;

        for(int x=1; x<(int)iterMap3D["vbf"]->GetXaxis()->GetNbins()+1; x++){
          for(int y=1; y<(int)iterMap3D["diboson"]->GetYaxis()->GetNbins()+1; y++){
            for(int z=1; z<(int)iterMap3D["ggf"]->GetZaxis()->GetNbins()+1; z++){
              if( iterMap3D["diboson"]->GetBinContent(x,y,z) > 0 && iterMap3D["vbf"]->GetBinContent(x,y,z) > 0 && iterMap3D["ggf"]->GetBinContent(x,y,z)> 0 && tmpbaseBckHist->GetBinContent(x,y,z)>0) {
              
              significanceWW_+=pow(iterMap3D["diboson"]->GetBinContent(x,y,z) / sqrt( iterMap3D["vbf"]->GetBinContent(x,y,z)+iterMap3D["ggf"]->GetBinContent(x,y,z)+tmpbaseBckHist->GetBinContent(x,y,z)),2);
              signifgicanceVBF_+=pow(iterMap3D["vbf"]->GetBinContent(x,y,z) / sqrt( iterMap3D["diboson"]->GetBinContent(x,y,z)+iterMap3D["ggf"]->GetBinContent(x,y,z)+tmpbaseBckHist->GetBinContent(x,y,z)),2);
              signifgicanceGFF_+=pow(iterMap3D["ggf"]->GetBinContent(x,y,z) / sqrt( iterMap3D["vbf"]->GetBinContent(x,y,z)+iterMap3D["diboson"]->GetBinContent(x,y,z)+tmpbaseBckHist->GetBinContent(x,y,z)),2);
              }

            }}}

        vector<pair<double,vector <double>>> thisIter;
        thisIter.push_back(make_pair(signifgicanceVBF_,xBins));
        thisIter.push_back(make_pair(significanceWW_,yBins));
        thisIter.push_back(make_pair(signifgicanceGFF_,zBins));
        allItersBinning.push_back(thisIter);
        
        cout<<" significance [WW] "<<significanceWW<<" [VBF] "<<signifgicanceVBF<<" [ggf] "<<signifgicanceGFF<<endl;
        cout<<" significance_ [WW] "<<significanceWW_<<" [VBF] "<<signifgicanceVBF_<<" [ggf] "<<signifgicanceGFF_<<endl;
        
        if(significanceWW_ > significanceWW * 1.1 || signifgicanceVBF_ > signifgicanceVBF *1.1 || signifgicanceGFF >  signifgicanceVBF *1.1)
          saveSplit=true;
        
        delete tmpbaseBckHist;
        
        
        if(!saveSplit) {
          for(std::map<string, TH3F*>::iterator it2=iterMap3D.begin(); it2!=iterMap3D.end(); it2++)
            delete (*it2).second; 
        }
        else{
          for(std::map<string, TH3F*>::iterator it2=iterMap3DFinal.begin(); it2!=iterMap3DFinal.end(); it2++)
            delete (*it2).second;
          
          for(std::map<string, TH3F*>::iterator it2=iterMap3D.begin(); it2!=iterMap3D.end(); it2++)
            iterMap3DFinal[(*it2).first]=(*it2).second;
          //if(baseBckHistFinal!=nullptr) delete baseBckHistFinal
          //baseBckHistFinal=tmpbaseBckHist;
          Newboundaries[dim] = tmpBoundaries[dim];
          cout<<"Split occured" <<endl;
        }
    }*/

    
    //vector <double> optimizeAxis[3](int iDim,std::map<string, TH3F*>:: iterMap3D, vector <double>[3] inBinning, int iter,vector <double> signficances,std::map<string, TTree*> treeMap,int binToSplit=0)

    vector <vector<double>> Newboundaries_;
    for( int i=0; i<(int)nDim; i++)
      Newboundaries_.push_back(Newboundaries[i]);
    
    Newboundaries_=optimizeAxis(dim, iterMap3DFinal,Newboundaries_,0,iterSig,treeMap,0);

    cout<<"Done optimizing axis "<<dim<<endl;
    
    for( int i=0; i<(int)nDim; i++){
      cout<<"Clearing"<<endl;
      Newboundaries[i].clear();
      cout<<"Storing"<<endl;
      Newboundaries[i]=Newboundaries_[i];
      cout<<Newboundaries[i][0]<<endl;
      cout<<"Done"<<endl;
     }
    
    vector<pair<double,vector <double>>> thisIter;
    for( int n=0; n<nDim; n++)
      thisIter.push_back(make_pair(iterSig[n],Newboundaries[n]));

    allItersBinning.push_back(thisIter);
    //cout<<" significance [WW] "<<allItersBinning.at(allItersBinning.size()-1) <<" [VBF] "<<signifgicanceVBF<<" [ggf] "<<signifgicanceGFF<<endl;
    //cout<<" significance_ [WW] "<<significanceWW_<<" [VBF] "<<signifgicanceVBF_<<" [ggf] "<<signifgicanceGFF_<<endl;
    
    
    cout<<"Iteration  step: boundaries are :"<<endl;
    for(unsigned int dim=0; dim<nDim; dim++){
      cout<<"axis "<<dim<<" ";
      for(unsigned int  k=0; k<Newboundaries[dim].size(); k++){
        cout<<Newboundaries[dim].at(k)<<" ";
      }
      cout<<endl;
    
      //break ;
    }
     if(optimalCondition)
       cout<<"Converged afer "<<step<<" iterations "<<endl;
     if( step +1 == maxSteps)
       cout<<"Reached max steps after "<<step<<" iterations "<<endl;
     step++;
    }}


  // optimal binning for all maximum
  //vector< vector<pair<double,vector <double>> >  allItersBinning
  vector<pair<double,vector <double>>> bestForAll;
  vector<pair<double,vector <double>>> bestForVBF;
    
  double bestSig[3]={0,0,0};
  double bestSigVBF[3]={0,0,0};
  
  for( vector< vector<pair<double,vector <double> >>>::iterator it=allItersBinning.begin(); it!=allItersBinning.end(); it++){
    vector<pair<double,vector <double>>> thisSig=(*it);
    
    if( thisSig[0].first > bestSig[0] && thisSig[1].first > bestSig[1] && thisSig[2].first>bestSig[2]) {
      for( int i=0; i<nDim; i++) {
        bestSig[i]= thisSig[i].first;
        bestForAll=(*it);
      }
    }
    if( thisSig[0].first > bestSigVBF[0] ){
      for( int i=0; i<nDim; i++) {
        bestSigVBF[i]= thisSig[i].first;
        bestForVBF=(*it);
      }}
    
  }
  
  cout<<"Best significance for all is "<<endl;
  cout<<"signficance VBF "<<bestForAll[0].first<<" WW "<<bestForAll[1].first<<" ggF: "<<bestForAll[2].first<<endl;
  cout<<"Binning for dimension opt "<<endl;
  cout<<"bdt_VBF :";
  DumpBins(bestForAll[0].second);
  cout<<"bdt_WW :";
  DumpBins(bestForAll[1].second);
  cout<<"bdt_ggF :";
  DumpBins(bestForAll[2].second);


  cout<<"Best significance for all VBF "<<endl;
  cout<<"signficance VBF "<<bestForVBF[0].first<<" WW "<<bestForVBF[1].first<<" ggF: "<<bestForVBF[2].first<<endl;
  cout<<"Binning for dimension opt "<<endl;
  cout<<"bdt_VBF :";
  DumpBins(bestForVBF[0].second);
  cout<<"bdt_WW :";
  DumpBins(bestForVBF[1].second);
  cout<<"bdt_ggF :";
  DumpBins(bestForVBF[2].second);


  
  


  std::map<string,THStack*> bdt1D_stack;
  std::map<string, std::map<string, TH1F*>>    h1DHists;
  std::map<string, TLegend*>  bdt1DLegs;

  
   for(std::map<string, TH3F*>::iterator it2=iterMap3DFinal.begin(); it2!=iterMap3DFinal.end(); it2++)
    delete (*it2).second;

   int isample=0;
    for(std::map<string, TTree*>::iterator it=treeMap.begin(); it!=treeMap.end(); it++){
      vector <double> xBins=bestForAll[0].second;
      vector <double> yBins=bestForAll[1].second;
      vector <double> zBins=bestForAll[2].second;
      
        iterMap3DFinal[(*it).first]= new TH3F(Form("optimalFinal_%s",(*it).first.c_str()),Form("optimalFinal_%s",(*it).first.c_str()),
                                              (int) xBins.size()-1, &xBins[0],
                                              (int) yBins.size()-1,  &yBins[0],
                                              (int) zBins.size()-1,   &zBins[0]);
        (*it).second->Draw(Form("%s:%s:%s>>%s",bdtClasses["ggf"].c_str(),bdtClasses["diboson"].c_str(),bdtClasses["vbf"].c_str(),base3dHistMap[(*it).first]->GetName()),Form("weight*(%s)",globalCuts.c_str()));
        //(*it).second->Draw(Form("%s:%s:%s>>%s",bdtClasses["ggf"].c_str(),bdtClasses["vbf"].c_str(),bdtClasses["diboson"].c_str(),base3dHistMap[(*it).first]->GetName()),Form("weight*(%s)",globalCuts.c_str()));

        int idim=0;
        for(std::map <string,string>::iterator it2=bdtClasses.begin(); it2!=bdtClasses.end(); it2++){
          if(isample==0){
            bdt1DLegs[(*it2).second]=DefLeg(0.85,0.85,0.99,0.99);
            bdt1D_stack[(*it2).second]=new THStack(Form("stack_%s",(*it2).second.c_str()),"");
          }
          vector <double> iBins=bestForAll[idim].second;
          
          h1DHists[(*it2).second][(*it).first] = new TH1F(Form("h1D_%s_%s",(*it2).second.c_str(),(*it).first.c_str()),Form("h1D_%s_%s",(*it2).second.c_str(),(*it).first.c_str()),iBins.size()-1,&iBins[0]);
          TH1F *h1D=h1DHists[(*it2).second][(*it).first];

          (*it).second->Draw(Form("%s>>%s",(*it2).second.c_str(),h1D->GetName()),Form("weight*(%s)",globalCuts.c_str()));

          h1DHists[(*it2).second][(*it).first]->GetXaxis()->SetTitle((*it2).second.c_str());
          h1DHists[(*it2).second][(*it).first]->GetZaxis()->SetTitle("Events / bin");
          //%.2lf",h1D->GetBinWidth(1)));
         
          h1D->SetStats(0);
          //SetDef(h1D);
          h1D->SetFillColor(colorMap[(*it).first]);
          h1D->SetLineColor(colorMap[(*it).first]);
          bdt1D_stack[(*it2).second]->Add(h1D);
          
          bdt1DLegs[(*it2).second]->AddEntry(h1D,(*it).first.c_str(),"FL");
          idim++;
        }
        
          
        isample++;
        
  }
    TH3F *finalBackground=getBackgroundHist(iterMap3DFinal,"finalBackground");

    // Make the stacks
    std::map<string, TCanvas*> cBDt1DSub;

    for(std::map<string,THStack*>::iterator it=bdt1D_stack.begin(); it!=bdt1D_stack.end(); it++){
      cBDt1DSub[(*it).first]=new TCanvas(Form("C_opt_%s",(*it).first.c_str()),"");{
        SetCanvasDefaults( cBDt1DSub[(*it).first]);
        cBDt1DSub[(*it).first]->cd();
        TH1F *hDummy=new TH1F( Form("hDummy_%s",(*it).first.c_str()),"",100,0,1);
        SetDef(hDummy);
        hDummy->GetYaxis()->SetRangeUser(1,1000);
        cBDt1DSub[(*it).first]->SetLogy();
        hDummy->GetXaxis()->SetTitle(h1DHists[(*it).first]["vbf"]->GetXaxis()->GetTitle());
        hDummy->GetYaxis()->SetTitle(h1DHists[(*it).first]["vbf"]->GetYaxis()->GetTitle());
        hDummy->Draw();                          
        
        (*it).second->Draw("hist same");
        bdt1DLegs[(*it).first]->Draw();
        cBDt1DSub[(*it).first]->RedrawAxis();
      }

      

    }
   
      
  
}

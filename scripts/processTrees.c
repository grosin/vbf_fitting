#include "functions.h"
#include "TFile.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include "TH1.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TF1.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TF1.h"
#include <RooCategory.h>
#include <RooWorkspace.h>
#include <RooMinimizer.h>
#include <RooAddition.h>
#include <RooStats/AsymptoticCalculator.h>
#include "TRandom3.h"
#include <RooFitResult.h>
#include <RooArgSet.h>
#include <RooArgList.h>
#include "TLegend.h"
#include "TTree.h"
#include "TTreeFormula.h"
#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooGaussModel.h"
#include "RooConstVar.h"
#include "RooDecay.h"
#include "RooLandau.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooPlot.h"
#include "TAxis.h"
#include <RooMomentMorph.h>
#include "minitree.h"
#include "Math/Math.h"
#include "Math/QuantFuncMathCore.h"
#include <limits>
#include "TMVA/Reader.h"
#include "TMVA/Tools.h"
#include <chrono>
#include "RooKeysPdf.h"


using namespace RooFit ;
using namespace std;
using namespace RooStats;
using namespace std::chrono; 

// floating scheme configurations
bool addggFDiscr=false;
bool decorrelateABCDSystematics=true;
bool SplitZeroOnejetSamples=true; 
bool SplitggFCRTopWWSamples=true;
bool doABCD=true;
bool  doZeroOne=true;
double GeVtoMeV=1e3;
  
string whichSample(string name, vector<string>vec,vector<string>vec2){
  //cout<<"Searching for sample for "<<name<<endl;
  
  string sample="";
  //remove useless parts
  //if(TString(name.c_str()).Contains("Nom"))
  //name=eraseSubStrToEnd(name,"Nom");

  bool containsOther=false;
  for( vector<string>::iterator it=vec2.begin(); it!=vec2.end(); it++){
    string thisSampleTest=(*it);
    //cout<<"Testing (shortlist) "<<thisSampleTest<<" compared to "<<name<<endl;
    if( (TString(name.c_str())).Contains(thisSampleTest.c_str())){
      containsOther=true;
      sample=(*it);
      //      cout<<"Sample is (shortlist) "<<sample<<endl;
      return sample;
    }
  }
  
  for( vector<string>::iterator it2=vec.begin(); it2!=vec.end(); it2++){
    string thisSampleTest2= (*it2) ;
    //cout<<"Testing "<<thisSampleTest2<<" compared to "<<name<<endl;
    if( (TString(name.c_str())).Contains(thisSampleTest2.c_str())){
      sample=(*it2);
      //cout<<"Sample is "<<sample<<endl;
      return sample ;
    }
  }

  //cout<<"Sample is not found "<<sample<<endl;
  return sample;
}

string whichVariation(string name="",string sampleName="",string rName=""){
  if ( name.find("Nom")!=string::npos) return "nominal";
  name=eraseSubStr(name, sampleName);
  if( name.find("High_")!=string::npos)
    name=eraseSubStrToEnd(name,"High");
  else if (name.find("Low_")!=string::npos)
    name=eraseSubStrToEnd(name,"Low");
  else
    return "unkown";
  return name; 
}


int main (int argc, char* argv[]){

    auto Tstart = high_resolution_clock::now(); 

    string fname=argv[1];    // intree 
    string fnameout=argv[2]; // outtree 
    string cutConfigFile=argv[3]; // cut configuration file
    

    bool trimOut=false;
    bool MeVtoGeV=true;
    bool patch=false;
    bool noBDTweights=true;
    bool removeTreePrefix=false;
    string prefixtoRemove="";
    string pattern="_";
    string prefix="/eos/user/l/lbergste/merged_sys/Cut2Jet_v2/16a/";
    string dataset3D=prefix+"/dataset_METCut_SR_v2_noMjj";
    string datasetZjets=prefix+"/dataset_Zjets_SR";
    string datasetggFCR1=prefix+"/dataset_ggf_all_CR1";
    string datasetggFCR2=prefix+"/dataset_ggf_all_CR2";
    string datasetTop=prefix+"/dataset_TopWW_SR";
    int nEvents=-1;
    string chainFiles="";
    bool benchmark=false;
    int start=-1;
    int  stop=-1;
    bool quiet=false; 
    bool removePrefixFromBase=false;
    string diffVariable="";
    //string topWWDscir="bdt_TopWWAll";
    string topWWDscir="bdt_TopWWAll2";
    int nBinsDiff=8;
    bool isDiff=true;
    
    for( int i=3; i<(int)argc; i++){
      if( string(argv[i]).compare("-noGeV")==0){ MeVtoGeV=false; cout<<"No transformation MeV--> GeV"<<endl; }
      if( string(argv[i]).compare("-trim")==0) { trimOut=true; cout<<"Will be thinning output brnaches"<<endl;}
      if( string(argv[i]).compare("-patch")==0) { patch=true; cout<<"Will be only patching "<<endl; }
      if( string(argv[i]).compare("-pattern")==0){
        pattern=argv[i+1];
        cout<<"Will be processing only files matching "<<pattern<<endl;
      }
      if (string(argv[i]).compare("-quiet")==0){quiet=false;}
      if( string(argv[i]).compare("-prefix")==0){ prefix=argv[i+1];}
      if( string(argv[i]).compare("-removePrefixFromBase")==0){removePrefixFromBase=true; } 
      if (string(argv[i]).compare("-remove")==0){removeTreePrefix=true; prefixtoRemove = argv[i+1];}
      if (string(argv[i]).compare("-nEvents")==0){nEvents = atoi(argv[i+1]);}
      // run over multiple files at onece making a TChain. In this case the first argument, the input file, is used to determine the list of trees to create the chain on based on the list (or directory found in this argument 
      if( string(argv[i]).compare("-chain")==0) {
        chainFiles=argv[i+1];
        cout<<"Will be forming chain from list of files "<<chainFiles<<endl;
      }

      // calculate the number of events in the chain do not proccess anything else will return the number of entries for the first chain that is being read
      if (string(argv[i]).compare("-benchmark")==0){benchmark=true;}
      if (string(argv[i]).compare("-start")==0){start = atoi(argv[i+1]); cout<<"Will be starting reading from entry "<<start<<endl;}
      if (string(argv[i]).compare("-stop")==0){stop = atoi(argv[i+1]); cout<<"Will stop reading from entry "<<stop<<endl;}
      
      if( string(argv[i]).compare("-var")==0) {
        diffVariable=argv[i+1];
        isDiff=true;
        cout<<"Variable is  "<<diffVariable<<endl;
      }
      
    }
    

    
    
    
    TFile *f=TFile::Open(fname.c_str());
    TFile* fout=TFile::Open(fnameout.c_str(), chainFiles.size()==0 ? "RECREATE":"UPDATE");
    fout->cd();

     // identify the number of trees
    std::map <string,string> AllTrees;
    std::map<string,string> RunOverTrees;
    TKey *keyP=nullptr;
    TIter nextP(f->GetListOfKeys());
    int nTrees=0;
    if( !quiet) 
      cout<<"Will process trees :";
    while ((keyP=(TKey*)nextP())) {
      if (strcmp(keyP->GetClassName(),"TTree")) continue;
      string ttname=remove_extension(keyP->GetName());

      if( removePrefixFromBase) eraseAllSubStr(ttname,prefixtoRemove);
      
      //AllTrees[nTrees]=keyP->GetName();
      AllTrees[ttname]=ttname;
      if( TString(ttname).Contains(pattern.c_str())){
        RunOverTrees[ttname]=ttname;
        if(!quiet)
          cout<<RunOverTrees[ttname]<<" ";
        
        if(TString(ttname).Contains("__1up")) {
          // erase the __1up string and add __1down
          TString ttname2=TString(ttname).ReplaceAll("__1up","__1down");
            RunOverTrees[ttname2.Data()]=ttname2.Data();
            if(!quiet)
              cout<<RunOverTrees[ttname2.Data()]<<" ";
        }
        if(TString(ttname).Contains("__1down")) {
          // erase the __1up string and add __1down
          TString ttname2=TString(ttname).ReplaceAll("__1down","__1up");
          RunOverTrees[ttname2.Data()]=ttname2.Data();
          if(!quiet)
            cout<<RunOverTrees[ttname2.Data()]<<" ";
        }
        
       
      }
      nTrees++;
    }
    if(!quiet)
      cout<<endl;
   
    
    if(!quiet)
      cout<<"File has "<<nTrees<<" and will process "<<RunOverTrees.size()<<endl;

    // determine in the case of a chain wihch files contain which trees
    vector <string> filesIncludeChain=cleanChainFiles(RunOverTrees,chainFiles);

    
    vector<string> regions;
    std::map<string,string> regionCutsMap;
    // added cuts per sample
    std::map<string,string> addedCutMap;

      vector <string> treeNames;
    treeNames={"diboson","top","ggf","Zjets","Vgamma","Fakes","data","vh","htt","vbf"};


    std::map<string, vector<double>> binEdgesMap;
    binEdgesMap["Mjj"].push_back(200000.0);
    binEdgesMap["Mjj"].push_back(450000.0);
    binEdgesMap["Mjj"].push_back(700000.0);
    binEdgesMap["Mjj"].push_back(950000.0);
    binEdgesMap["Mjj"].push_back(1200000.0);
    binEdgesMap["Mjj"].push_back(1500000.0);
    binEdgesMap["Mjj"].push_back(2200000.0);
    binEdgesMap["Mjj"].push_back(3000000.0);
    binEdgesMap["Mjj"].push_back(5000000.0);
    
    
    // histograms defintions;
    std::map<string,std::map< string, vector<double>>> mapBins;
    std::map<string,string> cutsMap;
    if(isDiff){
      for(int i=0; i<nBinsDiff; i++){
        mapBins[topWWDscir][Form("CRTop_%d",i)]={6,-1,+1};
        mapBins["bdt_vbf"][Form("SRVBF_%d",i)]={25,0.5,1};
      }}
    
    mapBins["bdt_TopWW"]["CRWW"]={5,0.5,1.0};
    mapBins["bdt_ggFCR1"]["CRGGF1"]={4,-1,+1};
    mapBins["bdt_ggFCR2"]["CRGGF2"]={4,-1,+1};
    mapBins["bdt_ggFCR3"]["CRGGF3"]={8,-1,+1};
    mapBins["bdt_vbfggf"]["SRGGF"]={6,-1,+1};
    mapBins["MT"]["CRZjets"]={10,40*GeVtoMeV,150*GeVtoMeV};

    std::map<string,vector<string>> treesToSampleMap;

    treesToSampleMap["vbf"].push_back("hvbf0");
    for(int i=0;i<nBinsDiff; i++)
      treesToSampleMap["vbf"].push_back(Form("hvbf0_%d",i));
    
    treesToSampleMap["diboson"]={"hdiboson", "hdiboson1", "hdiboson2", "hdiboson3"};
    treesToSampleMap["top"]={"htop","htop1","htop1","htop2","htop3"};
    treesToSampleMap["ggf"]={"hggf","hggf1","hggf2","hggf3"};
    treesToSampleMap["Zjets"]={"hZjets0", "hZjets1"};
    treesToSampleMap["Vgamma"]={"hVgamma"};
    treesToSampleMap["Fakes"]={"hFakes"};
    treesToSampleMap["data"]={"hdata"};
    treesToSampleMap["vh"]={"hvh"};
    treesToSampleMap["htt"]={"hhtt"};

    for(std::map<string, vector<string>>::iterator it=treesToSampleMap.begin(); it!=treesToSampleMap.end(); it++)
      for(std::vector<string>::iterator ib=(*it).second.begin(); ib!=(*it).second.end(); ib++)
        addedCutMap[ (*ib) ] = "";
    
    // process the cuts file
    
    ifstream file(cutConfigFile.c_str());
    string line="";
    nBinsDiff=0;
    while ( getline (file,line) ){
      char reg[200];
      char cut[200];
      sscanf(line.c_str(),"%s %[^\n]",reg,cut);
      cutsMap[reg]=cut;

      cout<<"Region "<<reg<<" cuts "<<cut<<endl;
      
      if( TString(reg).Contains("SRVBF")){
        addedCutMap[Form("hvbf0_%d",nBinsDiff)]=Form("%s>=%.4lf && %s < %.4lf",diffVariable.c_str(),binEdgesMap[diffVariable].at(nBinsDiff),diffVariable.c_str(),binEdgesMap[diffVariable].at(nBinsDiff+1));
        cout<<"Added cuts" << addedCutMap[Form("hvbf0_%d",nBinsDiff)]<<endl;
        nBinsDiff++;
      }
      
      regions.push_back(reg);
    }
    file.close();
    


  
    
    
    // list of regions 
    //regions.push_back("CRGGF1");
    //regions.push_back("CRGGF2");
    //regions.push_back("CRGGF3");
    //regions.push_back("CRWW");
    //regions.push_back("CRZjets");
    //if(!isDiff){
    //      regions.push_back("CRTop");
    //regions.push_back("SRVBF");
//}

    
    
  vector <string> samples; 
  samples.push_back("hvbf0");
  samples.push_back("hdiboson");
  samples.push_back("htop");
  samples.push_back("hggf");
  //samples.push_back("hZjets");
  samples.push_back("hZjets0");
  samples.push_back("hVgamma");
  samples.push_back("Fakes");
  samples.push_back("hdata");
  samples.push_back("hvh");
  samples.push_back("hhtt");
  
  vector <string> samples2; 
  samples2.push_back("hZjets1");
  samples2.push_back("hdiboson1");
  samples2.push_back("hdiboson2");
  samples2.push_back("hdiboson3");
  samples2.push_back("htop1");
  samples2.push_back("htop2");
  samples2.push_back("htop3");
  samples2.push_back("hggf1");
  samples2.push_back("hggf2");
  samples2.push_back("hggf3");

  // observables per region
  std::map<string, string> obsRegions;
  obsRegions["SRVBF"]="bdt_vbf";
  //for( int i=0; i<(int)15; i++)
  //obsRegions[Form("SRVBF_%d",i)]="bdt_vbf";
  obsRegions["CRTop"]=topWWDscir;
  obsRegions["CRGGF3"]="bdt_ggFCR3";
  obsRegions["CRGGF2"]="bdt_ggFCR2";
  obsRegions["CRGGF1"]="bdt_ggFCR1";
  obsRegions["CRZjets"]="MT";
  obsRegions["CRWW"]="bdt_TopWW";
  //obsRegions["SRGGF"]="bdt_vbfggf";
  
  
  if(isDiff){
    for(int i=0; i<nBinsDiff; i++){
      //regions.push_back(Form("SRVBF_%d",i));
      //regions.push_back(Form("CRTop_%d",i));
      samples.push_back(Form("hvbf0_%d",i));
      samples2.push_back(Form("hvbf0_%d",i));
      obsRegions[Form("SRVBF_%d",i)]="bdt_vbf";
      obsRegions[Form("CRTop_%d",i)]=topWWDscir;
    }
  }
  
  // vbf bins are a special casse
  // diboson
  if(SplitZeroOnejetSamples && !SplitggFCRTopWWSamples)
    addedCutMap["hdiboson"]="inggFCR3!=1";
  else if (SplitZeroOnejetSamples &&  SplitggFCRTopWWSamples)
    addedCutMap["hdiboson"]="inggFCR3!=1 && inggFCR2!=1 && inggFCR1!=1";
  else if(SplitggFCRTopWWSamples && ! SplitZeroOnejetSamples)
    addedCutMap["hdiboson"]="inggFCR2!=1 && inggFCR1!=1";
  if(doZeroOne && SplitZeroOnejetSamples)
    addedCutMap["hdiboson1"]="inggFCR3==1";
  if(SplitggFCRTopWWSamples){
    addedCutMap["hdiboson2"]="inggFCR2==1";
    addedCutMap["hdiboson3"]="inggFCR1==1";
  }
  // ggF
  if( doABCD ){
    if ( SplitZeroOnejetSamples ) 
      addedCutMap["hggf"]="inggFCR1!=1 && inggFCR2!=1 && inggFCR3!=1 && inSR==1";
    else
      addedCutMap["hggf"]="inggFCR1!=1 && inggFCR2!=1 && inggFCR3!=1";
    
    addedCutMap["hggf1"]="inggFCR1==1";
    addedCutMap["hggf2"]="inggFCR2==1";
    addedCutMap["hggf3"]="inggFCR3==1";
  }
  // Zjets
  if(SplitZeroOnejetSamples)
    addedCutMap["hZjets0"]="inggFCR3!=1";
  if(doZeroOne && SplitZeroOnejetSamples)
    addedCutMap["hZjets1"]="inggFCR3==1";
  // top
  if( SplitZeroOnejetSamples && !SplitggFCRTopWWSamples)
    addedCutMap["htop"]="inggFCR3!=1";
  else if ( SplitZeroOnejetSamples && SplitggFCRTopWWSamples)
    addedCutMap["htop"]="inggFCR3!=1 && inggFCR2!=1 && inggFCR1!=1";
  else if( SplitggFCRTopWWSamples && !SplitZeroOnejetSamples)
    addedCutMap["htop"]="inggFCR2!=1 && inggFCR1!=1";
  if(doZeroOne && SplitZeroOnejetSamples)
    addedCutMap["htop1"]="inggFCR3==1";
  if( SplitggFCRTopWWSamples){
    addedCutMap["htop2"]="inggFCR2==1";
    addedCutMap["htop3"]="inggFCR1==1";
  }


  
  
  
  
  
  for(std::map<string,string>::iterator it=RunOverTrees.begin(); it!=RunOverTrees.end(); it++){
    
    cout<<"Running over tree "<< (*it).second << endl;
    
    TChain *tin = nullptr;
    tin  = new TChain((*it).second.c_str(),(*it).second.c_str());
    if( chainFiles.size()==0) 
        tin->AddFile(f->GetName());
      else{
        for(std::vector<string>::iterator ss=filesIncludeChain.begin(); ss!=filesIncludeChain.end(); ss++){
          if(!quiet)
            cout<<"Adding to chain "<<(*ss)<<endl;
          tin->Add( (*ss).c_str());
        }}

      auto cachesize = 10000000;   //10 MBytes
      //tin->SetCacheSize(cachesize*50);   //<<<
      tin->SetCacheLearnEntries(5);   //<<< we can take the decision after 5 entries
      
      Long64_t totalEvents = tin->GetEntries();
    
    vector <string> empty;
    string treeBaseName=whichSample( (*it).second,treeNames,empty);
    
    
    // determine which type of variation this is;
    string var="";
    if(TString( (*it).second).Contains("nominal")){
      var="Nom";
    }
    else{
      TString tmp((*it).second.c_str());
      tmp.ReplaceAll(Form("%s_",treeBaseName.c_str()),"");
      tmp.ReplaceAll("__1up","High");
      tmp.ReplaceAll("__1down","Low");
      var=string(tmp.Data());
    }
    cout<<"Variation is " <<var<<endl;

    vector <string> hbases=treesToSampleMap[treeBaseName];
    for( vector <string>::iterator ib=hbases.begin(); ib!=hbases.end(); ib++){
      
      string histBaseName = (*ib) + var ;
      cout<<" Histograms is "<< histBaseName<<endl;

      // Need to loop over all regions and the corresponding selection cuts;
      for( vector<string>::iterator is=regions.begin(); is!=regions.end(); is++){

        cout<<"Loping over region "<< (*is)<<endl;
        
        string cuts="(";
        cuts+=cutsMap[ (*is)];
        if(addedCutMap[(*ib)].size()>0)
          cuts+=" && "+addedCutMap[ (*ib) ];
        cuts+=")";
        
        cout<<" "<< (*is)<<" sample "<< (*ib)<<" cuts: "<<cuts<<" for variable "<<obsRegions[ (*is)]<<endl;
        
        string thisHistName= histBaseName+"_"+(*is)+"_obs_"+obsRegions[ (*is)];
        
        
        
        f->cd();
        TTree *intree=(TTree*)tin->CopyTree(cuts.c_str());
        intree->SetDirectory(0);
        TH1F *h=nullptr;
        fout->cd();
        /*
        if( (TString( (*it).second).Contains("Fake") || intree->GetEntries()/ (mapBins[ obsRegions[ (*is) ]][*is])[0] > 1e4  || intree->GetEntries()==0)
            && ( !((TString((*it).second)).Contains("CRTop") || (TString((*it).second)).Contains("SRVBF"))  && intree->GetEntries()>0)){
          
          h=new TH1F(Form("%s",thisHistName.c_str()),"",(int)(mapBins[ obsRegions[ (*is) ]][*is])[0],(mapBins[ obsRegions[ (*is) ]][*is])[1],(mapBins[ obsRegions[ (*is) ]][*is])[2]); 
          
          //if(intree->GetEntries()>0)
          intree->Draw(Form("%s>>%s",obsRegions[ (*is)].c_str(),thisHistName.c_str()),Form("weight*(%s)",cuts.c_str()),"");

          h->Sumw2(true); 
          h->Write();
          TH1F* hNorm=(TH1F*)h->Clone(Form("%sNorm",h->GetName()));
          hNorm->Write();
          delete hNorm;
        }
        */
        //else {
          
          RooRealVar var(obsRegions[ (*is)].c_str(),obsRegions[ (*is)].c_str(),0,(mapBins[ obsRegions[ (*is) ]][*is])[1],(mapBins[ obsRegions[ (*is) ]][*is])[2]);
          RooRealVar wvar("weight","weight",1,-1e10,1e10);
          RooArgSet vars;
          vars.add(var);
          vars.add(wvar);
          
          RooDataSet data("data","data",intree,vars,"","weight");
          
          
          h=(TH1F*)data.createHistogram(Form("%s_InputHis",thisHistName.c_str()),var,
                                        Binning((int)(mapBins[ obsRegions[ (*is) ]][*is])[0],(mapBins[ obsRegions[ (*is) ]][*is])[1],(mapBins[ obsRegions[ (*is) ]][*is])[2]));
          
          if(h==nullptr)
            h=new TH1F(Form("%s_InputHis",thisHistName.c_str()),"",(int)(mapBins[ obsRegions[ (*is) ]][*is])[0],(mapBins[ obsRegions[ (*is) ]][*is])[1],(mapBins[ obsRegions[ (*is) ]][*is])[2]); 
          
          h->Sumw2(true); 
         
          
           // maybe fix this at some point 
          for(int b=1; b<(int)h->GetNbinsX()+1;b++){
            if( h->GetBinContent(b) < 0 ){
              h->SetBinContent(b,0);
              h->SetBinError(b,0);
            }
          }
          
          //if(h->Integral()>0 && !((TString( (*it).second).Contains("Fake") || intree->GetEntries()/ (mapBins[ obsRegions[ (*is) ]][*is])[0] > 1e4  || intree->GetEntries()==0)
          //                      && ( !((TString((*it).second)).Contains("CRTop") || (TString((*it).second)).Contains("SRVBF"))  && intree->GetEntries()>0))){

          if( h->Integral()> 0 && !TString( (*it).second).Contains("Fake") && intree->GetEntries()/ (mapBins[ obsRegions[ (*is) ]][*is])[0] < 1e4 ){

            RooKeysPdf smoothed("smoothed","smoothed",var,data,RooKeysPdf::Mirror::NoMirror,1.5);
            TH1F *hnsmoothed = (TH1F*)smoothed.createHistogram(thisHistName.c_str(),var,
                                                               Binning((int)(mapBins[ obsRegions[ (*is) ]][*is])[0],(mapBins[ obsRegions[ (*is) ]][*is])[1],(mapBins[ obsRegions[ (*is) ]][*is])[2]),
                                                               Extended(false));
            
            
            hnsmoothed->Sumw2(true); 
            hnsmoothed->SetName(thisHistName.c_str());
            hnsmoothed->Scale(h->Integral()/hnsmoothed->Integral());
            // maybe fix this at some point 
            for(int b=1; b<(int)h->GetNbinsX()+1;b++){
              if( hnsmoothed->GetBinContent(b) < 0 )
                hnsmoothed->SetBinContent(b,0);
              
              hnsmoothed->SetBinError(b,hnsmoothed->GetBinContent(b)*h->GetBinError(b)/h->GetBinContent(b));
              if( h->GetBinContent(b)==0)
              hnsmoothed->SetBinError(b,TMath::Abs(hnsmoothed->GetBinContent(b)*2));
            }
          
        
            cout<<"Smoothed integral "<<hnsmoothed->Integral()<<" orig "<<h->Integral()<<endl;
            hnsmoothed->Write();
            
            TH1F* hNorm=(TH1F*)hnsmoothed->Clone(Form("%sNorm",hnsmoothed->GetName()));
            hNorm->Write();
            delete hnsmoothed;
            delete hNorm;
          }

          else {
            h->SetName(thisHistName.c_str());
            TH1F* hNorm=(TH1F*)h->Clone(Form("%sNorm",h->GetName()));
            hNorm->Write();
            delete hNorm;
          }
          
          h->Write();
          
          delete h;
          delete intree;
          
          
      }

    }
    

    delete tin;

  }

  fout->Close();
  
  auto Tstop = high_resolution_clock::now();
  auto duration = duration_cast<microseconds>(Tstop - Tstart); 
  cout<< "Execution time " << duration.count() << endl;
  
  
    return 0; 
}

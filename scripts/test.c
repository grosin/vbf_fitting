#include "tools.h"
#include "minitree.h"

#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooGaussModel.h"
#include "RooConstVar.h"
#include "RooDecay.h"
#include "RooLandau.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooPlot.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TH1.h"
#include "RooNDKeysPdf.h"
#include <RooMomentMorph.h>


using namespace RooFit;
using namespace RooStats;
using namespace std;
void test(){

  RooWorkspace *w=new RooWorkspace("w","w");
  ModelConfig *model=new ModelConfig("model","model");
  model->SetWorkspace(*w);

  RooRealVar x("x","x",0,-1,+1);
  RooRealVar mean("mean","mean",0,-1,+1);
  RooRealVar sigma("sigma","sigma",1);
  RooRealVar sigma2("sigma2","sigma2",2);
  RooGaussian *gaus=new RooGaussian("gauss","gauss",x,mean,sigma);
  RooGaussian *gaus2=new RooGaussian("gauss2","gauss",x,mean,sigma2);
  
  model->SetPdf(*gaus);
  model->SetParametersOfInterest(RooArgSet(mean,sigma));
  model->SetObservables(RooArgSet(x));
  RooDataSet *data=(RooDataSet*)AsymptoticCalculator::GenerateAsimovData(*gaus,x);
  w->import(*model);
  w->import(*data);
  w->writeToFile("testModel.root");
  
}

void changeTreeNames(){
  TFile *f=new TFile ("NtuplesWithCuts/eval_3D_AllSamps_ggF_0-1jet_Jan18.root","UPDATE");

  //  
  std::map<string,string> name;
  //"VBFPreSel3_data_nominal diboson_nominal top_nominal Vgamma_nominal Zjets_nominal ggf_nominal vbf_nominal"
    name["data_nominal"]="Data";
    name["diboson_nominal"]="WW";
    name["top_nominal"]="Top";
    name["Vgamma_nominal"]=" Vgamma";
    name["Zjets_nominal"]="Zjets";
    name["ggf_nominal"]="GGF";
    name["vbf_nominal"]="VBF";
    TTree *t=nullptr;      

    for(std::map<string, string>::iterator it=name.begin(); it!=name.end(); it++){
      t=(TTree*)f->Get((*it).second.c_str());
      if(t == nullptr) { cout<<"Did not find tree " <<(*it).first<<endl; continue; } 
      cout<<"Setting name "<<(*it).first.c_str();
      t->SetName((*it).first.c_str());
    }  
    f->Write();
    f->Close();


}

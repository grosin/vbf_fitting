#include "tools.h"
bool doSignificance=false;
bool doRanking=false;
bool isBBB=false;
bool addOtherTheory=true;
bool normTheoryPredtoYR4=true;
//string topWWDscir="bdt_TopWWAll";
string topWWDscir="bdt_TopWWAll2";
bool doReg=false;
double  tauD=0.5;
std::map<string,vector<double>> ScanAsimovVar(RooWorkspace *w=nullptr,
                                              RooWorkspace *w2=nullptr,
                                              RooAbsPdf *NewPdf=nullptr,
                                              string parName="",double val=1.0,string newName=""){

 std:map<string, vector<double>> fitResults;
 
 
  ModelConfig *model=(ModelConfig*)w->obj("ModelConfig");
  RooCategory *channelCat = (RooCategory*)w->cat("channelCat" );
  RooSimultaneous *pdf=(RooSimultaneous*)model->GetPdf();
  RooArgSet *global=(RooArgSet*)model->GetGlobalObservables();
  RooRealVar *mu_var=(RooRealVar*)w->var(parName.c_str());
  mu_var->setVal(val);

  //ModelConfig *altModel=(ModelConfig*)w->obj("altModel");
  ModelConfig *altModel=(ModelConfig*)w2->obj("atlModel");
  //RooAbsPdf *NewPdf=altModel->GetPdf();

  //const RooArgSet *nomSnap=w->getSnapshot("prefit");
  cout<<"Generating data for point "<<val<<endl;
  //RooAbsData *data=AsymptoticCalculator::MakeAsimovData(*model,*nomSnap,*global);
  RooAbsData *data=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
  //cout<<"Scanning point 8"<<val<<endl;
  //mu_var->SetName(newName.c_str());
  //w->import(*mu_var);
  //cout<<"Scanning point 9"<<val<<endl;

  string oldName=mu_var->GetName();
  mu_var->SetName(newName.c_str());

  
  
  cout<<"Fitting "<<endl;
  RooAbsReal *mNll= NewPdf->createNLL( *data,
                                       RooFit::Constrain(*model->GetNuisanceParameters()), 
                                       RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                       RooFit::Offset(kTRUE), 
                                       Extended(true),
                                       RooFit::Verbose(kFALSE));

  RooMinimizer *msys= new RooMinimizer(*mNll);;
  msys->setEps(0.1);
  msys->optimizeConst(2);
  msys->setStrategy(1);
  msys->minimize("Minuit2");
  
  RooFitResult *fitRes=msys->save("tmpFit");
  RooArgList parsFitr=fitRes->floatParsFinal();
  
  TIter it = parsFitr.createIterator();
  RooRealVar *tmpV=nullptr;
  while( (tmpV=(RooRealVar*)it.Next())){
    vector <double> rr;
    rr.push_back(tmpV->getVal());
    rr.push_back(tmpV->getError());
    fitResults[tmpV->GetName()]=rr;
  }
  
  mu_var->SetName(oldName.c_str());
  mu_var->setVal(1.0);
  
  delete fitRes;
  delete msys;
  delete mNll;
  delete data;

  model->GetPdf()->Print("V");
  
  return fitResults;
}


// isBBB = is bin by bin; 
void fitWS(string fname="BkgOnly_combined_NormalMeasurement_model_afterFit_asimovData.root",bool patchDiff=true,string wsname="w"){
  ROOT::Math::MinimizerOptions::SetDefaultStrategy(2);
  //ROOT::Math::MinimizerOptions::SetDefaultPrecision(0.0000001);
  ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");


  //TFile *f=TFile::Open("combined.root");
  TFile *f=TFile::Open(fname.c_str());
  RooWorkspace *w=(RooWorkspace*)f->Get(wsname.c_str());
  w->Print();
  ModelConfig *model=(ModelConfig*)w->obj("ModelConfig");
  RooCategory *channelCat = (RooCategory*)w->cat("channelCat" );
  //RooSimultaneous *pdf=(RooSimultaneous*)model->GetPdf();
  RooSimultaneous *pdf=(RooSimultaneous*)w->pdf("simPdf");
  if(pdf==nullptr) { cout<<"pdf is null"<<endl; return ; } 
  RooArgSet *global=(RooArgSet*)model->GetGlobalObservables();
  RooRealVar *poi=(RooRealVar*)w->var("mu_vbf");
  poi->Print();
  poi->setVal(1.0);
  poi->setConstant(true);
  //poi->setRange(0.0,5);
  //  return ;

  TH2F *hConfusionMatrix=nullptr;
  
  RooRealVar *mu_top=(RooRealVar*)w->var("mu_top");
  //mu_top->setConstant(false);
  //mu_top->setRange(0,5);

  RooRealVar *mu_ww=(RooRealVar*)w->var("mu_ww");
  //if(mu_ww!=nullptr)
  //mu_ww->setConstant(false);
  //mu_ww->setRange(0,5);
  
  RooRealVar *mu_wwTop=(RooRealVar*)w->var("mu_wwTop");
  mu_wwTop->setConstant(false);
  
  
  RooRealVar *mu_ggf=(RooRealVar*)w->var("mu_ggf");
   mu_ggf->setConstant(true);
  //mu_ggf->setRange(-5,+5);
  
  //mH->setVal(125);
  //RooAbsData *data=(RooAbsData*)w->data("asimovData");


 w->allVars().selectByName("mu_top*")->setAttribAll("Constant",true);
w->allVars().selectByName("mu_ww")->setAttribAll("Constant",true);
w->allVars().selectByName("mu_ww1")->setAttribAll("Constant",true);
w->allVars().selectByName("mu_ww2")->setAttribAll("Constant",true);
w->allVars().selectByName("mu_ww3")->setAttribAll("Constant",true);
w->allVars().selectByName("mu_ww4")->setAttribAll("Constant",true);

w->allVars().selectByName("OneOverC_bin_")->setAttribAll("Constant",true);
w->allVars().selectByName("matrix_bin+")->setAttribAll("Constant",true);

isBBB=w->var("sigma_bin0")==nullptr ? true:false;

 RooRealVar *lumi=(RooRealVar*)w->var("Lumi");
 lumi->Print();
 
  RooAbsData *data=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
  //RooAbsData *data=AsymptoticCalculator::GenerateAsimovData(*pdf,RooArgSet(*m4l,*channelCat));
  //RooAbsData *data=AsymptoticCalculator::MakeAsimovData(*model,RooArgSet(*xs,mH), *global);

  /*
  // Make two worksapces with both settings on
  mu_ww->setConstant(true);
  mu_top->setConstant(true);
  mu_wwTop->setConstant(false);
  RooAbsData *data1=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
  data1->SetName("asimovData_floatingTopWW");
  w->import(*data1,RecycleConflictNodes());
  w->writeToFile("XS_singleParWWTop_v10_04_2020.root");
  //w->writeToFile("singleParWWTop_v10_04_2020.root");
  */
  /*
  // Make two worksapces with both settings on
  mu_ww->setConstant(false);
  mu_top->setConstant(false);
  mu_wwTop->setConstant(true);
  RooAbsData *data2=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
  //data2->SetName("asimovData_commonTopWW");
  w->import(*data2,RecycleConflictNodes());
  w->writeToFile("XS_sepWWTopParams.root_v10_04_2020.root");
  //w->writeToFile("sepWWTopParams.root_v10_04_2020.root");
  //return ;
  */


  std::map<string,double> UnitsScaling;
  UnitsScaling["ab"]=1e3;
  UnitsScaling["fb"]=1;
  
  std::map<string,int> nBinsDiff;
  nBinsDiff["Mjj"]=8;
  nBinsDiff["lep0_pt"]=8;
  nBinsDiff["jet0_pt"]=7;
  nBinsDiff["Mll"]=9;
  nBinsDiff["DYjj"]=11;
  nBinsDiff["DYll"]=10;
  nBinsDiff["DPhill"]=12;
  
  string currentDistribution="DYll";
  std::map<string, vector<double>> binEdesMap;
  double GeVtoMeV=1e3;
  std::map<string, string> unitsMap;
  unitsMap["DYll"]="fb";
  std::map<string,string> labelMap;
  labelMap["Mjj"]="#it{m}_{#it{jj}}";
  labelMap["DYll"]="#it{DY}_{#it{ll}}";
  const int isize=nBinsDiff[currentDistribution]+1;
  double binEdges[]={0,0.15,0.3,0.45,0.6,0.75,0.9,1.05,1.2,1.5,4.0};
  
  
  RooRealVar *mu_Zjets=(RooRealVar*)w->var("mu_Zjets");
  //mu_Zjets->setConstant(true);
  
  pdf->Print("V");

  //return ;
  RooArgSet allPars;
  w->allVars().selectByName("alpha_*")->setAttribAll("Constant",false);
  //w->allVars().selectByName("alpha_*theo*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("alpha_*JER*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("alpha_*EG*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("alpha_*JET*")->setAttribAll("Constant",true);
  w->allVars().selectByName("alpha_*fJVT*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("mu_ggf_*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("mu_wwTop*")->setAttribAll("Constant",true);
  //w->allVars().selectByName("mu_wwTop_bin*")->setAttribAll("Constant",false);
  /*
  poi->setConstant(true); 
  std::map<string,RooRealVar *> diffXsPois;
  for( int i=0; i<(int)nBinsDiff[currentDistribution]; i++){
    diffXsPois[Form("mu_vbf_bin_%d",i)]=(RooRealVar*)w->var(Form("mu_vbf_bin_%d",i));
    diffXsPois[Form("mu_vbf_bin_%d",i)]->setConstant(false);
    diffXsPois[Form("mu_vbf_bin_%d",i)]->setRange(-0.5,+3);
    
    
  }
  //diffXsPois[Form("mu_vbf_bin_%d",0)]->setConstant(true);
  */
  
  /*
  RooArgSet *sset=(RooArgSet*)w->allVars().selectByName("mu_wwTop*");
  TIter ssetIter_=sset->createIterator();
  RooRealVar *tmpAllPars_set=nullptr;
  while( (tmpAllPars_set=(RooRealVar*)ssetIter_.Next())){
    tmpAllPars_set->SetName("mu_wwTop");
    //w->renameSet(tmpAllPars_set->GetName(),"mu_wwTop");
  }
  */
  poi->setVal(1.0); 
 
  const RooArgSet *sset2=model->GetNuisanceParameters();
  TIter ssetIter2_=sset2->createIterator();
  RooRealVar *tmpAllPars2_set=nullptr;
  while( (tmpAllPars2_set=(RooRealVar*)ssetIter2_.Next())){
    tmpAllPars2_set->SetName( (TString(tmpAllPars2_set->GetName()).ReplaceAll("Region1","")).Data());
    tmpAllPars2_set->SetName( (TString(tmpAllPars2_set->GetName()).ReplaceAll("Region2","")).Data());
    tmpAllPars2_set->SetName( (TString(tmpAllPars2_set->GetName()).ReplaceAll("Region3","")).Data());
  }
  
  const RooArgSet *sset3=model->GetGlobalObservables();
  TIter ssetIter3_=sset3->createIterator();
  RooRealVar *tmpAllPars3_set=nullptr;
  while( (tmpAllPars3_set=(RooRealVar*)ssetIter3_.Next())){
    tmpAllPars3_set->SetName( (TString(tmpAllPars3_set->GetName()).ReplaceAll("Region1","")).Data());
    tmpAllPars3_set->SetName( (TString(tmpAllPars3_set->GetName()).ReplaceAll("Region2","")).Data());
    tmpAllPars3_set->SetName( (TString(tmpAllPars3_set->GetName()).ReplaceAll("Region3","")).Data());
  }



  
  // In case of a differential workspace, loop over all channels and add a cross section for each bin
  if(patchDiff) {
    // open file with matrices
    std::map<string, vector<vector<double>>>  rawMatrix;
    //std::map<string, vector<vector<double>>>  normMatrix;
    ifstream matrixfile("../../matrix_projections_with_fakes.txt");
    string line="";
    string whichVar="";
    int binCounterx=0;
    int binCountery=0;
    std::map<string, std::pair<int,int>> counters;

    for(std::map<string,int>::iterator it=nBinsDiff.begin(); it!=nBinsDiff.end(); it++){
      vector <double> empty;
      for( int lk=0; lk<(nBinsDiff[currentDistribution]+1); lk++)
        empty.push_back(0);
      vector <vector<double>> emptyY; 
      for(int lk=0; lk<(int)nBinsDiff[currentDistribution]; lk++)
        emptyY.push_back(empty);
      rawMatrix[ (*it).first]=emptyY;
      counters[ (*it).first] = make_pair(0,0); 
    }
    
    while ( getline (matrixfile,line) ){
      if(line.size()==0) continue; 
      if(line.size()>0 && line[0]==' ') continue ; // emptyline, skip; 

      string newVar="";
      string restline;
      string binS;

      vector <string> tokens; 
      int itoken=0;
      tokens.push_back("");
      for( int ic=0;ic<(int)line.size(); ic++){
        if(line[ic]==',') { tokens.push_back(""); itoken++; continue; }
        tokens.at(itoken)+=line[ic];
        //cout<<"token "<<tokens.at(itoken)<<" "<<itoken<<endl;
      }
     
      
      newVar=tokens.at(0);
      binS=tokens.at(1);
      if(newVar.compare(currentDistribution)!=0) continue; 
      //cout<<"New var "<<newVar<<endl;
      
      
      //process the file
      binCounterx=0;
      binCountery=counters[newVar].first;

      for( int ic=2; ic<(int)tokens.size(); ic++){
        cout<<"counter "<<ic<<endl;
        cout<<"value "<<tokens.at(ic)<<endl;
        double migs=std::stod(tokens.at(ic));
        (rawMatrix[newVar])[binCountery][binCounterx]=migs;
        //(normMatrix[newVar])[binCountery][binCounterx]=migs;
        cout<< newVar<<" x,y "<< binCountery<<" - "<< binCounterx<<" "<<(rawMatrix[newVar])[binCountery][binCounterx]<<endl;
        counters[newVar].second=binCounterx;
        binCounterx++;
      }
      counters[newVar].first=counters[newVar].first+1;
    }
    matrixfile.close();

    
    // loop over all categories
    int iCat=0;
    //for (const auto nameIdx : *channelCat) {
    TIterator *iterCat = channelCat->typeIterator();
    RooCatType *tt=nullptr; 
    while ( (tt= (RooCatType*)iterCat->Next())){
      RooProdPdf *channelPdf=(RooProdPdf*)pdf->getPdf(tt->GetName() );
      //replace things in the SRVBF and CR Top
      if( TString(tt->GetName() ).Contains("SRVBF") || TString(tt->GetName()).Contains("CRTop")){
        channelPdf->Print();
        const RooArgList pdfList=channelPdf->pdfList();

        TIterator *iterPdf=pdfList.createIterator();
        RooAbsPdf *ipdf=nullptr;
        while( ( ipdf = (RooAbsPdf*)iterPdf->Next())) {
          if( TString(ipdf->GetName() ).Contains("SRVBF") || TString(ipdf->GetName()).Contains("CRTop")){
            ipdf->Print();

          }
        }
        cout<<endl;
      }
      iCat++;
    }
    // regions to apply the matrix to
    vector <pair<string,string>> matRegions;
    matRegions.push_back(make_pair("SRVBF","bdt_vbf"));
    matRegions.push_back(make_pair("CRTop",topWWDscir));

    vector <double> norms;
    
    
    for(int y=0; y<(int)nBinsDiff[currentDistribution]; y++){
        double norm=0;
        for(int x=0; x<(int)nBinsDiff[ currentDistribution]; x++){
          norm+=(rawMatrix[currentDistribution])[y][x];
        }
        norms.push_back(norm);
        //for(int x=0; x<(int)nBinsDiff[ (*mMat).first]; x++){
        //normMatrix[ (*mMat).first ][y][x]=1/norm * normMatrix[ (*mMat).first ][y][x];
        //}
      }
    
    
   
    TFile *fnew=new TFile("ws.root","RECREATE");
    fnew->cd();
    RooWorkspace *w2=new RooWorkspace("w","w");
    //RooRealVar *dummy=new RooRealVar("dummy","dummy",2,0,100);
    //w2->import(*dummy);

    if(hConfusionMatrix!=nullptr) delete hConfusionMatrix;
    fnew->cd();
    hConfusionMatrix=new TH2F("hConfusionMatrix","",nBinsDiff[currentDistribution],0,nBinsDiff[currentDistribution],nBinsDiff[currentDistribution],0,nBinsDiff[currentDistribution]);
    hConfusionMatrix->SetDirectory(0);
    hConfusionMatrix->SetStats(0);
    hConfusionMatrix->GetXaxis()->SetTitle("Reco");
    hConfusionMatrix->GetYaxis()->SetTitle("True");

    RooRealVar *sigma=new RooRealVar("sigma","sigma",0);
    sigma->setConstant(true);
    
    std::map<int, RooRealVar*> sigmaMap;
    std::map<int, RooRealVar*> muMap;

    RooArgSet argElements;
       // vbf0_0_SRVBF_0_bdt_vbf_overallNorm_x_sigma_epsilon
    for( int ik=0 ; ik<(int)nBinsDiff[currentDistribution]; ik++){
      RooRealVar *mu_bin=(RooRealVar*)w->var(Form("mu_vbf_bin_%d",ik));
      mu_bin->setConstant(true);
      RooRealVar *sigma_bin=new RooRealVar(Form("sigma_bin%d",ik),Form("sigma_bin%d",ik),mu_bin->getVal(),0,20,"fb");
      sigma_bin->Print();
      w2->import(*sigma_bin);
      mu_bin->setVal(1.0);
      mu_bin->setConstant(true);
      w2->import(*mu_bin);
      sigmaMap[ik]=sigma_bin;
      muMap[ik]=mu_bin;
      argElements.add(*sigma_bin);
      sigma->setVal(sigma->getVal()+sigma_bin->getVal());
    }
    w2->import(*sigma);

    std::map<string,RooRealVar*> matElementV;

    RooArgSet normMapArgs;
    string normMapString="(";
    
        for( int ik=0 ; ik<(int)nBinsDiff[currentDistribution]; ik++){
          RooRealVar *notFid=new RooRealVar(Form("notFid_bin%d_incl",ik),Form("notFid_bin%d_incl",ik),0);
          notFid->setConstant(true);
          w2->import(*notFid);
          delete notFid;

          for(vector<pair<string,string>>::iterator it=matRegions.begin(); it!=matRegions.end(); it++){
            
            RooProduct *prod=(RooProduct*)w->obj(Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()));
            prod->Print();
            RooArgList components=prod->components();

            string matElement="(";

            RooArgSet thisArgElements;
            thisArgElements.add(argElements);

          for( int iy=0 ; iy<(int)nBinsDiff[currentDistribution]; iy++){

            if(matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)]!=nullptr) delete matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)];

            matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)]=new RooRealVar(Form("eff_bin%d_inclbin%d_incl",ik,iy),Form("eff_bin%d_inclbin%d_incl",ik,iy),1/norms.at(iy)*(rawMatrix[currentDistribution])[ik][iy],1/norms.at(iy)*(rawMatrix[currentDistribution])[ik][iy],1/norms.at(iy)*(rawMatrix[currentDistribution])[ik][iy]);

            thisArgElements.add(*matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)]);

            matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)]->setConstant(true);
            cout<<"Mat element V"<<endl;
            matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)]->Print();
            w2->import(*matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)]);
            
            //matElement+=Form("%lf*%s+",1/norms.at(iy)*(rawMatrix[currentDistribution])[ik][iy],sigmaMap[iy]->GetName());
            matElement+=Form("%s*%s+",matElementV[Form("eff_bin%d_inclbin%d_incl",ik,iy)]->GetName(),sigmaMap[iy]->GetName());
            hConfusionMatrix->SetBinContent(ik+1,iy+1,1/norms.at(iy)*(rawMatrix[currentDistribution])[ik][iy]);
            hConfusionMatrix->GetXaxis()->SetBinLabel(ik+1,Form("sigma_bin%d",ik));
            hConfusionMatrix->GetYaxis()->SetBinLabel(ik+1,Form("sigma_bin%d",ik));

          

          }
          matElement.pop_back();
          matElement+=")";

          
          
          RooFormulaVar *migMat=new RooFormulaVar(Form("migMatProj_%s_bin%d", (*it).first.c_str(),ik),matElement.c_str(),thisArgElements);
          //w2->import(*migMat);
          
          migMat->Print();
          migMat->Print("V");
          cout<<"BBB cross section "<<sigmaMap[ik]->getVal()<<" formula "<<migMat->getVal()<<endl;

          //Attention Lumi set manually here 
          normMapString+=Form("139*%s+",migMat->GetName());
          normMapArgs.add(*migMat);
          
          components.add(*migMat);
          //components.add(*dummy);
          //delete prod;
          //prod->SetName(Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon_PreFix",ik,(*it).first.c_str(),ik,(*it).second.c_str()));
          //w->import(*prod,false);
          //w->import(*prod,true);
          //delete prod; 
          RooProduct *prod2=new RooProduct(Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()),
                                           Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()),
                                           components);
          
          prod2->Print();
          w2->import(*prod2);
          string sreplace="prod::"+string(Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()))+"(";
          string sreplace2; 
          TIterator *ttb=components.createIterator();
          RooAbsArg *element=nullptr;
          while(( element=(RooAbsArg*)ttb->Next())){
            sreplace+=element->GetName();
            sreplace+=",";
          sreplace2+=element->GetName();
          sreplace2+=",";
        }
        sreplace.pop_back();
        sreplace+=")";
        sreplace2.pop_back(); 
        cout<<" s replace "<<sreplace<<endl;

        //w->extendSet(Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()),
        //sreplace2.c_str());
        //w->factory(sreplace.c_str());
        
        //prod->Print();
        //w->import(components,true);
        //w->renameSet(Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()),Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon_PreFix",ik,(*it).first.c_str(),ik,(*it).second.c_str()));
        //prod=new RooProduct(Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()),
        //Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()),
        //                  components);

        //w->import(*prod,RecycleConflictNodes());
        //w->import(*pdf,RecycleConflictNodes());
        
        //w->import(*prod,true);
        //delete prod; 
        //RooProduct *pp=(RooProduct*)w->obj(Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",ik,(*it).first.c_str(),ik,(*it).second.c_str()));
        //cout<<"New prod "<<endl;
        //pp->Print();

        
          }
          normMapString.pop_back();
          normMapString+=")";
          RooFormulaVar *normMapV= new RooFormulaVar(Form("norm_bin%d_incl",ik),normMapString.c_str(),normMapArgs);
          w2->import(*normMapV);
            
          
    }
    cout<<"Bye "<<endl;

    
    
    string pdfName=pdf->GetName();
    RooSimultaneous* simPDF2=(RooSimultaneous*)pdf->Clone(pdfName.c_str());
    
    w2->import(*simPDF2,RecycleConflictNodes());
    w2->import(*simPDF2,RecycleConflictNodes());
    w2->import(*data,RecycleConflictNodes());
    w2->merge(*w);

    ModelConfig *model2=new ModelConfig(model->GetName(),w2);
    //(ModelConfig*)model->Clone("ModelConfig");
    model2->SetWorkspace(*w2);
    model2->SetPdf(*simPDF2);
    
    model2->SetGlobalObservables(*global);
    model2->SetObservables(*model->GetObservables());
    if(model->GetParametersOfInterest()!=nullptr)
      model2->SetParametersOfInterest(argElements);
    if(model->GetConstraintParameters()!=nullptr)
      model2->SetConstraintParameters(*model->GetConstraintParameters());
    if(model->GetConditionalObservables()!=nullptr)
      model2->SetConditionalObservables(*model->GetConditionalObservables());
    if(model->GetNuisanceParameters()!=nullptr)
      model2->SetNuisanceParameters(*model->GetNuisanceParameters());
    //model2->SetPdf(pdfName.c_str());
    
    model2->Print("V");
    model2->SetName(model->GetName());
    model2->SetWorkspace(*w2);
    w2->import(*model2);
    model2->SetWorkspace(*w2);
    //std::list< TObject * > lobj=w->allGenericObjects();
    //    for(std::list<TObject*>::iterator it=lobj.begin(); it!=lobj.end(); it++)
    //w2->import( *(*it));
    //w->saveSnapshot("prefit",allPars);
    //w2->SetName("w");
    w2->writeToFile("ws.root",true);
    //model2->Write();
    //cout<<" pdf is "<<model2->GetPdf()->GetName()<<endl;

        
    fnew->Close();
    TFile *fnew2=new TFile(Form("matrixHistos_%s",currentDistribution.c_str()),"RECREATE");
    
    hConfusionMatrix->SetDirectory(fnew2);
    hConfusionMatrix->Write();
    fnew2->Close();
    
    //delete w2;
    return ; 
    
  }

  
  string pdfName=pdf->GetName();
  //w->import(*pdf,RecycleConflictNodes());
  //delete pdf;
  //pdf->SetName(Form("%s_PreFix",pdf->GetName()));
  //w->import(*pdf);
  //pdf=(RooSimultaneous*)w->pdf(pdfName.c_str());
  
  //model->SetPdf(*pdf);
  //model->SetWS(*w);
  ///w->import(*model->GetGlobalObservables());
  //w->import(*model->GetNuisanceParameters()); 
  
  
  allPars.add(*model->GetNuisanceParameters());
  allPars.add(*model->GetParametersOfInterest());

  //w->saveSnapshot("prefit",allPars);
  //w->writeToFile("ws.root");

  //return ; 
  ModelConfig * bModel = (ModelConfig*) model->Clone();
  // load the correct snapshot
  bModel->SetName("BModel");      
  poi->setVal(0);
  bModel->SetSnapshot( *poi  );

  string xsecVar="sigma_bin";
  if(isBBB)
    xsecVar="mu_vbf_bin_"; 

  RooArgSet minosPOi;

  std::map<string,TH1*> theoryLables;
  // theory
  vector <double> xSecTheory;
  double totXs=0;
  for(int i=0; i<(int)nBinsDiff[currentDistribution]; i++){
    RooRealVar *xx=(RooRealVar*)w->var(Form("%s%d",xsecVar.c_str(),i));
    minosPOi.add(*xx);
    double binWidth=binEdges[i+1]-binEdges[i];
    xSecTheory.push_back(xx->getVal()/binWidth);
    totXs+=xx->getVal()/binWidth;
  }
  
  TH1D *hvbfNLO=nullptr; 
  if(addOtherTheory){
    TFile *ftheorO=TFile::Open("VBFNLO_NLO_DYll.root");
    
    TH1D *hvbfNLO=(TH1D*)ftheorO->Get(currentDistribution.c_str());
    hvbfNLO->SetDirectory(0);
    hvbfNLO->SetFillColor(kBlue);
    hvbfNLO->SetLineColor(kBlue);
    hvbfNLO->SetFillStyle(3144);
    if(normTheoryPredtoYR4)
      hvbfNLO->Scale(totXs/hvbfNLO->Integral());

    for(int i=1; i<(int)hvbfNLO->GetNbinsX()+1; i++){
      hvbfNLO->SetBinError(i,UnitsScaling["fb"]*hvbfNLO->GetBinError(i));
      hvbfNLO->SetBinContent(i,UnitsScaling["fb"]*hvbfNLO->GetBinContent(i));
    }
    if(normTheoryPredtoYR4)
      theoryLables["VBFNLO@NLO  (YR4)"]=hvbfNLO;
    else 
      theoryLables["VBFNLO@NLO,Fixed order"]=hvbfNLO;
    ftheorO->Close();

    TFile *ftheor1=TFile::Open("MGHw7_DYll_noTheo.root");
    
    TH1D *hvbfMG=(TH1D*)ftheor1->Get(currentDistribution.c_str());
    hvbfMG->SetDirectory(0);
    hvbfMG->SetFillColor(kMagenta);
    hvbfMG->SetLineColor(kMagenta);
    hvbfMG->SetFillStyle(3140);
    hvbfMG->SetStats(0);
    hvbfMG->SetMarkerStyle(28);
    hvbfMG->SetMarkerColor(28); 
    if(normTheoryPredtoYR4)
      hvbfMG->Scale(totXs/hvbfMG->Integral());

    for(int i=1; i<(int)hvbfNLO->GetNbinsX()+1; i++){
      hvbfMG->SetBinError(i,UnitsScaling["fb"]*hvbfMG->GetBinError(i));
      hvbfMG->SetBinContent(i,UnitsScaling["fb"]*hvbfMG->GetBinContent(i));
    }
    if(normTheoryPredtoYR4)
      theoryLables["MG_aMC@NLO  (YR4)"]=hvbfMG;
    else 
      theoryLables["MG_aMC@NLO"]=hvbfMG;
    
    ftheor1->Close(); 
  }
  




  RooRealVar *tau=new RooRealVar("tau","#lambda",tauD,-10,+10);
  tau->setVal(tauD);
  tau->setConstant(true);
  RooArgList *argsFormula=new RooArgList();
  //string formulaSum="TMath::Exp(tau *(";
  string formulaSum="(tau *(";
  argsFormula->add(*tau);

  int nPois=isBBB ? 0:nBinsDiff[currentDistribution];
  vector <RooRealVar*> *poisV=new vector <RooRealVar*>();
  TIter tp=(model->GetParametersOfInterest())->createIterator();
  RooRealVar *rtmp=nullptr;
  int lp=0;
  while((rtmp=(RooRealVar*)tp.Next())){
    if( (string(rtmp->GetName())).find("sigma_bin")!=string::npos || TString(rtmp->GetName()).Contains("mu_bin")){
      poisV->push_back(rtmp);
      lp++;
    }
  }
  
  
  
  for(int k=1;k<nPois-1;k++){

    /*
    formulaSum+=Form("((%s/%.3lf-%s/%.3lf) - (%s/%.3lf-%s/%.3lf)*(%s/%.3lf-%s/%.3lf))",
                     poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal());
    */

    /*
    formulaSum+=Form("((-%s/%.3lf + 2*%s/%.3lf - %s/%.3lf)*(-%s/%.3lf + 2*%s/%.3lf -%s/%.3lf))",
                     poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal(),
                     poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal());
                     if(k<nPois-2) formulaSum+=" + ";
                     argsFormula->add(*poisV->at(k+1));
    argsFormula->add(*poisV->at(k));
    argsFormula->add(*poisV->at(k-1));
    argsFormula->add(*poisV->at(k+1));
    argsFormula->add(*poisV->at(k));
    argsFormula->add(*poisV->at(k-1));
    */

    
    formulaSum+=Form("TMath::Power( (%s/%.3lf -%s/%.3lf) - (%s/%.3lf -%s/%.3lf),2) ",
                     poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal());
    
    if(k<nPois-2) formulaSum+=" + ";
    
    argsFormula->add(*poisV->at(k+1));
    argsFormula->add(*poisV->at(k));
    argsFormula->add(*poisV->at(k));
    argsFormula->add(*poisV->at(k-1));
    
  } 
  formulaSum+="))";
  
  
  
  RooFormulaVar *regSum=new RooFormulaVar("regSum","regSum",formulaSum.c_str(),*argsFormula);
  regSum->Print();
  RooGenericPdf *regPdf= new RooGenericPdf("regPdf","regPdf","@0*@1",RooArgList(*pdf,*regSum));
  cout<<"Print pdf "<<endl;
  pdf->Print("v");

  
  
  // First Stat+Sys fit 
  
  RooAbsReal *mNll= pdf->createNLL( *data,
                                    //RooFit::Constrain(*model->GetNuisanceParameters()), 
                                    //RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                    //RooFit::Offset(kTRUE), 
                                    //                                    Extended(true),
                                    RooFit::Verbose(kFALSE));

  RooAddition *NllMod=new RooAddition("nllSum","nullSum",RooArgList(*mNll,*regSum));

  
  RooMinimizer *msys= new RooMinimizer(doReg ? *NllMod:*mNll);;
  msys->setEps(0.05);
  msys->optimizeConst(2);
  msys->setStrategy(2);
  msys->minimize("Minuit2");
  
  msys->minos(*poi);
  //msys->minos(minosPOi);
  w->saveSnapshot("postfit",allPars);
  model->SetSnapshot(*poi);
  RooFitResult *rnom=msys->save();
  double minNll = rnom->minNll();
  cout<<"MinNLL for nominal "<<minNll<<endl;
  //return ;
  vector <double> xSec;
  vector <double> xSecUp;
  vector <double> xSecDown;
  vector <double> xbins;
  for( int i=0; i<(int) nBinsDiff[currentDistribution]; i++){
    RooRealVar *xP=(RooRealVar*)w->var(Form("%s%d",xsecVar.c_str(),i));
    double binWidth=binEdges[i+1]-binEdges[i];

    xSec.push_back(UnitsScaling["fb"]*xP->getVal()/binWidth);
    xSecUp.push_back(UnitsScaling["fb"]*fabs(xP->getErrorHi()/binWidth));
    xSecDown.push_back(UnitsScaling["fb"]*fabs(xP->getErrorLo()/binWidth));
    xbins.push_back(binEdges[i]+0.5*binWidth);
  }

  // Get the correlation matrix for floating non nuisance paramters
  RooArgSet corrPars;
  corrPars.add(minosPOi);
  RooAbsCollection *collection=w->allVars().selectByName("mu_*");
  corrPars.add(*collection);

  RooArgSet corrParsX;
  corrParsX.add(corrPars);
  
  TIterator *corrIterX=corrParsX.createIterator();
  TIterator *corrIterY=corrPars.createIterator();
  RooRealVar *corrX=nullptr;
  RooRealVar *corrY=nullptr;
  vector <vector <double>> doubleXY;
  int corrSize=0;
  while (( corrX=(RooRealVar*)corrIterX->Next())){
    if( corrX==nullptr) continue;
    if( corrX->isConstant()) continue;
    vector <double> ycors;
    while (( corrY=(RooRealVar*)corrIterY->Next())){
      if( corrY==nullptr || corrX==nullptr) continue;
      if( corrY->isConstant() || corrX->isConstant()) continue;
      ycors.push_back(rnom->correlation(*corrY,*corrX));
    }
    corrIterY->Reset();
    doubleXY.push_back(ycors);
    corrSize++;
  }
  corrIterX->Reset();
  corrIterY->Reset();
  
  TH2F *hcorr=new TH2F("hcorr","",corrSize,0,corrSize,corrSize,0,corrSize);
  int icorrx=1;
  int icorry=1;

  while (( corrX=(RooRealVar*)corrIterX->Next())){
    if( corrX==nullptr) continue;
    if( corrX->isConstant()) continue;
    icorry=1;

    while (( corrY=(RooRealVar*)corrIterY->Next())){
      if( corrY==nullptr || corrX==nullptr) continue;
      if( corrY->isConstant() || corrX->isConstant()) continue;

      hcorr->SetBinContent(icorrx,icorry,rnom->correlation(*corrY,*corrX));

      hcorr->GetYaxis()->SetBinLabel(icorry,corrY->GetName());
      hcorr->GetXaxis()->SetBinLabel(icorrx,corrX->GetName());
      icorry++;
    }
    corrIterY->Reset();
    icorrx++;
  }
  hcorr->SetStats(0);
  hcorr->GetZaxis()->SetRangeUser(-1,+1);
  hcorr->GetYaxis()->SetTitleOffset(1.2);
  hcorr->GetYaxis()->SetTitleOffset(0.9);
  
  gStyle->SetPaintTextFormat("4.1f");
  string regType="matrix";
  if(doReg) regType+="_reg";
  TCanvas *cCorr=new TCanvas( Form("cCorr_%s_%s", string(isBBB ? "bbb" : regType).c_str(), currentDistribution.c_str()),"");{
    cCorr->cd();
    hcorr->Draw("colz text");
  }

  cCorr->Print(Form("%s.eps",cCorr->GetName()),"eps");

  // Draw the confusion matrix
  TFile *fmatrices=TFile::Open(Form("matrixHistos_%s",currentDistribution.c_str()));
  hConfusionMatrix = (TH2F*)fmatrices->Get("hConfusionMatrix");
  TCanvas *cConfMatrix=nullptr;
  if( hConfusionMatrix!=nullptr && !isBBB) {
    //SetDef(hConfusionMatrix);
    hConfusionMatrix->GetZaxis()->SetRangeUser(-1,+1);
    hConfusionMatrix->GetYaxis()->SetTitleOffset(1.2);
    hConfusionMatrix->GetYaxis()->SetTitleOffset(0.9);
    
    cConfMatrix=new TCanvas(Form("cConfMatrix_%s",currentDistribution.c_str()),"");{
      cConfMatrix->cd();
      cConfMatrix->SetBottomMargin(0.1);
      cConfMatrix->SetTopMargin(0.1);
      hConfusionMatrix->Draw("colz text2");
    }
    cConfMatrix->Print(Form("%s.eps",cConfMatrix->GetName()),"eps");
  }
  
  
  

  // Stat Only fit
  w->allVars().selectByName("alpha_*")->setAttribAll("Constant",true);

  RooAbsReal *mNllStat= pdf->createNLL( *data,
                                        //RooFit::Constrain(*model->GetNuisanceParameters()), 
                                        //RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                        //                                        RooFit::Offset(kTRUE), 
                                        //                                        Extended(true),
                                        RooFit::Verbose(kFALSE));

  RooAddition *NllModStat=new RooAddition("nllSum","nullSum",RooArgList(*mNllStat,*regSum));
  
  RooMinimizer *msysStat= new RooMinimizer(doReg ? *NllModStat:*mNllStat);;
  msysStat->setEps(0.1);
  msysStat->optimizeConst(2);
  msysStat->setStrategy(2);
  msysStat->minimize("Minuit2");
  
  msysStat->minos(*poi);
  msysStat->minos(minosPOi);
  w->saveSnapshot("postfitStat",allPars);
  //model->SetSnapshot(*poi);
    RooFitResult *rnomStat=msys->save();
  double minNllStat = rnomStat->minNll();
  cout<<"MinNLL for nominal stat only "<<minNllStat<<endl;

  // Stat Only fit
  w->allVars().selectByName("alpha_*")->setAttribAll("Constant",false);


 
  
  
  vector <double> xSecStat;
  vector <double> xSecUpStat;
  vector <double> xSecDownStat;

  

  RooRealVar *xPStat=nullptr;
  for( int i=0; i<(int) nBinsDiff[currentDistribution]; i++){
    RooRealVar *xP=(RooRealVar*)w->var(Form("%s%d",xsecVar.c_str(),i));
      double binWidth=binEdges[i+1]-binEdges[i];
      xSecStat.push_back(xP->getVal()/binWidth*UnitsScaling["fb"]);
      xSecUpStat.push_back(UnitsScaling["fb"]*fabs(xP->getErrorHi()/binWidth));
      xSecDownStat.push_back(UnitsScaling["fb"]*fabs(xP->getErrorLo()/binWidth));
      cout<<" bin "<<xSecStat.back()<<" "<<xSecUpStat.back()<<" bin "<<xbins.at(i)<<endl;

     

  }
    
    TGraphAsymmErrors *statPsys=new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSec[0],0,0,&xSecUp[0],&xSecDown[0]);
    statPsys->SetMarkerStyle(8);

    TGraphAsymmErrors *statPstat=new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSecStat[0],0,0,&xSecUpStat[0],&xSecDownStat[0]);
    statPstat->SetMarkerStyle(8);

    TGraphAsymmErrors *theory1=new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSecTheory[0],0,0,0,0);
    theory1->SetLineColor(kRed);
    theory1->SetLineWidth(2);
    

    TH1F *htheory1=new TH1F("htheory1","",nBinsDiff[currentDistribution],&binEdges[0]);
    SetDef(htheory1);
    htheory1->SetLineColor(kRed);
    htheory1->SetLineWidth(2);
    htheory1->SetMarkerStyle(25);
    for( int i=0; i<(int)nBinsDiff[currentDistribution]; i++)
      htheory1->SetBinContent(i+1,UnitsScaling["fb"]*xSecTheory[i]);


    vector <double> xSecRatioData;
    vector <double> xSecRatioDataUp;
    vector <double> xSecRatioDataDown;
    
    
    vector <double> xSecRatioth1;
    vector <double> xSecRatioth1Up;
    vector <double> xSecRatioth1Down;
    
    vector <double> xSecRatioth2;
    vector <double> xSecRatioth2Up;
    vector <double> xSecRatioth2Down;
    
    
    vector <double> xSecRatioth3;
    vector <double> xSecRatioth3Up;
    vector <double> xSecRatioth3Down;
    

  // Fill the theory comparisons
  for ( int i=0; i<(int)xSec.size(); i++){
    
    xSecRatioData.push_back(1.0);
    xSecRatioDataUp.push_back(xSecUp[i]/xSec[i]);
    xSecRatioDataDown.push_back(xSecDown[i]/xSec[i]);
    
    xSecRatioth1.push_back( htheory1->GetBinContent(i+1)  /xSec[i]);
    xSecRatioth1Up.push_back( xSecRatioth1.back() * htheory1->GetBinError(i+1) / htheory1->GetBinContent(i+1));
    xSecRatioth1Down.push_back(xSecRatioth1.back() *  htheory1->GetBinError(i+1) / htheory1->GetBinContent(i+1));

    TH1F *htheory2=(TH1F*) (normTheoryPredtoYR4 ? theoryLables["VBFNLO@NLO  (YR4)"] :   theoryLables["VBFNLO@NLO,Fixed order"]);
    TH1F *htheory3=(TH1F*) (normTheoryPredtoYR4? theoryLables["MG_aMC@NLO  (YR4)"] : theoryLables["MG_aMC@NLO"]);
    
    if(normTheoryPredtoYR4){
       xSecRatioth2.push_back(theoryLables["VBFNLO@NLO  (YR4)"]->GetBinContent(i+1) / xSec[i]);
       xSecRatioth3.push_back(theoryLables["MG_aMC@NLO  (YR4)"]->GetBinContent(i+1) / xSec[i]);
       
    }
    else{
       xSecRatioth2.push_back(1/xSec.at(i)/theoryLables["VBFNLO@NLO,Fixed order"]->GetBinContent(i+1));
       xSecRatioth3.push_back(1/xSec.at(i)/theoryLables["MG_aMC@NLO"]->GetBinContent(i+1));
    }

    xSecRatioth2Up.push_back(xSecRatioth2.back() * htheory2->GetBinError(i+1) / htheory2->GetBinContent(i+1));
    xSecRatioth2Down.push_back(xSecRatioth2.back() * htheory2->GetBinError(i+1) / htheory2->GetBinContent(i+1));

    xSecRatioth3Up.push_back(xSecRatioth3.back() * htheory3->GetBinError(i+1) / htheory3->GetBinContent(i+1));
    xSecRatioth3Down.push_back(xSecRatioth3.back() * htheory3->GetBinError(i+1) / htheory3->GetBinContent(i+1));

    cout<<" MG "<<xSecRatioth3.back()<<endl;
  }


  TGraphAsymmErrors *gCompData= new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSecRatioData[0],0,0,&xSecRatioDataUp[0],&xSecRatioDataDown[0]);
  gCompData->SetMarkerStyle(8);
  gCompData->SetMarkerColor(kBlack);
  gCompData->SetLineColor(kBlack);
  gCompData->SetFillStyle(3144);
  gCompData->SetLineWidth(2);
  

  TGraphAsymmErrors *gComp1= new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSecRatioth1[0],0,0,&xSecRatioth1Up[0],&xSecRatioth1Down[0]);
  gComp1->SetMarkerStyle(htheory1->GetMarkerStyle());
  gComp1->SetMarkerColor(htheory1->GetMarkerColor());
  gComp1->SetLineColor(htheory1->GetLineColor());
  gComp1->SetLineWidth(2);
  
  TGraphAsymmErrors *gComp2= new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSecRatioth2[0],0,0,&xSecRatioth2Up[0],&xSecRatioth2Down[0]);
  TGraphAsymmErrors *gComp3= new TGraphAsymmErrors((int)xbins.size(),&xbins[0],&xSecRatioth3[0],0,0,&xSecRatioth3Up[0],&xSecRatioth3Down[0]);

  if(normTheoryPredtoYR4){
    gComp2->SetMarkerStyle(theoryLables["VBFNLO@NLO  (YR4)"]->GetMarkerStyle());
    gComp2->SetMarkerColor(theoryLables["VBFNLO@NLO  (YR4)"]->GetMarkerColor());
    gComp2->SetLineColor(theoryLables["VBFNLO@NLO  (YR4)"]->GetLineColor());
    gComp2->SetFillStyle(theoryLables["VBFNLO@NLO  (YR4)"]->GetFillStyle());
    gComp2->SetFillColor(theoryLables["VBFNLO@NLO  (YR4)"]->GetFillColor());
    gComp2->SetLineWidth(2);


    gComp3->SetMarkerStyle(theoryLables["MG_aMC@NLO  (YR4)"]->GetMarkerStyle());
    gComp3->SetMarkerColor(theoryLables["MG_aMC@NLO  (YR4)"]->GetMarkerColor());
    gComp3->SetLineColor(theoryLables["MG_aMC@NLO  (YR4)"]->GetLineColor());
    gComp3->SetFillStyle(theoryLables["MG_aMC@NLO  (YR4)"]->GetFillStyle());
    gComp3->SetFillColor(theoryLables["MG_aMC@NLO  (YR4)"]->GetFillColor());
    gComp3->SetLineWidth(2);
  }
  else {
    gComp2->SetMarkerStyle(theoryLables["VBFNLO@NLO,Fixed order"]->GetMarkerStyle());
    gComp2->SetMarkerColor(theoryLables["VBFNLO@NLO,Fixed order"]->GetMarkerColor());
    gComp2->SetLineColor(theoryLables["VBFNLO@NLO,Fixed order"]->GetLineColor());
    gComp2->SetFillStyle(theoryLables["VBFNLO@NLO,Fixed order"]->GetFillStyle());
    gComp2->SetFillColor(theoryLables["VBFNLO@NLO,Fixed order"]->GetFillColor());
    gComp2->SetLineWidth(2);


    gComp3->SetMarkerStyle(theoryLables["MG_aMC@NLO"]->GetMarkerStyle());
    gComp3->SetMarkerColor(theoryLables["MG_aMC@NLO"]->GetMarkerColor());
    gComp3->SetLineColor(theoryLables["MG_aMC@NLO"]->GetLineColor());
    gComp3->SetFillStyle(theoryLables["MG_aMC@NLO"]->GetFillStyle());
    gComp3->SetFillColor(theoryLables["MG_aMC@NLO"]->GetFillColor());
    gComp3->SetLineWidth(2);
    
    
  }

    // Make a nice plot
    TH1F *hDummy=new TH1F("hDummy","",nBinsDiff[currentDistribution],&binEdges[0]);
    SetDef(hDummy);

    hDummy->GetYaxis()->SetRangeUser(-0.1,5);
    hDummy->GetXaxis()->SetTitle(Form("%s [GeV]",labelMap[currentDistribution].c_str()));
    hDummy->GetYaxis()->SetTitle(Form("d#it{#sigma} / d%s [%s]",labelMap[currentDistribution].c_str(),unitsMap[currentDistribution].c_str()));
    hDummy->GetYaxis()->SetTitleOffset(1.4);
    hDummy->SetLineColor(kWhite);
    hDummy->SetFillColor(kWhite);

    TH1F *hDummyRatio=(TH1F*)hDummy->Clone("hDummyRatio");
    hDummyRatio->GetYaxis()->SetTitle("Pred / Data");
    //hDummyRatio->GetYaxis()->SetRangeUser(-0.24,2.24);
    hDummyRatio->GetYaxis()->SetRangeUser(0.354,1.756);
    hDummyRatio->GetYaxis()->CenterTitle(true);
    hDummyRatio->GetXaxis()->SetTitleOffset(3.0);
    hDummyRatio->GetYaxis()->SetTitleOffset(1.4);

    
    TLegend *leg=DefLeg(0.5,0.6,0.89,0.9);
    leg->AddEntry(statPsys,"Data stat#oplussys","PE");
    leg->AddEntry(statPstat,"Data stat only","E");
    leg->AddEntry(htheory1,"Powheg+Pythia8 (YR4)","PLE");
    for(std::map<string,TH1*>::iterator it=theoryLables.begin(); it!=theoryLables.end(); it++)
      leg->AddEntry((*it).second,(*it).first.c_str(),"PFLE");

    TLatex *lat=new TLatex();
    lat->SetTextSize(22);
    lat->SetTextFont(43);

    
    TPad*    stack3 =nullptr;
    TPad*    comparison =nullptr;
    
    double canX=800;
    double canY=800;
    

    TCanvas *cDiff=new TCanvas(Form("cDiff_%s_%s",string(isBBB ? "bbb" : regType).c_str(),currentDistribution.c_str()),"",canX,canY);{
      cDiff->cd();
      SetCanvasDefaults(cDiff);
      
      cDiff->SetBottomMargin(0);
      
      stack3= new TPad(Form("stack3_%s",currentDistribution.c_str()), "", 0, 0.4, 1, 1);   
      comparison= new TPad(Form("comp3_%s",currentDistribution.c_str()), "", 0,0, 1, 0.4);
      
      stack3->SetRightMargin(0.02);
      stack3->SetLeftMargin(0.16);
      stack3->SetTopMargin(0.02);
      stack3->SetBottomMargin(0.001);
      
      comparison->SetRightMargin(0.02);
      comparison->SetTopMargin(0);
      comparison->SetBottomMargin(0.45);
      comparison->SetLeftMargin(0.16);
      
      stack3->SetTicks();
      comparison->SetTicks();
      
      cDiff->cd();
      stack3->Draw();
      comparison->Draw();   
      
      stack3->cd();
      
      
      hDummy->Draw("hist");
      for(std::map<string,TH1*>::iterator it=theoryLables.begin(); it!=theoryLables.end(); it++)
        (*it).second->Draw("PE2 same");
      
      htheory1->Draw("hist same ");
      
      
      statPstat->Draw("[] same");
      statPsys->Draw("PE");
      leg->Draw();
      lat->DrawLatex(binEdges[0]+0.4*binEdges[0],4.5,"#it{#bf{ATLAS}} Internal");
      lat->DrawLatex(binEdges[0]+0.4*binEdges[0],4.5*0.92,Form("#scale[1.0]{ #sqrt{#it{s}} = 13 TeV, %.1f fb^{-1}}",139.1));
      if(isBBB)
        lat->DrawLatex(binEdges[0]+0.4*binEdges[0],4.5*0.8,"bin by bin");
      else
        if (doReg)
          lat->DrawLatex(binEdges[0]+0.4*binEdges[0],4.5*0.8,Form("reg. matrix #tau=%.2lf",tauD));
        else 
          lat->DrawLatex(binEdges[0]+0.4*binEdges[0],4.5*0.8,"unreg. likelihood matrix.");
      
      ///ATLASLabel(lat,binEdges[0]+0.1*binEdges[0],4.5,false,2,139.0);
      comparison->cd();
      hDummyRatio->Draw("hist");
      gCompData->Draw("e same");
      gComp1->Draw("pe same");
      gComp2->Draw("pe same");
      gComp3->Draw("pe same");
    }

    cDiff->Print(Form("%s.eps",cDiff->GetName()),"eps");

  


  
  
  if(doRanking) {
    RooAbsData *dataPostFit=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
    
    // make a ranking
    std::map<string, std::map<string,std::pair<double,double>>> ParResults_up;
    std::map<string, std::map<string,std::pair<double,double>>> ParResults_down;
    
    std::map<string, std::map<string,std::pair<double,double>>> ParResults_upPull;
    std::map<string, std::map<string,std::pair<double,double>>> ParResults_downPull;
    
    std::map<string, std::map<string,std::pair<double,double>>> ParResults_FixedZeroPrefit;
    std::map<string, std::map<string,std::pair<double,double>>> ParResults_FixedZeroPostfit;
    
    
    TIter itPars_=allPars.createIterator();
    RooRealVar *tmpAllPars_=nullptr;
    
    std::map<string,std::pair<double,double>> fitResultsNom;
    
    while( (tmpAllPars_=(RooRealVar*)itPars_.Next())){
      fitResultsNom[tmpAllPars_->GetName()]=make_pair(tmpAllPars_->getVal(),tmpAllPars_->getError());
    }
    
    ParResults_up["nominal"]=fitResultsNom;
    ParResults_down["nominal"]=fitResultsNom;
    
    ParResults_upPull["nominal"]=fitResultsNom;
    ParResults_downPull["nominal"]=fitResultsNom;
    
    ParResults_FixedZeroPrefit["nominal"]=fitResultsNom;
    ParResults_FixedZeroPostfit["nominal"]=fitResultsNom;
    
    
    
    RooArgSet *nuisanceParameters=(RooArgSet*)model->GetNuisanceParameters();
  TIter itNPs=nuisanceParameters->createIterator();
  RooRealVar *tmpVNPShift=nullptr;
  int pnCount=0;
  while( (tmpVNPShift=(RooRealVar*)itNPs.Next())){
    
    w->loadSnapshot("prefit");
    double shift[2]={0,0};

    // first do the prefit ranking 
    if( TString(tmpVNPShift->GetName()).Contains("alpha_")) { shift[0]=tmpVNPShift->getVal()-1; shift[1]=tmpVNPShift->getVal()+1;} 
    else { shift[0]=tmpVNPShift->getVal()-tmpVNPShift->getError(); shift[1]=tmpVNPShift->getVal()+tmpVNPShift->getError();}


    std::map<string,std::pair<double,double>> fitResultsDownPull;
    std::map<string,std::pair<double,double>> fitResultsUpPull;
    std::map<string,std::pair<double,double>> fitResultsBreakDownPrefit;
    
    
    for(int i=0; i<2; i++){

      tmpVNPShift->setVal(shift[i]);
      tmpVNPShift->setConstant(true);
      RooAbsReal *mNlltmp= pdf->createNLL( *data,
                                           RooFit::Constrain(*model->GetNuisanceParameters()), 
                                           RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                           RooFit::Offset(kTRUE), 
                                           Extended(true),
                                           RooFit::Verbose(kFALSE));
      
      RooMinimizer *msystmp= new RooMinimizer(*mNlltmp);;
      msystmp->setEps(0.01);
      msystmp->optimizeConst(2);
      msystmp->setStrategy(1);
      msystmp->minimize("Minuit2");

      TIter itPars=allPars.createIterator();
      RooRealVar *tmpAllPars=nullptr;
      while( (tmpAllPars=(RooRealVar*)itPars.Next())){
        if(i==0)
          fitResultsDownPull[tmpAllPars->GetName()]=make_pair(tmpAllPars->getVal(),tmpAllPars->getError());
        else 
          fitResultsUpPull[tmpAllPars->GetName()]=make_pair(tmpAllPars->getVal(),tmpAllPars->getError());
      }
      tmpVNPShift->setConstant(false);
      w->loadSnapshot("prefit");
      delete msystmp;
      delete mNlltmp;
    }

    ParResults_upPull[tmpVNPShift->GetName()]=fitResultsUpPull;
    ParResults_downPull[tmpVNPShift->GetName()]=fitResultsDownPull;
    
    // here make a fit with fixing the np
    w->loadSnapshot("prefit");
    tmpVNPShift->setConstant(true);
    // make the fit
    RooAbsReal *mNlltmp2= pdf->createNLL( *data,
                                          RooFit::Constrain(*model->GetNuisanceParameters()), 
                                          RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                          RooFit::Offset(kTRUE), 
                                          Extended(true),
                                          RooFit::Verbose(kFALSE));
      
    RooMinimizer *msystmp2= new RooMinimizer(*mNlltmp2);;
    msystmp2->setEps(0.01);
    msystmp2->optimizeConst(2);
    msystmp2->setStrategy(1);
    msystmp2->minimize("Minuit2");
    
    TIter itPars2=allPars.createIterator();
    RooRealVar *tmpAllPars2=nullptr;
      while( (tmpAllPars2=(RooRealVar*)itPars2.Next())){
        fitResultsBreakDownPrefit[tmpAllPars2->GetName()]=make_pair(tmpAllPars2->getVal(),tmpAllPars2->getError());
      }
      tmpVNPShift->setConstant(false);
      delete msystmp2;
      delete mNlltmp2;
      
      ParResults_FixedZeroPrefit[tmpVNPShift->GetName()]=fitResultsBreakDownPrefit;
      
      w->loadSnapshot("prefit");
    
      // here make the postfit 
      std::map<string,std::pair<double,double>> fitResultsDown;
      std::map<string,std::pair<double,double>> fitResultsUp;
      std::map<string,std::pair<double,double>> fitResultsBreakDownPostfit;
      w->loadSnapshot("postfit");
      
      // first do the prefit ranking 
      if( TString(tmpVNPShift->GetName()).Contains("alpha_")) { shift[0]=tmpVNPShift->getVal()-1; shift[1]=tmpVNPShift->getVal()+1;} 
      else { shift[0]=tmpVNPShift->getVal()-tmpVNPShift->getError(); shift[1]=tmpVNPShift->getVal()+tmpVNPShift->getError();}
      
    for(int i=0; i<2; i++){
      
      tmpVNPShift->setVal(shift[i]);
      tmpVNPShift->setConstant(true);
      
      //RooAbsData *datatmp=AsymptoticCalculator::GenerateAsimovData(*pdf,*model->GetObservables());
      
      RooAbsReal *mNlltmp= pdf->createNLL( *dataPostFit,
                                           RooFit::Constrain(*model->GetNuisanceParameters()), 
                                           RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                           RooFit::Offset(kTRUE), 
                                           Extended(true),
                                           RooFit::Verbose(kFALSE));
      
      RooMinimizer *msystmp= new RooMinimizer(*mNlltmp);;
      msystmp->setEps(0.01);
      msystmp->optimizeConst(2);
      msystmp->setStrategy(1);
      msystmp->minimize("Minuit2");

      TIter itPars=allPars.createIterator();
      RooRealVar *tmpAllPars=nullptr;
      while( (tmpAllPars=(RooRealVar*)itPars.Next())){
        if(i==0)
          fitResultsDown[tmpAllPars->GetName()]=make_pair(tmpAllPars->getVal(),tmpAllPars->getError());
        else 
          fitResultsUp[tmpAllPars->GetName()]=make_pair(tmpAllPars->getVal(),tmpAllPars->getError());
      }
      delete msystmp;
      delete mNlltmp;
      //delete datatmp;
      tmpVNPShift->setConstant(false);
      w->loadSnapshot("postfit");
    }

    ParResults_up[tmpVNPShift->GetName()]=fitResultsUp;
    ParResults_down[tmpVNPShift->GetName()]=fitResultsDown;


    w->loadSnapshot("postfit");
    tmpVNPShift->setConstant(true);
    // make the fit
    RooAbsReal *mNlltmp4= pdf->createNLL( *dataPostFit,
                                          RooFit::Constrain(*model->GetNuisanceParameters()), 
                                          RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                          RooFit::Offset(kTRUE), 
                                          Extended(true),
                                          RooFit::Verbose(kFALSE));
      
    RooMinimizer *msystmp4= new RooMinimizer(*mNlltmp4);;
    msystmp4->setEps(0.01);
    msystmp4->optimizeConst(2);
    msystmp4->setStrategy(1);
    msystmp4->minimize("Minuit2");
    
    TIter itPars3=allPars.createIterator();
    RooRealVar *tmpAllPars3=nullptr;
      while( (tmpAllPars3=(RooRealVar*)itPars3.Next())){
        fitResultsBreakDownPostfit[tmpAllPars3->GetName()]=make_pair(tmpAllPars3->getVal(),tmpAllPars3->getError());
      }

      
      delete msystmp4;
      delete mNlltmp4;
      ParResults_FixedZeroPostfit[tmpVNPShift->GetName()]=fitResultsBreakDownPostfit;

      tmpVNPShift->setConstant(false);
      w->loadSnapshot("prefit");

      //if(pnCount > 5) break;
      pnCount++;
  }


  // Order the results
  vector <string> rankingOrder;
  for( auto & parIter : ParResults_upPull )
    rankingOrder.push_back(parIter.first);
  std::sort(rankingOrder.begin(), rankingOrder.end(), [&ParResults_upPull,&ParResults_downPull](string a,string b){
      double up_a=( (ParResults_upPull[a])["mu_vbf"].first - (ParResults_upPull["nominal"])["mu_vbf"].first)/((ParResults_upPull["nominal"])["mu_vbf"].first);
      double down_a= ( (ParResults_downPull[a])["mu_vbf"].first - (ParResults_downPull["nominal"])["mu_vbf"].first)/((ParResults_downPull["nominal"])["mu_vbf"].first);
      double err_a=0.5*(fabs(up_a)+fabs(down_a));

      double up_b=( (ParResults_upPull[b])["mu_vbf"].first - (ParResults_upPull["nominal"])["mu_vbf"].first)/((ParResults_upPull["nominal"])["mu_vbf"].first);
      double down_b= ( (ParResults_downPull[b])["mu_vbf"].first - (ParResults_downPull["nominal"])["mu_vbf"].first)/((ParResults_downPull["nominal"])["mu_vbf"].first);
      double err_b=0.5*(fabs(up_b)+fabs(down_b));

      return a < b ; 
    });
  
  
  // order with respect to the postfit ranking
  //for( auto & parIter : ParResults_up ){
  //}

  // print the ranking
  cout<<endl;
  cout<<endl;
  cout<<"Variation & POI sigma up [%]& POI sigma down [%] NP val & NP err & breakdown uncertainty" <<endl;
  //for( auto & parIter : ParResults_upPull ){
  //string variation=parIter.first;
  for(vector<string>::iterator it=rankingOrder.begin(); it!=rankingOrder.end(); it++){
    string variation=(*it);
    cout<<variation<<" & "<<
      ( (ParResults_upPull[variation])["mu_vbf"].first - (ParResults_upPull["nominal"])["mu_vbf"].first)/((ParResults_upPull["nominal"])["mu_vbf"].first)*100 <<" & "<<
      ( (ParResults_downPull[variation])["mu_vbf"].first - (ParResults_downPull["nominal"])["mu_vbf"].first)/((ParResults_downPull["nominal"])["mu_vbf"].first)*100
        <<" & "<<(ParResults_upPull["nominal"])[variation].first<<" & +/- "<<(ParResults_upPull["nominal"])[variation].second<<" & "<<
      sqrt( pow(ParResults_FixedZeroPrefit["nominal"]["mu_vbf"].second,2) - pow(ParResults_FixedZeroPrefit[variation]["mu_vbf"].second,2))<<endl;
  }

  cout<<endl;
  cout<<endl;

  // print the ranking
  cout<<"Postfit "<<endl;
  cout<<"Variation & POI sigma up [%]& POI sigma down [%] NP val & NP err & breakdown "<<endl;
  //for( auto & parIter : ParResults_up ){
  //string variation=parIter.first;
 for(vector<string>::iterator it=rankingOrder.begin(); it!=rankingOrder.end(); it++){
    string variation=(*it);
    cout<<variation<<" & "<<
      ( (ParResults_up[variation])["mu_vbf"].first - (ParResults_up["nominal"])["mu_vbf"].first)/((ParResults_up["nominal"])["mu_vbf"].first)*100 <<"   &   " << 
      ( (ParResults_down[variation])["mu_vbf"].first - (ParResults_down["nominal"])["mu_vbf"].first)/((ParResults_down["nominal"])["mu_vbf"].first)*100<<"  &  "
        <<" & "<<(ParResults_upPull["nominal"])[variation].first<<" & +/- "<<(ParResults_upPull["nominal"])[variation].second<<" & "<<
      sqrt( pow(ParResults_FixedZeroPostfit["nominal"]["mu_vbf"].second,2) - pow(ParResults_FixedZeroPostfit[variation]["mu_vbf"].second,2))<<endl;
    
 }

  
  

  /*
  // create output tree
  TFile *ffitResults=new new TFile("ffitResults","RECREATE");
  TTree *t=new TTree("fit","fits");
  vector <TString*> 
  t->Branch(
  */
 //return ;
  }
  
  if( doSignificance){
    // Get the significance
    //return ;
    w->loadSnapshot("postfit");
    //w->loadSnapshot("prefit");
    // create the AsymptoticCalculator from data,alt model, null model
    //AsymptoticCalculator  ac(*data, *model, *bModel);
    //ac.SetOneSidedDiscovery(false);  // for one-side discovery test
    //ac.SetPrintLevel(-1);  // to suppress print level 

    // Make a manual fit;

    //RooAbsPdf *bPdf=bModel->GetPdf();
    poi->setVal(0.0);
    poi->setConstant(true); 
    RooAbsReal *mNllSig= pdf->createNLL( *data,
                                          RooFit::Constrain(*model->GetNuisanceParameters()), 
                                         RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                         RooFit::Offset(kTRUE), 
                                         Extended(true),
                                         RooFit::Verbose(kFALSE));
    
    RooMinimizer *msysSig= new RooMinimizer(*mNllSig);
    msysSig->setEps(0.01);
    msysSig->optimizeConst(2);
    msysSig->setStrategy(1);
    msysSig->minimize("Minuit2");
    
    // print the results
    RooFitResult *rsig=msysSig->save();
    double minNllAlt = rsig->minNll();
    cout << "NLL after minimisation for alternative hypothesis: " << setprecision(15) << minNllAlt<<endl;
    
    int ndf = 1; 
    double tmu = 2. * (minNllAlt - minNll);
    double p = TMath::Prob(tmu, ndf);
    double z = ROOT::Math::gaussian_quantile(1. - p / 2., 1);


    // run the calculator
    //HypoTestResult * asResult = ac.GetHypoTest();
    //asResult->Print();
    
    cout << "Significance: " << setprecision(15) << z << " sigma" <<endl;
    cout << "p-value: " << setprecision(15) << p << "  " <<endl;
    cout << "Significance: " << setprecision(15) << PValueToSignificance(p) << " sigma" <<endl;


    poi->setVal(1);
    poi->setConstant(false); 
    w->loadSnapshot("postfit");
  } 
  

  return ;

  RooWorkspace *w2=new RooWorkspace("w2");
  ModelConfig *altModel=(ModelConfig*)model->Clone("AltModel");
  //RooRealVar *mu_top=(RooRealVar*)w->var("mu_top");
  RooAbsPdf *altpdfmd=altModel->GetPdf();
  mu_top->SetName("mu_ww");
  RooAbsPdf *atlpdf=(RooAbsPdf*)altpdfmd->Clone("altpdfmd");
  altModel->SetPdf(*atlpdf);
  cout<<"New pdf"<<endl;
  altModel->Print("V");
  cout<<"---"<<endl;
  altModel->SetWorkspace(*w2);
  //mu_top->SetName("mu_top");
  w2->renameSet("mu_top","mu_ww");

  //msys->migrad();
  //msys->minos(*model->GetParametersOfInterest()->find("mH"));
  /*

  // set mu_top to 1.2 and create asimov

  RooRealVar *mu_top=(RooRealVar*)w->var("mu_top");
  mu_top->setVal(1.0);

  RooRealVar *mu_ww=(RooRealVar*)w->var("mu_ww");
  mu_ww->setVal(1.2);
  
  RooAbsData *dataTop=AsymptoticCalculator::MakeAsimovData(*model,*model->GetObservables(),*global);
  mu_top->SetName("mu_ww");
  */
  
  RooAbsReal *mNll2= atlpdf->createNLL( *data,
                                        RooFit::Constrain(*model->GetNuisanceParameters()), 
                                        RooFit::GlobalObservables(*model->GetGlobalObservables()),
                                        RooFit::Offset(kTRUE), 
                                        Extended(true),
                                        RooFit::Verbose(kFALSE));
  
  RooMinimizer *msys2= new RooMinimizer(*mNll2);;
  msys2->setEps(0.01);
  msys2->optimizeConst(2);
  msys2->setStrategy(1);
  msys2->minimize("Minuit2");

  return ;

  vector <double> valVars;
  valVars.push_back(0.8); valVars.push_back(0.9); valVars.push_back(1.0); valVars.push_back(1.1); valVars.push_back(1.2);

  vector <double> mu_topVars;
  vector <double> mu_wwVars;
  vector <double> mu_vbfVars;
  vector <double> mu_ZjetsVars;


  vector <double> mu_topVars_e;
  vector <double> mu_wwVars_e;
  vector <double> mu_vbfVars_e;
  vector <double> mu_ZjetsVars_e;
  
  for( int i=0; i<(int) valVars.size(); i++) {
    w->loadSnapshot("prefit");
    
    std::map<string, vector <double>> fitRes=ScanAsimovVar(w,w2,atlpdf, "mu_top",valVars[i],"mu_ww");
    
    mu_vbfVars.push_back(fitRes["mu_vbf"].at(0));
    mu_vbfVars_e.push_back(fitRes["mu_vbf"].at(1));

    mu_wwVars.push_back(fitRes["mu_ww"].at(0));
    mu_wwVars_e.push_back(fitRes["mu_ww"].at(1));
    
  }
  

  cout<<endl;
  cout<<endl;
  for( int i=0; i<(int) valVars.size(); i++) {
    cout<<"Input mu_top "<<valVars[i]<<" output mu_vbf "<<mu_vbfVars[i]<<" +/- " <<mu_vbfVars_e[i]<<endl;
  }
  

  
}

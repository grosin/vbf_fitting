#include "tools.h"

using namespace std;

vector <string> GetTreeNames(string treeName="", string prefix=""){
 vector <string> treeNames;

 if(treeName.size()==0){
   treeNames.push_back("Zjets");
   treeNames.push_back("top");
   treeNames.push_back("diboson");
   treeNames.push_back("ggf");
   treeNames.push_back("vbf");
   treeNames.push_back("Vgamma");
   treeNames.push_back("FakeE");
   treeNames.push_back("FakeM");
   treeNames.push_back("Vgamma");
   treeNames.push_back("vh");
   treeNames.push_back("data");
 }

  else {
    std::istringstream iss(treeName);  
    std::string s;                                                               
    while(getline(iss, s, ','))                                                  
      {                                                                            
        treeNames.push_back(s);                                                           
      }
  }

 // add the prefix
 if(prefix.size()>0){
   for(vector<string>::iterator s=treeNames.begin(); s!=treeNames.end(); s++){
     (*s)=prefix+(*s); 
     cout<<"Adding prefix .."<<(*s)<<endl;
   }
 }
   
 return treeNames;
  
}



void checkSys(string infile="",string var="sysVars.txt",string prefix=""){
  if (infile.size()==0 ) { cout <<"empty filename"<<endl; return; }

  TChain *chain=nullptr;

  TFile *f=nullptr;
  bool isChain=true;
  if( infile.find(".root")!=string::npos){
    cout<<"Reading sinle file";
      f=TFile::Open(infile.c_str());
    isChain=false;
  }
  vector <string> infiles;

  if( infile.find(".txt")!=string::npos){
    ifstream file2(infile.c_str());
    string line2="";
    while ( getline (file2,line2) ){
      cout<<"Adding file "<<line2<<endl;
      infiles.push_back(line2);
    }
    file2.close();
  }
  else
    infiles.push_back(infile);

  
  vector <string> treeNames=GetTreeNames("",prefix);

  std::map<string,string> regions;
  regions["SR"]="inSR==1";
  regions["Top"]="inTopCR==1";
  regions["Zjets"]="inZjetsCR==1";

  regions["CRGGF1"]="inggFCR1==1";
  regions["CRGGF2"]="inggFCR2==1";
  regions["CRGGF3"]="inggFCR3==1";

  // normalisation maps for each sample for systematics
  std::map<string,string> NormMap;
  NormMap[prefix+"vbf"]="SR";
  NormMap[prefix+"top"]="SR";
  NormMap[prefix+"diboson"]="SR";
  NormMap[prefix+"ggf"]="SR";
  

  
  // sample, region, nominal, up, down
  std::map<string, std::map<string, double>> nom;
  // systematic sample, region, up, down
  std::map<string,std::map<string, std::map<string, double>>> up;
  std::map<string,std::map<string, std::map<string, double>>> down;
  
  vector <string> variations;
  ifstream file(var.c_str());
  //file.open();
  string line="";
  
  while ( getline (file,line) ){
    cout<<"Reading sys "<<line<<endl;
    variations.push_back(line);
  }
  file.close();

  vector <string> missingTrees; 


  // loop regions
  for(std::map<string,string>::iterator r=regions.begin(); r!=regions.end(); r++){
    // loop components
    for(std::vector<string>::iterator s=treeNames.begin(); s!=treeNames.end(); s++){

      //TTree *tnom =(TTree*)f->Get(((*s)+"_nominal").c_str());
      TChain *tnom=new TChain(((*s)+"_nominal").c_str(),"chain");
      for(vector<string>::iterator sf=infiles.begin(); sf!=infiles.end(); sf++){
        tnom->Add((*sf).c_str());
      }
      
      if(tnom==nullptr){ cout<<"nominal not found for "<<tnom<<endl; missingTrees.push_back(((*s)+"_nominal"));  continue; } 
      
      TH1F *hnom=new TH1F("hnom","hnom",10,-100000,+100000);
      tnom->Draw("Mll>>hnom",Form("weight*(%s)",(*r).second.c_str()),"");
      nom[(*s)][(*r).first]=hnom->Integral();
      delete hnom;
      delete tnom; 
      // loop over variations 
      

      for(vector<string>::iterator vars=variations.begin(); vars!=variations.end(); vars++){
        //TTree *tup=(TTree*)f->Get(((*s)+"_"+(*vars)+"__1up").c_str());

        TChain *tup=new TChain(((*s)+"_"+(*vars)+"__1up").c_str(),"chain");
        for(vector<string>::iterator sf=infiles.begin(); sf!=infiles.end(); sf++){
          tup->Add((*sf).c_str());
        }
        

        if(tup==nullptr){ cout<<"Missing tree "<<endl; up[(*vars)][(*s)][(*r).first]=nom[(*s)][(*r).first];   missingTrees.push_back(((*s)+"_"+(*vars)+"__1up")); continue; } 
        TH1F *hup= new TH1F("hup","hup",10,-100000,+100000);
        tup->Draw("Mll>>hup",Form("weight*(%s)",(*r).second.c_str()),"");
        up[(*vars)][(*s)][(*r).first]=hup->Integral();

        //cout<<" asas "<< up[(*vars)][(*s)][(*r).first]<<" --- "<<hup->Integral()<<endl;
        delete hup;
        delete tup;

        //TTree *tdown=(TTree*)f->Get(((*s)+"_"+(*vars)+"__1down").c_str());

        TChain *tdown=new TChain(((*s)+"_"+(*vars)+"__1down").c_str(),"chain");
        for(vector<string>::iterator sf=infiles.begin(); sf!=infiles.end(); sf++){
          tdown->Add((*sf).c_str());
        }

        if(tdown==nullptr){ cout<<"Missing tree "<<endl; down[(*vars)][(*s)][(*r).first]=nom[(*s)][(*r).first];   missingTrees.push_back(((*s)+"_"+(*vars)+"__1down")); continue; } 
        TH1F *hdown=new TH1F("hdown","hdown",10,-100000,+100000);
        tdown->Draw("Mll>>hdown",Form("weight*(%s)",(*r).second.c_str()),"");
        down[(*vars)][(*s)][(*r).first]=hdown->Integral();
        delete hdown;
        delete tdown;
      }
  }}

  cout<<"Missing trees: "<<endl;
  for(std::vector<string>::iterator it=missingTrees.begin(); it!=missingTrees.end(); it++){
    cout<<(*it)<<endl;
  }


  //std::map<string,std::vector<string>> considerSys;
  std::map<string,std::map<string,string>> considerSys;
  
  
  // Draw the table
  // loop regions
  for(std::map<string,string>::iterator r=regions.begin(); r!=regions.end(); r++){
    cout<<"Region "<<(*r).first<<endl;
    // loop components
    for(std::vector<string>::iterator s=treeNames.begin(); s!=treeNames.end(); s++){
      cout<<"Sample :"<<(*s)<<endl;
      cout<<" systematic & nominal & up [%] & down [%]"<<endl;
      for(vector<string>::iterator vars=variations.begin(); vars!=variations.end(); vars++){
        double trans=nom[(*s)]["SR"]/nom[(*s)][(*r).first];
        double transUP=up[(*vars)][(*s)]["SR"]/up[(*vars)][(*s)][(*r).first];
        double transDown=down[(*vars)][(*s)]["SR"]/down[(*vars)][(*s)][(*r).first];
        
        if( (fabs((1.0-up[(*vars)][(*s)][(*r).first]/nom[(*s)][(*r).first])) < .95 && fabs((1.0-down[(*vars)][(*s)][(*r).first]/nom[(*s)][(*r).first])) < 0.95 && up[(*vars)][(*s)][(*r).first]!=nom[(*s)][(*r).first] && down[(*vars)][(*s)][(*r).first]!=nom[(*s)][(*r).first]) ){ //||  ((fabs((1.0 - transUP/trans)) < .95 && fabs((1.0-transDown/trans)) < 0.95 && transUP!=trans && transDown!=trans))){
          cout<< (*vars)<<"  & "<<nom[(*s)][(*r).first]<<" & "<<100*(1-up[(*vars)][(*s)][(*r).first]/nom[(*s)][(*r).first])<<" & "<<100*(1-down[(*vars)][(*s)][(*r).first]/nom[(*s)][(*r).first])<<"  TF  ";
          cout<<nom[(*s)]["SR"]/nom[(*s)][(*r).first]<<" "<<100*(1-transUP/trans)<<" & "<<100*(1-transDown/trans)<<endl;

          if( (*s).compare("top") == 0 && (*r).first.compare("Top") == 0 ) 
            considerSys[(*s)].insert(make_pair(*vars,*vars));
          else if( (*s).compare("top") == 0 && (*r).first.compare("Top") != 0 )
            continue ;
          else if( (*s).compare("Zjets") == 0 && (*r).first.compare("Zjets") == 0 ) 
              considerSys[(*s)].insert(make_pair(*vars,*vars));
          else if( (*s).compare("Zjets") == 0 && (*r).first.compare("Zjets") != 0 )
            continue ;

          considerSys[(*s)].insert(make_pair(*vars,*vars));
          //else if( ((*s).compare("Zjets") !=0 && (*s).compare("top")!=0) && 
          
        }

      }}}

  cout<<"List of systematics to consider : "<<endl;

  
  for( std::map<string,std::map<string,string>>::iterator it=considerSys.begin(); it!=considerSys.end() ;it++ ){
    ofstream myfile;
    cout<<"SAMPLE "<<(*it).first<<endl;
    myfile.open("sysList_"+(*it).first+".txt");
    std::map<string,string> tmpa=(*it).second;
    for( std::map<string,string>::iterator it2=tmpa.begin(); it2!=tmpa.end(); it2++){
      cout<< (*it2).second<<endl;
      myfile << (*it2).second<<endl;
    }
    myfile.close();
  }
    
}

#include "tools.h"
//string prefix="Cut2Jet_v2_";
string prefix="";


TH1F *find_kname(string name, std::map<string, TH1F*> histMaps){
  TH1F *h=nullptr;
  for(std::map<string, TH1F*>::iterator it =histMaps.begin(); it!=histMaps.end(); it++){
    if( (*it).first.compare( name) ==0 ) { h=(TH1F*) (*it).second; break ;} 

  }
  return h; 

} 
 

string eraseSubStr(std::string in, const std::string & toErase)
{
  std::string mainStr=in;
  // Search for the substring in string
  size_t pos = mainStr.find(toErase);
 
  if (pos != std::string::npos)
	{
      // If found then erase it from string
      mainStr.erase(pos, toErase.length());
	}
  return mainStr;
}

void drawHSFitResults(string fname=""){
  
  string basename=eraseSubStr(fname,".root");
  
  TFile *f=TFile::Open(fname.c_str());
  std::map<string, TObject*> objMap;
  std::map<string, TH1F*> histMaps;
  std::map<string, RooCurve*> curveMaps;
  std::map<string, RooHist*> rooHistMaps;
  std::map<string, string> legendMap;
  

  std::map<string, int> colorMap;

  colorMap[prefix+"ggf"]=kBlue+1;
  colorMap[prefix+"ggf1"]=kBlue+1;
  colorMap[prefix+"ggf2"]=kBlue+1;
  colorMap[prefix+"ggf3"]=kBlue+1;
  
  colorMap[prefix+"vbf"]=kRed;
  colorMap[prefix+"vbf0"]=kRed;
  colorMap[prefix+"Vgamma"]=kGreen+2;
  colorMap[prefix+"Zjets0"]=kGreen;
  colorMap[prefix+"diboson"]=kYellow+2;
  colorMap[prefix+"top"]=kMagenta+2;
  colorMap[prefix+"vh"]=kBlue+3;
  colorMap[prefix+"htt"]=kBlue+4;
  colorMap[prefix+"Fakes"]=kYellow+4;
    
  colorMap[prefix+"Zjets1"]=kGreen;
  colorMap[prefix+"diboson1"]=kYellow+2;
  colorMap[prefix+"top1"]=kMagenta+2;

  legendMap[prefix+"ggf"]="ggF";
  legendMap[prefix+"ggf1"]="ggF";
  legendMap[prefix+"ggf2"]="ggF";
  legendMap[prefix+"ggf3"]="ggF";
  
  //  legendMap[prefix+"vbf"]="VBF";
  legendMap[prefix+"vbf0"]="VBF";
  legendMap[prefix+"Vgamma"]="#it{V}/#gamma";
  legendMap[prefix+"Zjets0"]="#it{Z}+jets";
  legendMap[prefix+"top"]="Top";
  legendMap[prefix+"diboson"]="Diboson";
  legendMap[prefix+"Fakes"]="Fakes";
  legendMap[prefix+"vh"]="VH";
  legendMap[prefix+"htt"]="#it{t#bar{t}H}";
  legendMap["SM_total"]="Total";


  legendMap[prefix+"Vgamma1"]="#it{V}/#gamma";
  legendMap[prefix+"Zjets1"]="#it{Z}+jets";
  legendMap[prefix+"top1"]="Top";
  legendMap[prefix+"diboson1"]="Diboson";
  legendMap[prefix+"Fakes1"]="Fakes";

  // missing vh, ttH, et al 
           
  // components to draw 
  vector <string> draw;
  draw.push_back(prefix+"ggf"); //draw.push_back(prefix+"Zjets"); draw.push_back(prefix+"top"); draw.push_back(prefix+"diboson");
  draw.push_back(prefix+"ggf1");
  draw.push_back(prefix+"ggf2");
  draw.push_back(prefix+"ggf3");
  draw.push_back(prefix+"htt");
  draw.push_back(prefix+"vh");
  draw.push_back(prefix+"Vgamma");
  draw.push_back(prefix+"Fakes");
  draw.push_back(prefix+"Zjets0");
  draw.push_back(prefix+"Zjets1");
  draw.push_back(prefix+"diboson1");
  draw.push_back(prefix+"diboson");
  draw.push_back(prefix+"top1");
  draw.push_back(prefix+"top");
  draw.push_back(prefix+"vbf0");
  
  vector <string> load;
  for( vector<string>::iterator it=draw.begin(); it!=draw.end(); it++) load.push_back(*it);
  load.push_back("h_asimovData");
  load.push_back("h_ratio");
  load.push_back("h_ratio_excl_sig");
  load.push_back("h_rel_error_band");
  load.push_back("h_total_error_band");

  // Stack of contributions 
  THStack *stack = new THStack(Form("stack_%s",basename.c_str()),"");
  
  //for(vector<string>::iterator it = load.begin(); it!=load.end(); it++){
  TKey *keyP=nullptr;
  TIter nextP(f->GetListOfKeys());

  TLegend *leg=DefLeg(0.75,0.65,0.9,0.95);

  
  
  while ((keyP=(TKey*)nextP())) {
    string kName=keyP->GetName();
    //if (strcmp(keyP->GetClassName(),"TTree")) continue;
    //if( (TObject*)f->Get((*it).c_str())==nullptr) { cout<<"object "<<*it<<" not found skipping "<<endl; continue; }
    objMap[kName]=(TObject*)f->Get(kName.c_str());


    if( (TString(keyP->GetClassName())).Contains("TH1")){
      histMaps[kName]=(TH1F*)f->Get(kName.c_str());
      SetDef(histMaps[kName]);
      if( colorMap.count(kName) ) {
        cout<<"Setting color for "<<kName<<" color "<<colorMap[kName]<<endl;
        histMaps[kName]->SetFillColor(colorMap[kName]);
        histMaps[kName]->SetLineColor(kBlack);
        histMaps[kName]->SetLineWidth(2);
        //histMaps[kName]->SetFillStyle(1);
        //stack->Add((TH1F*) histMaps[kName]);
        
      }
    }
    
    else if ( (TString(keyP->GetClassName())).Contains("RooCurve")){
      curveMaps[kName]=(RooCurve*)f->Get(kName.c_str());
    }
    else if ( (TString(keyP->GetClassName())).Contains("RooHist")){
      rooHistMaps[kName]=(RooHist*)f->Get(kName.c_str());
    }
  }

  // add the histograms to the stack
  for(int i=0; i<(int)draw.size(); i++){
    stack->Add(find_kname(draw.at(i), histMaps));
  }
  


  
  rooHistMaps["h_ratio_excl_sig"]->SetMarkerStyle(25);
  rooHistMaps["h_ratio_excl_sig"]->SetMarkerColor(kRed);
  rooHistMaps["h_ratio_excl_sig"]->SetLineColor(kRed);
  

  TH1F *hDraw=new TH1F(Form("hDraw_%s",basename.c_str()),"",100,histMaps[prefix+"vbf0"]->GetBinLowEdge(1),histMaps[prefix+"vbf0"]->GetBinLowEdge(histMaps[prefix+"vbf0"]->GetNbinsX()+1));
  SetDef(hDraw);
  hDraw->SetFillColor(kWhite);
  hDraw->SetLineColor(kWhite);
  hDraw->GetXaxis()->SetTitle(histMaps[prefix+"vbf0"]->GetXaxis()->GetTitle());
  hDraw->GetXaxis()->SetTitleOffset(9999);
  hDraw->GetYaxis()->SetTitle(histMaps[prefix+"vbf0"]->GetYaxis()->GetTitle());
  hDraw->GetYaxis()->SetTitleOffset(1.4);
  hDraw->GetYaxis()->SetRangeUser(0.25,1e6);


  TH1F *hDrawRatio=new TH1F(Form("hDrawRatio_%s",basename.c_str()),"",100,histMaps[prefix+"vbf0"]->GetBinLowEdge(1),histMaps[prefix+"vbf0"]->GetBinLowEdge(histMaps[prefix+"vbf0"]->GetNbinsX()+1));
  SetDef(hDrawRatio);
  hDrawRatio->SetFillColor(kWhite);
  hDrawRatio->SetLineColor(kWhite);
  hDrawRatio->GetXaxis()->SetTitle(histMaps[prefix+"vbf0"]->GetXaxis()->GetTitle());
  //hDrawRatio->GetYaxis()->SetTitle("Data / Expectation");
  hDrawRatio->GetYaxis()->SetTitle("Ratio");
  hDrawRatio->GetYaxis()->CenterTitle(true);
  hDrawRatio->GetXaxis()->SetTitleOffset(3.0);
  hDrawRatio->GetYaxis()->SetRangeUser(-0.24,2.24);
  hDrawRatio->GetYaxis()->SetTitleOffset(1.4);

  /*
  hDrawRatio->GetXaxis()->SetTitleFont(43);
  hDrawRatio->GetYaxis()->SetTitleFont(43);
  hDrawRatio->GetXaxis()->SetLabelFont(43);
  hDrawRatio->GetYaxis()->SetLabelFont(43);
  hDrawRatio->GetXaxis()->SetTitleSize(20);
  hDrawRatio->GetYaxis()->SetTitleSize(20);
  hDrawRatio->GetXaxis()->SetLabelSize(20);
  hDrawRatio->GetYaxis()->SetLabelSize(20);
  hDrawRatio->GetYaxis()->SetRangeUser(0,+2.34);
  hDrawRatio->GetXaxis()->SetTitleOffset(2.5);
  hDrawRatio->GetYaxis()->SetTitleOffset(1.5);
  */
  
  leg->AddEntry(rooHistMaps["h_asimovData"],"Asimov","PE");
  // add the legend stuff in the correct order
  for(std::map<string,TH1F*>::const_iterator it=histMaps.begin(); it!=histMaps.end(); it++)
    if( colorMap.count( (*it).first))
      leg->AddEntry((*it).second,legendMap[(*it).first].c_str(),"F");
        
  TLatex *lat=new TLatex();
  lat->SetTextFont(43);

 
  TPad*    stack3 =nullptr;
  TPad*    comparison =nullptr;

   double canX=800;
   double canY=800;
  
   TCanvas *can=new TCanvas(Form("can_%s",basename.c_str()),"",canX,canY);{
     SetCanvasDefaults(can);
     can->cd();
     can->SetBottomMargin(0);

    stack3= new TPad(Form("stack3_%s",basename.c_str()), "", 0, 0.4, 1, 1);   
    comparison= new TPad(Form("comp3_%s",basename.c_str()), "", 0,0, 1, 0.4);

    stack3->SetRightMargin(0.02);
    stack3->SetLeftMargin(0.16);
    stack3->SetTopMargin(0.02);
    stack3->SetBottomMargin(0.001);
   
    comparison->SetRightMargin(0.02);
    comparison->SetTopMargin(0);
    comparison->SetBottomMargin(0.45);
    comparison->SetLeftMargin(0.16);
    
    stack3->SetTicks();
    comparison->SetTicks();
    
    can->cd();
    stack3->Draw();
    comparison->Draw();   

    stack3->cd();
    // top pad 
    stack3->SetLogy();
    hDraw->Draw();
    stack->Draw("hist same");
    //curveMaps["h_total_error_band"]->Draw("same");
    //srooHistMaps["h_asimovData"]->Draw("PE same");
    leg->Draw();
    //can->RedrawAxis();
    //lat->DrawLatexNDC(0.15,1e5,"#bf{#it{ATLAS}} Internal");
    ATLASLabel(lat,0.2,0.92,false,1,139.1);
    stack3->RedrawAxis();
    // bottom pad;

    comparison->cd();
    hDrawRatio->Draw();
    rooHistMaps["h_ratio"]->Draw("PE same");
    rooHistMaps["h_ratio_excl_sig"]->Draw("PE same");
    curveMaps["h_rel_error_band"]->Draw("E3 same");
    comparison->RedrawAxis();
    
  }


}

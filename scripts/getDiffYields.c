#include "tools.h"

void getDiffYields(string fname=""){
  TFile *f=TFile::Open(fname.c_str());
  std::map<string,int> nBinsDiff;
  nBinsDiff["Mjj"]=8;
  string currentDistribution="Mjj";

  std::map<string, string> regionRegSignal;
  regionRegSignal["SRVBF"]="bdt_vbf";
  regionRegSignal["CRTop"]="bdt_TopWWAll2";

  double sigYileds[nBinsDiff[currentDistribution]]; 

  double GeVtoMeV=1e3;
  double binEdges[9]={200*GeVtoMeV, 450*GeVtoMeV, 700.0*GeVtoMeV,950.0*GeVtoMeV,1200.0*GeVtoMeV,1500.0*GeVtoMeV, 2200.0*GeVtoMeV,3000.0*GeVtoMeV,5000*GeVtoMeV};
  double cFactorsDiff[8]={0.449,0.459,0.469,0.475,0.481,0.474,0.470,0.470};
  const double lumi=139; 

  
  // loop over the samples
  for( int k=0; k<(int)nBinsDiff[currentDistribution]; k++){
    sigYileds[k]=0;
    // loop over the bins
    for( int i=0; i<(int)nBinsDiff[currentDistribution]; i++){
      //loop over the regions 
      for(std::map<string,string>::iterator it=regionRegSignal.begin(); it!=regionRegSignal.end(); it++){
        //for(int j=0; j<(int)regionRegSignal.size(); j++){
        TH1F *h=(TH1F*)f->Get(Form("hvbf0_%dNom_%s_%d_obs_%s",k, (*it).first.c_str(),i, (*it).second.c_str()));
        sigYileds[k]+=h->Integral();
        delete h;
      }
  }}

  double totCrossSec=0;
  double totEvents=0;
  for( int k=0; k<(int)nBinsDiff[currentDistribution]; k++){
    cout<<" Bin "<<k<<" Events "<< sigYileds[k] <<" xsec "<< sigYileds[k]/(lumi*cFactorsDiff[k])  << " Diff xsec" << sigYileds[k]/(lumi*cFactorsDiff[k]) / ( binEdges[k+1] - binEdges[k])  <<" fb "<<endl;
    totCrossSec+=sigYileds[k]/(lumi*cFactorsDiff[k]);
    totEvents+=sigYileds[k];
  }
  cout<<"Total "<<totEvents<<" xsec "<<totCrossSec<<" fb "<<endl;


  cout<<"OneOverCDiff=[";
  for(int k=0; k<(int)nBinsDiff[currentDistribution]; k++){
    cout<<1/(sigYileds[k]/(lumi*cFactorsDiff[k]))<<", ";
  }
  cout<<"]"<<endl;
  
  cout<<"DiffXsec=[";
  for(int k=0; k<(int)nBinsDiff[currentDistribution]; k++) {
    cout<<sigYileds[k]/(lumi*cFactorsDiff[k])<<", ";
  }
  cout<<"]"<<endl;
  
}

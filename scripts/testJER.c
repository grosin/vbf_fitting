#include "tools.h"
using namespace std;

vector <string> GetTreeNames(string treeName="", string prefix=""){
 vector <string> treeNames;

 if(treeName.size()==0){
   treeNames.push_back("Zjets");
   treeNames.push_back("top");
   treeNames.push_back("diboson");
   treeNames.push_back("ggf");
   treeNames.push_back("vbf");
   treeNames.push_back("Vgamma");
   treeNames.push_back("FakeE");
   treeNames.push_back("FakeM");
   treeNames.push_back("Vgamma");
   treeNames.push_back("vh");
   treeNames.push_back("data");
 }

  else {
    std::istringstream iss(treeName);  
    std::string s;                                                               
    while(getline(iss, s, ','))                                                  
      {                                                                            
        treeNames.push_back(s);                                                           
      }
  }

 // add the prefix
 if(prefix.size()>0){
   for(vector<string>::iterator s=treeNames.begin(); s!=treeNames.end(); s++){
     (*s)=prefix+(*s); 
     cout<<"Adding prefix .."<<(*s)<<endl;
   }
 }
   
 return treeNames;
  
}


void testJER(){

  string listFiles="/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/October2019_withSys/LauraHF/list_all_local_v20_v2.txt";

  vector <string> distrs;
  distrs.push_back("bdt_vbf");
  distrs.push_back("Mjj");
  std::map<string, string> regions;
  regions["SR"]="inSR==1";
  
  vector <string> samples=GetTreeNames();
  //samples.push_back("vbf");
  //samples.push_back("diboson");
  //samples.push_back("top");
  //samples.push_back("ggf");
  //samples.push_back("Zjets");
  //samples.push_back("Vgamma");
  
  
    vector <string> removePatterns;
    removePatterns.push_back("JET_JER_DataVsMC_MC162");
    removePatterns.push_back("JET_JER_EffectiveNP_12");
    removePatterns.push_back("JET_JER_EffectiveNP_22");
    removePatterns.push_back("JET_JER_EffectiveNP_32");
    removePatterns.push_back("JET_JER_EffectiveNP_42");
    removePatterns.push_back("JET_JER_EffectiveNP_52");
    removePatterns.push_back("JET_JER_EffectiveNP_62");
    removePatterns.push_back("JET_JER_EffectiveNP_72");
    removePatterns.push_back("JET_JER_EffectiveNP_82");
    removePatterns.push_back("JET_JER_EffectiveNP_92");
    removePatterns.push_back("JET_JER_EffectiveNP_102");
    removePatterns.push_back("JET_JER_EffectiveNP_112");
    removePatterns.push_back("JET_JER_EffectiveNP_12restTerm2");
    
    /*
    removePatterns.push_back("JET_JER_DataVsMC_MC16");
    removePatterns.push_back("JET_JER_EffectiveNP_1");
    removePatterns.push_back("JET_JER_EffectiveNP_2");
    removePatterns.push_back("JET_JER_EffectiveNP_3");
    removePatterns.push_back("JET_JER_EffectiveNP_4");
    removePatterns.push_back("JET_JER_EffectiveNP_5");
    removePatterns.push_back("JET_JER_EffectiveNP_6");
    removePatterns.push_back("JET_JER_EffectiveNP_7");
    removePatterns.push_back("JET_JER_EffectiveNP_8");
    removePatterns.push_back("JET_JER_EffectiveNP_9");
    removePatterns.push_back("JET_JER_EffectiveNP_10");
    removePatterns.push_back("JET_JER_EffectiveNP_11");
    removePatterns.push_back("JET_JER_EffectiveNP_12restTerm");
    */
    

  std::map<strig, std::map<string,TChain*>> mapChain; 
  
  
  
  



}

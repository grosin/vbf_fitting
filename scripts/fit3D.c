/* Gaetano Barone <gaetano.barone@cern.ch>
 */
#include "tools.h"
#include "minitree.h"

#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooGaussModel.h"
#include "RooConstVar.h"
#include "RooDecay.h"
#include "RooLandau.h"
#include "RooProdPdf.h"
#include "RooHistPdf.h"
#include "RooPlot.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TH1.h"
#include "RooNDKeysPdf.h"
#include <RooMomentMorph.h>


using namespace RooFit;
using namespace RooStats;
using namespace std;



// 0 for 3D hist; 1 for 3D cond with background cond (Keys); 2 for 3D cond signal and template background (Keys)
int fitCase=0; 
bool doPLLCurves=false;
bool do1D=false;
bool do2D=false; // float WW and VBF in SR
bool autoHist3DBinning=false;
bool reparametrizeTrop = false ; // Add top's contribution into the WW template and fit the sum. 
bool fitTopInsteadOfGGF=false ;
bool addUncertaintyOnNjets=true;//true;

enum DataSetType{Asimov=0, Data=1, MC=2};
DataSetType datasetType=DataSetType::Asimov;

double getRatioggFError(double num1=0,double num1_e=0,double num2=0, double num2_e=0,double den=0, double den_e=0){
  double err=0;
  err+=pow(num2/den*num1_e,2);
  err+=pow(num1/den*num2_e,2);
  err+=pow(num1*num2/(den*den)*den_e,2);
  err=sqrt(err);
  return err;
}


bool hasBackground(string name, vector <string> list){
  for( vector<string>::iterator it=list.begin(); it!=list.end(); it++)
    if( (*it).compare(name)==0) return true; 
  return false;
}

RooDataSet *ReduceDataSet(RooDataSet *data=nullptr,RooArgSet *vars=nullptr,double fraction=1.0){
  if(fraction==1.0) return data;
  TRandom3 r;
  int startEventountSignal=r.Uniform(1,data->numEntries()*0.5);
  RooDataSet *reduced=(RooDataSet*)data->reduce(SelectVars(*vars),EventRange(startEventountSignal,(int) startEventountSignal+fraction*data->numEntries()));
  return reduced;
  
}

RooDataSet *mcDataSet(std::map<string, RooDataSet*> mapData, vector <string> consider, RooArgSet *vars,double fraction=1.0 ){
  RooDataSet *mcDataSet=nullptr;
  for(std::vector<string>::iterator it=consider.begin(); it!=consider.end(); it++){
    if(mcDataSet==nullptr) mcDataSet= (RooDataSet*) mapData[(*it)]->emptyClone("mcDataSet");
    mcDataSet->append(*ReduceDataSet(mapData[*it],vars,fraction));
  }
  
  return mcDataSet;
}


vector <string> GetTreeNames(string treeName=""){
 vector <string> treeNames;
 /*
  if(treeName.size()==0){
    treeNames.push_back("Zjets");
    treeNames.push_back("Top");
    treeNames.push_back("WW");
    treeNames.push_back("GGF");
    treeNames.push_back("VBF");
    //treeNames.push_back("FakeE");
    //treeNames.push_back("FakeM");
    //treeNames.push_back("Vgamma");
    //treeNames.push_back("vh");
    treeNames.push_back("Data");
    }*/

 if(treeName.size()==0){
   treeNames.push_back("Zjets");
   treeNames.push_back("top");
   treeNames.push_back("diboson");
   treeNames.push_back("ggf");
   treeNames.push_back("vbf");
   //treeNames.push_back("FakeE");
   //treeNames.push_back("FakeM");
   //treeNames.push_back("Vgamma");
   //treeNames.push_back("vh");
   treeNames.push_back("data");
 }

  else {
    std::istringstream iss(treeName);  
    std::string s;                                                               
    while(getline(iss, s, ','))                                                  
      {                                                                            
        treeNames.push_back(s);                                                           
      }
  }
  return treeNames;
  
}

// infile eval_VBF_WW_GGF_TOP_Data_Zjets.root
// infile eval_3D_AllSamps_2jet_Dec17.root <-- latest
// infile Feb12TopFiles/eval_4D_Top_AllSamps_VBF_SR_Feb5.root for 4D including top 
// infile /Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/June2019SS_withSys/eval_sys_total_merged.root

void fit3D(string infile="eval_3D_AllSamps_2jet_Dec17.root",string treeName="" ){
  RooWorkspace *w=new RooWorkspace("w","workspace");
  ModelConfig *model=new ModelConfig("ModelConfig","ModelConfig",w);

  // Here loop over the CR 
  // Build a map of files asscociated to the CR; 
  std::map<string,string> channelFiles;
  //channelFiles["signal"]=infile; 
  //channelFiles["ggFCR"]="AllSamps_2jet_Dec17.root";
  //channelFiles["ggFCR"]="AllSamps_1jet_Dec17.root";
  //channelFiles["ggFCRNj2"]=infile;
  //channelFiles["ggFCRNj1"]="NtuplesWithCuts/AllSamps_ggF_1jet_Jan18.root";
  //channelFiles["ggFCRNj0"]="NtuplesWithCuts/AllSamps_ggF_0jet_Jan18.root";

  //channelFiles["ggFCRNj1"]="NtuplesWithCuts/eval_3D_AllSamps_ggF_1jet_Jan18.root";
  //channelFiles["ggFCRNj0"]="NtuplesWithCuts/eval_3D_AllSamps_ggF_0jet_Jan18.root";
  //channelFiles["TopCR"]="NtuplesWithCuts/eval_3D_AllSamps_Top_CR_Jan18.root";

  //channelFiles["signal"]="Jan18/eval_DNN_AllSamps_VBF_SR_Feb5.root";
  //channelFiles["ggFCRNj2"]="eval_3Ddataset_Fullset_Oct31.root";
  //channelFiles["ggFCRNj1"]=infile;
  //channelFiles["ggFCRNj0"]="NtuplesWithCuts/eval_3D_AllSamps_ggF_0-1jet_Jan18.root";
  //channelFiles["TopCR"]="NtuplesWithCuts/eval_3D_AllSamps_Top_CR_Jan18.root";

  // To be used with DNN 
  //channelFiles["signal"]="Jan18/eval_DNN_AllSamps_VBF_SR_Feb5.root";
  //channelFiles["ggFCRNj2"]="eval_3Ddataset_Fullset_Oct31.root";
  //channelFiles["ggFCRNj1"]="eval_3Ddataset_Fullset_Oct31.root";
  //channelFiles["ggFCRNj0"]="NtuplesWithCuts/eval_3D_AllSamps_ggF_0-1jet_Jan18.root";
  //channelFiles["TopCR"]="NtuplesWithCuts/eval_3D_AllSamps_Top_CR_Jan18.root";
  //infile = channelFiles["signal"];

  /*
  channelFiles["signal"]="eval_3D_AllSamps_2jet_Dec17.root";
  channelFiles["ggFCRNj2"]="eval_3D_AllSamps_2jet_Dec17.root";
  channelFiles["ggFCRNj1"]="eval_3D_AllSamps_2jet_Dec17.root";
  channelFiles["ggFCRNj0"]="NtuplesWithCuts/eval_3D_AllSamps_ggF_0-1jet_Jan18.root";
  //channelFiles["TopCR"]="NtuplesWithCuts/eval_3D_AllSamps_Top_CR_Jan18.root";
  //channelFiles["TopCR"]="Feb12TopFiles/eval_4D_Top_AllSamps_VBF_SR_Feb5.root";
  channelFiles["TopCR"]="Feb12TopFiles/eval_TopWW_AllSamps_VBF_SR_Feb5.root";
  */

  /*
  // Latest samples as of 8/4/2019
  channelFiles["signal"]="eval_3D_AllSamps_2jet_Dec17.root";
  channelFiles["ggFCRNj2"]="Jan18_withBDTggFCR/ggFCRNj2/eval_datasetggFCRNj2_AllSamps_2jet_Feb26.root";
  channelFiles["ggFCRNj1"]="Jan18_withBDTggFCR/ggFCRNj1/eval_datasetggFCRNj1_AllSamps_2jet_Feb26.root";
  //channelFiles["ggFCRNj1"]="NtuplesWithCuts/eval_3D_AllSamps_ggF_0-1jet_Jan18.root";
  channelFiles["ggFCRNj0"]="NtuplesWithCuts/eval_3D_AllSamps_ggF_0-1jet_Jan18.root";
  //channelFiles["TopCR"]="NtuplesWithCuts/eval_3D_AllSamps_Top_CR_Jan18.root";
  //channelFiles["TopCR"]="Feb12TopFiles/eval_4D_Top_AllSamps_VBF_SR_Feb5.root";
  channelFiles["TopCR"]="Feb12TopFiles/eval_TopWW_AllSamps_VBF_SR_Feb5.root";
  */

  /*
 // Latest samples as of 26/4/2019 with all ranges 0 -- 1 for BDTs
  channelFiles["signal"]="eval_3D_AllSamps_2jet_Dec17.root";
  channelFiles["ggFCRNj2"]="Jan18_withBDTggFCR_withTranform/ggFCRNj2/MulticlassOutput/eval_datasetggFCRNj2_AllSamps_2jet_Feb26.root";
  channelFiles["ggFCRNj1"]="Jan18_withBDTggFCR_withTranform/ggFCRNj1/MulticlassOutputs/eval_datasetggFCRNj1_AllSamps_2jet_Feb26.root";
  //channelFiles["ggFCRNj1"]="NtuplesWithCuts/eval_3D_AllSamps_ggF_0-1jet_Jan18.root";
  channelFiles["ggFCRNj0"]="NtuplesWithCuts/eval_3D_AllSamps_ggF_0-1jet_Jan18.root";
  //channelFiles["TopCR"]="NtuplesWithCuts/eval_3D_AllSamps_Top_CR_Jan18.root";
  //channelFiles["TopCR"]="Feb12TopFiles/eval_4D_Top_AllSamps_VBF_SR_Feb5.root";
  channelFiles["TopCR"]="Feb12TopFiles/eval_TopWW_AllSamps_VBF_SR_Feb5.root";
  */

  // channels files with june 19 samples 
  channelFiles["signal"]="/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/June2019SS_withSys/total_merged_2jet_noFakes.root";
  channelFiles["ggFCRNj2"]="/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/June2019SS_withSys/total_merged_2jet_noFakes.root";
  channelFiles["ggFCRNj1"]="/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/June2019SS_withSys/total_merged_0jet_noFakes.root";
  channelFiles["ggFCRNj0"]="/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/June2019SS_withSys/total_merged_0jet_noFakes.root";
  channelFiles["TopCR"]="/Users/gaetano/Atlas/Analysis/HiggsWW/Analysis/Fit/June2019SS_withSys/total_merged_2jet_noFakes.root";

  
  
  TFile *wsfile=new TFile("workspace.root","RECREATE");
  // Create the workpace
  //RooWorkspace *w=new RooWorkspace("w","workspace");
  //Channels 
  // map of channels and cuts
  std::map<string,string> channelCuts;
  channelCuts["signal"]="(inSR==1 && nJetsTight>=2 && nBJetsSubMV2c10<1 && centralJetVetoLeadpT<20000 && OLV==1 && mtt/1000<mZ/1000-25)";
  //channelCuts["signal"]="(nJetsTight>=2 && nBJetsSubMV2c10<1 && centralJetVetoLeadpT<20000 && OLV==1 && mtt/1000<mZ/1000-25 && Mll<100000 && MT<150000)";
  //channelCuts["signal"]="nJetsTight>1";
  //channelCuts["signal"]="";
  //channelCuts["ggFCR"]="nJetsTight<2 && nBJetsSubMV2c10<1 && Mll<55000 && DPhill <1.8  && mtt/1000<mZ/1000-25  && centralJetVetoLeadpT<20000 && OLV==1";
  //channelCuts["ggFCR"]="(nJetsTight>=2 && nBJetsSubMV2c10<1 && (centralJetVetoLeadpT>=20000 || OLV!=1) && mtt/1000<mZ/1000-25)";
  
  // Tuned.. 
  //channelCuts["ggFCRNj2"]="(bdt_WW>0.15 && Mll<65000 && Mjj<400000 && DEtajj<4 && DPhill<1.8 && MET<100000 && nJetsTight==2 && nBJetsSubMV2c10==0 && OLV!=1 && mtt/1000<mZ/1000-25 && centralJetVetoLeadpT<50000)";
  //channelCuts["ggFCRNj1"]="(nJetsTight==1)";
  //channelCuts["ggFCRNj0"]="(nJetsTight==0)";
  //channelCuts["TopCR"]="(nJetsTight>=2)";
  
  // Tuned.. 
  //channelCuts["ggFCRNj2"]="(bdt_WW>0.15 && Mll<65000 && Mjj<400000 && DEtajj<4 && DPhill<1.8 && MET<100000 && nJetsTight>2 && nBJetsSubMV2c10==0 && OLV!=1 && mtt/1000<mZ/1000-25 && centralJetVetoLeadpT<50000)";
  //channelCuts["ggFCRNj1"]="(bdt_WW>0.15 && Mll<65000 && Mjj<400000 && DEtajj<4 && DPhill<1.8 && MET<100000 && nJetsTight==2 && nBJetsSubMV2c10==0 && OLV!=1 && mtt/1000<mZ/1000-25)";
  //channelCuts["ggFCRNj0"]="(nJetsTight==0 || nJetsTight==1)";
  //channelCuts["TopCR"]="(nJetsTight>=2)"

  // With jet multiplicity and CJV and OLV stuff
  channelCuts["ggFCRNj2"]="(nJetsTight>=2 && nBJetsSubMV2c10==0 && mtt/1000<mZ/1000-25 && centralJetVetoLeadpT>=20000 && OLV!=1)";
  channelCuts["ggFCRNj1"]="(nJetsTight>=2 && nBJetsSubMV2c10==0 && mtt/1000<mZ/1000-25 && ((centralJetVetoLeadpT<20000 && OLV==1) || (centralJetVetoLeadpT>=20000 && OLV!=1)))";
  channelCuts["ggFCRNj0"]="(nJetsTight <2  && nBJetsSubMV2c10==0 && mtt/1000<mZ/1000-25)";
  channelCuts["TopCR"]="(inTopCR==1)";
  channelCuts["ZjetsCR"]="(inZjetsCR==1)";

  // With jet multiplicity and CJV and OLV stuff (experimental)
  //channelCuts["ggFCRNj2"]="(nJetsTight>=2 && nBJetsSubMV2c10==0 && mtt/1000<mZ/1000-25 && centralJetVetoLeadpT<20000)";
  //channelCuts["ggFCRNj1"]="(nJetsTight>0 && nJetsTight<2 && nBJetsSubMV2c10==0 && mtt/1000<mZ/1000-25)";
  //channelCuts["ggFCRNj0"]="(nJetsTight<1)";
  //channelCuts["TopCR"]="(nJetsTight>=2)";
  

  
  RooCategory *channelCat=new RooCategory("channel","channel");
  channelCat->defineType("signal");
  channelCat->defineType("ggFCRNj2");
  channelCat->defineType("ggFCRNj1");
  channelCat->defineType("ggFCRNj0");
  channelCat->defineType("TopCR");
  channelCat->defineType("ZjetsCR");
  
  //for( unsigned int i=0; i<channelCuts.size(); i++) 
  //channelCat->defineType(channelCuts.at(i).first);
  
  RooRealVar normLumi("normLumi","#it{n}_{L}",36);
  RooRealVar targLumi("targLumi","#it{t}_{L}",139);
  RooFormulaVar lumiScaling("lumiScaling","#it{s}_{L}","@0/@1",RooArgSet(targLumi,normLumi));
  
  //w->import(normLumi);
  //w->import(targLumi);
  //w->import(lumiScaling);

  //const int nBBTBins=fitCase == 0  ? 15:50;
  // Stefania
  const int nBBTBins=15;
  // Create the observables: 3D BDT 
  //RooRealVar bdt_ggF("bdt_ggF","#it{R}_{#it{gg}F}",0,1);
  // for NN
  //RooRealVar bdt_ggF("bdt_ggf","#it{R}_{#it{gg}F}",0,1);
  RooRealVar *bdt_ggF=new RooRealVar ("bdt_ggF","#it{R}_{#it{gg}F}",0,1);
  RooBinning *ggF_bins=new RooBinning(0,1,"ggF_bins");
  ggF_bins->addUniform(nBBTBins,0,1);
  bdt_ggF->setBinning(*ggF_bins,"ggF_bins");


  RooRealVar *bdt_WW=new RooRealVar("bdt_WW","#it{R}_{#it{WW}}",0,0,1);
  RooBinning *WW_bins=new RooBinning(0,1,"WW_bins");
  WW_bins->addUniform(nBBTBins,0.0,1);  
  bdt_WW->setBinning(*WW_bins,"WW_bins");
  
  RooRealVar *bdt_vbf=new RooRealVar("bdt_vbf","#it{R}_{VBF}",0,0,1);
  RooBinning *vbf_bins=new RooBinning(0,1,"vbf_bins");
  vbf_bins->addUniform(nBBTBins,0.0,1);
  bdt_vbf->setBinning(*vbf_bins,"vbf_bins");

  RooRealVar *bdt_Top=new RooRealVar("bdt_top","#it{R}_{Top}",0,1);
  RooBinning *Top_bins=new RooBinning(0.0,1,"Top_bins");
  Top_bins->addUniform(nBBTBins,0.0,1);
  bdt_Top->setBinning(*Top_bins,"Top_bins");

  // Map of binning 
  std::map<string,string> mapBinning;
  mapBinning["bdt_vbf"]="vbf_bins";
  mapBinning["bdt_WW"]="WW_bins";
  mapBinning["bdt_ggF"]="ggF_bins";
  mapBinning["bdt_Top"]="Top_bins";


  RooRealVar weight("weight","",-1e6,+1e6);

  // Auxiliary observables on which we cut 
  RooRealVar nJetsTight("nJetsTight","nJetsTight",0,1e3);
  RooRealVar nBJetsSubMV2c10("nBJetsSubMV2c10","nBJetsSubMV2c10",-1e10,1e10);
  RooRealVar centralJetVetoLeadpT("centralJetVetoLeadpT","centralJetVetoLeadpT",-1e10,1e10);
  RooRealVar OLV("OLV","OLV",0,1);
  RooRealVar mtt("mtt","mtt",-1e10,+1e10);
  RooRealVar mZ("mZ","mZ",91,0,1e10);
  RooRealVar Mll("Mll","Mll",0,0,+1e10);
  //RooRealVar MT("MT","MT",0,0,400000); 
  RooRealVar MT("MT","MT",0,25000,300000);
  MT.setBins(25);
  RooRealVar Mjj("Mjj","Mjj",0,0,4e10);
  RooRealVar DEtajj("DEtajj","DEtajj",0,-1e10,1e10);
  RooRealVar MET("MET","MET",0,0,1e10);
  RooRealVar DPhill("DPhill","DPhill",0,-1e3,+1e3);
  RooRealVar inSR("inSR","inSR",0,0,2);
  RooRealVar inTopCR("inTopCR","inTopCR",0,0,2);
  RooRealVar inWWCR("inWWCR","inWWCR",0,0,2);
  RooRealVar inZjetsCR("inZjetsCR","inZjetsCR",0,0,2);
  RooRealVar inggFCR1("inggFCR1","inggFCR1",0,0,2);
  RooRealVar inggFCR2("inggFCR2","inggFCR2",0,0,2);
  RooRealVar inggFCR3("inggFCR3","inggFCR3",0,0,2);
   
 


  RooArgSet vars; 
  if(!fitTopInsteadOfGGF) 
    vars.add(*bdt_ggF); 
  vars.add(*bdt_WW); vars.add(*bdt_WW); vars.add(*bdt_vbf);
  vars.add(nJetsTight); vars.add(nBJetsSubMV2c10); vars.add(centralJetVetoLeadpT); vars.add(OLV); vars.add(mtt); 
  vars.add(mZ); vars.add(MT); vars.add(Mll); vars.add(DPhill); vars.add(Mjj); vars.add(DEtajj); vars.add(MET); vars.add(DPhill);

  vars.add(inSR); vars.add(inTopCR);  vars.add(inWWCR); vars.add(inZjetsCR); vars.add(inggFCR1); vars.add(inggFCR2); vars.add(inggFCR3);
  vars.add(weight);
  if(fitTopInsteadOfGGF) 
    vars.add(*bdt_Top);

  // Create a map of the observables 
  std::map<string,RooRealVar*> mapBDTObs;
  if(!do1D){
    if(!do2D) 
      mapBDTObs["bdt_ggF"]=bdt_ggF;
    mapBDTObs["bdt_WW"]=bdt_WW;
  }
  mapBDTObs["bdt_vbf"]=bdt_vbf;

  std::map<string,RooBinning*> mapBDTBins;
  if(!do1D){
    if(!do2D){
      if(!fitTopInsteadOfGGF)
        mapBDTBins["bdt_ggF"]=ggF_bins;
      else 
        mapBDTBins["bdt_Top"]=Top_bins;
    }

    mapBDTBins["bdt_WW"]=WW_bins;
  }
  mapBDTBins["bdt_vbf"]=vbf_bins;
  
  TIter cpoiI=vars.createIterator();
  RooRealVar *dummyVarZ=nullptr;
  while((dummyVarZ = (RooRealVar*)cpoiI.Next())){
    //w->import(*dummyVarZ); 
  }
            
  RooArgSet mainObs("mainObs");


  mainObs.add(*bdt_vbf); // add the channel category 
  if(!do1D){
    if(!do2D){
        if(!fitTopInsteadOfGGF)
          mainObs.add(*bdt_ggF); 
        else 
          mainObs.add(*bdt_Top);
    }
    mainObs.add(*bdt_WW); 
  }

  // Signal and background parameters; 
  RooRealVar *n_ggF=new RooRealVar("n_ggF","#it{n}_{#it{gg}F}",1,0,10);
  RooRealVar *n_WW=new RooRealVar("n_WW","#it{n}_{#it{WW}}",1,0,10);
  RooRealVar *n_vbf=new RooRealVar("n_vbf","#it{n}_{VBF}",1,0,10);
  std::map<string,RooRealVar*> mapBDTCoeff;
  mapBDTCoeff["bdt_ggF"]=n_ggF;
  mapBDTCoeff["bdt_WW"]=n_WW;
  mapBDTCoeff["bdt_vbf"]=n_vbf;
  RooArgSet pois; pois.add(*n_ggF); pois.add(*n_WW); pois.add(*n_vbf);
  //w->import(n_ggF); w->import(n_WW); w->import(n_vbf); 

  RooRealVar *norm_ggF=new RooRealVar("norm_ggF","#it{b}_{#it{gg}F}",1,-1e8,1e8);
  RooRealVar *norm_WW=new RooRealVar("norm_WW","#it{b}_{#it{WW}}",1,-1e8,1e8);
  RooRealVar *norm_vbf=new RooRealVar("norm_vbf","#it{b}_{VBF}",1,-1e8,1e8);
  std::map<string,RooRealVar*> mapBDTNormCoeff;
  mapBDTNormCoeff["bdt_ggF"]=norm_ggF;
  mapBDTNormCoeff["bdt_WW"]=norm_WW;
  mapBDTNormCoeff["bdt_vbf"]=norm_vbf;

  //RooFormulaVar  *otherScaling=new RooFormulaVar();
  //RooFormulaVar *f_ggF=new RooFormulaVar("{f}_{#it{gg}F}","@0*@1*@2*@3",RooArgSet(n_ggF,norm_ggF,lumiScaling,*otherScaling));
  RooFormulaVar *f_WW=new RooFormulaVar("f_WW","#it{f}_{#it{WW}}","@0*@1*@2",RooArgSet(*n_WW,*norm_WW,lumiScaling));
  RooFormulaVar *f_vbf=new RooFormulaVar("f_vbf","#it{f}_{VBF}","@0*@1*@2",RooArgSet(*n_vbf,*norm_vbf,lumiScaling));

  std::map<string,RooFormulaVar*> mapBDTFCoeff;
  //mapBDTFCoeff["bdt_ggF"]=f_ggF;
  mapBDTFCoeff["bdt_WW"]=f_WW;
  mapBDTFCoeff["bdt_vbf"]=f_vbf;

  std::map<string,RooRealVar*> mapSRNormCoeff;


  // Open the trees 
  TFile *f=TFile::Open(infile.c_str());
   vector <string> treeNames=GetTreeNames(treeName);
   std::map<string,std::map<string,TTree*>> treeMap;
   
   std::map<string,int> colorMap;;
   colorMap["vbf"]=kRed;
   colorMap["ggf"]=kBlue;
   colorMap["Diboson"]=kYellow-3;
   colorMap["Zjets"]=kMagenta;
   colorMap["top"]=kCyan;
   colorMap["data"]=kBlack;
   colorMap["FakeE"]=kOrange;
   colorMap["FakeM"]=kOrange+2;
   colorMap["Vgamma"]=kGreen;
   colorMap["vh"]=kGreen+2;
   
   
   std::map <string,string> bdtClasses;
   //bdtClasses["VBF"]="bdt_vbf";
   //bdtClasses["WW"]="bdt_WW";
   //bdtClasses["GGF"]="bdt_ggF";

   bdtClasses["vbf"]="bdt_vbf";
   bdtClasses["diboson"]="bdt_WW";
   bdtClasses["ggf"]="bdt_ggF";
   
   std::vector <string> bdtDiminesions;
   bdtDiminesions.push_back("bdt_vbf");
   if(!do1D){
     if(!do2D) 
       bdtDiminesions.push_back("bdt_ggF");
     bdtDiminesions.push_back("bdt_WW");
   }
   
   std::map <string,string> ibdtClasses;
   ibdtClasses["bdt_vbf"]="vbf";
   ibdtClasses["bdt_WW"]="diboson";
   ibdtClasses["bdt_ggF"]="ggf";
     
   // create 3 datasets 
   // Signal Region 
   std::map<string, RooDataSet*> mapData;
   
   // Load the tree
  for( vector <string>::iterator it=treeNames.begin(); it!=treeNames.end(); it++){
    //TTree *t=(TTree*)f->Get((*it).c_str());
    TTree *t=nullptr;
    t=(TTree*)f->Get( ((*it)+"_nominal").c_str());
  
    if( t==nullptr) { cout<<"Tree "<< *it<<" is null ptr skipping "<<endl; continue ;} 
    treeMap["signal"][*it]=t;
    // SignalRegion 
    unsigned int index=0;
    channelCat->setIndex(index);
    mapData[*(it)] = new RooDataSet(Form("dataset_%s",(*it).c_str()),"",vars,Import(*t),WeightVar(weight),
                                    Cut((channelCuts["signal"]).c_str()));

    //w->import(*mapData[*(it)]);
    
    //if((*it).compare("VBF")==0 || (*it).compare("GGF")==0 || (*it).compare("WW")==0){
      if((*it).compare("vbf")==0 || (*it).compare("ggf")==0 || (*it).compare("diboson")==0){
      //mapBDTCoeff[bdtClasses[*it]]->setVal(mapData[*(it)]->sumEntries());
      mapBDTNormCoeff[bdtClasses[*it]]->setVal(mapData[*(it)]->sumEntries());
      mapBDTNormCoeff[bdtClasses[*it]]->setConstant(true);
      cout<<"Dataset "<<*it<<endl;
      mapData[*(it)]->Print();
      mapBDTCoeff[bdtClasses[*it]]->Print();
      mapSRNormCoeff[*it]=mapBDTNormCoeff[bdtClasses[*it]];
      cout<<endl;
      //w->import(*mapBDTNormCoeff[bdtClasses[*it]]);
    }
    else {
      mapSRNormCoeff[(*it)] = new RooRealVar(Form("norm_SR_%s",(*it).c_str()),Form("#it{b}^{SR}_{#it%s}",(*it).c_str()),1,-1e10,+1e10);
      mapSRNormCoeff[(*it)] ->setVal(mapData[(*it)]->sumEntries());
      mapSRNormCoeff[(*it)] ->setConstant(true);
      //w->import(* mapSRNormCoeff[(*it)]);
    }
      
    cout<<"Normalisation for SR "<<(*it)<<" is "<<mapSRNormCoeff[(*it)]->getVal()<<endl;
    
  }

  double rhoBackground=2;
  double rho=1.0;
  // Create the RooKeysPDFs for the 3BDTs 
  // need to create a map for each axis
  std::map<string, std::map<string, RooKeysPdf*> > mapKeys;
  std::map<string, RooNDKeysPdf*> mapKeysND;

  // Create also simple histpdf3d
  std::map<string, RooDataHist*> mapDataHist;
  std::map<string, RooHistPdf*>  mapSigHistPdf;
  RooArgSet SigHistPdfSet;
  
  // create a map of TH3F 
  std::map<string, TH3F*> mapTH3FHists;


  
  std::map<string, RooNDKeysPdf*>  mapKeysBckg;
  std::map<string,std::map<string, RooKeysPdf*>>  mapKeysBckgCond;
  std::map<string, RooProdPdf*>   mapKeysBckgProdCond;
  // Now we need to build the backgrounds bdfs 
  vector <string> backgroundsConsider;
  backgroundsConsider.push_back("top"); 
  backgroundsConsider.push_back("Zjets"); 
  /*backgroundsConsider.push_back("FakeE"); backgroundsConsider.push_back("FakeM");*/ 
  //backgroundsConsider.push_back("Vgamma"); 
  //backgroundsConsider.push_back("vh");
 

  std::map<string, RooRealVar*> bckPars;
  std::map<string, RooFormulaVar*> fbckPars;

  RooArgSet bckParsArg;
  std::map<string, RooArgSet> bckCondMap;
  RooArgSet bckPdfs;
  // maka a map of each background to be added in product 
  std::map<string, RooArgSet> mapKeysArgCond;
  RooArgSet sumBackPdfArg;


   // Create also simple histpdf3d
  std::map<string, RooDataHist*> mapBckHist;
  std::map<string, RooHistPdf*>  maBckHistPdf;
  RooArgSet BckHistPdfSet;

  // create a map of TH3F 
  std::map<string, TH3F*> mapBckTH3;

  // now loop over all backgrounds 
  for(vector<string>::iterator it=backgroundsConsider.begin(); it!=backgroundsConsider.end(); it++){
    
    bckPars[*it] = new RooRealVar(Form("n_%s",(*it).c_str()),Form("#it{n}_%s",(*it).c_str()),0,1e10);
    // Incase we want to fit the number of events 
    //fbckPars[*it] = new RooFormulaVar(Form("f_%s",(*it).c_str()),Form("#it{f}_%s",(*it).c_str()),"@0*@1",RooArgSet(*bckPars[*it],lumiScaling));
    //Using mus 
    fbckPars[*it] = new RooFormulaVar(Form("f_%s",(*it).c_str()),Form("#it{f}_%s",(*it).c_str()),"@0*@1*@2",RooArgSet(*bckPars[*it],lumiScaling,*mapSRNormCoeff[(*it)]));

    //w->import(*bckPars[*it]);
    //w->import(*fbckPars[*it]);
    
    // now loop over all axis 
    RooDataSet *dataset=mapData[*it];
    bckPars[*it]->setVal(dataset->sumEntries());
    bckPars[*it]->setVal(1.0);
    bckPars[*it]->setConstant(true);
    cout<<"Background "<<*it<<endl; 
    bckPars[*it]->Print();
    //bckParsArg.add(*bckPars[*it]);
    if((!reparametrizeTrop || !hasBackground("top",backgroundsConsider)) || (reparametrizeTrop && (*it).compare("top")!=0))
      bckParsArg.add(*fbckPars[*it]);
    
    // build the 3d binned  version 
   
    if(do1D)
      mapBckTH3[(*it)]=(TH3F*)dataset->createHistogram(Form("th3_bckg_%s",(*it).c_str()),*mapBDTObs["bdt_vbf"],Binning(*mapBDTBins["bdt_vbf"]));
    else if(do2D)
      mapBckTH3[(*it)]=(TH3F*)dataset->createHistogram(Form("th3_bckg_%s",(*it).c_str()),*mapBDTObs["bdt_vbf"],Binning(*mapBDTBins["bdt_vbf"]),
                                                       YVar(*mapBDTObs["bdt_WW"],Binning(*mapBDTBins["bdt_WW"])));
    else 
      mapBckTH3[(*it)]=(TH3F*)dataset->createHistogram(Form("th3_bckg_%s",(*it).c_str()),*mapBDTObs["bdt_vbf"],Binning(*mapBDTBins["bdt_vbf"]),
                                                       YVar(*mapBDTObs["bdt_ggF"],Binning(*mapBDTBins["bdt_ggF"])),ZVar(*mapBDTObs["bdt_WW"],Binning(*mapBDTBins["bdt_WW"])));

    if(!autoHist3DBinning){
    //mapBckHist[(*it)]=new RooDataHist(Form("mapBckHist_%s",(*it).c_str()),Form("mapBckHist_%s",(*it).c_str()),mainObs, mapBckTH3[(*it)]);
      mapBckHist[(*it)]=new RooDataHist(Form("mapBckHist_%s",(*it).c_str()),Form("mapBckHist_%s",(*it).c_str()),mainObs, Import(*mapBckTH3[(*it)],true));
    }
    
    else{
      mapBckHist[(*it)]=new RooDataHist(Form("mapBckHist_%s",(*it).c_str()),Form("mapBckHist_%s",(*it).c_str()),
                                        mainObs,*dataset);
    }
    
    maBckHistPdf[(*it)]=new RooHistPdf(Form("mapBckHistPdf_%s",(*it).c_str()),Form("maBckHistPdf_%s",(*it).c_str()),
                                       mainObs,*mapBckHist[(*it)]);
    
    if((!reparametrizeTrop || !hasBackground("top",backgroundsConsider)) || (reparametrizeTrop && (*it).compare("top")!=0))
      BckHistPdfSet.add(*maBckHistPdf[(*it)]);

    if(fitCase!=0){
      // Build the keys pdfs
      mapKeysBckg[*it] = new RooNDKeysPdf(Form("bckg_NDKeys_%s",(*it).c_str()),Form("bckg_NDKeys_%s",(*it).c_str()),mainObs,*dataset,rhoBackground);
      bckPdfs.add(*mapKeysBckg[*it]);
      // loop over all axis
      //for(std::map<string,string>::iterator it2 = bdtClasses.begin(); it2!=bdtClasses.end(); it2++){
      for(vector<string>::iterator it2 = bdtDiminesions.begin(); it2!=bdtDiminesions.end(); it2++){
        
        mapKeysBckgCond[*it][(*it2)]=new RooKeysPdf(Form("Keys_bck_%s_%s",(*it).c_str(),(*it2).c_str()),Form("Keys_bck_%s_%s",(*it).c_str(),(*it2).c_str()),
                                                    *mapBDTObs[(*it2)],*dataset,RooKeysPdf::NoMirror,rhoBackground);
        mapKeysArgCond[*it].add(*mapKeysBckgCond[*it][(*it2)]);
      }
      
      // Make the Prod cond KeyspDF  // Add conditionality .. ? 
      mapKeysBckgProdCond[*it]=new RooProdPdf(Form("Prod_bckCondKeys_%s",(*it).c_str()),Form("Prod_bckCondKeys_%s",(*it).c_str()),mapKeysArgCond[*it]);//
      //Conditional(*mapKeysBckgCond[*it]["bdt_vbf"],bdt_vbf));
      
      if((!reparametrizeTrop || !hasBackground("top",backgroundsConsider)) || (reparametrizeTrop && (*it).compare("top")!=0))
        sumBackPdfArg.add(*mapKeysBckgProdCond[*it]);
    }}
  
  // Here treat the case where Top is reparametized 
  RooFormulaVar tmpProd_f_WW("tmpProd_f_diboson","tmpProd_f_diboson","@0*@1*@2",RooArgSet(*n_WW,*norm_WW,lumiScaling));
  if(reparametrizeTrop && hasBackground("top",backgroundsConsider)){
    delete f_WW;
    f_WW=new RooFormulaVar("f_diboson","#it{f}_{#it{diboson}}","@0+@1",RooArgSet(tmpProd_f_WW,*fbckPars["top"]));
  }
  // Make the 3D Hist Background PDF 
  RooAddPdf *bckdHist3DPdf=new RooAddPdf("bckdHist3DPdf","bckdHist3DPdf",BckHistPdfSet,bckParsArg);
  cout<<"3D background hist  pdf "<<endl;
  //w->import(*bckdHist3DPdf);
  bckdHist3DPdf->Print();

  // Make the NDKeys PDF
  RooAddPdf *bcktotPds=nullptr;
  if(fitCase!=0){
    bcktotPds=new RooAddPdf("bcktotPds","bcktotPds",bckPdfs,bckParsArg);
    cout<<" Background Keys  pdf"<<endl;
    bcktotPds->Print();
  }
  
  // Make the sum of Kyes PDFs 
  RooAddPdf *bcktotPdsKeysProd=nullptr;
  if(fitCase!=0)
    bcktotPdsKeysProd=new RooAddPdf("bcktotPdsKeysProd","bcktotPdsKeysProd",sumBackPdfArg,bckParsArg);
 

  // Declare obserbavles for the CR 

  std::map<string,RooRealVar*> CRchannelObs;
  //CRchannelObs["ggFCRNj2"]=&MT;
  CRchannelObs["ggFCRNj2"]=bdt_ggF;
  //CRchannelObs["ggFCRNj1"]=&MT;
  CRchannelObs["ggFCRNj1"]=bdt_ggF;
  CRchannelObs["ggFCRNj0"]=&MT;
  //CRchannelObs["TopCR"]=&MT;
  CRchannelObs["TopCR"]=bdt_Top;
  CRchannelObs["ZjetsCR"]=&MT;

  std::map<string,RooRealVar*> nps;
  std::map<string,RooRealVar*> npsGLob;
  std::map<string,RooRealVar*> sigmaNP;
  std::map<string,RooGaussian*> constraints;
  std::map<string, RooRealVar*> uncertaintyPerCat;
  RooArgSet constraintsSet;
  RooArgSet npList;
  RooArgSet GlobalObs;
  
  if(addUncertaintyOnNjets){
    nps["nJetSys"]=new RooRealVar("hat_alpha_nJetSys","#hat{alpha}_{nJetSys}",0,-5,+5);
    npList.add(*nps["nJetSys"]);
    nps["nJetSys"]->setConstant(true);
    npsGLob["nJetSys"]=new RooRealVar("alpha_nJetSys","#alpha_{nJetSys}",0,-5,+5);
    npsGLob["nJetSys"]->setConstant(true);
    GlobalObs.add(*npsGLob["nJetSys"]);
    sigmaNP["nJetSys"]=new RooRealVar("sigma_nJetSys","#sigma_{nJetSys}",1);
    sigmaNP["nJetSys"]->setConstant(true);
    constraints["nJetSys"]=new RooGaussian("G_nJetSys","#it{G}_{nJetSys}",*npsGLob["nJetSys"],*nps["nJetSys"],*sigmaNP["nJetSys"]);
    constraintsSet.add(*constraints["nJetSys"]);
    uncertaintyPerCat["ggFCRNj0"]=new RooRealVar("r_ggFCRNj0","r_ggFCRNj0",1.00);
    uncertaintyPerCat["ggFCRNj1"]=new RooRealVar("r_ggFCRNj1","r_ggFCRNj1",1.00);
    uncertaintyPerCat["ggFCRNj2"]=new RooRealVar("r_ggFCRNj2","r_ggFCRNj2",1.00);
  }
  
  std::map<string,std::map<string, RooDataSet*>> mapCRData;
  
  // Create also simple histpdf3d for the CR
  std::map<string, std::map<string, RooDataHist*>> mapDataCRHist;
  std::map<string, std::map<string, RooHistPdf*>>  mapCRHistPdf;
  
  std::map<string, std::map<string,RooRealVar*>> mapCRNormCoeff;
  std::map<string, std::map<string,RooFormulaVar*>> mapCRTrasnferFact;

  std::map<string,std::map<string, std::map<string,RooFormulaVar*>>> mapAllTransferFacts;
  
  std::map<string, RooFormulaVar*> f_CR_ggF;
  std::map<string, RooFormulaVar*> f_CR_WW;
  std::map<string, RooFormulaVar*> f_CR_vbf;

  std::map<string, RooFormulaVar*> f_CR_SystSum_ggF;
  std::map<string, RooFormulaVar*> f_CR_SystSum_WW;
  std::map<string, RooFormulaVar*> f_CR_SystSum_vbf;
  

  std::map<string, RooRealVar*> n_CR_ggF;
  std::map<string, RooRealVar*> n_CR_WW;
  std::map<string, RooRealVar*> n_CR_vbf;

  std::map<string, RooArgSet>  CRSignalPars;
  std::map<string, RooArgSet>  CRSigHistPdfSet;
  std::map <string,  RooAddPdf*> CRsignalHistPdf;

  // Backgrounds
  std::map <string, RooArgSet> CRBckNormSet;
  std::map<string, RooArgSet> CRBckPdfSet;
  std::map<string, std::map<string,RooFormulaVar*>> CRBckNorm;
  
  std::map<string, RooAddPdf*> CRbckgHistPdf;
  
  // total pdfs
  std::map<string,RooAddPdf*> totCRpdf; 
  
  std::map<string,RooArgSet> varsCR;

  // observables in the 2 jet bin 
  varsCR["ggFCRNj2"].add(vars);
  varsCR["ggFCRNj2"].add(*CRchannelObs["ggFCRNj2"]); varsCR["ggFCRNj2"].add(nJetsTight); varsCR["ggFCRNj2"].add(weight);

  // observables in the 0 jet bin 
  varsCR["ggFCRNj0"].add(*CRchannelObs["ggFCRNj0"]); varsCR["ggFCRNj0"].add(nJetsTight); varsCR["ggFCRNj0"].add(weight);

  // observables in the 1 jet bin 
  //varsCR["ggFCRNj1"].add(*CRchannelObs["ggFCRNj1"]); varsCR["ggFCRNj1"].add(nJetsTight); varsCR["ggFCRNj1"].add(weight);

  // obserbables for the Nj=2 
  varsCR["ggFCRNj1"].add(vars);
  varsCR["ggFCRNj1"].add(*CRchannelObs["ggFCRNj1"]); varsCR["ggFCRNj1"].add(nJetsTight); varsCR["ggFCRNj1"].add(weight);

  // observables for Top
  varsCR["TopCR"].add(*CRchannelObs["TopCR"]); varsCR["TopCR"].add(vars); varsCR["TopCR"].add(weight);

  varsCR["ZjetsCR"].add(*CRchannelObs["ZjetsCR"]); varsCR["ZjetsCR"].add(vars); varsCR["ZjetsCR"].add(weight);
  

  // Loop over all control regions
  for(std::map<string,string>::iterator cr=channelFiles.begin(); cr!=channelFiles.end(); cr++){
    if( (*cr).first.compare("signal")==0 ) continue; 
    TFile *fcr=TFile::Open((*cr).second.c_str());
    
    string crName=(*cr).first;
    // Load the tree
    for( vector <string>::iterator it=treeNames.begin(); it!=treeNames.end(); it++){

      TTree *t=nullptr;//(TTree*)fcr->Get((*it).c_str());
      t=(TTree*)fcr->Get(((*it)+"_nominal").c_str());
      if( t==nullptr) { cout<<"Tree "<< *it<<" is null ptr skipping "<<endl; continue ;} 
      treeMap[crName][*it]=t;

      unsigned int index=1+std::distance(channelFiles.begin(),cr);
      channelCat->setIndex(index);
      mapCRData[crName][(*it)] = new RooDataSet(Form("dataset_CR_%s",(*it).c_str()),"",varsCR[crName],Import(*t),WeightVar(weight),
                                        Cut((channelCuts[(*cr).first]).c_str()));

      // create the histogram 
      mapDataCRHist[crName][(*it)]=new RooDataHist(Form("mapDataCRHist_%s",(*it).c_str()),Form("mapDataCR_%s_Hist_%s",crName.c_str(),(*it).c_str()),*CRchannelObs[crName],*mapCRData[crName][(*it)]);
      // create the pdf 
      mapCRHistPdf[crName][(*it)]= new RooHistPdf(Form("mapCRHistPdf_%s",(*it).c_str()),Form("mapCR_%s_HistPdf_%s",crName.c_str(),(*it).c_str()),*CRchannelObs[crName],*mapDataCRHist[crName][(*it)]);

      //* Need to ad NormFactor for each contribution and Trasnfer factors from SR to CR
      if((*it).compare("data")!=0){
        
        mapCRNormCoeff[crName][(*it)] = new RooRealVar(Form("norm_CR_%s_%s",crName.c_str(),(*it).c_str()),Form("#it{b}^{%s}_{#it%s}",crName.c_str(),(*it).c_str()),1,-1e8,+1e8);

        mapCRNormCoeff[crName][(*it)] ->setVal(mapCRData[crName][(*it)]->sumEntries());
        mapCRNormCoeff[crName][(*it)] ->setConstant(true);

        // find corresponing coeff 
        RooRealVar *srCoeff= mapSRNormCoeff[(*it)];
        
        mapCRTrasnferFact[crName][(*it)] = new RooFormulaVar(Form("trasns_CR_%s_%s",crName.c_str(),(*it).c_str()),Form("#it{t}^{%s}_{#it%s}",crName.c_str(),(*it).c_str()),"@0/@1",RooArgSet(*mapCRNormCoeff[crName][(*it)],*srCoeff));
        
        cout<<"Dataset "<<*it<<endl;
        mapCRData[crName][*(it)]->Print();
        mapCRNormCoeff[crName][(*it)]->Print();
        mapCRTrasnferFact[crName][(*it)]->Print();
        cout<<endl;
        
        cout<<"Normalisation for CR "<<(*it)<<" is "<<mapCRNormCoeff[crName][(*it)]->getVal()<<endl;
      }
    }
    fcr->Close();
    delete fcr;

    
    // Now create coeefficient for each: mu*NormFactor or mu*NormFactor*Trasn
    // Needs to be put into an map;
    // include the uncertainty terms
    cout<<"Adding nuisance parameters response"<<endl;
    std::map<string,string> formula;
    std::map<string,RooArgSet*> npForm;
    vector <string> kind;
    kind.push_back("diboson");  kind.push_back("vbf");  kind.push_back("ggf");
    
    for(vector<string>::iterator sk=kind.begin(); sk!=kind.end(); sk++){
      cout<<"Doing it for "<<*sk<<endl;
      npForm[*sk]=new RooArgSet();
      formula[*sk]="(1";
      int ictr=0;
      if( (*sk).compare("ggf")==0 && (crName.compare("ggFCRNj1")==0 || crName.compare("ggFCRNj0")==0 || crName.compare("ggFCRNj2")==0)){
        for(std::map<string, RooRealVar*>::iterator ctr=nps.begin(); ctr!=nps.end(); ctr++){
          cout<<" -- > "<<crName<<endl;
          uncertaintyPerCat[crName]->Print();
          cout<<"<------> "<<endl;
          formula[*sk]+=Form("*(1+@%d)*@%d",ictr+1,ictr);
          npForm[*sk]->add(*uncertaintyPerCat[crName]);
          npForm[*sk]->add(*(*ctr).second);
          ictr++; ictr++;
        }}
      formula[*sk]+=")";
    }


    f_CR_SystSum_WW[crName]=new RooFormulaVar(Form("f_CR_SystSum%s_diboson",crName.c_str()),Form("#it{r}^{%s}_{#it{WW}}",crName.c_str()),formula["diboson"].c_str(),*npForm["diboson"]);
    f_CR_SystSum_WW[crName]->Print();
    f_CR_SystSum_vbf[crName]=new RooFormulaVar(Form("f_CR_SystSum%s_vbf",crName.c_str()),Form("#it{r}^{%s}_{#it{vbf}}",crName.c_str()),formula["vbf"].c_str(),*npForm["vbf"]);
    f_CR_SystSum_vbf[crName]->Print();
    f_CR_SystSum_ggF[crName]=new RooFormulaVar(Form("f_CR_SystSum%s_ggf",crName.c_str()),Form("#it{r}^{%s}_{#it{ggF}}",crName.c_str()),formula["ggf"].c_str(),*npForm["ggf"]);
    f_CR_SystSum_ggF[crName]->Print();
    //if(crName.compare("ggFCRNj1")==0 || crName.compare("ggFCRNj0")==0 || crName.compare("ggFCRNj2")==0)
    
    
    //f_CR_ggF[crName]=new RooFormulaVar(Form("f_CR_%s_ggF",crName.c_str()),Form("#it{f}^{%s}_{#it{gg}F}",crName.c_str()),"@0*@1",RooArgSet(*n_ggF,*mapCRTrasnferFact[crName]["GGF"]));
    f_CR_WW[crName]=new RooFormulaVar(Form("f_CR_%s_diboson",crName.c_str()),Form("#it{f}^{%s}_{#it{WW}}",crName.c_str()),"@0*@1*@2",RooArgSet(*n_WW,*mapCRTrasnferFact[crName]["diboson"],*f_CR_SystSum_WW[crName]));
    f_CR_vbf[crName]=new RooFormulaVar(Form("f_CR_%s_vbf",crName.c_str()),Form("#it{f}^{%s}_{vbf}",crName.c_str()),"@0*@1*@2",RooArgSet(*n_vbf,*mapCRTrasnferFact[crName]["vbf"],*f_CR_SystSum_vbf[crName]));
    
    n_CR_ggF[crName]=new RooRealVar(Form("n_CR_%s_ggf",crName.c_str()),Form("n_%s_ggf",crName.c_str()),1,0,1e6);
    //n_CR_ggF[crName]->setConstant(true);
    n_CR_WW[crName]=new RooRealVar(Form("n_CR_%s_diboson",crName.c_str()),Form("n_%s_diboson",crName.c_str()),1,0,1e6);
    n_CR_WW[crName]->setConstant(true);
    n_CR_vbf[crName]=new RooRealVar(Form("n_CR_%s_vbf",crName.c_str()),Form("n_%s_vbf",crName.c_str()),1,0,1e6);
    n_CR_vbf[crName]->setConstant(true);

    
    // Dedicated pois only for the ggF CR
    if(crName.compare("ggFCRNj1")==0 || crName.compare("ggFCRNj0")==0 || crName.compare("ggFCRNj2")==0)
      f_CR_ggF[crName]=new RooFormulaVar(Form("f_CR_%s_ggF",crName.c_str()),Form("#it{f}^{%s}_{#it{gg}F}",crName.c_str()),"@0*@1*@2",RooArgSet(*n_CR_ggF[crName],*mapCRNormCoeff[crName]["ggf"],*f_CR_SystSum_ggF[crName]));
    else {
      f_CR_ggF[crName]=new RooFormulaVar(Form("f_CR_%s_ggF",crName.c_str()),Form("#it{f}^{%s}_{#it{gg}F}",crName.c_str()),"@0*@1*@2",RooArgSet(*n_CR_ggF[crName],*mapCRNormCoeff[crName]["ggf"],*f_CR_SystSum_ggF[crName]));
      //f_CR_ggF[crName]=new RooFormulaVar(Form("f_CR_%s_ggF",crName.c_str()),Form("#it{f}^{%s}_{#it{gg}F}",crName.c_str()),"@0*@1*@2",RooArgSet(f_ggF,*mapCRNormCoeff[crName]["GGF"],*f_CR_SystSum_ggF[crName]));
      n_CR_ggF[crName]->setConstant(true);
      f_CR_ggF[crName]->Print();
    }
    
    //if((crName.compare("ggFCRNj1")==0 || crName.compare("ggFCRNj0")==0 || crName.compare("ggFCRNj2")==0))
    // return ;
    
    
    //RooFormulaVar f_CR_ggF("f_CR_ggF","#it{f}^{CR}_{#it{gg}F}","@0*@1*1/@2",RooArgSet(n_ggF,*mapCRNormCoeff["GGF"],*mapSRNormCoeff["GGF"]));
    //RooFormulaVar f_CR_WW("f_CR_WW","#it{f}^{CR}_{#it{WW}}","@0*@1",RooArgSet(n_CR_WW,*mapCRNormCoeff["WW"]));
    //RooFormulaVar f_CR_vbf("f_CR_vbf","#it{f}^{CR}_{VBF}","@0*@1",RooArgSet(n_CR_vbf,*mapCRNormCoeff["VBF"]));
    
    // Add the amplitudes 
    CRSignalPars[crName].add(*f_CR_ggF[crName]);
    CRSignalPars[crName].add(*f_CR_WW[crName]);
    CRSignalPars[crName].add(*f_CR_vbf[crName]);

    // Add the pdfs 
    CRSigHistPdfSet[crName].add(*mapCRHistPdf[crName]["ggf"]);
    CRSigHistPdfSet[crName].add(*mapCRHistPdf[crName]["diboson"]);
    CRSigHistPdfSet[crName].add(*mapCRHistPdf[crName]["vbf"]);
    
    CRsignalHistPdf[crName] = new RooAddPdf(Form("CRsignalHistPdf_%s",crName.c_str()),Form("CRsignalHistPdf_%s",crName.c_str()),CRSigHistPdfSet[crName],CRSignalPars[crName]);
    CRsignalHistPdf[crName]->Print();
    
    // Now do the same for the backgrounds to consider
    for(vector<string>::iterator it=backgroundsConsider.begin(); it!=backgroundsConsider.end(); it++){
      CRBckNorm[crName][(*it)]=new RooFormulaVar(Form("f_CR_%s_%s",crName.c_str(),(*it).c_str()),Form("f_CR_%s_%s",crName.c_str(),(*it).c_str()),
                                                 "@0*@1",RooArgSet(*fbckPars[*it],*mapCRTrasnferFact[crName][*it]));
      CRBckNormSet[crName].add(*CRBckNorm[crName][*it]);
      CRBckPdfSet[crName].add(*mapCRHistPdf[crName][(*it)]);
    }
    
    CRbckgHistPdf[crName]=new RooAddPdf(Form("CRbckgHistPdf_%s",crName.c_str()),Form("CRbckgHistPdf_%s",crName.c_str()),CRBckPdfSet[crName],CRBckNormSet[crName]);
    CRbckgHistPdf[crName]->Print();
    
    totCRpdf[crName]=new RooAddPdf(Form("totCRpdf_%s",crName.c_str()),Form("totCRpdf_%s",crName.c_str()),RooArgSet(*CRsignalHistPdf[crName],*CRbckgHistPdf[crName]));
    totCRpdf[crName]->Print();
  }
  cout<<"All good "<<endl;

  // Here re parametrize the GGF mu
  //RooFormulaVar *tmpVar1=new RooFormulaVar("tmpVar1","tmpVar1","@0*@1/@2",RooArgList(*n_CR_ggF["ggFCRNj0"],*n_CR_ggF["ggFCRNj1"],*n_CR_ggF["ggFCRNj2"]));

  //RooFormulaVar *tmpVar1=new RooFormulaVar("tmpVar1","tmpVar1","(@0*@1)/@2*@3",RooArgList(*n_CR_ggF["ggFCRNj1"],*n_CR_ggF["ggFCRNj2"],*n_CR_ggF["ggFCRNj0"],*norm_ggF));

  RooFormulaVar *tmpVar1=new RooFormulaVar("tmpVar1","tmpVar1","(@0*@1/@2)*(@3/(@4*@5))*@6",RooArgSet(*f_CR_ggF["ggFCRNj1"],*f_CR_ggF["ggFCRNj2"],*f_CR_ggF["ggFCRNj0"],*mapCRNormCoeff["ggFCRNj0"]["ggf"],*mapCRNormCoeff["ggFCRNj1"]["ggf"],*mapCRNormCoeff["ggFCRNj2"]["ggf"],*norm_ggF));

  //RooFormulaVar *tmpVar1=new RooFormulaVar("tmpVar1","tmpVar1","(@0*@1/@2)*(@3/(@4*@5))*@6",RooArgSet(*f_CR_ggF["ggFCRNj0"],*f_CR_ggF["ggFCRNj1"],*f_CR_ggF["ggFCRNj2"],*mapCRNormCoeff["ggFCRNj2"]["GGF"],*mapCRNormCoeff["ggFCRNj1"]["GGF"],*mapCRNormCoeff["ggFCRNj0"]["GGF"],*norm_ggF));

  //RooFormulaVar *tmpVar1=new RooFormulaVar("tmpVar1","tmpVar1","(@0*@1/@2)",RooArgSet(*f_CR_ggF["ggFCRNj0"],*f_CR_ggF["ggFCRNj1"],*f_CR_ggF["ggFCRNj2"]));
  //RooFormulaVar *tmpVar1=new RooFormulaVar("tmpVar1","tmpVar1","(@0*@1/@2)",RooArgSet(*f_CR_ggF["ggFCRNj1"],*f_CR_ggF["ggFCRNj2"],*f_CR_ggF["ggFCRNj0"]));
  
  //RooFormulaVar tmpVar1("tmpVar1","tmpVar1","@0*@1/@2",RooArgList(*f_CR_ggF["ggFCRNj2"],*f_CR_ggF["ggFCRNj0"],*f_CR_ggF["ggFCRNj1"]));

  //RooFormulaVar tmpVar1("tmpVar1","tmpVar1","@0*@1/@2",RooArgList(*n_CR_ggF["ggFCRNj1"],*n_CR_ggF["ggFCRNj0"],*n_CR_ggF["ggFCRNj2"]));
  //-->Latest One 
  //RooFormulaVar tmpVar1("tmpVar1","tmpVar1","@0*@1/@2",RooArgList(*n_CR_ggF["ggFCRNj1"],*n_CR_ggF["ggFCRNj2"],*n_CR_ggF["ggFCRNj0"]));

  //RooFormulaVar tmpVar1("tmpVar1","tmpVar1","@0*@1/@2",RooArgList(*n_CR_ggF["ggFCRNj2"],*n_CR_ggF["ggFCRNj0"],*n_CR_ggF["ggFCRNj1"]));
  //RooFormulaVar tmpVar1("tmpVar1","tmpVar1","@0 * @1 / @2",RooArgList(*f_CR_ggF["ggFCRNj2"],*f_CR_ggF["ggFCRNj0"],*f_CR_ggF["ggFCRNj1"]));
  tmpVar1->Print();
  //otherScaling=&tmpVar1;
  //RooFormulaVar *f_ggF=new RooFormulaVar("f_ggF","{f}_{#it{gg}F}","@0*@1*@2",RooArgSet(*tmpVar1,lumiScaling,*norm_ggF));
  //RooFormulaVar *f_ggF=new RooFormulaVar("f_ggF","{f}_{#it{gg}F}","@0*@1",RooArgSet(*tmpVar1,lumiScaling));

  RooFormulaVar *f_ggF=new RooFormulaVar("f_ggF","{f}_{#it{gg}F}","@0*(@1*@2/@3)*(@4/(@5*@6))*@7",RooArgSet(lumiScaling,*f_CR_ggF["ggFCRNj1"],*f_CR_ggF["ggFCRNj2"],*f_CR_ggF["ggFCRNj0"],*mapCRNormCoeff["ggFCRNj0"]["ggf"],*mapCRNormCoeff["ggFCRNj1"]["ggf"],*mapCRNormCoeff["ggFCRNj2"]["ggf"],*norm_ggF));

  f_ggF->Print();
  mapBDTFCoeff["bdt_ggF"]=f_ggF;

  //n_ggF=RooFormulaVar("f_ggF","#it{f}_{#it{gg}F}","@0 * @1* @2",RooArgList(lumiScaling,*mapBDTNormCoeff["bdt_ggF"],tmpVar1));
  pois.add(*n_CR_ggF["ggFCRNj1"]); pois.add(*n_CR_ggF["ggFCRNj0"]); pois.add(*n_CR_ggF["ggFCRNj2"]); 
  pois.add(*bckPars["top"]);
  //n_ggF->setConstant(true);


  // Build the signal pdf:

  
  //RooPlot *xplot=bdt_ggF.frame();
  //for(map<string,RooDataSet*>::iterator it=mapData.begin(); it!=mapData.end(); it++)
  //(*it).second->plotOn(xplot,Name((*it).first.c_str()));
  //TCanvas *cc=new TCanvas("cc","");{
  //xplot->Draw();
  //  }
  // loop on the components
  for(std::map<string,string>::iterator it = bdtClasses.begin(); it!=bdtClasses.end(); it++){
    
    cout<<"Looping over compoenent "<<(*it).first<<endl;
    // loop on the axis 
    RooDataSet *dataset=mapData[(*it).first];
    
    mapTH3FHists[(*it).first]=nullptr;
    
    if(do1D)
      mapTH3FHists[(*it).first]=(TH3F*)dataset->createHistogram(Form("th3_signal_%s",(*it).first.c_str()),*mapBDTObs["bdt_vbf"],Binning(*mapBDTBins["bdt_vbf"]));
    else if(do2D) 
      mapTH3FHists[(*it).first]=(TH3F*)dataset->createHistogram(Form("th3_signal_%s",(*it).first.c_str()),*mapBDTObs["bdt_vbf"],Binning(*mapBDTBins["bdt_vbf"]),
                                                                YVar(*mapBDTObs["bdt_WW"],Binning(*mapBDTBins["bdt_WW"])));
    else 
      mapTH3FHists[(*it).first]=(TH3F*)dataset->createHistogram(Form("th3_signal_%s",(*it).first.c_str()),*mapBDTObs["bdt_vbf"],Binning(*mapBDTBins["bdt_vbf"]),
                                                                YVar(*mapBDTObs["bdt_ggF"],Binning(*mapBDTBins["bdt_ggF"])),ZVar(*mapBDTObs["bdt_WW"],Binning(*mapBDTBins["bdt_WW"])));

    //w->import(*(TH3F*)mapTH3FHists[(*it).first]);
    
    if(!autoHist3DBinning){
      mapDataHist[(*it).first]=new RooDataHist(Form("mapDataHist_%s",(*it).first.c_str()),Form("mapDataHist_%s",(*it).first.c_str()),mainObs,Import(*mapTH3FHists[(*it).first],true));
      //mapDataHist[(*it).first]=new RooDataHist(Form("mapDataHist_%s",(*it).first.c_str()),Form("mapDataHist_%s",(*it).first.c_str()),mainObs, mapTH3FHists[(*it).first]);
    }

    else{
      mapDataHist[(*it).first]=new RooDataHist(Form("mapDataHist_%s",(*it).first.c_str()),Form("mapDataHist_%s",(*it).first.c_str()),mainObs,*dataset);
                                               
    }

    //w->import(*mapDataHist[(*it).first],RecycleConflictNodes());
    
    mapSigHistPdf[(*it).first]=new RooHistPdf(Form("mapSigHistPdf_%s",(*it).first.c_str()),Form("mapSigHistPdf_%s",(*it).first.c_str()),
                                              mainObs,*mapDataHist[(*it).first]);
    //w->import(*mapSigHistPdf[(*it).first],RecycleConflictNodes());
    if(fitCase!=0){
      for(vector<string>::iterator it2 = bdtDiminesions.begin(); it2!=bdtDiminesions.end(); it2++){
        cout<<"--> Axis "<<(*it2)<<endl;
        cout<<"Extracting pdf from dataset "<<dataset->GetName()<<" with entries "<<dataset->numEntries()<<endl;
        mapKeys[(*it).first][(*it2)]=new RooKeysPdf(Form("Keys_%s_%s",(*it).first.c_str(),(*it2).c_str()),Form("Keys_%s_%s",(*it).first.c_str(),(*it2).c_str()),
                                                    *mapBDTObs[(*it2)],*dataset,RooKeysPdf::NoMirror,rho);
        cout<<"Axis "<<(*it).first<<" compoenent "<<(*it2)<<" PDF "<<endl;
        mapKeys[(*it).first][(*it2)]->Print();
      }
      mapKeysND[(*it).first]= new RooNDKeysPdf(Form("ND_Keys_%s",(*it).first.c_str()),Form("ND_Keys_%s",(*it).first.c_str()),mainObs,*dataset,rho);
    }
  }
  
  // P(x,y,z) = P(x ; P(x)*P(y,z)) =P(x ; P(x)*P(y;z)*P(z;y))
  // build the VBF 3D pdf; 
  // Partial conditional 
  /*
  RooProdPdf *partCond_1=new RooProdPdf("partCond_1","partCond_1",RooArgSet(*mapKeys["VBF"]["bdt_ggF"],*mapKeys["VBF"]["bdt_WW"]),Conditional(*mapKeys["VBF"]["bdt_WW"],bdt_WW));
  //RooProdPdf *partCond_2=new RooProdPdf("partCond_2","partCond_2",RooArgSet(*mapKeys["VBF"]["bdt_WW"],*mapKeys["VBF"]["bdt_vbf"]),Conditional(*mapKeys["VBF"]["bdt_vbf"],bdt_vbf));
  RooProdPdf *vbf3D = new RooProdPdf("vbf3D","vbf3D",RooArgSet(*partCond_1,*mapKeys["VBF"]["bdt_vbf"]),Conditional(*mapKeys["VBF"]["bdt_vbf"],bdt_vbf));
  vbf3D->Print();
  
  
  RooProdPdf *partCond_3=new RooProdPdf("partCond_3","partCond_3",RooArgSet(*mapKeys["WW"]["bdt_ggF"],*mapKeys["WW"]["bdt_vbf"]),Conditional(*mapKeys["WW"]["bdt_ggF"],bdt_ggF));
  // RooProdPdf *partCond_4=new RooProdPdf("partCond_4","partCond_4",RooArgSet(*mapKeys["WW"]["bdt_vbf"],*mapKeys["WW"]["bdt_WW"]),Conditional(*mapKeys["WW"]["bdt_WW"],bdt_WW));
  RooProdPdf *ww3D=new RooProdPdf("ww3D","ww3D",RooArgSet(*partCond_3,*mapKeys["WW"]["bdt_WW"]), Conditional(*mapKeys["WW"]["bdt_WW"],bdt_WW));
  ww3D->Print();
                                 
  RooProdPdf *partCond_5=new RooProdPdf("partCond_5","partCond_5",RooArgSet(*mapKeys["GGF"]["bdt_vbf"],*mapKeys["GGF"]["bdt_WW"]),Conditional(*mapKeys["GGF"]["bdt_vbf"],bdt_vbf));
  //RooProdPdf *partCond_6=new RooProdPdf("partCond_6","partCond_6",RooArgSet(*mapKeys["GGF"]["bdt_WW"],*mapKeys["GGF"]["bdt_ggF"]),Conditional(*mapKeys["GGF"]["bdt_vbf"],bdt_ggF));
  RooProdPdf *ggF3D=new RooProdPdf("ggF3D","ggF3D",RooArgSet(*partCond_5,*mapKeys["GGF"]["bdt_ggF"]),Conditional(*mapKeys["GGF"]["bdt_ggF"],bdt_ggF));
  ggF3D->Print();
  */

  RooProdPdf *vbf3D=nullptr;
  RooProdPdf *ww3D=nullptr; 
  RooProdPdf *ggF3D=nullptr;

  if(fitCase!=0){
  if(do1D){
    vbf3D = new RooProdPdf("vbf3D","vbf3D",RooArgSet(*mapKeys["vbf"]["bdt_vbf"]));
    ww3D=new RooProdPdf("ww3D","ww3D",RooArgSet(*mapKeys["diboson"]["bdt_vbf"]));
    ggF3D=new RooProdPdf("ggF3D","ggF3D",RooArgSet(*mapKeys["ggf"]["bdt_vbf"]));
  }
  else if(do2D){
    vbf3D = new RooProdPdf("vbf3D","vbf3D",RooArgSet(*mapKeys["vbf"]["bdt_vbf"],*mapKeys["vbf"]["bdt_WW"]),Conditional(*mapKeys["vbf"]["bdt_vbf"],*bdt_vbf));
    ww3D=new RooProdPdf("ww3D","ww3D",RooArgSet(*mapKeys["diboson"]["bdt_vbf"],*mapKeys["diboson"]["bdt_WW"]),Conditional(*mapKeys["diboson"]["bdt_WW"],*bdt_WW));
    ggF3D=new RooProdPdf("ggF3D","ggF3D",RooArgSet(*mapKeys["ggf"]["bdt_vbf"],*mapKeys["ggf"]["bdt_WW"]), Conditional(*mapKeys["ggf"]["bdt_WW"],*bdt_WW));
  }
  
  else {
    vbf3D = new RooProdPdf("vbf3D","vbf3D",RooArgSet(*mapKeys["vbf"]["bdt_vbf"],*mapKeys["vbf"]["bdt_ggF"],*mapKeys["vbf"]["bdt_WW"]),Conditional(*mapKeys["vbf"]["bdt_vbf"],*bdt_vbf));
    
    ww3D=new RooProdPdf("ww3D","ww3D",RooArgSet(*mapKeys["diboson"]["bdt_vbf"],*mapKeys["diboson"]["bdt_ggF"],*mapKeys["diboson"]["bdt_WW"]),Conditional(*mapKeys["diboson"]["bdt_WW"],*bdt_WW));
    
    
    ggF3D=new RooProdPdf("ggF3D","ggF3D",RooArgSet(*mapKeys["ggf"]["bdt_vbf"],*mapKeys["ggf"]["bdt_ggF"],*mapKeys["ggf"]["bdt_WW"]),Conditional(*mapKeys["ggf"]["bdt_ggF"],*bdt_ggF));
  }
  }

  /*
  // Integrate over the same observable: 
  RooProdPdf *vbf3D = new RooProdPdf("vbf3D","vbf3D",RooArgSet(*mapKeys["VBF"]["bdt_vbf"],*mapKeys["VBF"]["bdt_ggF"],*mapKeys["VBF"]["bdt_WW"]),
                                     Conditional(*mapKeys["VBF"]["bdt_vbf"],bdt_vbf));
  RooProdPdf *ww3D=new RooProdPdf("ww3D","ww3D",RooArgSet(*mapKeys["WW"]["bdt_vbf"],*mapKeys["WW"]["bdt_ggF"],*mapKeys["WW"]["bdt_WW"]),
                                Conditional(*mapKeys["WW"]["bdt_vbf"],bdt_vbf));
  RooProdPdf *ggF3D=new RooProdPdf("ggF3D","ggF3D",RooArgSet(*mapKeys["GGF"]["bdt_vbf"],*mapKeys["GGF"]["bdt_ggF"],*mapKeys["GGF"]["bdt_WW"]),
                                   Conditional(*mapKeys["GGF"]["bdt_vbf"],bdt_vbf));
  */

  // Now Make the addition of all compoents 
  RooArgSet SignalPars;
  SignalPars.add(*f_vbf);
  SignalPars.add(*f_WW);
  SignalPars.add(*f_ggF);

  SigHistPdfSet.add(*mapSigHistPdf["vbf"]);
  SigHistPdfSet.add(*mapSigHistPdf["diboson"]);
  SigHistPdfSet.add(*mapSigHistPdf["ggf"]);

  // Conditional model here 
  // build the signal p.d.f 
  RooArgSet SignalModelSet;
  if(fitCase!=0){
    SignalModelSet.add(*vbf3D);
    SignalModelSet.add(*ww3D);
    SignalModelSet.add(*ggF3D);
  }

  RooAddPdf *signalPdf=nullptr;
  //new RooAddPdf("signalPdf","signalPdf",SignalModelSet,SignalPars);
  //signalPdf->Print();
  //w->import(*signalPdf,RecycleConflictNodes());
  //Make the addition of the ND Keys PDF:
  
  RooArgSet keysNDset;
  if(fitCase!=0){
    keysNDset.add(*mapKeysND["vbf"]);
    keysNDset.add(*mapKeysND["diboson"]);
    keysNDset.add(*mapKeysND["ggf"]);
  }

  RooAddPdf *signalNDpdf=nullptr;
  //new RooAddPdf("signalNDpdf","signalNDpdf",keysNDset,SignalPars);
  //signalNDpdf->Print();
  //w->import(*signalNDpdf,RecycleConflictNodes());
  // Make the conditional model 
  //RooAddPdf *totPDF=new RooAddPdf("totPDF","totPDF",RooArgSet(*signalPdf,*bcktotPds));

  RooAddPdf *totPDF=nullptr; 
  if(fitCase==2)
    totPDF=new RooAddPdf("totPDF","totPDF",RooArgSet(*signalPdf,*bcktotPdsKeysProd));
  else if (fitCase==1) 
    totPDF=new RooAddPdf("totPDF","totPDF",RooArgSet(*signalPdf,*bckdHist3DPdf));
  if(fitCase!=0){
    cout<<"Conditional model "<<endl;
    totPDF->Print();
  }
  // build the full keys ND model only 
  RooAddPdf *totPDFND =nullptr;
  if(fitCase!=0){
    totPDFND=new RooAddPdf("totPDFND","totPDFND",RooArgSet(*signalNDpdf,*bcktotPds));
    cout<<"ND Keys model "<<endl;
    totPDFND->Print();
    //w->import(*totPDFND);
    cout<<endl;
  }

  // 3D hist pdf 
  RooAddPdf *signalHistPdf = new RooAddPdf("signalHistPdf","signalHistPdf",SigHistPdfSet,SignalPars);
  //w->import(*signalHistPdf,RecycleConflictNodes());

  
  // Make the 3d hist model: 
  RooAddPdf  *totHistPDF=new RooAddPdf("totHistPDF","totHistPDF",RooArgSet(*signalHistPdf,*bckdHist3DPdf));
  //w->import(*totHistPDF,RecycleConflictNodes());
  cout<<"3D hist pdf "<<endl;
  totHistPDF->Print();
  
  
  vector <string> consider; 
  consider.push_back("vbf"); consider.push_back("ggf"); consider.push_back("diboson"); 
  //for(vector<string>::iterator it=backgroundsConsider.begin(); it!=backgroundsConsider.end(); it++) consider.push_back(*it);
  consider.push_back("top"); consider.push_back("Zjets");
  
  RooArgSet toFitVars;
  toFitVars.add(mainObs);
  toFitVars.add(weight);
  //w->import(toFitVars);
  
  /*
    for( vector <string>::iterator it=treeNames.begin(); it!=treeNames.end(); it++){
    if((*it).compare("Data")==0) continue; 
    for(std::map<string,string>::iterator cr1=channelFiles.begin(); cr1!=channelFiles.end(); cr1++){
    for(std::map<string,string>::iterator cr=channelFiles.begin(); cr!=channelFiles.end(); cr++){
    if( (*cr).first.compare("signal")==0 || (*cr1).first.compare("signal")==0 ) {
    
    if((*cr).first.compare("signal")==0 && (*cr1).first.compare("signal")!=0)
    mapAllTransferFacts[(*it)][(*cr).first][(*cr1).first] = new RooFormulaVar(Form("Trans_%s_to_%s",(*cr).first.c_str(),(*cr1).first.c_str()),Form("T_{%s}^{%s}",(*cr).first.c_str(),(*cr1).first.c_str()),
                                                                                      "@0/@1",RooArgSet(*mapSRNormCoeff[(*it)],*mapCRNormCoeff[(*cr1).first][(*it)]));
                                                                                      
                                                                                      
                                                                                      else if((*cr).first.compare("signal")!=0 && (*cr1).first.compare("signal")==0)
                                                                                      mapAllTransferFacts[(*it)][(*cr).first][(*cr1).first] = new RooFormulaVar(Form("Trans_%s_to_%s",(*cr).first.c_str(),(*cr1).first.c_str()),Form("T_{%s}^{%s}",(*cr).first.c_str(),(*cr1).first.c_str()),
                                                                                      "@0/@1",RooArgSet(*mapCRNormCoeff[(*cr).first][(*it)],*mapSRNormCoeff[(*it)]));
                                                                                      else 
            
                                                                                      mapAllTransferFacts[(*it)][(*cr).first][(*cr1).first] = new RooFormulaVar(Form("Trans_%s_to_%s",(*cr).first.c_str(),(*cr1).first.c_str()),Form("T_{%s}^{%s}",(*cr).first.c_str(),(*cr1).first.c_str()),
                                                                                      "@0/@1",RooArgSet(*mapSRNormCoeff[(*it)],*mapSRNormCoeff[(*it)]));
                                                                                      
                                                                                      }
                                                                                      else {
                                                                                      mapAllTransferFacts[(*it)][(*cr).first][(*cr1).first] = new RooFormulaVar(Form("Trans_%s_to_%s",(*cr).first.c_str(),(*cr1).first.c_str()),Form("T_{%s}^{%s}",(*cr).first.c_str(),(*cr1).first.c_str()),
                                                                                      "@0/@1",RooArgSet(*mapCRNormCoeff[(*cr).first][(*it)],*mapCRNormCoeff[(*cr1).first][(*it)]));
                                                                                      }
                                                                                      }}}
                                                                                      
                                                                                      
                                                                                      /// Print out the tranfer factors 
                                                                                      for( vector <string>::iterator it=treeNames.begin(); it!=treeNames.end(); it++){
   if((*it).compare("Data")==0) continue; 
   cout<<"-----------------------------Component "<<*it<<" ------------------"<<endl;
   for(std::map<string,string>::iterator cr1=channelFiles.begin(); cr1!=channelFiles.end(); cr1++)
     cout<<(*cr1).first<<" ";
   cout<<endl;
   for(std::map<string,string>::iterator cr=channelFiles.begin(); cr!=channelFiles.end(); cr++){
     cout<<(*cr).first<<" ";
     for(std::map<string,string>::iterator cr1=channelFiles.begin(); cr1!=channelFiles.end(); cr1++){
       cout<<mapAllTransferFacts[(*it)][(*cr).first][(*cr).first]->getVal()<<" ";
       cout<<endl;
     }}}
 cout<<endl;
*/


  // Print out numbers for the CR
  for(std::map<string,string>::iterator cr=channelFiles.begin(); cr!=channelFiles.end(); cr++){
    if( (*cr).first.compare("signal")==0 ) continue; 
    string crName=(*cr).first;
    cout<<"-------- CR "<<(*cr).first<<"------------ "<<endl;
    for( vector <string>::iterator it=treeNames.begin(); it!=treeNames.end(); it++){
      if((*it).compare("data")!=string::npos) continue;
      cout<<(*it)<<" Events "<<mapCRNormCoeff[crName][(*it)]->getVal()<<" Transf fact "<<mapCRTrasnferFact[crName][(*it)]->getVal()<<endl;
    }
    cout<<"-------------------- "<<endl;
  }

  RooArgSet allPdfs;
  
  // Now build the simulatenus pdf 

  RooSimultaneous *simPDFNoCtr=new RooSimultaneous("simPDFNoCtr","simPDFNoCtr",*channelCat);
  if(fitCase==0)
    simPDFNoCtr->addPdf(*totHistPDF,"signal");
  else
    simPDFNoCtr->addPdf(*totPDF,"signal");
  //mainObs.add(*CRchannelObs["ggFCR"]);
  //simPDF->addPdf(*totCRpdf["ggFCR"],"ggFCR");
  //simPDF->addPdf(*CRsignalHistPdf,"ggFCR");
  // add also the obserbavle of the CR; 
  //w->import(*simPDFNoCtr);
  
  for(std::map<string,string>::iterator cr=channelFiles.begin(); cr!=channelFiles.end(); cr++){
    if( (*cr).first.compare("signal")==0 ) continue; 
    mainObs.add(*CRchannelObs[(*cr).first]);
    simPDFNoCtr->addPdf(*totCRpdf[(*cr).first],(*cr).first.c_str()); 
  }
  
  cout<<"Final simPDF"<<endl;
  simPDFNoCtr->Print();
  allPdfs.add(*simPDFNoCtr);

  cout<<" Add now the constraints "<<endl;

  RooSimultaneous *simPDF=new RooSimultaneous("simPDF","simPDfWithCtr",*channelCat);

  RooArgSet simPDFComp;
 std:map<string ,  RooAbsPdf*> PDfWithCtr;
  if(fitCase==0)
    simPDFComp.add(*totHistPDF);
  else
    simPDFComp.add(*totPDF);
  
  simPDFComp.add(constraintsSet);

  PDfWithCtr["signal"]=new RooProdPdf(Form("%s_%s","TotSPlusBPDF","ctr"),"",simPDFComp);
  PDfWithCtr["signal"]->Print();
  simPDF->addPdf(*PDfWithCtr["signal"],"signal");

  for(std::map<string,string>::iterator cr=channelFiles.begin(); cr!=channelFiles.end(); cr++){
    if( (*cr).first.compare("signal")==0 ) continue; 
    RooArgSet set;
    set.add(*totCRpdf[(*cr).first]);
    cout<<"Adding pdf "<<endl;
    totCRpdf[(*cr).first]->Print();
    set.add(constraintsSet);
    PDfWithCtr[(*cr).first]=new RooProdPdf(Form("%s_%s",totCRpdf[(*cr).first]->GetName(),"ctr"),"",set);
    cout<<"Channel "<<(*cr).first<<" pdf: "<<endl;
    PDfWithCtr[(*cr).first]->Print();
    simPDF->addPdf(*PDfWithCtr[(*cr).first],(*cr).first.c_str());
    mainObs.add(*CRchannelObs[(*cr).first]);
    //for(std::map<string,RooRealVar*>::iterator ii=npsGLob.begin(); ii!=npsGLob.end(); ii++)
    //mainObs.add(*((*ii).second));
    simPDF->Print();
  }

 
  
  
  //allPdfs.add(*simPDF);
  //w->import(*simPDF);
   
  cout<<"Final pdf with constraints: "<<endl;
  simPDF->Print();
  allPdfs.add(constraintsSet);
  //w->import(allPdfs,RecycleConflictNodes());
  //w->defineSet("allPdfs",allPdfs,kTRUE);
  //RooSimultaneous *clonePdf=new RooSimultaneous(*simPDF,"simpPDF_new");
  //w->import(*clonePdf);
  model->SetPdf(*simPDF);
  model->SetParametersOfInterest(pois);
  model->SetNuisanceParameters(npList);
  model->SetObservables(mainObs);
  model->SetGlobalObservables(GlobalObs);

  //model->SetConditionalObservables(cond);
  if(!w->import(*model)) cout<<"Problem importing model"<<endl;
  
  RooAbsPdf *fitPdf=simPDF;//(RooAbsPdf*)model->GetPdf();
  //RooAbsPdf *fitPdf=(RooAbsPdf*)simPDF;
  //RooDataSet *mcToy=mcDataSet(mapData,consider,&mainObs,1);
  //RooDataSet *mcToy=mapData["Data"];
  //RooDataSet *mcToy=(RooDataSet*)AsymptoticCalculator::GenerateAsimovData(*fitPdf,mainObs);
  RooDataSet *mcToy=nullptr;
  double orig1= n_CR_ggF["ggFCRNj2"]->getVal();
  double orig2= n_CR_ggF["ggFCRNj0"]->getVal();
  double orig3= n_CR_ggF["ggFCRNj1"]->getVal();

  // Set the n_CR_ggFCRNj2_ggF 20% higher 
  //n_CR_ggF["ggFCRNj2"]->setVal(2*orig1); 
  //n_CR_ggF["ggFCRNj0"]->setVal(1*orig2); 
  //n_CR_ggF["ggFCRNj1"]->setVal(0.5*orig3); 


  /*
 n_CR_ggF[Form("ggFCRNj%d",0)]->setVal( 1.0);
 cout<<"For 1 "<<f_ggF->getVal()<<" pdf "<<simPDF->getVal()<<endl;
 n_CR_ggF[Form("ggFCRNj%d",1)]->setVal( 2.0);
 cout<<"For 2 "<<f_ggF->getVal()<<" pdf "<<simPDF->getVal()<<endl;
 n_CR_ggF[Form("ggFCRNj%d",2)]->setVal( 3.0);
 cout<<"For 3 "<<f_ggF->getVal()<<" pdf "<<simPDF->getVal()<<endl;
 //for(int i=0;i<3; i++)
 //n_CR_ggF[Form("ggFCRNj%d",i)]->setConstant(true);
 */

  if(datasetType==DataSetType::Asimov){
    mcToy=(RooDataSet*)AsymptoticCalculator::GenerateAsimovData(*simPDF,mainObs);
    mcToy->SetName("asimovData");
  }
  else if(datasetType==DataSetType::MC)
    mcToy=mcDataSet(mapData,consider,&mainObs,1);
  else if(datasetType==DataSetType::Data)
    mcToy=mapData["Data"];
  else {
    cout<<" DataSetType"<< datasetType<<" not known "<<endl;
    return; 
  }
  w->import(*mcToy);
  
  // restore 
  n_CR_ggF["ggFCRNj2"]->setVal(orig1); 
  n_CR_ggF["ggFCRNj0"]->setVal(orig2); 
  n_CR_ggF["ggFCRNj1"]->setVal(orig3);

  n_CR_ggF["ggFCRNj2"]->setConstant(true);
  n_CR_ggF["ggFCRNj0"]->setConstant(true);
  n_CR_ggF["ggFCRNj1"]->setConstant(true);
  

  // free the top normalization: 
  bckPars["top"]->setConstant(false);
  
  // just for now
  //n_ggF.setConstant(true);
  //n_WW.setConstant(true); 
  //signalPdf->fitTo(*mcToy,SumW2Error(false));
  //totPDF->fitTo(*mcToy,NumCPU(2),SumW2Error(false));
  //totPDFND->fitTo(*mcToy,NumCPU(2),SumW2Error(false));

  //model->SetProtoData(*mcToy);
 
  
  //if(!w->import(*mcToy)) cout<<"Problem importing dataset"<<endl;
  //w->import(*fitPdf);
  //w->import(mainObs);
  //w->import(constraintsSet);
  //w->import(allPdfs);
  
  // Create a minimizer 
  RooLinkedList fitCmds18;
 
 fitCmds18.Add(new RooCmdArg(Strategy(2)));
 fitCmds18.Add(new RooCmdArg(Extended(true)));
 fitCmds18.Add(new RooCmdArg(InitialHesse(true)));//                                                                                                      
 fitCmds18.Add(new RooCmdArg(Minos(true)));//                                                                                                             
 fitCmds18.Add(new RooCmdArg(Optimize(true)));
 fitCmds18.Add(new RooCmdArg(SumW2Error(false)));
 //fitCmds18.Add(new RooCmdArg(ConditionalObservables(cond)));
 fitCmds18.Add(new RooCmdArg(NumCPU(2)));

 cout<<"Before fit "<<endl;
 SignalPars.Print("V");


 
 RooAbsReal *nll=simPDF->createNLL(*mcToy,NumCPU(2),SumW2Error(true),Constrain(npList));
 RooMinimizer *minim = new RooMinimizer(*nll);              ////OffSet true default
  minim->setEps(0.01);
  minim->setStrategy(3);
  minim->minimize("Minuit2");
  RooArgSet minosPars;
  minosPars.add(pois);
  minosPars.add(npList);
  minim->minos(minosPars);
  RooFitResult *fitResult=minim->save("fitResut","fitResult");
  const RooArgSet *snapShotManual3D= (RooArgSet*) pois.snapshot();
  w->saveSnapshot("fitResult_snap",pois);
  RooFitResult* r = minim->save();
  w->import(*snapShotManual3D);
  
  
  pois.Print("V");
  TIter cpIter=SignalPars.createIterator();
  RooAbsReal *dummyVar=nullptr;
  while((dummyVar = (RooAbsReal*)cpIter.Next())){
    cout<<dummyVar->GetName()<<" "<<dummyVar->getVal()<<"+/-"<<dummyVar->getPropagatedError(*fitResult)<<endl;
  }
  
  // print out the error on mu_ggF when it is parametrized 
  double mu_ggFErr=f_ggF->getPropagatedError(*fitResult);
  cout<<"mu_ggF "<<f_ggF->getVal()<<" +/- "<<mu_ggFErr<<"[events] mu "
      <<f_ggF->getVal()/mapBDTNormCoeff["bdt_ggF"]->getVal()<<" +/-"<<mu_ggFErr/f_ggF->getVal()
    //<<" manual "<<getRatioggFError( n_CR_ggF["ggFCRNj0"]->getVal(),
    //                                n_CR_ggF["ggFCRNj0"]->getError(),
    //                                n_CR_ggF["ggFCRNj1"]->getVal(),
    //                                n_CR_ggF["ggFCRNj1"]->getError(),
    //                                n_CR_ggF["ggFCRNj2"]->getVal(),
    //                                n_CR_ggF["ggFCRNj2"]->getError())<<endl;
      <<" manual "<<getRatioggFError( n_CR_ggF["ggFCRNj1"]->getVal(),
                                      n_CR_ggF["ggFCRNj1"]->getError(),
                                      n_CR_ggF["ggFCRNj2"]->getVal(),
                                      n_CR_ggF["ggFCRNj2"]->getError(),
                                      n_CR_ggF["ggFCRNj0"]->getVal(),
                                      n_CR_ggF["ggFCRNj0"]->getError())<<endl;


  RooFormulaVar *reparMuggF=new RooFormulaVar("reparMuggF","#mu_{ggF}","@0/@1",RooArgSet(*f_ggF,*mapBDTNormCoeff["bdt_ggF"]));
  
  
  TH2F *hcorrelation = (TH2F*)fitResult->correlationHist("fitResut_hcorrelation");
  SetDef(hcorrelation);
  TCanvas *cCorr= new TCanvas("cCorr","cCorr");{
    SetCanvasDefaults(cCorr);
    cCorr->cd();
    gStyle->SetPaintTextFormat("4.2f");
    hcorrelation->Draw("colz text");
  }


  //w->import(*simPDF,RecycleConflictNodes());
  //w->import(*simPDFNoCtr,RecycleConflictNodes());
  //w->defineSet("mainObs",mainObs,kTRUE);
  //w->defineSet("allVars",*simPDF->getVariables(kFALSE),kTRUE);
  w->writeToFile(wsfile->GetName());

  wsfile->Close();
  return ;

  RooMsgService::instance().setSilentMode(kTRUE);
  RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL);   
 
  
  // Iteratevelly change the uncertainty on nJ
  // Model used range from 0.001 to 1
  vector <double> j1;
  vector <double> j2;
  vector <double> j3;

  //j1.push_back(-1.0);  j1.push_back(-0.5); j1.push_back(1.0); j1.push_back(0.5); /*j1.push_back(0.7);*/ j1.push_back(1.0); 
  //j2.push_back(-1.0);  j2.push_back(-0.5); j2.push_back(1.0); j2.push_back(0.5); /*j1.push_back(0.7);*/ j2.push_back(1.0); 
  //j3.push_back(-1.0);  j3.push_back(-0.5); j3.push_back(1.0); j3.push_back(0.5); /*j1.push_back(0.7);*/ j3.push_back(1.0);

  
  j1.push_back(0.1);  /*j1.push_back(0.25); j1.push_back(0.5); j1.push_back(1); j1.push_back(1.5);*/ j1.push_back(2.0); 
  j2.push_back(0.1);  /*j2.push_back(0.25); j2.push_back(0.5); j2.push_back(1); j2.push_back(1.5);*/ j2.push_back(2.0); 
  j3.push_back(0.1);  /*j3.push_back(0.25); j3.push_back(0.5); j3.push_back(1); j3.push_back(1.5);*/ j3.push_back(2.0);
  
  //j1.push_back(0.001); j2.push_back(1.0); j3.push_back(5.0); 
  
  
  std::map<string,std::map<double, std::map<double, std::map<double, double>>>> parVal;
  std::map<string,std::map<double, std::map<double, std::map<double, double>>>>  parLowErr;
  std::map<string,std::map<double, std::map<double, std::map<double, double>>>>  parHighErr;

  std::map<string,double> crNomVal;
  for(int p=0; p<3; p++)
    crNomVal[Form("ggFCRNj%d",p)]=n_CR_ggF[Form("ggFCRNj%d",p)]->getVal();
  
  
  for(vector<double>::iterator ii=j1.begin(); ii!=j1.end(); ii++){
    int i=0;
    for(vector<double>::iterator kk=j2.begin(); kk!=j2.end(); kk++){
      int j=1;
        for(vector<double>::iterator ll=j3.begin(); ll!=j3.end(); ll++){
          int k=2;

          //nps["nJetSys"]->setConstant(true);
          //nps["nJetSys"]->setVal(1.0);
          // Set the value 
          //uncertaintyPerCat[Form("ggFCRNj%d",i)]->setVal(*ii);
          ///uncertaintyPerCat[Form("ggFCRNj%d",j)]->setVal(*kk);
          //uncertaintyPerCat[Form("ggFCRNj%d",k)]->setVal(*ll);
          
          // perform the fit
          //RooDataSet *mcToy_tmp=(RooDataSet*)AsymptoticCalculator::GenerateAsimovData(*simPDF,mainObs);
          //uncertaintyPerCat[Form("ggFCRNj%d",i)]->setVal(1.0);
          //uncertaintyPerCat[Form("ggFCRNj%d",j)]->setVal(1.0);
          //uncertaintyPerCat[Form("ggFCRNj%d",k)]->setVal(1.0);
          //nps["nJetSys"]->setVal(0.0);
          //nps["nJetSys"]->setConstant(true);
          
          //n_CR_ggF[Form("ggFCRNj%d",i)]->setVal( (*ii)*crNomVal[Form("ggFCRNj%d",i)]);
          //n_CR_ggF[Form("ggFCRNj%d",j)]->setVal( (*kk)*crNomVal[Form("ggFCRNj%d",j)]);
          //n_CR_ggF[Form("ggFCRNj%d",k)]->setVal( (*ll)*crNomVal[Form("ggFCRNj%d",k)]);

          n_CR_ggF[Form("ggFCRNj%d",i)]->setVal( (*ii));
          n_CR_ggF[Form("ggFCRNj%d",j)]->setVal( (*kk));
          n_CR_ggF[Form("ggFCRNj%d",k)]->setVal( (*ll));

          n_CR_ggF[Form("ggFCRNj%d",i)]->setConstant(true);
          n_CR_ggF[Form("ggFCRNj%d",j)]->setConstant(true);
          n_CR_ggF[Form("ggFCRNj%d",k)]->setConstant(true);
        
          cout<<"F ggF pre generation "<<endl;
          f_ggF->Print();
          cout<<"f_ggF "<<f_ggF->getVal()<<endl;

          RooDataSet *mcToy_tmp=(RooDataSet*)AsymptoticCalculator::GenerateAsimovData(*simPDF,mainObs);

          //n_CR_ggF[Form("ggFCRNj%d",i)]->setConstant(false);
          //n_CR_ggF[Form("ggFCRNj%d",j)]->setConstant(false);
          //n_CR_ggF[Form("ggFCRNj%d",k)]->setConstant(false);
          
          //n_CR_ggF[Form("ggFCRNj%d",i)]->setVal( crNomVal[Form("ggFCRNj%d",i)]);
          //n_CR_ggF[Form("ggFCRNj%d",j)]->setVal( crNomVal[Form("ggFCRNj%d",j)]);
          //n_CR_ggF[Form("ggFCRNj%d",k)]->setVal( crNomVal[Form("ggFCRNj%d",k)]);
          
          RooAbsReal *nll_tmp=simPDF->createNLL(*mcToy_tmp,SumW2Error(true),Constrain(npList));
          //RooAbsReal *nll_tmp=pdf->createNLL(*mcToy_tmp,SumW2Error(true),Constrain(npList));
          RooMinimizer *minim_tmp = new RooMinimizer(*nll_tmp);              ////OffSet true default
          minim_tmp->setEps(0.01);
          minim_tmp->setStrategy(2);
          minim_tmp->minimize("Minuit2");
          minim_tmp->minos(minosPars);
          RooFitResult *fitResult_tmp=minim_tmp->save("fitResut_tmp","fitResult_tmp");
         

          //Get the uncertainty on f_ggF
          TIter cpIter1=pois.createIterator();
          dummyVarZ=nullptr;
          while((dummyVarZ = (RooRealVar*)cpIter1.Next())){
            parVal[dummyVarZ->GetName()][*ii][*kk][*ll]=dummyVarZ->getVal();
            parHighErr[dummyVarZ->GetName()][*ii][*kk][*ll]=dummyVarZ->getErrorHi();
            parLowErr[dummyVarZ->GetName()][*ii][*kk][*ll]=dummyVarZ->getErrorLo();
            dummyVarZ->setVal(1.0);
            dummyVarZ->setError(0.0);
            
          }
          // Loop over the nuisance parameters
          TIter cpIter2=npList.createIterator();
          dummyVarZ=nullptr;
          while((dummyVarZ = (RooRealVar*)cpIter2.Next())){
            parVal[dummyVarZ->GetName()][*ii][*kk][*ll]=dummyVarZ->getVal();
            parHighErr[dummyVarZ->GetName()][*ii][*kk][*ll]=dummyVarZ->getErrorHi();
            parLowErr[dummyVarZ->GetName()][*ii][*kk][*ll]=dummyVarZ->getErrorLo();
            dummyVarZ->setVal(0.0);
            dummyVarZ->setError(0.0);
          }
          
          parVal[f_ggF->GetName()][*ii][*kk][*ll]=f_ggF->getVal();
          parHighErr[f_ggF->GetName()][*ii][*kk][*ll]=f_ggF->getPropagatedError(*fitResult_tmp);
          parLowErr[f_ggF->GetName()][*ii][*kk][*ll]=f_ggF->getPropagatedError(*fitResult_tmp);

          parVal[reparMuggF->GetName()][*ii][*kk][*ll]=reparMuggF->getVal();
          parHighErr[reparMuggF->GetName()][*ii][*kk][*ll]=reparMuggF->getPropagatedError(*fitResult_tmp);
          parLowErr[reparMuggF->GetName()][*ii][*kk][*ll]=reparMuggF->getPropagatedError(*fitResult_tmp);

          cout<<"F ggF post fit " <<endl;
          f_ggF->Print();
          cout<<"f_ggF "<<f_ggF->getVal()<<endl;
          
            
          // restore the nominal parameters
          //w->loadSnapshot("fitResult_snap");
          n_CR_ggF[Form("ggFCRNj%d",i)]->setVal( crNomVal[Form("ggFCRNj%d",i)]);
          n_CR_ggF[Form("ggFCRNj%d",j)]->setVal( crNomVal[Form("ggFCRNj%d",j)]);
          n_CR_ggF[Form("ggFCRNj%d",k)]->setVal( crNomVal[Form("ggFCRNj%d",k)]);
          
           delete fitResult_tmp;
           delete minim_tmp;
           delete nll_tmp;
           delete mcToy_tmp;
           //uncertaintyPerCat[Form("ggFCRNj%d",i)]->setVal(1.0);
           //uncertaintyPerCat[Form("ggFCRNj%d",j)]->setVal(1.0);
           //uncertaintyPerCat[Form("ggFCRNj%d",k)]->setVal(1.0);

           
        }}}


  
  // Make also some histograms;
  std::map<string, TH1F*> sysVarHist;
  std::map<string, TCanvas*> syshistCanvas;
  std::map<string, TCanvas*> sysgraphCanvas;
  
  
  std::map<string, TGraphAsymmErrors*> sysVarGraph;
  for(std::map<string,std::map<double, std::map<double, std::map<double, double>>>>::iterator pV=parVal.begin(); pV!=parVal.end(); pV++){
    sysVarHist[(*pV).first]=new TH1F(Form("hsysVarHist_%s",(*pV).first.c_str()),"",3*(*pV).second.size(),0,3*(*pV).second.size());

    int binC=1;
    vector <double> xp;
    vector <double> yp;
    vector <double> ypU;
    vector <double> ypD;
    
    for( std::map<double, std::map<double, std::map<double, double>>>::iterator pOne=(*pV).second.begin(); pOne!=(*pV).second.end(); pOne++){
      for( std::map<double, std::map<double, double>>::iterator pTwo=(*pOne).second.begin(); pTwo!=(*pOne).second.end(); pTwo++){
        for( std::map<double, double>::iterator pThree=(*pTwo).second.begin(); pThree!=(*pTwo).second.end(); pThree++){

          sysVarHist[(*pV).first]->SetBinContent(binC,(*pThree).second);
          sysVarHist[(*pV).first]->SetBinError(binC,0.5*(fabs(parHighErr[(*pV).first][(*pOne).first][(*pTwo).first][(*pThree).first])+fabs(parLowErr[(*pV).first][(*pOne).first][(*pTwo).first][(*pThree).first])));
          yp.push_back((*pThree).second);
          xp.push_back(binC);
          ypU.push_back(parHighErr[(*pV).first][(*pOne).first][(*pTwo).first][(*pThree).first]);
          ypD.push_back(parLowErr[(*pV).first][(*pOne).first][(*pTwo).first][(*pThree).first]);
          
          binC++;
        }}}

    sysgraphCanvas[(*pV).first]=new TCanvas(Form("sysVarGraphCanvas_%s",(*pV).first.c_str()),"");{
      sysVarGraph[(*pV).first]=new TGraphAsymmErrors(xp.size(),&xp[0],&yp[0],0,0,&ypD[0],&ypU[0]);
      sysVarGraph[(*pV).first]->SetMarkerStyle(8);
      sysVarGraph[(*pV).first]->SetFillStyle(3004);
      sysVarGraph[(*pV).first]->Draw("AP3");
    }
    
    syshistCanvas[(*pV).first]=new TCanvas(Form("sysVarHistCanvas_%s",(*pV).first.c_str()),"");{
      sysVarHist[(*pV).first]->Draw("PE");
    }
    
  }
                                               
  
  for(std::map<string,std::map<double, std::map<double, std::map<double, double>>>>::iterator pV=parVal.begin(); pV!=parVal.end(); pV++){
    cout<<"\\begin{tabular}{l c c c c}"<<endl;
    cout<<"Param name & Parmam Val &  $\\Delta{ggFCRNj0}$ & $\\Delta{ggFCRNj1}$ & $\\Delta{ggFCRNj2}$ \\\\"<<endl;
    //cout<<"Parameter : "<<(*pV).first<<"
    std::cout<<std::fixed;
    for( std::map<double, std::map<double, std::map<double, double>>>::iterator pOne=(*pV).second.begin(); pOne!=(*pV).second.end(); pOne++){
      for( std::map<double, std::map<double, double>>::iterator pTwo=(*pOne).second.begin(); pTwo!=(*pOne).second.end(); pTwo++){
        for( std::map<double, double>::iterator pThree=(*pTwo).second.begin(); pThree!=(*pTwo).second.end(); pThree++){
          cout<<std::setprecision(3)<< (*pV).first<<"&  $"<<(*pThree).second<<"^{"<<"+"<<parHighErr[(*pV).first][(*pOne).first][(*pTwo).first][(*pThree).first]<<"}_{"<<parLowErr[(*pV).first][(*pOne).first][(*pTwo).first][(*pThree).first]<<"}$ & "<<(*pOne).first<<" & "<<(*pTwo).first<<" & "<<(*pThree).first<<" \\\\ "<<endl;

         
                                                 
        }
      }
    }
    
    cout<<"\\end{tabular}"<<endl;
    cout<<"\\\\"<<endl;
  }
  



  

  if(doPLLCurves){
  // Plot the profiles 
  RooAbsReal *pll_n_vbf = nll->createProfile(*n_vbf);
  RooAbsReal *pll_n_ggF = nll->createProfile(*n_ggF);
  RooAbsReal *pll_n_WW = nll->createProfile(*n_WW);
  
  RooPlot *n_vbf_frame=n_vbf->frame(Title(" "));
  pll_n_vbf->plotOn(n_vbf_frame,ShiftToZero());


  TCanvas *cpllVBDF=new TCanvas("cpllVBDF","");{
    SetCanvasDefaults(cpllVBDF);
    pll_n_vbf->Draw();
  }

  RooPlot *n_ggF_frame=n_ggF->frame(Title(" "));
  pll_n_ggF->plotOn(n_ggF_frame,ShiftToZero());

  TCanvas *cpllGGF=new TCanvas("cpllGGF","");{
    SetCanvasDefaults(cpllGGF);
    n_ggF_frame->Draw();
  }
  
  RooPlot *n_WW_frame=n_WW->frame(Title(" "));
  pll_n_WW->plotOn(n_WW_frame,ShiftToZero());
  
  TCanvas *cpllWW=new TCanvas("cpllWW","");{
    SetCanvasDefaults(cpllWW);
    n_WW_frame->Draw();
  }

  // 2D contours;

  // Make contour plot of mx vs sx at 1,2,3 sigma
  RooPlot* contVBF_ggF = minim->contour(*n_ggF,*n_vbf,1,2) ;
  contVBF_ggF->SetTitle(" ") ;
  TCanvas *cCont1=new TCanvas("cCont1","cCont1");{
    SetCanvasDefaults(cCont1);
    contVBF_ggF->Draw();
  }

  RooPlot* cont_ggFWW = minim->contour(*n_ggF,*n_WW,1,2) ;
  cont_ggFWW->SetTitle(" ") ;
  TCanvas *cCont2=new TCanvas("cCont2","cCont2");{
    SetCanvasDefaults(cCont2);
    cont_ggFWW->Draw();
  }

RooPlot* cont_VBFWW = minim->contour(*n_vbf,*n_WW,1,2) ;
 cont_VBFWW->SetTitle(" ") ;
  TCanvas *cCont3=new TCanvas("cCont3","cCont3");{
    SetCanvasDefaults(cCont3);
    cont_VBFWW->Draw();
  }
  }
  
 

  RooPlot *xplotVBF=bdt_vbf->frame();
  if(datasetType==DataSetType::Asimov || datasetType==DataSetType::Data)
    mcToy->plotOn(xplotVBF,Cut("channel==channel::signal"),DataError(RooAbsData::Poisson));
  else 
    mcToy->plotOn(xplotVBF,Cut("channel==channel::signal"),DataError(RooAbsData::SumW2));

 

  //signalPdf->plotOn(xplotVBF,Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy));
  //signalPdf->plotOn(xplotVBF,Components("*WW*"),LineColor(kRed));

  signalHistPdf->plotOn(xplotVBF,Slice(*channelCat,"signal"),ProjWData(*channelCat,*mcToy));
  signalHistPdf->plotOn(xplotVBF,Components("*diboson*"),LineColor(kRed));
  
  //for(map<string,RooDataSet*>::iterator it=mapData.begin(); it!=mapData.end(); it++)
  //(*it).second->plotOn(xplot,Name((*it).first.c_str()));
  std::map<string, RooPlot*> xplots;
  std::map<string, TCanvas*> cc3d;
  std::map<string, TLegend*> cc3Legs;

 // create the histograms for the stacks // region; component; 
  std::map<string, std::map<string, TH1F*>> histMapPDF;
  std::map<string, THStack* > mapStack;

  for(std::map<string,string>::iterator it = bdtClasses.begin(); it!=bdtClasses.end(); it++){
    bool considerAxis=false;
    for(vector<string>::iterator it2 = bdtDiminesions.begin(); it2!=bdtDiminesions.end(); it2++){
      if( (*it2).compare( (*it).second)==0 ){ 
        considerAxis=true; break;
    }}

    if(!considerAxis) continue;
      
    xplots[(*it).first]=mapBDTObs[(*it).second]->frame(Title(" "),Binning(*mapBDTBins[(*it).second]));
    cc3Legs[(*it).first]=DefLeg(0.8,0.75,0.9,0.95);

    if(datasetType==DataSetType::Asimov || datasetType==DataSetType::Data){
      mcToy->plotOn(xplots[(*it).first],Cut("channel==channel::signal"),DataError(RooAbsData::Poisson),Name("MC"),Title(""),Binning(*mapBDTBins[(*it).second]));
      //histMapPDF[(*it).first]["Data"]=(TH1F*)mcToy->createHistogram(Form("hist_%s_%s",mcToy->GetName(),(*it).first.c_str()),
      //                                                            *mapBDTObs[(*it).second],Cut("channel==channel::signal"),DataError(RooAbsData::Poisson),
      //                                                            Name("MC"),Title(""),Binning(*mapBDTBins[(*it).second]));
    }
      else {
      mcToy->plotOn(xplots[(*it).first],Cut("channel==channel::signal"),DataError(RooAbsData::SumW2),Name("MC"),Title(""),Binning(*mapBDTBins[(*it).second]));
      //histMapPDF[(*it).first]["Data"]=(TH1F*)mcToy->createHistogram(Form("hist_%s_%s",mcToy->GetName(),(*it).first.c_str()),
      //                                                            *mapBDTObs[(*it).second],Cut("channel==channel::signal"),DataError(RooAbsData::SumW2),
      //                                                            Name("MC"),Title(""),Binning(*mapBDTBins[(*it).second]));
      }
    


    fitPdf->plotOn(xplots[(*it).first],Slice(*channelCat,"signal"),ProjWData(*channelCat,*mcToy),LineColor(kBlack),FillStyle(kFSolid),VLines(),FillColor(kWhite));
    //histMapPDF[(*it).first]["Total"]=(TH1F*)simPDF-createHistogram(Form("hist_%s_%s_total",simPDF->GetName(),(*it).first.c_str()),Binning(*mapBDTBins[(*it).second]));
    //                                                             Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy));
    
    if(fitCase==0){

      fitPdf->plotOn(xplots[(*it).first],Slice(*channelCat,"signal"),ProjWData(*channelCat,*mcToy),Name("vbf"),Components("mapSigHistPdf_vbf"),LineColor(colorMap["vbf"]),LineStyle(1));
      fitPdf->plotOn(xplots[(*it).first],Slice(*channelCat,"signal"),ProjWData(*channelCat,*mcToy),Name("diboson"),Components("mapSigHistPdf_diboson"),LineColor(colorMap["diboson"]),LineStyle(2));
      fitPdf->plotOn(xplots[(*it).first],Slice(*channelCat,"signal"),ProjWData(*channelCat,*mcToy),Name("ggf"),Components("mapSigHistPdf_ggf"),LineColor(colorMap["ggf"]),LineStyle(2));

      //if(!reparametrizeTrop)
      //simPDF->plotOn(xplots[(*it).first],Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy),Name("Top"),Components("*Top*"),LineColor(colorMap["Top"]),LineStyle(2));
      //simPDF->plotOn(xplots[(*it).first],Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy),Name("Zjets"),Components("*Zjets*"),LineColor(colorMap["Zjets"]),LineStyle(2));
      /* // Here plot Stacks 
      simPDF->plotOn(xplots[(*it).first],Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy),Name("GGF"),
                     Components("mapSigHistPdf_GGF"),LineColor(colorMap["GGF"]),LineStyle(2),FillColor(colorMap["GGF"]),FillStyle(kFSolid),VLines());
      simPDF->plotOn(xplots[(*it).first],Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy),Name("Zjets"),
                     Components("mapSigHistPdf_GGF,*Zjets*"),LineColor(colorMap["Zjets"]),LineStyle(2),FillColor(colorMap["Zjets"]),FillStyle(kFSolid),VLines());
      if(!reparametrizeTrop)
        simPDF->plotOn(xplots[(*it).first],Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy),Name("Top"),
                       Components("mapSigHistPdf_GGF,*Zjets*,*Top*"),LineColor(colorMap["Top"]),LineStyle(2),FillColor(colorMap["Top"]),FillStyle(kFSolid),VLines());
      simPDF->plotOn(xplots[(*it).first],Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy),Name("WW"),
                     Components("mapSigHistPdf_GGF,*Zjets*,*Top,mapSigHistPdf_WW"),LineColor(colorMap["WW"]),LineStyle(2),FillColor(colorMap["WW"]),FillStyle(kFSolid),VLines());
      simPDF->plotOn(xplots[(*it).first],Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy),Name("VBF"),
                     Components("mapSigHistPdf_GGF,*Zjets*,*Top,mapSigHistPdf_WW,*Zjets*,mapSigHistPdf_VBF"),LineColor(colorMap["VBF"]),LineStyle(1),FillColor(colorMap["VBF"]),FillStyle(kFSolid),VLines());
      */

    }
    else if(fitCase==1 || fitCase==2){
      
      fitPdf->plotOn(xplots[(*it).first],Slice(*channelCat,"signal"),ProjWData(*channelCat,*mcToy),Name("vbf"),Components("vbf3D"),LineColor(colorMap["vbf"]),LineStyle(1));
      fitPdf->plotOn(xplots[(*it).first],Slice(*channelCat,"signal"),ProjWData(*channelCat,*mcToy),Name("diboson"),Components("ww3D"),LineColor(colorMap["diboson"]),LineStyle(2));
      fitPdf->plotOn(xplots[(*it).first],Slice(*channelCat,"signal"),ProjWData(*channelCat,*mcToy),Name("ggf"),Components("ggF3D"),LineColor(colorMap["ggf"]),LineStyle(2));
      //if(!reparametrizeTrop)
      //simPDF->plotOn(xplots[(*it).first],Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy),Name("Top"),Components("*Top*"),LineColor(colorMap["Top"]),LineStyle(2));
      //simPDF->plotOn(xplots[(*it).first],Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy),Name("Zjets"),Components("*Zjets*"),LineColor(colorMap["Zjets"]),LineStyle(2));
      
      /*
      // Here plot Stacks 
      simPDF->plotOn(xplots[(*it).first],Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy),Name("GGF"),Components("ggF3D"),LineColor(colorMap["GGF"]),FillColor(colorMap["GGF"]),LineStyle(2));
      simPDF->plotOn(xplots[(*it).first],Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy),Name("Zjets"),Components("ggF3D,*Zjets*"),LineColor(colorMap["Zjets"]),FillColor(colorMap["Zjets"]),LineStyle(2));
      if(!reparametrizeTrop)
        simPDF->plotOn(xplots[(*it).first],Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy),Name("Top"),Components("ggF3D,*Zjets*,*Top*"),LineColor(colorMap["Top"]),FillColor(colorMap["Top"]),LineStyle(2));
      simPDF->plotOn(xplots[(*it).first],Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy),Name("WW"),Components("ggF3D,*Zjets*,*Top,ww3D"),LineColor(colorMap["WW"]),FillColor(colorMap["WW"]),LineStyle(2));
      simPDF->plotOn(xplots[(*it).first],Slice(channelCat,"signal"),ProjWData(channelCat,*mcToy),Name("VBF"),Components("ggF3D,*Zjets*,*Top,ww3D,*Zjets*,vbf3D"),LineColor(colorMap["VBF"]),FillColor(colorMap["VBF"]),LineStyle(1));
      */
    }
    
    for(vector<string>::iterator it2=backgroundsConsider.begin(); it2!=backgroundsConsider.end(); it2++){
      if(reparametrizeTrop && (*it2).compare("top")==0) continue;
      fitPdf->plotOn(xplots[(*it).first],Slice(*channelCat,"signal"),ProjWData(*channelCat,*mcToy),Name((*it2).c_str()),Components(Form("*%s*",(*it2).c_str())),LineColor(colorMap[(*it2)]),LineStyle(2));
      
    }




    if(datasetType==DataSetType::Asimov)
    cc3Legs[(*it).first]->AddEntry("MC","Pseudo data","PLE");
    else if(datasetType==DataSetType::MC)
      cc3Legs[(*it).first]->AddEntry("MC","MC","PLE");
    else if(datasetType==DataSetType::Data)    
      cc3Legs[(*it).first]->AddEntry("MC","Data","PLE");
    
    cc3Legs[(*it).first]->AddEntry("vbf","VBF","L");
    cc3Legs[(*it).first]->AddEntry("ggf","GGF","L");
    if(!reparametrizeTrop)
      cc3Legs[(*it).first]->AddEntry("diboson","Diboson","L");
    else if( reparametrizeTrop && hasBackground("top",backgroundsConsider))
      cc3Legs[(*it).first]->AddEntry("diboson","Diboson+Top","L");
    if(!reparametrizeTrop)
      cc3Legs[(*it).first]->AddEntry("top","Top","L");
    cc3Legs[(*it).first]->AddEntry("Zjets","Zjets","L");
    

    cc3d[(*it).first]=new TCanvas(Form("cc3d_%s",(*it).second.c_str()),"");{
      SetCanvasDefaults(cc3d[(*it).first]);
      cc3d[(*it).first]->SetLogy();
      cc3d[(*it).first]->cd();
      xplots[(*it).first]->Draw();
      cc3Legs[(*it).first]->Draw();
    }
  }


  // Loop over the control regions and make plots 
  std::map<string,TCanvas*> crCavnas;
  std::map<string,RooPlot*> crRooPlots;
  std::map<string,TLegend*> crLegends;
  
  for(std::map<string,RooRealVar*>::iterator ic=CRchannelObs.begin(); ic!=CRchannelObs.end(); ic++){
    if( (*ic).first.compare("signal")==0) continue; 
  
    crLegends[(*ic).first]=DefLeg();
    crLegends[(*ic).first]->SetHeader((*ic).first.c_str());

    crRooPlots[(*ic).first]=(*ic).second->frame(Title(" "));
    RooPlot *tplot=crRooPlots[(*ic).first];
    if(datasetType==DataSetType::Asimov || datasetType==DataSetType::Data)
      mcToy->plotOn( tplot,Name("Data"),DataError(RooAbsData::Poisson),Cut(Form("channel==channel::%s",(*ic).first.c_str())));
    else 
      mcToy->plotOn( tplot,Name("Data"),DataError(RooAbsData::SumW2),Cut(Form("channel==channel::%s",(*ic).first.c_str())));
    
    if(datasetType==DataSetType::Asimov)
      crLegends[(*ic).first]->AddEntry("Data","Pseudo data","PLE");
    else if (datasetType==DataSetType::MC)
      crLegends[(*ic).first]->AddEntry("Data","MC","PLE");
    else if (datasetType==DataSetType::Data)
      crLegends[(*ic).first]->AddEntry("Data","Data","PLE");

    fitPdf->plotOn(tplot,Slice(*channelCat,(*ic).first.c_str()),ProjWData(*channelCat,*mcToy),LineColor(kBlack),Name("Tot"));
    crLegends[(*ic).first]->AddEntry("Tot","Fit","L");
 
    vector <string> comps;
    comps.push_back("vbf");  comps.push_back("ggf"); comps.push_back("diboson"); 
    for(vector<string>::iterator it=backgroundsConsider.begin(); it!=backgroundsConsider.end(); it++) comps.push_back(*it);
    
    //simPDF->plotOn(tplot,Slice(channelCat,(*ic).first.c_str()),ProjWData(channelCat,*mcToy),LineColor(kBlack),Name("GGF"),Components("*GGF*"),LineColor(colorMap["GGF"]),LineStyle(2));
    for( vector <string>::iterator it=comps.begin(); it!=comps.end(); it++){
      fitPdf->plotOn(tplot,Name((*it).c_str()),Slice(*channelCat,(*ic).first.c_str()),ProjWData(*channelCat,*mcToy),LineColor(kBlack),Name((*it).c_str()),Components(Form("*%s*",(*it).c_str())),LineColor(colorMap[*it]),LineStyle(2));
      crLegends[(*ic).first]->AddEntry((*it).c_str(),(*it).c_str(),"L");
    }    


  crCavnas[(*ic).first]=new TCanvas(Form("cc_CR_%s",(*ic).first.c_str()),"");{
    SetCanvasDefaults(crCavnas[(*ic).first]);
    //crCavnas[(*ic).first]->SetLogy();
    crCavnas[(*ic).first]->cd();
    tplot->Draw();
    crLegends[(*ic).first]->Draw();
    
  }
  }
  
  
  
}


# vbfhww_fitting

# 1. checkAndFix 

This is step two (after processTree) in the workspace making procedure. The input of this script is the hadd-ed rootfile of all output of processTree, plus the theory systematics histograms. 

First compile by:

```
make -f make_checkInputs
```

to run:
```
./checkAndFixFitInputs_compile makeInputs/inc_bdt_vbf_fix_allSys_allSamples.root inc true
./checkAndFixFitInputs_compile makeInputs/inc_bdt_vbf_fix_allSys_allSamples.root inc false
```
where first cmd with `make_clones = true` simply clone some histograms, rename them and append them to the same input file; second cmd will actually process the updated root file, which takes longer.

Maps and variables that might need changes:
* 

# 2. HistFitter 

Download histfitter and use this particular version:
```
git clone https://github.com/histfitter/histfitter.git
git fetch
git checkout v0.61.0
```

this version of histfitter needs root6.16:
```
setupATLAS
lsetup "root 6.16.00-x86_64-slc6-gcc62-opt"
```

compile by:
```
cd src
make
```

To use histfitter, one has to do
```
source <histfitter dir>/setup.sh
```



#include "tools.h"
bool drawIBU=true;
bool noDraw=false;
bool useCustomShapeDrawUncert=false;
bool useRanomVariationsOfAllBins=true;
int nBinsX=15;
double p1Range[2]={-1.5,1.5};
std::map<string, int> nBinsMap;
std::map<string, string> VarMap;
//vector <string> matRegions={"SRVBF","CRTop"};
vector <string> matRegions={"SRVBF"};
string addedSel=Form("(p1> %.3lf && p1<%.3lf)",p1Range[0],p1Range[1]);

Double_t getMinVal(TProfile *p){
  double min=0;
  for( int i=1; i<(int)p->GetNbinsX()+1; i++)
    if( min >= p->GetBinContent(i))
      min=p->GetBinContent(i);
  return min; 
}

Double_t getMaxVal(TProfile *p){
  double max=0;
  for( int i=1; i<(int) p->GetNbinsX()+1; i++)
    if( max >= p->GetBinContent(i))
      max=p->GetBinContent(i);
  return max; 
}

Double_t polyN(Double_t *x,Double_t *p){
  int order= (int) p[0];
  double f=1;
  // constant allways set to zero 
  for( int i=1; i<order+1 ; i++) {
    f+=p[i]*pow(x[0],i);
  }
  return f;
}


string eraseSubStr(std::string in, const std::string & toErase)
{
  std::string mainStr=in;
  // Search for the substring in string
  size_t pos = mainStr.find(toErase);
 
  if (pos != std::string::npos)
	{
      // If found then erase it from string
      mainStr.erase(pos, toErase.length());
	}
  return mainStr;
}


vector <double> getCLSlide(TH1F *h,double CL=0.683){
  int nq=2;
  Double_t xq[nq];  // position where to compute the quantiles in [0,1]
  vector <double> yq;
  yq.resize(nq);
  //double yq[2];  // array to contain the quantiles
  xq[0]=(CL*0.5);
  xq[1]=(1-CL+xq[0]);
  h->GetQuantiles(nq,&yq[0],xq);
  //return yq;
  TF1 *g=new TF1("g","gaus",h->GetBinLowEdge(1),h->GetBinLowEdge(h->GetNbinsX()+1));
  h->Fit(g);
  yq[0]=g->GetParameter(1)-g->GetParameter(2);
  yq[1]=g->GetParameter(1)+g->GetParameter(2);
  return yq;

  yq[0]=0; 
  yq[1]=0;

  double ii=(1-CL)*h->Integral()*0.5;
  cout<<" Looking for integral "<<ii<<" @ "<<CL<<endl;
  double r=0;
  for(double x=h->GetBinLowEdge(h->GetNbinsX()); x>h->GetBinLowEdge(0); x-=h->GetBinWidth(1)){
    //r+=h->GetBinContent(h->FindBin(x));
    double r=h->Integral(h->FindBin(x),h->GetBinLowEdge(h->GetNbinsX()));
    //cout<<"Low edge "<<x<<" integral "<<r<<endl;
    cout<<"Integral is "<<r/h->Integral()<<" for x "<<x<<endl;
    if(r >=ii  ) { 
      cout<<"Lower bound for  "<<r/h->Integral()<<" for x "<<x<<endl;
      yq[0]=x; break; 

    }
  }
  r=0;
  for(double x=h->GetBinLowEdge(1); x<h->GetBinLowEdge(h->GetNbinsX()+1); x+=h->GetBinWidth(1)){
    r+=h->GetBinContent(h->FindBin(x));
    double r=h->Integral(h->FindBin(h->GetBinLowEdge(1)),h->FindBin(x));
    //cout<<"High edge "<<x<<" integral "<<r<<endl;
    cout<<"Integral is "<<r/h->Integral()<<" for x "<<x<<endl;
    if( r>=ii ) {
      cout<<"Lower bound for  "<<r/h->Integral()<<" for x "<<x<<endl;
      yq[1]=x; break; 
    }
  }
  
  return yq; 
  
 
}


void drawToys_(string fname="iterationsToys_ws_njet_expected.root", int iter=0,int rooUnfoldIter=0,TFile *fout=nullptr,TTree *tout=nullptr){
  /*
    std::map<int, std::map<int,double>> &meanCF,
    std::map<int, std::map<int,double>> &meanCF_e,
    std::map<int, std::map<int,double>> &meanMatrix,
    std::map<int, std::map<int,double>> &meanMatrix_e){
  */

  if(iter !=0 || rooUnfoldIter !=0) drawIBU=true;
  else drawIBU=false;
  
  vector <string> formats;
  formats.push_back("eps");
  formats.push_back("pdf");


  TFile *f=TFile::Open(fname.c_str());
  TTree *t=(TTree*)f->Get("toys");
  if( t==nullptr) return ; 
  TObjArray* tb=t->GetListOfBranches();
  int PoiLoop=0;
  for(PoiLoop=0; PoiLoop < 100 ; PoiLoop++){
    //if( tb->FindObject(Form("Sigma_sigma_bin%d_incl",PoiLoop))!=nullptr ){
    if( tb->FindObject(Form("Sigma_sigma_bin%d",PoiLoop))!=nullptr ){
      nBinsMap[fname]=PoiLoop+1;
      string tmpS= eraseSubStr(fname,"_expected_mtx_v20v2.root");
      VarMap[fname]=eraseSubStr(tmpS,"iterationsToys_ws_");
    }
    else break; 
  }
  cout<<"N bins map "<<nBinsMap[fname]<<endl;

  /*
  if(nBinsMap.size()==0){
    //nBinsMap["iterationsToys_ws_njet_expected.root"]=4;
    nBinsMap["iterationsToys_ws_njet_expected_mtx_v20v2.root"]=4;
    nBinsMap["iterationsToys_ws_pt_expected_mtx_v20v2.root"]=9;
    nBinsMap["iterationsToys_ws_y_expected_mtx_v20v2.root"]=9;
    nBinsMap["iterationsToys_ws_sjpt_expected_mtx_v20v2.root"]=3;
  }
  if(VarMap.size()==0){
    VarMap["iterationsToys_ws_njet_expected_mtx_v20v2.root"]="#it{N_{j}}";
    VarMap["iterationsToys_ws_pt_expected_v20v2.root"]="#it{p}_{T}^{4l}";
    VarMap["iterationsToys_ws_y_expected_mtx_v20v2.root"]="#it{y}^{4l}";
    VarMap["iterationsToys_ws_sjpt_expected_mtx_v20v2.root"]="sjpt";
  }
 */

  TRandom rand;
  rand.SetSeed(12345);

  vector <string> methods;
  methods.push_back("matrix");
  methods.push_back("CF");
  methods.push_back("IBU");
  
  const int nBins=nBinsMap[fname];
  
  // make some pulls 
  std::map<int,TH1F*> hPullMatrix;
  std::map<int,TH1F*> hPullCF;
  std::map<int,TH1F*> hPullIBU;
  
  
  
  std::map<int,TF1*> fPullMatrix;
  std::map<int,TF1*> fPullCF;
  std::map<int,TF1*> fPullIBU;
  
  std::map<int,TCanvas*> ccPullCom;
  std::map<int,TLegend*> ccPullLeg;

  std::map<int, TH1F*> hErrDraw;
 
  
  std::map<int,TH1F*> hErrMartrix;
  std::map<int,TH1F*> hErrCF;
  std::map<int,TH1F*> hErrIBU;
  std::map<int,TCanvas*> ccErrCom;
  std::map<int,TLegend*> ccErrLeg;

  std::map<int,TH1F*> hPullRecoMatrix;
  std::map<int,TH1F*> hPullRecoCF;
  std::map<int,TH1F*> hPullRecoIBU;

  
  std::map<int,TF1*> fPullRecoMatrix;
  std::map<int,TF1*> fPullRecoCF;
  std::map<int,TF1*> fPullRecoIBU;
  
  std::map<int,TCanvas*> ccPullRecoCom; 
  std::map<int,TLegend*> ccPullRecoLeg;

  std::map<int, TCanvas*> ccBelts;
  std::map<int, TH1F*> hBeltsDraw;
  std::map<int, TLine*> vLineEXSM;
  
  std::map<int, TGraphErrors*> grBeltMatrixCenter;
  std::map<int, TGraphErrors*> grBeltMatrixLow;
  std::map<int, TGraphErrors*> grBeltMatrixHigh;
  
  std::map<int, TGraphErrors*> grFitMatrix;
  std::map<int, TGraphErrors*> grFitIBU;
  std::map<int, TGraphErrors*> grFitCF;

  std::map<int, TH2F*> hBFitMatrix;
  std::map<int, TH2F*> hBFitIBU;
  std::map<int, TH2F*> hBFitCF;

  std::map<int, TGraphErrors*> grBeltCFCenter;
  std::map<int, TGraphErrors*> grBeltCFLow;
  std::map<int, TGraphErrors*> grBeltCFHigh;

  std::map<int, TGraphErrors*> grBeltIBUCenter;
  std::map<int, TGraphErrors*> grBeltIBULow;
  std::map<int, TGraphErrors*> grBeltIBUHigh;
  
  
  TLatex *lat=new TLatex();
  lat->SetTextFont(42);
  std::map<int, TH2F*> h2DdiffMatrix;
  std::map<int, TH2F*> h2DdiffCF;
  std::map<int, TH2F*> h2DdiffIBU;

  std::map<int,vector<double>> variationsPerBin;
  vector <double> varP;
  varP.push_back(0);
  varP.push_back(0);
  varP.push_back(0);
  varP.push_back(0);
  varP.push_back(0);
  varP.push_back(0);
  varP.push_back(0);
  varP.push_back(0);
  varP.push_back(0);
  varP.push_back(0);
  varP.push_back(0);
  varP.push_back(0);
  
  /*
  varP.push_back(1.2);
  varP.push_back(1.1);
  varP.push_back(0.8);
  varP.push_back(0.6);
  varP.push_back(0.6);
  varP.push_back(0.6);
  varP.push_back(0.6);
  varP.push_back(0.6);
  varP.push_back(0.6);
  varP.push_back(0.6);
  varP.push_back(0.6);
  varP.push_back(0.6);
  */
  

  // rest is for pt4;
  //  if(fname.compare("pt")!=string::npos){
  //varP.push_back(0.6);
  //varP.push_back(0.5);
  //varP.push_back(0.5);
  //varP.push_back(0.4);
  //varP.push_back(0.4);
  //}

  
  
  variationsPerBin[0]=varP;

  //variationsPerBin[0]=0.95;
  //variationsPerBin[1]=1.1;
  //variationsPerBin[1]=1.2;
  std::map<int, std::map<string, vector<TH2F*>>> hWeightBinp1;
  std::map<int, std::map<string, vector <TCanvas*> >> hWeightBinp1C;
  std::map<int, std::map<string, vector<TProfile*> >> hWeightBinp1Prof;
  std::map<int, std::map<string, vector<TCanvas*> >> hWeightBinp1ProfC;
  std::map<int, std::map<string, TGraph2D*>> hWeightBDTp1;
  std::map<int, std::map<string, TCanvas*>> hWeightBDTp1C;
  std::map<int, std::map<string, TH2F*>> hWeightBDTp1Dummy;
  std::map<int, TH1F*> hDiffXsecShapeMatrix;
  std::map<int, TH1F*> hDiffXsecShapeCF;
  std::map<int, TH1F*> hDiffXsecShapeIBU;

  std::map<int, TH1F*> hDiffXsecShapeMatrix_up;
  std::map<int, TH1F*> hDiffXsecShapeCF_up;
  std::map<int, TH1F*> hDiffXsecShapeIBU_up;

  // Store bias results in a TGraphAsymmError

 
  TVectorD Tcenterx(nBins);
  TVectorD Ttau(nBins);
  std::map<string, TVectorD*> TerrorYH_methods;
  TerrorYH_methods["matrix"]=new TVectorD(nBins+1);
  TerrorYH_methods["CF"]=new TVectorD(nBins+1);
  TerrorYH_methods["IBU"]=new TVectorD(nBins+1);
  
  std::map<string, TVectorD*> TerrorYL_methods;
  TerrorYL_methods["matrix"]=new TVectorD(nBins+1);
  TerrorYL_methods["CF"]=new TVectorD(nBins+1);
  TerrorYL_methods["IBU"]=new TVectorD(nBins+1);
  

  vector <double> centerx;
  vector <double> centery;

  vector <double> errorYH_matrix;
  vector <double> errorYL_matrix;

  vector <double> errorYH_IBU;
  vector <double> errorYL_IBU;
  
  vector <double> errorYH_CF;
  vector <double> errorYL_CF;

  TGraphAsymmErrors *grBiasBand_matrix;
  TGraphAsymmErrors *grBiasBand_CF;
  TGraphAsymmErrors *grBiasBand_IBU;

  
  TLegend *lcShape=DefLeg(0.2,0.75,0.4,0.98);
  TLegend *lcShapeMid=DefLeg(0.6,0.75,0.8,0.98);
  TLegend *lcShape2=DefLeg(0.2,0.2,0.4,0.35);
  
  
  for(std::map<int, vector<double>>::iterator it=variationsPerBin.begin(); it!=variationsPerBin.end(); it++){
    const int ii=(*it).first;
    hDiffXsecShapeMatrix[ii]=new TH1F(Form("hDiffXsecShapeMatrix_%d",ii),"",nBinsMap[fname],0,nBinsMap[fname]);
    SetDef(hDiffXsecShapeMatrix[ii]);
    hDiffXsecShapeMatrix[ii]->GetXaxis()->SetTitle(Form("Bin %s",VarMap[fname].c_str()));
    hDiffXsecShapeMatrix[ii]->GetYaxis()->SetTitle(Form("<#hat{#sigma}^{bin}_{fit}-#sigma^{bin}_{true}/#sigma^{bin}_{true}>"));
    hDiffXsecShapeMatrix[ii]->SetLineStyle(ii+1);
    
    hDiffXsecShapeCF[ii]=new TH1F(Form("hDiffXsecShapeCF_%d",ii),"",nBinsMap[fname],0,nBinsMap[fname]);
    SetDef(hDiffXsecShapeCF[ii]);
    hDiffXsecShapeCF[ii]->GetXaxis()->SetTitle(Form("Bin %s",VarMap[fname].c_str()));
    hDiffXsecShapeCF[ii]->GetYaxis()->SetTitle(Form("<#hat{#sigma}^{bin}_{fit}-#sigma^{bin}_{true}/#sigma^{bin}_{true}>"));
    hDiffXsecShapeCF[ii]->SetLineStyle(ii+1);

    
    hDiffXsecShapeIBU[ii]=new TH1F(Form("hDiffXsecShapeIBU_%d",ii),"",nBinsMap[fname],0,nBinsMap[fname]);
    SetDef(hDiffXsecShapeIBU[ii]);
    hDiffXsecShapeIBU[ii]->GetXaxis()->SetTitle(Form("Bin %s",VarMap[fname].c_str()));
    hDiffXsecShapeIBU[ii]->GetYaxis()->SetTitle(Form("<#hat{#sigma}^{bin}_{fit}-#sigma^{bin}_{true}/#sigma^{bin}_{true}>"));
    hDiffXsecShapeIBU[ii]->SetLineStyle(ii+1);


    hDiffXsecShapeMatrix_up[ii]=new TH1F(Form("hDiffXsecShapeMatrix_up_%d",ii),"",nBinsMap[fname],0,nBinsMap[fname]);
    SetDef(hDiffXsecShapeMatrix_up[ii]);
    hDiffXsecShapeMatrix_up[ii]->GetXaxis()->SetTitle(Form("Bin %s",VarMap[fname].c_str()));
    hDiffXsecShapeMatrix_up[ii]->GetYaxis()->SetTitle(Form("<#hat{#sigma}^{bin}_{fit}-#sigma^{bin}_{true}/#sigma^{bin}_{true}>"));
    hDiffXsecShapeMatrix_up[ii]->SetLineStyle(ii+1+2);
    hDiffXsecShapeMatrix_up[ii]->SetLineWidth(2);
    
    hDiffXsecShapeCF_up[ii]=new TH1F(Form("hDiffXsecShapeCF_up_%d",ii),"",nBinsMap[fname],0,nBinsMap[fname]);
    SetDef(hDiffXsecShapeCF_up[ii]);
    hDiffXsecShapeCF_up[ii]->GetXaxis()->SetTitle(Form("Bin %s",VarMap[fname].c_str()));
    hDiffXsecShapeCF_up[ii]->GetYaxis()->SetTitle(Form("<#hat{#sigma}^{bin}_{fit}-#sigma^{bin}_{true}/#sigma^{bin}_{true}>"));
    hDiffXsecShapeCF_up[ii]->SetLineStyle(ii+1+2);
    hDiffXsecShapeCF_up[ii]->SetLineWidth(2);

    
    hDiffXsecShapeIBU_up[ii]=new TH1F(Form("hDiffXsecShapeIBU_up_%d",ii),"",nBinsMap[fname],0,nBinsMap[fname]);
    SetDef(hDiffXsecShapeIBU_up[ii]);
    hDiffXsecShapeIBU_up[ii]->GetXaxis()->SetTitle(Form("Bin %s",VarMap[fname].c_str()));
    hDiffXsecShapeIBU_up[ii]->GetYaxis()->SetTitle(Form("<#hat{#sigma}^{bin}_{fit}-#sigma^{bin}_{true}/#sigma^{bin}_{true}>"));
    hDiffXsecShapeIBU_up[ii]->SetLineStyle(ii+1+2);
    hDiffXsecShapeIBU_up[ii]->SetLineWidth(2);
  }
  
  
  std::map<int, TProfile*> hProfilediffMatrix;
  std::map<int, TProfile*> hProfilediffCF;
  std::map<int, TProfile*> hProfilediffIBU;
  
  std::map<int,TCanvas*> ccProfiles;;
  std::map<int,TCanvas*> cc2D;
  std::map<int,TLegend*> ccProfilesLeg;
  std::map<int,TLegend*> ccProfilesLeg2;
  std::map<int,TLine*> tLProfileHL;
  std::map<int,TLine*> tLProfileHR;
  
  std::map<int,TLine*> tLProfileVL;
  std::map<int,TLine*> tLProfileVR;

  std::map<int,TLine*> tLProfileIBUL;
  std::map<int,TLine*> tLProfileIBUR;
   
  std::map<int, double> trueMean;
  std::map<int, double> trueRMS;

  std::map<int,double> medianErrorCF;
  std::map<int,double> medianErrorMatrix;
  std::map<int,double> medianErrorIBU;

  //const double tau=-1*(1.0 - TMath::Exp(0.5*0.5*rooUnfoldIter));
  const double tau=-1*(1.0 - TMath::Exp(0.2*rooUnfoldIter));
   
  string path="";
  if(rooUnfoldIter==iter)
    path=Form("plots/iter_%d/%s",iter,eraseSubStr(fname,".root").c_str());
  else
    path=Form("plots/iter_%d/%s",rooUnfoldIter,eraseSubStr(fname,".root").c_str());

  system(Form("mkdir -p %s",path.c_str()));

  // store here the SM xs
  vector <double> SM_xsec;
  
  
  for( int i=0; i<nBinsMap[fname]; i++){

    // store the sm xsec for later on 
    TH1F *hhtt=new TH1F("hhtt","",100,-1000,+1000);
    t->Draw(Form("Sigma_sigma_bin%d>>hhtt",i),"","");
    SM_xsec.push_back(hhtt->GetMean());
    delete hhtt;

    if( iter == 0 ) {
      // fill here the variations of the bin weights as a function of p1
      for(vector<string>::iterator lm=matRegions.begin(); lm!=matRegions.end(); lm++){
	// define how many bins there are on this histogram 
	TH1F *hnBins = new TH1F("hnBins","",20,0,20);
	t->Draw(Form("weight_%s_bin%dsize>>%s",(*lm).c_str(),i,hnBins->GetName()),addedSel.c_str(),"colz");
	int nBinsDiscr=(int)hnBins->GetMean();
	delete hnBins;
	cout<<"Axis has "<<nBinsDiscr<<" bins "<<endl;
	
	// loop untill the histograms are empty
	for(int bbin=0; bbin<nBinsDiscr; bbin++){
	  hWeightBinp1C[i][*lm].push_back(new TCanvas(Form("hWeightBinp1C_%s_bin%d_bdt%d",(*lm).c_str(),i,bbin),""));
	  TCanvas *cit=hWeightBinp1C[i][*lm].back();
	  cit->cd();
	  SetCanvasDefaults(cit);
	  TH2F *hb=new TH2F(Form("hb2D_%s_diffBin%d_bdtBin_%d",(*lm).c_str(),i,bbin),"",nBinsX,p1Range[0],p1Range[1],nBinsX*10,-10,+10);
	  hb->GetXaxis()->SetTitle("#it{p}_{1}");
	  hb->GetYaxis()->SetTitle(Form("#it{D}^{%s}_{%d}(%d)/#it{D}^{nom}",(*lm).c_str(),i,bbin));
	  hb->SetStats(0);
	  t->Draw(Form("weight_%s_bin%d[%d]:p1>>%s",(*lm).c_str(),i,bbin,hb->GetName()),addedSel.c_str(),"colz");
	  hb->Draw("colz");
	  for(auto ff : formats)
	    cit->Print(Form("%s/%s.%s",path.c_str(),cit->GetName(),ff.c_str()),ff.c_str());
	  if( hb->GetEntries() <=0 ) { break; } 
	  hWeightBinp1[i][*lm].push_back(hb);
	  // create the profile
	  hWeightBinp1ProfC[i][*lm].push_back(new TCanvas(Form("hWeightBinp1ProfC_%s_bin%d_bdt%d",(*lm).c_str(),i,bbin),""));
	  TCanvas* cip=hWeightBinp1ProfC[i][*lm].back();
	  cip->cd();
	  SetCanvasDefaults(cip);
	  TProfile *p =(TProfile*)hb->ProfileX(Form("hbProf_%s_diffBin%d_bdtBin_%D",(*lm).c_str(),i,bbin),1,-1,"");
	  if(p == nullptr) continue; 
	  SetDef(p);
	  hWeightBinp1Prof[i][*lm].push_back(p); 
	  p->SetMarkerStyle(8);
	  p->GetYaxis()->SetTitle(hb->GetYaxis()->GetTitle());
	  p->GetXaxis()->SetTitle(hb->GetXaxis()->GetTitle());
	  p->Draw("PE");
	  for(auto ff : formats)
	    cip->Print(Form("%s/%s.%s",path.c_str(),cip->GetName(),ff.c_str()),ff.c_str());
	}
	// now loop over all profiles 
	vector<TProfile*> pvec=hWeightBinp1Prof[i][*lm];
	
	vector <double> binW;
	vector <double> p1V;
	vector <double> binW_e;
	vector <double> p1V_e;
	vector <double> bdtBinV;
	//for(double xp=-2; xp < 2; xp+=pvec[0]->GetXaxis()->GetBinWidth(1) ){
	for( int ixp=1; ixp<(int)pvec[0]->GetNbinsX()+1; ixp++){
	  for( int ip=0; ip<(int)pvec.size(); ip++){
	    if(pvec[ip] == nullptr) continue; 
	    if( ixp > pvec[ip]->GetNbinsX() ) break;
	    binW.push_back(pvec[ip]->GetBinContent(ixp));
	    binW_e.push_back(pvec[ip]->GetBinError(ixp));
	    p1V.push_back(pvec[ip]->GetBinCenter(ixp));
	    p1V_e.push_back(0.0);
	    bdtBinV.push_back(ip);
	    
	    cout<<"For "<<(*lm)<<" bbt bin "<<ip<<" p1: "<<p1V.back()<<" weight "<<binW.back()<<endl;
	  }}
	hWeightBDTp1C[i][*lm]=new TCanvas(Form("hWeightBDTp1C_%s_bin%d",(*lm).c_str(),i),"");
	TCanvas *igrp=hWeightBDTp1C[i][*lm];{
	  igrp->cd();
	  //SetCanvasDefaults(igrp);
	  hWeightBDTp1Dummy[i][*lm]=new TH2F(Form("hWeightBDTp1Dummy_%s_%d",(*lm).c_str(),i),"",100,0,pvec.size(),nBinsX,p1Range[0],p1Range[1]);
	  SetDef(hWeightBDTp1Dummy[i][*lm]);
	  hWeightBDTp1Dummy[i][*lm]->SetFillColor(kWhite);
	  hWeightBDTp1Dummy[i][*lm]->SetLineColor(kWhite);
	  hWeightBDTp1Dummy[i][*lm]->GetYaxis()->SetRangeUser(p1Range[0],p1Range[1]);
	  hWeightBDTp1Dummy[i][*lm]->GetZaxis()->SetRangeUser(-10,+10);
	  
	  
	  TGraph2D *igr=new TGraph2D(binW.size(),&bdtBinV[0],&p1V[0],&binW[0]);
	  igr->SetTitle("");
	  igr->SetName(Form("BdtBiasGraph_%s_%d",(*lm).c_str(),i));
	  hWeightBDTp1Dummy[i][*lm]->GetYaxis()->SetTitle("#it{p}_{1}");
	  hWeightBDTp1Dummy[i][*lm]->GetXaxis()->SetTitle("#it{N(D)}");
	  hWeightBDTp1Dummy[i][*lm]->GetZaxis()->SetTitle(Form("#it{D}^{%s,%d}/#it{D}^{nom}",(*lm).c_str(),i));

	  
	  
	  igr->SetMarkerStyle(8);
	  igr->SetLineColor(kBlack);
	  hWeightBDTp1[i][*lm]=igr;
	  gStyle->SetPalette(1);
	  hWeightBDTp1Dummy[i][*lm]->Draw("surf");
	  igr->Draw("COLZ");
	  //igr->Draw("surf1");
	  igrp->Update();
	  igr->GetYaxis()->SetTitle("#it{p}_{1}");
	  igr->GetXaxis()->SetTitle("#it{N(D)}");
	  igr->GetZaxis()->SetTitle(Form("#it{D}^{%s,%d}/#it{D}^{nom}",(*lm).c_str(),i));
	  
	  //igr->Draw("A surf1");
	  //gPad->Update()
	}
	for(auto ff : formats)
	  igrp->Print(Form("%s/%s.%s",path.c_str(),igrp->GetName(),ff.c_str()),ff.c_str());
	
      }
    }

    
    // Fill here the error distributions 
    ccErrCom[i]=new TCanvas(Form("ccErrCom_%d_iter%d",i,rooUnfoldIter),"");{
      SetCanvasDefaults(ccErrCom[i]);
      ccErrCom[i]->cd();
      ccErrCom[i]->SetLogy();
      ccErrLeg[i]=DefLeg(0.75,0.75,0.9,0.95);
     
      ccErrLeg[i]->SetHeader(Form("#bf{%s}, bin %d",VarMap[fname].c_str(),i));
      
      hErrMartrix[i]=new TH1F(Form("h_err_Matrix_bin%d",i),"",50*(i+1),0,+0.5*(i+1));
      SetDef( hErrMartrix[i]);
      hErrMartrix[i]->SetMarkerStyle(8);
      t->Draw(Form("(matrix_e_sigma_bin%d[%d]/matrix_sigma_bin%d[%d])>>h_err_Matrix_bin%d",i,iter,i,iter,i),addedSel.c_str(),"");
      ccErrLeg[i]->AddEntry(hErrMartrix[i],"Matrix","PLE");
      
      hErrCF[i]=new TH1F(Form("h_err_CF_bin%d",i),"",50*(i+1),0,+0.5*(i+1));
      hErrCF[i]->GetXaxis()->SetTitle(Form("#scale[0.7]{#frac{#delta#hat{#sigma}^{bin,%d}_{fit}}{#hat{#sigma}^{bin,%d}}}",i,i));
      hErrCF[i]->GetYaxis()->SetTitle(Form("Pseudo experiments / %.2lf",hErrCF[i]->GetBinWidth(1)));
      
      SetDef(hErrCF[i]);
      t->Draw(Form("(CF_e_sigma_bin%d[%d]/CF_sigma_bin%d[%d])>>h_err_CF_bin%d",i,iter,i,iter,i),addedSel.c_str(),"");
      ccErrLeg[i]->AddEntry(hErrCF[i],"bin-by-bin","L");
      hErrCF[i]->SetLineColor(kRed);
      
      hErrIBU[i]=new TH1F(Form("h_err_IBU_bin%d",i),"",50*(i+1),0,+0.5*(i+1));
      hErrIBU[i]->GetXaxis()->SetTitle(Form("#scale[0.7]{#frac{#delta#hat{#sigma}^{bin,%d}_{fit}}{#hat{#sigma}^{bin,%d}}}",i,i));
      hErrIBU[i]->GetYaxis()->SetTitle(Form("Pseudo experiments / %.2lf",hErrIBU[i]->GetBinWidth(1)));
      
      SetDef(hErrIBU[i]);
      t->Draw(Form("(IBU_e_sigma_bin%d[%d]/IBU_sigma_bin%d[%d])>>h_err_IBU_bin%d",i,rooUnfoldIter,i,rooUnfoldIter,i),addedSel.c_str(),"");
      //ccErrLeg[i]->AddEntry(hErrIBU[i],Form("IBU %d",rooUnfoldIter+1),"L");
      if(drawIBU)
        ccErrLeg[i]->AddEntry(hErrIBU[i],Form("#tau %.2lf",tau),"L");
      hErrIBU[i]->SetLineColor(kBlue);
      hErrIBU[i]->SetLineStyle(1);

      hErrDraw[i]=new TH1F(Form("hErrDraw_%d",i),"",100,-0.75,+0.6*(i+1));
      hErrDraw[i]->SetStats(0);
      hErrDraw[i]->SetLineColor(kWhite);
      hErrDraw[i]->GetYaxis()->SetRangeUser(1,hErrCF[i]->GetMaximum()*10);
      hErrDraw[i]->GetXaxis()->SetTitle(Form("#scale[0.7]{#frac{#delta#hat{#sigma}^{bin,%d}_{fit}}{#hat{#sigma}^{bin,%d}}}",i,i));
      hErrDraw[i]->GetYaxis()->SetTitle(Form("Pseudo experiments / %.2lf",hErrCF[i]->GetBinWidth(1)));
      
      hErrDraw[i]->Draw("hist");
      hErrCF[i]->Draw("hist same");
      if(drawIBU)
        hErrIBU[i]->Draw("hist same");
      hErrMartrix[i]->Draw("hist PE same");
      ccErrLeg[i]->Draw();

      lat->DrawLatexNDC(0.15,0.92,"Matrix");
      lat->DrawLatexNDC(0.15,0.88,Form("Mean=%.2lf#pm%.2lf",hErrMartrix[i]->GetMean(),0.005+hErrMartrix[i]->GetMeanError()));
      lat->DrawLatexNDC(0.15,0.84,Form("RMS=%.2lf#pm%.2lf",hErrMartrix[i]->GetRMS(),0.005+hErrMartrix[i]->GetRMSError(2)));
      lat->DrawLatexNDC(0.15,0.78,"Bin-by-bin");
      lat->DrawLatexNDC(0.15,0.72,Form("Mean=%.2lf#pm%.2lf",hErrCF[i]->GetMean(),0.005+hErrCF[i]->GetMeanError()));
      lat->DrawLatexNDC(0.15,0.68,Form("RMS=%.2lf#pm%.2lf",hErrCF[i]->GetRMS(),0.005+hErrCF[i]->GetRMSError(2)));
      if(drawIBU){
        lat->DrawLatexNDC(0.15,0.62,Form("#tau=%.2lf",-1*(1.0 - TMath::Exp(0.5*0.5*rooUnfoldIter))));
        lat->DrawLatexNDC(0.15,0.58,Form("Mean=%.2lf#pm%.2lf",hErrIBU[i]->GetMean(),0.005+hErrIBU[i]->GetMeanError()));
        lat->DrawLatexNDC(0.15,0.54,Form("RMS=%.2lf#pm%.2lf",hErrIBU[i]->GetRMS(),0.005+hErrIBU[i]->GetRMSError()));
      }
    }
  

    for(auto ff : formats)
      ccErrCom[i]->Print(Form("%s/%s.%s",path.c_str(),ccErrCom[i]->GetName(),ff.c_str()),ff.c_str());

    //continue; 
    // Fill here the pulls 
    ccPullCom[i]=new TCanvas(Form("ccPullCom_%d_iter%d",i,rooUnfoldIter),"");{
      SetCanvasDefaults(ccPullCom[i]);
      ccPullCom[i]->cd();
      ccPullCom[i]->SetLogy();
      ccPullLeg[i]=DefLeg(0.75,0.75,0.9,0.95);
     
      ccPullLeg[i]->SetHeader(Form("#bf{%s}, bin %d",VarMap[fname].c_str(),i));
      
      hPullMatrix[i]=new TH1F(Form("h_pull_Matrix_bin%d",i),"",100,-5,+5);
      SetDef( hPullMatrix[i]);
      hPullMatrix[i]->GetYaxis()->SetRangeUser(1,pow(10,2+TMath::Log10(hPullMatrix[i]->GetBinContent(hPullMatrix[i]->GetMaximumBin()))));
      
      hPullMatrix[i]->SetMarkerStyle(8);
      t->Draw(Form("(matrix_sigma_bin%d[%d]-trueXS_sigma_bin%d)/Sigma_sigma_bin%d>>h_pull_Matrix_bin%d",i,iter,i,i,i),addedSel.c_str(),"");
      ccPullLeg[i]->AddEntry(hPullMatrix[i],"Matrix","PLE");
      
      hPullCF[i]=new TH1F(Form("h_pull_CF_bin%d",i),"",100,-5,+5);
      hPullCF[i]->GetXaxis()->SetTitle(Form("#scale[0.7]{#frac{#hat{#sigma}^{bin,%d}_{fit} - #sigma^{bin,%d}_{true}}{Var(#sigma^{bin,%d}_{true})}}",i,i,i));
      hPullCF[i]->GetYaxis()->SetTitle(Form("Pseudo experiments / %.2lf",hPullCF[i]->GetBinWidth(1)));
      
      SetDef(hPullCF[i]);
      t->Draw(Form("(CF_sigma_bin%d[%d]-trueXS_sigma_bin%d)/Sigma_sigma_bin%d>>h_pull_CF_bin%d",i,iter,i,i,i),addedSel.c_str(),"");
      ccPullLeg[i]->AddEntry(hPullCF[i],"bin-by-bin","L");
      hPullCF[i]->SetLineColor(kRed);
      
      hPullIBU[i]=new TH1F(Form("h_pull_IBU_bin%d",i),"",100,-5,+5);
      hPullIBU[i]->GetXaxis()->SetTitle(Form("#scale[0.7]{#frac{#hat{#sigma}^{bin,%d}_{fit} - #sigma^{bin,%d}_{true}}{Var(#sigma^{bin,%d}_{true})}}",i,i,i));
      hPullIBU[i]->GetYaxis()->SetTitle(Form("Pseudo experiments / %.2lf",hPullIBU[i]->GetBinWidth(1)));
      
      SetDef(hPullIBU[i]);
      t->Draw(Form("(IBU_sigma_bin%d[%d]-trueXS_sigma_bin%d)/Sigma_sigma_bin%d>>h_pull_IBU_bin%d",i,rooUnfoldIter,i,i,i),addedSel.c_str(),"");
      //ccPullLeg[i]->AddEntry(hPullIBU[i],Form("IBU %d",rooUnfoldIter+1),"L");
      if(drawIBU)
        ccPullLeg[i]->AddEntry(hPullIBU[i],Form("#tau %.2lf",tau),"L");
      hPullIBU[i]->SetLineColor(kBlue);
      hPullIBU[i]->SetLineStyle(1);
      
      
      hPullCF[i]->Draw("hist");
      if(drawIBU)
        hPullIBU[i]->Draw("hist same");
      hPullMatrix[i]->Draw("hist PE same");

      fPullMatrix[i]=new TF1(Form("f_pull_Matrix_bin%d",i),"gaus",-5,+5);
      fPullMatrix[i]->SetLineColor(kBlack);
      fPullMatrix[i]->SetLineWidth(2);

      fPullCF[i]=new TF1(Form("f_pull_CF_bin%d",i),"gaus",-5,+5);
      fPullCF[i]->SetLineColor(kRed);
      fPullCF[i]->SetLineWidth(2);
      if(drawIBU){
        fPullIBU[i]=new TF1(Form("f_pull_IBU_bin%d",i),"gaus",-5,+5);
        fPullIBU[i]->SetLineColor(kBlue);
        fPullIBU[i]->SetLineWidth(2);
      }
      hPullMatrix[i]->Fit(fPullMatrix[i]);
      fPullMatrix[i]->Draw("L same");
      hPullCF[i]->Fit(fPullCF[i]);
      fPullCF[i]->Draw("L same");

      if(drawIBU){
        hPullIBU[i]->Fit(fPullIBU[i]);
        fPullIBU[i]->Draw("L same");
      }
      ccPullLeg[i]->Draw();

      lat->DrawLatexNDC(0.15,0.92,"Matrix");
      lat->DrawLatexNDC(0.15,0.88,Form("#mu=%.2lf#pm%.2lf",fPullMatrix[i]->GetParameter(1),fPullMatrix[i]->GetParError(1)));
      lat->DrawLatexNDC(0.15,0.84,Form("#sigma=%.2lf#pm%.2lf",fPullMatrix[i]->GetParameter(2),fPullMatrix[i]->GetParError(2)));
      lat->DrawLatexNDC(0.15,0.78,"Bin-by-bin");
      lat->DrawLatexNDC(0.15,0.72,Form("#mu=%.2lf#pm%.2lf",fPullCF[i]->GetParameter(1),fPullCF[i]->GetParError(1)));
      lat->DrawLatexNDC(0.15,0.68,Form("#sigma=%.2lf#pm%.2lf",fPullCF[i]->GetParameter(2),fPullCF[i]->GetParError(2)));
      //lat->DrawLatexNDC(0.15,0.62,"IBU");
      if(drawIBU){
        lat->DrawLatexNDC(0.15,0.62,Form("#tau %.2lf",tau));
        lat->DrawLatexNDC(0.15,0.58,Form("#mu=%.2lf#pm%.2lf",fPullIBU[i]->GetParameter(1),fPullIBU[i]->GetParError(1)));
        lat->DrawLatexNDC(0.15,0.54,Form("#sigma=%.2lf#pm%.2lf",fPullIBU[i]->GetParameter(2),fPullIBU[i]->GetParError(2)));
      }
      
    }
  

    for(auto ff : formats)
      //cout<<Form("%/%s.%s",path.c_str(),ccPullCom[i]->GetName(),ff.c_str())<<endl;
      ccPullCom[i]->Print(Form("%s/%s.%s",path.c_str(),ccPullCom[i]->GetName(),ff.c_str()),ff.c_str());

    

    // plot the pull vs the reco error:
    ccPullRecoCom[i]=new TCanvas(Form("ccPullRecoCom_%d_iter%d",i,rooUnfoldIter),"");{
      SetCanvasDefaults(ccPullRecoCom[i]);
      ccPullRecoCom[i]->cd();
      ccPullRecoCom[i]->SetLogy();
      ccPullRecoLeg[i]=DefLeg(0.75,0.75,0.9,0.95);
     
      ccPullRecoLeg[i]->SetHeader(Form("#bf{%s}, bin %d",VarMap[fname].c_str(),i));
      
      hPullRecoMatrix[i]=new TH1F(Form("h_pullReco_Matrix_bin%d",i),"",100,-5,+5);
      SetDef( hPullRecoMatrix[i]);
      hPullRecoMatrix[i]->SetMarkerStyle(8);
      t->Draw(Form("(matrix_sigma_bin%d[%d]-trueXS_sigma_bin%d)/matrix_e_sigma_bin%d[%d]>>h_pullReco_Matrix_bin%d",i,iter,i,i,iter,i),
              Form("matrix_e_sigma_bin%d[%d]>0 && %s",i,iter,addedSel.c_str()),"");
      ccPullRecoLeg[i]->AddEntry(hPullRecoMatrix[i],"Matrix","PLE");
      
      hPullRecoCF[i]=new TH1F(Form("h_pullReco_CF_bin%d",i),"",100,-5,+5);
      hPullRecoCF[i]->GetXaxis()->SetTitle(Form("#scale[0.7]{#frac{#hat{#sigma}^{bin,%d}_{fit} - #sigma^{bin,%d}_{true}}{#delta#hat{#sigma}^{bin,%d}_{fit}}}",i,i,i));
      hPullRecoCF[i]->GetYaxis()->SetTitle(Form("Pseudo experiments / %.2lf",hPullCF[i]->GetBinWidth(1)));
      SetDef(hPullRecoCF[i]);
      t->Draw(Form("(CF_sigma_bin%d[%d]-trueXS_sigma_bin%d)/CF_e_sigma_bin%d[%d]>>h_pullReco_CF_bin%d",i,iter,i,i,iter,i),
              Form("CF_e_sigma_bin%d[%d]>0 &&%s",i,iter,addedSel.c_str()),"");
      ccPullRecoLeg[i]->AddEntry(hPullRecoCF[i],"bin-by-bin","L");
      hPullRecoCF[i]->SetLineColor(kRed);


      hPullRecoIBU[i]=new TH1F(Form("h_pullReco_IBU_bin%d",i),"",100,-5,+5);
      hPullRecoIBU[i]->GetXaxis()->SetTitle(Form("#scale[0.7]{#frac{#hat{#sigma}^{bin,%d}_{fit} - #sigma^{bin,%d}_{true}}{#delta#hat{#sigma}^{bin,%d}_{fit}}}",i,i,i));
      hPullRecoIBU[i]->GetYaxis()->SetTitle(Form("Pseudo experiments / %.2lf",hPullCF[i]->GetBinWidth(1)));
      SetDef(hPullRecoIBU[i]);
      t->Draw(Form("(IBU_sigma_bin%d[%d]-trueXS_sigma_bin%d)/IBU_e_sigma_bin%d[%d]>>h_pullReco_IBU_bin%d",i,rooUnfoldIter,i,i,rooUnfoldIter,i),
              Form("IBU_e_sigma_bin%d[%d]>0 &&%s",i,rooUnfoldIter,addedSel.c_str()),"");
      //ccPullRecoLeg[i]->AddEntry(hPullRecoIBU[i],Form("IBU %d",rooUnfoldIter+1),"L");
      if(drawIBU)
        ccPullRecoLeg[i]->AddEntry(hPullRecoIBU[i],Form("#tau %.2lf",tau),"L");
      hPullRecoIBU[i]->SetLineColor(kBlue);
      
      
      hPullRecoCF[i]->Draw("hist");
      hPullRecoMatrix[i]->Draw("hist PE same");
      if(drawIBU)
        hPullRecoIBU[i]->Draw("hist same");

      fPullRecoMatrix[i]=new TF1(Form("f_pullReco_Matrix_bin%d",i),"gaus",-5,+5);
      fPullRecoMatrix[i]->SetLineColor(kBlack);
      fPullRecoMatrix[i]->SetLineWidth(2);

      fPullRecoCF[i]=new TF1(Form("f_pullReco_CF_bin%d",i),"gaus",-5,+5);
      fPullRecoCF[i]->SetLineColor(kRed);
      fPullRecoCF[i]->SetLineWidth(2);

      fPullRecoIBU[i]=new TF1(Form("f_pullReco_IBU_bin%d",i),"gaus",-5,+5);
      fPullRecoIBU[i]->SetLineColor(kBlue);
      fPullRecoIBU[i]->SetLineWidth(2);

      
      hPullRecoMatrix[i]->Fit(fPullRecoMatrix[i]);
      fPullRecoMatrix[i]->Draw("L same");
      hPullRecoCF[i]->Fit(fPullRecoCF[i]);
      if(drawIBU)
        hPullRecoIBU[i]->Fit(fPullRecoIBU[i]);
      fPullRecoCF[i]->Draw("L same");
      ccPullRecoLeg[i]->Draw();
      if(drawIBU)
        fPullRecoIBU[i]->Draw("L same");
      ccPullRecoLeg[i]->Draw();
      

      lat->DrawLatexNDC(0.15,0.92,"Matrix");
      lat->DrawLatexNDC(0.15,0.88,Form("#mu=%.2lf#pm%.2lf",fPullRecoMatrix[i]->GetParameter(1),fPullRecoMatrix[i]->GetParError(1)));
      lat->DrawLatexNDC(0.15,0.84,Form("#sigma=%.2lf#pm%.2lf",fPullRecoMatrix[i]->GetParameter(2),fPullRecoMatrix[i]->GetParError(2)));
      lat->DrawLatexNDC(0.15,0.78,"Bin-by-bin");
      lat->DrawLatexNDC(0.15,0.72,Form("#mu=%.2lf#pm%.2lf",fPullRecoCF[i]->GetParameter(1),fPullRecoCF[i]->GetParError(1)));
      lat->DrawLatexNDC(0.15,0.68,Form("#sigma=%.2lf#pm%.2lf",fPullRecoCF[i]->GetParameter(2),fPullRecoCF[i]->GetParError(2)));
      //lat->DrawLatexNDC(0.15,0.64,"IBU");
      if(drawIBU){
        lat->DrawLatexNDC(0.15,0.64,Form("#tau %.2lf",tau));
        lat->DrawLatexNDC(0.15,0.60,Form("#mu=%.2lf#pm%.2lf",fPullRecoIBU[i]->GetParameter(1),fPullRecoIBU[i]->GetParError(1)));
        lat->DrawLatexNDC(0.15,0.56,Form("#sigma=%.2lf#pm%.2lf",fPullRecoIBU[i]->GetParameter(2),fPullRecoIBU[i]->GetParError(2)));
      }
    }
  

    for(auto ff : formats)
      //cout<<Form("%/%s.%s",path.c_str(),ccPullCom[i]->GetName(),ff.c_str())<<endl;
      ccPullRecoCom[i]->Print(Form("%s/%s.%s",path.c_str(),ccPullRecoCom[i]->GetName(),ff.c_str()),ff.c_str());




    
    // plot here the 2D 
    cc2D[i]=new TCanvas(Form("cc2D_%d_iter%d",i,rooUnfoldIter),"");{
      cc2D[i]->cd();
      TH1F *hh=new TH1F("hh","",100,-10,+10);
      t->Draw(Form("trueXS_sigma_bin%d>>hh",i),addedSel.c_str(),"");
      double mean=hh->GetMean();
      double rms=hh->GetRMS();
      trueMean[i]=mean;
      trueRMS[i]=rms;
      delete hh;

      int nq=1;
      Double_t xq[nq];  // position where to compute the quantiles in [0,1]
      Double_t yqCF[nq];
      Double_t yqIBU[nq];
      Double_t yq[nq];  // array to contain the quantiles
      xq[0]=0.5;
     
      

      TH1F *h2=new TH1F("h2","",100,0,2);
      t->Draw(Form("matrix_e_sigma_bin%d[%d]/trueXS_sigma_bin%d>>h2",i,iter,i),addedSel.c_str(),"");
      h2->GetQuantiles(nq,yq,xq);
      medianErrorMatrix[i]=h2->GetBinCenter(h2->GetMaximumBin());//h2->GetMean();//yq[0];
      delete h2;

      TH1F *h3=new TH1F("h3","",100,0,2);
      t->Draw(Form("CF_e_sigma_bin%d[%d]/trueXS_sigma_bin%d>>h3",i,iter,i),addedSel.c_str(),"");
      h3->GetQuantiles(nq,yqCF,xq);
      medianErrorCF[i]=h3->GetBinCenter(h3->GetMaximumBin());//h3->GetMean();//yqCF[0];

      delete h3;

      TH1F *h4=new TH1F("h4","",100,0,2);
      t->Draw(Form("IBU_e_sigma_bin%d[%d]/trueXS_sigma_bin%d>>h4",i,rooUnfoldIter,i),addedSel.c_str(),"");
      h4->GetQuantiles(nq,yqIBU,xq);
      medianErrorIBU[i]=h4->GetBinCenter(h4->GetMaximumBin());
      delete h4;

      

      h2DdiffMatrix[i]=new TH2F(Form("h2DdiffMatrix_%d",i),"",nBinsX,p1Range[0],p1Range[1],20,-10,10);
      //h2DdiffMatrix[i]->GetXaxis()->SetTitle(Form("#sigma^{bin,%d}_{true} [fb]",i));
      h2DdiffMatrix[i]->GetXaxis()->SetTitle("p_{1}");
      h2DdiffMatrix[i]->GetYaxis()->SetTitle(Form("#hat{#sigma}^{bin,%d}_{fit}-#sigma^{bin,%d}_{true}/#delta#sigma^{bin,%d}",i,i,i));
      h2DdiffMatrix[i]->SetLineColor(kBlack);
      t->Draw(Form("(matrix_sigma_bin%d[%d]-trueXS_sigma_bin%d)/matrix_e_sigma_bin%d[%d]:p1>>h2DdiffMatrix_%d",i,iter,i,i,iter,i),addedSel.c_str(),"box");
      h2DdiffMatrix[i]->SetLineColor(kBlack);
      
      h2DdiffCF[i]=new TH2F(Form("h2DdiffCF_%d",i),"",nBinsX,p1Range[0],p1Range[1],20,-10,10);
      h2DdiffCF[i]->SetLineColor(kRed);

      t->Draw(Form("(CF_sigma_bin%d[%d]-trueXS_sigma_bin%d)/CF_e_sigma_bin%d[%d]:p1>>h2DdiffCF_%d",i,iter,i,i,iter,i),addedSel.c_str(),"box same");

      
      h2DdiffIBU[i]=new TH2F(Form("h2DdiffIBU_%d",i),"",nBinsX,p1Range[0],p1Range[1],20,-10,10);
      h2DdiffIBU[i]->SetLineColor(kBlue);

      t->Draw(Form("(IBU_sigma_bin%d[%d]-trueXS_sigma_bin%d)/IBU_e_sigma_bin%d[%d]:p1>>h2DdiffIBU_%d",i,rooUnfoldIter,i,i,iter,i),addedSel.c_str(),"box same");
      h2DdiffIBU[i]->SetLineColor(kBlue);
    }


    ccProfiles[i]=new TCanvas(Form("ccProfiles_%d_iter%d",i,rooUnfoldIter),"");{
      SetCanvasDefaults(ccProfiles[i]);
      ccProfiles[i]->cd();
      ccProfilesLeg[i]=DefLeg(0.75,0.75,0.9,0.95);
      ccProfilesLeg[i]->SetHeader(Form("#bf{%s}, bin %d",VarMap[fname].c_str(),i));
      
      hProfilediffMatrix[i]=(TProfile*)h2DdiffMatrix[i]->ProfileX(Form("hProfilediffMatrix_%d",i),1,-1,"");
      SetDef(hProfilediffMatrix[i]);
      
      hProfilediffMatrix[i]->GetYaxis()->SetRangeUser(-3.2,+3.2);
      hProfilediffMatrix[i]->GetYaxis()->SetTitle(Form("<#hat{#sigma}^{bin,%d}_{fit}-#sigma^{bin,%d}_{true}/#delta#sigma^{bin,%d}_{true}>",i,i,i));
      hProfilediffMatrix[i]->Draw("PE");
      ccProfilesLeg[i]->AddEntry(hProfilediffMatrix[i],"Matrix","PLE");

      hProfilediffMatrix[i]->SetMarkerStyle(8);
      
      
      hProfilediffCF[i]=(TProfile*)h2DdiffCF[i]->ProfileX(Form("hProfilediffCF_%d",i),1,-1,"");
      SetDef(hProfilediffCF[i]);
      hProfilediffCF[i]->GetYaxis()->SetRangeUser(-1,+1);
      hProfilediffCF[i]->GetYaxis()->SetTitle(Form("<#hat{#sigma}^{bin,%d}_{fit}-#sigma^{bin,%d}_{true}/#delta#sigma^{bin,%d}_{true}>",i,i,i));
      hProfilediffCF[i]->SetMarkerStyle(25);
      hProfilediffCF[i]->SetLineColor(kRed);
      hProfilediffCF[i]->SetMarkerColor(kRed);
      hProfilediffCF[i]->Draw("PE SAME");
      ccProfilesLeg[i]->AddEntry(hProfilediffCF[i],"bin-by-bin","PLE");



      hProfilediffIBU[i]=(TProfile*)h2DdiffIBU[i]->ProfileX(Form("hProfilediffIBU_%d",i),1,-1,"");
      SetDef(hProfilediffIBU[i]);
      hProfilediffIBU[i]->GetYaxis()->SetRangeUser(-1,+1);
      hProfilediffIBU[i]->GetYaxis()->SetTitle(Form("<#hat{#sigma}^{bin,%d}_{fit}-#sigma^{bin,%d}_{true}/#delta#sigma^{bin,%d}_{true}>",i,i,i));
      hProfilediffIBU[i]->SetMarkerStyle(35);
      hProfilediffIBU[i]->SetLineColor(kBlue);
      hProfilediffIBU[i]->SetMarkerColor(kBlue);
      if(drawIBU)
        hProfilediffIBU[i]->Draw("PE SAME");
      //ccProfilesLeg[i]->AddEntry(hProfilediffIBU[i],Form("IBU %d",rooUnfoldIter+1),"PLE");
      if(drawIBU)
        ccProfilesLeg[i]->AddEntry(hProfilediffIBU[i],Form("#tau %.2lf",tau),"L");

      ccProfilesLeg[i]->Draw();

      
      //tLProfileHL[i]=new TLine(-2,(trueMean[i]-trueMean[i]*medianErrorCF[i])/trueMean[i],+2,(trueMean[i]-trueMean[i]*medianErrorCF[i])/trueMean[i]);
      tLProfileHL[i]=new TLine(p1Range[0],+1,p1Range[1],+1);
      tLProfileHL[i]->SetLineColor(kBlack);
      tLProfileHL[i]->SetLineWidth(2);
      tLProfileHL[i]->SetLineStyle(2);
      tLProfileHL[i]->SetLineColor(kRed);
      tLProfileHL[i]->Draw();

      //tLProfileHR[i]=new TLine(-2,(trueMean[i]+trueMean[i]*medianErrorCF[i])/trueMean[i],+2,(trueMean[i]+trueMean[i]*medianErrorCF[i])/trueMean[i]);
      tLProfileHR[i]=new TLine(p1Range[0],-1,p1Range[1],-1);
      tLProfileHR[i]->SetLineColor(kBlack);
      tLProfileHR[i]->SetLineWidth(2);
      tLProfileHR[i]->SetLineStyle(2);
      tLProfileHR[i]->SetLineColor(kRed);
      tLProfileHR[i]->Draw();
      
      
      tLProfileVL[i]=new TLine(-p1Range[0],(trueMean[i]-trueMean[i]*medianErrorMatrix[i])/trueMean[i],p1Range[1],(trueMean[i]-trueMean[i]*medianErrorMatrix[i])/trueMean[i]);
      tLProfileVL[i]->SetLineColor(kBlack);
      tLProfileVL[i]->SetLineWidth(2);
      tLProfileVL[i]->SetLineStyle(3);
      //tLProfileVL[i]->Draw();
      
      tLProfileVR[i]=new TLine(p1Range[0],(trueMean[i]+trueMean[i]*medianErrorMatrix[i])/trueMean[i],+p1Range[1],(trueMean[i]+trueMean[i]*medianErrorMatrix[i])/trueMean[i]);
      tLProfileVR[i]->SetLineColor(kBlack);
      tLProfileVR[i]->SetLineWidth(2);
      tLProfileVR[i]->SetLineStyle(3);
      //tLProfileVR[i]->Draw();



      tLProfileIBUL[i]=new TLine(p1Range[0],(trueMean[i]-trueMean[i]*medianErrorIBU[i])/trueMean[i],+p1Range[1],(trueMean[i]-trueMean[i]*medianErrorIBU[i])/trueMean[i]);
      tLProfileIBUL[i]->SetLineColor(kBlue);
      tLProfileIBUL[i]->SetLineWidth(2);
      tLProfileIBUL[i]->SetLineStyle(3);
      //if(drawIBU)
      //tLProfileIBUL[i]->Draw();


      tLProfileIBUR[i]=new TLine(p1Range[0],(trueMean[i]+trueMean[i]*medianErrorIBU[i])/trueMean[i],+p1Range[1],(trueMean[i]+trueMean[i]*medianErrorIBU[i])/trueMean[i]);
      tLProfileIBUR[i]->SetLineColor(kBlue);
      tLProfileIBUR[i]->SetLineWidth(2);
      tLProfileIBUR[i]->SetLineStyle(3);
      //if(drawIBU)
      //tLProfileIBUR[i]->Draw();
      

      
      ccProfilesLeg2[i]=DefLeg(0.7,0.2,0.95,0.3);
      ccProfilesLeg2[i]->SetHeader("68.3% CL");
      ccProfilesLeg2[i]->AddEntry(tLProfileVR[i],"Matrix","L");
      ccProfilesLeg2[i]->AddEntry(tLProfileHR[i],"bin-by-bin","L");
      //ccProfilesLeg2[i]->AddEntry(tLProfileIBUL[i],Form("IBU %d",rooUnfoldIter+1),"L");
      if(drawIBU)
        ccProfilesLeg2[i]->AddEntry(tLProfileIBUL[i],Form("#tau %.2lf",tau),"L");
      ccProfilesLeg2[i]->Draw();

     
      
    }
    for(auto ff : formats)
      ccProfiles[i]->Print(Form("%s/%s.%s",path.c_str(),ccProfiles[i]->GetName(),ff.c_str()),ff.c_str());

    //no updates for BDT rw from here on 

    
    // Dump the interpolated error values
    cout<<" bin "<<i<<" bias bin-by-bin "<<hProfilediffCF[i]->Interpolate(trueMean[i]-trueMean[i]*medianErrorCF[i])<<" , "<<hProfilediffCF[i]->Interpolate(trueMean[i]+trueMean[i]*medianErrorCF[i])<<" "<<" Matrix "<<hProfilediffMatrix[i]->Interpolate(trueMean[i]-trueMean[i]*medianErrorMatrix[i])<<" , "<<hProfilediffMatrix[i]->Interpolate(trueMean[i]+trueMean[i]*medianErrorMatrix[i])<<" regularized tau "<< tau <<hProfilediffIBU[i]->Interpolate(trueMean[i]-trueMean[i]*medianErrorIBU[i])<<" , "<<hProfilediffIBU[i]->Interpolate(trueMean[i]+trueMean[i]*medianErrorIBU[i])<<endl;


    Tcenterx[i]=trueMean[i];

    Ttau[i]=tau;
    // get the min and the max of the sigma

    (*TerrorYH_methods["matrix"])[i]=getMaxVal(hProfilediffMatrix[i]);//*medianErrorMatrix[i];
    (*TerrorYL_methods["matrix"])[i]=getMinVal(hProfilediffMatrix[i]);//*medianErrorMatrix[i];
    
    (*TerrorYH_methods["IBU"])[i]=getMaxVal(hProfilediffIBU[i]);//*medianErrorIBU[i];
    (*TerrorYL_methods["IBU"])[i]=getMinVal(hProfilediffIBU[i]);//*medianErrorIBU[i];
    
    (*TerrorYH_methods["CF"])[i]=getMaxVal(hProfilediffCF[i]);//*medianErrorCF[i];
    (*TerrorYL_methods["CF"])[i]=getMinVal(hProfilediffCF[i]);//*medianErrorCF[i];
    
    /*
    // In reality one need to use the CF error to evaluate the bias as it's closer to Poisson(Nobserved). 
    (*TerrorYH_methods["matrix"])[i]=hProfilediffMatrix[i]->Interpolate(trueMean[i]+trueMean[i]*medianErrorMatrix[i]);
    (*TerrorYL_methods["matrix"])[i]=hProfilediffMatrix[i]->Interpolate(trueMean[i]-trueMean[i]*medianErrorMatrix[i]);
    
    (*TerrorYH_methods["IBU"])[i]=hProfilediffIBU[i]->Interpolate(trueMean[i]+trueMean[i]*medianErrorIBU[i]);
    (*TerrorYL_methods["IBU"])[i]=hProfilediffIBU[i]->Interpolate(trueMean[i]-trueMean[i]*medianErrorIBU[i]);
    
    (*TerrorYH_methods["CF"])[i]=hProfilediffCF[i]->Interpolate(trueMean[i]+trueMean[i]*medianErrorMatrix[i]);
    (*TerrorYL_methods["CF"])[i]=hProfilediffCF[i]->Interpolate(trueMean[i]-trueMean[i]*medianErrorCF[i]);
    */
      
    centerx.push_back(trueMean[i]);
    centery.push_back(0);
    // up and down variations may be swapped 
    errorYL_matrix.push_back( fabs(hProfilediffMatrix[i]->Interpolate(trueMean[i]-trueMean[i]*medianErrorMatrix[i])));
    errorYH_matrix.push_back( fabs(hProfilediffMatrix[i]->Interpolate(trueMean[i]+trueMean[i]*medianErrorMatrix[i])));

    errorYL_IBU.push_back( fabs(hProfilediffIBU[i]->Interpolate(trueMean[i]-trueMean[i]*medianErrorIBU[i])));
    errorYH_IBU.push_back( fabs(hProfilediffIBU[i]->Interpolate(trueMean[i]+trueMean[i]*medianErrorIBU[i])));
    
    errorYL_CF.push_back( fabs(hProfilediffCF[i]->Interpolate(trueMean[i]-trueMean[i]*medianErrorCF[i])));
    errorYH_CF.push_back( fabs(hProfilediffCF[i]->Interpolate(trueMean[i]+trueMean[i]*medianErrorCF[i])));
    
    
    
    if(useCustomShapeDrawUncert){
    // Fill in the plots of shape
    for(std::map<int, vector<double>>::iterator it=variationsPerBin.begin(); it!=variationsPerBin.end(); it++){
      const int ii=(*it).first;
      //const double val=(1.0+ (*it).second) * trueMean[i]*trueMean[i];
      //const double val=pow((*it).second,i)*trueMean[i];
      const double val=(*it).second.at(i)*trueMean[i];
      int ibinN=hProfilediffMatrix[i]->FindBin(val);
      double xxsec=hProfilediffMatrix[i]->GetBinContent(ibinN);
      hDiffXsecShapeMatrix[ii]->SetBinContent(i+1,xxsec);
      hDiffXsecShapeMatrix_up[ii]->SetBinContent(i+1,xxsec);

      xxsec=hProfilediffCF[i]->GetBinContent(ibinN);
      hDiffXsecShapeCF[ii]->SetBinContent(i+1,xxsec);
      hDiffXsecShapeCF_up[ii]->SetBinContent(i+1,xxsec);

      xxsec=hProfilediffIBU[i]->GetBinContent(ibinN);
      hDiffXsecShapeIBU[ii]->SetBinContent(i+1,xxsec);
      hDiffXsecShapeIBU_up[ii]->SetBinContent(i+1,xxsec);
    }}

    else if (useRanomVariationsOfAllBins){
      if(i == nBinsMap[fname]-1 ){
        cout<<"Evaluate correlated bias "<<endl;
        cout<<"Bin iteration "<<i<<endl;
        // number of randomisation option with correlated variations 
        int nrandomVars=nBinsX;// 500;//100;//20;// 500;//100;
        //TF1 *fvar=new TF1("fvar",polyN,1,nBinsMap[fname],nBinsMap[fname]);
	//fvar->FixParameter(0,nBinsMap[fname]-1);

	// just p1
	TF1 *fvar=new TF1("fvar",polyN,1,nBinsMap[fname],nBinsMap[fname]);
	fvar->FixParameter(0,1);

	std::map<string, vector <double>> finalerrorsForVariation;
        std::map <string, double> worstChi2;
        
        for( vector<string>::iterator m=methods.begin(); m<methods.end(); m++){
          for ( int ll=1; ll<nBinsMap[fname]+1; ll++){
            finalerrorsForVariation[(*m)].push_back(0);
            worstChi2[(*m)]=0;
          }}
        
        
        for( int ii=0; ii<nrandomVars; ii++){
          cout<<"[Correlated variation] Considering correlated variaiton  "<<ii<<endl;
          // create polinomial for deternimining the variation 
          // set the random coefficient values 
          //for( int p=0; p<3; p++){
            ////if( p==2)
            //fvar->SetParameter(p,rand.Uniform(-0.1,+0.1));
            ////fvar->SetParameter(p,rand.Uniform(-0.5,0.5));
            ////else
            ////fvar->SetParameter(p,0);
            //fvar->SetParName(p,Form("p_%d",p));
            //cout<<"Parameters are "<<p<<" "<<fvar->GetParameter(p)<<" ";
          //}
          cout<<"Poly is :";
          //for( int p=1; p<nBinsMap[fname]; p++){
            ////fvar->SetParameter(p,rand.Uniform(-0.1,+0.1));
            //fvar->SetParameter(p,rand.Uniform(-0.5,+0.5));
            //fvar->SetParName(p,Form("p_%d",p));
            //cout<<"p"<<p<<": "<<fvar->GetParameter(p)<<" ";
	  //}
	  fvar->SetParameter(1,0);
	  fvar->SetParameter(2,p1Range[0]+fabs(p1Range[1]-p1Range[0])/nBinsX); 
	  //fvar->SetParameter(2,rand.Uniform(p1Range[0],p1Range[1]));
	  
          cout<<endl;
          // create the selection of true cross sections
	  /*
          string selection="(";
          for( int ii=0; ii<nBinsMap[fname]; ii++){
            if(ii>0) selection+=" && ";
	    selection+=Form("( (trueXS_sigma_bin%d/%.3lf> %.3lf && trueXS_sigma_bin%d/%.3lf < %.3lf) || (trueXS_sigma_bin%d/%.3lf > %.3lf && trueXS_sigma_bin%d/%.3lf < %.3lf) )",
                            ii,trueMean[ii],fvar->Eval(ii)*0.8,ii,trueMean[ii],fvar->Eval(ii)*1.2,ii,trueMean[ii],fvar->Eval(ii)*0.8,ii,trueMean[ii],fvar->Eval(ii)*1.2);
	  }
	  selection+=")";
	  */
	  // just select tranches of p1
	  string selection;
	  selection+=Form("p1 > %.3lf && p1 <  %.3lf",fvar->GetParameter(2)-fabs(p1Range[1]-p1Range[0])/nBinsX,fvar->GetParameter(2)+fabs(p1Range[1]-p1Range[0])/nBinsX);

	  //if(fvar->GetParameter(2)-fvar->GetParameter(2)*4*10./nrandomVars < fvar->GetParameter(2)+fvar->GetParameter(2)*4*10./nrandomVars ) 
	  //selection=Form("p1 > %.3lf && p1 <  %.3lf",fvar->GetParameter(2)-fvar->GetParameter(2)*4*10./nrandomVars,fvar->GetParameter(2)+fvar->GetParameter(2)*4*10./nrandomVars);
	  //else
	  //selection=Form("p1 < %.3lf && p1 >  %.3lf",fvar->GetParameter(2)-fvar->GetParameter(2)*4*10./nrandomVars,fvar->GetParameter(2)+fvar->GetParameter(2)*4*10./nrandomVars);

	  
	  std::map<string,vector <double>>  errorsForVariation; 
        
	  cout<<"Selection "<<selection<<endl;
        
        
        // Create histogram list for all correlated variations 
        // loop over all variations
        std::map<string,TH2F*> h2DVarCorr;
        std::map<string,TProfile*> hProfileCorr;
        std::map<string, TH1F*> h1DVarCorr;
        std::map<string, TH1F*> h1DVarCorrErr;

        std::map <string, double> chi2;
        for( vector<string>::iterator m=methods.begin(); m<methods.end(); m++){

          for ( int ll=0; ll<nBinsMap[fname]; ll++){
            cout<<"[Correlated variation] Considring bin "<<ll<<" and method " << (*m).c_str() << endl;
            h2DVarCorr[(*m)]=new TH2F(Form("h2DVarCorr_%s_%d",(*m).c_str(),ll),"",nBinsX,p1Range[0],p1Range[1],20,-10,+10);
            h2DVarCorr[(*m)]->GetXaxis()->SetTitle(Form("#p1^{bin,%d}_{true} [fb]",ll));
            h2DVarCorr[(*m)]->GetYaxis()->SetTitle(Form("#hat{#sigma}^{bin,%d}_{fit}-#sigma^{bin,%d}_{true}/#delta#sigma^{bin,%d}",ll,ll,ll));
            h2DVarCorr[(*m)]->SetLineColor(kBlack);
            
            t->Draw(Form("(%s_sigma_bin%d[%d]-trueXS_sigma_bin%d)/%s_e_sigma_bin%d[%d]:p1>>h2DVarCorr_%s_%d",
                         (*m).c_str(),ll,iter,ll,(*m).c_str(),ll,iter,(*m).c_str(),ll),selection.c_str(),"box");

	    cout<<"Ok 1"<<endl;
	    
            hProfileCorr[(*m)]=(TProfile*)h2DVarCorr[(*m)]->ProfileX(Form("hProfileCorr%s_%d",(*m).c_str(),ll),1,-1,"");
            SetDef(hProfileCorr[(*m)]);

	    cout<<"Ok 2"<<endl;
                                      
            h1DVarCorr[(*m)]=new TH1F(Form("h1DVarCorr_%s_%d",(*m).c_str(),ll),"",50,-5,5);
            h1DVarCorr[(*m)]->GetXaxis()->SetTitle(Form("#hat{#sigma}^{bin,%d}_{fit}-#sigma^{bin,%d}_{true}/#delta#sigma^{bin,%d}_{true}",ll,ll,ll));
            h1DVarCorr[(*m)]->GetYaxis()->SetTitle("Pseudo experiments");
            h1DVarCorr[(*m)]->SetLineColor(kBlack);
            
            t->Draw(Form("(%s_sigma_bin%d[%d]-trueXS_sigma_bin%d)/%s_e_sigma_bin%d[%d]>>h1DVarCorr_%s_%d",
                         (*m).c_str(),ll,iter,ll,(*m).c_str(),ll,iter,(*m).c_str(),ll),selection.c_str());
	    
	    cout<<"Ok 3 --> "<<Form("(%s_sigma_bin%d[%d]-trueXS_sigma_bin%d)/%s_e_sigma_bin%d[%d]:p1>>h1DVarCorr_%s_%d",
				    (*m).c_str(),ll,iter,ll,(*m).c_str(),ll,iter,(*m).c_str(),ll)<<endl;
	    
            h1DVarCorrErr[(*m)]=new TH1F(Form("h1DVarCorrErr_%s_%d",(*m).c_str(),ll),"",500,-100,100);
            h1DVarCorrErr[(*m)]->GetXaxis()->SetTitle(Form("#p1^{bin,%d}_{true} [fb]",ll));
            h1DVarCorrErr[(*m)]->GetYaxis()->SetTitle("Pseudo experiments");
            h1DVarCorrErr[(*m)]->SetLineColor(kBlack);
          
            t->Draw(Form("%s_e_sigma_bin%d[%d]>>h1DVarCorrErr_%s_%d",
                         (*m).c_str(),ll,iter,(*m).c_str(),ll),selection.c_str());

	    cout<<"Ok 4 --> "<<Form("%s_e_sigma_bin%d[%d]>>h1DVarCorrErr_%s_%d",
				    (*m).c_str(),ll,iter,(*m).c_str(),ll)<<endl;
	    

            cout<<"Bias for this bin "<<h1DVarCorr[(*m)]->GetMean()<<" Integral "<<h1DVarCorr[(*m)]->Integral()<<" RMS "<<h1DVarCorr[(*m)]->GetRMS()<<endl;
            cout<<"selection "<<selection<<endl;

	    errorsForVariation[(*m)].push_back(h1DVarCorr[(*m)]->GetMean());

            double avErr=h1DVarCorrErr[(*m)]->GetMean();
            
            chi2[(*m)]+=pow( (h1DVarCorr[(*m)]->GetMean()-SM_xsec[i]) / avErr,2);

            //if( TMath::Abs(errorsForVariation[(*m)][ll]) > TMath::Abs(finalerrorsForVariation[(*m)][ll])) finalerrorsForVariation[(*m)][ll]= errorsForVariation[(*m)][ll];
            //cout<<"Error for bin "<<ll<<" Bias is for type "<< (*m)<<" " <<errorsForVariation[(*m)].back()<<" biggest variation "<<finalerrorsForVariation[(*m)][ll]<<endl;

            // Prune out all variation that go above the one sigma of expected uncertainty.
            //if( TMath::Abs(h1DVarCorr[(*m)]->GetMean()) > medianErrorCF[i] || h1DVarCorr[(*m)]->Integral() < 100 ) chi2[(*m)]=0;
            if(h1DVarCorr[(*m)]->Integral() < 100 ) chi2[(*m)]=0;
             

            
             delete h2DVarCorr[(*m)];
             delete hProfileCorr[(*m)];
             delete h1DVarCorr[(*m)];
             delete h1DVarCorrErr[(*m)];
            }
          // Decide if this variation has the maximal bias 

          if ( chi2[(*m)] >= worstChi2[(*m)] ) {

            worstChi2[(*m)] = chi2[(*m)];
            for ( int ll=0; ll<nBinsMap[fname]; ll++)
              finalerrorsForVariation[(*m)][ll] = errorsForVariation[(*m)][ll]; 
          }
          
        }//end loop over methods 
        
      }//end random variations 
      cout<<"[Correlated variation] Now filling the histogram"<<endl;
      
      for(std::map<int, vector<double>>::iterator it=variationsPerBin.begin(); it!=variationsPerBin.end(); it++){
        const int ii=(*it).first;
        for ( int ll=0; ll<nBinsMap[fname]; ll++){
        hDiffXsecShapeMatrix[ii]->SetBinContent(ll+1,finalerrorsForVariation["matrix"][ll]);
        hDiffXsecShapeMatrix_up[ii]->SetBinContent(ll+1,finalerrorsForVariation["matrix"][ll]);
 
        hDiffXsecShapeCF[ii]->SetBinContent(ll+1,finalerrorsForVariation["CF"][ll]);
        hDiffXsecShapeCF_up[ii]->SetBinContent(ll+1,finalerrorsForVariation["CF"][ll]);

        hDiffXsecShapeIBU[ii]->SetBinContent(ll+1,finalerrorsForVariation["IBU"][ll]);
        hDiffXsecShapeIBU_up[ii]->SetBinContent(ll+1,finalerrorsForVariation["IBU"][ll]);
        }
      }

      delete fvar;
      }}



    
    else {
      for(std::map<int, vector<double>>::iterator it=variationsPerBin.begin(); it!=variationsPerBin.end(); it++){
        const int ii=(*it).first;
        hDiffXsecShapeMatrix[ii]->SetBinContent(i+1,(*TerrorYL_methods["matrix"])[i]);
        hDiffXsecShapeMatrix_up[ii]->SetBinContent(i+1,(*TerrorYH_methods["matrix"])[i]);

        hDiffXsecShapeCF[ii]->SetBinContent(i+1,(*TerrorYL_methods["CF"])[i]);
        hDiffXsecShapeCF_up[ii]->SetBinContent(i+1,(*TerrorYH_methods["CF"])[i]);

        hDiffXsecShapeIBU[ii]->SetBinContent(i+1,(*TerrorYL_methods["IBU"])[i]);
        hDiffXsecShapeIBU_up[ii]->SetBinContent(i+1,(*TerrorYH_methods["IBU"])[i]);
      }
    }
  

    // Need to add the belts for the baysian.

    // Construct the belts for the matrix 
    vector <double> xx;
    vector <double> yy;
    vector <double> yy_eHi;
    vector <double> yy_eLow;
      
    vector <double> yy_rM_eHi;
    vector <double> yy_rM_eLow;

    const int nBinsTrueBelt=20;
    const int nBinsRecoBelt=20;

    TCanvas *ctmp = new TCanvas("ctmp","");{
      // make the confidence intervals
      TH2F *hbeltBase=new TH2F("hbeltBase","",nBinsRecoBelt,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i],nBinsTrueBelt,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i]);
      //t->Draw(Form("trueXS_sigma_bin%d_incl:matrix_sigma_bin%d_incl[%d]>>hbeltBase",i,i,iter),"","colz");
      t->Draw(Form("matrix_sigma_bin%d[%d]:trueXS_sigma_bin%d>>hbeltBase",i,iter,i),"","colz");

      
      TH2F *hFitErrorReco=new TH2F("hFitErrorReco","",nBinsRecoBelt,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i],nBinsTrueBelt,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i]);
      //t->Draw(Form("matrix_e_sigma_bin%d_incl[%d]:trueXS_sigma_bin%d_incl>>hFitErrorReco",i,iter,i),"","");
      //t->Draw(Form("2*matrix_e_sigma_bin%d_incl[%d]+matrix_sigma_bin%d_incl[%d]:trueXS_sigma_bin%d_incl>>hFitErrorReco",i,iter,i,iter,i),"","");
      t->Draw(Form("trueXS_sigma_bin%d:-matrix_e_sigma_bin%d[%d]+matrix_sigma_bin%d[%d]>>hFitErrorReco",i,i,iter,i,iter),"","");
      
      hBFitMatrix[i]=hFitErrorReco;
      
      
      for(int bin=1; bin<hbeltBase->GetXaxis()->GetNbins()+1;bin++){
        TH1F *hproj=(TH1F*)hbeltBase->ProjectionY("hproj",bin,bin+4);
        xx.push_back(hbeltBase->GetXaxis()->GetBinCenter(bin));
        vector<double> mcl=getCLSlide(hproj);
        //yy.push_back(hbeltBase->GetXaxis()->GetBinCenter(bin));
        yy.push_back(hproj->GetMean());
        
        yy_eLow.push_back(mcl[0]);
        yy_eHi.push_back(mcl[1]);
        delete hproj;
        
        //TH2F *hFitErrorReco=new TH2F("hFitErrorReco","",20,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i],20,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i]);
        //t->Draw(Form("matrix_e_sigma_bin%d_incl[%d]:matrix_sigma_bin%d_incl[%d]>>hFitErrorReco",i,iter,i,iter),
        //     Form("trueXS_sigma_bin%d_incl>%lf && trueXS_sigma_bin%d_incl<=%lf",i,yy_eLow.back(),i,yy_eHi.back()),
        //      "colz");
        
        //TH1F *hFitErrorReco=new TH1F("hFitErrorReco","",20,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i]);
        //t->Draw(Form("CF_e_sigma_bin%d_incl[%d]",i,iter),
        //      Form("trueXS_sigma_bin%d_incl>%lf && trueXS_sigma_bin%d_incl<=%lf",i,hbeltBase->GetXaxis()->GetBinLowEdge(bin),i,hbeltBase->GetXaxis()->GetBinLowEdge(bin+1)),"");
        //xx.back(),i,xx.back()+hbeltBase->GetXaxis()->GetBinWidth(1)),"");
        //Form("trueXS_sigma_bin%d_incl>%lf && trueXS_sigma_bin%d_incl<=%lf",i,yy_eLow.back(),i,yy_eHi.back()),"");
        
        //t->Draw(Form("matrix_e_sigma_bin%d_incl[%d]:trueXS_sigma_bin%d_incl>>hFitErrorReco",i,iter,i),//"",
        //Form("trueXS_sigma_bin%d_incl>%lf && trueXS_sigma_bin%d_incl<=%lf",i,hbeltBase->GetYaxis()->GetBinLowEdge(bin),i,hbeltBase->GetYaxis()->GetBinLowEdge(bin+1)),
        //"colz");
        
        TH1F *herrProj=(TH1F*)hFitErrorReco->ProjectionY("herrProj",bin,bin+4);
        yy_rM_eHi.push_back(yy.back()+  herrProj->GetBinLowEdge(herrProj->GetMaximumBin()));
        yy_rM_eLow.push_back(yy.back()- herrProj->GetBinLowEdge(herrProj->GetMaximumBin()));
        
        //yy_rM_eHi.push_back(trueMean[i]+trueRMS[i]);
        //yy_rM_eLow.push_back(trueMean[i]-trueRMS[i]);

        //vector <double> kx=getCLSlide(hFitErrorReco,0.5);
        //yy_rM_eHi.push_back(yy.back()-kx[0]);
        //yy_rM_eLow.push_back(yy.back()+kx[1]);
        delete herrProj;
        //delete hFitErrorReco;
        
        cout<<" Reco "<<xx.back()<<" true interval ["<< yy_eLow.back()<<" , "<<yy_eHi.back()<<"] mean "<<yy.back()<<" fit errors ["<<yy_rM_eLow.back()<<" , "<<yy_rM_eHi.back()<<"]"<<endl;
      }
      
      delete hbeltBase;
      //delete hFitErrorReco;
      
    }
    delete ctmp;
      
    // Construct the belts for the CF 
    vector <double> xx_CF;
    vector <double> yy_CF;
    vector <double> yy_eHi_CF;
    vector <double> yy_eLow_CF;
      
    vector <double> yy_rM_eHi_CF;
    vector <double> yy_rM_eLow_CF;


    TCanvas *ctmp2 = new TCanvas("ctmp2","");{
      

      // make the confidence intervals
      TH2F *hbeltBaseCF=new TH2F("hbeltBaseCF","",nBinsRecoBelt,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i],nBinsTrueBelt,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i]);
      t->Draw(Form("CF_sigma_bin%d[%d]:trueXS_sigma_bin%d>>hbeltBaseCF",i,iter,i),"","colz");
      
      TH2F *hFitErrorRecoCF=new TH2F("hFitErrorRecoCF","",nBinsRecoBelt,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i],nBinsTrueBelt,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i]);

      t->Draw(Form("trueXS_sigma_bin%d:-CF_e_sigma_bin%d[%d]+CF_sigma_bin%d[%d]>>hFitErrorRecoCF",i,i,iter,i,iter),"","");
      //t->Draw(Form("trueXS_sigma_bin%d_incl:CF_sigma_bin%d_incl[%d]>>hFitErrorRecoCF",i,i,iter),"","");
      
      hBFitCF[i]=hFitErrorRecoCF;
      
      for(int bin=1; bin<hbeltBaseCF->GetXaxis()->GetNbins()+1;bin++){
        TH1F *hproj=(TH1F*)hbeltBaseCF->ProjectionY("hprojCF",bin,bin+4);
        xx_CF.push_back(hbeltBaseCF->GetXaxis()->GetBinCenter(bin));
        vector<double> mcl=getCLSlide(hproj);
        yy_CF.push_back(hproj->GetMean());
        
        yy_eLow_CF.push_back(mcl[0]);
        yy_eHi_CF.push_back(mcl[1]);
        delete hproj;

        TH1F *herrProj=(TH1F*)hFitErrorRecoCF->ProjectionY("herrProjCF",bin,bin+4);
        yy_rM_eHi_CF.push_back(yy_CF.back()+  herrProj->GetBinLowEdge(herrProj->GetMaximumBin()));
        yy_rM_eLow_CF.push_back(yy_CF.back()- herrProj->GetBinLowEdge(herrProj->GetMaximumBin()));
        
        delete herrProj;
        cout<<" Reco "<<xx_CF.back()<<" true interval ["<< yy_eLow_CF.back()<<" , "<<yy_eHi_CF.back()<<"] mean "<<yy_CF.back()<<" fit errors ["<<yy_rM_eLow_CF.back()<<" , "<<yy_rM_eHi_CF.back()<<"]"<<endl;
      }
      delete hbeltBaseCF;
      //delete hFitErrorRecoCF;
    }
    delete ctmp2;



    
    // Construct the belts for the IBU
    vector <double> xx_IBU;
    vector <double> yy_IBU;
    vector <double> yy_eHi_IBU;
    vector <double> yy_eLow_IBU;
    
    vector <double> yy_rM_eHi_IBU;
    vector <double> yy_rM_eLow_IBU;


    TCanvas *ctmp3 = new TCanvas("ctmp3","");{
      // make the confidence intervals
      TH2F *hbeltBaseIBU=new TH2F("hbeltBaseIBU","",nBinsRecoBelt,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i],nBinsTrueBelt,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i]);
      t->Draw(Form("IBU_sigma_bin%d[%d]:trueXS_sigma_bin%d>>hbeltBaseIBU",i,iter,i),"","colz");
      
      TH2F *hFitErrorRecoIBU=new TH2F("hFitErrorRecoIBU","",nBinsRecoBelt,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i],nBinsTrueBelt,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i]);
      //t->Draw(Form("IBU_e_sigma_bin%d_incl[%d]:trueXS_sigma_bin%d_incl>>hFitErrorRecoIBU",i,iter,i),"","");
      //t->Draw(Form("2*IBU_e_sigma_bin%d_incl[%d]+IBU_sigma_bin%d_incl[%d]:trueXS_sigma_bin%d_incl>>hFitErrorRecoIBU",i,iter,i,iter,i),"","");
      t->Draw(Form("trueXS_sigma_bin%d:-IBU_e_sigma_bin%d[%d]+IBU_sigma_bin%d[%d]>>hFitErrorRecoIBU",i,i,iter,i,iter),"","");
      
      hBFitIBU[i]=hFitErrorRecoIBU;

      
      for(int bin=1; bin<hbeltBaseIBU->GetXaxis()->GetNbins()+1;bin++){
        TH1F *hproj=(TH1F*)hbeltBaseIBU->ProjectionY("hprojIBU",bin,bin+4);
        xx_IBU.push_back(hbeltBaseIBU->GetXaxis()->GetBinCenter(bin));
        vector<double> mcl=getCLSlide(hproj);
        yy_IBU.push_back(hproj->GetMean());
        
        yy_eLow_IBU.push_back(mcl[0]);
        yy_eHi_IBU.push_back(mcl[1]);
        delete hproj;

        TH1F *herrProj=(TH1F*)hFitErrorRecoIBU->ProjectionY("herrProjIBU",bin,bin+4);
        yy_rM_eHi_IBU.push_back(yy_IBU.back()+  herrProj->GetBinLowEdge(herrProj->GetMaximumBin()));
        yy_rM_eLow_IBU.push_back(yy_IBU.back()- herrProj->GetBinLowEdge(herrProj->GetMaximumBin()));
        
        delete herrProj;
        cout<<" Reco IBU"<<xx_IBU.back()<<" true interval ["<< yy_eLow_IBU.back()<<" , "<<yy_eHi_IBU.back()<<"] mean "<<yy_IBU.back()<<" fit errors ["<<yy_rM_eLow_IBU.back()<<" , "<<yy_rM_eHi_IBU.back()<<"]"<<endl;
      }
      delete hbeltBaseIBU;
      //delete hFitErrorRecoIBU;
    }
    delete ctmp3;
    

   
    vLineEXSM[i]=new TLine(trueMean[i],trueMean[i]-3*trueRMS[i],trueMean[i],trueMean[i]+3*trueRMS[i]);
    vLineEXSM[i]->SetLineColor(kBlack);
    vLineEXSM[i]->SetLineWidth(3);
    
      
    ccBelts[i]=new TCanvas(Form("ccBelts_%d_iter%d",i,rooUnfoldIter),"");{
      SetCanvasDefaults(ccBelts[i]);
      ccBelts[i]->cd();
      hBeltsDraw[i]=new TH1F(Form("hBeltsDraw_%d_iter%d",i,iter),"",100,trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i]);
      hBeltsDraw[i]->GetYaxis()->SetRangeUser(trueMean[i]-3*trueRMS[i],trueMean[i]+3*trueRMS[i]);
      hBeltsDraw[i]->GetYaxis()->SetTitle(Form("#sigma^{bin,%d}_{true} [fb]",i));
      hBeltsDraw[i]->GetXaxis()->SetTitle(Form("#sigma^{bin,%d}_{fit} [fb]",i));
        
        SetDef(hBeltsDraw[i]);
        
        hBeltsDraw[i]->SetLineColor(kWhite);
        hBeltsDraw[i]->Draw("hist");

        hBFitCF[i]->SetLineColor(kRed);
        hBFitCF[i]->Draw("box same");
       

        hBFitMatrix[i]->SetLineColor(kBlack);
        hBFitMatrix[i]->Draw("box same");
        

        hBFitIBU[i]->SetLineColor(kBlue);
        hBFitIBU[i]->Draw("box same");
       
        
        

        grBeltMatrixCenter[i]=new TGraphErrors(xx.size(),&xx[0],&yy[0],0,0);
        grBeltMatrixCenter[i]->SetLineColor(kBlack);
        grBeltMatrixCenter[i]->SetLineStyle(2);
        grBeltMatrixCenter[i]->SetLineWidth(3);
        grBeltMatrixCenter[i]->Draw("C");
      
        grBeltMatrixLow[i]=new TGraphErrors(xx.size(),&xx[0],&yy_eLow[0],0,0);
        grBeltMatrixLow[i]->SetLineColor(kBlack);
        grBeltMatrixLow[i]->SetLineWidth(3);
        grBeltMatrixLow[i]->Draw("C");
      
        grBeltMatrixHigh[i]=new TGraphErrors(xx.size(),&xx[0],&yy_eHi[0],0,0);
        grBeltMatrixHigh[i]->SetLineColor(kBlack);
        grBeltMatrixHigh[i]->SetLineWidth(3);
        grBeltMatrixHigh[i]->Draw("C");

        // the IBU belt
        
        grBeltIBUCenter[i]=new TGraphErrors(xx.size(),&xx_IBU[0],&yy_IBU[0],0,0);
        grBeltIBUCenter[i]->SetLineColor(kBlue);
        grBeltIBUCenter[i]->SetLineStyle(2);
        grBeltIBUCenter[i]->SetLineWidth(3);
        grBeltIBUCenter[i]->Draw("C");

        
        grBeltIBULow[i]=new TGraphErrors(xx.size(),&xx_IBU[0],&yy_eLow_IBU[0],0,0);
        grBeltIBULow[i]->SetLineColor(kBlue);
        grBeltIBULow[i]->SetLineWidth(3);
        grBeltIBULow[i]->Draw("C");
        
        grBeltIBUHigh[i]=new TGraphErrors(xx.size(),&xx_IBU[0],&yy_eHi_IBU[0],0,0);
        grBeltIBUHigh[i]->SetLineColor(kBlue);
        grBeltIBUHigh[i]->SetLineWidth(3);
        grBeltIBUHigh[i]->Draw("C");
      

        // Draw the CF

        grBeltCFCenter[i]=new TGraphErrors(xx_CF.size(),&xx_CF[0],&yy_CF[0],0,0);
        grBeltCFCenter[i]->SetLineColor(kRed);
        grBeltCFCenter[i]->SetLineStyle(2);
        grBeltCFCenter[i]->SetLineWidth(3);
        grBeltCFCenter[i]->Draw("C");
      
        grBeltCFLow[i]=new TGraphErrors(xx_CF.size(),&xx_CF[0],&yy_eLow_CF[0],0,0);
        grBeltCFLow[i]->SetLineColor(kRed);
        grBeltCFLow[i]->SetLineWidth(2);
        grBeltCFLow[i]->Draw("C");
        
        grBeltCFHigh[i]=new TGraphErrors(xx_CF.size(),&xx_CF[0],&yy_eHi_CF[0],0,0);
        grBeltCFHigh[i]->SetLineColor(kRed);
        grBeltCFHigh[i]->SetLineWidth(3);
        grBeltCFHigh[i]->Draw("C");
        
        //grFitCFLow[i]=new TGraphErrors(xx_CF.size(),&xx_CF[0],&yy_rM_eLow_CF[0],0,0);
        //grFitCFLow[i]->SetLineColor(kYellow);
        //grFitCFLow[i]->SetLineWidth(4);
        //grFitCFLow[i]->Draw("C");
        
        //grFitCFHigh[i]=new TGraphErrors(xx_CF.size(),&xx_CF[0],&yy_rM_eHi_CF[0],0,0);
        //grFitCFHigh[i]->SetLineColor(kYellow);
        //grFitCFHigh[i]->SetLineWidth(4);
        //grFitCFHigh[i]->Draw("C");

        
        
        vLineEXSM[i]->Draw();

        
      }
    
    for(auto ff : formats)
      ccBelts[i]->Print(Form("%s/%s.%s",path.c_str(),ccBelts[i]->GetName(),ff.c_str()),ff.c_str());



    
      TH1F * hShapeDrawMatrix = new TH1F("hShapeDrawMatrix","",nBinsMap[fname],0,nBinsMap[fname]);
      SetDef(hShapeDrawMatrix);
      hShapeDrawMatrix->GetXaxis()->SetTitle(Form("Bin %s",VarMap[fname].c_str()));
      hShapeDrawMatrix->GetYaxis()->SetTitle(Form("<#hat{#sigma}^{bin}_{fit}-#sigma^{bin}_{true}/#sigma^{bin}_{true}>"));
      hShapeDrawMatrix->GetYaxis()->SetRangeUser(-3,+3);
      TH1F *hShapeDrawCF=(TH1F*)hShapeDrawMatrix->Clone("hShapeDrawCF");
      TH1F *hShapeDrawIBU=(TH1F*)hShapeDrawMatrix->Clone("hShapeDrawIBU");
      
      
      for(int i=0; i<(int)nBinsMap[fname]; i++){
        hShapeDrawMatrix->SetBinError(i+1,medianErrorMatrix[i]);
        hShapeDrawIBU->SetBinError(i+1,medianErrorIBU[i]);
        hShapeDrawCF->SetBinError(i+1,medianErrorCF[i]);
      }

      hShapeDrawMatrix->SetFillColor(kBlack);
      hShapeDrawCF->SetFillColor(kRed);
      hShapeDrawIBU->SetFillColor(kBlue);

      hShapeDrawMatrix->SetFillStyle(3444);
      hShapeDrawCF->SetFillStyle(3444);
      hShapeDrawIBU->SetFillStyle(3444);


     
      
      
      
      TCanvas *ccShape=new TCanvas("ccShape","");{
        SetCanvasDefaults(ccShape);
        ccShape->cd();
        hShapeDrawMatrix->Draw("E2");
        if(drawIBU)
          hShapeDrawIBU->Draw("E2 same");
        hShapeDrawCF->Draw("E2 same");
        for(std::map<int, vector <double>>::iterator it=variationsPerBin.begin(); it!=variationsPerBin.end(); it++){
          const int ii=(*it).first;

          hDiffXsecShapeCF[ii]->SetLineColor(kRed);
          hDiffXsecShapeCF_up[ii]->SetLineColor(kRed);

          hDiffXsecShapeCF[ii]->SetLineWidth(3);
          hDiffXsecShapeMatrix[ii]->SetLineWidth(3);
          hDiffXsecShapeIBU[ii]->SetLineWidth(3);

          hDiffXsecShapeCF_up[ii]->SetLineWidth(3);
          hDiffXsecShapeMatrix_up[ii]->SetLineWidth(3);
          hDiffXsecShapeIBU_up[ii]->SetLineWidth(3);
          
          hDiffXsecShapeCF[ii]->Draw("hist same");
          if(!useCustomShapeDrawUncert)
            hDiffXsecShapeCF_up[ii]->Draw("hist same");

          hDiffXsecShapeMatrix[ii]->SetLineColor(kBlack);
          hDiffXsecShapeMatrix_up[ii]->SetLineColor(kBlack);
          hDiffXsecShapeMatrix[ii]->Draw("hist same");
          if(!useCustomShapeDrawUncert)
            hDiffXsecShapeMatrix_up[ii]->Draw("hist same");
          if(drawIBU){
            hDiffXsecShapeIBU[ii]->SetLineColor(kBlue);
            hDiffXsecShapeIBU_up[ii]->SetLineColor(kBlue);
            hDiffXsecShapeIBU[ii]->Draw("hist same");
            if(!useCustomShapeDrawUncert)
              hDiffXsecShapeIBU_up[ii]->Draw("hist same");
          }
          //break ;
        }

        if( i==0){
        if(useCustomShapeDrawUncert){
          lcShape->AddEntry(hDiffXsecShapeCF[0],"Bin-by-bin","L");
          lcShape->AddEntry(hDiffXsecShapeMatrix[0],"Matrix","L");
          if(drawIBU)
            lcShape->AddEntry(hDiffXsecShapeIBU[0],Form("#tau %.2lf",tau),"L");
        }
        else {
          lcShape->AddEntry(hDiffXsecShapeCF[0],"Bin-by-bin (low)","L");
          lcShapeMid->AddEntry(hDiffXsecShapeCF_up[0],"Bin-by-bin (high)","L");
          lcShape->AddEntry(hDiffXsecShapeMatrix[0],"Matrix (low)","L");
          lcShapeMid->AddEntry(hDiffXsecShapeMatrix_up[0],"Matrix (high)","L");
          if(drawIBU){
            lcShape->AddEntry(hDiffXsecShapeIBU[0],Form("#tau %.2lf (low)",tau),"L");
            lcShapeMid->AddEntry(hDiffXsecShapeIBU_up[0],Form("#tau %.2lf (high)",tau),"L");
          }
        }
        lcShape2->SetHeader("S.M. expected uncertainties");
        lcShape2->AddEntry(hShapeDrawCF,"Bin-by-bin","F");
        lcShape2->AddEntry(hShapeDrawMatrix,"Matrix","F");
        }

        lcShape->Draw();
        lcShape2->Draw();
        lcShapeMid->Draw();
        ccShape->RedrawAxis();
      }
      for(auto ff : formats)
        ccShape->Print(Form("%s/%s.%s",path.c_str(),ccShape->GetName(),ff.c_str()),ff.c_str());
      
  }


  // store the graphs
  grBiasBand_matrix=new TGraphAsymmErrors((int)centerx.size(),&centerx[0],&centery[0],0,0,&errorYL_matrix[0],&errorYH_matrix[0]);
  grBiasBand_matrix->SetName(Form("graph_%s_iter_%d_matrix",VarMap[fname].c_str(),rooUnfoldIter));
  
  grBiasBand_CF=new TGraphAsymmErrors((int)centerx.size(),&centerx[0],&centery[0],0,0,&errorYL_CF[0],&errorYH_CF[0]);
  grBiasBand_CF->SetName(Form("graph_%s_iter_%d_CF",VarMap[fname].c_str(),rooUnfoldIter));
  
  grBiasBand_IBU=new TGraphAsymmErrors((int)centerx.size(),&centerx[0],&centery[0],0,0,&errorYL_IBU[0],&errorYH_IBU[0]);
  grBiasBand_IBU->SetName(Form("graph_%s_iter_%d_IBU",VarMap[fname].c_str(),rooUnfoldIter));
  
      if(fout!=nullptr) {
        fout->cd();
        grBiasBand_matrix->Write();
        grBiasBand_CF->Write();
        grBiasBand_IBU->Write();
        for (auto h :  h2DdiffCF)  h.second->Write(Form("%s_iter_%d",h.second->GetName(),rooUnfoldIter));
        for (auto h :  h2DdiffMatrix)  h.second->Write(Form("%s_iter_%d",h.second->GetName(),rooUnfoldIter));
        for (auto h :  h2DdiffIBU) h.second->Write(Form("%s_iter_%d",h.second->GetName(),rooUnfoldIter));

        for (auto h :  hProfilediffCF)  h.second->Write(Form("%s_iter_%d",h.second->GetName(),rooUnfoldIter));
        for (auto h :  hProfilediffMatrix)  h.second->Write(Form("%s_iter_%d",h.second->GetName(),rooUnfoldIter));
        for (auto h :  hProfilediffIBU)  h.second->Write(Form("%s_iter_%d",h.second->GetName(),rooUnfoldIter));

        for (auto h :  fPullCF)  h.second->Write(Form("%s_iter_%d",h.second->GetName(),rooUnfoldIter));
        for (auto h :  fPullMatrix)  h.second->Write(Form("%s_iter_%d",h.second->GetName(),rooUnfoldIter));
        for (auto h :  fPullIBU)  h.second->Write(Form("%s_iter_%d",h.second->GetName(),rooUnfoldIter));

        for (auto h :  hPullCF)  h.second->Write(Form("%s_iter_%d",h.second->GetName(),rooUnfoldIter));
        for (auto h :  hPullMatrix)  h.second->Write(Form("%s_iter_%d",h.second->GetName(),rooUnfoldIter));
        for (auto h :  hPullIBU)  h.second->Write(Form("%s_iter_%d",h.second->GetName(),rooUnfoldIter));

        // Write the vectors
        for( auto h : TerrorYH_methods) h.second->Write(Form("biasH_%s_iter_%d",h.first.c_str(),rooUnfoldIter));
        for( auto h : TerrorYL_methods) h.second->Write(Form("biasL_%s_iter_%d",h.first.c_str(),rooUnfoldIter));
        Tcenterx.Write(Form("meanTruth_iter_%d",rooUnfoldIter));
        Ttau.Write(Form("tau_iter_%d",rooUnfoldIter));
        
      }
     
      
      if(noDraw) {

        for (auto h :  h2DdiffCF) delete h.second;
        for (auto h :  h2DdiffMatrix) delete h.second;
        for (auto h :  h2DdiffIBU) delete h.second;
        for (auto h :  hProfilediffCF) delete h.second;
        for (auto h :  hProfilediffMatrix) delete h.second;
        for (auto h :  hProfilediffIBU) delete h.second;
        
        for (auto h :  fPullCF) delete h.second;
        for (auto h :  fPullMatrix) delete h.second;
        for (auto h :  fPullIBU) delete h.second;
        
        for (auto h :  hPullCF) delete h.second;
        for (auto h :  hPullMatrix) delete h.second;
        for (auto h :  hPullIBU)  delete h.second;

        for (auto h : hErrMartrix) delete h.second;
        for (auto h : hErrCF) delete h.second;
        for (auto h : hErrIBU) delete h.second;
        for (auto h : ccErrCom) delete h.second;
        for (auto h : ccErrLeg) delete h.second;
        for (auto h : hPullRecoMatrix) delete h.second;
        for (auto h : hPullRecoCF) delete h.second;
        for (auto h : hPullRecoIBU) delete h.second;
        for (auto h : ccPullRecoCom) delete h.second;
        for (auto h : ccPullRecoLeg) delete h.second;
        for (auto h : ccBelts) delete h.second;
        for (auto h : hBeltsDraw) delete h.second;
        for( auto h : vLineEXSM) delete h.second; 
        for (auto h : grBeltMatrixCenter) delete h.second;
        for (auto h : grBeltMatrixLow) delete h.second;
        for (auto h : grBeltMatrixHigh) delete h.second;
        for (auto h : grFitMatrix) delete h.second;
        for (auto h : grFitIBU) delete h.second;
        for (auto h : grFitCF) delete h.second;
        for (auto h : hBFitMatrix) delete h.second;
        for (auto h : hBFitIBU) delete h.second;
        for (auto h : hBFitCF) delete h.second;
        

        
        for (auto h : grBeltCFCenter) delete h.second;
        for (auto h : grBeltCFLow) delete h.second;
        for (auto h : grBeltCFHigh) delete h.second;

        for (auto h : grBeltIBUCenter) delete h.second;
        for (auto h : grBeltIBULow) delete h.second;
        for (auto h : grBeltIBUHigh) delete h.second;
        delete lat;
        for (auto h : hDiffXsecShapeMatrix) delete h.second;
        for (auto h : hDiffXsecShapeCF) delete h.second;
        for (auto h : hDiffXsecShapeIBU) delete h.second;
        for (auto h : hDiffXsecShapeMatrix_up) delete h.second;
        for (auto h : hDiffXsecShapeCF_up) delete h.second;
        for (auto h : hDiffXsecShapeIBU_up) delete h.second;
        for( auto h : TerrorYH_methods) delete h.second;
        for( auto h : TerrorYL_methods) delete h.second;

        delete lcShapeMid;
        delete lcShape;
        delete lcShape2;
        
      }
  
}

void drawToysFile(string fname="iterationsToys_ws_njet_expected.root", int iter=4,bool diffrooUnfolditer=true){
  string foutname="biases_" +eraseSubStr(eraseSubStr(fname,"_expected_mtx_v20v2.root"),"iterationsToys_ws_")+".root";
  TFile *fout=new TFile(foutname.c_str(),"RECREATE");
  // Also store some results in a tree
  //TTree *tout=new TTree( Form("biases_%s",(eraseSubStr(eraseSubStr(fname,"_expected_mtx_v20v2.root"),"iterationsToys_ws_")).c_str()),"");
  
  vector <int> maxIter;
  for(int i=0; i<iter+1;i++){
    if(diffrooUnfolditer)
      drawToys_(fname,i,i,fout,nullptr);
    else
      drawToys_(fname,0,i,fout,nullptr);
  }
  /*
    vector <double> xs_CF;
    vector <double> xs_CFe;
    vector <double> xs_Matrix;
    vector <double> xs_Matrixe;
    std::map<int, vector<double>> xs_IBU;
    std::map<int, vector<double>> xs_IBUe;

    for( int i=0; i<(int)iter;i++){
    // Take the XS spectra for CF;
    TH1F *hdummy = new TH1F("hdummy","",1000,0,20);
    toys->Draw(

    }
  */

  //tout->Write();
  fout->Close();
  
  //delete tout;
  
}

void drawToysList(string listName,int iter=4,bool diffrooUnfolditer=true){
  string modelConfigName="ModelConfig";
  
  ifstream file(listName.c_str());
  string line="";
  RooRealVar *rtmp=nullptr;
  while (getline(file,line)){
    TFile *f=TFile::Open(line.c_str());
    //RooWorkspace *w=(RooWorkspace*)f->Get("myWS");
    //ModelConfig *model=(ModelConfig*)w->obj(modelConfigName.c_str());
    //const RooArgSet *pois=model->GetParametersOfInterest();
    //vector <RooRealVar*> *poisV=new vector <RooRealVar*>();
    //TIter tp=pois->createIterator();
    //rtmp=nullptr;
    //int lp=0;
    //string VarName = "";
    //while((rtmp=(RooRealVar*)tp.Next())){
    //if( (string(rtmp->GetName())).find("sigma")!=string::npos){
    //poisV->push_back(rtmp);
    //VarName=poisV->at(0)->GetTitle();
    // }
    //}
    cout<<"Reading file "<<line<<endl;
    //nBinsMap[line]=poisV->size();
    //     VarMap[line]=VarName;
    drawToysFile(line,iter,diffrooUnfolditer);
  }
}
void drawBdtBiasToys(string listName,int iter=4,bool diffrooUnfolditer=false){
  if( TString(listName).Contains(".root"))
    drawToysFile(listName,iter,diffrooUnfolditer);
  else if( TString(listName).Contains(".txt"))
    drawToysList(listName,iter,diffrooUnfolditer);
}

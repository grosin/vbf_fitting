
//#define INCLROOUNFOLD false 
#include "tools.h"
#include "TGraphAsymmErrors.h"
#ifdef INCLROOUNFOLD
#include "RooUnfoldResponse.h"
#include <RooUnfoldBayes.h>
#include <RooUnfoldBinByBin.h>
#endif 
#include <RooCategory.h>
#include <RooWorkspace.h>
#include <RooMinimizer.h>
#include <RooAddition.h>
#include <RooStats/AsymptoticCalculator.h>
#include "TRandom3.h"
#include <RooFitResult.h>
#include <RooArgSet.h>
#include <RooArgList.h>
#include <RooProduct.h>
#include "TLegend.h"
#include "RooHistFunc.h"
#include "RooSimultaneous.h"

//#include "RooStarMomentMorph.h"
/*

#include "/Users/gaetano/Atlas/Analysis/Higgs4L/Analysis/DiffCrossSectionFullRun2/RooUnfold/trunk/RooUnfold/src/RooUnfoldResponse.h"
#include "/Users/gaetano/Atlas/Analysis/Higgs4L/Analysis/DiffCrossSectionFullRun2/RooUnfold/trunk/RooUnfold/src/RooUnfoldSvd.h"
#include "/Users/gaetano/Atlas/Analysis/Higgs4L/Analysis/DiffCrossSectionFullRun2/RooUnfold/trunk/RooUnfold/src/RooUnfoldBayes.h"
#include "/Users/gaetano/Atlas/Analysis/Higgs4L/Analysis/DiffCrossSectionFullRun2/RooUnfold/trunk/RooUnfold/src/RooUnfoldBinByBin.h"
*/
//gROOT->ProcessLine(".include /Users/gaetano/Atlas/Analysis/Higgs4L/Analysis/DiffCrossSectionFullRun2/RooUnfold/trunk/RooUnfold/src");
//gROOT->ProcessLine(".L /Users/gaetano/Atlas/Analysis/Higgs4L/Analysis/DiffCrossSectionFullRun2/RooUnfold/trunk/RooUnfold/libRooUnfold.so");
//gSystem->Load("/Users/gaetano/Atlas/Analysis/Higgs4L/Analysis/DiffCrossSectionFullRun2/RooUnfold/trunk/RooUnfold/libRooUnfold.so");


//gSystem->Load("libRooUnfold.so");

using namespace std;
using namespace RooStats;
using namespace RooFit;

int main (int argc, char* argv[]){
  
  string fname=argv[1];//"ws_pt_expected.root";
  string wsName="w";
  const int    maxIter=1;
  int    nToys=5000;//20000;
  string modelConfigName="ModelConfig";
  bool useMinos=false;
  bool fastFit=false;
  bool doSeparateIterationsRoounfold=true;
  const int  nBaesIter=4;
  bool randomizeAlsotheBDT=false;
  string extraName="";
  for(int i =2 ; i<(int) argc; i++){
    if( string(argv[i]).compare("-split")==0 ) {
      extraName+=Form("_split_%s_",argv[i+1]);
    }
    if( string(argv[i]).compare("-nToys")==0){
      nToys=atoi(argv[i+1]);
    }

    if(string(argv[i]).compare("-bdt")==0){
      randomizeAlsotheBDT=true;
    }
    
  }


  // regions to apply the matrix to
  string topWWDscir="bdt_TopWWAll";
  vector <pair<string,string>> matRegions;
  matRegions.push_back(make_pair("SRVBF","bdt_vbf"));
  matRegions.push_back(make_pair("CRTop",topWWDscir));
  
  /*
void wsexp(string fname="ws_pt_expected.root",
           string wsName="myWS",
           const int    maxIter=1,
           const int    nToys=10000,
           string modelConfigName="ModelConfig",
           bool useMinos=false,
           bool fastFit=false,
           bool doSeparateIterationsRoounfold=true,
           const int  nBaesIter=4
           ){
             
  */
  //gSystem->Load("libRooUnfold.so");
  
  //gSystem->AddIncludePath("- I/Users/gaetano/Atlas/Analysis/Higgs4L/Analysis/DiffCrossSectionFullRun2/RooUnfold/trunk/RooUnfold/src");
  //gSystem->Load("libRooUnfold.so");
  
  
  TFile *f=TFile::Open(fname.c_str());
  RooWorkspace *w=(RooWorkspace*)f->Get(wsName.c_str());
  w->Print();
  ModelConfig *model=(ModelConfig*)w->obj(modelConfigName.c_str());
  model->Print();
  RooArgSet *obsrvables=(RooArgSet*)model->GetObservables();
  cout<<"Observables"<<endl;
  obsrvables->Print();

  // fix all nuisance parameters

  const  RooArgSet *nps= model->GetNuisanceParameters();
  TIter tnp=nps->createIterator();
  RooRealVar *rtmp=nullptr;
  while((rtmp=(RooRealVar*)tnp.Next())){
    if(TString(rtmp->GetName()).Contains("alpha"))
      rtmp->setConstant(true);
  }

  w->allVars().selectByName("mu_*")->setAttribAll("Constant",true);
  
  RooCategory *channelCat=(RooCategory*)w->cat("channelCat");
  channelCat->Print("v");

  //RooAbsData *data=(RooAbsData*)w->data("obsData");
  //data->Print();
  RooAbsData *asimov=(RooAbsData*)w->data("asimovDataFullModel");
  asimov->Print();

  const RooArgSet *pois=model->GetParametersOfInterest();
  pois->Print();
  
  vector <RooRealVar*> *poisV=new vector <RooRealVar*>();
  vector <RooRealVar*> *binWidthV=new vector <RooRealVar*>();
  
  TIter tp=pois->createIterator();
  rtmp=nullptr;
  int lp=0;
  while((rtmp=(RooRealVar*)tp.Next())){
    if( (string(rtmp->GetName())).find("sigma")!=string::npos){
      poisV->push_back(rtmp);
      RooRealVar *lvar=(RooRealVar*)w->var(Form("BinWidth_bin%d",lp));
      if(lvar!=nullptr){
        binWidthV->push_back(lvar);
        binWidthV->back()->Print();
      lp++;
      }
    }
  }
  
  std::map <string, std::map<int,double>> iterPoiMap;

  std::map <string, std::map<int,double>> iterPoiErrLow;
  std::map <string, std::map<int,double>> iterPoiErrHigh;

  std::map<string, std::map<int,double>> notFidIter;
  std::map<string, double> iterPoiMapInputXsec;
  std::map<string , std::map<int, double>> iterNtotEvents;
  vector <double> initNtotEvents;
  vector <double> initTrueEvents;

  std::map<int,RooRealVar*> diffXsMap;
  
  // size of the pois: 
  int nPois=0;
  TIter it0 = pois->createIterator();
  RooRealVar *t2=nullptr;
  vector <double> nPoisV;
  while((t2=(RooRealVar*)it0.Next())){
    nPois++;
    nPoisV.push_back(nPois);
    t2->Print();
    diffXsMap[nPois-1]=t2;
  }


  // Retrive the non fiducial fraction: 
  std::map<string , RooRealVar*> notFidMap;
  for(int i=0; i<(int)nPois; i++){
    notFidMap[Form("sigma_bin%d",i)]=(RooRealVar*)w->var(Form("notFid_bin%d_incl",i));
    notFidMap[Form("sigma_bin%d",i)]->Print();
    notFidIter[Form("sigma_bin%d",i)][0]=notFidMap[Form("sigma_bin%d",i)]->getVal();
  }

  // get the lumi, BR and sigma 
  RooRealVar *sigma=(RooRealVar*)w->var("sigma");
  //RooRealVar *L=new RooRealVar("llumi","llumu",139,139,139);
  RooRealVar *L=new RooRealVar("llumi","llumu",1,1,1);
  L->setConstant(true);
  //RooRealVar *L=(RooRealVar*)w->var("Lumi");
  
  
  RooRealVar *BR_ZZ_incl=(RooRealVar*)w->var("BR_ZZ_incl");
  if(BR_ZZ_incl==nullptr){
    BR_ZZ_incl=new RooRealVar("BR_ZZ_incl","BR_ZZ_incl",1,1,1);
    BR_ZZ_incl->setConstant(true);
  }

  std::map<int,std::map<int ,RooRealVar*>> matrix;
  std::map<int,std::map<int ,RooRealVar*>> nullElements;
  // retrive the normalization coefficients 
  std::map<int , RooFormulaVar* > normMap;
  std::map<string,RooProduct*> yieldReg;
  vector <double> Cfactors;

  
  for(int k=0;k<nPois;k++){
    normMap[k]=(RooFormulaVar*)w->obj(Form("norm_bin%d_incl",k));
    if(normMap[k]==nullptr){

      cout<<"Null pointer for "<<Form("norm_bin%d_incl",k)<<endl;
      RooArgSet addToYield; 
      string addToYieldSum="";
      for(vector<pair<string,string>>::iterator it=matRegions.begin(); it!=matRegions.end(); it++){
	yieldReg[(*it).first] = (RooProduct*)w->obj(Form("vbf0_%d_%s_%d_%s_overallNorm_x_sigma_epsilon",k,(*it).first.c_str(),k,(*it).second.c_str()));
	addToYield.add( 	* yieldReg[(*it).first] );
	if(addToYieldSum.size()>0)
	  addToYieldSum+="+";
	addToYieldSum+=yieldReg[(*it).first]->GetName();
      }
      addToYieldSum="("+addToYieldSum+")";
      
      //normMap[k]=new RooFormulaVar(Form("norm_bin%d_incl",k),Form("1.0 * sigma_bin%d",k), *diffXsMap[k]);
      normMap[k]=new RooFormulaVar(Form("norm_bin%d_incl",k),addToYieldSum.c_str(),addToYield);
    } 
    normMap[k]->Print();
    
    for(int j=0; j<nPois; j++){
      matrix[k][j]=(RooRealVar*)w->var(Form("eff_bin%d_inclbin%d_incl",k,j));
      if(matrix[k][j]==nullptr){ 
        matrix[k][j]=new RooRealVar(Form("eff_bin%d_inclbin%d_incl",k,j),Form("eff_bin%d_inclbin%d_incl",k,j),0);
      }
      matrix[k][j]->Print();
    }}
  

  for(int k=0;k<nPois;k++){
    double val=0;
    for(int j=0; j<nPois; j++)
      val+=matrix[k][j]->getVal();
    Cfactors.push_back(val);
  }

  // retrive the pdf 
  RooAbsPdf *pdf_= model->GetPdf();
  RooRealVar *tau=new RooRealVar("tau","#lambda",0,-10,+10);
  tau->setVal(0.0);
  tau->setConstant(true);
  RooArgList *argsFormula=new RooArgList();
  //string formulaSum="TMath::Exp(tau *(";
  string formulaSum="(tau *(";
  argsFormula->add(*tau);

  /*
  // Here is the variations with the cross section smoothing. 
  for(int k=1;k<nPois-1;k++){
    //formulaSum+=Form("((@%d-@%d) - (@%d-@%d)*(@%d-@%d))",k+1,k,k,k-1,k,k-1);
    //formulaSum+=Form("((%s-%s) - (%s-%s)*(%s-%s))",
    //poisV->at(k+1)->GetName(),
    //               poisV->at(k)->GetName(),
    //             poisV->at(k)->GetName(),
    //           poisV->at(k-1)->GetName(),
    //         poisV->at(k)->GetName(),poisV->at(k-1)->GetName());
    
    formulaSum+=Form("((%s - 2*%s +%s)*(%s- 2*%s +%s))",
                     poisV->at(k+1)->GetName(),
                     poisV->at(k)->GetName(),
                     poisV->at(k-1)->GetName(),
                     poisV->at(k+1)->GetName(),
                     poisV->at(k)->GetName(),
                     poisV->at(k-1)->GetName());

    
    if(k<nPois-2) formulaSum+=" + ";

    argsFormula->add(*poisV->at(k+1));
    argsFormula->add(*poisV->at(k));
    argsFormula->add(*poisV->at(k-1));
    argsFormula->add(*poisV->at(k+1));
    argsFormula->add(*poisV->at(k));
    argsFormula->add(*poisV->at(k-1));
  } 
  formulaSum+="))";
  */

  
  // here is the version with the cross section normalized to the expectation used.
  for(int k=1;k<nPois-1;k++){
    //formulaSum+=Form("((%s/%.3lf-%s/%.3lf) - (%s/%.3l2f-%s/%.3lf)*(%s/%.3lf-%s/%.3lf))",
    //               poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),
    //               poisV->at(k)->GetName(),poisV->at(k)->getVal(),
    //               poisV->at(k)->GetName(),poisV->at(k)->getVal(),
    //               poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal(),
    //               poisV->at(k)->GetName(),poisV->at(k)->getVal(),
    //               poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal());


    //formulaSum+=Form("TMath::Sqrt((%s/%.3lf/%.3lf - 2*%s/%.3lf/%.3lf - %s/%.3lf/%.3lf)*(%s/%.3lf/%.3lf - 2*%s/%.3lf/%.3lf -%s/%.3lf/%.3lf))",
    //               poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),binWidthV->at(k+1)->getVal(),
    //               poisV->at(k)->GetName(),poisV->at(k)->getVal(),binWidthV->at(k)->getVal(),
    //               poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal(),binWidthV->at(k-1)->getVal(),
    //               poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),binWidthV->at(k+1)->getVal(),
    //               poisV->at(k)->GetName(),poisV->at(k)->getVal(),binWidthV->at(k)->getVal(),
    //               poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal(),binWidthV->at(k-1)->getVal());
    
    /*
    formulaSum+=Form("((-%s/%.3lf + 2*%s/%.3lf - %s/%.3lf)*(-%s/%.3lf + 2*%s/%.3lf -%s/%.3lf))",
                     poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal(),
                     poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal());
    
    
    if(k<nPois-2) formulaSum+=" + ";

    argsFormula->add(*poisV->at(k+1));
    argsFormula->add(*poisV->at(k));
    argsFormula->add(*poisV->at(k-1));
    argsFormula->add(*poisV->at(k+1));
    argsFormula->add(*poisV->at(k));
    argsFormula->add(*poisV->at(k-1));
    */

    formulaSum+=Form("TMath::Power( (%s/%.3lf -%s/%.3lf) - (%s/%.3lf -%s/%.3lf),2) ",
                     poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal());
    
    if(k<nPois-2) formulaSum+=" + ";
    
    argsFormula->add(*poisV->at(k+1));
    argsFormula->add(*poisV->at(k));
    argsFormula->add(*poisV->at(k));
    argsFormula->add(*poisV->at(k-1));
    

  } 
  formulaSum+="))";
  
  
  
  RooFormulaVar *regSum=new RooFormulaVar("regSum","regSum",formulaSum.c_str(),*argsFormula);
  regSum->Print();

  
  //RooExponential *regSumPdf=new RooExponential("regSumPdf","regSumPdf",*tau,*regSum);
  //RooGenericPdf *regSumPdf=new RooGenericPdf("regPdf","regPdf",formulaSum.c_str(),*argsFormula);

  //RooProdPdf *regPdf=new RooProdPdf("regPdf","regPdf",RooArgList(*pdf_,*regSumPdf));
  RooGenericPdf *regPdf= new RooGenericPdf("regPdf","regPdf","@0*@1",RooArgList(*pdf_,*regSum));
  //RooProdPdf *regPdf=new RooProdPdf("regPdf","regPdf",RooArgList(*pdf_,*regSumPdf),Conditional(*regSumPdf,*pois));
  //RooProdPdf *regPdf=new RooProdPdf("regPdf","regPdf",RooArgList(*pdf_,*regSumPdf));
  
  RooAbsPdf *pdf=pdf_;
  //RooAbsPdf *pdf=(RooAbsPdf*)regPdf;
  cout<<"Print pdf "<<endl;
  pdf->Print("v");

  
  // Perform a fit
  RooAbsReal *Nll = pdf->createNLL(*asimov,NumCPU(2,2));//
  RooAddition *NllMod=new RooAddition("nllSum","nullSum",RooArgList(*Nll,*regSum));


  //ConditionalObservables(*tau),
  //                                ExternalConstraints(*regPdf)
  //);
                                     
  //RooAbsReal *Nll = pdf->createNLL(*asimov);
  //RooMinimizer *minim = new RooMinimizer(*Nll);              ////OffSet true default
  RooMinimizer *minim = new RooMinimizer(*NllMod);              ////OffSet true default
  
  //minim->setPrintLevel(0);
  minim->setEps(0.1);
  //minim->optimizeConst(2);
  //minim->setStrategy(2);
  minim->optimizeConst(2);
  minim->setStrategy(2);
  minim->minimize("Minuit2");
  minim->minos(*pois);
  pois->Print("V");
  std::map<string, double> reg_poiCentral;
  std::map<string, double> reg_poiErrUp;
  std::map<string, double> reg_poiErrDown;
  TIter itl = pois->createIterator();
  t2=nullptr;
  while((t2=(RooRealVar*)itl.Next())){
    reg_poiCentral[t2->GetName()]=t2->getVal();
    reg_poiErrUp[t2->GetName()]=t2->getErrorHi();
    reg_poiErrDown[t2->GetName()]=t2->getErrorLo();
  }
  // Here create the default minimization
  
  RooMinimizer *minimNoReg = new RooMinimizer(*Nll);              ////OffSet true default
  minimNoReg->setEps(0.1);
  minimNoReg->optimizeConst(2);
  minimNoReg->setStrategy(2);
  minimNoReg->minimize("Minuit2");
  minimNoReg->minos(*pois);
  std::map<string, double> noreg_poiCentral;
  std::map<string, double> noreg_poiErrUp;
  std::map<string, double> noreg_poiErrDown;
  TIter itlp = pois->createIterator();
  t2=nullptr;
  while((t2=(RooRealVar*)itlp.Next())){
    noreg_poiCentral[t2->GetName()]=t2->getVal();
    noreg_poiErrUp[t2->GetName()]=t2->getErrorHi();
    noreg_poiErrDown[t2->GetName()]=t2->getErrorLo();
  }


  cout<<"bin & unregularized & regularized with tau = "<<tau->getVal()<<"\\\\"<<endl;
  for(std::map<string,double>::iterator it=reg_poiCentral.begin(); it!=reg_poiCentral.end(); it++){
    cout<<std::setprecision(3)<<(*it).first<<" & "<<noreg_poiCentral[(*it).first]<<"^{+"<<noreg_poiErrUp[(*it).first]<<"}_{"<<noreg_poiErrDown[(*it).first]<<"} & "<<
      reg_poiCentral[(*it).first]<<"^{+"<<reg_poiErrUp[(*it).first]<<"}_{"<<reg_poiErrDown[(*it).first]<<"}\\\\"<<endl;
  }

  
  RooArgSet allPars;
  allPars.add(*model->GetParametersOfInterest());
  //allPars.add(*model->GetNuisanceParameters());
  //allPars.add(*model->GetConstraintParameters());

  w->saveSnapshot("nominal",allPars);
  
 
  // Increase the cross section by 10%
  double pCount=1;
  TIter it2 = pois->createIterator();
  
  int lll=0;
  while((t2=(RooRealVar*)it2.Next())){
    iterPoiMap[t2->GetName()][0]=t2->getVal();
    if(useMinos){
      iterPoiErrLow[t2->GetName()][0]=t2->getErrorLo();
      iterPoiErrHigh[t2->GetName()][0]=t2->getErrorHi();
    }
    else {
      iterPoiErrLow[t2->GetName()][0]=t2->getError();
      iterPoiErrHigh[t2->GetName()][0]=t2->getError();
    }
      

    // Constant increase 
    //t2->setVal(1.1*t2->getVal());
    // linear 
    //t2->setVal(1.1*pCount*t2->getVal());
    
    iterPoiMapInputXsec[t2->GetName()]=t2->getVal();
    iterNtotEvents[t2->GetName()][0]=normMap[lll]->getVal()*L->getVal();
    initNtotEvents.push_back(normMap[lll]->getVal()*L->getVal());
    initTrueEvents.push_back(t2->getVal()*BR_ZZ_incl->getVal()*L->getVal());
    pCount+=1.0;
    lll++;
  }



  RooAbsData *myAsimov=AsymptoticCalculator::GenerateAsimovData(*pdf_,*obsrvables);
  RooAbsReal *Nll2 = pdf->createNLL(*myAsimov,NumCPU(2,2));
  RooMinimizer *minim2 = new RooMinimizer(*Nll2);              ////OffSet true default
  minim2->setEps(0.1);
  minim2->optimizeConst(2);
  minim2->setStrategy(2);
  minim2->minimize("Minuit2");
  if(useMinos)
    minim2->minos(*pois);

  pois->Print("V");

// Here run the iterations for the regularization.. 

// create a vector to hold the iteration numbers 
  vector <double> iterV;
  iterV.push_back(0);

  RooMsgService::instance().setSilentMode(kTRUE);
  RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL);   






for( int iter=1; iter<maxIter; iter++){
  
  //RooAbsData *myAsimov2=AsymptoticCalculator::GenerateAsimovData(*pdf,*obsrvables);
  //RooAbsReal *Nll2 = pdf->createNLL(*myAsimov2);
  // Make the fit 
  RooAbsReal *Nll2 = pdf->createNLL(*myAsimov,NumCPU(2,2));
  // fit the nominal 
  //RooAbsReal *Nll2 = pdf->createNLL(*asimov);
  RooMinimizer *minim2 = new RooMinimizer(*Nll2);              ////OffSet true default
  minim2->setEps(0.1);
  minim2->optimizeConst(2);
  minim2->setStrategy(2);
  minim2->minimize("Minuit2");
  if(useMinos)
    minim2->minos(*pois);

  pois->Print("V");

 
  // get the cross sections and the non fid fractions 
  TIter it3 = pois->createIterator();
  int ll=0;
  
  while((t2=(RooRealVar*)it3.Next())){
    iterPoiMap[t2->GetName()][iter]=t2->getVal();
    if(useMinos){
      iterPoiErrLow[t2->GetName()][iter]=t2->getErrorLo();
      iterPoiErrHigh[t2->GetName()][iter]=t2->getErrorHi();
    }
    else{
      iterPoiErrLow[t2->GetName()][iter]=t2->getError();
      iterPoiErrHigh[t2->GetName()][iter]=t2->getError();
    }

    notFidIter[t2->GetName()][iter]=notFidMap[t2->GetName()]->getVal();

    double calibF=(normMap[ll]->getVal()*L->getVal() - sigma->getVal()*L->getVal()*BR_ZZ_incl->getVal())/(sigma->getVal()*L->getVal()*BR_ZZ_incl->getVal());
    double incr=1+iterPoiMap[t2->GetName()][iter]/iterPoiMap[t2->GetName()][iter-1];

    cout<<calibF<<endl;
    double newF=notFidIter[t2->GetName()][iter];
    
    cout<<"Old f "<< notFidIter[t2->GetName()][iter]<<" "<<notFidMap[t2->GetName()]->getVal()<<" new "<<newF<<" delta xsec "<<incr<<" tot events "<<normMap[ll]->getVal()*L->getVal()<<"xsec new "<<t2->getVal()<<" iter old "<<iterPoiMap[t2->GetName()][iter-1]<<endl;
    
    notFidMap[t2->GetName()]->setVal(newF);
    cout<<"Iter "<<iter<<" "<<t2->GetName()<<" "<<iterPoiMap[t2->GetName()][iter]
        <<"+ "<<iterPoiErrHigh[t2->GetName()][iter]<<" - "
        <<iterPoiErrLow[t2->GetName()][iter]<<" f "<<notFidIter[t2->GetName()][iter]<<endl;

    ll++;
  }
  

  // Re weight the matrix 
  for(int k=0;k<nPois;k++){
    for(int j=0; j<nPois; j++){
      double w1=iterPoiMap[Form("sigma_bin%d",k)][iter]/iterPoiMap[Form("sigma_bin%d",k)][iter-1];
      double w2=iterPoiMap[Form("sigma_bin%d",j)][iter]/iterPoiMap[Form("sigma_bin%d",j)][iter-1];
    }}
 
  
  delete  minim2;
  delete Nll2;
  iterV.push_back(iter);
  //delete myAsimov2;
 }//end of iterations


 
 TFile *fout=nullptr;
 if( extraName.size()!=0)
   fout=new TFile(Form("iterationsToys_%s.root",extraName.c_str()),"RECREATE");
 else
   fout=new TFile(Form("iterationsToys_%s",fname.c_str()),"RECREATE");
 fout->cd();


 
// Summarize results; 
// Make some plots;
 std::map<string, TGraphAsymmErrors*> xsecIter;
 std::map<string, TGraphAsymmErrors*> nonFidIter;

 std::map<int, TGraphAsymmErrors*> DiffxsecIter;
 std::map<int, TGraphAsymmErrors*> DiffnonFidIter;

 

 TIter it4 = pois->createIterator();
 int color=0;
 while((t2=(RooRealVar*)it4.Next())){
   
   vector <double> xsec; 
   vector <double> xsec_up; 
   vector <double> xsec_down;
   vector <double> nonFid;

  cout<<"Summary for "<<t2->GetName()<<endl;
  for( int iter=0; iter<maxIter; iter++){
    cout<<"Iter "<<iter<<" "<<iterPoiMap[t2->GetName()][iter]
        <<"+ "<<iterPoiErrHigh[t2->GetName()][iter]<<" - "
        <<iterPoiErrLow[t2->GetName()][iter]<<" f "<<notFidIter[t2->GetName()][iter]<<endl;


    xsec.push_back(100*(iterPoiMap[t2->GetName()][iter]-iterPoiMapInputXsec[t2->GetName()])/iterPoiMapInputXsec[t2->GetName()]);
    xsec_up.push_back(fabs(xsec.back()*iterPoiErrHigh[t2->GetName()][iter]/iterPoiMap[t2->GetName()][iter]));
    xsec_down.push_back(fabs(xsec.back()*iterPoiErrLow[t2->GetName()][iter]/iterPoiMap[t2->GetName()][iter]));
    nonFid.push_back(100*(notFidIter[t2->GetName()][iter]-notFidIter[t2->GetName()][0])/notFidIter[t2->GetName()][0]);
    cout<<"xsec "<<xsec.back()<<" + "<<xsec_up.back()<<" - "<<xsec_down.back()<<endl;
  }

  xsecIter[t2->GetName()]=new TGraphAsymmErrors(iterV.size(),&iterV[0],&xsec[0],0,0,&xsec_down[0],&xsec_up[0]);
  xsecIter[t2->GetName()]->SetMarkerStyle(20+color); 
  xsecIter[t2->GetName()]->SetMarkerColor(1+color); 
  xsecIter[t2->GetName()]->SetLineColor(1+color); 
  nonFidIter[t2->GetName()]=new TGraphAsymmErrors(iterV.size(),&iterV[0],&nonFid[0],0,0,0,0);
  nonFidIter[t2->GetName()]->SetLineStyle(1+color);
  nonFidIter[t2->GetName()]->SetMarkerColor(1+color);
  nonFidIter[t2->GetName()]->SetLineColor(1+color);
  
  nonFidIter[t2->GetName()]->SetLineWidth(2);
  color++;
 }

 color=0;
 
 
 for( int iter=0; iter<maxIter; iter++){
   vector <double> xsec; 
   vector <double> xsec_up; 
   vector <double> xsec_down;
   vector <double> nonFid;

   t2=nullptr;
   TIter it5 = pois->createIterator();
   while((t2=(RooRealVar*)it5.Next())){
    
     cout<<"Summary for "<<t2->GetName()<<endl;
     cout<<"Iter "<<iter<<" "<<iterPoiMap[t2->GetName()][iter]
         <<"+ "<<iterPoiErrHigh[t2->GetName()][iter]<<" - "
         <<iterPoiErrLow[t2->GetName()][iter]<<" f "<<notFidIter[t2->GetName()][iter]<<endl;
     
     
     xsec.push_back(100*(iterPoiMap[t2->GetName()][iter]-iterPoiMapInputXsec[t2->GetName()])/iterPoiMapInputXsec[t2->GetName()]);
     xsec_up.push_back(fabs(xsec.back()*iterPoiErrHigh[t2->GetName()][iter]/iterPoiMap[t2->GetName()][iter]));
     xsec_down.push_back(fabs(xsec.back()*iterPoiErrLow[t2->GetName()][iter]/iterPoiMap[t2->GetName()][iter]));
     nonFid.push_back(100*(notFidIter[t2->GetName()][iter]-notFidIter[t2->GetName()][0])/notFidIter[t2->GetName()][0]);
     cout<<"Diff [%] "<<xsec.back()<<" + "<<xsec_up.back()<<" - "<<xsec_down.back()<<endl;
   }
   DiffxsecIter[iter]=new TGraphAsymmErrors(nPoisV.size(),&nPoisV[0],&xsec[0],0,0,&xsec_down[0],&xsec_up[0]);
   DiffxsecIter[iter]->SetMarkerStyle(20+color); 
   DiffnonFidIter[iter]=new TGraphAsymmErrors(nPoisV.size(),&nPoisV[0],&nonFid[0],0,0,0,0);
   DiffnonFidIter[iter]->SetLineStyle(1+color);
   DiffnonFidIter[iter]->SetLineWidth(2);
   
   color++;
}

 

 TLegend *leg=DefLeg();

 TH1F *hcXsecDraw=new TH1F("hcXsecDraw","",maxIter+4,0,maxIter+4);
 SetDef(hcXsecDraw);
 hcXsecDraw->GetXaxis()->SetTitle("Iteration");
 hcXsecDraw->GetYaxis()->SetTitle("#Deltad(#sigma/d#it{x}) [%]");
 hcXsecDraw->SetLineColor(kWhite);
 hcXsecDraw->GetYaxis()->SetRangeUser(-100,+100);

 TCanvas *cXsec=new TCanvas("cXsec","");{
   SetCanvasDefaults(cXsec);
   cXsec->cd();
   hcXsecDraw->Draw();
   for(map<string, TGraphAsymmErrors*>::iterator it=xsecIter.begin(); it!=xsecIter.end(); it++){
     (*it).second->Draw("PE same");
     (*it).second->Write();
     //nonFidIter[(*it).first]->Draw("L same");
     leg->AddEntry((*it).second,Form("%s",(*it).first.c_str()),"PLE");
   }
   leg->Draw();
   
 }
 

 TLegend *legDiff=DefLeg();
 //legDiff->SetHeader();
 TH1F *hcDiffXsecDraw=new TH1F("hcDiffXsecDraw","",nPois+4,0,nPois+4);
 SetDef(hcDiffXsecDraw);
 hcDiffXsecDraw->GetXaxis()->SetTitle("Bin");
 hcDiffXsecDraw->GetYaxis()->SetTitle("#Deltad(#sigma/d#it{x}) [%]");
 hcDiffXsecDraw->SetLineColor(kWhite);
 hcDiffXsecDraw->GetYaxis()->SetRangeUser(-100,+100);
 
 TCanvas *cDiffXsec=new TCanvas("cDiffXsec","");{
   SetCanvasDefaults(cDiffXsec);
   cDiffXsec->cd();
   hcDiffXsecDraw->Draw();
   for(map<int, TGraphAsymmErrors*>::iterator it=DiffxsecIter.begin(); it!=DiffxsecIter.end(); it++){
     if((*it).first==0) continue; 
     (*it).second->Draw("PE same");
     (*it).second->Write();
     //DiffnonFidIter[(*it).first]->Draw("L same");
     legDiff->AddEntry((*it).second,Form("Iter. %d",(*it).first),"PLE");
   }
   legDiff->Draw();
 }

 w->loadSnapshot("nominal");
 //setup RooUnfold 
 // create a histogram of the measured
 TH1F *hmeas=new TH1F("hmeas","hmeas",nPois,0,nPois);
 TH1F *htrue=new TH1F("htrue","htrue",nPois,0,nPois);
 TH1F *htrueXsec=new TH1F("htrueXsec","htrueXsec",nPois,0,nPois);
 TH2F *migMatrix=new TH2F("htrue","htrue",nPois,0,nPois,nPois,0,nPois);
 TH1D *hmeasXsec= new TH1D("hmeasXsec","hmeasXsec",nPois,0,nPois);
 hmeasXsec->SetLineColor(kBlue);
 

 for(int k=0;k<nPois;k++){
   hmeas->SetBinContent(k+1,initNtotEvents[k]);
   cout<<"Reco bin "<<hmeas->GetBinContent(k+1)<<endl;
   //hmeas->SetBinContent(k+1,initTrueEvents[k]);
   htrue->SetBinContent(k+1,initTrueEvents[k]);
   double ineff=0;

   for(int j=0; j<nPois; j++)
     ineff+=matrix[k][j]->getVal();
   
 for(int j=0; j<nPois; j++){
     //migMatrix->SetBinContent(k+1,j+1,matrix[k][j]->getVal()*initTrueEvents[k]);
     //migMatrix->SetBinContent(k+1,j+1,matrix[k][j]->getVal());
     //migMatrix->SetBinContent(j+1,k+1,matrix[k][j]->getVal()*initNtotEvents[k]);
   migMatrix->SetBinContent(k+1,j+1,matrix[k][j]->getVal()*(initTrueEvents[k]-initNtotEvents[k]*(ineff)) );
     
     //migMatrix->SetBinContent(k+1,j+1,matrix[k][j]->getVal()*initTrueEvents[k]);
     //if(k==j)
     //migMatrix->SetBinContent(k+1,j+1,initTrueEvents[k]);
     //else
     ///migMatrix->SetBinContent(k+1,j+1,0);
   }
 }

#ifdef INCLROOUNFOLD
 RooUnfoldResponse rooUnfoldResponse(hmeas,htrue,migMatrix,"test","test");

 TIter ilp = pois->createIterator();
 t2=nullptr;
 int bin=1;
 while((t2=(RooRealVar*)ilp.Next())){
   htrueXsec->SetBinContent(bin,t2->getVal());
   htrueXsec->SetBinError(bin,t2->getError());
   //for(double e=0.0; e < initNtotEvents[bin]*notFidMap[t2->GetName()]->getVal(); e++)
   //rooUnfoldResponse.Fake(bin);
   bin++;
 }

 //RooUnfoldBayes   unfold (&rooUnfoldResponse, hmeas, 1);    // OR
 //RooUnfoldInvert   unfold (&rooUnfoldResponse, hmeas);    // OR
 RooUnfoldBinByBin unfold (&rooUnfoldResponse, hmeas); 
 TH1D* hReco= (TH1D*) unfold.Hreco();
 for( int i=1; i<hReco->GetNbinsX()+1; i++){
   hmeasXsec->SetBinContent(i,hReco->GetBinContent(i)/(BR_ZZ_incl->getVal()*L->getVal()));
   hmeasXsec->SetBinError(i,hReco->GetBinError(i)/hReco->GetBinContent(i)* hReco->GetBinContent(i)/(BR_ZZ_incl->getVal()*L->getVal()));
   cout<<"Bin "<<hmeasXsec->GetBinContent(i)<<" +/- "<<hmeasXsec->GetBinError(i)<<endl;
 }

 unfold.PrintTable (cout, htrue);
 // here create a bin - by - bin result 
#endif
 
 // here fit the toy with the CF: 
 for(int k=0;k<nPois;k++){
   for(int j=0; j<nPois; j++){
     //if( k==j)  matrix[k][j]->setVal(initNtotEvents[k]/initTrueEvents[k]);
     //else matrix[k][j]->setVal(0.0);
   }}

 TIter illToyp = pois->createIterator();
 t2=nullptr;
 while((t2=(RooRealVar*)illToyp.Next())){
   notFidMap[t2->GetName()]->setVal(0);
 }

  RooAbsReal *Nll4 = pdf->createNLL(*myAsimov,NumCPU(2,2));
  // fit the nominal 
  RooMinimizer *minim4 = new RooMinimizer(*Nll4);              ////OffSet true default
  minim4->setEps(0.1);
  minim4->optimizeConst(2);
  minim4->setStrategy(2);
  minim4->minimize("Minuit2");
  TH1D *hmeasCF= new TH1D("hmeasCF","hmeasCF",nPois,0,nPois);

  TIter ilp2 = pois->createIterator();
  t2=nullptr;
  int binl=1;
  while((t2=(RooRealVar*)ilp2.Next())){
    hmeasCF->SetBinContent(binl,t2->getVal());
    hmeasCF->SetBinError(binl,t2->getError());
    t2->Print();
    binl++;
  }
  hmeasCF->SetLineColor(kGreen);

  
 TCanvas *cUft=new TCanvas("cUft","");{
   SetCanvasDefaults(cUft);
   htrueXsec->GetYaxis()->SetRangeUser(0,1);
   htrueXsec->SetLineColor(kRed);
   htrueXsec->Draw("hist E");
   hmeasXsec->Draw("PE same");
   hmeasCF->Draw("PE same");
   //cUft->Draw();
   //hReco->Draw();
   //hmeas->Draw("SAME");
   //htrue->SetLineColor(8);
   //hmeas->Draw("SAME");
 }
 //return ;
 
 /// make some toys 
 w->loadSnapshot("nominal");

 // Fit the nominal dataset setting all off diagonoal elelments of the matrix to 0; 

 std::map<int,std::map<int,double>> inputM;
 // Re weight the matrix 
 for(int k=0;k<nPois;k++){
   for(int j=0; j<nPois; j++){
     inputM[k][j]=matrix[k][j]->getVal();
     
   }
 }

 // change the matrix
 for(int k=0;k<nPois;k++){
   for(int j=0; j<nPois; j++){
     matrix[k][j]->setRange(-1000,+1000);
     if( k==j) {
       //matrix[k][j]->setVal(Cfactors[k]);
       matrix[k][j]->setVal(1.0);
       //matrix[k][j]->setVal(initNtotEvents[k]/initTrueEvents[k]);
       //matrix[k][j]->setVal(matrix[k][j]->getVal()+totDiag[k]);
       //cout<<"New CF "<<matrix[k][j]->getVal()<<" matrix eff "<<inputM[k][j]<<" bin "<<k<<" j"<<endl;
     }
     else 
       matrix[k][j]->setVal(0.0);
     cout<<"New CF "<<matrix[k][j]->getVal()<<" matrix eff "<<inputM[k][j]<<" bin "<<k<<" " << j <<endl;;
   }}
 
 TIter ill = pois->createIterator();
  t2=nullptr;
  while((t2=(RooRealVar*)ill.Next())){
    //notFidMap[t2->GetName()]->setVal(notFidIter[t2->GetName()][0]);
    notFidMap[t2->GetName()]->setVal(0);
  }
  

  RooAbsReal *Nll3 = pdf->createNLL(*myAsimov,NumCPU(2,2));
  RooMinimizer *minim3 = new RooMinimizer(*Nll3);              ////OffSet true default
  minim3->setEps(0.1);
  minim3->optimizeConst(2);
  minim3->setStrategy(2);
  minim3->minimize("Minuit2");
  if(useMinos)
    minim3->minos(*pois);
  cout<<"Fit with correction factor "<<endl;
  pois->Print("V");

  w->saveSnapshot("CF",allPars);
  
// Re weight the matrix 
 for(int k=0;k<nPois;k++){
   for(int j=0; j<nPois; j++){
     matrix[k][j]->setVal(inputM[k][j]);
   }
 }

 TIter ill2 = pois->createIterator();
 t2=nullptr;
 while((t2=(RooRealVar*)ill2.Next())){
   notFidMap[t2->GetName()]->setVal(notFidIter[t2->GetName()][0]);
   cout<<t2->GetName()<<" CF - Matrix / Matrix "<<(t2->getVal() - iterPoiMap[t2->GetName()][0])/iterPoiMap[t2->GetName()][0]*100<<" % "<<endl;
 }
 

 w->loadSnapshot("nominal");


 TTree *toysTree=new TTree("toys","toys");
 //toysTree->SetDirectory(fout);
 
  RooMsgService::instance().setSilentMode(kTRUE);
  RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL);   

  std::map<int, std::map<string, TH1F * >> pullXsecBin;
  std::map<int, std::map<string, TH1F * >> DiffXsecBin;
  std::map<int, std::map<string, TH1F * >> DiffXSecBinError;

  std::map<int, std::map<string, TH1F * >> pullXsecBinCF;
  std::map<int, std::map<string, TH1F * >> DiffXsecBinCF;
  std::map<int, std::map<string, TH1F * >> DiffXSecBinErrorCF;
  std::map<string, double> trueSigma;
  std::map<string,TBranch*> trueSigmaBranchMap;

  
  std::map<string, vector<double>> sigmaBinMatrix;
  std::map<string, vector<double>> sigmaBinMatrix_e;
  std::map<string, vector<double>> sigmaBinCF;
  std::map<string, vector<double>> sigmaBinCF_e;
  std::map<string, vector<double>> sigmaBinIBU;
  std::map<string, vector<double>> sigmaBinIBU_e;
  std::map<string, double[20] > weightBdt;
  std::map<string, int> weightBdtSize; 
  
  std::map<string,TBranch*> sigmaBinMatrixBranch;
  std::map<string,TBranch*> sigmaBinMatrix_eBranch;
  std::map<string,TBranch*> sigmaBinCFBranch;
  std::map<string,TBranch*> sigmaBinCF_eBranch;
  std::map<string,TBranch*> sigmaBinIBUBranch;
  std::map<string,TBranch*> sigmaBinIBU_eBranch;
  std::map<string, TBranch*> weightBdtBranch;
  std::map<string, TBranch*> weightBdtSizeBranch;
  
  TBranch *p1Branch;
  TBranch *p2Branch;

  map< string, TBranch*> trueXSBranchMap;
  
  std::map <string, double> PoiMapTrueToy;

  TIter itt = pois->createIterator();
  t2=nullptr;
  while((t2=(RooRealVar*)itt.Next())){
    if(useMinos)
      trueSigma[t2->GetName()]=0.5*(fabs(t2->getErrorLo())+fabs(t2->getErrorHi()));
    else  trueSigma[t2->GetName()]=t2->getError();
    
    t2->setVal(iterPoiMap[t2->GetName()][0]);
  }
  
  for(std::map<string,double>::iterator it=trueSigma.begin(); it!=trueSigma.end(); it++)
    trueSigmaBranchMap[(*it).first]=toysTree->Branch(Form("Sigma_%s",(*it).first.c_str()),&(*it).second,Form("Sigma_%s/D",(*it).first.c_str()));

  double p1=0;
  double p2=0;

  p1Branch=toysTree->Branch("p1",&p1);
  p2Branch=toysTree->Branch("p2",&p2);

  for(vector<pair<string,string>>::iterator it=matRegions.begin(); it!=matRegions.end(); it++){
    for(int n=0; n<nPois; n++){
      weightBdtSize[Form("weight_%s_bin%d",(*it).first.c_str(),n)]=20;
      //weightBdt[Form("weight_%s_bin%d",(*it).first.c_str(),n)]=vector<double>(weightBdtSize[Form("weight_%s_bin%d",(*it).first.c_str(),n)]);
    }
  }
  
  TRandom3 *rand=new TRandom3();
  rand->SetSeed(0);
  
  for ( int nToy=0; nToy<nToys; nToy++){
    //cout << nToy << " toy " <<endl;
    
    w->loadSnapshot("nominal");
    TIter it6 = pois->createIterator();
    t2=nullptr;

    p1=0;
    p2=0;

    
    for(vector<pair<string,string>>::iterator it=matRegions.begin(); it!=matRegions.end(); it++){
      for(int n=0; n<nPois; n++){
	// initialize all to 0 weights
	for( int ik=0; ik<(int)weightBdtSize[Form("weight_%s_bin%d",(*it).first.c_str(),n)]; ik++){
	  weightBdt[Form("weight_%s_bin%d",(*it).first.c_str(),n)][ik]=0;
	}
	if(nToy==0){
	  weightBdtSizeBranch[Form("weight_%s_bin%d",(*it).first.c_str(),n)]=toysTree->Branch(Form("weight_%s_bin%dsize",(*it).first.c_str(),n),&weightBdtSize[Form("weight_%s_bin%d",(*it).first.c_str(),n)],
											      Form("weight_%s_bin%dsize/I",(*it).first.c_str(),n));
	  
	  weightBdtBranch[Form("weight_%s_bin%d",(*it).first.c_str(),n)]=toysTree->Branch(Form("weight_%s_bin%d",(*it).first.c_str(),n),&weightBdt[Form("weight_%s_bin%d",(*it).first.c_str(),n)],Form("weight_%s_bin%d[weight_%s_bin%dsize]/D",(*it).first.c_str(),n,(*it).first.c_str(),n));
	}
      }}
    
    
    
    

    
    
    while((t2=(RooRealVar*)it6.Next())){
      notFidMap[t2->GetName()]->setVal(notFidIter[t2->GetName()][0]);
      sigmaBinMatrix[t2->GetName()].clear();
      sigmaBinMatrix_e[t2->GetName()].clear();
      sigmaBinCF[t2->GetName()].clear();
      sigmaBinCF_e[t2->GetName()].clear();
      sigmaBinIBU[t2->GetName()].clear();
      sigmaBinIBU_e[t2->GetName()].clear();


      if(nToy==0){
        sigmaBinMatrixBranch[t2->GetName()]=toysTree->Branch(Form("matrix_%s",t2->GetName()),&sigmaBinMatrix[t2->GetName()]);
        sigmaBinMatrix_eBranch[t2->GetName()]=toysTree->Branch(Form("matrix_e_%s",t2->GetName()),&sigmaBinMatrix_e[t2->GetName()]);
        sigmaBinCFBranch[t2->GetName()]=toysTree->Branch(Form("CF_%s",t2->GetName()),&sigmaBinCF[t2->GetName()]);
        sigmaBinCF_eBranch[t2->GetName()]=toysTree->Branch(Form("CF_e_%s",t2->GetName()),&sigmaBinCF_e[t2->GetName()]);

        sigmaBinIBUBranch[t2->GetName()]=toysTree->Branch(Form("IBU_%s",t2->GetName()),&sigmaBinIBU[t2->GetName()]);
        sigmaBinIBU_eBranch[t2->GetName()]=toysTree->Branch(Form("IBU_e_%s",t2->GetName()),&sigmaBinIBU_e[t2->GetName()]);
	
	
      }

      //sigmaBinMatrixBranch[t2->GetName()]->SetAddress(&sigmaBinMatrix[t2->GetName()]);
      //sigmaBinMatrix_eBranch[t2->GetName()]->SetAddress(&sigmaBinMatrix_e[t2->GetName()]);
      //sigmaBinCFBranch[t2->GetName()]->SetAddress(&sigmaBinCF[t2->GetName()]);
      //sigmaBinCF_eBranch[t2->GetName()]->SetAddress(&sigmaBinCF_e[t2->GetName()]);
    }

    std::map <string, std::map<int,double>> iterPoiMapToy;
    
    std::map <string, std::map<int,double>> iterPoiErrLowToy;
    std::map <string, std::map<int,double>> iterPoiErrHighToy;
    std::map<string, std::map<int,double>>  notFidIterToy;
    std::map<string ,std::map<int,double>> nTotIter;

    if(nToy % 100 == 0 ) cout<<"Generated "<<nToy*100.00/nToys<<" % "<<endl;
    
    // here fluctuate the cross section with the 
    // For each toy change the dataset
    //RooAbsData *toy=AsymptoticCalculator::GenerateAsimovData(*pdf,*obsrvables);
    

    RooArgSet toRandomize;
    toRandomize.add(*obsrvables);
    //toRandomize.add(pois);
    
    TIter itp = pois->createIterator();
    t2=nullptr;
    while((t2=(RooRealVar*)itp.Next())){
      // Poisson sampling 
      //PoiMapTrueToy[t2->GetName()]=rand->Poisson(t2->getVal());
      // Gaussian sampling 
      PoiMapTrueToy[t2->GetName()]=rand->Gaus(t2->getVal(),trueSigma[t2->GetName()]);
      
      // Uniform sampling
      if(randomizeAlsotheBDT){
	//PoiMapTrueToy[t2->GetName()]=rand->Uniform(t2->getVal()-3*trueSigma[t2->GetName()],t2->getVal()+3*trueSigma[t2->GetName()]);
	// diable the random total cross sections fluctuatins
	PoiMapTrueToy[t2->GetName()]= trueSigma[t2->GetName()];
      }

      t2->setVal( PoiMapTrueToy[t2->GetName()]);
      
      if( nToy==0){
        trueXSBranchMap[t2->GetName()]=toysTree->Branch(Form("trueXS_%s",t2->GetName()),&PoiMapTrueToy[t2->GetName()]);
      }
      //trueSigmaBranchMap[t2->GetName()]->SetAddress(&trueSigma[t2->GetName()]);
      //trueXSBranchMap[t2->GetName()]->SetAddress(&PoiMapTrueToy[t2->GetName()]);
    }

    cout<<"Input corss sections "<<endl;
    pois->Print("V"); 

    RooDataSet *toy=nullptr;

    
    if( randomizeAlsotheBDT){
      
      
      // is this a correct range ? 
      p1= rand->Uniform(-2,+2);
      //p2 = rand->Uniform(-1,+1);
      cout<<"Randimization parameters "<<p1<<" and "<<p2<<endl; 
      // modify the bdt signal spectra
      RooWorkspace *wtemp=new RooWorkspace("wtemp","wtemp");
      // loop over all compoents of the RooSimultaneous
      // loop over all the differntial bins 
      for(int kk=0; kk<nPois; kk++){
	// for each differntial bin modify the reco shape of the bdt observabale

	std::map<string,double> totalSignalEvents;
	double intTotSigEvents=0;
	for(vector<pair<string,string>>::iterator it=matRegions.begin(); it!=matRegions.end(); it++){
	  RooHistFunc *tmpFunc=(RooHistFunc*)w->obj(Form("vbf0_%d_%s_%d_%s_nominal",kk,(*it).first.c_str(),kk,(*it).second.c_str()));
	  if( tmpFunc==nullptr ) {
	    cout<<"object "<<Form("vbf0_%d_%s_%d_%s_nominal",kk,(*it).first.c_str(),kk,(*it).second.c_str())<<" is null" <<endl;
	    exit(-1);
	    continue ;
	  }
	  
	  totalSignalEvents[ (*it).first] = (tmpFunc->dataHist()).numEntries();
	  intTotSigEvents+=(tmpFunc->dataHist()).numEntries();
	}
	

	// take the nominal original RooHistFunction; vbf0_0_SRVBF_0_bdt_vbf_nominal
	for(vector<pair<string,string>>::iterator it=matRegions.begin(); it!=matRegions.end(); it++){
	  RooHistFunc *tmpFunc=(RooHistFunc*)w->obj(Form("vbf0_%d_%s_%d_%s_nominal",kk,(*it).first.c_str(),kk,(*it).second.c_str()));
	  if( tmpFunc==nullptr ) {
	    cout<<"object "<<Form("vbf0_%d_%s_%d_%s_nominal",kk,(*it).first.c_str(),kk,(*it).second.c_str())<<" is null" <<endl;
	    exit(-1);
	    continue ;
	  }
	  //obs_x_SRVBF_0_bdt_vbf
	  RooRealVar *tmpobs=(RooRealVar*)w->obj(Form("obs_x_%s_%d_%s",(*it).first.c_str(),kk,(*it).second.c_str()));
	  if(tmpobs==nullptr) {
	    cout<<"Object "<<Form("obs_x_%s_%d_%s",(*it).first.c_str(),kk,(*it).second.c_str())<<" is null"<<endl;
	    exit(-1);
	    continue;

	  }
	  RooDataHist dataHist=tmpFunc->dataHist();
	  // integral
	  double sumw=dataHist.sumEntries();
	  int numE=dataHist.numEntries();
	  vector <double> inWeights;
	  for( int e=0; e<(int)numE; e++){
	    const RooArgSet *currarg=dataHist.get(e);
	    //currarg->Print("V");
	    double origWeight=dataHist.weight();
	    RooRealVar *xvar=(RooRealVar*)currarg->find(tmpobs->GetName());
	    //cout<<"Value "<<xvar->getVal()<<endl;
	    double newWeight=origWeight*(1+xvar->getVal()*p1+pow(xvar->getVal(),2)*p2);
	    //if(newWeight<0) newWeight = 0; 
	    dataHist.set(*currarg,newWeight);
	    const RooArgSet *currarg2=dataHist.get(e);
	    //cout<<"Changing weight from "<<origWeight<<" to "<<dataHist.weight()<<" cross check "<<origWeight*(1+xvar->getVal()*p1+pow(xvar->getVal(),2)*p2)  <<endl;
	    inWeights.push_back(origWeight);
	  }
	  double newIntegral=dataHist.sumEntries();
	  // rescale so that the integral matches that of before

	  weightBdtSize[Form("weight_%s_bin%d",(*it).first.c_str(),kk)]=numE;

	  double weights[numE]; 
	  
	  for ( int e=0; e<(int)numE; e++){
	    const RooArgSet *currarg=dataHist.get(e);
	    double origWeight=dataHist.weight();
	    
	    dataHist.set(*currarg,origWeight*sumw /newIntegral);
	    // normalise to the integral of cross section on both regions
	    //dataHist.set(*currarg, origWeight * intTotSigEvents / newIntegral *   totalSignalEvents[ (*it)] )


	    const RooArgSet *currarg2=dataHist.get(e);
	    //cout <<" Global normalisation "<<dataHist.weight()<<" previous normalisations "<<newIntegral<<" old integral "<<sumw<<endl;
	    weights[e]=dataHist.weight() / inWeights.at(e);
	    
	    //weightBdt[Form("weight_%s_bin%d",(*it).first.c_str(),kk)].at(e)=( dataHist.weight() / inWeights.at(e));
	    
	    //cout<<"Name "<<Form("weight_%s_bin%d",(*it).first.c_str(),kk)<<" size "<<weightBdt[Form("weight_%s_bin%d",(*it).first.c_str(),kk)].size()<<endl;
	    //cout<<"Weight is "<<weightBdt[Form("weight_%s_bin%d",(*it).first.c_str(),kk)].at(e)<<endl;
	  }

	  for( int e=0; e<(int)numE; e++)
	    weightBdt[Form("weight_%s_bin%d",(*it).first.c_str(),kk)][e]=weights[e];
	  
	  wtemp->import(dataHist,RecycleConflictNodes());
	  // create a new RooHistFunct
	  
	  RooHistFunc newFunc(tmpFunc->GetName(),tmpFunc->GetTitle(),*tmpobs,dataHist,tmpFunc->getInterpolationOrder());
	  //tmpFunc->SetName(Form("oldName_%s",tmpFunc->GetName()));
	  //cout<<"Importing new HistFunction"<<endl;
	  //wtemp->import(*tmpobs, RecycleConflictNodes());
	  wtemp->import(newFunc,RecycleConflictNodes());
	  
	  //delete newFunc;
	  //delete tmpobs;
	  //delete tmpFunc;
	}

	
	
      }//end loop over diff bins 

      
      string pdfName=pdf_->GetName();
      RooSimultaneous* simPDF2=(RooSimultaneous*)(model->GetPdf())->Clone(pdfName.c_str());
      //wtemp->import(*simPDF2,RecycleConflictNodes());
      wtemp->merge(*w);
    
      ModelConfig *model2=new ModelConfig(model->GetName(),wtemp);
      model2->SetWorkspace(*wtemp);
      model2->SetPdf(*simPDF2);
      
      model2->SetGlobalObservables(*model->GetGlobalObservables());
      model2->SetObservables(*model->GetObservables());
      if(model->GetParametersOfInterest()!=nullptr)
	model2->SetParametersOfInterest(*model->GetParametersOfInterest());
      if(model->GetConstraintParameters()!=nullptr)
	model2->SetConstraintParameters(*model->GetConstraintParameters());
      if(model->GetConditionalObservables()!=nullptr)
	model2->SetConditionalObservables(*model->GetConditionalObservables());
      if(model->GetNuisanceParameters()!=nullptr)
	model2->SetNuisanceParameters(*model->GetNuisanceParameters());
      
      model2->SetName(model->GetName());
      model2->SetWorkspace(*wtemp);
      wtemp->import(*model2);
      
      RooAbsPdf *newPdf=(RooAbsPdf*)wtemp->obj(pdfName.c_str());
      if(newPdf==nullptr) {
	cout<<"New pdf is null!! ??"<<endl;
	exit(-1);
      }
	
      toy=(RooDataSet*)newPdf->generate(toRandomize,Extended(),AutoBinned());
      cout<<"Finalised toy generation"<<endl;
      //delete model2;
      //delete newPdf;
      delete wtemp;
      
    }
    
    else {
      toy=(RooDataSet*)pdf->generate(toRandomize,Extended(),AutoBinned());
      //RooDataSet *toy=(RooDataSet*)pdf->generateBinned(toRandomize,Extended());
      //RooDataSet *toy=(RooDataSet*)AsymptoticCalculator::GenerateAsimovData(*pdf,*obsrvables);
    }


    
    toy->Print();
    // reset the values of the cross sections;
    w->loadSnapshot("nominal");
    
    
    // Make some iterations 
    for( int iter=0; iter<maxIter; iter++){
      // reset the values of the cross sections;
      w->loadSnapshot("nominal");
      // setting some stuff 
      TIter itl = pois->createIterator();
      t2=nullptr;
      int lk=0;
      while((t2=(RooRealVar*)itl.Next())){
        nTotIter[t2->GetName()][iter]=normMap[lk]->getVal()*L->getVal();
        //notFidIterToy[t2->GetName()][iter]=notFidMap[t2->GetName()]->getVal();
        lk++;
      }

      RooAbsReal *Nllt = nullptr;
      RooMinimizer *minimt = nullptr;
      int status=-1;
      if(!fastFit){
        Nllt=pdf->createNLL(*toy,NumCPU(2,2));
        minimt=new RooMinimizer(*Nllt);              ////OffSet true default
        minimt->setEps(0.1);
        minimt->optimizeConst(2);
        minimt->setStrategy(2);
        status=minimt->minimize("Minuit2");
      }
      else {
        RooFitResult *r=pdf->fitTo(*toy,NumCPU(4),Extended(),Save());
        status=r->status();
        delete r; 
      }
      if(useMinos)
          status+=minimt->minos(*pois);
      /*
      if(status!=0) {
        cout<<" Fit failed "<<endl;
        delete Nllt;
        delete minimt;
        continue;
      }
      */

      //goto end; 
      //if(nToy % 100 == 0 ){
      cout<<"Iteration "<<iter<<endl;
      pois->Print("V");
      //}
      
      TIter it5 = pois->createIterator();
        t2=nullptr;
        lk=0;
        while((t2=(RooRealVar*)it5.Next())){
          if(nToy==0){
            pullXsecBin[iter][t2->GetName()]=new TH1F(Form("h_pullToys_%s_iter_%d",t2->GetName(),iter),Form("h_pullToys_%s_iter_%d",t2->GetName(),iter),100,-5,+5);
            DiffXsecBin[iter][t2->GetName()]=new TH1F(Form("h_DiffXsecBin_%s_iter_%d",t2->GetName(),iter),Form("h_DiffXsecBin_%s_iter_%d",t2->GetName(),iter),100,-5,+5);
            DiffXSecBinError[iter][t2->GetName()]=new TH1F(Form("h_DiffXSecBinError_%s_iter_%d",t2->GetName(),iter),Form("h_DiffXSecBinError_%s_iter_%d",t2->GetName(),iter),100,t2->getError()*0.1,t2->getError()*10);
            
            pullXsecBinCF[iter][t2->GetName()]=new TH1F(Form("h_pullToysCF_%s_iter_%d",t2->GetName(),iter),Form("h_pullToysCF_%s_iter_%d",t2->GetName(),iter),100,-5,+5);
            pullXsecBinCF[iter][t2->GetName()]->SetLineColor(kRed);
            
            DiffXsecBinCF[iter][t2->GetName()]=new TH1F(Form("h_DiffXsecBinCF_%s_iter_%d",t2->GetName(),iter),Form("h_DiffXsecBinCF_%s_iter_%d",t2->GetName(),iter),100,-5,+5);
            DiffXsecBinCF[iter][t2->GetName()]->SetLineColor(kRed);
            
            DiffXSecBinErrorCF[iter][t2->GetName()]=new TH1F(Form("h_DiffXSecBinErrorCF_%s_iter_%d",t2->GetName(),iter),Form("h_DiffXSecBinErrorCF_%s_iter_%d",t2->GetName(),iter),100,t2->getError(),t2->getError()*10);
            DiffXSecBinErrorCF[iter][t2->GetName()]->SetLineColor(kRed);
          }


          iterPoiMapToy[t2->GetName()][iter]=t2->getVal();
          if(useMinos){
          iterPoiErrLowToy[t2->GetName()][iter]=t2->getErrorLo();
          iterPoiErrHighToy[t2->GetName()][iter]=t2->getErrorHi();
          }
          else {
            iterPoiErrLowToy[t2->GetName()][iter]=t2->getError();
            iterPoiErrHighToy[t2->GetName()][iter]=t2->getError();
          }

          notFidIterToy[t2->GetName()][iter]=notFidMap[t2->GetName()]->getVal();
          if(nToy % 100 == 0 ){
            cout<<"Iter "<<iter<<" "<<t2->GetName()<<" "<<iterPoiMapToy[t2->GetName()][iter]
                <<"+ "<<iterPoiErrHighToy[t2->GetName()][iter]<<" - "
                <<iterPoiErrLowToy[t2->GetName()][iter]<<" f "<<notFidIterToy[t2->GetName()][iter]<<endl;
          }
          
          if(useMinos)
            pullXsecBin[iter][t2->GetName()]->Fill( (t2->getVal() -iterPoiMapInputXsec[t2->GetName()] ) / (t2->getError()));
          else 
            pullXsecBin[iter][t2->GetName()]->Fill( (t2->getVal() -iterPoiMapInputXsec[t2->GetName()] ) / (0.5*(fabs(t2->getErrorLo())+fabs(t2->getErrorHi()))));

          DiffXsecBin[iter][t2->GetName()]->Fill(t2->getVal() - PoiMapTrueToy[t2->GetName()]);
          if(useMinos)
            DiffXSecBinError[iter][t2->GetName()]->Fill(0.5*(fabs(t2->getErrorLo())+fabs(t2->getErrorHi())));
          else 
            DiffXSecBinError[iter][t2->GetName()]->Fill(t2->getError());

          if(iter>0){
            // number of fiducial events: 
            double nFid= nTotIter[t2->GetName()][iter-1]*(1-notFidIterToy[t2->GetName()][iter-1]);
            double nNotFid=nTotIter[t2->GetName()][iter]*notFidIterToy[t2->GetName()][iter-1];
            double DeltaN=nTotIter[t2->GetName()][iter] - nFid - nNotFid ; 
            // f(i+1)=NnotFid/NTot(i) + DeltaN/Ntot(i+1)
            //notFidMap[t2->GetName()]->setVal(nNotFid/nTotIter[t2->GetName()][iter]+DeltaN/normMap[lk]->getVal());
            //cout<<"Old f "<< notFidIterToy[t2->GetName()][iter-1]<<" new f "<< notFidIterToy[t2->GetName()][iter]<<" nFid "<<
            //nFid<<" nNotFid "<<nNotFid<<" delta N "<<DeltaN<<endl;
          }

          notFidIterToy[t2->GetName()][iter]=notFidMap[t2->GetName()]->getVal();
          // tree related stuff 
          sigmaBinMatrix[t2->GetName()].push_back(t2->getVal());
          if(useMinos)
            sigmaBinMatrix_e[t2->GetName()].push_back((0.5*(fabs(t2->getErrorLo())+fabs(t2->getErrorHi()))));
          else 
            sigmaBinMatrix_e[t2->GetName()].push_back(t2->getError());

          lk++;
        }

        if(iter>0){
        
        // Re weight the matrix 
          for(int k=0;k<nPois;k++){
            for(int j=0; j<nPois; j++){
              double w1=iterPoiMapToy[Form("sigma_bin%d",k)][iter]/iterPoiMapToy[Form("sigma_bin%d",k)][iter-1];
              double w2=iterPoiMapToy[Form("sigma_bin%d",j)][iter]/iterPoiMapToy[Form("sigma_bin%d",j)][iter-1];
              //if(k==j) 
              //matrix[k][j]->setVal(matrix[k][j]->getVal()*w1);
              //cout<<"Setting value of ("<<k<<","<<j<<") "<<matrix[k][j]->getVal()<<" w1 "<<w1<<" w2 "<<w2;
              //matrix[k][j]->setVal(matrix[k][j]->getVal()*sqrt(w1*w2));
              //cout<<" to "<< matrix[k][j]->getVal()<<endl;
            }}
        }
        
        //delete Nllt;
        delete minimt;

        

        // here fit the toy with the CF: 
        for(int k=0;k<nPois;k++){
          for(int j=0; j<nPois; j++){
	    matrix[k][j]->setRange(-1000,+1000);
            if( k==j) {
	      //matrix[k][j]->setVal(Cfactors[k]);
	      matrix[k][j]->setVal(1.0);
	      //matrix[k][j]->setVal(initNtotEvents[k]/initTrueEvents[k]);
	    }
            else matrix[k][j]->setVal(0.0);
          }}
        
        TIter illToy2 = pois->createIterator();
        t2=nullptr;
        while((t2=(RooRealVar*)illToy2.Next())){
          notFidMap[t2->GetName()]->setVal(0);
        }
  

        RooAbsReal *Nll3t =nullptr; 
        RooMinimizer *minim3t = nullptr ;
        int statusCF=-1;

        if(!fastFit){
          Nll3t=pdf->createNLL(*toy,NumCPU(2,2));
          minim3t=new RooMinimizer(*Nll3t);              ////OffSet true default
          minim3t->setEps(0.1);
          minim3t->optimizeConst(2);
          minim3t->setStrategy(2);
          statusCF= minim3t->minimize("Minuit2");
          if(useMinos)
            statusCF+=minim3t->minos(*pois);
        }
        else {
          RooFitResult *r=pdf->fitTo(*toy,NumCPU(4),Extended(),Save());
          status=r->status();
          delete r; 
        }

        for(int k=0;k<nPois;k++){
          for(int j=0; j<nPois; j++){
            matrix[k][j]->setVal(inputM[k][j]);
          }
        }

        if(statusCF!=0) {
          cout<<"CF fit did not converge"<<endl;
          w->loadSnapshot("nominal");
          delete Nll3t;
          delete minim3t;
          TIter ill3R = pois->createIterator();
          t2=nullptr;
          while((t2=(RooRealVar*)ill3R.Next())){
            notFidMap[t2->GetName()]->setVal(notFidIter[t2->GetName()][0]);
          }
          continue;
        }
        

        TIter ill2R = pois->createIterator();
        t2=nullptr;
        //if(nToy % 100 == 0 )
	cout<<"CF results "<<endl;
        while((t2=(RooRealVar*)ill2R.Next())){
          // Fill the histograms 
          //if(nToy % 100 == 0 )
            cout<<t2->GetName()<<" "<<t2->getVal()<<" "<<t2->getError()<<" "<<t2->getErrorLo()<<" + "<<t2->getErrorHi()<<endl;

          pullXsecBinCF[iter][t2->GetName()]->Fill( (t2->getVal() -iterPoiMapInputXsec[t2->GetName()] ) / (t2->getError()));
          DiffXsecBinCF[iter][t2->GetName()]->Fill(t2->getVal() - PoiMapTrueToy[t2->GetName()]);
          if(useMinos)
            DiffXSecBinErrorCF[iter][t2->GetName()]->Fill(0.5*(fabs(t2->getErrorLo())+fabs(t2->getErrorHi())));
          else 
            DiffXSecBinErrorCF[iter][t2->GetName()]->Fill(t2->getError());
          notFidMap[t2->GetName()]->setVal(notFidIter[t2->GetName()][0]);
           // tree related stuff 
          sigmaBinCF[t2->GetName()].push_back(t2->getVal());
          if(useMinos)
            sigmaBinCF_e[t2->GetName()].push_back((0.5*(fabs(t2->getErrorLo())+fabs(t2->getErrorHi()))));
          else 
            sigmaBinCF_e[t2->GetName()].push_back(t2->getError());
        }
          // Here unfold with Baesian 
          TH1F *hmeasT=new TH1F("hmeasT","hmeas",nPois,0,nPois);
          TH1F *htrueT=new TH1F("htrueT","htrue",nPois,0,nPois);
          TH2F *migMatrixT=new TH2F("migMatrixT","migMatrixT",nPois,0,nPois,nPois,0,nPois);

          TIter ilpk = pois->createIterator();
          t2=nullptr;
          int bin=1;
          while((t2=(RooRealVar*)ilpk.Next())){
            // double nFid= nTotIter[t2->GetName()][iter-1]*(1-notFidIterToy[t2->GetName()][iter-1]);
            hmeasT->SetBinContent(bin,normMap[bin-1]->getVal()*L->getVal());
            if(t2->getVal()>0)
              hmeasT->SetBinError(bin,t2->getError()/t2->getVal()* hmeasT->GetBinContent(bin));
            bin++;
          }
          
          for(int k=0;k<nPois;k++){
            htrueT->SetBinContent(k+1,initTrueEvents[k]);
            for(int j=0; j<nPois; j++){
              migMatrixT->SetBinContent(k+1,j+1,inputM[k][j]);
            }
        }
          
          //for( int bUnIter= doSeparateIterationsRoounfold ? 1:iter+1 ; bUnIter < doSeparateIterationsRoounfold ? nBaesIter:iter+2; bUnIter++){
          for( int bUnIter= 1 ; bUnIter <  nBaesIter;  bUnIter++){
            
            /*
            // for( bin=1; bin<hmeasT->GetNbinsX()+1; bin++)
            //cout<<"Events Bin "<<bin<<" "<<hmeasT->GetBinContent(bin)<<" +/- "<<hmeasT->GetBinError(bin)<<" true events "<<htrueT->GetBinContent(bin)<<endl;
            //RooUnfoldResponse rooUnfoldResponseT(hmeasT,hmeasT,migMatrixT,"testT","testT");
            RooUnfoldBayes   unfoldT (&rooUnfoldResponse, hmeasT, bUnIter+1);  
            //RooUnfoldBayes   unfoldT (&rooUnfoldResponseT, hmeasT, iter+1);  
            TH1D* hRecoT= (TH1D*) unfoldT.Hreco();
            //unfoldT.PrintTable (cout, htrueT);
            // tree related stuff 
            TIter ilpkl = pois->createIterator();
            t2=nullptr;
            bin=1;
            while((t2=(RooRealVar*)ilpkl.Next())){
              sigmaBinIBU[t2->GetName()].push_back(hRecoT->GetBinContent(bin)/(BR_ZZ_incl->getVal()*L->getVal()));
              sigmaBinIBU_e[t2->GetName()].push_back(hRecoT->GetBinError(bin)/hRecoT->GetBinContent(bin)* hRecoT->GetBinContent(bin)/(BR_ZZ_incl->getVal()*L->getVal()));
              //cout<<"Bayesian toy # "<<nToy<<" iteration "<<iter<<" "<<sigmaBinIBU[t2->GetName()].back()<<" +/- "<<sigmaBinIBU_e[t2->GetName()].back()<<" unfolded "<<hRecoT->GetBinContent(bin)<<" +/- "<<hRecoT->GetBinError(bin)<<" truth  "<<htrueT->GetBinContent(bin)<<" reco "<<hmeasT->GetBinContent(bin)<<endl;
              bin++;
            }
            */

            // Here minimize the regularized ws as a function of tau
            //double deltatau=0.05;
            double deltatau=0.2;
            //if(bUnIter==1) tau->setVal(0.0);
            //else 
            //tau->setVal(tau->getVal()+deltatau);

            //tau->setVal(-1*(1.0 - TMath::Exp(deltatau*bUnIter*bUnIter)));
            tau->setVal(-1*(1.0 - TMath::Exp(deltatau*bUnIter)));
            
            RooAddition *NllAdd=new RooAddition("nllSum","nullSum",RooArgList(*Nllt,*regSum));
            RooMinimizer *minimtreg = nullptr;
            int regstatus=-1;
            minimtreg=new RooMinimizer(*NllAdd);              ////OffSet true default
            minimtreg->optimizeConst(2);
            minimtreg->setStrategy(2);
            regstatus=minimtreg->minimize("Minuit2");
            if(useMinos)
              regstatus+=minimtreg->minos(*pois);
            if(regstatus!=0) {
              delete NllAdd;
              delete minimtreg;
              
              TIter ilpkl = pois->createIterator();
              t2=nullptr;
              while((t2=(RooRealVar*)ilpkl.Next())){
                sigmaBinIBU[t2->GetName()].push_back(-999);
                sigmaBinIBU_e[t2->GetName()].push_back(-999);
              }
              continue;
            }
            
            TIter ilpkl = pois->createIterator();
            t2=nullptr;
            bin=1;
            while((t2=(RooRealVar*)ilpkl.Next())){
              sigmaBinIBU[t2->GetName()].push_back(t2->getVal());
              if(useMinos)
                sigmaBinIBU_e[t2->GetName()].push_back(0.5*(t2->getErrorHi()+fabs(t2->getErrorLo())));
              else 
                sigmaBinIBU_e[t2->GetName()].push_back(t2->getError());
              //cout<<"Regularized toy # "<<nToy<<" iteration "<<bUnIter<<" tau "<<tau->getVal()<<" "<<sigmaBinIBU[t2->GetName()].back()<<" +/- "<<sigmaBinIBU_e[t2->GetName()].back()<<endl;
            }
            
            delete minimtreg;
            delete NllAdd;
          }
          delete htrueT;
          delete hmeasT;
          delete migMatrixT;
          
          w->loadSnapshot("nominal");
          delete minim3t;
          delete Nll3t;
          delete Nllt;
    }

    //for(vector<pair<string,string>>::iterator it=matRegions.begin(); it!=matRegions.end(); it++){
    //for(int n=0; n<nPois; n++){
    //weightBdtBranch[Form("weight_%s_bin%d",(*it).first.c_str(),n)]->SetAddress(&weightBdt[Form("weight_%s_bin%d",(*it).first.c_str(),n)]);
    //}}

    //weightBdt = weightsBdt2;
    for(vector<pair<string,string>>::iterator it=matRegions.begin(); it!=matRegions.end(); it++){
      for(int n=0; n<nPois; n++){
	cout<<"Branch name "<<weightBdtBranch[Form("weight_%s_bin%d",(*it).first.c_str(),n)]->GetName()<<endl;
	cout<<"With address "<<weightBdtBranch[Form("weight_%s_bin%d",(*it).first.c_str(),n)]->GetAddress()<<endl;
      }}
    
    
    toysTree->Fill();
    delete toy;
 }
 // plot the resutls 


   RooMsgService::instance().setSilentMode(0);
   RooMsgService::instance().setGlobalKillBelow(RooFit::INFO);   


   // Draw everything 

   std::map<int, std::map<string,TCanvas*>> ccPullToys;
   //for(std::map<int,std::map<string, TH1F*>>::iterator it=pullXsecBin.begin(); it!=pullXsecBin.end(); it++){
     for(std::map<int,std::map<string, TH1F*>>::iterator it=DiffXsecBin.begin(); it!=DiffXsecBin.end(); it++){
     for( std::map<string,TH1F*>::iterator it2=(*it).second.begin(); it2!=(*it).second.end(); it2++){
       ccPullToys[(*it).first][(*it2).first]= new TCanvas(Form("canvas_%s",((*it2).second)->GetName()),"");{
         SetCanvasDefaults(ccPullToys[(*it).first][(*it2).first]);
         ccPullToys[(*it).first][(*it2).first]->cd();
         (*it2).second->Draw("HIST PE");
         DiffXsecBinCF[(*it).first][(*it2).first]->Draw("hist  same");


       }
     }
   }


 std::map<int, std::map<string,TCanvas*>> ccPullToysErrors;
   //for(std::map<int,std::map<string, TH1F*>>::iterator it=pullXsecBin.begin(); it!=pullXsecBin.end(); it++){
     for(std::map<int,std::map<string, TH1F*>>::iterator it=DiffXSecBinError.begin(); it!=DiffXSecBinError.end(); it++){
     for( std::map<string,TH1F*>::iterator it2=(*it).second.begin(); it2!=(*it).second.end(); it2++){
       ccPullToysErrors[(*it).first][(*it2).first]= new TCanvas(Form("cErrs_%s",((*it2).second)->GetName()),"");{
         SetCanvasDefaults(ccPullToysErrors[(*it).first][(*it2).first]);
         ccPullToysErrors[(*it).first][(*it2).first]->cd();
         (*it2).second->Draw("HIST PE");
         DiffXSecBinErrorCF[(*it).first][(*it2).first]->Draw("hist  same");
       }
     }
   }


     cout<<"Comparison"<<endl;
     for(std::map<int,std::map<string, TH1F*>>::iterator it=DiffXsecBin.begin(); it!=DiffXsecBin.end(); it++){
       cout<<"Iteration "<<(*it).first<<endl;
       for( std::map<string,TH1F*>::iterator it2=(*it).second.begin(); it2!=(*it).second.end(); it2++){
         int nq=1;
         Double_t xq[1];  // position where to compute the quantiles in [0,1]
         Double_t yqCF[1];
         Double_t yq[1];  // array to contain the quantiles
         xq[0]=0.683;
         DiffXSecBinError[(*it).first][(*it2).first]->GetQuantiles(nq,yq,xq);
         DiffXSecBinErrorCF[(*it).first][(*it2).first]->GetQuantiles(nq,yqCF,xq);
         cout<<"Bin "<<(*it2).first<<" Matrix:"<<(*it2).second->GetRMS()<<" Mean error "<<yq[0]<<" CF: "<<
           DiffXsecBinCF[(*it).first][(*it2).first]->GetRMS()<<" Mean error "<<yqCF[0] <<" true error "<<trueSigma[(*it2).first]<<" Pulls: Matrix mu:"<<pullXsecBin[(*it).first][(*it2).first]->GetMean()<<" +/- "<<
           pullXsecBin[(*it).first][(*it2).first]->GetMeanError()<<" sigma: "<<pullXsecBin[(*it).first][(*it2).first]->GetRMS()<<" CF: "<<pullXsecBinCF[(*it).first][(*it2).first]->GetMean()<<" +/- "<<
           pullXsecBinCF[(*it).first][(*it2).first]->GetMeanError()<<" sigma: "<<pullXsecBinCF[(*it).first][(*it2).first]->GetRMS()<<endl;

         //cout<<"Bin "<<(*it2).first<<" Matrix:"<<(*it2).second->GetRMS()<<" Mean error "<<DiffXSecBinError[(*it).first][(*it2).first]->GetMean()<<" CF: "<<
         //DiffXSecBinErrorCF[(*it).first][(*it2).first]->GetRMS()<<" Mean error "<<DiffXSecBinErrorCF[(*it).first][(*it2).first]->GetMean()<<endl;

         //         (*it2).second->SetDirectory(fout);
         (*it2).second->Write();

         //DiffXsecBinCF[(*it).first][(*it2).first]->SetDirectory(fout);
         DiffXsecBinCF[(*it).first][(*it2).first]->Write();

         //         pullXsecBin[(*it).first][(*it2).first]->SetDirectory(fout);
         pullXsecBin[(*it).first][(*it2).first]->Write();

         //         pullXsecBinCF[(*it).first][(*it2).first]->SetDirectory(fout);
         pullXsecBinCF[(*it).first][(*it2).first]->Write();

         //         DiffXSecBinErrorCF[(*it).first][(*it2).first]->SetDirectory(fout);
         DiffXSecBinErrorCF[(*it).first][(*it2).first]->Write();

         //         DiffXSecBinError[(*it).first][(*it2).first]->SetDirectory(fout);
         DiffXSecBinError[(*it).first][(*it2).first]->Write();
       }}

    
     toysTree->Write();
     fout->Close();
     return 0;
}
